﻿Public Class clsUserMenu

    Private _AppID As String
    Public Property AppID() As String
        Get
            Return _AppID
        End Get
        Set(ByVal value As String)
            _AppID = value
        End Set
    End Property

    Private _MenuID As String
    Public Property MenuID() As String
        Get
            Return _MenuID
        End Get
        Set(ByVal value As String)
            _MenuID = value
        End Set
    End Property

    Private _MenuName As String
    Public Property MenuName() As String
        Get
            Return _MenuName
        End Get
        Set(ByVal value As String)
            _MenuName = value
        End Set
    End Property

    Private _MenuDesc As String
    Public Property MenuDesc() As String
        Get
            Return _MenuDesc
        End Get
        Set(ByVal value As String)
            _MenuDesc = value
        End Set
    End Property

    Private _GroupID As String
    Public Property GroupID() As String
        Get
            Return _GroupID
        End Get
        Set(ByVal value As String)
            _GroupID = value
        End Set
    End Property

    Private _MenuIndex As String
    Public Property MenuIndex() As String
        Get
            Return _MenuIndex
        End Get
        Set(ByVal value As String)
            _MenuIndex = value
        End Set
    End Property

    Private _GroupIndex As String
    Public Property GroupIndex() As String
        Get
            Return _GroupIndex
        End Get
        Set(ByVal value As String)
            _GroupIndex = value
        End Set
    End Property

    Private _SeqNo As String
    Public Property SeqNo() As String
        Get
            Return _SeqNo
        End Get
        Set(ByVal value As String)
            _SeqNo = value
        End Set
    End Property

    Private _AllowAccess As String
    Public Property AllowAccess() As String
        Get
            Return _AllowAccess
        End Get
        Set(ByVal value As String)
            _AllowAccess = value
        End Set
    End Property

    Private _AllowUpdate As String
    Public Property AllowUpdate() As String
        Get
            Return _AllowUpdate
        End Get
        Set(ByVal value As String)
            _AllowUpdate = value
        End Set
    End Property

    Private _AllowPrint As String
    Public Property AllowPrint() As String
        Get
            Return _AllowPrint
        End Get
        Set(ByVal value As String)
            _AllowPrint = value
        End Set
    End Property
End Class

﻿Imports System.Data.SqlClient

Public Class clsPasswordHistoryDB
#Region "Method"
    Public Shared Function GetServerDate(ConStr As String) As Date
        Using con As New SqlConnection(ConStr)
            con.Open()
            Dim q As String = "Select GetDate()"
            Dim cmd As New SqlCommand(q, con)
            Dim ServerDate As Date = cmd.ExecuteScalar
            Return ServerDate
        End Using
    End Function

    Public Shared Function GetLastData(ByVal pObj As clsPasswordHistory) As clsPasswordHistory
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim sql As String = ""
                sql = "select top 1 * from JSOX_PasswordHistory where UserID = @UserID order by UpdateDate desc"

                Dim cmd As New SqlCommand(sql, con)
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count = 0 Then
                    Return Nothing
                Else
                    With dt.Rows(0)
                        Dim History As New clsPasswordHistory
                        History.UserID = .Item("UserID")
                        History.SeqNo = .Item("SeqNo")
                        History.UpdateDate = .Item("UpdateDate")
                        History.Password = .Item("Password")
                        Return History
                    End With
                End If
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetList(UserID As String, Retain As Integer, ConStr As String) As List(Of clsPasswordHistory)
        Try
            Dim PassList As New List(Of clsPasswordHistory)

            Using con As New SqlConnection(ConStr)
                con.Open()
                Dim sql As String = ""
                sql = "select top " & Retain & " * from JSOX_PasswordHistory " & vbCrLf & _
                                        "where UserID = @UserID " & vbCrLf & _
                                        "order by UpdateDate desc"

                Dim cmd As New SqlCommand(sql, con)
                cmd.Parameters.AddWithValue("@UserID", UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)

                For i = 0 To dt.Rows.Count - 1
                    With dt.Rows(i)
                        Dim History As New clsPasswordHistory
                        History.UserID = .Item("UserID")
                        History.SeqNo = .Item("SeqNo")
                        History.UpdateDate = .Item("UpdateDate")
                        History.Password = .Item("Password")
                        PassList.Add(History)
                    End With
                Next
            End Using
            Return PassList
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pObj As clsPasswordHistory) As Integer
        'Using Cn As New SqlConnection(pObj.ConnectionString)
        '    Cn.Open()
        '    Dim q As String = "Insert into JSOX_PasswordHistory (AppID, UserID, SeqNo, UpdateDate, Password) values " & vbCrLf &
        '        "('P01', @UserID, (select isnull(max(SeqNo), 0) + 1 from JSOX_PasswordHistory where UserID = @UserID), " & vbCrLf &
        '        "GetDate(), @Password)"
        '    Dim cmd As New SqlCommand(q, Cn)
        '    cmd.Parameters.AddWithValue("UserID", pObj.UserID)
        '    cmd.Parameters.AddWithValue("Password", pObj.Password)
        '    Dim i As Integer = cmd.ExecuteNonQuery
        '    Return i
        'End Using
    End Function
#End Region
    
End Class

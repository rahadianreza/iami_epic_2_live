﻿Public Class clsJSOXSetup
    Inherits clsCon

#Region "Initial"
    Private _RuleID As String
    Public Property RuleID() As String
        Get
            Return _RuleID
        End Get
        Set(ByVal value As String)
            _RuleID = value
        End Set
    End Property

    Private _Enable As Boolean
    Public Property Enable() As Boolean
        Get
            Return _Enable
        End Get
        Set(ByVal value As Boolean)
            _Enable = value
        End Set
    End Property

    Private _Description As String
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal value As String)
            _Description = value
        End Set
    End Property

    Private _ParamValue As Integer
    Public Property ParamValue() As Integer
        Get
            Return _ParamValue
        End Get
        Set(ByVal value As Integer)
            _ParamValue = value
        End Set
    End Property

    Private _UpdateUser As String
    Public Property UpdateUser() As String
        Get
            Return _UpdateUser
        End Get
        Set(ByVal value As String)
            _UpdateUser = value
        End Set
    End Property
#End Region

    
End Class

﻿Imports System.Data.SqlClient

Public Class clsUserSetupDB
    ''' <summary>
    ''' Update Existing user
    ''' </summary>
    ''' <param name="pObj"></param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function Update(ByVal pObj As clsUserSetup) As Integer
        ' update existing user

        Try
            'Dim enc As New pisGlobal.clsDESEncryption("TOS")
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim sql As String
                sql = "Update usersetup set UserName=@UserName,Password=@Password," & _
                    "ShiftGroup=@ShiftGroup,Locked=@Locked,AdminStatus=@AdminStatus,Description=@Description," & _
                    "FirstLoginFlag=@FirstLoginFlag,UpdateUser=@UserLogin where appid=@AppID and UserID=@UserID"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                With cmd.Parameters
                    .AddWithValue("AppID", pObj.AppID)
                    .AddWithValue("UserID", pObj.UserID)
                    .AddWithValue("UserName", pObj.UserName)
                    '.AddWithValue("Password", enc.EncryptData(pUserSetup.Password)) 'encrypt password
                    .AddWithValue("Password", pObj.Password) 'encrypt password
                    .AddWithValue("ShiftGroup", pObj.ShiftGroup)
                    .AddWithValue("Locked", IIf(pObj.Locked = "Yes", "1", "0")) 'Yes:1, No:2
                    .AddWithValue("AdminStatus", IIf(pObj.AdminStatus = "Yes", "1", "0")) 'Yes:1, No:2
                    .AddWithValue("Description", pObj.Description)
                    .AddWithValue("FirstLoginFlag", IIf(pObj.FirstLoginFlag = "Yes", "1", "0")) 'Yes:1, No:2
                    .AddWithValue("UserLogin", pObj.UserLogin)
                End With

                Dim i As Integer
                i = cmd.ExecuteNonQuery
                cmd.Parameters.Clear()
                cmd.Dispose()
                Return i
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.Update error: " & ex.Message)
        End Try
    End Function

    ''' <summary>
    ''' Update new password
    ''' </summary>    
    ''' <param name="pObj"></param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function UpdatePassword(ByVal pObj As clsUserSetup) As Integer
        ' update new password

        'Dim enc As New pisGlobal.clsDESEncryption("TOS")
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand("Update Usersetup set password=@Password where appID=@AppID and UserID=@UserID", con)
                cmd.CommandType = CommandType.Text
                With cmd.Parameters
                    .AddWithValue("AppID", pObj.AppID)
                    .AddWithValue("UserID", pObj.UserID)
                    '.AddWithValue("Password", enc.EncryptData(pNewPassword)) 'encrypt password
                    .AddWithValue("Password", pObj.Password)
                End With

                Dim i As Integer
                i = cmd.ExecuteNonQuery
                cmd.Parameters.Clear()
                cmd.Dispose()
                Return i
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.UpdatePassword error: " & ex.Message)
        End Try
    End Function

    ''' <summary>
    ''' Update security question and security answer
    ''' </summary>    
    ''' <param name="pObj"></param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function UpdateSecurityQuestion(ByVal pObj As clsUserSetup) As Integer
        ' update new password

        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand("Update Usersetup set SecurityQuestion=@SecurityQuestion," & _
                                          " SecurityAnswer=@SecurityAnswer where appID=@AppID and UserID=@UserID", con)
                cmd.CommandType = CommandType.Text
                With cmd.Parameters
                    .AddWithValue("AppID", pObj.AppID)
                    .AddWithValue("UserID", pObj.UserID)
                    .AddWithValue("SecurityQuestion", pObj.SecurityQuestion)
                    .AddWithValue("SecurityAnswer", pObj.SecurityAnswer)
                End With

                Dim i As Integer
                i = cmd.ExecuteNonQuery
                cmd.Parameters.Clear()
                cmd.Dispose()
                Return i
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.UpdateSecurityQuestion error: " & ex.Message)
        End Try
    End Function

    ''' <summary>
    ''' Update user first login flag
    ''' </summary>
    ''' <param name="pObj"></param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function UpdateFirstLogin(ByVal pObj As clsUserSetup) As Integer
        ' update new password

        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand("Update Usersetup set FirstLoginFlag=@FirstLoginFlag" & _
                                          " where appID=@AppID and UserID=@UserID", con)
                cmd.CommandType = CommandType.Text
                With cmd.Parameters
                    .AddWithValue("AppID", pObj.AppID)
                    .AddWithValue("UserID", pObj.UserID)
                    .AddWithValue("FirstLoginFlag", pObj.FirstLoginFlag)
                End With

                Dim i As Integer
                i = cmd.ExecuteNonQuery
                cmd.Parameters.Clear()
                cmd.Dispose()
                Return i
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.UpdateFirstLogin error: " & ex.Message)
        End Try
    End Function

    ''' <summary>
    ''' Update last login
    ''' </summary>
    ''' <param name="pObj"></param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function UpdateLastLogin(ByVal pObj As clsUserSetup) As Integer
        ' update new password

        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand("Update Usersetup set LastLogin=@LastLogin" & _
                                          " where appID=@AppID and UserID=@UserID", con)
                cmd.CommandType = CommandType.Text
                With cmd.Parameters
                    .AddWithValue("AppID", pObj.AppID)
                    .AddWithValue("UserID", pObj.UserID)
                    .AddWithValue("LastLogin", pObj.LastLogin)
                End With

                Dim i As Integer
                i = cmd.ExecuteNonQuery
                cmd.Parameters.Clear()
                cmd.Dispose()
                Return i
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.UpdateLastLogin error: " & ex.Message)
        End Try
    End Function

    Shared Function UpdateLockStatus(ByVal pObj As clsUserSetup) As Integer
        ' update new password

        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand("Update Usersetup set Locked=@Locked" & _
                                          " where appID=@AppID and UserID=@UserID", con)
                cmd.CommandType = CommandType.Text
                With cmd.Parameters
                    .AddWithValue("AppID", pObj.AppID)
                    .AddWithValue("UserID", pObj.UserID)
                    .AddWithValue("Locked", pObj.Locked)
                End With

                Dim i As Integer
                i = cmd.ExecuteNonQuery
                cmd.Parameters.Clear()
                cmd.Dispose()
                Return i
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.UpdateLockStatus error: " & ex.Message)
        End Try
    End Function

    ''' <summary>
    ''' Insert New user
    ''' </summary>
    ''' <param name="pObj"></param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function Insert(ByVal pObj As clsUserSetup) As Integer
        ' Insert new user to database
        Try
            'Dim enc As New pisGlobal.clsDESEncryption("TOS")
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim sql As String
                sql = "Insert into usersetup (AppID,UserID, UserName, Password,ShiftGroup,Locked, AdminStatus," & _
                    "Description,FirstLoginFlag,Entrydate,EntryUser) VALUES (@AppID,@UserID, @UserName, @Password," & _
                    "@ShiftGroup,@Locked, @AdminStatus,@Description,@FirstLoginFlag,Getdate(),@UserLogin)"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                With cmd.Parameters
                    .AddWithValue("AppID", pObj.AppID)
                    .AddWithValue("UserID", pObj.UserID)
                    .AddWithValue("UserName", pObj.UserName)
                    '.AddWithValue("Password", enc.EncryptData(pUserSetup.Password)) 'encrypt password
                    .AddWithValue("Password", pObj.Password)
                    .AddWithValue("ShiftGroup", pObj.ShiftGroup)
                    .AddWithValue("Locked", IIf(pObj.Locked = "Yes", "1", "0")) 'Yes:1, No:2
                    .AddWithValue("AdminStatus", IIf(pObj.AdminStatus = "Yes", "1", "0")) 'Yes:1, No:2
                    .AddWithValue("Description", pObj.Description)
                    .AddWithValue("FirstLoginFlag", IIf(pObj.FirstLoginFlag = "Yes", "1", "0")) 'Yes:1, No:2
                    .AddWithValue("UserLogin", pObj.UserLogin)
                End With

                Dim i As Integer
                i = cmd.ExecuteNonQuery
                cmd.Parameters.Clear()
                cmd.Dispose()
                Return i
            End Using

        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.Insert error: " & ex.Message)
        End Try
    End Function

    ''' <summary>
    ''' Delete existing user and user privilege
    ''' </summary>
    ''' <param name="pObj"></param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function Delete(ByVal pObj As clsUserSetup) As Integer
        If pObj.UserID = "" Then
            Throw New Exception("User ID parameter is required!")
        End If

        If pObj.AppID = "" Then
            Throw New Exception("App ID parameter is required!")
        End If

        Dim i As Integer
        Try
            ' Delete existing user and menu
            Using scope As New Transactions.TransactionScope
                Using con As New SqlConnection(pObj.ConnectionString)
                    con.Open()
                    Dim cmd As New SqlCommand("Delete from usersetup where userid=@UserID and AppID=@AppID ", con)
                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.AddWithValue("@AppID", pObj.AppID)
                    cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                    i = cmd.ExecuteNonQuery
                    cmd.Parameters.Clear()

                    cmd.CommandText = "Delete from userprivilege where UserID=@UserID and AppID=@AppID"
                    cmd.Parameters.AddWithValue("@AppID", pObj.AppID)
                    cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                    i = cmd.ExecuteNonQuery
                    cmd.Parameters.Clear()

                    cmd.Dispose()
                End Using
                scope.Complete()
            End Using

            Return i
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.Delete error: " & ex.Message)
        End Try

    End Function

    
    'Shared Function GetUserID(ByVal pObj As clsUserSetup) As clsUserSetup
    '    ' get user detail
    '    Dim sql As String        
    '    Dim c_user As New clsUserSetup

    '    sql = " select * from usersetup where userid=@UserID"
    '    Try
    '        Using con As New SqlConnection(pObj.ConnectionString)
    '            con.Open()
    '            Dim cmd As New SqlCommand(sql)
    '            cmd.Connection = con
    '            cmd.Parameters.AddWithValue("UserID", pObj.UserID)
    '            Dim da As New SqlDataAdapter(cmd)
    '            Dim ds As New DataSet
    '            da.Fill(ds)
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                With ds.Tables(0)
    '                    c_user.AppID = .Rows(0)("AppID")
    '                    c_user.UserID = .Rows(0)("UserID").ToString.Trim
    '                    c_user.UserName = .Rows(0)("UserName").ToString.Trim & ""
    '                    'c_user.Password = enc.DecryptData(.Rows(0)("Password"))
    '                    c_user.Password = .Rows(0)("Password")
    '                    c_user.ShiftGroup = .Rows(0)("ShiftGroup").ToString.Trim & ""
    '                    c_user.Locked = .Rows(0)("Locked")
    '                    c_user.AdminStatus = .Rows(0)("AdminStatus").ToString.Trim & ""
    '                    c_user.Description = .Rows(0)("Description").ToString.Trim & ""
    '                    c_user.FirstLoginFlag = .Rows(0)("FirstLoginFlag") & ""
    '                    c_user.SecurityQuestion = .Rows(0)("SecurityQuestion").ToString.Trim & ""
    '                    c_user.SecurityAnswer = .Rows(0)("SecurityAnswer").ToString.Trim & ""
    '                End With                                    
    '            End If
    '            da.Dispose()
    '            cmd.Parameters.Clear()
    '            cmd.Dispose()
    '        End Using
    '    Catch ex As Exception
    '        Throw New Exception("clsUserSetupDB.GetUserID error: " & ex.Message)
    '        Return Nothing
    '    End Try
    '    Return c_user
    'End Function

    Public Shared Function getPassword(ByVal pObj As clsUserSetup) As String
        Dim sql As String = ""
        Dim sPassword As String = ""
        Dim dt As New DataTable
        Dim cmd As SqlCommand
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()

                sql = " select Password from UserSetup where UserID = @UserID "

                cmd = New SqlCommand
                cmd.Connection = con
                cmd.CommandText = sql
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                Dim da As New SqlDataAdapter(cmd)                
                da.Fill(dt)                
                If dt.Rows.Count > 0 Then
                    sPassword = dt.Rows(dt.Rows.Count - 1)("Password")
                End If
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.getPassword error: " & ex.Message)
        End Try
        Return sPassword
    End Function

    Public Shared Function GetList(ByVal pObj As clsUserSetup, Optional ByRef pErr As String = "") As List(Of clsUserSetup)
        Dim clsDESEncryption As New clsDESEncryption("TOS")
        Dim cmd As SqlCommand
        Dim sql As String

        Try
            Using con As New SqlConnection(pObj.ConnectionString)


                'sql = " select  AppID, ISNULL(UserID ,'') UserID, ISNULL(UserName,'') UserName, ISNULL(UserType,0) UserType, Password, ISNULL(Description,'') Description , isnull(User_Email,'')User_Email," & vbCrLf & _
                '    "           AdminStatus, COALESCE(B.Supplier_Name,'') ,ISNULL(CatererID,'') CatererID,COALESCE(A.Supplier_Code,'') Supplier_Code, Locked, CreateDate, CreateUser, " & vbCrLf & _
                '    " COALESCE(Department_Code,'')Department_Code,COALESCE(Section_Code,'')Section_Code,COALESCE(Job_Position,'')Job_Position" & vbCrLf & _
                '    " from dbo.UserSetup A " & vbCrLf & _
                '    " LEFT JOIN dbo.Mst_Supplier B ON A.Supplier_Code = B.Supplier_Code " & vbCrLf & _
                '    " where AppID = @AppID " & vbCrLf

                sql = "sp_UserSetup_ListUser"

                cmd = New SqlCommand(sql, con)
                cmd.Connection = con
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@AppID", pObj.AppID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim Users As New List(Of clsUserSetup)
                For i = 0 To dt.Rows.Count - 1
                    Dim User As New clsUserSetup With {
                            .AppID = dt.Rows(i)("AppID"),
                            .UserID = dt.Rows(i)("UserID").ToString.Trim,
                            .UserName = dt.Rows(i)("UserName").ToString.Trim(),
                            .UserType = dt.Rows(i)("UserType") & "",
                            .Password = clsDESEncryption.DecryptData(dt.Rows(i)("Password")),
                            .Description = dt.Rows(i)("Description").ToString.Trim(),
                            .CatererID = dt.Rows(i)("CatererID").ToString.Trim & "",
                            .Supplier_Code = dt.Rows(i)("Supplier_Code").ToString.Trim & "",
                            .Department_Code = dt.Rows(i)("Department_Code").ToString.Trim & "",
                            .Section_Code = dt.Rows(i)("Section_Code").ToString.Trim & "",
                            .Job_Position = dt.Rows(i)("Job_Position").ToString.Trim & "",
                            .User_Email = dt.Rows(i)("User_Email").ToString.Trim,
                            .PIC_PO = dt.Rows(i)("PIC_PO").ToString.Trim,
                            .Locked = dt.Rows(i)("Locked").ToString & ""}
                    Users.Add(User)
                Next
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
                Return Users
            End Using
        Catch ex As Exception            
            pErr = "clsUserSetupDB.getPassword error: " & ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetData(ByVal pUserID As String, ConStr As String) As clsUserSetup
        Using cn As New SqlConnection(ConStr)
            Dim sql As String = "SELECT * FROM UserSetup WHERE userID = @UserID "
            Dim Cmd As New SqlCommand(sql, cn)
            Cmd.Parameters.AddWithValue("UserID", pUserID)
            Dim da As New SqlDataAdapter(Cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Dim User As New clsUserSetup With {
                        .AppID = dt.Rows(0)("AppID"),
                        .UserID = dt.Rows(0)("UserID"),
                        .UserName = dt.Rows(0)("UserName"),
                        .UserType = dt.Rows(0)("UserType") & "",
                        .Password = dt.Rows(0)("Password"),
                        .CatererID = (dt.Rows(0)("CatererID") & "").ToString.Trim,
                        .Description = dt.Rows(0)("Description") & "",
                        .Locked = dt.Rows(0)("Locked") & "",
                        .PIC_PO = dt.Rows(0)("PIC_PO") & "",
                        .PasswordHint = dt.Rows(0)("PasswordHint") & "",
                        .SecurityQuestion = dt.Rows(0)("SecurityQuestion") & "",
                        .SecurityAnswer = dt.Rows(0)("SecurityAnswer") & "",
                        .AdminStatus = dt.Rows(0)("AdminStatus") & ""}

                Dim enc As New clsDESEncryption("TOS")
                Dim pwd As String = enc.DecryptData(User.Password)

                Return User
            Else
                Return Nothing
            End If
        End Using
    End Function

    Public Shared Function GetUserID(ByVal pObj As clsUserSetup, Optional ByRef pErr As String = "") As clsUserSetup
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                Dim sql As String = "SELECT * FROM UserSetup WHERE AppID = @AppID and UserID= @UserID "
                Dim cmd As New SqlCommand(sql, con)
                cmd.Parameters.AddWithValue("@AppID", pObj.AppID)
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    Dim User As New clsUserSetup With {
                            .AppID = dt.Rows(0)("AppID"),
                            .UserID = dt.Rows(0)("UserID"),
                            .UserName = dt.Rows(0)("UserName"),
                            .UserType = dt.Rows(0)("UserType"),
                            .Password = dt.Rows(0)("Password"),
                            .Description = dt.Rows(0)("Description") & "",
                            .Locked = dt.Rows(0)("Locked") & "",
                              .PIC_PO = dt.Rows(0)("PIC_PO") & "",
                            .AdminStatus = dt.Rows(0)("AdminStatus") & "",
                            .CatererID = dt.Rows(0)("CatererID"),
                            .CreateDate = dt.Rows(0)("CreateDate") & "",
                            .UpdateDate = dt.Rows(0)("UpdateDate") & ""}

                    Dim enc As New clsDESEncryption("TOS")
                    Dim pwd As String = enc.DecryptData(User.Password)

                    If pObj.CONN_METHOD = clsCon.CONNECTION_METHOD.ASPNET Then
                        If pObj.Password <> pwd Then
                            Return Nothing
                        End If
                    End If
                    
                    Return User
                End If
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = "clsUserSetupDB.GetUserID error: " & ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UnlockUser(UserID As String, Interval As Integer, ConStr As String) As Integer
        Using Con As New SqlConnection(ConStr)
            Con.Open()
            Dim q As String = _
                "update UserSetup set Locked = 0, FailedLogin = 0 " & vbCrLf & _
                "where FailedLogin >= 3 And DateDiff(Second, isnull(LastLogin, '2000-01-01'), GetDate()) > @Interval * 60 " & vbCrLf & _
                "and UserID = @UserID " & vbCrLf
            Dim cmd As New SqlCommand(q, Con)
            cmd.Parameters.AddWithValue("UserID", UserID)
            cmd.Parameters.AddWithValue("Interval", Interval)
            Dim i As Integer = cmd.ExecuteNonQuery
            Return i
        End Using
    End Function

    Public Shared Function AddFailedLogin(ByVal pObj As clsUserSetup) As Integer
        Dim i As Integer
        Try
            'Using con As New SqlConnection(pObj.ConnectionString)
            '    con.Open()
            '    Dim q As String = "Update UserSetup set FailedLogin = isnull(FailedLogin, 0) + 1, LastLogin = GetDate() where UserID = @UserID and isnull(AdminStatus, '0') <> '1' "
            '    Dim cmd As New SqlCommand(q, con)
            '    cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
            '    i = cmd.ExecuteNonQuery

            '    Dim MaxLogin As Integer = 0
            '    q = "select * from JSOXSetup where RuleID = 4 and Enable = 1 "
            '    cmd = New SqlCommand(q, con)
            '    Dim da As New SqlDataAdapter(cmd)
            '    Dim dt As New DataTable
            '    da.Fill(dt)
            '    If dt.Rows.Count > 0 Then
            '        MaxLogin = dt.Rows(0)("ParamValue")
            '    End If

            '    If MaxLogin > 0 Then
            '        q = "Update UserSetup set Locked = 1 where UserID = @UserID and isnull(FailedLogin, 0) >= " & MaxLogin & vbCrLf & _
            '            "and isnull(AdminStatus, '0') <> '1' "
            '        cmd = New SqlCommand(q, con)
            '        cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
            '        i = cmd.ExecuteNonQuery
            '    End If
            'End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.GetUserID error: " & ex.Message)
            Return Nothing
        End Try
        Return i
    End Function

    Public Shared Function GetUserLock(ByVal pObj As clsUserSetup, Optional ByRef pErr As String = "") As Boolean
        Dim Locked As Boolean = False
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                Dim sql As String = "select * from UserSetup where UserID = @UserID and isnull(Locked, '0') <> '1' "
                Dim cmd As New SqlCommand(sql, con)
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    Locked = True
                End If
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
                Return Locked
            End Using
        Catch ex As Exception
            pErr = "clsUserSetupDB.GetUserLock error: " & ex.Message
            Return Locked
        End Try
    End Function

    Public Shared Function UpdateLock(ByVal pObj As clsUserSetup) As Integer
        Dim i As Integer
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim q As String = "Update UserSetup set Locked = @Locked where UserID = @UserID"
                Dim cmd As New SqlCommand(q, con)
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                cmd.Parameters.AddWithValue("@Locked", pObj.Locked)
                i = cmd.ExecuteNonQuery
                cmd.Parameters.Clear()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.UpdateLock error: " & ex.Message)
            Return Nothing
        End Try
        Return i
    End Function

    Public Shared Function ResetFailedLogin(ByVal pObj As clsUserSetup) As Integer
        Dim i As Integer
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim q As String = "Update UserSetup set FailedLogin = 0 where UserID = @UserID"
                Dim cmd As New SqlCommand(q, con)
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                i = cmd.ExecuteNonQuery
                cmd.Parameters.Clear()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.ResetFailedLogin error: " & ex.Message)
            Return Nothing
        End Try
        Return i
    End Function

    Public Shared Function GetListMenuPrivilege(ByVal pObj As clsUserSetup, Optional ByRef pErr As String = "") As List(Of clsUserMenu)
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                Dim sql As String = "  SELECT GroupID, USM.MenuID,   " & vbCrLf &
                                  "  MenuDesc, " & vbCrLf &
                                  "  ISNULL(AllowAccess,'0') AS AllowAccess,  " & vbCrLf &
                                  "  ISNULL(AllowUpdate,'0') AS AllowUpdate  " & vbCrLf &
                                  "  FROM UserMenu USM " & vbCrLf &
                                  "  LEFT JOIN (SELECT * FROM UserPrivilege WHERE UserID=@UserID ) UP   " & vbCrLf &
                                  "  ON USM.AppID = UP.AppID AND USM.MenuID=UP.MenuID    " & vbCrLf &
                                  "  WHERE USM.AppID=@AppID" & vbCrLf &
                                  "  ORDER BY USM.MenuID  "
                Dim cmd As New SqlCommand(sql, con)
                cmd.Parameters.AddWithValue("@AppID", pObj.AppID)
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim MenuList As New List(Of clsUserMenu)
                For i = 0 To dt.Rows.Count - 1
                    Dim Menu As New clsUserMenu
                    Menu.GroupID = dt.Rows(i)("GroupID")
                    Menu.MenuID = dt.Rows(i)("MenuID")
                    Menu.MenuDesc = dt.Rows(i)("MenuDesc")
                    Menu.AllowAccess = dt.Rows(i)("AllowAccess")
                    Menu.AllowUpdate = dt.Rows(i)("AllowUpdate")
                    MenuList.Add(Menu)
                Next
                Return MenuList
            End Using
        Catch ex As Exception
            pErr = "clsUserSetupDB.GetListMenuPrivilege error: " & ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetListUserIDSearch(ByVal pObj As clsUserSetup, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(pObj.ConnectionString)                
                sql = " select * from dbo.UserSetup " & vbCrLf

                If pObj.UserID <> "" Then
                    sql = sql + " where UserID = @UserID "
                End If

                Dim cmd As New SqlCommand(sql, con)
                If pObj.UserID <> "" Then
                    cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                End If
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                If pObj.UserID <> "" Then
                    cmd.Parameters.Clear()
                End If
                cmd.Dispose()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
        Return ds
    End Function

    'Shared Function GetList(ByVal pObj As clsUserSetup) As List(Of clsUserSetup)
    '    ' get user detail    
    '    Dim sql As String
    '    Dim c_users As New List(Of clsUserSetup)
    '    Try
    '        sql = "select   AppID, UserName, UserID, Password, ShiftGroup, " & vbCrLf & _
    '            "           Description, SecurityQuestion, SecurityAnswer, Locked, AdminStatus, FirstLoginFlag " & vbCrLf & _
    '            " from usersetup where appID=@AppID " & vbCrLf & _
    '            " order by userid "
    '        Using cn As New SqlConnection(pObj.ConnectionString)
    '            cn.Open()
    '            Dim cmd As New SqlCommand()
    '            cmd.CommandText = sql
    '            cmd.Connection = cn
    '            cmd.Parameters.AddWithValue("@AppID", pObj.AppID)
    '            Dim rd As SqlDataReader
    '            rd = cmd.ExecuteReader

    '            Do While rd.Read
    '                Dim c_user As New clsUserSetup
    '                c_user.AppID = rd("AppID").ToString.Trim
    '                c_user.UserName = rd("UserName").ToString.Trim
    '                c_user.UserID = rd("UserID").ToString.Trim
    '                'c_user.Password = enc.DecryptData(rd("Password"))
    '                c_user.Password = rd("Password")
    '                c_user.ShiftGroup = rd("ShiftGroup").ToString.Trim
    '                c_user.Description = rd("Description").ToString.Trim
    '                c_user.SecurityQuestion = rd("SecurityQuestion").ToString.Trim & ""
    '                c_user.SecurityAnswer = rd("SecurityAnswer").ToString.Trim & ""
    '                c_user.Locked = IIf(rd("Locked") & "" = "1", "Yes", "No")
    '                c_user.AdminStatus = IIf(rd("AdminStatus") & "" = "1", "Yes", "No")
    '                c_user.FirstLoginFlag = IIf(rd("FirstLoginFlag") & "" = "1", "Yes", "No")
    '                c_users.Add(c_user)
    '            Loop
    '            rd.Close()
    '            cmd.Parameters.Clear()
    '            cmd.Dispose()
    '        End Using
    '    Catch ex As Exception
    '        Throw New Exception("clsUserSetupDB.GetList error: " & ex.Message)
    '        Return Nothing
    '    End Try
    '    Return c_users
    'End Function

    ''' <summary>
    ''' Get datatable all user
    ''' </summary>
    ''' <param name="pObj"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function GetDataTable(ByVal pObj As clsUserSetup) As DataTable
        ' get dataset of user
        Dim sql As String
        Dim dt As New DataTable

        sql = "select * from UserSetup where appID=@AppID order by userid"

        Try
            Using con = New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand()
                cmd.CommandText = sql
                cmd.Connection = con
                cmd.Parameters.AddWithValue("@AppID", pObj.AppID)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            Throw New Exception("clsUsersetupDB.GetDataTable error: " & ex.Message)
            Return Nothing
        End Try
        Return dt
    End Function

    ''' <summary>
    ''' get user information
    ''' </summary>
    ''' <param name="pObj"></param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function GetLastLogin(ByVal pObj As clsUserSetup) As String
        ' get user detail
        Dim sql As String
        Dim lastlogin As String
        'Dim enc As New pisGlobal.clsDESEncryption("TOS")

        sql = " select LastLogin from usersetup where userid=@UserID"
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand(sql)
                cmd.Connection = con
                cmd.Parameters.AddWithValue("UserID", pObj.UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    With ds.Tables(0)
                        lastlogin = CDate(.Rows(0)("LastLogin")).ToString("dd-MMM-yyyy HH:mm:ss")
                    End With
                Else
                    lastlogin = ""
                End If
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.GetLastLogin error: " & ex.Message)
            Return Nothing
        End Try
        Return lastlogin
    End Function

    ''' <summary>
    ''' Get single datatable of user
    ''' </summary>
    ''' <param name="pObj"></param>    
    ''' <returns></returns>
    ''' <remarks></remarks>
    Shared Function GetDataTableByUserID(ByVal pObj As clsUserSetup) As DataTable
        ' get dataset of user
        Dim sql As String
        Dim dt As New DataTable

        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()

                sql = " select   AppID, UserID, UserName, UserType, Password, ShiftGroup, Locked, AdminStatus, Description, SecurityQuestion, SecurityAnswer, FirstLoginFlag, LastLogin, " & vbCrLf & _
                    "           FailedLogin, CatererID, CreateDate, CreateUser, UpdateDate, UpdateUser " & vbCrLf & _
                    " from  UserSetup " & vbCrLf & _
                    " where appID=@AppID and userID=@UserID " & vbCrLf & _
                    " order by userid "

                Dim cmd As New SqlCommand()
                cmd.CommandText = sql
                cmd.Connection = con
                cmd.Parameters.AddWithValue("@AppID", pObj.AppID)
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
            End Using            
        Catch ex As Exception
            Throw New Exception("clsUsersetupDB.GetDataTableByUserID error: " & ex.Message)
            Return Nothing
        End Try
        Return dt
    End Function

    Shared Function getDataTableUserPrivilege(ByVal pObj As clsUserSetup) As DataTable
        Dim sql As String
        Dim dt As New DataTable
        Try            
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()

                sql = " select	AppID, UserID, MenuID, AllowAccess, AllowUpdate, AllowSpecial, CreateDate, CreateUser, UpdateDate, UpdateUser " & vbCrLf & _
                        " from	UserPrivilege " & vbCrLf & _
                        " where	AppID = @AppID and " & vbCrLf & _
                        " 		UserID = @UserID and " & vbCrLf & _
                        " 		MenuID = @MenuID "

                Dim cmd As New SqlCommand()
                cmd.CommandText = sql
                cmd.Connection = con
                cmd.Parameters.AddWithValue("@AppID", pObj.AppID)
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                cmd.Parameters.AddWithValue("@MenuID", pObj.MenuID)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            Throw New Exception("clsUsersetupDB.getDataTableUserPrivilege error: " & ex.Message)
            Return Nothing
        End Try
        Return dt
    End Function

    Public Shared Function GetDataCategory(pParentCode As String, pGroupCode As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Dim pObj As New clsUserSetup

        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()

                sql = "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description " & vbCrLf & _
                      "From Mst_Parameter Where Par_Group = '" & pGroupCode & "' " & vbCrLf
                sql = sql + "And Par_ParentCode = '" & pParentCode & "'" & vbCrLf
                sql = sql + "ORDER BY Code "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillCombo_ReLoad(pCodeDepartment As String, pCodeSection As String, pCodeJobPos As String, pSupplier As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Dim pObj As New clsUserSetup

        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()

                sql = "SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'Department' AND Par_Code = '" & pCodeDepartment & "'" & vbCrLf & _
                    " SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'Section' AND Par_ParentCode = '" & pCodeDepartment & "' AND Par_Code = '" & pCodeSection & "'" & vbCrLf & _
                    " SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'JobPosition' AND Par_Code = '" & pCodeJobPos & "'" & vbCrLf & _
                    " SELECT * FROM dbo.Mst_Supplier WHERE Supplier_Code = '" & pSupplier & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Shared Function GetSectionCode(ByVal pDesc As String) As String
        Dim sql As String
        Dim pObj As New clsUserSetup

        sql = " SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'Section' AND Par_Description = '" & Trim(pDesc) & "'"
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand(sql)
                cmd.Connection = con                
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    With ds.Tables(0)
                        Return Trim(.Rows(0)("Par_Code"))
                    End With
                Else
                    Return ""
                End If
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            Throw New Exception("clsUserSetupDB.GetLastLogin error: " & ex.Message)
            Return Nothing
        End Try        
    End Function
End Class

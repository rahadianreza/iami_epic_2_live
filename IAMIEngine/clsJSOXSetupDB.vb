﻿Imports System.Data.SqlClient

Public Class clsJSOXSetupDB
    Shared Function Update(ByVal pObj As clsJSOXSetup) As Integer
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand("Update JSOXSetup set Enable = @Enable, " &
                                          " ParamValue = @ParamValue where RuleID = @RuleID", con)
                cmd.CommandType = CommandType.Text
                With cmd.Parameters
                    .AddWithValue("@RuleID", pObj.RuleID)
                    .AddWithValue("@Enable", IIf(pObj.Enable, "1", "0"))
                    .AddWithValue("@ParamValue", pObj.ParamValue)
                    .AddWithValue("@UpdateUser", pObj.UpdateUser)
                End With

                Dim i As Integer
                i = cmd.ExecuteNonQuery
                Return i
            End Using
        Catch ex As Exception
            Throw New Exception("clsJSOXSetupDB.Update error: " & ex.Message)
        End Try
    End Function

    Shared Function GetRule(ByVal pObj As clsJSOXSetup) As clsJSOXSetup
        ' get user detail
        Dim sql As String
        sql = " select * from JSOXSetup where RuleID = @RuleID "
        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand(sql)
                cmd.Connection = con
                cmd.Parameters.AddWithValue("@RuleID", pObj.RuleID)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim c_JSOX As New clsJSOXSetup
                    With ds.Tables(0)
                        c_JSOX.RuleID = .Rows(0)("RuleID")
                        c_JSOX.Enable = IIf(.Rows(0)("Enable") = "1", True, False)
                        c_JSOX.Description = .Rows(0)("Description")
                        c_JSOX.ParamValue = .Rows(0)("ParamValue")
                    End With
                    Return c_JSOX
                Else
                    Return Nothing
                End If
            End Using
        Catch ex As Exception
            Throw New Exception("clsJSOXSetupDB.GetRule error: " & ex.Message)
        End Try
    End Function

    Shared Function GetList(ByVal pObj As clsJSOXSetup) As List(Of clsJSOXSetup)
        ' get user detail

        Try
            Using con As New SqlConnection(pObj.ConnectionString)
                con.Open()
                Dim cmd As New SqlCommand("select * from JSOX0Setup order by ruleid", con)
                Dim rd As SqlDataReader
                rd = cmd.ExecuteReader
                Dim c_jsoxs As New List(Of clsJSOXSetup)
                Do While rd.Read
                    Dim c_jsox As New clsJSOXSetup
                    c_jsox.RuleID = rd.Item("RuleID")
                    c_jsox.Enable = IIf(rd.Item("Enable") = "1", True, False)
                    c_jsox.Description = rd.Item("Description")
                    c_jsox.ParamValue = rd.Item("ParamValue")
                    c_jsoxs.Add(c_jsox)
                Loop
                rd.Close()
                Return c_jsoxs
            End Using
        Catch ex As Exception
            Throw New Exception("clsJSOXSetupDB.GetList error: " & ex.Message)
        End Try

    End Function
End Class

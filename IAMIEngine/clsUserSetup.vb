﻿Public Class clsUserSetup
    Inherits clsCon

#Region "Initial"
    Private m_AppID As String
    Public Property AppID As String
        Get
            Return m_AppID
        End Get
        Set(ByVal value As String)
            m_AppID = value
        End Set
    End Property

    Private m_UserID As String
    Public Property UserID As String
        Get
            Return m_UserID
        End Get
        Set(ByVal value As String)
            m_UserID = value
        End Set
    End Property

    Private m_PO As String
    Public Property PIC_PO As String
        Get
            Return m_PO
        End Get
        Set(ByVal value As String)
            m_PO = value
        End Set
    End Property

    Private m_UserName As String
    Public Property UserName As String
        Get
            Return m_UserName
        End Get
        Set(ByVal value As String)
            m_UserName = value
        End Set
    End Property

    Private m_UserType As String
    Public Property UserType() As String
        Get
            Return m_UserType
        End Get
        Set(ByVal value As String)
            m_UserType = value
        End Set
    End Property

    Private m_Password As String
    Public Property Password As String
        Get
            Return m_Password
        End Get
        Set(ByVal value As String)
            m_Password = value
        End Set
    End Property

    Private m_Description As String
    Public Property Description As String
        Get
            Return m_Description
        End Get
        Set(ByVal value As String)
            m_Description = value
        End Set
    End Property

    Private m_Email As String
    Public Property User_Email As String
        Get
            Return m_Email
        End Get
        Set(ByVal value As String)
            m_Email = value
        End Set
    End Property

    Private m_Locked As String
    Public Property Locked As String
        Get
            Return m_Locked
        End Get
        Set(ByVal value As String)
            m_Locked = value
        End Set
    End Property

    Private m_AdminStatus As String
    Public Property AdminStatus As String
        Get
            Return m_AdminStatus
        End Get
        Set(ByVal value As String)
            m_AdminStatus = value
        End Set
    End Property

    Private m_CatererID As String
    Public Property CatererID() As String
        Get
            Return m_CatererID
        End Get
        Set(ByVal value As String)
            m_CatererID = value
        End Set
    End Property

    Private m_SupplierID As String
    Public Property Supplier_Code() As String
        Get
            Return m_SupplierID
        End Get
        Set(ByVal value As String)
            m_SupplierID = value
        End Set
    End Property

    Private m_LastLogin As String
    Public Property LastLogin As String
        Get
            Return m_LastLogin
        End Get
        Set(ByVal value As String)
            m_LastLogin = value
        End Set
    End Property

    Private m_ShiftGroup As String
    Public Property ShiftGroup As String
        Get
            Return m_ShiftGroup
        End Get
        Set(ByVal value As String)
            m_ShiftGroup = value
        End Set
    End Property

    Private m_SecurityQuestion As String
    Public Property SecurityQuestion As String
        Get
            Return m_SecurityQuestion
        End Get
        Set(ByVal value As String)
            m_SecurityQuestion = value
        End Set
    End Property

    Private m_SecurityAnswer As String
    Public Property SecurityAnswer As String
        Get
            Return m_SecurityAnswer
        End Get
        Set(ByVal value As String)
            m_SecurityAnswer = value
        End Set
    End Property

    Private m_FirstLoginFlag As String
    Public Property FirstLoginFlag As String
        Get
            Return m_FirstLoginFlag
        End Get
        Set(ByVal value As String)
            m_FirstLoginFlag = value
        End Set
    End Property

    Private m_UserLogin As String
    Public Property UserLogin As String
        Get
            Return m_UserLogin
        End Get
        Set(ByVal value As String)
            m_UserLogin = value
        End Set
    End Property

    Private m_MenuID As String
    Public Property MenuID() As String
        Get
            Return m_MenuID
        End Get
        Set(ByVal value As String)
            m_MenuID = value
        End Set
    End Property

    Private m_CreateDate As String
    Public Property CreateDate() As String
        Get
            Return m_CreateDate
        End Get
        Set(ByVal value As String)
            m_CreateDate = value
        End Set
    End Property

    Private m_UpdateDate As String
    Public Property UpdateDate() As String
        Get
            Return m_UpdateDate
        End Get
        Set(ByVal value As String)
            m_UpdateDate = value
        End Set
    End Property

    Private m_DepartmentID As String
    Public Property Department_Code() As String
        Get
            Return m_DepartmentID
        End Get
        Set(ByVal value As String)
            m_DepartmentID = value
        End Set
    End Property

    Private m_SectionID As String
    Public Property Section_Code() As String
        Get
            Return m_SectionID
        End Get
        Set(ByVal value As String)
            m_SectionID = value
        End Set
    End Property

    Private m_JobPosition As String
    Public Property Job_Position() As String
        Get
            Return m_JobPosition
        End Get
        Set(ByVal value As String)
            m_JobPosition = value
        End Set
    End Property

    Public Property PasswordHint As String

#End Region
End Class

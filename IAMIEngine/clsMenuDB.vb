﻿Imports System.Data.SqlClient

Public Class clsMenuDB

#Region "Method"
    Public Shared Function getDataTableUserMenu(ByVal pObj As clsMenu, Optional ByRef pErrMsg As String = "") As DataTable
        Dim sql As String
        Dim cmd As SqlCommand = Nothing
        Dim da As SqlDataAdapter = Nothing
        Dim ds As New DataSet
        Dim dt As DataTable = Nothing
        Try
            Using con = New SqlConnection(pObj.ConnectionString)
                con.Open()

                sql = "sp_UserMenu_Sel"
                cmd = New SqlCommand()
                cmd.Connection = con
                cmd.CommandText = sql
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@AppID", pObj.AppID)
                cmd.Parameters.AddWithValue("@MenuID", pObj.MenuID)
                da = New SqlDataAdapter(cmd)
                da.Fill(ds)
                dt = ds.Tables(0)
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            pErrMsg = "PGMEATSEngine.clsMenuDB.getDataTableUserMenu: " & ex.Message
            Return Nothing
        End Try
        Return dt
    End Function
#End Region

End Class

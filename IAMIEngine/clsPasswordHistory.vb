﻿Public Class clsPasswordHistory
    Inherits clsCon

#Region "Initial"
    Private _UserID As String
    Public Property UserID() As String
        Get
            Return _UserID
        End Get
        Set(ByVal value As String)
            _UserID = value
        End Set
    End Property

    Private _SeqNo As Integer
    Public Property SeqNo() As Integer
        Get
            Return _SeqNo
        End Get
        Set(ByVal value As Integer)
            _SeqNo = value
        End Set
    End Property

    Private _UpdateDate As Date
    Public Property UpdateDate() As Date
        Get
            Return _UpdateDate
        End Get
        Set(ByVal value As Date)
            _UpdateDate = value
        End Set
    End Property

    Private _Password As String
    Public Property Password() As String
        Get
            Return _Password
        End Get
        Set(ByVal value As String)
            _Password = value
        End Set
    End Property
#End Region
    
End Class

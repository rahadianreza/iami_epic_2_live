﻿Imports System.Data.SqlClient
Imports System.Xml
Imports System.Configuration

Public Class clsCon
#Region "Initial"
    Private m_Server As String = ""
    Private m_Database As String = ""
    Private m_User As String = ""
    Private m_Password As String = ""
    Private m_IntegratedSecurity As String = "0"
    Private m_CommandTimeout As Integer
    Private m_DatabaseTimeout As Integer
    Private m_ConnectionString As String
    Private m_LinkServer As String
    Private m_MaxRetry As Integer
    Private m_RetryInterval As Integer
    Private m_SchedulerInterval As Integer

    Private m_TServer As String = ""
    Private m_TDatabase As String = ""
    Private m_TUser As String = ""
    Private m_TPassword As String = ""
    Private m_TIntegratedSecurity As String = "0"
    Private m_TCommandTimeout As Integer
    Private m_TDatabaseTimeout As Integer
    Private m_TConnectionString As String
    Private m_TLinkServer As String
    Private m_TMaxRetry As Integer
    Private m_TRetryInterval As Integer

    Private m_TIntervalIdle As Integer
    Private m_TIntervalClear As Integer

    Private m_DayShift As String = ""
    Private m_NightShift As String = ""

    Private m_LocationID As String = ""
    Private m_TerminalID As String = ""
    Private m_TTerminalID As String = ""
    Private m_POSTerminalID As String = ""

    Public Property OraclePort As String '= "1521"
    Public Property SID As String '= "xe"
    Public Property OraclePassword As String '= "Tosis123"
    Public Property OracleHost As String '= "192.168.0.2"
    Public Property OracleUser As String '= "system"

    Public Property PISDatabase As String
    Public Property MatrixLinkedServer As String
    Public Property MatrixDatabase As String

    Public Property MsgInterval As Integer

    Public Enum ENUM_SYSID
        SYS_EATSSYSTEM = 0
        SYS_PEOPLESOFT = 1
        SYS_MATRIX = 2
        SYS_PIS = 3
        SYS_ACCOUNT = 4
    End Enum

    Public Enum ENUM_PROCESS_SYS_EATSYSTEM
        AUTO_TOPUP = 1
        AUTO_DEDUCTION = 2
    End Enum

    Public Enum ENUM_PROCESS_SYS_PEOPLESOFT
        STAFF_MST = 1
        ATTENDANCE = 2
    End Enum

    Public Enum ENUM_PROCESS_SYS_MATRIX
        CARD = 1
    End Enum

    Public Enum ENUM_PROCESS_SYS_PIS
        WORKING_CALENDAR = 1
    End Enum

    Public Enum ENUM_PROCESS_SYS_ACCOUNT
        INVOICE = 1
        GENERAL_LEDGER = 2
    End Enum

    Public Enum CONNECTION_METHOD
        ASPNET = 0
        DESKTOP = 1
    End Enum

    Private m_CONN_METHOD As CONNECTION_METHOD
    Public Property CONN_METHOD() As CONNECTION_METHOD
        Get
            Return m_CONN_METHOD
        End Get
        Set(ByVal value As CONNECTION_METHOD)
            m_CONN_METHOD = value
        End Set
    End Property

    Public Property configServer As String
        Get
            Return m_Server
        End Get
        Set(value As String)
            m_Server = value
        End Set
    End Property

    Public Property configDatabase As String
        Get
            Return m_Database
        End Get
        Set(value As String)
            m_Database = value
        End Set
    End Property

    Public Property configUserID As String
        Get
            Return m_User
        End Get
        Set(value As String)
            m_User = value
        End Set
    End Property

    Public Property configPassword As String
        Get
            Return m_Password
        End Get
        Set(value As String)
            m_Password = value
        End Set
    End Property

    Public Property configIntegratedSecurity As String
        Get
            Return m_IntegratedSecurity
        End Get
        Set(value As String)
            m_IntegratedSecurity = value
        End Set
    End Property

    Public Property configCommandTimeout As Integer
        Get
            Return m_CommandTimeout
        End Get
        Set(value As Integer)
            m_CommandTimeout = value
        End Set
    End Property

    Public Property configDatabaseTimeout As Integer
        Get
            Return m_DatabaseTimeout
        End Get
        Set(value As Integer)
            m_DatabaseTimeout = value
        End Set
    End Property

    Public Property configMaxRetry As Integer
        Get
            Return m_MaxRetry
        End Get
        Set(value As Integer)
            m_MaxRetry = value
        End Set
    End Property

    Public Property configRetryInterval As Integer
        Get
            Return m_RetryInterval
        End Get
        Set(value As Integer)
            m_RetryInterval = value
        End Set
    End Property

    Public Property configSchedulerInterval() As Integer
        Get
            Return m_SchedulerInterval
        End Get
        Set(ByVal value As Integer)
            m_SchedulerInterval = value
        End Set
    End Property

    Public Property PISLinkedServer As String
        Get
            Return m_LinkServer
        End Get
        Set(value As String)
            m_LinkServer = value
        End Set
    End Property

    Public Property configTIntervalIdle() As Integer
        Get
            Return m_TIntervalIdle
        End Get
        Set(ByVal value As Integer)
            m_TIntervalIdle = value
        End Set
    End Property

    Public Property configTIntervalClear() As Integer
        Get
            Return m_TIntervalClear
        End Get
        Set(ByVal value As Integer)
            m_TIntervalClear = value
        End Set
    End Property

    Public Property configLocationID As String
        Get
            Return m_LocationID
        End Get
        Set(value As String)
            m_LocationID = value
        End Set
    End Property

    Public Property configTerminalID As String
        Get
            Return m_TerminalID
        End Get
        Set(value As String)
            m_TerminalID = value
        End Set
    End Property

    Public Property configTransTerminalID As String
        Get
            Return m_TTerminalID
        End Get
        Set(value As String)
            m_TTerminalID = value
        End Set
    End Property

    Public Property configPOSTerminalID As String
        Get
            Return m_POSTerminalID
        End Get
        Set(value As String)
            m_POSTerminalID = value
        End Set
    End Property

    Public Property configTServer As String
        Get
            Return m_TServer
        End Get
        Set(value As String)
            m_TServer = value
        End Set
    End Property

    Public Property configTDatabase As String
        Get
            Return m_TDatabase
        End Get
        Set(value As String)
            m_TDatabase = value
        End Set
    End Property

    Public Property configTUserID As String
        Get
            Return m_TUser
        End Get
        Set(value As String)
            m_TUser = value
        End Set
    End Property

    Public Property configTPassword As String
        Get
            Return m_TPassword
        End Get
        Set(value As String)
            m_TPassword = value
        End Set
    End Property

    Public Property configTIntegratedSecurity As String
        Get
            Return m_TIntegratedSecurity
        End Get
        Set(value As String)
            m_TIntegratedSecurity = value
        End Set
    End Property

    Public Property configTCommandTimeout As Integer
        Get
            Return m_TCommandTimeout
        End Get
        Set(value As Integer)
            m_TCommandTimeout = value
        End Set
    End Property

    Public Property configTDatabaseTimeout As Integer
        Get
            Return m_TDatabaseTimeout
        End Get
        Set(value As Integer)
            m_TDatabaseTimeout = value
        End Set
    End Property

    Public Property configTMaxRetry As Integer
        Get
            Return m_TMaxRetry
        End Get
        Set(value As Integer)
            m_TMaxRetry = value
        End Set
    End Property

    Public Property configTRetryInterval As Integer
        Get
            Return m_TRetryInterval
        End Get
        Set(value As Integer)
            m_TRetryInterval = value
        End Set
    End Property

    Public Property DayShift As String
        Get
            Return m_DayShift
        End Get
        Set(value As String)
            m_DayShift = value
        End Set
    End Property

    Public Property NightShift As String
        Get
            Return m_NightShift
        End Get
        Set(value As String)
            m_NightShift = value
        End Set
    End Property
#End Region

#Region "Method"
    Public Sub New()
        Dim ls_path As String = System.IO.Path.Combine(My.Application.Info.DirectoryPath, "config.xml")
        If Not My.Computer.FileSystem.FileExists(ls_path) Then
            Exit Sub
        End If

        If Trim(IO.File.ReadAllText(ls_path).Length) = 0 Then
            Exit Sub
        End If
        Dim document = XDocument.Load(ls_path)
        Dim NewEnryption As New clsDESEncryption("TOS")

        Dim DBSetting = document.Descendants("DB").FirstOrDefault()
        If Not IsNothing(DBSetting) Then
            If Not IsNothing(DBSetting.Element("ServerName")) Then m_Server = NewEnryption.DecryptData(DBSetting.Element("ServerName").Value)
            If Not IsNothing(DBSetting.Element("Database")) Then m_Database = NewEnryption.DecryptData(DBSetting.Element("Database").Value)
            If Not IsNothing(DBSetting.Element("UserID")) Then m_User = NewEnryption.DecryptData(DBSetting.Element("UserID").Value)
            If Not IsNothing(DBSetting.Element("Password")) Then m_Password = NewEnryption.DecryptData(DBSetting.Element("Password").Value)
            If Not IsNothing(DBSetting.Element("Integrated")) Then m_IntegratedSecurity = DBSetting.Element("Integrated").Value
            If Not IsNothing(DBSetting.Element("CommandTimeOut")) Then m_CommandTimeout = IIf(IsNumeric(NewEnryption.DecryptData(DBSetting.Element("CommandTimeOut").Value)) = True, NewEnryption.DecryptData(DBSetting.Element("CommandTimeOut").Value), 0)
            If Not IsNothing(DBSetting.Element("DatabaseTimeOut")) Then m_DatabaseTimeout = IIf(IsNumeric(NewEnryption.DecryptData(DBSetting.Element("DatabaseTimeOut").Value)) = True, NewEnryption.DecryptData(DBSetting.Element("DatabaseTimeOut").Value), 0)
            If Not IsNothing(DBSetting.Element("LinkServer")) Then m_LinkServer = NewEnryption.DecryptData(DBSetting.Element("LinkServer").Value)
            If Not IsNothing(DBSetting.Element("MaxRetry")) Then m_MaxRetry = NewEnryption.DecryptData(DBSetting.Element("MaxRetry").Value)
            If Not IsNothing(DBSetting.Element("RetryInterval")) Then m_RetryInterval = NewEnryption.DecryptData(DBSetting.Element("RetryInterval").Value)
            If Not IsNothing(DBSetting.Element("SchedulerInterval")) Then m_SchedulerInterval = NewEnryption.DecryptData(DBSetting.Element("SchedulerInterval").Value)

            If Not IsNothing(DBSetting.Element("LocationID")) Then m_LocationID = NewEnryption.DecryptData(DBSetting.Element("LocationID").Value)
            If Not IsNothing(DBSetting.Element("TerminalID")) Then m_TerminalID = NewEnryption.DecryptData(DBSetting.Element("TerminalID").Value)
            If Not IsNothing(DBSetting.Element("POSTerminalID")) Then m_POSTerminalID = NewEnryption.DecryptData(DBSetting.Element("POSTerminalID").Value)

            If Not IsNothing(DBSetting.Element("PISDatabase")) Then PISDatabase = NewEnryption.DecryptData(DBSetting.Element("PISDatabase").Value)
            If Not IsNothing(DBSetting.Element("MatrixLinkServer")) Then MatrixLinkedServer = NewEnryption.DecryptData(DBSetting.Element("MatrixLinkServer").Value)
            If Not IsNothing(DBSetting.Element("MatrixDatabase")) Then MatrixDatabase = NewEnryption.DecryptData(DBSetting.Element("MatrixDatabase").Value)
            If Not IsNothing(DBSetting.Element("MsgInterval")) Then
                MsgInterval = IIf(IsNumeric(NewEnryption.DecryptData(DBSetting.Element("MsgInterval").Value)) = True, NewEnryption.DecryptData(DBSetting.Element("MsgInterval").Value), 0)
            End If


            If Not IsNothing(DBSetting.Element("OracleHost")) Then OracleHost = NewEnryption.DecryptData(DBSetting.Element("OracleHost").Value)
            If Not IsNothing(DBSetting.Element("OraclePort")) Then OraclePort = NewEnryption.DecryptData(DBSetting.Element("OraclePort").Value)
            If Not IsNothing(DBSetting.Element("OracleSID")) Then SID = NewEnryption.DecryptData(DBSetting.Element("OracleSID").Value)
            If Not IsNothing(DBSetting.Element("OracleUser")) Then OracleUser = NewEnryption.DecryptData(DBSetting.Element("OracleUser").Value)
            If Not IsNothing(DBSetting.Element("OraclePassword")) Then OraclePassword = NewEnryption.DecryptData(DBSetting.Element("OraclePassword").Value)


            Dim builder As New SqlConnectionStringBuilder
            builder.DataSource = m_Server
            builder.InitialCatalog = m_Database
            builder.UserID = m_User
            builder.Password = m_Password
            builder.IntegratedSecurity = m_IntegratedSecurity = "1"

            m_ConnectionString = builder.ConnectionString
        End If

        Dim TransDB = document.Descendants("TransDB").FirstOrDefault()
        If Not IsNothing(TransDB) Then
            If Not IsNothing(TransDB.Element("ServerName")) Then m_TServer = NewEnryption.DecryptData(TransDB.Element("ServerName").Value)
            If Not IsNothing(TransDB.Element("Database")) Then m_TDatabase = NewEnryption.DecryptData(TransDB.Element("Database").Value)
            If Not IsNothing(TransDB.Element("UserID")) Then m_TUser = NewEnryption.DecryptData(TransDB.Element("UserID").Value)
            If Not IsNothing(TransDB.Element("Password")) Then m_TPassword = NewEnryption.DecryptData(TransDB.Element("Password").Value)
            If Not IsNothing(TransDB.Element("Integrated")) Then m_TIntegratedSecurity = TransDB.Element("Integrated").Value
            If Not IsNothing(TransDB.Element("CommandTimeOut")) Then m_TCommandTimeout = IIf(IsNumeric(NewEnryption.DecryptData(TransDB.Element("CommandTimeOut").Value)) = True, NewEnryption.DecryptData(TransDB.Element("CommandTimeOut").Value), 0)
            If Not IsNothing(TransDB.Element("DatabaseTimeOut")) Then m_TDatabaseTimeout = IIf(IsNumeric(NewEnryption.DecryptData(TransDB.Element("DatabaseTimeOut").Value)) = True, NewEnryption.DecryptData(TransDB.Element("DatabaseTimeOut").Value), 0)
            If Not IsNothing(TransDB.Element("LinkServer")) Then m_TLinkServer = NewEnryption.DecryptData(TransDB.Element("LinkServer").Value)
            If Not IsNothing(TransDB.Element("MaxRetry")) Then m_TMaxRetry = NewEnryption.DecryptData(TransDB.Element("MaxRetry").Value)
            If Not IsNothing(TransDB.Element("RetryInterval")) Then m_TRetryInterval = NewEnryption.DecryptData(TransDB.Element("RetryInterval").Value)
            If Not IsNothing(TransDB.Element("TerminalID")) Then m_TTerminalID = NewEnryption.DecryptData(TransDB.Element("TerminalID").Value)

            If Not IsNothing(TransDB.Element("IntervalIdle")) Then m_TIntervalIdle = NewEnryption.DecryptData(TransDB.Element("IntervalIdle").Value)
            If Not IsNothing(TransDB.Element("IntervalClear")) Then m_TIntervalClear = NewEnryption.DecryptData(TransDB.Element("IntervalClear").Value)

            If Not IsNothing(TransDB.Element("DayShift")) Then m_DayShift = NewEnryption.DecryptData(TransDB.Element("DayShift").Value)
            If Not IsNothing(TransDB.Element("NightShift")) Then m_NightShift = NewEnryption.DecryptData(TransDB.Element("NightShift").Value)
            Dim builder As New SqlConnectionStringBuilder
            builder.DataSource = m_TServer
            builder.InitialCatalog = m_TDatabase
            builder.UserID = m_TUser
            builder.Password = m_TPassword
            builder.IntegratedSecurity = m_TIntegratedSecurity = "1"

            m_TConnectionString = builder.ConnectionString
        End If
    End Sub

    Public Function ConnectionStringOracle() As String
        Dim ConStr As String = "Driver={Microsoft ODBC for Oracle}; " & _
            "CONNECTSTRING=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & OracleHost & ") " & _
            "(PORT=" & OraclePort & "))" & _
            "(CONNECT_DATA=(SERVICE_NAME=" & SID & "))); " & _
            "Uid=" & OracleUser & ";Pwd=" & OraclePassword & ";"
        Return ConStr
    End Function

    Public Function ConnectionString() As String
        If m_CONN_METHOD = CONNECTION_METHOD.ASPNET Then
            Return ConfigurationManager.ConnectionStrings("ApplicationServices").ConnectionString
        Else
            If m_Server = "" Or m_Database = "" Or m_User = "" Then
                Return ""
            End If
            Dim bd As New SqlConnectionStringBuilder
            bd.DataSource = m_Server
            bd.InitialCatalog = m_Database
            bd.IntegratedSecurity = m_IntegratedSecurity = "1"
            If bd.IntegratedSecurity = False Then
                bd.UserID = m_User
                bd.Password = m_Password
            End If

            Return bd.ConnectionString
        End If
    End Function

    Public Function ConnectionStringLocal() As String
        If m_TServer = "" Or m_TDatabase = "" Or m_TUser = "" Then
            Return ""
        End If
        Dim bd As New SqlConnectionStringBuilder
        bd.DataSource = m_TServer
        bd.InitialCatalog = m_TDatabase
        bd.IntegratedSecurity = m_TIntegratedSecurity = "1"
        If bd.IntegratedSecurity = False Then
            bd.UserID = m_TUser
            bd.Password = m_TPassword
        End If

        Return bd.ConnectionString
    End Function
#End Region

End Class

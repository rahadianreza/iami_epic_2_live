﻿Imports System.Data.SqlClient

Public Class clsMenu
    Inherits clsCon
#Region "Initial"
    Private _AppID As String
    Public Property AppID() As String
        Get
            Return _AppID
        End Get
        Set(ByVal value As String)
            _AppID = value
        End Set
    End Property

    Private _MenuID As String
    Public Property MenuID() As String
        Get
            Return _MenuID
        End Get
        Set(ByVal value As String)
            _MenuID = value
        End Set
    End Property
#End Region

End Class

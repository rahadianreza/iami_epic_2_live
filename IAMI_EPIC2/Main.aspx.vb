﻿Imports IAMIEngine
Imports System
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO


Public Class Main
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "Dashboard"
       

        If Not Page.IsPostBack Then
            Dim UserName As String = Session("user")
            If UserName = "" Then
                Return
            End If

            setdashboard()
            lblTaskList1.Text = GetTaskListCount("PR")
            lblTaskList2.Text = GetTaskListCount("RFQ")
            lblTaskList3.Text = GetTaskListCount("CE")
            lblTaskList4.Text = GetTaskListCount("CPT")
            lblTaskList5.Text = GetTaskListCount("CPN")
            lblTaskList6.Text = GetTaskListCount("IA")
            BindGrid()
            Dim cPasswordHistory As New clsPasswordHistory
            cPasswordHistory.UserID = UserName
            Dim His As clsPasswordHistory = clsPasswordHistoryDB.GetLastData(cPasswordHistory)
            If His IsNot Nothing Then
                Dim cJSOXSetup As New clsJSOXSetup
                cJSOXSetup.RuleID = "3"
                Dim jsox As clsJSOXSetup = clsJSOXSetupDB.GetRule(cJSOXSetup)
                Dim dif As Integer = DateDiff(DateInterval.Day, His.UpdateDate, Date.Now)
                Dim expireDay As Integer = jsox.ParamValue
                If dif > expireDay Then
                    cJSOXSetup.RuleID = "2"
                    jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
                    Dim agingDay As Integer = jsox.ParamValue
                    If dif > expireDay + agingDay Then

                    End If
                End If
            End If
        End If
    End Sub

    Private Function GetTaskListCount(pModule As String) As String
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim strCount As String = "0"

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Task_List_Dashboard", "UserID|Module", Session("user").ToString & "|" & pModule, ErrMsg)

        If ErrMsg = "" Then
            strCount = ds.Tables(0).Rows.Count.ToString
        End If

        Return strCount
    End Function

    Private Sub setdashboard()

        Using con As New SqlConnection(Sconn.Stringkoneksi)
            Using cmd As New SqlCommand("select Case when UserType='2' then 'hidden-div' else 'show-div' end css from Usersetup where UserID='" & Session("user") & "'")
                Using sda As New SqlDataAdapter()
                    cmd.Connection = con
                    sda.SelectCommand = cmd
                    Using dt As New DataTable()
                        sda.Fill(dt)
                        rpMain.DataSource = dt
                        rpMain.DataBind()
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub BindGrid()

        Using con As New SqlConnection(Sconn.Stringkoneksi)
            Using cmd As New SqlCommand(" SELECT PR_Number, Current_Status ,  Progress,  Progress_Percentage from VW_TEST where ((PIC_Assign = '" & Session("user") & "') OR (Register_By = '" & Session("user") & "')) order by PR_Number ")
                Using sda As New SqlDataAdapter()
                    cmd.Connection = con
                    sda.SelectCommand = cmd
                    Using dt As New DataTable()
                        sda.Fill(dt)
                        GridViewReport.DataSource = dt
                        GridViewReport.DataBind()
                    End Using
                End Using
            End Using
        End Using
    End Sub

End Class
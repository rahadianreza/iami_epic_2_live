﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Drawing

'Imports System.Drawing

Public Class RFQCheekSheetApproval
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fRefresh As Boolean = False
    Dim PrjID As String = ""
    Dim Group As String = ""
    Dim commodity As String = ""
    Dim Groupcommodity As String = ""
    Dim datefrom As String = ""
    Dim dateto As String = ""
    Dim projtype As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("J060")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            FillCombo(PrjID, Group, commodity)
            PrjID = Session("PrjIDdetailApproval")
            Group = Session("GroupdetailApproval")
            commodity = Session("commoditydetailApproval")
            Groupcommodity = Session("GroupcommoditydetailApproval")
            If PrjID <> "" Then
                cboProject.Value = PrjID
                cboGroup.Value = Group
                cboCommodity.Value = commodity
                cboCommodityGroup.Value = Groupcommodity
                up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)
                Session("PrjIDdetailApproval") = ""
                Session("GroupdetailApproval") = ""
                Session("commoditydetailApproval") = ""
                Session("GroupcommoditydetailApproval") = ""
            Else
                cboProject.Value = "All"
                cboGroup.Value = "All"
                cboCommodity.Value = "All"
                cboCommodityGroup.Value = "All"
            End If
        End If
    End Sub
#End Region

#Region "Control Event"

    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)
        End If

    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "load"
                up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)

        End Select
    End Sub

    Private Sub up_Excel()
        up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=RfQCheckSheet" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Procedure"

    Private Sub up_GridLoad(ByVal pProjectID As String, ByVal pGroup As String, ByVal pComodity As String, ByVal pGroupComodity As String)
        Dim ErrMsg As String = ""
        'Dim Ses As New List(Of ClsRFQCheekSheet)
        'Ses = ClsRFQCheekSheetDB.getlistApproval(IIf(pProjectID = Nothing, "", pProjectID), IIf(pGroup = Nothing, "", pGroup), IIf(pComodity = Nothing, "", pComodity), IIf(pGroupComodity = Nothing, "", pGroupComodity), pUser, ErrMsg)
        Dim Ses As New DataSet
        Ses = ClsRFQCheekSheetDB.getlistdsApproval(IIf(pProjectID = Nothing, "", pProjectID), IIf(pGroup = Nothing, "", pGroup), IIf(pComodity = Nothing, "", pComodity), IIf(pGroupComodity = Nothing, "", pGroupComodity), pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
            addcolumn()
        End If
    End Sub
    Public Sub addcolumn()
        Dim pno As Integer = 16
        Dim ds As DataSet = ClsRFQCheekSheetDB.getlistApproveHeader()
        Dim pBandColName As String = ds.Tables(0).Rows(0)("Approval_Name").ToString()
        Dim colCheck As Object = Grid.Columns(pBandColName)
        If colCheck IsNot Nothing Then
            Return
        End If

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim bandColumn As GridViewBandColumn = New GridViewBandColumn()
            bandColumn.Caption = ds.Tables(0).Rows(i)("Approval_Name").ToString()
            bandColumn.VisibleIndex = pno
            bandColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

            Dim col As GridViewDataTextColumn = New GridViewDataTextColumn()
            col.Caption = "Approval Name"
            col.FieldName = "AppPerson" & CStr(i + 1)
            col.VisibleIndex = pno + 1
            col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
            bandColumn.Columns.Add(col)

            Dim col2 As GridViewDataTextColumn = New GridViewDataTextColumn()
            col2.Caption = "Approval Date"
            col2.FieldName = "AppDate" & CStr(i + 1)
            col2.VisibleIndex = pno + 2
            col2.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            col2.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
            bandColumn.Columns.Add(col2)
            pno = pno + 3
            Grid.Columns.Add(bandColumn)
        Next
    End Sub
#End Region

   
    Private Sub FillCombo(ByVal proj As String, ByVal group As String, ByVal commodity As String)
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsRFQCheekSheetDB.getProjectIDApproval("", "", "", 1, pUser)
        If pmsg = "" Then
            cboProject.DataSource = ds
            cboProject.DataBind()
        End If

        ds = ClsRFQCheekSheetDB.getProjectIDApproval(proj, "", "", 2, pUser)
        If pmsg = "" Then
            cboGroup.DataSource = ds
            cboGroup.DataBind()
        End If

        ds = ClsRFQCheekSheetDB.getProjectIDApproval(proj, group, "", 3, pUser)
        If pmsg = "" Then
            cboCommodity.DataSource = ds
            cboCommodity.DataBind()
        End If

        ds = ClsRFQCheekSheetDB.getProjectIDApproval(proj, group, commodity, 4, pUser)
        If pmsg = "" Then
            cboCommodityGroup.DataSource = ds
            cboCommodityGroup.DataBind()
        End If
    End Sub
   

    Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsRFQCheekSheetDB.getProjectIDApproval(cboProject.Value, "", "", 2, pUser)
        If pmsg = "" Then
            cboGroup.DataSource = ds
            cboGroup.DataBind()
        End If
    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsRFQCheekSheetDB.getProjectIDApproval(cboProject.Value, cboGroup.Value, "", 3, pUser)
        If pmsg = "" Then
            cboCommodity.DataSource = ds
            cboCommodity.DataBind()
        End If
    End Sub

    Private Sub cboCommodityGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodityGroup.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsRFQCheekSheetDB.getProjectIDApproval(cboProject.Value, cboGroup.Value, cboCommodity.Value, 4, pUser)
        If pmsg = "" Then
            cboCommodityGroup.DataSource = ds
            cboCommodityGroup.DataBind()
        End If
    End Sub
End Class
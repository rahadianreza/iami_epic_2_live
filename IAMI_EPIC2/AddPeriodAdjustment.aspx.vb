﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxImageZoom
Imports DevExpress.Web.Data

Public Class AddPeriodAdjustment
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Procedure"
    Private Sub up_LoadData(pPeriodID As String)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim cPeriodAdjustment As New ClsPeriodAdjustment

        cPeriodAdjustment.PeriodID = pPeriodID
        ds = ClsPeriodAdjustmentDB.GetDataItem(cPeriodAdjustment, pErr)

        If pErr = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                txtPeriodID.Text = Trim(ds.Tables(0).Rows(0)("PeriodID"))
                txtPeriodDescription.Text = Trim(ds.Tables(0).Rows(0)("PeriodDescription") & "")
                cboPeriodType.Text = Trim(ds.Tables(0).Rows(0)("PeriodType") & "")
                dtPeriodFrom.Value = ds.Tables(0).Rows(0)("PeriodFrom")
                dtPeriodTo.Value = ds.Tables(0).Rows(0)("PeriodTo")
                dtPODateFrom.Value = ds.Tables(0).Rows(0)("PODateFrom")
                dtPODateTo.Value = ds.Tables(0).Rows(0)("PODateTo")
            End If
        Else
            txtPeriodID.Text = ""
            txtPeriodDescription.Text = ""
            cboPeriodType.Text = ""
            dtPeriodFrom.Text = ""
            dtPeriodTo.Text = ""
            dtPODateFrom.Text = ""
            dtPODateTo.Text = ""
        End If
    End Sub

    Private Sub Show_Error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbSave.JSProperties("cpMessage") = ErrMsg
    End Sub

    Private Sub Show_Delete(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbDelete.JSProperties("cpMessage") = ErrMsg
    End Sub

    Private Sub Show_Clear(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbClear.JSProperties("cpMessage") = ErrMsg
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|A040", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnDelete.SetEnabled(false);" & vbCrLf & _
                            "btnSubmit.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "btnSubmit", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboPeriodType)
        End If
    End Sub

    Private Sub up_Save(Optional ByRef pErr As String = "")
        Try
            Dim PeriodAdjustment As New ClsPeriodAdjustment
            Dim rtn As Integer
            Dim ds As New DataSet
            Dim MsgErr As String = ""

            If txtPeriodID.Text = "--NEW--" Then
                ds = ClsPeriodAdjustmentDB.GeneratePeriodID(cboPeriodType.SelectedItem.GetValue("Description").ToString())
                If ds.Tables(0).Rows.Count > 0 Then
                    PeriodAdjustment.PeriodID = ds.Tables(0).Rows(0)("PeriodID")
                End If
            Else
                PeriodAdjustment.PeriodID = Request.QueryString("ID")
            End If

            If ClsPeriodAdjustmentDB.isExist(PeriodAdjustment.PeriodID) = True Then
                cbSave.JSProperties("cpMessage") = "Data Is Already Exist!"
                Exit Sub
            End If

            PeriodAdjustment.PeriodType = cboPeriodType.SelectedItem.GetValue("Code").ToString()
            PeriodAdjustment.PeriodDescription = txtPeriodDescription.Text
            PeriodAdjustment.PeriodFrom = dtPeriodFrom.Value
            PeriodAdjustment.PeriodTo = dtPeriodTo.Value
            PeriodAdjustment.PODateFrom = dtPODateFrom.Value
            PeriodAdjustment.PODateTo = dtPODateTo.Value

            If Request.QueryString("ID") & "" = "" Then
                rtn = ClsPeriodAdjustmentDB.Insert(PeriodAdjustment, pUser, MsgErr)
                If MsgErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
                    cbSave.JSProperties("cpItemCode") = PeriodAdjustment.PeriodID
                Else
                    cbSave.JSProperties("cpMessage") = MsgErr
                End If
            Else
                rtn = ClsPeriodAdjustmentDB.Update(PeriodAdjustment, pUser, MsgErr)
                If MsgErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                    cbSave.JSProperties("cpItemCode") = PeriodAdjustment.PeriodID
                Else
                    cbSave.JSProperties("cpMessage") = MsgErr
                End If
            End If
            txtPeriodID.BackColor = Color.FromName("#CCCCCC")
        Catch ex As Exception
            Show_Error(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub
#End Region

#Region "Control Event"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboPeriodType)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A130")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user").ToString.ToUpper()
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A130")

        Dim itemcode As String
        itemcode = Request.QueryString("ID") & ""

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If itemcode <> "" Then
                txtPeriodID.Text = itemcode
                txtPeriodID.BackColor = Color.FromName("#CCCCCC")
                txtPeriodID.ReadOnly = True

                up_LoadData(itemcode)

                btnClear.Text = "Cancel"
                btnDelete.Enabled = True
            Else
                txtPeriodID.Text = "--NEW--"
            End If
        End If
        cbSave.JSProperties("cpMessage") = ""
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/PeriodAdjustmentList.aspx")
    End Sub
    Private Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        Dim ErrMsg As String = ""
        up_Save(ErrMsg)
    End Sub

    Private Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.Click
        If txtPeriodID.Text = "--NEW--" Then
            cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"
        Else
            cbClear.JSProperties("cpMessage") = "Data Cancel Successfully!"
            up_LoadData(txtPeriodID.Text)
        End If
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim PeriodAdjustment As New ClsPeriodAdjustment
            Dim rtn As Integer

            PeriodAdjustment.PeriodID = Request.QueryString("ID")
            rtn = ClsPeriodAdjustmentDB.Delete(PeriodAdjustment, pUser)

            cbDelete.JSProperties("cpMessage") = "Data Deleted Successfully!"
        Catch ex As Exception
            Show_Delete(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub
#End Region
End Class
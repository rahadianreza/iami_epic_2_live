﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO
Imports System.Transactions
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors

Public Class QDCComparisonDetailApproval
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""
    Dim ls_VSRNumber As String
    Dim vSRNumber As String
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"

    Private Sub up_GridHeader(pCPNo As String, pSupplier As String)
        Dim headerCaption As String = ""
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
            Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
            Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
            Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
            Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
            Grid.Columns("Supplier1").Visible = True
            Grid.Columns("Supplier2").Visible = True
            Grid.Columns("Supplier3").Visible = True
            Grid.Columns("Supplier4").Visible = True
            Grid.Columns("Supplier5").Visible = True

            '#Set by condition
            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaptionLoad(pCPNo), "|")(0)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.Columns("Supplier1").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaptionLoad(pCPNo), "|")(1)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.Columns("Supplier2").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaptionLoad(pCPNo), "|")(2)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.Columns("Supplier3").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaptionLoad(pCPNo), "|")(3)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.Columns("Supplier4").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaptionLoad(pCPNo), "|")(4)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("Supplier5").Visible = False
            End Try

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try

    End Sub

    Private Function GetAllSupplierForGridHeaderCaption()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "SELECT Sup = dbo.MergeSupplierSR('" & txtCPNumber.Text & "')", "", "", ErrMsg)

        If ErrMsg = "" Then
            retVal = ds.Tables(0).Rows(0).Item("Sup").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, txtCPNumber.Text)
        End If

        Return retVal
    End Function

    Private Function GetAllSupplierForGridHeaderCaptionLoad(pCPNumber As String)
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "SELECT Sup = dbo.MergeSupplierSR('" & pCPNumber & "')", "", "", ErrMsg)

        If ErrMsg = "" Then
            retVal = ds.Tables(0).Rows(0).Item("Sup").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, txtCPNumber.Text)
        End If

        Return retVal
    End Function

    Private Sub up_GridLoadQCD(CPNumber As String, SRNumber As String, Revision As Integer)
        Dim ds As New DataSet
        Dim ds_Delivery As New DataSet
        Dim ds_Quality As New DataSet
        Dim ErrMsg As String = ""
        Dim vSupplier As String = ""
        Dim headSupplier As String = ""
        Dim vname As String = "Supplier"
        Dim vfield As String = "S"
        Dim a As Integer

        Dim GridCost As String = "COST"
        Dim GridDelivery As String = "DELIVERY"
        Dim GridQuality As String = "QUALITY"

        'Dim ds1 As New DataSet
        ds = GetDataSource(CmdType.StoreProcedure, "sp_SR_DetailQCD_List", "CPNumber|SRNumber|Rev|Variable", CPNumber & "|" & SRNumber & "|" & Revision & "|" & GridCost)
        ds_Delivery = GetDataSource(CmdType.StoreProcedure, "sp_SR_DetailQCD_List", "CPNumber|SRNumber|Rev|Variable", CPNumber & "|" & SRNumber & "|" & Revision & "|" & GridDelivery)
        ds_Quality = GetDataSource(CmdType.StoreProcedure, "sp_SR_DetailQCD_List", "CPNumber|SRNumber|Rev|Variable", CPNumber & "|" & SRNumber & "|" & Revision & "|" & GridQuality)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()

            Grid_Delivery.DataSource = ds_Delivery
            Grid_Delivery.DataBind()

            Grid_Quality.DataSource = ds_Quality
            Grid_Quality.DataBind()

        End If

    End Sub

    Private Sub up_SaveDetail(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs, Optional ByRef pErr As String = "")
        Dim a As Integer
        Dim ls_Code As String
        Dim ls_Information As String
        Dim ls_Weight As String
        Dim ls_Supplier1 As String
        Dim ls_Point1 As String
        Dim ls_WeightPoint1 As String
        Dim ls_Notes1 As String

        Dim ls_Supplier2 As String
        Dim ls_Point2 As String
        Dim ls_WeightPoint2 As String
        Dim ls_Notes2 As String

        Dim ls_Supplier3 As String
        Dim ls_Point3 As String
        Dim ls_WeightPoint3 As String
        Dim ls_Notes3 As String

        Dim ls_Supplier4 As String
        Dim ls_Point4 As String
        Dim ls_WeightPoint4 As String
        Dim ls_Notes4 As String

        Dim ls_Supplier5 As String
        Dim ls_Point5 As String
        Dim ls_WeightPoint5 As String
        Dim ls_Notes5 As String


        Dim ls_SRNumber As String

        Dim ls_GroupType As String

        Dim SRCls As New clsSupplierRecommendation

        a = e.UpdateValues.Count

        ls_SRNumber = txtSRNumber.Text
        Dim i As Integer
        Dim aa As Integer
        For iLoop = 0 To a - 1
            aa = e.UpdateValues(iLoop).NewValues.Count
            If aa = 23 Then
                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                ls_Point3 = e.UpdateValues(iLoop).NewValues("Point3").ToString()
                ls_WeightPoint3 = e.UpdateValues(iLoop).NewValues("WeightPoint3").ToString()
                ls_Notes3 = e.UpdateValues(iLoop).NewValues("Notes3").ToString()
                '15
                ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                ls_Point4 = e.UpdateValues(iLoop).NewValues("Point4").ToString()
                ls_WeightPoint4 = e.UpdateValues(iLoop).NewValues("WeightPoint4").ToString()
                ls_Notes4 = e.UpdateValues(iLoop).NewValues("Notes4").ToString()
                '19
                ls_Supplier5 = e.UpdateValues(iLoop).NewValues("Supplier5").ToString()
                ls_Point5 = e.UpdateValues(iLoop).NewValues("Point5").ToString()
                ls_WeightPoint5 = e.UpdateValues(iLoop).NewValues("WeightPoint5").ToString()
                ls_Notes5 = e.UpdateValues(iLoop).NewValues("Notes5").ToString()

            ElseIf aa = 19 Then
                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                ls_Point3 = e.UpdateValues(iLoop).NewValues("Point3").ToString()
                ls_WeightPoint3 = e.UpdateValues(iLoop).NewValues("WeightPoint3").ToString()
                ls_Notes3 = e.UpdateValues(iLoop).NewValues("Notes3").ToString()
                '15
                ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                ls_Point4 = e.UpdateValues(iLoop).NewValues("Point4").ToString()
                ls_WeightPoint4 = e.UpdateValues(iLoop).NewValues("WeightPoint4").ToString()
                ls_Notes4 = e.UpdateValues(iLoop).NewValues("Notes4").ToString()
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            ElseIf aa = 15 Then

                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                ls_Point3 = e.UpdateValues(iLoop).NewValues("Point3").ToString()
                ls_WeightPoint3 = e.UpdateValues(iLoop).NewValues("WeightPoint3").ToString()
                ls_Notes3 = e.UpdateValues(iLoop).NewValues("Notes3").ToString()
                '15
                ls_Supplier4 = ""
                ls_Point4 = "0"
                ls_WeightPoint4 = "0"
                ls_Notes4 = ""
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            ElseIf aa = 11 Then

                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = ""
                ls_Point3 = "0"
                ls_WeightPoint3 = "0"
                ls_Notes3 = ""
                '15
                ls_Supplier4 = ""
                ls_Point4 = "0"
                ls_WeightPoint4 = "0"
                ls_Notes4 = ""
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            Else

                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = ""
                ls_Point2 = "0"
                ls_WeightPoint2 = "0"
                ls_Notes2 = ""
                '11
                ls_Supplier3 = ""
                ls_Point3 = "0"
                ls_WeightPoint3 = "0"
                ls_Notes3 = ""
                '15
                ls_Supplier4 = ""
                ls_Point4 = "0"
                ls_WeightPoint4 = "0"
                ls_Notes4 = ""
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            End If
            ls_GroupType = "COST"

            SRCls.SRNumber = ls_SRNumber
            SRCls.CPNumber = txtCPNumber.Text
            SRCls.Information_Code = ls_Code
            SRCls.Information = ls_Information

            SRCls.Weight = ls_Weight
            SRCls.Supplier1 = ls_Supplier1
            SRCls.Point1 = ls_Point1
            SRCls.WeightPoint1 = ls_WeightPoint1
            SRCls.Notes1 = ls_Notes1

            SRCls.Supplier2 = ls_Supplier2
            SRCls.Point2 = ls_Point2
            SRCls.WeightPoint2 = ls_WeightPoint2
            SRCls.Notes2 = ls_Notes2

            SRCls.Supplier3 = ls_Supplier3
            SRCls.Point3 = ls_Point3
            SRCls.WeightPoint3 = ls_WeightPoint3
            SRCls.Notes3 = ls_Notes3

            SRCls.Supplier4 = ls_Supplier4
            SRCls.Point4 = ls_Point4
            SRCls.WeightPoint4 = ls_WeightPoint4
            SRCls.Notes4 = ls_Notes4

            SRCls.Supplier5 = ls_Supplier5
            SRCls.Point5 = ls_Point5
            SRCls.WeightPoint5 = ls_WeightPoint5
            SRCls.Notes5 = ls_Notes5
            SRCls.GroupType = ls_GroupType

            i = clsSupplierRecommendationDB.UpdateDetailQCD(SRCls, pUser, pErr)

            If i = 0 Then
                clsSupplierRecommendationDB.InsertDetailQCD(SRCls, pUser, pErr)
            End If
        Next
    End Sub

    Private Sub up_SaveDetail_Delivery(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs, Optional ByRef pErr As String = "")
        Dim a As Integer
        Dim ls_Code As String
        Dim ls_Information As String
        Dim ls_Weight As String
        Dim ls_Supplier1 As String
        Dim ls_Point1 As String
        Dim ls_WeightPoint1 As String
        Dim ls_Notes1 As String

        Dim ls_Supplier2 As String
        Dim ls_Point2 As String
        Dim ls_WeightPoint2 As String
        Dim ls_Notes2 As String

        Dim ls_Supplier3 As String
        Dim ls_Point3 As String
        Dim ls_WeightPoint3 As String
        Dim ls_Notes3 As String

        Dim ls_Supplier4 As String
        Dim ls_Point4 As String
        Dim ls_WeightPoint4 As String
        Dim ls_Notes4 As String

        Dim ls_Supplier5 As String
        Dim ls_Point5 As String
        Dim ls_WeightPoint5 As String
        Dim ls_Notes5 As String


        Dim ls_SRNumber As String

        Dim ls_GroupType As String

        Dim SRCls As New clsSupplierRecommendation

        a = e.UpdateValues.Count

        ls_SRNumber = txtSRNumber.Text
        Dim i As Integer
        Dim aa As Integer
        For iLoop = 0 To a - 1
            aa = e.UpdateValues(iLoop).NewValues.Count
            If aa = 23 Then
                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                ls_Point3 = e.UpdateValues(iLoop).NewValues("Point3").ToString()
                ls_WeightPoint3 = e.UpdateValues(iLoop).NewValues("WeightPoint3").ToString()
                ls_Notes3 = e.UpdateValues(iLoop).NewValues("Notes3").ToString()
                '15
                ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                ls_Point4 = e.UpdateValues(iLoop).NewValues("Point4").ToString()
                ls_WeightPoint4 = e.UpdateValues(iLoop).NewValues("WeightPoint4").ToString()
                ls_Notes4 = e.UpdateValues(iLoop).NewValues("Notes4").ToString()
                '19
                ls_Supplier5 = e.UpdateValues(iLoop).NewValues("Supplier5").ToString()
                ls_Point5 = e.UpdateValues(iLoop).NewValues("Point5").ToString()
                ls_WeightPoint5 = e.UpdateValues(iLoop).NewValues("WeightPoint5").ToString()
                ls_Notes5 = e.UpdateValues(iLoop).NewValues("Notes5").ToString()

            ElseIf aa = 19 Then
                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                ls_Point3 = e.UpdateValues(iLoop).NewValues("Point3").ToString()
                ls_WeightPoint3 = e.UpdateValues(iLoop).NewValues("WeightPoint3").ToString()
                ls_Notes3 = e.UpdateValues(iLoop).NewValues("Notes3").ToString()
                '15
                ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                ls_Point4 = e.UpdateValues(iLoop).NewValues("Point4").ToString()
                ls_WeightPoint4 = e.UpdateValues(iLoop).NewValues("WeightPoint4").ToString()
                ls_Notes4 = e.UpdateValues(iLoop).NewValues("Notes4").ToString()
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            ElseIf aa = 15 Then

                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                ls_Point3 = e.UpdateValues(iLoop).NewValues("Point3").ToString()
                ls_WeightPoint3 = e.UpdateValues(iLoop).NewValues("WeightPoint3").ToString()
                ls_Notes3 = e.UpdateValues(iLoop).NewValues("Notes3").ToString()
                '15
                ls_Supplier4 = ""
                ls_Point4 = "0"
                ls_WeightPoint4 = "0"
                ls_Notes4 = ""
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            ElseIf aa = 11 Then

                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = ""
                ls_Point3 = "0"
                ls_WeightPoint3 = "0"
                ls_Notes3 = ""
                '15
                ls_Supplier4 = ""
                ls_Point4 = "0"
                ls_WeightPoint4 = "0"
                ls_Notes4 = ""
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            Else

                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = ""
                ls_Point2 = "0"
                ls_WeightPoint2 = "0"
                ls_Notes2 = ""
                '11
                ls_Supplier3 = ""
                ls_Point3 = "0"
                ls_WeightPoint3 = "0"
                ls_Notes3 = ""
                '15
                ls_Supplier4 = ""
                ls_Point4 = "0"
                ls_WeightPoint4 = "0"
                ls_Notes4 = ""
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            End If
            ls_GroupType = "DELIVERY"

            SRCls.SRNumber = ls_SRNumber
            SRCls.CPNumber = txtCPNumber.Text
            SRCls.Information_Code = ls_Code
            SRCls.Information = ls_Information

            SRCls.Weight = ls_Weight
            SRCls.Supplier1 = ls_Supplier1
            SRCls.Point1 = ls_Point1
            SRCls.WeightPoint1 = ls_WeightPoint1
            SRCls.Notes1 = ls_Notes1

            SRCls.Supplier2 = ls_Supplier2
            SRCls.Point2 = ls_Point2
            SRCls.WeightPoint2 = ls_WeightPoint2
            SRCls.Notes2 = ls_Notes2

            SRCls.Supplier3 = ls_Supplier3
            SRCls.Point3 = ls_Point3
            SRCls.WeightPoint3 = ls_WeightPoint3
            SRCls.Notes3 = ls_Notes3

            SRCls.Supplier4 = ls_Supplier4
            SRCls.Point4 = ls_Point4
            SRCls.WeightPoint4 = ls_WeightPoint4
            SRCls.Notes4 = ls_Notes4

            SRCls.Supplier5 = ls_Supplier5
            SRCls.Point5 = ls_Point5
            SRCls.WeightPoint5 = ls_WeightPoint5
            SRCls.Notes5 = ls_Notes5
            SRCls.GroupType = ls_GroupType

            i = clsSupplierRecommendationDB.UpdateDetailQCD(SRCls, pUser, pErr)

            If i = 0 Then
                clsSupplierRecommendationDB.InsertDetailQCD(SRCls, pUser, pErr)
            End If
        Next
    End Sub

    Private Sub up_SaveDetail_Quality(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs, Optional ByRef pErr As String = "")
        Dim a As Integer
        Dim ls_Code As String
        Dim ls_Information As String
        Dim ls_Weight As String
        Dim ls_Supplier1 As String
        Dim ls_Point1 As String
        Dim ls_WeightPoint1 As String
        Dim ls_Notes1 As String

        Dim ls_Supplier2 As String
        Dim ls_Point2 As String
        Dim ls_WeightPoint2 As String
        Dim ls_Notes2 As String

        Dim ls_Supplier3 As String
        Dim ls_Point3 As String
        Dim ls_WeightPoint3 As String
        Dim ls_Notes3 As String

        Dim ls_Supplier4 As String
        Dim ls_Point4 As String
        Dim ls_WeightPoint4 As String
        Dim ls_Notes4 As String

        Dim ls_Supplier5 As String
        Dim ls_Point5 As String
        Dim ls_WeightPoint5 As String
        Dim ls_Notes5 As String


        Dim ls_SRNumber As String

        Dim ls_GroupType As String

        Dim SRCls As New clsSupplierRecommendation

        a = e.UpdateValues.Count

        ls_SRNumber = txtSRNumber.Text
        Dim i As Integer
        Dim aa As Integer
        For iLoop = 0 To a - 1
            aa = e.UpdateValues(iLoop).NewValues.Count
            If aa = 23 Then
                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                ls_Point3 = e.UpdateValues(iLoop).NewValues("Point3").ToString()
                ls_WeightPoint3 = e.UpdateValues(iLoop).NewValues("WeightPoint3").ToString()
                ls_Notes3 = e.UpdateValues(iLoop).NewValues("Notes3").ToString()
                '15
                ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                ls_Point4 = e.UpdateValues(iLoop).NewValues("Point4").ToString()
                ls_WeightPoint4 = e.UpdateValues(iLoop).NewValues("WeightPoint4").ToString()
                ls_Notes4 = e.UpdateValues(iLoop).NewValues("Notes4").ToString()
                '19
                ls_Supplier5 = e.UpdateValues(iLoop).NewValues("Supplier5").ToString()
                ls_Point5 = e.UpdateValues(iLoop).NewValues("Point5").ToString()
                ls_WeightPoint5 = e.UpdateValues(iLoop).NewValues("WeightPoint5").ToString()
                ls_Notes5 = e.UpdateValues(iLoop).NewValues("Notes5").ToString()

            ElseIf aa = 19 Then
                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                ls_Point3 = e.UpdateValues(iLoop).NewValues("Point3").ToString()
                ls_WeightPoint3 = e.UpdateValues(iLoop).NewValues("WeightPoint3").ToString()
                ls_Notes3 = e.UpdateValues(iLoop).NewValues("Notes3").ToString()
                '15
                ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                ls_Point4 = e.UpdateValues(iLoop).NewValues("Point4").ToString()
                ls_WeightPoint4 = e.UpdateValues(iLoop).NewValues("WeightPoint4").ToString()
                ls_Notes4 = e.UpdateValues(iLoop).NewValues("Notes4").ToString()
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            ElseIf aa = 15 Then

                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                ls_Point3 = e.UpdateValues(iLoop).NewValues("Point3").ToString()
                ls_WeightPoint3 = e.UpdateValues(iLoop).NewValues("WeightPoint3").ToString()
                ls_Notes3 = e.UpdateValues(iLoop).NewValues("Notes3").ToString()
                '15
                ls_Supplier4 = ""
                ls_Point4 = "0"
                ls_WeightPoint4 = "0"
                ls_Notes4 = ""
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            ElseIf aa = 11 Then

                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                ls_Point2 = e.UpdateValues(iLoop).NewValues("Point2").ToString()
                ls_WeightPoint2 = e.UpdateValues(iLoop).NewValues("WeightPoint2").ToString()
                ls_Notes2 = e.UpdateValues(iLoop).NewValues("Notes2").ToString()
                '11
                ls_Supplier3 = ""
                ls_Point3 = "0"
                ls_WeightPoint3 = "0"
                ls_Notes3 = ""
                '15
                ls_Supplier4 = ""
                ls_Point4 = "0"
                ls_WeightPoint4 = "0"
                ls_Notes4 = ""
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            Else

                ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                ls_Weight = e.UpdateValues(iLoop).NewValues("Weight").ToString()
                ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                ls_Point1 = e.UpdateValues(iLoop).NewValues("Point1").ToString()
                ls_WeightPoint1 = e.UpdateValues(iLoop).NewValues("WeightPoint1").ToString()
                ls_Notes1 = e.UpdateValues(iLoop).NewValues("Notes1").ToString()
                '7
                ls_Supplier2 = ""
                ls_Point2 = "0"
                ls_WeightPoint2 = "0"
                ls_Notes2 = ""
                '11
                ls_Supplier3 = ""
                ls_Point3 = "0"
                ls_WeightPoint3 = "0"
                ls_Notes3 = ""
                '15
                ls_Supplier4 = ""
                ls_Point4 = "0"
                ls_WeightPoint4 = "0"
                ls_Notes4 = ""
                '19
                ls_Supplier5 = ""
                ls_Point5 = "0"
                ls_WeightPoint5 = "0"
                ls_Notes5 = ""

            End If
            ls_GroupType = "QUALITY"

            SRCls.SRNumber = ls_SRNumber
            SRCls.CPNumber = txtCPNumber.Text
            SRCls.Information_Code = ls_Code
            SRCls.Information = ls_Information

            SRCls.Weight = ls_Weight
            SRCls.Supplier1 = ls_Supplier1
            SRCls.Point1 = ls_Point1
            SRCls.WeightPoint1 = ls_WeightPoint1
            SRCls.Notes1 = ls_Notes1

            SRCls.Supplier2 = ls_Supplier2
            SRCls.Point2 = ls_Point2
            SRCls.WeightPoint2 = ls_WeightPoint2
            SRCls.Notes2 = ls_Notes2

            SRCls.Supplier3 = ls_Supplier3
            SRCls.Point3 = ls_Point3
            SRCls.WeightPoint3 = ls_WeightPoint3
            SRCls.Notes3 = ls_Notes3

            SRCls.Supplier4 = ls_Supplier4
            SRCls.Point4 = ls_Point4
            SRCls.WeightPoint4 = ls_WeightPoint4
            SRCls.Notes4 = ls_Notes4

            SRCls.Supplier5 = ls_Supplier5
            SRCls.Point5 = ls_Point5
            SRCls.WeightPoint5 = ls_WeightPoint5
            SRCls.Notes5 = ls_Notes5
            SRCls.GroupType = ls_GroupType

            i = clsSupplierRecommendationDB.UpdateDetailQCD(SRCls, pUser, pErr)

            If i = 0 Then
                clsSupplierRecommendationDB.InsertDetailQCD(SRCls, pUser, pErr)
            End If
        Next
    End Sub


#End Region

#Region "EVENTS"

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "QDC Comparison"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E050")
        Dim Data As String = ""

        gs_Message = ""

        Dim pSRNumber As String
        Dim pRev As Integer
        Dim pCPNumber As String
        Dim pSupplierRecommend As String
        Dim pConsideration As String
        Dim pNotes As String
        Dim pStatus As String

        pSRNumber = Session("SRNumber")
        pRev = Session("Revision")
        pCPNumber = Session("CPNumber")
        pSupplierRecommend = Session("SupplierRecommend")
        pConsideration = Session("Consideration")
        pNotes = Session("Notes")
        pStatus = Session("Status")

        If Not Page.IsPostBack Then


            txtSRNumber.Text = pSRNumber
            txtCPNumber.Text = pCPNumber
            cbSuppNo.Text = pSupplierRecommend
            memoConsi.Text = pConsideration
            memoNote.Text = pNotes
            cbDraf.JSProperties("cpStatus") = pStatus
            up_GridLoadQCD(pCPNumber, pSRNumber, pRev)

        End If
        cbDraf.JSProperties("cpMessage") = ""
    End Sub

    Private Sub cbGrid_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbGrid.Callback
        Dim pfunction As String = Split(e.Parameter, "|")(0)
        Dim pCPNo As String = Split(e.Parameter, "|")(3)
        Dim pRev As Integer

        If Split(e.Parameter, "|")(2) = "" Then pRev = 0

        up_GridLoadQCD(txtCPNumber.Text, txtSRNumber.Text, 0)

    End Sub

    Private Sub cbDraf_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDraf.Callback
        'up_UploadFile()

        If gs_Message = "" Then

            cbDraf.JSProperties("cpMessage") = "Draft data saved successfull"
            If gs_SRNumber = "" Then
                cbDraf.JSProperties("cpSRNo") = ls_VSRNumber
            Else
                cbDraf.JSProperties("cpSRNo") = gs_SRNumber
            End If

            If IsNothing(gs_SRRevNo) Then
                cbDraf.JSProperties("cpRevision") = 0
            Else
                cbDraf.JSProperties("cpRevision") = gs_SRRevNo
            End If


        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Dim pSRNumber As String
        Dim pRev As Integer
        Dim pCPNumber As String

        pSRNumber = Session("SRNumber")
        pRev = Session("Revision")
        pCPNumber = Session("CPNumber")
        Response.Redirect("~/SupplierRecomendApprovalDetail.aspx?ID=" & pSRNumber & "|" & pRev & "|" & pCPNumber)
    End Sub

    '----------------------------------------- Event Untuk Tab Grid "COST" -------------------------------------------------

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback

        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Dim pCPNo As String
        If Split(e.Parameters, "|")(1) <> "" Then
            pCPNo = Split(e.Parameters, "|")(3)
        End If

        Dim pRev As Integer

        'If txtRev.Text = "" Then pRev = 0
        If Split(e.Parameters, "|")(1) = "" Then
            pRev = 0
        Else
            pRev = Split(e.Parameters, "|")(2)
        End If


        If pfunction = "gridload" Then

            Dim headerCaption As String = ""
            Grid.JSProperties("cpType") = ""
            Grid.JSProperties("cpMessage") = ""




        ElseIf pfunction = "draft" Then

            up_GridLoadQCD(txtCPNumber.Text, txtSRNumber.Text, 0)

        ElseIf pfunction = "view" Then
            Try
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("Supplier1").Visible = True
                Grid.Columns("Supplier2").Visible = True
                Grid.Columns("Supplier3").Visible = True
                Grid.Columns("Supplier4").Visible = True
                Grid.Columns("Supplier5").Visible = True

                Dim headerCaption As String = ""
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid.Columns("Supplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid.Columns("Supplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid.Columns("Supplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid.Columns("Supplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid.Columns("Supplier5").Visible = False
                End Try





            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
            End Try
        End If


    End Sub

    Protected Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        'Grid.CancelEdit()
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        If e.UpdateValues.Count = 0 Then
            gs_Message = "Data is not found"
            Exit Sub
        End If
        Dim errmsg As String = ""
        'GAK PERLU PAKAI HEADER IKUT HEADER SR_HEADER

        'INSERT DETAIL
        If errmsg = "" Then
            up_SaveDetail(sender, e, errmsg)
        End If

        If errmsg = "" Then
            gs_Message = ""
        Else
            gs_Message = errmsg
        End If
        Grid.EndUpdate()
    End Sub

    '----------------------------------------- ----------------------------- -----------------------------------------------

    '----------------------------------------- Event Untuk Tab Grid "DELIVERY" ---------------------------------------------

    Private Sub Grid_Delivery_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid_Delivery.CustomCallback

        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Dim pCPNo As String
        If Split(e.Parameters, "|")(1) <> "" Then
            pCPNo = Split(e.Parameters, "|")(3)
        End If

        Dim pRev As Integer

        'If txtRev.Text = "" Then pRev = 0
        If Split(e.Parameters, "|")(1) = "" Then
            pRev = 0
        Else
            pRev = Split(e.Parameters, "|")(2)
        End If


        If pfunction = "gridload" Then

            Dim headerCaption As String = ""
            Grid_Delivery.JSProperties("cpType") = ""
            Grid_Delivery.JSProperties("cpMessage") = ""



        ElseIf pfunction = "draft" Then
            '    'If gs_CENumber <> "" And gs_CENumber <> "ALL" Then
            up_GridLoadQCD(txtCPNumber.Text, txtSRNumber.Text, 0)
            'End If

        ElseIf pfunction = "view" Then
            Try
                Grid_Delivery.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid_Delivery.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid_Delivery.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid_Delivery.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid_Delivery.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid_Delivery.Columns("Supplier1").Visible = True
                Grid_Delivery.Columns("Supplier2").Visible = True
                Grid_Delivery.Columns("Supplier3").Visible = True
                Grid_Delivery.Columns("Supplier4").Visible = True
                Grid_Delivery.Columns("Supplier5").Visible = True

                Dim headerCaption As String = ""
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption1") = headerCaption : Grid_Delivery.Columns("Supplier1").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid_Delivery.Columns("Supplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption2") = headerCaption : Grid_Delivery.Columns("Supplier2").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid_Delivery.Columns("Supplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption3") = headerCaption : Grid_Delivery.Columns("Supplier3").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid_Delivery.Columns("Supplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption4") = headerCaption : Grid_Delivery.Columns("Supplier4").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid_Delivery.Columns("Supplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption5") = headerCaption : Grid_Delivery.Columns("Supplier5").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid_Delivery.Columns("Supplier5").Visible = False
                End Try





            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid_Delivery)
            End Try
        End If


    End Sub

    Protected Sub Grid_Delivery_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid_Delivery.RowUpdating
        e.Cancel = True
        'Grid.CancelEdit()
    End Sub

    Private Sub Grid_Delivery_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid_Delivery.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_Delivery_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid_Delivery.BatchUpdate
        If e.UpdateValues.Count = 0 Then
            gs_Message = "Data is not found"
            Exit Sub
        End If
        Dim errmsg As String = ""

        If errmsg = "" Then
            up_SaveDetail_Delivery(sender, e, errmsg)
        End If

        If errmsg = "" Then
            gs_Message = ""
        Else
            gs_Message = errmsg
        End If
        Grid_Delivery.EndUpdate()
    End Sub

    '----------------------------------------- ----------------------------- -----------------------------------------------

    '----------------------------------------- Event Untuk Tab Grid "QUALITY" ----------------------------------------------

    Private Sub Grid_Quality_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid_Quality.CustomCallback

        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Dim pCPNo As String
        If Split(e.Parameters, "|")(1) <> "" Then
            pCPNo = Split(e.Parameters, "|")(3)
        End If

        Dim pRev As Integer

        'If txtRev.Text = "" Then pRev = 0
        If Split(e.Parameters, "|")(1) = "" Then
            pRev = 0
        Else
            pRev = Split(e.Parameters, "|")(2)
        End If


        If pfunction = "gridload" Then

            Dim headerCaption As String = ""
            Grid_Quality.JSProperties("cpType") = ""
            Grid_Quality.JSProperties("cpMessage") = ""



        ElseIf pfunction = "draft" Then
            '    'If gs_CENumber <> "" And gs_CENumber <> "ALL" Then
            up_GridLoadQCD(txtCPNumber.Text, txtSRNumber.Text, 0)
            'End If

        ElseIf pfunction = "view" Then
            Try
                Grid_Quality.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid_Quality.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid_Quality.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid_Quality.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid_Quality.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid_Quality.Columns("Supplier1").Visible = True
                Grid_Quality.Columns("Supplier2").Visible = True
                Grid_Quality.Columns("Supplier3").Visible = True
                Grid_Quality.Columns("Supplier4").Visible = True
                Grid_Quality.Columns("Supplier5").Visible = True

                Dim headerCaption As String = ""
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption1") = headerCaption : Grid_Quality.Columns("Supplier1").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid_Quality.Columns("Supplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption2") = headerCaption : Grid_Quality.Columns("Supplier2").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid_Quality.Columns("Supplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption3") = headerCaption : Grid_Quality.Columns("Supplier3").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid_Quality.Columns("Supplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption4") = headerCaption : Grid_Quality.Columns("Supplier4").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid_Quality.Columns("Supplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption5") = headerCaption : Grid_Quality.Columns("Supplier5").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid_Quality.Columns("Supplier5").Visible = False
                End Try


            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid_Quality)
            End Try
        End If


    End Sub

    Protected Sub Grid_Quality_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid_Quality.RowUpdating
        e.Cancel = True
        'Grid.CancelEdit()
    End Sub

    Private Sub Grid_Quality_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid_Quality.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_Quality_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid_Quality.BatchUpdate
        If e.UpdateValues.Count = 0 Then
            gs_Message = "Data is not found"
            Exit Sub
        End If
        Dim errmsg As String = ""


        If errmsg = "" Then
            up_SaveDetail_Quality(sender, e, errmsg)
        End If

        If errmsg = "" Then
            gs_Message = ""
        Else
            gs_Message = errmsg
        End If
        Grid_Delivery.EndUpdate()
    End Sub

    '----------------------------------------- ----------------------------- -----------------------------------------------


    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
            'Non-Editable
            e.Cell.BackColor = Color.LemonChiffon
            e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")

    End Sub

    Private Sub Grid_Delivery_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid_Delivery.HtmlDataCellPrepared
        'Non-Editable
        e.Cell.BackColor = Color.LemonChiffon
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")

    End Sub

    Private Sub Grid_Quality_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid_Quality.HtmlDataCellPrepared
        'Non-Editable
        e.Cell.BackColor = Color.LemonChiffon
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")

    End Sub

#End Region

End Class
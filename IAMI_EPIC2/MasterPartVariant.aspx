﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="MasterPartVariant.aspx.vb" Inherits="IAMI_EPIC2.MasterPartVariant" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function Message(paramType, paramMessage) {

            if (paramType == 0) { //Info
                toastr.info(paramMessage, 'Info');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 1) { //Success
                toastr.success(paramMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 2) { //Warning
                toastr.warning(paramMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 3) { //Error
                toastr.error(paramMessage, 'Error');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function cboVariantCode_Init(s, e) {
            CboVariantCode.SetValue('ALL');
            txtVariantName.SetText('ALL');
            txtVariantName.SetEnabled(false);
        }

        function cboVariantCode_SelectedIndexChanged(s, e) {
            var desc = s.GetSelectedItem().GetColumnText('VariantName');
            txtVariantName.SetText(desc);
        }

        function cboVariantCode_LostFocus(s, e) {
            var id = s.GetValue();
            if (id == null) {
                txtVariantName.SetText('');
            }
        }

        function cboPartNo_Init(s, e) {
            cboPartNo.SetValue('ALL');
            txtPartNo.SetText('ALL');
            txtPartNo.SetEnabled(false);
        }

        function cboPartNo_SelectedIndexChanged(s, e) {
            var desc = s.GetSelectedItem().GetColumnText('PartName');
            txtPartNo.SetText(desc);
        }

        function cboPartNo_LostFocus(s, e) {
            var id = s.GetValue();
            if (id == null) {
                txtPartNo.SetText('');
            }
        }

        function cboType_Init(s, e) {
            CboType.SetValue('ALL');
        }        

        function btnRefresh_Click(s, e) {

            if (CboVariantCode.GetSelectedIndex() < 0) {
                Message(2, 'Please select Variant Code!');
                CboVariantCode.Focus();
                e.processOnServer = false;
                return;
            } else if (cboPartNo.GetSelectedIndex() < 0) {
                Message(2, 'Please select Part No!');
                cboPartNo.Focus();
                e.processOnServer = false;
                return;
            } else if (CboType.GetSelectedIndex() < 0) {
                Message(2, 'Please select Type!');
                CboType.Focus();
                e.processOnServer = false;
                return;
            }            
            Grid.PerformCallback('load|' + CboVariantCode.GetSelectedItem().GetColumnText(0) + '|' + cboPartNo.GetSelectedItem().GetColumnText(0) + '|' + CboType.GetSelectedItem().GetColumnText(0));
        }

        function Grid_EndCallback(s, e) {
        debugger
            if (s.cp_disabled == "N") {
                //alert(s.cp_disabled);
                btnDownload.SetEnabled(true);
            } else if (s.cp_disabled == "Y") {
                //alert(s.cp_disabled);
                btnDownload.SetEnabled(false);

                toastr.info("There's no data to show!", 'Info');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }


            if (s.cp_message != '') {
                Message(s.cp_type, s.cp_message);
                s.cp_message = '';
            }
        }

        function cbAction_CallbackComplete(s, e) {
            var result = e.result;
            if (result == 'delete') {
                Message(s.cp_type, s.cp_message);
                if (s.cp_type == 1) {
                    var ExchangeDateFrom = formatDateISO(dtDateFrom.GetDate());
                    var ExchangeDateTo = AddDay(AddMonth(dtDateTo.GetDate(), 1), -1);

                    Grid.PerformCallback("load|" + ExchangeDateFrom + "|" + ExchangeDateTo + "|" + cboSupplierCode.GetSelectedItem().GetColumnText(0) + "|" + cboCurrencyCode.GetSelectedItem().GetColumnText(0));
                }
            }
        }
	    
</script>

<style type="text/css">        
    .table-col-title01
    {
        width: 160px;                     
    }
    .table-col-control01
    {
        width: 110px;        
    }   
    .table-col-control02
    {
        width: 250px; 
    }   
    .table-col-title02
    {
        width: 130px;
    }   
    .table-col-control03
    {
        width: 180px;
    }   
    .table-height
    {
        height: 35px      
    }    
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

<div style="padding: 5px 5px 5px 5px">    
    <table style="width: 100%;border: 1px solid black">  
        <tr style="height:15px">
            <td class="table-col-title01">&nbsp;</td>       <%-- Title 1 --%>
            <td class="table-col-control01">&nbsp;</td>     <%-- Control 1 --%>
            <td class="table-col-control02">&nbsp;</td>     <%-- Control 2 --%>
            <td class="table-col-title02">&nbsp;</td>       <%-- Title 2 --%>
            <td class="table-col-control03">
                <dx:ASPxCallback ID="cbAction" runat="server" ClientInstanceName="cbAction">
                    <ClientSideEvents CallbackComplete="cbAction_CallbackComplete" />
                </dx:ASPxCallback>
            </td>     <%-- Control 1 --%>
            <td><dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid" /></td>
        </tr>                 
        <tr class="table-height">
            <td class="table-col-title01">            
                <dx:ASPxLabel ID="lblVariant" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Variant Code" style="padding-left:10px" />
            </td>
            <td class="table-col-control01">      
                <dx:ASPxComboBox ID="CboVariantCode" runat="server" ClientInstanceName="CboVariantCode"
                        Width="100px" Font-Names="Segoe UI" TextField="Description" 
                        ValueField="VariantCode" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" DataSourceID="ds_Variant"
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">
                           
                        <Columns>
                            <dx:ListBoxColumn Caption="VariantCode" FieldName="VariantCode" Width="100px" />
                            <dx:ListBoxColumn Caption="VariantName" FieldName="VariantName" Width="250px" />                             
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>

                        <ClientSideEvents Init="cboVariantCode_Init" SelectedIndexChanged="cboVariantCode_SelectedIndexChanged" LostFocus="cboVariantCode_LostFocus" />
                </dx:ASPxComboBox>                     
            </td>
            <td class="table-col-control02">
                <dx:ASPxTextBox runat="server" ID="txtVariantName" 
                    ClientInstanceName="txtVariantName" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="230px" style="height:25px" />
            </td>            
            <td class="table-col-title02" style="width:8%">
                <dx:ASPxLabel ID="lblType" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Type" style="padding-left:10px" />                    
            </td>
            <td class="table-col-control03">           
                <dx:ASPxComboBox ID="CboType" runat="server" ClientInstanceName="CboType"
                        Width="100px" Font-Names="Segoe UI" TextField="Description" 
                        ValueField="Type" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" DataSourceID="ds_Variant"
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">
                           
                        <Columns>
                            <dx:ListBoxColumn Caption="Type" FieldName="Type" Width="100px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>

                        <ClientSideEvents Init="cboType_Init"/>
                </dx:ASPxComboBox>           
            </td>
            <td>
                <asp:SqlDataSource ID="ds_Variant" runat="server"
                    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"                         
                    SelectCommand="SELECT 'ALL' As Type, 'ALL' As VariantCode, 'ALL' As VariantName UNION ALL SELECT DISTINCT Type AS Type, VariantCode AS VariantCode, VariantName AS VariantName FROM dbo.Mst_PartVariant">
                </asp:SqlDataSource>
            </td>                
        </tr>          
        <tr class="table-height">
            <td class="table-col-title01">            
                <dx:ASPxLabel ID="lblPartNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Part No" style="padding-left:10px" />
            </td>
            <td class="table-col-control01">
                <dx:ASPxComboBox ID="cboPartNo" runat="server" ClientInstanceName="cboPartNo"
                        Width="100px" Font-Names="Segoe UI" TextField="Description" 
                        ValueField="PartNo" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" DataSourceID="ds_PartNo"
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">                           
                        <Columns>
                            <dx:ListBoxColumn Caption="PartNo" FieldName="PartNo" Width="100px" />
                            <dx:ListBoxColumn Caption="PartName" FieldName="PartName" Width="240px" />                             
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>

                        <ClientSideEvents Init="cboPartNo_Init" SelectedIndexChanged="cboPartNo_SelectedIndexChanged" LostFocus="cboPartNo_LostFocus" />
                </dx:ASPxComboBox>
            </td>
            <td class="table-col-control02">
                <dx:ASPxTextBox runat="server" ID="txtPartNo" 
                    ClientInstanceName="txtPartNo" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="230px" style="height:25px" />
            </td>
            <td class="table-col-control02">&nbsp;</td>                         
            <td class="table-col-control02">&nbsp;</td>  
            <td class="table-height">
                <asp:SqlDataSource ID="ds_PartNo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"                         
                    SelectCommand="Select 'ALL' As PartNo, 'ALL' PartName UNION ALL SELECT Part_No AS PartNo, Part_Name AS PartName FROM Mst_PartItem">
                </asp:SqlDataSource>
            </td>                
        </tr>               
        
        <tr style="height:60px">
            <td style=" padding:0px 0px 0px 10px" colspan="6">            
                <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnRefresh" Theme="Default">                        
                    <ClientSideEvents Click="btnRefresh_Click" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>&nbsp;
                <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnDownload" Theme="Default">                                         
                    <Paddings Padding="2px" />
                </dx:ASPxButton>&nbsp;
                <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="false"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                    ClientInstanceName="btnAdd" Theme="Default" >                        
                    <Paddings Padding="2px" />
                </dx:ASPxButton>&nbsp;
            </td>
        </tr>                 
    </table>
</div>
<div style="padding: 5px 5px 5px 5px">
    <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid" Width="100%"
        EnableTheming="True" KeyFieldName="Type;VariantCode;PartNo" Theme="Office2010Black" Font-Size="9pt" Font-Names="Segoe UI" 
        OnAfterPerformCallback="Grid_AfterPerformCallback" OnRowValidating="Grid_RowValidating" OnStartRowEditing="Grid_StartRowEditing" OnRowDeleting="Grid_RowDeleting" >
        <ClientSideEvents EndCallback="Grid_EndCallback" />
        <Columns>
            <dx:GridViewCommandColumn
                VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" Caption=" "
                Width="100px">
            </dx:GridViewCommandColumn>

            <dx:GridViewDataTextColumn FieldName="Type" Caption="Type"
                VisibleIndex="1" Width="100px" Settings-AutoFilterCondition="Contains"  Visible="True">
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <PropertiesTextEdit MaxLength="15"/>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn FieldName="VariantCode" Caption="Variant Code"
                VisibleIndex="2" Width="100px" Settings-AutoFilterCondition="Contains"  Visible="True">
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <PropertiesTextEdit MaxLength="25"/>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="VariantName" Caption="Variant Name"
                VisibleIndex="3" Width="300px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <Settings AutoFilterCondition="Contains" />
                <PropertiesTextEdit MaxLength="100" ClientInstanceName="VariantName"/>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn FieldName="MaterialFUSAP" Caption="Material FU SAP"
                VisibleIndex="4" Width="100px" Settings-AutoFilterCondition="Contains">
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <Settings AutoFilterCondition="Contains" />
                <PropertiesTextEdit MaxLength="100" ClientInstanceName="MaterialFUSAP"/>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn FieldName="PartNo" Caption="Part No"
                VisibleIndex="5" Width="100px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn FieldName="PartName" Caption="Part Name"
                VisibleIndex="6" Width="300px" Settings-AutoFilterCondition="Contains">
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <Settings AutoFilterCondition="Contains" />
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn FieldName="Qty" Caption="Quantity"
                VisibleIndex="7" Width="100px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <PropertiesTextEdit MaxLength="15"  DisplayFormatString="#,##">
                    <MaskSettings Mask="<0..1000000000g>"  IncludeLiterals="DecimalSymbol" />
                    <Style HorizontalAlign="Left"/>
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains" />
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn FieldName="RegisterBy" Caption="Register By"
                VisibleIndex="8" Width="120px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <Settings AllowAutoFilter="False" />
                <EditFormSettings Visible="False" />
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="RegisterDate" Caption="Register Date"
                VisibleIndex="9" Width="130px" Settings-AutoFilterCondition="Contains" >                
                <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd MMM yyyy"/>
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <Settings AllowAutoFilter="False" />
                <EditFormSettings Visible="False" />
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataDateColumn>

            <dx:GridViewDataTextColumn FieldName="UpdateBy" Caption="Update By"
                VisibleIndex="10" Width="120px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <Settings AllowAutoFilter="False" />
                <EditFormSettings Visible="False" />
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn FieldName="UpdateDate" Caption="Update Date"
                VisibleIndex="11" Width="130px" Settings-AutoFilterCondition="Contains" >
                <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd MMM yyyy"/>
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <Settings AllowAutoFilter="False" />
                <EditFormSettings Visible="False" />
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataDateColumn>
        </Columns>
        <SettingsBehavior ColumnResizeMode="Control" />
        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270" HorizontalScrollBarMode="Auto" />
                <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm"/>
        <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
        <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
        <Styles>
            <Header>
                <Paddings Padding="2px"></Paddings>
            </Header>
        </Styles>
        <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                      
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                          
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
    </dx:ASPxGridView> 
</div>

</asp:Content>

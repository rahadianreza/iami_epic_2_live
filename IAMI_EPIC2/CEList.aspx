﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CEList.aspx.vb" Inherits="IAMI_EPIC2.CEList" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    function FillComboCENumber() {
        cboCENumber.PerformCallback(dtDateFrom.GetText() + '|' + dtDateTo.GetText());
    }


    function LoadComboCENumber() {
        cboCENumber.PerformCallback(dtDateFrom.GetText() + '|' + dtDateTo.GetText());
    }

    function Download() {
        Grid.PerformCallback('excel|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|' + cboCENumber.GetValue());
    }

    function AddData() {
        window.location.href = "/CEDetail.aspx?Date=" + dtDateFrom.GetText() + "|" + dtDateTo.GetText();
    }

    function MessageBox(s, e) {

        if (s.cpmessage == "Download Excel Successfull") {
            toastr.success(s.cpmessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpmessage == null || s.cpmessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpmessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

<div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
         <tr style="height: 20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="CE Date ">
                    </dx1:aspxlabel>                 
                </td>
                <td style="width:30px">&nbsp;</td>
                <td style="width:110px">
                <dx:aspxdateedit ID="dtDateFrom" runat="server" Theme="Office2010Black" 
                                        Width="100px" AutoPostBack="false" ClientInstanceName="dtDateFrom"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:aspxdateedit>   
                    
                </td>
                <td style="width:30px">   
                    
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="To">
                    </dx1:aspxlabel>   
                                                     
                </td>
                <td style="width:100px">
                <dx:aspxdateedit ID="dtDateTo" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateTo"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ClientSideEvents DateChanged="FillComboCENumber" />
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                    </dx:aspxdateedit>   
                    
                </td>
                             <td></td>
                <td></td>
                <td></td>
            </tr>      
         <tr style="height: 40px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="CE Number">
                    </dx1:ASPxLabel>                 
                </td>
                <td></td>
                <td colspan="5">
           
        <dx1:ASPxComboBox ID="cboCENumber" runat="server" ClientInstanceName="cboCENumber"
                            Width="180px" Font-Names="Segoe UI"  TextField="CE_Number"
                            ValueField="CE_Number" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <ClientSideEvents Init="LoadComboCENumber" />
                            <Columns>            
                            <dx:ListBoxColumn Caption="CE Number" FieldName="CE_Number" Width="100px" />
                       
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td></td>
            </tr>        
      <tr style="height: 10px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                         <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                    </td>
            </tr>     
      <tr style="height: 30px">
                <td colspan="8" style=" padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnRefresh" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {
	                        Grid.PerformCallback('gridload|'+ dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|' + cboCENumber.GetValue());
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                     &nbsp;
                    <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnRefresh" Theme="Default">                             
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 &nbsp;
                    <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnAdd" Theme="Default">                        
        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 </td>
            </tr>                                              
      <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>                                              
        </table>

</div>
<div style="padding: 5px 5px 5px 5px">

                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select CE_Number From CE_Header">
                    </asp:SqlDataSource>
                    
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="CE_Number;Revision_No;RFQ_Set" >

             <ClientSideEvents CustomButtonClick="function(s, e) {
	                        if(e.buttonID == 'edit'){                     
                                        var rowKey = Grid.GetRowKey(e.visibleIndex);  
                                                            
	                                    window.location.href= 'CEDetail.aspx?ID=' + rowKey;
                                    }
}" EndCallback="MessageBox" />

             <Columns>
                 <dx:GridViewCommandColumn VisibleIndex="0" Caption=" " FixedStyle="Left">
                     <CustomButtons>
                         <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                         </dx:GridViewCommandColumnCustomButton>
                     </CustomButtons>
                 </dx:GridViewCommandColumn>
                 <dx:GridViewDataTextColumn Caption="CE Number" VisibleIndex="1" FixedStyle="Left"
                     FieldName="CE_Number" Width="170px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CE Date" VisibleIndex="3" FixedStyle="Left"
                     FieldName="CE_Date" Width="100px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="RFQ Set Number" VisibleIndex="7" 
                     FieldName="RFQ_Set" Width="200px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="RFQ Date" VisibleIndex="8" Width="100px" 
                     FieldName="RFQ_Date">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Skip Budget" VisibleIndex="9" Width="90px" 
                     FieldName="Skip_Budget">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Urgent Status" VisibleIndex="10" Width="90px" 
                     FieldName="Urgent_Status">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Revision" FieldName="Revision_No" 
                     VisibleIndex="2" Width="60px" FixedStyle="Left">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CE Status" FieldName="CE_Status" 
                     VisibleIndex="6" Width="90px">
                     <Settings AutoFilterCondition="Contains" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewBandColumn Caption="Cost Control Sect Head" Name="ccsect" 
                    VisibleIndex="14">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Name" FieldName="CC_Sect_Head_ApproveBy" 
                            VisibleIndex="0" Width="100px">
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Date" FieldName="CC_Sect_Head_ApproveDate" 
                            VisibleIndex="1" Width="100px">
                            <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Notes" FieldName="CC_Sect_Head_ApproveNote" 
                            VisibleIndex="2" Width="120px">
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Left"></CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                     <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Cost Control Dept Head" Name="ccdept" 
                    VisibleIndex="15">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Name" FieldName="CC_Dept_Head_ApproveBy" 
                            VisibleIndex="0" Width="100px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Date" FieldName="CC_Dept_Head_ApproveDate" 
                            VisibleIndex="1" Width="100px">
                            <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Notes" FieldName="CC_Dept_Head_ApproveNote" 
                            VisibleIndex="2" Width="120px">
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Left"></CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="User Dept" Name="userdept" 
                    VisibleIndex="16">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Name" FieldName="User_Dept_ApproveBy" 
                            VisibleIndex="0" Width="100px">
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Date" FieldName="User_Dept_ApproveDate" 
                            VisibleIndex="1" Width="100px">
                            <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Notes" FieldName="User_Dept_ApproveNote" 
                            VisibleIndex="2" Width="120px">
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Left"></CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="User Dept Head" Name="userhead" 
                    VisibleIndex="17">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Name" FieldName="User_Dept_Head_ApproveBy" 
                            VisibleIndex="0" Width="100px">
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Date" FieldName="User_Dept_Head_ApproveDate" 
                            VisibleIndex="1" Width="100px">
                            <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Notes" FieldName="User_Dept_Head_ApproveNote" 
                            VisibleIndex="2" Width="120px">
                             <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Left"></CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Center" />
                </dx:GridViewBandColumn>
             </Columns>

            <Settings ShowFilterRow="True" VerticalScrollableHeight="300" 
                HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>

                <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
</EditFormColumnCaption>
                 </Styles>

             </dx:ASPxGridView>
    </div>
</div>
</asp:Content>

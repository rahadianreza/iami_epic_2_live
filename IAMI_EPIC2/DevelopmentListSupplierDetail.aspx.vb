﻿Imports DevExpress.XtraPrinting
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxPopupControl

Public Class DevelopmentListSupplierDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region


#Region "Procedure"
    Private Sub up_GridLoad(pProjectName As String)
        Dim ErrMsg As String = ""
        'Dim Pro As List(Of clsLabor)
        Dim ds As New DataSet
        ds = ClsDevelopmentSupplierDB.getListDevelopmentSupplierDetail(pProjectName, pUser, ErrMsg)
        'ds = ClsIADGSparePartDB.getListDevelopmentPartDetail(pProjectName, pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()

        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_ExcelGridAuto(Optional ByRef pErr As String = "")
        Try
            up_GridLoad(Session("ProjectID"))

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=DevelopmentSupplierDetail" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using
            ps.Dispose()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub
#End Region
    'Private Sub up_FillCombo()
    '    Dim ds As New DataSet
    '    Dim pErr As String = ""

    '    ds = ClsDevelopmentSupplierDB.GetDataSupplier(pErr)
    '    If pErr = "" Then
    '        For i = 0 To ds.Tables(0).Rows.Count - 1
    '            cboSupplier1.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
    '            cboSupplier2.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
    '            cboSupplier3.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
    '            cboSupplier4.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
    '            cboSupplier5.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
    '            cboSupplier6.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))

    '        Next
    '    End If


    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("O030")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "O030")
        Session("ProjectID") = Request.QueryString("ID").Split("|")(0)

        If Not Page.IsCallback And Not Page.IsPostBack Then
            up_GridLoad(Session("ProjectID"))
            'fill combo supplier
            'up_FillCombo()
        End If

        

        Dim ds As New DataSet
        ds = ClsDevelopmentSupplierDB.checkStatusSubmit(Session("ProjectID"))

        Dim script As String = ""
        If ds.Tables(0).Rows(0)("Status") = "1" Then
            script = "btnSubmit.SetEnabled(false);"
            ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "btnSubmit", script, True)
            Grid.Enabled = False
        Else
            script = "btnSubmit.SetEnabled(true);"
            ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "btnSubmit", script, True)
            Grid.Enabled = True
        End If


    End Sub
    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pProject As String
        pProject = Split(e.Parameters, "|")(0)
        up_GridLoad(pProject)

    End Sub

    Protected Sub BtnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        'cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        Dim ErrMsg As String = ""
        'up_Excel(ErrMsg)
        up_ExcelGridAuto(ErrMsg)

        'If ErrMsg <> "" Then
        '    cbExcel.JSProperties("cpmessage") = ErrMsg
        'Else
        '    cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        'End If

    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("DevelopmentListSupplier.aspx")
    End Sub

    Protected Sub Grid_CustomButtonCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs)
        Dim grid As ASPxGridView = DirectCast(sender, ASPxGridView)
        Dim ProjectYear As String = grid.GetRowValues(e.VisibleIndex, "ProjectYear").ToString()
        Dim ProjectID As String = grid.GetRowValues(e.VisibleIndex, "ProjectID").ToString()
        Dim PartNo As String = grid.GetRowValues(e.VisibleIndex, "PartNo").ToString()
        Dim Model As String = grid.GetRowValues(e.VisibleIndex, "Model").ToString()

        grid.JSProperties("cpKVProjectYear") = ProjectYear
        grid.JSProperties("cpKVProjectID") = ProjectID
        grid.JSProperties("cpKVPartNo") = PartNo
        grid.JSProperties("cpKVModel") = Model
    End Sub

    Protected Sub popup_WindowCallback(ByVal source As Object, ByVal e As PopupWindowCallbackArgs)

        Dim ProjectYear As String = e.Parameter.Split("|")(0)
        Dim ProjectID As String = e.Parameter.Split("|")(1)
        Dim PartNo As String = e.Parameter.Split("|")(2)
        Dim Model As String = e.Parameter.Split("|")(3)
        Dim ds As New DataSet
        ds = ClsDevelopmentSupplierDB.DevelopmentSupplierDetail_bindForm(ProjectYear, ProjectID, PartNo, Model, pUser)

        txtProjectID.Text = ProjectID
        txtPartNo.Text = PartNo
        If ds.Tables(0).Rows.Count > 0 Then
           
            'supplier 1-6
            cboSupplier1.Value = ds.Tables(0).Rows(0)("Supplier1").ToString
            cboSupplier2.Value = ds.Tables(0).Rows(0)("Supplier2").ToString
            cboSupplier3.Value = ds.Tables(0).Rows(0)("Supplier3").ToString
            cboSupplier4.Value = ds.Tables(0).Rows(0)("Supplier4").ToString
            cboSupplier5.Value = ds.Tables(0).Rows(0)("Supplier5").ToString
            cboSupplier6.Value = ds.Tables(0).Rows(0)("Supplier6").ToString
            'cboSupplier6.SelectedIndex = cboSupplier6.Items.IndexOf(cboSupplier6.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Supplier6") & "")))
        End If
    End Sub

    'Protected Sub btnUpdate_OnClick(sender As Object, e As EventArgs) Handles btnUpdate.Click
    '    Dim PartNo
    '    Dim ds As New DataSet
    '    ds = ClsIADGSparePartDB.developmentPartDetail_Update(Session("ProjectID"), partNo, txtFIGNo.Text, pUser)
    '    Grid.DataBind()
    '    popup.ShowOnPageLoad = False
    'End Sub

    Protected Sub cbUpdate_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbUpdate.Callback
        Dim ProjectYear As String = e.Parameter.Split("|")(0)
        Dim ProjectID As String = e.Parameter.Split("|")(1)
        Dim PartNo As String = e.Parameter.Split("|")(2)
        Dim Model As String = e.Parameter.Split("|")(3)
        Dim msgErr As String = ""
        Dim ds As New DataSet

        ds = ClsDevelopmentSupplierDB.DevelopmentSupplierDetail_Insert(ProjectYear, ProjectID, PartNo, Model, cboSupplier1.Value, cboSupplier2.Value, cboSupplier3.Value, cboSupplier4.Value, cboSupplier5.Value, cboSupplier6.Value, pUser, msgErr)
        If msgErr = "" Then
            cbUpdate.JSProperties("cpMessage") = "Update Data Successfully!"
        Else
            cbUpdate.JSProperties("cpMessage") = msgErr
        End If
        ' up_GridLoad(ProjectID)
    End Sub

    Protected Sub cbSubmit_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSubmit.Callback
        ' Dim ProjectID As String = e.Parameter.Split("|")(0)
        Dim msgErr As String = ""
        Dim ds As New DataSet
        'ds = ClsIADGSparePartDB.developmentPartDetail_CompleteStatus(Session("ProjectID"), pUser, msgErr)

        'If msgErr = "" Then
        '    cbCompleted.JSProperties("cpMessage") = "Data Completed Successfully!"
        '    cbCompleted.JSProperties("cpProjectID") = Session("ProjectID")
        'Else
        '    cbCompleted.JSProperties("cpMessage") = msgErr
        'End If



    End Sub

    'Protected Sub cboSupplier1_Init(sender As Object, e As EventArgs) Handles cboSupplier1.Init
    '    Dim ds As New DataSet
    '    Dim pErr As String
    '    ds = ClsDevelopmentSupplierDB.GetDataSupplier(pErr)
    '    If pErr = "" Then
    '        cboSupplier1.DataSource = ds
    '        cboSupplier1.DataBind()
    '    End If
    'End SubS
End Class
﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class ApprovalSetup
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            up_GridLoad(GridPR, "PR")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("Z040")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0, GridPR)
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "Z040")
        If AuthUpdate = False Then
            Dim commandColumn = TryCast(GridPR.Columns(0), GridViewCommandColumn)
            commandColumn.Visible = False
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal pGrid As ASPxGridView, ByVal pGroupID As String)
        Dim ErrMsg As String = ""
        Dim Ses As New List(Of ClsApprovalSetup)
        Ses = ClsApprovalSetupDB.getlist(pGroupID, ErrMsg)
        If ErrMsg = "" Then
            pGrid.DataSource = Ses
            pGrid.DataBind()
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_ExportExcel(ByVal pGrid As ASPxGridView)

    End Sub
#End Region

#Region "Control Event"
    Protected Sub GridPR_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridPR.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad(GridPR, "PR")
        End If
    End Sub

    Protected Sub GridPR_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsApprovalSetup With {
            .Approval_Group = e.NewValues("Approval_Group"),
            .Approval_ID = e.NewValues("Approval_ID"),
            .Approval_Level = e.NewValues("Approval_Level"),
            .Approval_Name = e.NewValues("Approval_Name"),
        .RegisterBy = pUser
        }

        ClsApprovalSetupDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPR)
        Else
            GridPR.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridPR)
            up_GridLoad(GridPR, "")
        End If
    End Sub

    Protected Sub GridPR_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridPR.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsApprovalSetup With {
            .Approval_Group = e.NewValues("Approval_Group"),
            .Approval_ID = e.NewValues("Approval_ID"),
            .Approval_Level = e.NewValues("Approval_Level"),
            .Approval_Name = e.NewValues("Approval_Name"),
            .RegisterBy = pUser
        }

        ClsApprovalSetupDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPR)
        Else
            GridPR.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridPR)
            up_GridLoad(GridPR, "")
        End If
    End Sub

    Protected Sub GridPR_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsApprovalSetup With {
            .Approval_Group = e.Values("Approval_Group"),
            .Approval_ID = e.Values("Approval_ID")
        }
        ClsApprovalSetupDB.Delete(ses, "", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridPR)
            up_GridLoad(GridPR, "")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPR)
        End If
    End Sub

    Private Sub GridPR_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridPR.BeforeGetCallbackResult
        If GridPR.IsNewRowEditing Then
            GridPR.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridPR_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridPR.CellEditorInitialize
        If Not GridPR.IsNewRowEditing Then
            If e.Column.FieldName = "Approval_Group" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If

            If e.Column.FieldName = "Approval_ID" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Protected Sub GridPR_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridPR.IsNewRowEditing) Then
            GridPR.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridPR)
    End Sub

    Private Sub GridPR_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridPR.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"

                up_ExportExcel(GridPR)

        End Select
    End Sub

    Protected Sub GridPR_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridPR.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If


            If dataColumn.FieldName = "Approval_Group" Then
                If IsNothing(e.NewValues("Approval_Group")) OrElse e.NewValues("Approval_Group").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Approval Group !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPR)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Approval_ID" Then
                If IsNothing(e.NewValues("Approval_ID")) OrElse e.NewValues("Approval_ID").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Approval ID !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPR)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsApprovalSetupDB.isExist(e.NewValues("Approval_ID"), e.NewValues("Approval_Group")) Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPR)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "Approval_Level" Then
                If IsNothing(e.NewValues("Approval_Level")) OrElse e.NewValues("Approval_Level").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Approval Level !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPR)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Approval_Name" Then
                If IsNothing(e.NewValues("Approval_Name")) OrElse e.NewValues("Approval_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Approval Name !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPR)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim ParameterName As String = ""

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)

            up_GridLoad(GridPR, "")
             link1.Component = GridExporter

        Dim compositeLink As New CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})
        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ApprovalSetup" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub
#End Region

End Class
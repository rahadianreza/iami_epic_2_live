﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Drawing

'Imports System.Drawing

Public Class RFQCheekSheet
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fRefresh As Boolean = False
    Dim PrjID As String = ""
    Dim Group As String = ""
    Dim commodity As String = ""
    Dim Groupcommodity As String = ""
    Dim datefrom As String = ""
    Dim dateto As String = ""
    Dim projtype As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("J050")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            Dim dfrom As Date
            dfrom = Now
            ProjectDate_From.Value = dfrom
            ProjectDate_To.Value = Now

            PrjID = Session("PrjIDdetail")
            Group = Session("Groupdetail")
            commodity = Session("commoditydetail")
            Groupcommodity = Session("Groupcommoditydetail")
            datefrom = Session("datefromdetail")
            dateto = Session("datetodetail")
            projtype = Session("projtypedetail")
            If PrjID <> "" Then
                FillCombo(projtype, PrjID, Group, commodity)
                'cboProjectType.Value = projtype
                ProjectDate_From.Text = Format(CDate(Session("datefromdetail")), "dd-MMM-yyyy") 'Format(CDate(Request.QueryString("datefrom")), "dd-MMM-yyyy")
                ProjectDate_To.Text = Format(CDate(Session("datetodetail")), "dd-MMM-yyyy") 'Format(CDate(Request.QueryString("dateto")), "dd-MMM-yyyy")
                cboProject.Value = PrjID
                cboGroup.Value = Group
                cboCommodity.Value = commodity
                cboCommodityGroup.Value = Groupcommodity
                up_GridLoad("", Format(CDate(Session("datefromdetail")), "yyyy-MM-dd"), Format(CDate(Session("datetodetail")), "yyyy-MM-dd"), cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)
                Session("PrjIDdetail") = ""
                Session("Groupdetail") = ""
                Session("commoditydetail") = ""
                Session("Groupcommoditydetail") = ""
                Session("datefromdetail") = ""
                Session("datetodetail") = ""
                Session("projtypedetail") = ""
            Else
                FillCombo(projtype, PrjID, Group, commodity)
                cboProject.Value = "All"
                cboGroup.Value = "All"
                cboCommodity.Value = "All"
                cboCommodityGroup.Value = "All"
            End If
        End If
    End Sub
#End Region

#Region "Control Event"

    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        'If e.CallbackName <> "CANCELEDIT" Then
        up_GridLoad("", Format(ProjectDate_From.Value, "yyyy-MM-dd"), Format(ProjectDate_To.Value, "yyyy-MM-dd"), cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)
        'End If

    End Sub

    Private Sub Grid_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles Grid.BeforeGetCallbackResult
        If Grid.IsNewRowEditing Then
            Grid.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub Grid_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If Grid.IsNewRowEditing Then
            If e.Column.FieldName = "NoRFQ" Or e.Column.FieldName = "DateRFQ" Or e.Column.FieldName = "VendorName" Or e.Column.FieldName = "AttDeptHeadName" Or e.Column.FieldName = "AttProjectPICName" Or _
                 e.Column.FieldName = "AttCostPICName" Or e.Column.FieldName = "CostControlDeptHeadName" Or e.Column.FieldName = "SectionHeadName" Or _
                  e.Column.FieldName = "EmailCostPIC" Or e.Column.FieldName = "EmailProjectPIC" Or e.Column.FieldName = "EmailDeptHead" Or e.Column.FieldName = "AppPerson1" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
                e.Editor.Visible = False
                e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                'Else
                '    Dim ds As DataSet = ClsRFQCheekSheetDB.getlistApproveHeader()
                '    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                '        If e.Column.FieldName = "AppPerson" & CStr(i + 1) Then
                '            e.Editor.ReadOnly = True
                '            e.Editor.ForeColor = Color.Silver
                '            e.Editor.Visible = False
                '            e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                '        End If
                '        If e.Column.FieldName = "AppDate" & CStr(i + 1) Then
                '            e.Editor.ReadOnly = True
                '            e.Editor.ForeColor = Color.Silver
                '            e.Editor.Visible = False
                '            e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                '        End If
                '    Next
            End If
        End If
        If Not Grid.IsNewRowEditing Then
            If e.Column.FieldName = "NoRFQ" Or e.Column.FieldName = "VendorCode" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
                e.Editor.Visible = True
            End If

            If e.Column.FieldName = "DateRFQ" Or e.Column.FieldName = "VendorName" Or e.Column.FieldName = "AttDeptHeadName" Or e.Column.FieldName = "AttProjectPICName" Or _
                e.Column.FieldName = "AttCostPICName" Or e.Column.FieldName = "CostControlDeptHeadName" Or e.Column.FieldName = "SectionHeadName" Or _
                e.Column.FieldName = "EmailCostPIC" Or e.Column.FieldName = "EmailProjectPIC" Or e.Column.FieldName = "EmailDeptHead" Or _
                e.Column.FieldName = "AppPerson1" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
                e.Editor.Visible = False
                e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                'Else
                '    Dim ds As DataSet = ClsRFQCheekSheetDB.getlistApproveHeader()
                '    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                '        If e.Column.FieldName = "AppPerson" & CStr(i + 1) Then
                '            e.Editor.ReadOnly = True
                '            e.Editor.ForeColor = Color.Silver
                '            e.Editor.Visible = False
                '            e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                '        End If
                '        If e.Column.FieldName = "AppDate" & CStr(i + 1) Then
                '            e.Editor.ReadOnly = True
                '            e.Editor.ForeColor = Color.Silver
                '            e.Editor.Visible = False
                '            e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                '        End If
                '    Next
            End If
           
        End If
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.VisibleIndex < 0) Then
            Exit Sub
        End If

        If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Delete Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
            If ClsRFQCheekSheetDB.isExistinQoutation(Grid.GetRowValues(e.VisibleIndex, "Project_ID"), Grid.GetRowValues(e.VisibleIndex, "Group_ID"), Grid.GetRowValues(e.VisibleIndex, "Comodity"), Grid.GetRowValues(e.VisibleIndex, "Group_Comodity"), Grid.GetRowValues(e.VisibleIndex, "VendorCode")) = True Then
                e.Visible = False
            End If
            'If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
            '    e.Visible = False
            'End If
        End If

    End Sub
    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
       
        Select Case pFunction
            Case "load"
                Dim pProjectID As String = Split(e.Parameters, "|")(1)
                Dim pFrom As String = Split(e.Parameters, "|")(2)
                Dim pTo As String = Split(e.Parameters, "|")(3)
                up_GridLoad("", Format(ProjectDate_From.Value, "yyyy-MM-dd"), Format(ProjectDate_To.Value, "yyyy-MM-dd"), cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)

        End Select
    End Sub

    Private Sub up_Excel()
        up_GridLoad("", Format(ProjectDate_From.Value, "yyyy-MM-dd"), Format(ProjectDate_To.Value, "yyyy-MM-dd"), cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=RfQCheckSheet" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Procedure"

    Private Sub up_GridLoad(ByVal pProjecType As String, ByVal pFrom As String, ByVal pTo As String, ByVal pProjectID As String, ByVal pGroup As String, ByVal pComodity As String, ByVal pGroupComodity As String)
        Dim ErrMsg As String = ""
        Dim Ses As New DataSet
        'Dim Ses As New List(Of ClsRFQCheekSheet)
        'Ses = ClsRFQCheekSheetDB.getlist(pFrom, pTo, IIf(pProjecType = Nothing, "", pProjecType), IIf(pProjectID = Nothing, "", pProjectID), IIf(pGroup = Nothing, "", pGroup), IIf(pComodity = Nothing, "", pComodity), IIf(pGroupComodity = Nothing, "", pGroupComodity), pUser, ErrMsg)
        Ses = ClsRFQCheekSheetDB.getlistds(pFrom, pTo, IIf(pProjecType = Nothing, "", pProjecType), IIf(pProjectID = Nothing, "", pProjectID), IIf(pGroup = Nothing, "", pGroup), IIf(pComodity = Nothing, "", pComodity), IIf(pGroupComodity = Nothing, "", pGroupComodity), pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
            addcolumn()
        End If
    End Sub
    Public Sub addcolumn()
        Dim pno As Integer = 16
        Dim ds As DataSet = ClsRFQCheekSheetDB.getlistApproveHeader()
        Dim pBandColName As String = ds.Tables(0).Rows(0)("Approval_Name").ToString()
        Dim colCheck As Object = Grid.Columns(pBandColName)
        If colCheck IsNot Nothing Then
            Return
        End If

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim bandColumn As GridViewBandColumn = New GridViewBandColumn()
            bandColumn.Caption = ds.Tables(0).Rows(i)("Approval_Name").ToString()
            bandColumn.VisibleIndex = pno
            bandColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

            Dim col As GridViewDataTextColumn = New GridViewDataTextColumn()
            col.Caption = "Approval Name"
            col.FieldName = "AppPerson" & CStr(i + 1)
            col.VisibleIndex = pno + 1
            col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
            bandColumn.Columns.Add(col)

            Dim col2 As GridViewDataTextColumn = New GridViewDataTextColumn()
            col2.Caption = "Approval Date"
            col2.FieldName = "AppDate" & CStr(i + 1)
            col2.VisibleIndex = pno + 2
            col2.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            col2.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
            bandColumn.Columns.Add(col2)
            pno = pno + 3
            Grid.Columns.Add(bandColumn)
        Next
    End Sub
#End Region

    Protected Sub Grid_RowValidating(sender As Object, e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In Grid.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "VendorCode" Then
                If IsNothing(e.NewValues("VendorCode")) OrElse e.NewValues("VendorCode").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please select Supplier Name !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsRFQCheekSheetDB.isExist(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, e.NewValues("VendorCode"), pUser) Then
                            e.Errors(dataColumn) = "Supplier Name is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "ToRFQ" Then
                If IsNothing(e.NewValues("ToRFQ")) OrElse e.NewValues("ToRFQ").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please Input To!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "PhoneRFQ" Then
                If IsNothing(e.NewValues("PhoneRFQ")) OrElse e.NewValues("PhoneRFQ").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please Input Phone!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "FaxRFQ" Then
                If IsNothing(e.NewValues("FaxRFQ")) OrElse e.NewValues("FaxRFQ").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please Input Fax!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "AttDeptHead" Then
                If IsNothing(e.NewValues("AttDeptHead")) OrElse e.NewValues("AttDeptHead").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please Select Attn Dept Head!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "AttProjcetPIC" Then
                If IsNothing(e.NewValues("AttProjcetPIC")) OrElse e.NewValues("AttProjcetPIC").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please Select Attn Projcet PIC!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If


            If dataColumn.FieldName = "AttCostPIC" Then
                If IsNothing(e.NewValues("AttCostPIC")) OrElse e.NewValues("AttCostPIC").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please Select Attn Cost PIC!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If

           

                If dataColumn.FieldName = "CostControlDeptHead" Then
                    If IsNothing(e.NewValues("CostControlDeptHead")) OrElse e.NewValues("CostControlDeptHead").ToString.Trim = "" Then
                        e.Errors(dataColumn) = "Please Select Cost Control Dept Head!"
                        show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                        Exit Sub
                    End If
                End If

                If dataColumn.FieldName = "SectionHead" Then
                    If IsNothing(e.NewValues("SectionHead")) OrElse e.NewValues("SectionHead").ToString.Trim = "" Then
                        e.Errors(dataColumn) = "Please Select Section Head!"
                        show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                        Exit Sub
                    End If
                End If

        Next column
    End Sub

    'Protected Sub Grid_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
    'If (Not Grid.IsNewRowEditing) Then
    '        Grid.DoRowValidation()
    '    End If
    '    show_error(MsgTypeEnum.Info, "", 0, Grid)
    'End Sub
    Private Sub Grid_StartRowEditing1(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs) Handles Grid.StartRowEditing
        If (Not Grid.IsNewRowEditing) Then
            Grid.DoRowValidation()
        End If
    End Sub
    Protected Sub Grid_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsRFQCheekSheet With {.Project_ID = cboProject.Value,
                                              .Group_ID = cboGroup.Value,
                                              .Comodity = cboCommodity.Value,
                                              .Group_Comodity = cboCommodityGroup.Value,
                                              .VendorCode = e.NewValues("VendorCode"),
                                         .ToRFQ = e.NewValues("ToRFQ"),
                                         .PhoneRFQ = e.NewValues("PhoneRFQ"),
                                         .FaxRFQ = e.NewValues("FaxRFQ"),
                                         .AttDeptHead = e.NewValues("AttDeptHead"),
                                         .AttProjectPIC = e.NewValues("AttProjectPIC"),
                                         .AttCostPIC = e.NewValues("AttCostPIC"),
                                         .UserID = pUser}
        ClsRFQCheekSheetDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, Grid)
            up_GridLoad("", Format(ProjectDate_From.Value, "yyyy-MM-dd"), Format(ProjectDate_To.Value, "yyyy-MM-dd"), cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)
        End If
    End Sub

    Protected Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        '.Project_ID = cboProject.Value,
        '                                      .Group_ID = cboGroup.Value,
        '                                      .Comodity = cboCommodity.Value,
        '                                      .Group_Comodity = cboCommodityGroup.Value,
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsRFQCheekSheet With {.Project_ID = e.Keys("Project_ID"),
                                              .Group_ID = e.Keys("Group_ID"),
                                              .Comodity = e.Keys("Comodity"),
                                              .Group_Comodity = e.Keys("Group_Comodity"),
                                              .VendorCode = e.Values("VendorCode"),
                                              .NoRFQ = e.Values("NoRFQ"),
                                             .UserID = pUser}
        ClsRFQCheekSheetDB.Delete(ses, pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, Grid)
            up_GridLoad("", Format(ProjectDate_From.Value, "yyyy-MM-dd"), Format(ProjectDate_To.Value, "yyyy-MM-dd"), cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
        End If
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        '.Project_ID = cboProject.Value,
        '                                      .Group_ID = cboGroup.Value,
        '                                      .Comodity = cboCommodity.Value,
        '                                      .Group_Comodity = cboCommodityGroup.Value,
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsRFQCheekSheet With {.Project_ID = e.Keys("Project_ID"),
                                              .Group_ID = e.Keys("Group_ID"),
                                              .Comodity = e.Keys("Comodity"),
                                              .Group_Comodity = e.Keys("Group_Comodity"),
                                              .VendorCode = e.Keys("VendorCode"),
                                             .NoRFQ = e.NewValues("NoRFQ"),
                                             .ToRFQ = e.NewValues("ToRFQ"),
                                             .PhoneRFQ = e.NewValues("PhoneRFQ"),
                                             .FaxRFQ = e.NewValues("FaxRFQ"),
                                             .AttDeptHead = e.NewValues("AttDeptHead"),
                                             .AttProjectPIC = e.NewValues("AttProjectPIC"),
                                             .AttCostPIC = e.NewValues("AttCostPIC"),
                                             .UserID = pUser}

        ClsRFQCheekSheetDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, Grid)
            up_GridLoad("", Format(ProjectDate_From.Value, "yyyy-MM-dd"), Format(ProjectDate_To.Value, "yyyy-MM-dd"), cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value)
        End If
    End Sub
    Private Sub FillCombo(ByVal ptype As String, ByVal proj As String, ByVal group As String, ByVal commodity As String)
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsRFQCheekSheetDB.getProjectID(ptype, "", "", "", 1, pUser)
        If pmsg = "" Then
            cboProject.DataSource = ds
            cboProject.DataBind()
        End If

        ds = ClsRFQCheekSheetDB.getProjectID(ptype, proj, "", "", 2, pUser)
        If pmsg = "" Then
            cboGroup.DataSource = ds
            cboGroup.DataBind()
        End If

        ds = ClsRFQCheekSheetDB.getProjectID(ptype, proj, group, "", 3, pUser)
        If pmsg = "" Then
            cboCommodity.DataSource = ds
            cboCommodity.DataBind()
        End If

        ds = ClsRFQCheekSheetDB.getProjectID(ptype, proj, group, commodity, 4, pUser)
        If pmsg = "" Then
            cboCommodityGroup.DataSource = ds
            cboCommodityGroup.DataBind()
        End If
    End Sub
    Private Sub cboProject_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboProject.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsRFQCheekSheetDB.getProjectID("", "", "", "", 1, pUser)
        If pmsg = "" Then
            cboProject.DataSource = ds
            cboProject.DataBind()
        End If
    End Sub

    Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsRFQCheekSheetDB.getProjectID("", cboProject.Value, "", "", 2, pUser)
        If pmsg = "" Then
            cboGroup.DataSource = ds
            cboGroup.DataBind()
        End If
    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsRFQCheekSheetDB.getProjectID("", cboProject.Value, cboGroup.Value, "", 3, pUser)
        If pmsg = "" Then
            cboCommodity.DataSource = ds
            cboCommodity.DataBind()
        End If
    End Sub

    Private Sub cboCommodityGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodityGroup.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsRFQCheekSheetDB.getProjectID("", cboProject.Value, cboGroup.Value, cboCommodity.Value, 4, pUser)
        If pmsg = "" Then
            cboCommodityGroup.DataSource = ds
            cboCommodityGroup.DataBind()
        End If
    End Sub


End Class
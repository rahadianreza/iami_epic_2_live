﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO
Imports System.Transactions
Imports System.Web.UI
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors

Public Class SupplierRecomendApprovalDetail
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""
    Dim ls_VSRNumber As String 
    Dim vSRNumber As String
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"

    Private Sub up_GridHeader(pCPNo As String, pSupplier As String)
        Dim headerCaption As String = ""
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
            Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
            Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
            Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
            Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
            Grid.Columns("Supplier1").Visible = True
            Grid.Columns("Supplier2").Visible = True
            Grid.Columns("Supplier3").Visible = True
            Grid.Columns("Supplier4").Visible = True
            Grid.Columns("Supplier5").Visible = True

            '#Set by condition
            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaptionLoad(pCPNo), "|")(0)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.Columns("Supplier1").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaptionLoad(pCPNo), "|")(1)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.Columns("Supplier2").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaptionLoad(pCPNo), "|")(2)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.Columns("Supplier3").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaptionLoad(pCPNo), "|")(3)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.Columns("Supplier4").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaptionLoad(pCPNo), "|")(4)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("Supplier5").Visible = False
            End Try





        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try

    End Sub

    Private Sub FillComboCPNumber()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsSupplierRecommendationDB.GetComboCPNo(pErr)
        If pErr = "" Then
            cboCPNumber.DataSource = ds
            cboCPNumber.DataBind()
        End If
    End Sub

    Private Function GetAllSupplierForGridHeaderCaption()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "SELECT Sup = dbo.MergeSupplierSR('" & cboCPNumber.Text & "')", "", "", ErrMsg)

        If ErrMsg = "" Then
            retVal = ds.Tables(0).Rows(0).Item("Sup").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboCPNumber)
        End If

        Return retVal
    End Function

    Private Function GetAllSupplierForGridHeaderCaptionLoad(pCPNumber As String)
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "SELECT Sup = dbo.MergeSupplierSR('" & pCPNumber & "')", "", "", ErrMsg)

        If ErrMsg = "" Then
            retVal = ds.Tables(0).Rows(0).Item("Sup").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboCPNumber)
        End If

        Return retVal
    End Function

    Private Sub up_GridLoadSR(SRNumber As String, Revision As Integer)
        Dim ds As New DataSet
        Dim ds_Delivery As New DataSet
        Dim ds_Quality As New DataSet
        Dim ErrMsg As String = ""
        Dim vSupplier As String = ""
        Dim headSupplier As String = ""
        Dim vname As String = "Supplier"
        Dim vfield As String = "S"
        Dim a As Integer

        ds = clsSupplierRecommendationDB.GetSRDataApproval(SRNumber, Revision, pUser, ErrMsg)

        If ds.Tables(0).Rows.Count > 0 Then
            dtSRDate.Value = ds.Tables(0).Rows(0)("SR_Date")
            cboCPNumber.Text = ds.Tables(0).Rows(0)("CP_Number")
            txtRev.Text = Revision
            txtSRNumber.Text = SRNumber
            txtParameter.Text = SRNumber & "|" & Revision & "|" & ds.Tables(0).Rows(0)("CP_Number")

            If ds.Tables(0).Rows(0)("SR_Status") = "1" Then
                cbDraf.JSProperties("cpStatus") = "1"
                gs_Status = "1"
            ElseIf ds.Tables(0).Rows(0)("SR_Status") = "2" Then
                cbDraf.JSProperties("cpStatus") = "2"
                gs_Status = "2"
            ElseIf ds.Tables(0).Rows(0)("SR_Status") = "3" Then
                cbDraf.JSProperties("cpStatus") = "3"
                gs_Status = "3"
            Else
                cbDraf.JSProperties("cpStatus") = "0"
                gs_Status = "0"
            End If


            cbDraf.JSProperties("cpStatus") = ds.Tables(0).Rows(0)("SR_Status")
            gs_SRRevNo = ds.Tables(0).Rows(0)("Rev")

            memoConsi.Text = ds.Tables(0).Rows(0)("Consideration")
            memoNote.Text = ds.Tables(0).Rows(0)("Note")
            cbSuppNo.Text = ds.Tables(0).Rows(0)("Supplier_Recomm")

            dtSRDate.Enabled = False
            cboCPNumber.Enabled = False

            txtFlag.Text = 1
        End If




        'Dim ds1 As New DataSet
        ds = GetDataSource(CmdType.StoreProcedure, "sp_SR_Detail_ListSR", "CPNumber|SRNumber|Rev", cboCPNumber.Text & "|" & SRNumber & "|" & Revision)
        ds_Delivery = GetDataSource(CmdType.StoreProcedure, "sp_SR_Detail_ListSR_Delivery", "CPNumber|SRNumber|Rev", cboCPNumber.Text & "|" & SRNumber & "|" & Revision)
        ds_Quality = GetDataSource(CmdType.StoreProcedure, "sp_SR_Detail_ListSR_Quality", "CPNumber|SRNumber|Rev", cboCPNumber.Text & "|" & SRNumber & "|" & Revision)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()

            Grid_Delivery.DataSource = ds_Delivery
            Grid_Delivery.DataBind()

            Grid_Quality.DataSource = ds_Quality
            Grid_Quality.DataBind()
        End If

    End Sub

    Private Sub up_GridLoad(RFSet As String, Supplier As String)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        Dim ds_Delivery As New DataSet
        Dim ds_Quality As New DataSet
        Dim vSupplier As String = ""
        Dim headSupplier As String = ""
        Dim vname As String = "Supplier"
        Dim vfield As String = "S"
        Dim a As Integer


        ds = GetDataSource(CmdType.StoreProcedure, "sp_SR_Detail_List", "CPNumber|QTNumber", cboCPNumber.Text & "|")
        ds_Delivery = GetDataSource(CmdType.StoreProcedure, "sp_SR_Detail_List_Delivery", "CPNumber|QTNumber", cboCPNumber.Text & "|")
        ds_Quality = GetDataSource(CmdType.StoreProcedure, "sp_SR_Detail_List_Quality", "CPNumber|QTNumber", cboCPNumber.Text & "|")

        If ErrMsg = "" Then

            Grid.DataSource = ds
            Grid.DataBind()

            Grid_Delivery.DataSource = ds_Delivery
            Grid_Delivery.DataBind()

            Grid_Quality.DataSource = ds_Quality
            Grid_Quality.DataBind()


        End If
    End Sub

    Private Sub up_SaveDetail(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs, Optional ByRef pErr As String = "")
        Dim a As Integer
        Dim ls_Code As String
        Dim ls_Information As String
        Dim ls_Supplier1 As String
        Dim ls_Quotation1 As String
        Dim ls_Final1 As String

        Dim ls_Supplier2 As String
        Dim ls_Quotation2 As String
        Dim ls_Final2 As String

        Dim ls_Supplier3 As String
        Dim ls_Quotation3 As String
        Dim ls_Final3 As String

        Dim ls_Supplier4 As String
        Dim ls_Quotation4 As String
        Dim ls_Final4 As String

        Dim ls_Supplier5 As String
        Dim ls_Quotation5 As String
        Dim ls_Final5 As String


        Dim ls_SRNumber As String

        Dim ls_GroupType As String

        Dim SRCls As New clsSupplierRecommendation

        a = e.UpdateValues.Count

        If vSRNumber <> "" Then
            ls_SRNumber = vSRNumber
            SRCls.Revision = "0"
            Dim i As Integer
            Dim aa As Integer
            For iLoop = 0 To a - 1
                aa = e.UpdateValues(iLoop).NewValues.Count
                If aa = 17 Then
                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Final1 = e.UpdateValues(iLoop).NewValues("Final1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Final2 = e.UpdateValues(iLoop).NewValues("Final2").ToString()
                    ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                    ls_Quotation3 = e.UpdateValues(iLoop).NewValues("Quotation3").ToString()
                    ls_Final3 = e.UpdateValues(iLoop).NewValues("Final3").ToString()
                    '11
                    ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                    ls_Quotation4 = e.UpdateValues(iLoop).NewValues("Quotation4").ToString()
                    ls_Final4 = e.UpdateValues(iLoop).NewValues("Final4").ToString()
                    '14:
                    ls_Supplier5 = e.UpdateValues(iLoop).NewValues("Supplier5").ToString()
                    ls_Quotation5 = e.UpdateValues(iLoop).NewValues("Quotation5").ToString()
                    ls_Final5 = e.UpdateValues(iLoop).NewValues("Final5").ToString()

                ElseIf aa = 14 Then
                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Final1 = e.UpdateValues(iLoop).NewValues("Final1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Final2 = e.UpdateValues(iLoop).NewValues("Final2").ToString()
                    ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                    ls_Quotation3 = e.UpdateValues(iLoop).NewValues("Quotation3").ToString()
                    ls_Final3 = e.UpdateValues(iLoop).NewValues("Final3").ToString()
                    ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                    ls_Quotation4 = e.UpdateValues(iLoop).NewValues("Quotation4").ToString()
                    ls_Final4 = e.UpdateValues(iLoop).NewValues("Final4").ToString()
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""
                    ls_Final5 = ""

                ElseIf aa = 11 Then

                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Final1 = e.UpdateValues(iLoop).NewValues("Final1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Final2 = e.UpdateValues(iLoop).NewValues("Final2").ToString()
                    ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                    ls_Quotation3 = e.UpdateValues(iLoop).NewValues("Quotation3").ToString()
                    ls_Final3 = e.UpdateValues(iLoop).NewValues("Final3").ToString()
                    ls_Supplier4 = ""
                    ls_Quotation4 = ""
                    ls_Final4 = ""
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""
                    ls_Final5 = ""

                ElseIf aa = 8 Then

                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Final1 = e.UpdateValues(iLoop).NewValues("Final1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Final2 = e.UpdateValues(iLoop).NewValues("Final2").ToString()
                    ls_Supplier3 = ""
                    ls_Quotation3 = ""
                    ls_Final3 = ""
                    ls_Supplier4 = ""
                    ls_Quotation4 = ""
                    ls_Final4 = ""
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""
                    ls_Final5 = ""

                Else

                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Final1 = e.UpdateValues(iLoop).NewValues("Final1").ToString()
                    ls_Supplier2 = ""
                    ls_Quotation2 = ""
                    ls_Final2 = ""
                    ls_Supplier3 = ""
                    ls_Quotation3 = ""
                    ls_Final3 = ""
                    ls_Supplier4 = ""
                    ls_Quotation4 = ""
                    ls_Final4 = ""
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""
                    ls_Final5 = ""

                End If



                ls_GroupType = "COST"

                SRCls.SRNumber = ls_SRNumber

                If txtRev.Text = "" Then
                    SRCls.Revision = 0
                Else
                    SRCls.Revision = txtRev.Text
                End If

                SRCls.CPNumber = cboCPNumber.Text
                SRCls.Information_Code = ls_Code
                SRCls.Information = ls_Information
                SRCls.Supplier1 = ls_Supplier1
                SRCls.Quotation1 = ls_Quotation1
                SRCls.Final1 = ls_Final1
                SRCls.Supplier2 = ls_Supplier2
                SRCls.Quotation2 = ls_Quotation2
                SRCls.Final2 = ls_Final2
                SRCls.Supplier3 = ls_Supplier3
                SRCls.Quotation3 = ls_Quotation3
                SRCls.Final3 = ls_Final3
                SRCls.Supplier4 = ls_Supplier4
                SRCls.Quotation4 = ls_Quotation4
                SRCls.Final4 = ls_Final4
                SRCls.Supplier5 = ls_Supplier5
                SRCls.Quotation5 = ls_Quotation5
                SRCls.Final5 = ls_Final5
                SRCls.GroupType = ls_GroupType

                i = clsSupplierRecommendationDB.UpdateDetail(SRCls, pUser, pErr)

                If i = 0 Then
                    clsSupplierRecommendationDB.InsertDetail(SRCls, pUser, pErr)
                End If
            Next
        End If
    End Sub

    Private Sub up_SaveDetail_Delivery(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs, Optional ByRef pErr As String = "")
        Dim a As Integer
        Dim ls_Code As String
        Dim ls_Information As String
        Dim ls_Supplier1 As String
        Dim ls_Quotation1 As String

        Dim ls_Supplier2 As String
        Dim ls_Quotation2 As String

        Dim ls_Supplier3 As String
        Dim ls_Quotation3 As String

        Dim ls_Supplier4 As String
        Dim ls_Quotation4 As String

        Dim ls_Supplier5 As String
        Dim ls_Quotation5 As String


        Dim ls_SRNumber As String

        Dim ls_GroupType As String

        Dim SRCls As New clsSupplierRecommendation

        a = e.UpdateValues.Count

        If vSRNumber <> "" Then
            ls_SRNumber = vSRNumber
            SRCls.Revision = "0"
            Dim i As Integer
            Dim aa As Integer
            For iLoop = 0 To a - 1
                aa = e.UpdateValues(iLoop).NewValues.Count
                If aa = 12 Then
                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                    ls_Quotation3 = e.UpdateValues(iLoop).NewValues("Quotation3").ToString()
                    ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                    ls_Quotation4 = e.UpdateValues(iLoop).NewValues("Quotation4").ToString()
                    ls_Supplier5 = e.UpdateValues(iLoop).NewValues("Supplier5").ToString()
                    ls_Quotation5 = e.UpdateValues(iLoop).NewValues("Quotation5").ToString()

                ElseIf aa = 10 Then
                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                    ls_Quotation3 = e.UpdateValues(iLoop).NewValues("Quotation3").ToString()
                    ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                    ls_Quotation4 = e.UpdateValues(iLoop).NewValues("Quotation4").ToString()
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""

                ElseIf aa = 8 Then

                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                    ls_Quotation3 = e.UpdateValues(iLoop).NewValues("Quotation3").ToString()
                    ls_Supplier4 = ""
                    ls_Quotation4 = ""
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""

                ElseIf aa = 6 Then

                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Supplier3 = ""
                    ls_Quotation3 = ""
                    ls_Supplier4 = ""
                    ls_Quotation4 = ""
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""

                Else

                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Supplier2 = ""
                    ls_Quotation2 = ""
                    ls_Supplier3 = ""
                    ls_Quotation3 = ""
                    ls_Supplier4 = ""
                    ls_Quotation4 = ""
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""

                End If



                ls_GroupType = "DELIVERY"

                SRCls.SRNumber = ls_SRNumber

                If txtRev.Text = "" Then
                    SRCls.Revision = 0
                Else
                    SRCls.Revision = txtRev.Text
                End If

                SRCls.CPNumber = cboCPNumber.Text
                SRCls.Information_Code = ls_Code
                SRCls.Information = ls_Information
                SRCls.Supplier1 = ls_Supplier1
                SRCls.Quotation1 = ls_Quotation1
                SRCls.Final1 = ""
                SRCls.Supplier2 = ls_Supplier2
                SRCls.Quotation2 = ls_Quotation2
                SRCls.Final2 = ""
                SRCls.Supplier3 = ls_Supplier3
                SRCls.Quotation3 = ls_Quotation3
                SRCls.Final3 = ""
                SRCls.Supplier4 = ls_Supplier4
                SRCls.Quotation4 = ls_Quotation4
                SRCls.Final4 = ""
                SRCls.Supplier5 = ls_Supplier5
                SRCls.Quotation5 = ls_Quotation5
                SRCls.Final5 = ""
                SRCls.GroupType = ls_GroupType

                i = clsSupplierRecommendationDB.UpdateDetail(SRCls, pUser, pErr)

                If i = 0 Then
                    clsSupplierRecommendationDB.InsertDetail(SRCls, pUser, pErr)
                End If
            Next
        End If
    End Sub

    Private Sub up_SaveDetail_Quality(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs, Optional ByRef pErr As String = "")
        Dim a As Integer
        Dim ls_Code As String
        Dim ls_Information As String
        Dim ls_Supplier1 As String
        Dim ls_Quotation1 As String

        Dim ls_Supplier2 As String
        Dim ls_Quotation2 As String

        Dim ls_Supplier3 As String
        Dim ls_Quotation3 As String

        Dim ls_Supplier4 As String
        Dim ls_Quotation4 As String

        Dim ls_Supplier5 As String
        Dim ls_Quotation5 As String


        Dim ls_SRNumber As String

        Dim ls_GroupType As String

        Dim SRCls As New clsSupplierRecommendation

        a = e.UpdateValues.Count

        If vSRNumber <> "" Then
            ls_SRNumber = vSRNumber
            SRCls.Revision = "0"
            Dim i As Integer
            Dim aa As Integer
            For iLoop = 0 To a - 1
                aa = e.UpdateValues(iLoop).NewValues.Count
                If aa = 12 Then
                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                    ls_Quotation3 = e.UpdateValues(iLoop).NewValues("Quotation3").ToString()
                    ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                    ls_Quotation4 = e.UpdateValues(iLoop).NewValues("Quotation4").ToString()
                    ls_Supplier5 = e.UpdateValues(iLoop).NewValues("Supplier5").ToString()
                    ls_Quotation5 = e.UpdateValues(iLoop).NewValues("Quotation5").ToString()

                ElseIf aa = 10 Then
                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                    ls_Quotation3 = e.UpdateValues(iLoop).NewValues("Quotation3").ToString()
                    ls_Supplier4 = e.UpdateValues(iLoop).NewValues("Supplier4").ToString()
                    ls_Quotation4 = e.UpdateValues(iLoop).NewValues("Quotation4").ToString()
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""

                ElseIf aa = 8 Then

                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Supplier3 = e.UpdateValues(iLoop).NewValues("Supplier3").ToString()
                    ls_Quotation3 = e.UpdateValues(iLoop).NewValues("Quotation3").ToString()
                    ls_Supplier4 = ""
                    ls_Quotation4 = ""
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""

                ElseIf aa = 6 Then

                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Supplier2 = e.UpdateValues(iLoop).NewValues("Supplier2").ToString()
                    ls_Quotation2 = e.UpdateValues(iLoop).NewValues("Quotation2").ToString()
                    ls_Supplier3 = ""
                    ls_Quotation3 = ""
                    ls_Supplier4 = ""
                    ls_Quotation4 = ""
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""

                Else

                    ls_Code = Trim(e.UpdateValues(iLoop).NewValues("Information_Code").ToString())
                    ls_Information = Trim(e.UpdateValues(iLoop).NewValues("Information").ToString())
                    ls_Supplier1 = e.UpdateValues(iLoop).NewValues("Supplier1").ToString()
                    ls_Quotation1 = e.UpdateValues(iLoop).NewValues("Quotation1").ToString()
                    ls_Supplier2 = ""
                    ls_Quotation2 = ""
                    ls_Supplier3 = ""
                    ls_Quotation3 = ""
                    ls_Supplier4 = ""
                    ls_Quotation4 = ""
                    ls_Supplier5 = ""
                    ls_Quotation5 = ""

                End If



                ls_GroupType = "QUALITY"

                SRCls.SRNumber = ls_SRNumber

                If txtRev.Text = "" Then
                    SRCls.Revision = 0
                Else
                    SRCls.Revision = txtRev.Text
                End If

                SRCls.CPNumber = cboCPNumber.Text
                SRCls.Information_Code = ls_Code
                SRCls.Information = ls_Information
                SRCls.Supplier1 = ls_Supplier1
                SRCls.Quotation1 = ls_Quotation1
                SRCls.Final1 = ""
                SRCls.Supplier2 = ls_Supplier2
                SRCls.Quotation2 = ls_Quotation2
                SRCls.Final2 = ""
                SRCls.Supplier3 = ls_Supplier3
                SRCls.Quotation3 = ls_Quotation3
                SRCls.Final3 = ""
                SRCls.Supplier4 = ls_Supplier4
                SRCls.Quotation4 = ls_Quotation4
                SRCls.Final4 = ""
                SRCls.Supplier5 = ls_Supplier5
                SRCls.Quotation5 = ls_Quotation5
                SRCls.Final5 = ""
                SRCls.GroupType = ls_GroupType

                i = clsSupplierRecommendationDB.UpdateDetail(SRCls, pUser, pErr)

                If i = 0 Then
                    clsSupplierRecommendationDB.InsertDetail(SRCls, pUser, pErr)
                End If
            Next
        End If
    End Sub

    Private Function ConvertToFormatDate_yyyyMMdd(ByVal pDate As String) As String
        Dim retVal As String = "1900-01-01"
        Dim pYear As String = Split(pDate, "-")(0)
        Dim pMonth As String = Strings.Right("0" & Split(pDate, "-")(1), 2)
        Dim pDay As String = Strings.Right("0" & Split(pDate, "-")(2), 2)

        retVal = pYear & "-" & pMonth & "-" & pDay

        Return retVal
    End Function

    Private Sub up_SaveHeader(Optional ByRef pErr As String = "")
        Dim Errmsg As String = ""
        Dim ls_SRNumber As String = ""
        Dim SRCls As New clsSupplierRecommendation
        Dim ds As New DataSet
        Dim ls_Bln As String
        Dim li_Bln As Integer = Format(Now, "MM")
        Dim li_Year As Integer = Format(Now, "yyyy")

        If txtSRNumber.Text = "--NEW--" Then
            ds = clsSupplierRecommendationDB.GetMaxSRNo(li_Bln, li_Year, Errmsg)
            If ds.Tables(0).Rows.Count > 0 Then
                ls_Bln = uf_ConvertMonth(Format(Now, "MM"))

                ls_SRNumber = "IAMI/COST/SR/" & ls_Bln & "/" & Format(Now, "yy") & "/" & ds.Tables(0).Rows(0)("SR_Number")

            End If

            SRCls.Revision = "0"

        End If
        Dim i As Integer

        If ls_SRNumber <> "" Then
            ls_VSRNumber = ls_SRNumber
            vSRNumber = ls_SRNumber
            SRCls.SRNumber = ls_SRNumber
            SRCls.SRDate = Format(dtSRDate.Value, "yyyy-MM-dd")
            SRCls.SRStatus = "0"

            SRCls.Notes = memoNote.Text
            SRCls.Consideration = memoConsi.Text
            SRCls.CPNumber = cboCPNumber.Text
            SRCls.SupplierRecommend = cbSuppNo.Text


            i = clsSupplierRecommendationDB.UpdateHeader(SRCls, pUser, pErr)

            If i = 0 Then
                clsSupplierRecommendationDB.InsertHeader(SRCls, pUser, pErr)
            End If

            If pErr = "" Then
                txtSRNumber.Text = ls_SRNumber
                gs_SRNumber = ls_SRNumber

            End If
        End If

    End Sub

    Private Sub up_Submit()
        Dim Errmsg As String = ""
        Dim SRCls As New clsSupplierRecommendation
        SRCls.CPNumber = cboCPNumber.Text
        SRCls.Revision = gs_SRRevNo
        SRCls.SRNumber = gs_SRNumber

        clsSupplierRecommendationDB.SubmitData(SRCls, pUser, Errmsg)
    End Sub

    Private Sub up_Approval()
        Dim Errmsg As String = ""
        Dim SRCls As New clsSupplierRecommendation
        SRCls.Notes = txtApprovalNote.Text
        SRCls.Revision = txtRev.Text
        SRCls.SRNumber = txtSRNumber.Text

        clsSupplierRecommendationDB.Approval(SRCls, pUser, Errmsg)

        If Errmsg = "" Then
            cbApprove.JSProperties("cpMessage") = "Data Has Been Approved Successfully"
        Else
            cbApprove.JSProperties("cpMessage") = Errmsg
        End If

    End Sub

    Private Sub up_UpdateHeader(pSRNo As String, pSRStatus As String, Optional ByRef pErr As String = "")
        Dim Errmsg As String = ""
        Dim SRCls As New clsSupplierRecommendation

        SRCls.SRNumber = pSRNo
        SRCls.Revision = gs_SRRevNo
        SRCls.SRStatus = pSRStatus
        SRCls.SRDate = Format(dtSRDate.Value, "yyyy-MM-dd")
        SRCls.SRStatus = pSRStatus

        SRCls.Notes = memoNote.Text
        SRCls.Consideration = memoConsi.Text
        SRCls.CPNumber = cboCPNumber.Text
        SRCls.SupplierRecommend = cbSuppNo.Text


        ls_VSRNumber = pSRNo

        'clsCostEstimationDB.UpdateHeader(CECls, pUser, pErr)
        Dim i As Integer

        i = clsSupplierRecommendationDB.UpdateHeader(SRCls, pUser, pErr)

        'If i = 0 Then
        'clsCostEstimationDB.InsertHeader(CECls, pUser, pErr)
        'End If

    End Sub

    Private Sub up_QDC()
        'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/ViewPRAcceptance.aspx")
        Session("SRNumber") = txtSRNumber.Text
        Session("Revision") = txtRev.Text
        Session("CPNumber") = cboCPNumber.Text
        Session("SupplierRecommend") = cbSuppNo.Text
        Session("Consideration") = memoConsi.Text
        Session("Notes") = memoNote.Text
        Session("Status") = gs_Status
        Response.Redirect("~/QDCComparisonDetailApproval.aspx")

    End Sub

    Private Sub up_FillComboSupplier(pCPNo As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsSupplierRecommendationDB.GetDataSupplierRecommendation(pCPNo, pErr)
        If pErr = "" Then

            cbSuppNo.DataSource = ds.Tables(0)
            cbSuppNo.DataBind()
        End If
    End Sub

    Private Sub up_Approve()
        Dim SRApproval As New clsSupplierRecommendationApproval
        Dim MsgErr As String = ""
        Dim i As Integer

        SRApproval.SRNumber = txtSRNumber.Text
        SRApproval.ApprovalNote = txtApprovalNote.Text
        SRApproval.Revision = txtRev.Text
       

        clsSupplierRecommendationApprovalDB.ApprovePRFull(SRApproval, pUser, MsgErr)

        If MsgErr = "" Then
            cbApprove.JSProperties("cpMessage") = "Data Has Been Approved Successfully"
        Else
            cbApprove.JSProperties("cpMessage") = MsgErr
        End If
    End Sub

    Private Sub up_Reject()
        Dim SRApproval As New clsSupplierRecommendationApproval
        Dim MsgErr As String = ""
        Dim i As Integer

        SRApproval.SRNumber = txtSRNumber.Text
        SRApproval.ApprovalNote = txtApprovalNote.Text
        i = clsSupplierRecommendationApprovalDB.Reject(SRApproval, pUser, MsgErr)
        If MsgErr = "" Then
            cbReject.JSProperties("cpMessage") = "Data Has Been Reject Successfully"
        Else
            cbReject.JSProperties("cpMessage") = MsgErr
        End If
    End Sub
#End Region

#Region "EVENTS"
    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        'Non-Editable
        e.Cell.BackColor = Color.LemonChiffon
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")

    End Sub

    Private Sub Grid_Delivery_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid_Delivery.HtmlDataCellPrepared
        'Non-Editablee
        e.Cell.BackColor = Color.LemonChiffon
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")

    End Sub

    Private Sub Grid_Quality_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid_Quality.HtmlDataCellPrepared
        'Non-Editable
        e.Cell.BackColor = Color.LemonChiffon
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")

    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|E060", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnReject.SetEnabled(false);" & vbCrLf & _
                          "btnApprove.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, txtSRNumber)
            End Try
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "SUPPLIER RECOMMENDATION APPROVAL DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E060")

        If AuthUpdate = False Then
            btnApprove.Enabled = False
            btnReject.Enabled = False
        End If

        '  Dim dDate As Date
        Dim SRNumber As String = ""
        Dim Revision As Integer
        Dim CPNumber As String = ""

        Dim Data As String = ""

        gs_Message = ""

        If Request.QueryString("ID") <> "" Then
            SRNumber = Split(Request.QueryString("ID"), "|")(0)
            Revision = Split(Request.QueryString("ID"), "|")(1)
            CPNumber = Split(Request.QueryString("ID"), "|")(2)
        End If

        If Not Page.IsPostBack Then

            If SRNumber = "" Then
                gs_SRNumber = ""
                txtSRNumber.Text = "--NEW--"
                gs_SRNo = "--NEW--"
                FillComboCPNumber()
                cbDraf.JSProperties("cpStatus") = ""
                gs_Status = ""
                dtSRDate.Value = Now
                txtFlag.Text = 0
            Else
                gs_SRNo = SRNumber
                gs_SRNumber = SRNumber
                txtSRNumber.Text = SRNumber
                up_GridLoadSR(SRNumber, Revision)
                up_FillComboSupplier(CPNumber)
            End If
            'up_GridLoad(CENumber)
        End If

        cbDraf.JSProperties("cpMessage") = ""
        cbDraf.JSProperties("cpCP") = CPNumber
        cbApprove.JSProperties("cpMessage") = ""
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/SupplierRecomendApproval.aspx")
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        If e.UpdateValues.Count = 0 Then
            gs_Message = "Data is not found"
            Exit Sub
        End If
        Dim errmsg As String = ""
        If gs_SRNumber = "" Then
            up_SaveHeader(errmsg)
        Else
            vSRNumber = gs_SRNumber
        End If

        If errmsg = "" Then
            up_SaveDetail(sender, e, errmsg)
        End If

        If errmsg = "" Then
            gs_Message = ""
        Else
            gs_Message = errmsg
        End If
        Grid.EndUpdate()
    End Sub

    Private Sub Grid_Delivery_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid_Delivery.BatchUpdate
        If e.UpdateValues.Count = 0 Then
            gs_Message = "Data is not found"
            Exit Sub
        End If
        Dim errmsg As String = ""
        If gs_SRNumber = "" Then
            up_SaveHeader(errmsg)
        Else
            vSRNumber = gs_SRNumber
        End If

        If errmsg = "" Then
            up_SaveDetail_Delivery(sender, e, errmsg)
        End If

        If errmsg = "" Then
            gs_Message = ""
        Else
            gs_Message = errmsg
        End If
        Grid_Delivery.EndUpdate()
    End Sub

    Private Sub Grid_Quality_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid_Quality.BatchUpdate
        If e.UpdateValues.Count = 0 Then
            gs_Message = "Data is not found"
            Exit Sub
        End If
        Dim errmsg As String = ""
        If gs_SRNumber = "" Then
            up_SaveHeader(errmsg)
        Else
            vSRNumber = gs_SRNumber
        End If

        If errmsg = "" Then
            up_SaveDetail_Quality(sender, e, errmsg)
        End If

        If errmsg = "" Then
            gs_Message = ""
        Else
            gs_Message = errmsg
        End If
        Grid_Delivery.EndUpdate()
    End Sub

    Protected Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        'Grid.CancelEdit()
    End Sub

    Protected Sub Grid_Delivery_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid_Delivery.RowUpdating
        e.Cancel = True
        'Grid.CancelEdit()
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback

        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Dim pCPNo As String
        If Split(e.Parameters, "|")(1) <> "" Then
            pCPNo = Split(e.Parameters, "|")(3)
        End If

        Dim pRev As Integer

        'If txtRev.Text = "" Then pRev = 0
        If Split(e.Parameters, "|")(1) = "" Then
            pRev = 0
        Else
            pRev = Split(e.Parameters, "|")(2)
        End If


        If pfunction = "gridload" Then

            Dim headerCaption As String = ""
            Grid.JSProperties("cpType") = ""
            Grid.JSProperties("cpMessage") = ""

            Try
                up_GridLoad(pCPNo, "00")

                'SET HEADER CAPTION (SUPPLIER)
                '#Initialize
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("Supplier1").Visible = True
                Grid.Columns("Supplier2").Visible = True
                Grid.Columns("Supplier3").Visible = True
                Grid.Columns("Supplier4").Visible = True
                Grid.Columns("Supplier5").Visible = True

                '#Set by condition
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid.Columns("Supplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid.Columns("Supplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid.Columns("Supplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid.Columns("Supplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid.Columns("Supplier5").Visible = False
                End Try





            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
            End Try


        ElseIf pfunction = "draft" Then
            '    'If gs_CENumber <> "" And gs_CENumber <> "ALL" Then
            up_GridLoadSR(gs_SRNumber, pRev)
            'End If
        ElseIf pfunction = "view" Then
            Try
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("Supplier1").Visible = True
                Grid.Columns("Supplier2").Visible = True
                Grid.Columns("Supplier3").Visible = True
                Grid.Columns("Supplier4").Visible = True
                Grid.Columns("Supplier5").Visible = True

                Dim headerCaption As String = ""
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid.Columns("Supplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid.Columns("Supplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid.Columns("Supplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid.Columns("Supplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid.Columns("Supplier5").Visible = False
                End Try





            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
            End Try
        End If


    End Sub

    Private Sub Grid_Delivery_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid_Delivery.CustomCallback

        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Dim pCPNo As String
        If Split(e.Parameters, "|")(1) <> "" Then
            pCPNo = Split(e.Parameters, "|")(3)
        End If

        Dim pRev As Integer

        'If txtRev.Text = "" Then pRev = 0
        If Split(e.Parameters, "|")(1) = "" Then
            pRev = 0
        Else
            pRev = Split(e.Parameters, "|")(2)
        End If


        If pfunction = "gridload" Then

            Dim headerCaption As String = ""
            Grid_Delivery.JSProperties("cpType") = ""
            Grid_Delivery.JSProperties("cpMessage") = ""

            Try
                up_GridLoad(pCPNo, "00")

                'SET HEADER CAPTION (SUPPLIER)
                '#Initialize
                Grid_Delivery.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid_Delivery.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid_Delivery.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid_Delivery.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid_Delivery.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid_Delivery.Columns("Supplier1").Visible = True
                Grid_Delivery.Columns("Supplier2").Visible = True
                Grid_Delivery.Columns("Supplier3").Visible = True
                Grid_Delivery.Columns("Supplier4").Visible = True
                Grid_Delivery.Columns("Supplier5").Visible = True

                '#Set by condition
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption1") = headerCaption : Grid_Delivery.Columns("Supplier1").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid_Delivery.Columns("Supplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption2") = headerCaption : Grid_Delivery.Columns("Supplier2").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid_Delivery.Columns("Supplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption3") = headerCaption : Grid_Delivery.Columns("Supplier3").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid_Delivery.Columns("Supplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption4") = headerCaption : Grid_Delivery.Columns("Supplier4").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid_Delivery.Columns("Supplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption5") = headerCaption : Grid_Delivery.Columns("Supplier5").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid_Delivery.Columns("Supplier5").Visible = False
                End Try





            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid_Delivery)
            End Try


        ElseIf pfunction = "draft" Then
            '    'If gs_CENumber <> "" And gs_CENumber <> "ALL" Then
            up_GridLoadSR(gs_SRNumber, pRev)
        ElseIf pfunction = "view" Then

            Dim headerCaption As String = ""
            Grid_Delivery.JSProperties("cpType") = ""
            Grid_Delivery.JSProperties("cpMessage") = ""

            Try

                'SET HEADER CAPTION (SUPPLIER)
                '#Initialize
                Grid_Delivery.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid_Delivery.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid_Delivery.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid_Delivery.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid_Delivery.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid_Delivery.Columns("Supplier1").Visible = True
                Grid_Delivery.Columns("Supplier2").Visible = True
                Grid_Delivery.Columns("Supplier3").Visible = True
                Grid_Delivery.Columns("Supplier4").Visible = True
                Grid_Delivery.Columns("Supplier5").Visible = True

                '#Set by condition
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption1") = headerCaption : Grid_Delivery.Columns("Supplier1").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid_Delivery.Columns("Supplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption2") = headerCaption : Grid_Delivery.Columns("Supplier2").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid_Delivery.Columns("Supplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption3") = headerCaption : Grid_Delivery.Columns("Supplier3").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid_Delivery.Columns("Supplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption4") = headerCaption : Grid_Delivery.Columns("Supplier4").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid_Delivery.Columns("Supplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid_Delivery.JSProperties("cpHeaderCaption5") = headerCaption : Grid_Delivery.Columns("Supplier5").Visible = True
                Catch ex As Exception
                    Grid_Delivery.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid_Delivery.Columns("Supplier5").Visible = False
                End Try





            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid_Delivery)
            End Try
        End If


    End Sub

    Private Sub Grid_Quality_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid_Quality.CustomCallback

        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Dim pCPNo As String
        If Split(e.Parameters, "|")(1) <> "" Then
            pCPNo = Split(e.Parameters, "|")(3)
        End If

        Dim pRev As Integer

        'If txtRev.Text = "" Then pRev = 0
        If Split(e.Parameters, "|")(1) = "" Then
            pRev = 0
        Else
            pRev = Split(e.Parameters, "|")(2)
        End If


        If pfunction = "gridload" Then

            Dim headerCaption As String = ""
            Grid_Quality.JSProperties("cpType") = ""
            Grid_Quality.JSProperties("cpMessage") = ""

            Try
                up_GridLoad(pCPNo, "00")

                'SET HEADER CAPTION (SUPPLIER)
                '#Initialize
                Grid_Quality.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid_Quality.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid_Quality.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid_Quality.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid_Quality.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid_Quality.Columns("Supplier1").Visible = True
                Grid_Quality.Columns("Supplier2").Visible = True
                Grid_Quality.Columns("Supplier3").Visible = True
                Grid_Quality.Columns("Supplier4").Visible = True
                Grid_Quality.Columns("Supplier5").Visible = True

                '#Set by condition
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption1") = headerCaption : Grid_Quality.Columns("Supplier1").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid_Quality.Columns("Supplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption2") = headerCaption : Grid_Quality.Columns("Supplier2").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid_Quality.Columns("Supplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption3") = headerCaption : Grid_Quality.Columns("Supplier3").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid_Quality.Columns("Supplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption4") = headerCaption : Grid_Quality.Columns("Supplier4").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid_Quality.Columns("Supplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption5") = headerCaption : Grid_Quality.Columns("Supplier5").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid_Quality.Columns("Supplier5").Visible = False
                End Try





            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid_Quality)
            End Try


        ElseIf pfunction = "draft" Then
            '    'If gs_CENumber <> "" And gs_CENumber <> "ALL" Then
            up_GridLoadSR(gs_SRNumber, pRev)
            'End If
        ElseIf pfunction = "view" Then

            Dim headerCaption As String = ""
            Grid_Quality.JSProperties("cpType") = ""
            Grid_Quality.JSProperties("cpMessage") = ""

            Try
              
                'SET HEADER CAPTION (SUPPLIER)
                '#Initialize
                Grid_Quality.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid_Quality.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid_Quality.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid_Quality.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid_Quality.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid_Quality.Columns("Supplier1").Visible = True
                Grid_Quality.Columns("Supplier2").Visible = True
                Grid_Quality.Columns("Supplier3").Visible = True
                Grid_Quality.Columns("Supplier4").Visible = True
                Grid_Quality.Columns("Supplier5").Visible = True

                '#Set by condition
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption1") = headerCaption : Grid_Quality.Columns("Supplier1").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid_Quality.Columns("Supplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption2") = headerCaption : Grid_Quality.Columns("Supplier2").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid_Quality.Columns("Supplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption3") = headerCaption : Grid_Quality.Columns("Supplier3").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid_Quality.Columns("Supplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption4") = headerCaption : Grid_Quality.Columns("Supplier4").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid_Quality.Columns("Supplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid_Quality.JSProperties("cpHeaderCaption5") = headerCaption : Grid_Quality.Columns("Supplier5").Visible = True
                Catch ex As Exception
                    Grid_Quality.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid_Quality.Columns("Supplier5").Visible = False
                End Try





            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid_Quality)
            End Try
        End If


    End Sub

    Private Sub cbGrid_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbGrid.Callback
        Dim pfunction As String = Split(e.Parameter, "|")(0)
        Dim pCPNo As String = Split(e.Parameter, "|")(3)
        Dim pRev As Integer

        If Split(e.Parameter, "|")(2) = "" Then pRev = 0

        up_GridLoadSR(gs_SRNumber, pRev)

    End Sub

    Private Sub cbDraf_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDraf.Callback
        'up_UploadFile()

        If gs_Message = "" Then

            cbDraf.JSProperties("cpMessage") = "Draft data saved successfull"
            If gs_SRNumber = "" Then
                cbDraf.JSProperties("cpSRNo") = ls_VSRNumber
            Else
                cbDraf.JSProperties("cpSRNo") = gs_SRNumber
            End If

            If IsNothing(gs_SRRevNo) Then
                cbDraf.JSProperties("cpRevision") = 0
            Else
                cbDraf.JSProperties("cpRevision") = gs_SRRevNo
            End If

            If gs_Status = "" Then
                cbDraf.JSProperties("cpStatus") = 0
            Else
                cbDraf.JSProperties("cpStatus") = gs_Status
            End If
        End If
    End Sub

    Private Sub cbSubmit_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSubmit.Callback
        up_Submit()
    End Sub

    Private Sub Grid_Delivery_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid_Delivery.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_Quality_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid_Quality.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub cboCPNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCPNumber.Callback
        Try
            Call FillComboCPNumber()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboCPNumber)
        End Try
    End Sub

    Private Sub cbSuppNo_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbSuppNo.Callback
        'Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pCPNo As String = e.Parameter

        up_FillComboSupplier(pCPNo)
    End Sub

    Private Sub btnQCD_Click(sender As Object, e As System.EventArgs) Handles btnQCD.Click
        up_QCD()
    End Sub

#End Region

    Private Function GetJumlahSupplier(pCPNumber As String)
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "exec sp_SupplierRecommendation_GetJumlahSupplier '" & pCPNumber & "'", "", "", ErrMsg)

        If ErrMsg = "" Then
            retVal = ds.Tables(0).Rows(0).Item("JumData").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboCPNumber)
        End If

        Return retVal
    End Function
    Private Sub up_Print()
        'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/ViewPRAcceptance.aspx")
       

        Session("SRNumber") = txtSRNumber.Text
        Session("Revision") = txtRev.Text
        Session("CPNumber") = cboCPNumber.Text
        Session("Action") = "2"
        Session("Jumlah") = GetJumlahSupplier(cboCPNumber.Text)
        Response.Redirect("~/ViewSupplierRecommendation.aspx")

    End Sub

    Private Sub up_QCD()
        'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/ViewPRAcceptance.aspx")


        Session("SRNumber") = txtSRNumber.Text
        Session("Revision") = txtRev.Text
        Session("CPNumber") = cboCPNumber.Text
        Session("Action") = "2"
        Session("Jumlah") = GetJumlahSupplier(cboCPNumber.Text)
        Response.Redirect("~/ViewSupplierRecommendationQCDApproval.aspx")

    End Sub


    Private Sub cbApprove_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbApprove.Callback
        up_Approval()
    End Sub

    Private Sub cbReject_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbReject.Callback
        up_Reject()
    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        up_Print()
    End Sub
End Class
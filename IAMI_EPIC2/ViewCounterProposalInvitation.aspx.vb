﻿Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewCounterProposalInvitation
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"
    Private Sub up_LoadReport(pCPNumber As String, pSupplier As String, pRev As Integer)
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptCP_TenderInvitation

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_CP_CounterProposalReportInvitation '" & pCPNumber & "' ,'" & pSupplier & "', " & pRev & ""

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "CounterProposalTender_" & Format(CDate(Now), "yyyyMMdd_HHmmss")

                'Dim sql2 As String
                'Dim ds2 As New DataSet
                'sql2 = "exec sp_CostEstimationResume_Report '" & pCENumber & "' , " & pRev & ""

                'Dim da2 As New SqlDataAdapter(sql2, con)
                'da2.Fill(ds2)
                'Report.XrSubreport1.Report.DataSource = ds2
                'Report.XrSubreport1.Report.ApplyFiltering()
                ASPxDocumentViewer1.Report = Report
                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    
#End Region

#Region "EVENTS"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim CP_Number As String = ""
        Dim Supplier As String = ""
        Dim Rev As String = ""
       
        CP_Number = Split(Request.QueryString("ID"), "|")(0)
        Rev = Split(Request.QueryString("ID"), "|")(1)
        Supplier = Split(Request.QueryString("ID"), "|")(2)
       



        up_LoadReport(CP_Number, Supplier, Rev)
    End Sub


#End Region

End Class
﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class RFQList_aspx
    Inherits System.Web.UI.Page
    Dim RFQNo As String = ""
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cpMessage") = ErrMsg
        Grid.JSProperties("cpType") = msgType
        Grid.JSProperties("cpVal") = pVal
    End Sub


    Private Sub up_ExcelGrid()
        Try
         
            up_GridLoad(gs_DateFrom, gs_DateTo, gs_RFQNo, gs_FilterSupplier)

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=RFQList" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            gs_Message = ex.Message
        End Try
    End Sub

    Private Sub up_GridHeader()
        Dim ErrMsg As String = ""
        Dim pDate1 As String = ""
        Dim pDate2 As String = ""

        pDate1 = Format(dtDateFrom.Value, "yyyy-MM-dd")
        pDate2 = Format(dtDateTo.Value, "yyyy-MM-dd")

        For i = 0 To 1
            Grid.Columns.Item(14 + i).Visible = False
        Next

        Dim ds As New DataSet
        ds = clsRFQApprovalDB.GetHeaderApproval(pDate1, pDate2, pUser, ErrMsg)

        If ErrMsg = "" Then

            For i = 0 To ds.Tables(0).Rows.Count - 1
                Grid.Columns.Item(15 + i).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                Grid.Columns.Item(15 + i).Visible = True
            Next

        End If
    End Sub

    Private Sub up_GridLoad(pDateFrom As String, pDateTo As String, pRFQNo As String, pSupplier As String)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet

        If pRFQNo = "ALL" Then
            pRFQNo = ""
        End If

        up_GridHeader()
        ds = clsRFQDB.GetList(pUser, pDateFrom, pDateTo, pRFQNo,pSupplier, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()

            If ds.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cpMessage") = "Grid Load Data Success"
            Else
                Grid.JSProperties("cpMessage") = ""
            End If

        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_FillComboRFQ(pDate1 As Date, pDate2 As Date)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim dtfrom As String = Format(pDate1, "yyyy-MM-dd")
        Dim dtto As String = Format(pDate2, "yyyy-MM-dd")

        ds = clsRFQDB.GetComboData(pUser,dtfrom, dtto, pErr)
        If pErr = "" Then
            cboRFQNumber.DataSource = ds
            cboRFQNumber.DataBind()
        End If
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|C010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAdd.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAdd, btnAdd.GetType(), "btnAdd", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
   
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        Dim dfrom As Date
        dfrom = Year(Now) & "-" & Month(Now) & "-01"
        dtDateFrom.Value = dfrom
        dtDateTo.Value = Now
        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboRFQNumber)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("C010 ")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "C010 ")
        Dim dfrom As Date

        RFQNo = Request.QueryString("Par")

        gs_Message = ""

        'If (Not Page.IsPostBack) Then
        If RFQNo = "" Then
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dtDateFrom.Value = dfrom
            dtDateTo.Value = Now
            cboRFQNumber.SelectedIndex = 0
			cboSupplierCode.SelectedIndex = 0
        Else
            If gs_RFQNoFilter = "ALL" Then
                up_GridLoad(gs_DateFrom, gs_DateTo, "", cboSupplierCode.Value)
            Else
                up_GridLoad(gs_DateFrom, gs_DateTo, gs_RFQNo, gs_FilterSupplier)
            End If

            dtDateFrom.Value = gs_dtFromBack
            dtDateTo.Value = gs_dtToBack
        End If

        'End If
    End Sub

    'Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
    '    up_GridLoad()

    'End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        gs_RFQNo = cboRFQNumber.SelectedIndex
        gs_dtFromBack = dtDateFrom.Value
        gs_dtToBack = dtDateTo.Value

        gs_RFQNo1 = ""
        gs_RFQNo2 = ""
        gs_RFQNo3 = ""
        gs_RFQNo4 = ""
        gs_RFQNo5 = ""
        gs_SetRFQNo = ""
        Response.Redirect("~/RFQCreate.aspx")
    End Sub

    Private Sub cboRFQNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboRFQNumber.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pDateFrom As Date

        If Split(e.Parameter, "|")(1) <> "" Then
            pDateFrom = Split(e.Parameter, "|")(1)
        Else

            pDateFrom = Year(Now) & "-" & Month(Now) & "-01"
            dtDateFrom.Value = pDateFrom
        End If
        Dim pDateTo As Date
        If Split(e.Parameter, "|")(2) <> "" Then
            pDateTo = Split(e.Parameter, "|")(2)
        Else
            pDateTo = Now
            dtDateTo.Value = pDateTo
        End If

        up_FillComboRFQ(pDateFrom, pDateTo)
        If RFQNo <> "" And pFunction = "load" Then
            cboRFQNumber.SelectedIndex = gs_RFQNoIndex
            RFQNo = ""
        Else
            If cboRFQNumber.Items.Count > 1 Then
                cboRFQNumber.SelectedIndex = 0
            End If
        End If
    End Sub
    Private Sub Grid_CustomButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonEventArgs) Handles Grid.CustomButtonInitialize
        If e.Column.Grid.GetRowValues(e.VisibleIndex, "RFQ_Set") = "" Then
            e.Visible = DevExpress.Utils.DefaultBoolean.False
        Else
            e.Visible = DevExpress.Utils.DefaultBoolean.True
        End If
    End Sub
    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        up_GridLoad(gs_DateFrom, gs_DateTo, gs_RFQNo, gs_FilterSupplier)
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pDateFrom As Date = Split(e.Parameters, "|")(1)
        Dim pDateTo As Date = Split(e.Parameters, "|")(2)
        Dim pRFQNo As String = Split(e.Parameters, "|")(3)
		Dim pSupplier As String = Split(e.Parameters, "|")(4)


        Dim vDateFrom As String = Format(pDateFrom, "yyyy-MM-dd")
        Dim vDateTo As String = Format(pDateTo, "yyyy-MM-dd")

        up_FillComboRFQ(pDateFrom, pDateTo)

        gs_RFQNoFilter = pRFQNo
        gs_RFQNo = pRFQNo
		gs_FilterSupplier = pSupplier
        gs_RFQNoIndex = cboRFQNumber.SelectedIndex
        gs_dtFromBack = pDateFrom
        gs_dtToBack = pDateTo

        gs_DateFrom = vDateFrom
        gs_DateTo = vDateTo

        up_GridLoad(vDateFrom, vDateTo, pRFQNo, pSupplier)

    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        'Non-Editable
        If e.GetValue("RFQ_Set") <> "" Then
            'HEADER
            e.Cell.BackColor = Color.Silver
        Else
            'DETAIL
            e.Cell.BackColor = Color.LemonChiffon
        End If
    End Sub

    Private Sub Grid_HtmlRowPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles Grid.HtmlRowPrepared
        If IsNothing(e.GetValue("RFQ_Set")) = False Then
            If e.GetValue("RFQ_Set") = "" Then
                e.Row.BackColor = Color.LemonChiffon
            End If
        End If
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        Dim ErrMsg As String = ""
        up_ExcelGrid()
    End Sub
End Class
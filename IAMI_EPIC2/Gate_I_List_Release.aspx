﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="Gate_I_List_Release.aspx.vb" Inherits="IAMI_EPIC2.Gate_I_List_Release" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
     var currentColumnName;
     var dataExist;
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cpRelease =="disabled"){
                btnDraft.SetEnabled(false);
                btnSubmitKSH.SetEnabled(false);
            }
            else{
                btnDraft.SetEnabled(true);
                btnSubmitKSH.SetEnabled(true);
            }



            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }

            Grid.CancelEdit();
           
        };

         

        function SaveData(s, e) {
//            cbkValid.PerformCallback('save');
            Grid.UpdateEdit()
        }

        function disableenablebuttonsubmit(_val) {
        alert('adsds');
            if (_val = '1') {
                btnSave.SetEnabled(false)
            } else {
                btnSave.SetEnabled(true)
             }
        }

          function OnUpdateCheckedChanged(s, e) {
            if (s.GetValue() == -1) s.SetValue(1);
            Grid.SetFocusedRowIndex(-1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "Status_Drawing", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "Status_Drawing", s.GetValue());
                }                
            }
        }

//        function gv_OnCustomButtonClick(s, e) {
//            if(e.buttonID == 'Submit'){ 
//                var key =  Grid.GetRowKey(e.visibleIndex);
//                var keysplit =  key.split("|");
//                alert(key);
//                window.location.href='GetCheckSheetView.aspx?projecttype=' +  keysplit[0] +'&projectid=' + keysplit[1] + '&groupid=' + keysplit[2] + '&commodity=' + keysplit[3] + '&groupcommodity=' + keysplit[4] + '&buyer=' + keysplit[5] + '&pagetype=release'
//             }
//             else if(e.buttonID == 'Confirm'){ 
//                var key =  Grid.GetRowKey(e.visibleIndex);
//                var keysplit =  key.split("|");
//                alert(key);
//                window.location.href='GetCheckSheetConfirm.aspx?projecttype=' +  keysplit[0] +'&projectid=' + keysplit[1] + '&groupid=' + keysplit[2] + '&commodity=' + keysplit[3] + '&groupcommodity=' + keysplit[4] + '&buyer=' + keysplit[5] + '&pagetype=release'
//             }
//             else if(e.buttonID == 'Approve'){ 
//                var key =  Grid.GetRowKey(e.visibleIndex);
//                var keysplit =  key.split("|");
//                alert(key);
//                window.location.href='GetCheckSheetView.aspx?projecttype=' +  keysplit[0] +'&projectid=' + keysplit[1] + '&groupid=' + keysplit[2] + '&commodity=' + keysplit[3] + '&groupcommodity=' + keysplit[4] + '&buyer=' + keysplit[5] + '&pagetype=release'
//             }
//              else if(e.buttonID == 'AfterApprove'){ 
//             alert(key);
//                var key =  Grid.GetRowKey(e.visibleIndex);
//                var keysplit =  key.split("|");
//                window.location.href='GetCheckSheetView.aspx?projecttype=' +  keysplit[0] +'&projectid=' + keysplit[1] + '&groupid=' + keysplit[2] + '&commodity=' + keysplit[3] + '&groupcommodity=' + keysplit[4] + '&buyer=' + keysplit[5] + '&pagetype=release'
//             }
//        }

        function OnBatchEditStartEditing(s, e) {

          
            //var check = "";
//            if (dataExist == "1") {
//                // alert(dataExist);
//                e.cancel = true;
//                Grid.CancelEdit();
//                Grid.batchEditApi.EndEdit();
//                return false;
//            }
            
//            var keyIndex = s.GetColumnByField("AllowCheck").index;
//            var check = e.rowValues[keyIndex].value;

            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName != "KSH_Date" && currentColumnName != "Start_Time" && currentColumnName != "Finish_Time" && currentColumnName != "Meeting_Room" && currentColumnName != "AllowCheck") {
                //check = parseFloat(Grid.batchEditApi.GetCellValue(i, "AllowCheck"));
                e.cancel = true;
            }

            if (btnSubmitKSH.GetEnabled() == false && currentColumnName != "AllowCheck") {

                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        }

        function OnBatchEditEndEditing(s, e) {
            window.setTimeout(function () {
                var starttime = s.batchEditApi.GetCellValue(e.visibleIndex, "Start_Time");
                var endtime = s.batchEditApi.GetCellValue(e.visibleIndex, "Finish_Time");
                var date =  s.batchEditApi.GetCellValue(e.visibleIndex, "KSH_Date");
               // alert(date + " " + starttime);
                var dt = date;
                var yy = dt.getFullYear();
                var mm = dt.getMonth();
                var dd = dt.getDate();
                var dateString =  yy + '-' + mm + '-' + dd;
                var starttimeString = starttime.getHours() + ':' + starttime.getMinutes() + ':00';
                var endtimeString = endtime.getHours() + ':' + endtime.getMinutes() + ':00';
                
                if ((currentColumnName == "Start_Time") || (currentColumnName == "Finish_Time") ) {
                    var startTime1 = new Date(dateString + " " + starttimeString); 
                    var endTime2 = new Date(dateString + " " + endtimeString);
                    var difference = endTime2.getTime() - startTime1.getTime(); // This will give difference in milliseconds
                    var resultInMinutes = Math.round(difference / 60000);
                    s.batchEditApi.SetCellValue(e.visibleIndex, "Duration", resultInMinutes);
                }
            }, 10);
        }

//        function cbInvitationEndCallback(s,e){
//            if (s.cpSubmit == "1") {
//                alert(s.cpSubmit);
//                dataExist = s.cpSubmit;
//            }
//            else{
//                alert("0");
//            }
//        }

        function DraftInvitationKSH(s, e) {
           // cbInvitation.PerformCallback();

            startIndex = 0;
            var InvitationDate;
            var StartTime;
            var FinishTime;
            var MeetingRoom ;
            var Duration;
            var check = "";
            var check2 ;
            var x = false;
            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                 check = Grid.batchEditApi.GetCellValue(i, "AllowCheck");
                 check2 = parseFloat(Grid.batchEditApi.GetCellValue(i, "AllowCheckTemp"));
                 //alert(check2);
                if (check == "1" ) {
                    x = true;
                   //InvitationDate = Grid.batchEditApi.GetCellValue(i, "KSH_Date");
                   MeetingRoom = Grid.batchEditApi.GetCellValue(i, "Meeting_Room");   
                   Grid.batchEditApi.SetCellValue(i,"StatusRelease","Draft");
                   Duration = parseFloat(Grid.batchEditApi.GetCellValue(i, "Duration")); 
                }
            }
                if (x == true) {
                     //alert(InvitationDate);
//                    var dt = InvitationDate;
//                    var yy = dt.getFullYear();
//                    var mm = dt.getMonth();
//                    var dd = dt.getDate();
//                    var dtInvDate = new Date(yy, mm, dd, 0, 0, 0);

//                    var dt2 = new Date();
//                    var yy = dt2.getFullYear();
//                    var mm = dt2.getMonth();
//                    var dd = dt2.getDate();
//                    var dtComp = new Date(yy, mm, dd, 0, 0, 0);

//                    if (dtInvDate <= dtComp) {

//                        toastr.warning("Date cannot be less than today!", "Warning");
//                        toastr.options.closeButton = false;
//                        toastr.options.debug = false;
//                        toastr.options.newestOnTop = false;
//                        toastr.options.progressBar = false;
//                        toastr.options.preventDuplicates = true;
//                        toastr.options.onclick = null;
//                        e.processOnServer = false;
//                        return false;
//                        //GridInvitation.batchEditApi.SetCellValue(i, "Invitation_Date", dtComp);
//                    }
                    
                    if (Duration < 0){
                        toastr.warning("Finish Time must greater than Start Time!", "Warning");
                        toastr.options.closeButton = false;
                        toastr.options.debug = false;
                        toastr.options.newestOnTop = false;
                        toastr.options.progressBar = false;
                        toastr.options.preventDuplicates = true;
                        toastr.options.onclick = null;
                        e.processOnServer = false;
                        return false;
                    }

                    if (MeetingRoom == '' || MeetingRoom == null) {
                        toastr.warning("Please Input Meeting Room!", "Warning");
                        toastr.options.closeButton = false;
                        toastr.options.debug = false;
                        toastr.options.newestOnTop = false;
                        toastr.options.progressBar = false;
                        toastr.options.preventDuplicates = true;
                        toastr.options.onclick = null;
                        e.processOnServer = false;
                        return false;
                    }


                    Grid.UpdateEdit();

                }
                else {
                        toastr.warning('Please select data before draft', 'Warning');
                        toastr.options.closeButton = false;
                        toastr.options.debug = false;
                        toastr.options.newestOnTop = false;
                        toastr.options.progressBar = false;
                        toastr.options.preventDuplicates = true;
                        toastr.options.onclick = null;
                        e.processOnServer = false;
                        return false;
                }
            

            //Grid.UpdateEdit();
            millisecondsToWait = 1000;
            setTimeout(function () {
                Grid.PerformCallback('load|');
                toastr.success("Data save successfully!", "Success");
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }, millisecondsToWait);
            //cbInvitation.PerformCallback('IsSubmitInvitation');
        }



        function SubmitInvitationKSH(s, e) {
           // cbInvitation.PerformCallback();
            startIndex = 0;
            var InvitationDate;
            var StartTime;
            var FinishTime;
            var MeetingRoom ;
             var Duration;
            var status;
            var check = "";
            var check2;
            var x = false;
            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                 check = parseFloat(Grid.batchEditApi.GetCellValue(i, "AllowCheck"));
                 check2 = parseFloat(Grid.batchEditApi.GetCellValue(i, "AllowCheckTemp"));
                 //alert(check2);
                if (check == "1" ) {
                    x = true;
                    //InvitationDate = Grid.batchEditApi.GetCellValue(i, "KSH_Date");
                   MeetingRoom = Grid.batchEditApi.GetCellValue(i, "Meeting_Room");   
                   Grid.batchEditApi.SetCellValue(i,"StatusRelease","Submit");
                   Duration = parseFloat(Grid.batchEditApi.GetCellValue(i, "Duration")); 
                }
            }
                if (x == true) {
                     //alert(InvitationDate);
//                    var dt = InvitationDate;
//                    var yy = dt.getFullYear();
//                    var mm = dt.getMonth();
//                    var dd = dt.getDate();
//                    var dtInvDate = new Date(yy, mm, dd, 0, 0, 0);

//                    var dt2 = new Date();
//                    var yy = dt2.getFullYear();
//                    var mm = dt2.getMonth();
//                    var dd = dt2.getDate();
//                    var dtComp = new Date(yy, mm, dd, 0, 0, 0);

//                    if (dtInvDate <= dtComp) {

//                        toastr.warning("Date cannot be less than today!", "Warning");
//                        toastr.options.closeButton = false;
//                        toastr.options.debug = false;
//                        toastr.options.newestOnTop = false;
//                        toastr.options.progressBar = false;
//                        toastr.options.preventDuplicates = true;
//                        toastr.options.onclick = null;
//                        e.processOnServer = false;
//                        return false;
//                        //GridInvitation.batchEditApi.SetCellValue(i, "Invitation_Date", dtComp);
//                    }

                    if (Duration < 0){
                        toastr.warning("Finish Time must greater than Start Time!", "Warning");
                        toastr.options.closeButton = false;
                        toastr.options.debug = false;
                        toastr.options.newestOnTop = false;
                        toastr.options.progressBar = false;
                        toastr.options.preventDuplicates = true;
                        toastr.options.onclick = null;
                        e.processOnServer = false;
                        return false;
                    }

                    if (MeetingRoom == '' || MeetingRoom == null) {
                        toastr.warning("Please Input Meeting Room!", "Warning");
                        toastr.options.closeButton = false;
                        toastr.options.debug = false;
                        toastr.options.newestOnTop = false;
                        toastr.options.progressBar = false;
                        toastr.options.preventDuplicates = true;
                        toastr.options.onclick = null;
                        e.processOnServer = false;
                        return false;
                    }
                    
                    var msg = confirm('Are you sure want to release this data ?');
                    if (msg == false) {
                        e.processOnServer = false;
                        return;
                    }

                    Grid.UpdateEdit();

                }
                else {
                        toastr.warning('Please select data before release', 'Warning');
                        toastr.options.closeButton = false;
                        toastr.options.debug = false;
                        toastr.options.newestOnTop = false;
                        toastr.options.progressBar = false;
                        toastr.options.preventDuplicates = true;
                        toastr.options.onclick = null;
                        e.processOnServer = false;
                        return false;
                }
            

            //Grid.UpdateEdit();
            millisecondsToWait = 1000;
            setTimeout(function () {
                Grid.PerformCallback('load|');
                toastr.success("Data save successfully!", "Success");
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }, millisecondsToWait);
           //btnSubmitKSH.SetEnabled(false);
            //cbInvitation.PerformCallback('IsSubmitInvitation');
        }


        
        function CheckedChanged(s, e) {

            //Grid.SetFocusedRowIndex(-1);
            if (s.GetValue() == -1) s.SetValue(1);

            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                var check2;
                check2 = parseFloat(Grid.batchEditApi.GetCellValue(i, "AllowCheckTemp"));
                
                if (Grid.batchEditApi.GetCellValue(i, "AllowCheck", false) != s.GetValue()) {
                    //if (check2=="0") {
                        Grid.batchEditApi.SetCellValue(i, "AllowCheck", s.GetValue());
                    //}
                }
            }
        }


        
    </script>
    <style>
        .statusBar
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="border: 1px solid black">
            <table style="width: 100%;">
                <tr style="height: 10px">
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Project Type">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--satu--%>
                        <dx:ASPxComboBox ID="cboProjectType" runat="server" ClientInstanceName="cboProjectType"
                            TabIndex="2" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboProject.PerformCallback('filter|' + cboProjectType.GetSelectedItem().GetColumnText(0));
                                cboGroup.PerformCallback('filter| ' );
                                cboCommodity.PerformCallback('filter| ' );
                               
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Group ID">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Dua--%>
                        <dx:ASPxComboBox ID="cboGroup" runat="server" ClientInstanceName="cboGroup" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Group_ID" ValueField="Group_ID"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboCommodity.PerformCallback('filter|' + cboGroup.GetSelectedItem().GetColumnText(0));
                             
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Group ID" FieldName="Group_ID" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                    </td>
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Sourcing Group">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Lima--%>
                        <dx:ASPxComboBox ID="cboGroupCommodity" runat="server" ClientInstanceName="cboGroupCommodity"
                            Font-Names="Segoe UI" TextField="Group_Comodity" ValueField="Group_Comodity"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px"
                            TabIndex="5">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                    
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Sourcing Group" FieldName="Group_Comodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                <tr style="height: 10px">
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Project ID">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                            Font-Names="Segoe UI" TextField="Project_Name" ValueField="Project_ID" TextFormatString="{1}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboGroup.PerformCallback('filter|' + cboProject.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Commodity">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Lima--%>
                        <dx:ASPxComboBox ID="cboCommodity" runat="server" ClientInstanceName="cboCommodity"
                            Font-Names="Segoe UI" TextField="Commodity" ValueField="Commodity" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                cboGroupCommodity.PerformCallback('filter|' + cboCommodity.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Commodity" FieldName="Commodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                </tr>
            </table>
            <table>
                <tr style="height: 50px">
                    <td colspan="14" style="width: 300px; padding: 10px 0px 0px 10px">
                        <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShowData" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp; &nbsp;
                           <dx:ASPxButton ID="btnDraft" runat="server" AutoPostBack="False" ClientInstanceName="btnDraft"
                                Font-Names="Segoe UI" Font-Size="9pt" Text="Draft" Theme="Default"
                                Width="80px" Height="25px">
                                <ClientSideEvents Click="DraftInvitationKSH" />
                            </dx:ASPxButton>
                        &nbsp; &nbsp;
                           <dx:ASPxButton ID="btnSubmitKSH" runat="server" AutoPostBack="False" ClientInstanceName="btnSubmitKSH"
                                Font-Names="Segoe UI" Font-Size="9pt" Text="Release" Theme="Default"
                                Width="80px" Height="25px">
                                <ClientSideEvents Click="SubmitInvitationKSH" />
                            </dx:ASPxButton>
                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div  style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" KeyFieldName="Project_Type;Project_ID; Group_ID; Commodity; Group_Comodity;Buyer"
            Theme="Office2010Black" Width="100%" Font-Names="Segoe UI" Font-Size="9pt">
            <ClientSideEvents EndCallback="OnEndCallback" BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing"  >
                                                    
            </ClientSideEvents>
            <Columns>
                <%--<dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" Width="150px">
                   <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="Submit" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>

                    </CustomButtons>
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="Confirm" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="Approve" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="AfterApprove" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dx:GridViewCommandColumn>--%>
                <dx:GridViewDataColumn FieldName="Link" Caption=" " VisibleIndex="0" >
                    <DataItemTemplate>
                            <dx:ASPxButton ID="linkButton" runat="server" Text="Detail"  OnClick="ASPxButton1_Click" RenderMode="Link">
                                
                            </dx:ASPxButton>
                    </DataItemTemplate>
                    <CellStyle HorizontalAlign ="Center"></CellStyle>
                </dx:GridViewDataColumn>
                <dx:GridViewDataCheckColumn Caption=" " VisibleIndex="1" Width="35px" 
                      FieldName="AllowCheck" Name="AllowCheck">
                        <Settings AllowAutoFilter="False" />
                       <PropertiesCheckEdit ValueChecked="1" ValueType="System.String" AllowGrayedByClick="false" ValueUnchecked="0" >
                       </PropertiesCheckEdit>

                        <HeaderCaptionTemplate>
                        <dx:ASPxCheckBox ID="checkBox" runat="server" ClientInstanceName="checkBox"  ClientSideEvents-CheckedChanged="CheckedChanged"  ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="" Font-Names="Segoe UI" Font-Size="8pt" ForeColor="White" > 
                        </dx:ASPxCheckBox>

                        </HeaderCaptionTemplate> 

                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataTextColumn Caption="Project ID" FieldName="Project_ID" VisibleIndex="2"
                    Width="120px">
                    <PropertiesTextEdit MaxLength="15" Width="115px" ClientInstanceName="Project_ID">
                        <Style HorizontalAlign="Left">
                            
                        </Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Project Name" FieldName="Project_Name" VisibleIndex="3"
                    Width="120px">
                    <PropertiesTextEdit MaxLength="15" Width="115px" ClientInstanceName="Project_Name">
                        <Style HorizontalAlign="Left">
                            
                        </Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Group_ID" Width="120px" Caption="Group" VisibleIndex="4">
                    <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Group_ID">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Commodity" FieldName="Commodity" VisibleIndex="5"
                    Width="120px">
                    <PropertiesTextEdit Width="350px" DisplayFormatInEditMode="True" ClientInstanceName="Commodity">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Sourcing Group" FieldName="Group_Comodity" VisibleIndex="6"
                    Width="150px">
                    <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="True" ClientInstanceName="Group_Comodity">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
              <%--   <dx:GridViewDataTextColumn Caption="Part No" FieldName="Part_No" VisibleIndex="7"
                    Width="100px">
                    <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="True" ClientInstanceName="Part_No">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>--%>
                 <dx:GridViewDataTextColumn Caption="Buyer" FieldName="Buyer" VisibleIndex="8"
                    Width="100px">
                    <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="False" ClientInstanceName="Buyer">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Date" VisibleIndex="9" Width="100px" FieldName="KSH_Date">
                       <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" EditFormat="Date" >
                       </PropertiesDateEdit>
                       <Settings AllowAutoFilter ="False" />
                       <CellStyle HorizontalAlign="Center">
                       </CellStyle>
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTimeEditColumn Caption="Start" VisibleIndex="10" Width="150px"
                      FieldName="Start_Time">
                      <PropertiesTimeEdit Width="80px" DisplayFormatString="HH:mm" EditFormatString="HH:mm">
                          <ButtonStyle Width="5px" Paddings-Padding="3px" HorizontalAlign="Center" VerticalAlign="Top">
<Paddings Padding="3px"></Paddings>
                          </ButtonStyle>
                      </PropertiesTimeEdit>
                      <Settings AllowAutoFilter ="False" />
                      
                      <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" 
                          VerticalAlign="Middle" >
<Paddings PaddingLeft="6px"></Paddings>
                      </HeaderStyle>
                      <CellStyle HorizontalAlign="Left">
                      </CellStyle>
                </dx:GridViewDataTimeEditColumn>
                <dx:GridViewDataTimeEditColumn Caption="Finish" VisibleIndex="11" Width="150px"
                      FieldName="Finish_Time">
                      <PropertiesTimeEdit Width="80px" DisplayFormatString="HH:mm" EditFormatString="HH:mm">
                          <ButtonStyle Width="5px" Paddings-Padding="3px" HorizontalAlign="Center" VerticalAlign="Top">
<Paddings Padding="3px"></Paddings>
                          </ButtonStyle>
                      </PropertiesTimeEdit>
                      <Settings AllowAutoFilter ="False" />
                      <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" 
                          VerticalAlign="Middle" >
<Paddings PaddingLeft="6px"></Paddings>
                      </HeaderStyle>
                      <CellStyle HorizontalAlign="Left">
                      </CellStyle>
                </dx:GridViewDataTimeEditColumn>
                <dx:GridViewDataTextColumn FieldName="Duration" Width="120px" Caption="Duration" VisibleIndex="12">
                    <PropertiesTextEdit Width="200px" ClientInstanceName="Duration">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter ="False" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Meeting_Room" Width="120px" Caption="Meeting Room" VisibleIndex="13">
                    <PropertiesTextEdit Width="150px" ClientInstanceName="Meeting_Room">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>
                    
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Status" FieldName="GateStatus" VisibleIndex="14"
                    Width="100px">
                    <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="True" ClientInstanceName="GateStatus">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Status KSH" FieldName="StatusRelease" VisibleIndex="15"
                    Width="0px">
                    <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="True" ClientInstanceName="StatusRelease">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataCheckColumn Caption=" " VisibleIndex="16" Width="0px" 
                      FieldName="AllowCheckTemp" Name="AllowCheckTemp">
                        <Settings AllowAutoFilter="False" />
                       <PropertiesCheckEdit ValueChecked="1" ValueType="System.String" AllowGrayedByClick="false" ValueUnchecked="0" >
                       </PropertiesCheckEdit>

                        <HeaderCaptionTemplate>
                        <dx:ASPxCheckBox ID="checkBox" runat="server" ClientInstanceName="checkBox"  ClientSideEvents-CheckedChanged="CheckedChanged"  ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="" Font-Names="Segoe UI" Font-Size="8pt" ForeColor="White" > 
                        </dx:ASPxCheckBox>

                        </HeaderCaptionTemplate> 

                </dx:GridViewDataCheckColumn>
                <%--<dx:GridViewDataTextColumn Caption="AllowCheckTemp" FieldName="AllowCheck" VisibleIndex="16"
                    Width="0px">
                    <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="True" ClientInstanceName="AllowCheckTemp">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>--%>
            </Columns>
            <SettingsBehavior ConfirmDelete="True" AllowFocusedRow="True" AllowSelectByRowClick="True"
                AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True"  />
            <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
            </SettingsPager>
            <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                <BatchEditSettings ShowConfirmOnLosingChanges="False" />
            </SettingsEditing>
            <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="160" VerticalScrollBarMode="Visible" />
            <%--<Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260"
                HorizontalScrollBarMode="Auto" />--%>
            <%--<SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>--%>
           <%-- <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="320" />
            </SettingsPopup>
            <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />--%>
            <Styles>
                <Header HorizontalAlign="Center">
                    <Paddings PaddingBottom="5px" PaddingTop="5px" />
                </Header>
            </Styles>
             <StylesEditors ButtonEditCellSpacing="0">
                    <ProgressBar Height="21px">
                    </ProgressBar>
                </StylesEditors>

            <%--<Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header HorizontalAlign="Center">
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
                <StatusBar CssClass="statusBar">
                </StatusBar>
            </Styles>--%>
        </dx:ASPxGridView>
    </div>
    <div>
         <dx:ASPxCallback ID="cbInvitation" runat="server" ClientInstanceName="cbInvitation">
            <%-- <ClientSideEvents  EndCallback="cbInvitationEndCallback"/>--%>
         </dx:ASPxCallback>
    </div>
</asp:Content>

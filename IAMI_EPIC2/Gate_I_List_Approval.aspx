﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="Gate_I_List_Approval.aspx.vb" Inherits="IAMI_EPIC2.Gate_I_List_Approval" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };

         function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "Part_No" || currentColumnName == "Commodity" || currentColumnName == "Upc") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        };  

        function SaveData(s, e) {
//            cbkValid.PerformCallback('save');
            Grid.UpdateEdit()
        }

        function disableenablebuttonsubmit(_val) {
        alert('adsds');
            if (_val = '1') {
                btnSave.SetEnabled(false)
            } else {
                btnSave.SetEnabled(true)
             }
        }

          function OnUpdateCheckedChanged(s, e) {
            if (s.GetValue() == -1) s.SetValue(1);
            Grid.SetFocusedRowIndex(-1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "Status_Drawing", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "Status_Drawing", s.GetValue());
                }                
            }
        }

        function gv_OnCustomButtonClick(s, e) {
            if(e.buttonID == 'Approve'){ 
                var key =  Grid.GetRowKey(e.visibleIndex);
                var keysplit =  key.split("|");
              
                window.location.href='Gate_I_Check_Sheet_Approval.aspx?projectid=' + keysplit[0] + '&groupid=' + keysplit[1] + '&commodity=' + keysplit[2] + '&groupcommodity=' + keysplit[3] + '&buyer=' + keysplit[4]
             }
        }

    </script>
    <style>
        .statusBar
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="border: 1px solid black">
            <table style="width: 100%;">
                <tr style="height: 10px">
                    
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Project ID">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                            Font-Names="Segoe UI" TextField="Project_Name" ValueField="Project_ID" TextFormatString="{1}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboGroup.PerformCallback('filter|' + cboProject.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                   
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Commodity">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Lima--%>
                        <dx:ASPxComboBox ID="cboCommodity" runat="server" ClientInstanceName="cboCommodity"
                            Font-Names="Segoe UI" TextField="Commodity" ValueField="Commodity" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                cboGroupCommodity.PerformCallback('filter|' + cboCommodity.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Commodity" FieldName="Commodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                    </td>
                   <td style="padding: 0px 50px 0px 10px"></td>
                   <td style="padding: 0px 50px 0px 10px"></td>
                   <td style="padding: 0px 50px 0px 10px"></td>
                </tr>
                <tr style="height: 10px">
                    
                     <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Group ID">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Dua--%>
                        <dx:ASPxComboBox ID="cboGroup" runat="server" ClientInstanceName="cboGroup" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Group_ID" ValueField="Group_ID"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboCommodity.PerformCallback('filter|' + cboGroup.GetSelectedItem().GetColumnText(0));
                             
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Group ID" FieldName="Group_ID" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                    
                     <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Sourcing Group">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Lima--%>
                        <dx:ASPxComboBox ID="cboGroupCommodity" runat="server" ClientInstanceName="cboGroupCommodity"
                            Font-Names="Segoe UI" TextField="Group_Comodity" ValueField="Group_Comodity"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px"
                            TabIndex="5">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                    
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Sourcing Group" FieldName="Group_Comodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                </tr>
            </table>
            <table>
                <tr style="height: 50px">
                    <td style="width: 80px; padding: 10px 0px 0px 10px">
                        <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShowData" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div>
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" KeyFieldName="Project_ID; Group_ID; Commodity; Group_Comodity; Buyer"
            Theme="Office2010Black" Width="100%" Font-Names="Segoe UI" Font-Size="9pt">
            <ClientSideEvents EndCallback="OnEndCallback" CustomButtonClick="gv_OnCustomButtonClick"></ClientSideEvents>
            <Columns>
                <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" Width="150px">
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="Approve" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Project ID" FieldName="Project_ID" VisibleIndex="1"
                    Width="120px">
                    <PropertiesTextEdit MaxLength="15" Width="115px" ClientInstanceName="Project_ID">
                        <Style HorizontalAlign="Left">
                            
                        </Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Project Name" FieldName="Project_Name" VisibleIndex="1"
                    Width="120px">
                    <PropertiesTextEdit MaxLength="15" Width="115px" ClientInstanceName="Project_Name">
                        <Style HorizontalAlign="Left">
                            
                        </Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Group_ID" Width="120px" Caption="Group" VisibleIndex="2">
                    <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Group_ID">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Commodity" FieldName="Commodity" VisibleIndex="3"
                    Width="120px">
                    <PropertiesTextEdit Width="350px" DisplayFormatInEditMode="True" ClientInstanceName="Commodity">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Sourcing Group" FieldName="Group_Comodity" VisibleIndex="4"
                    Width="100px">
                    <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="True" ClientInstanceName="Group_Comodity">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Buyer" FieldName="Buyer" VisibleIndex="4"
                    Width="100px">
                    <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="True" ClientInstanceName="Buyer">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="True" AllowFocusedRow="True" AllowSelectByRowClick="True"
                AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
            <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260"
                HorizontalScrollBarMode="Auto" />
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="320" />
            </SettingsPopup>
            <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header HorizontalAlign="Center">
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
                <StatusBar CssClass="statusBar">
                </StatusBar>
            </Styles>
        </dx:ASPxGridView>
    </div>
</asp:Content>

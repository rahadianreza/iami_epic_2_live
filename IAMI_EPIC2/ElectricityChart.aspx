﻿<%@ Page Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeBehind="ElectricityChart.aspx.vb" Inherits="IAMI_EPIC2.ElectricityChart" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register assembly="DevExpress.XtraCharts.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script>
        //popup calender month year
        function formatDateISO(date) {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        function Period_From_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            Period_From.SetDate(myDate);
        }

        function Period_From_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();

                var startDate = formatDateISO(Period_From.GetDate());
                var endDate = formatDateISO(Period_To.GetDate());

                if (startDate > endDate) {
                    Message(2, 'Period from must be lower than period to!');
                    Period_From.Focus();
                    return false;
                }
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }
        }

        //period to
        function Period_To_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            Period_To.SetDate(myDate);
        }

        function Period_To_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();

                var startDate = formatDateISO(Period_From.GetDate());
                var endDate = formatDateISO(Period_To.GetDate());

                if (startDate > endDate) {
                    Message(2, 'Period from must be lower than period to!');
                    Period_From.Focus();
                    return false;
                }
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }
        }
        //end popup calender
    </script>
     <style type="text/css">
        .td-col-l 
        {
            padding:0px 0px 0px 10px;
            width:70px;
        }
        .td-col-m
        {
            width:20px;
        }
        .td-col-r
        {
            width:200px;
        }
        .td-col-f
        {
            width:50px;
        }
        .tr-height
        {
            height: 35px;
        }
            
        
    </style>
    <script type="text/javascript">
//        function LoadChart() {
//            ASPxCallbackPanel1.PerformCallback('show'|+cboProvince.GetValue());
//        }

     

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
 
    <%-- <div style="padding : 5px 5px 5px 5px">
       <dxchartsui:WebChartControl ID="WebChartControl1" runat="server">
        </dxchartsui:WebChartControl>
        
    </div>--%>
    <div style="padding: 5px 5px 5px 5px"> 
        <div style="padding: 5px 5px 5px 5px"> 
            <table runat="server" style="border: 1px solid; width: 100%">
                <tr style="height: 10px">
                    <td style="padding: 0px 0px 0px 10px; width: 80px">
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-r">
                    </td>
                    <td class="td-col-f">
                    </td>
                    <td class="td-col-l">
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-r">
                    </td>
                    <td colspan="2">
                    </td>
                </tr>
                <tr style="height: 20px">
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Period From">
                        </dx:ASPxLabel>
                    </td>
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                         <dx:ASPxDateEdit ID="Period_From" runat="server" ClientInstanceName="Period_From"
                            EnableTheming="True" ShowShadow="false" Font-Names="Segoe UI" Theme="Office2010Black"
                            Font-Size="9pt" Height="25px" DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy"
                            Width="120px">
                            <ClientSideEvents DropDown="Period_From_OnDropDown" Init="Period_From_OnInit" />
                        </dx:ASPxDateEdit>
                    </td>
                    <td class="style4" align="center">
                        &nbsp;
                    </td>
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Period To">
                        </dx:ASPxLabel>
                    </td>
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                         <dx:ASPxDateEdit ID="Period_To" runat="server" ClientInstanceName="Period_To" EnableTheming="True"
                            ShowShadow="false" Font-Names="Segoe UI" Theme="Office2010Black" Font-Size="9pt"
                            Height="25px" DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy" Width="120px">
                            <ClientSideEvents DropDown="Period_To_OnDropDown" Init="Period_To_OnInit" />
                        </dx:ASPxDateEdit>
                    </td>
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;
                    </td>
                    <td class="style4">
                        &nbsp;
                    </td>
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;
                    </td>
                    <td style="padding: 0px 0px 0px 10px; width: 200px">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="height:10px">
                    <td style="padding: 0px 0px 0px 10px;" class="style9">
                    </td>
                    <td style="padding: 0px 0px 0px 10px;" class="style9">
                    </td>
                    <td class="style10" align="center">
                    </td>
                    <td style="padding: 0px 0px 0px 10px;" class="style9">
                    </td>
                    <td class="style11">
                    </td>
                    <td class="style11">
                    </td>
                    <td class="style11">
                    </td>
                    <td class="style11">
                    </td>
                    <td class="style11">
                    </td>
                    <td class="style11">
                    </td>
                </tr>
                <tr style="height: 2px">
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text=" Rate Class">
                        </dx:ASPxLabel>
                    </td>
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxComboBox ID="cboRateClass" runat="server" ClientInstanceName="cboRateClass"
                            Width="120px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px"
                            TabIndex="3" DataSourceID="SqlDataSource1">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboVAKind.PerformCallback(cboRateClass.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Rate Class" FieldName="Code" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td class="style4" align="center">
                        &nbsp;
                    </td>
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="VA Kind">
                        </dx:ASPxLabel>
                    </td>
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxComboBox ID="cboVAKind" runat="server" ClientInstanceName="cboVAKind" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Par_Description" ValueField="Par_Code"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="VAKind" FieldName="Par_Description" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="height:10px">
                    <td style="padding: 0px 0px 0px 10px;" class="style9">
                    </td>
                    <td style="padding: 0px 0px 0px 10px;" class="style9">
                    </td>
                    <td class="style10" align="center">
                    </td>
                    <td style="padding: 0px 0px 0px 10px;" class="style9">
                    </td>
                    <td class="style11">
                    </td>
                    <td class="style11">
                    </td>
                    <td class="style11">
                    </td>
                    <td class="style11">
                    </td>
                    <td class="style11">
                    </td>
                    <td class="style11">
                    </td>
                </tr>
                <tr class="tr-height">
                    <td style="padding: 0px 0px 0px 10px" colspan="6">
                        <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnBack" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;&nbsp;
                        <dx:ASPxButton ID="btnShow" runat="server" Text="Show" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShow" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
                             if (cboRateClass.GetText()==''){
                                     toastr.warning('Please select Rate Class', 'Warning');
                                     toastr.options.closeButton = false;
                                     toastr.options.debug = false;
                                     toastr.options.newestOnTop = false;
                                     toastr.options.progressBar = false;
                                     toastr.options.preventDuplicates = true;
                                     toastr.options.onclick = null;
                                     e.processOnServer = false;
                                     return;
                                 }         
                                 
                            if (cboVAKind.GetText()==''){
                                     toastr.warning('Please select VAKind', 'Warning');
                                     toastr.options.closeButton = false;
                                     toastr.options.debug = false;
                                     toastr.options.newestOnTop = false;
                                     toastr.options.progressBar = false;
                                     toastr.options.preventDuplicates = true;
                                     toastr.options.onclick = null;
                                     e.processOnServer = false;
                                     return;
                            } 
                                             
	                        ASPxCallbackPanel1.PerformCallback('show|'+cboRateClass.GetValue()+'|'+cboVAKind.GetValue());
   
   
}" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td colspan="3">
                    </td>
                </tr>
                <tr style="height: 10px">
                    <td colspan="9">
                    </td>
                </tr>
            </table>

        
          </div>
    </div>
    <div style="padding :5px 5px 5px 5px">
        <table runat="server" style="width:100%; border : 1px solid">
            <tr>
                <td style="padding:5px 5px 5px 5px">
                    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" ClientInstanceName="ASPxCallbackPanel1" runat="server" >
                    </dx:ASPxCallbackPanel>

                </td>
            </tr>
            
        </table>
    </div>
    <div>
       
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'RateClass'">
        </asp:SqlDataSource>
         <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="Select * From VW_Electricity_Chart ">
        </asp:SqlDataSource>
        <dx:ASPxCallback ID="ASPxCallback1" runat="server">
            
        </dx:ASPxCallback>
       
    </div>
    
</asp:Content>


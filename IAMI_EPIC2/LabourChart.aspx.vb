﻿Imports DevExpress.XtraCharts
Imports System.Drawing
Imports DevExpress.XtraCharts.Web
Imports DevExpress.Web.ASPxCallbackPanel
Imports System.Data.SqlClient

Public Class LabourChart
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A070")
        Master.SiteTitle = "Labour Cost Chart"
        pUser = Session("user")

        If Not Page.IsCallback And Not Page.IsPostBack Then
            ASPxSpinEditPeriodFrom.Number = Date.Today.Year
            ASPxSpinEditPeriodTo.Number = Date.Today.Year

        End If
    End Sub

    'filter region per province
    Private Sub up_FillComboRegion(pProvince As String, Optional ByRef pErr As String = "")
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsLabourDB.GetDataRegion(pProvince, "ALL", pUser, pmsg)
        If pmsg = "" Then
            cboRegion.DataSource = ds
            cboRegion.DataBind()
            cboRegion.SelectedIndex = 0
        End If
    End Sub
    Private Sub cboRegion_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboRegion.Callback
        Dim pProvince As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        up_FillComboRegion(pProvince, errmsg)

    End Sub

    Protected Sub ASPxCallbackPanel1_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel1.Callback
        Dim wbc As New WebChartControl()
        Dim ds As New DataSet
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProvince As String = Split(e.Parameter, "|")(1)
        Dim pRegion As String = Split(e.Parameter, "|")(2)

        If pFunction = "show" Then
            Try
                Using con As New SqlConnection(Sconn.Stringkoneksi)
                    Dim cmd As New SqlCommand("sp_Mst_Labour_Chart", con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@PeriodFrom", ASPxSpinEditPeriodFrom.Number)
                    cmd.Parameters.AddWithValue("@PeriodTo", ASPxSpinEditPeriodTo.Number)
                    cmd.Parameters.AddWithValue("@Province", pProvince)
                    cmd.Parameters.AddWithValue("@CityRegion", pRegion)

                    Dim da As New SqlDataAdapter(cmd)
                    da.Fill(ds)
                    wbc.DataSource = ds

                    ' Create a line series. 
                    Dim series1 As New Series("Series 1", ViewType.Line)
                    CType(series1.View, LineSeriesView).Color = Color.MediumSeaGreen  '.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True
                    CType(series1.View, LineSeriesView).LineMarkerOptions.Kind = MarkerKind.Circle
                    CType(series1.View, LineSeriesView).LineStyle.Thickness = "1"
                    CType(series1.Label, PointSeriesLabel).BackColor = Color.DarkOrange

                    Dim chartTitle1 As New ChartTitle()
                    Dim chartTitle2 As New ChartTitle()

                    ' Define the text for the titles. 
                    chartTitle1.Text = "<b>Labour Cost (UMR)</b>"
                    chartTitle1.Alignment = StringAlignment.Center
                    chartTitle1.Dock = ChartTitleDockStyle.Top
                    chartTitle1.Antialiasing = True
                    chartTitle1.Font = New Font("Segoe UI", 14, FontStyle.Bold)


                    chartTitle2.Text = "*UMR = Upah Minimum Regional(Minimum Regional Wage) <br>*UMSP = Upah Minimum Sektoral Provinsi (Minimum Province Sectoral Wage)  "
                    chartTitle2.Alignment = StringAlignment.Center
                    chartTitle2.Dock = ChartTitleDockStyle.Bottom
                    chartTitle2.Font = New Font("Segoe UI", 9)

                    ' wbc.SmallChartText.Text = "Labour Cost"
                    'create chart bar
                    'ChartControl1.Titles(0).Text = "Test"
                    wbc.Series.Add(series1)
                    wbc.SeriesDataMember = "Region"
                    wbc.SeriesTemplate.ArgumentDataMember = "Period"
                    wbc.SeriesSorting = SortingMode.Ascending
                    wbc.SeriesTemplate.ValueDataMembers.Item(0) = "UMR"
                    wbc.PaletteName = "Office 2013"
                    wbc.Titles.AddRange(New ChartTitle() {chartTitle1, chartTitle2})


                    wbc.Series(0).ArgumentDataMember = "Period"
                    wbc.Series(0).SeriesPointsSorting = SortingMode.Ascending
                    wbc.Series(0).Name = "Estimated UMSP Level"
                    'wbc.Series(0).SummaryFunction = "AVERAGE([EstimatedUMSP])"
                    wbc.Series(0).ValueDataMembers.Item(0) = "EstimatedUMSP"

                    wbc.Series(0).ArgumentScaleType = ScaleType.Qualitative
                    wbc.Series(0).CrosshairEnabled = DevExpress.Utils.DefaultBoolean.True
                    wbc.Series(0).CrosshairLabelVisibility = DevExpress.Utils.DefaultBoolean.True


                    'position description city/region
                    wbc.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Center
                    wbc.Legend.AlignmentVertical = LegendAlignmentVertical.BottomOutside
                    wbc.Legend.Direction = LegendDirection.LeftToRight
                    wbc.DataBind()


                    'wbc.Series.Add(New Series("Series", ViewType.Line))
                    ' wbc.Series(0).ArgumentScaleType = ScaleType.DateTime
                    'wbc.Series(0).ValueScaleType = ScaleType.Numerical
                    wbc.Width = 1000
                    wbc.Height = 500

                    CType(sender, ASPxCallbackPanel).Controls.Add(wbc)
                End Using
            Catch ex As Exception
                'pErr = ex.Message

            End Try

        End If

        ' Dim r As New Random()
        'For i As Integer = 0 To 4
        '    wbc.Series(0).Points.Add(New SeriesPoint(DateTime.Today.AddDays(i), _
        '    (CInt(Fix((r.NextDouble() * 100) * 10))) / 10.0))
        'Next i


    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Labour.aspx")
    End Sub
End Class
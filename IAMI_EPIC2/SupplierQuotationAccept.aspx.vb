﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO

Public Class SupplierQuotationAccept
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"
    Private Sub FillComboQuotationNumber()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
        dtFrom = Format(dtQuotationFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtQuotationTo.Value, "yyyy-MM-dd")

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_FillCombo", "SQA|StartDate|EndDate|SupplierCode", "1|" & dtFrom & "|" & dtTo & "|" & cboSupplierCode.Text, ErrMsg)

        'Set last value because trigger from btnBack in SupplierQuotationAcceptDetail
        If IsNothing(Session("btnBack_SupplierQuotationAcceptDetail")) = False Then
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_FillCombo", "SQA|StartDate|EndDate|SupplierCode", "1|" & dtFrom & "|" & dtTo & "|" & Split(Session("btnBack_SupplierQuotationAcceptDetail"), "|")(0), ErrMsg)
        End If

        If ErrMsg = "" Then
            cboQuotationNumber.DataSource = ds.Tables(0)
            cboQuotationNumber.DataBind()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboQuotationNumber)
        End If
    End Sub

    Private Sub FillComboSupplier()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Acceptance_FillCombo", "", "", ErrMsg)

        If ErrMsg = "" Then
            cboSupplierCode.DataSource = ds.Tables(0)
            cboSupplierCode.DataBind()

            'Set last value because trigger from btnBack in SupplierQuotationAcceptDetail
            If IsNothing(Session("btnBack_SupplierQuotationAcceptDetail")) = False Then
                cboSupplierCode.SelectedItem = cboSupplierCode.Items.FindByValue(Split(Session("btnBack_SupplierQuotationAcceptDetail"), "|")(0))
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboSupplierCode)
        End If
    End Sub

    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01", ls_QuotationNo As String = ""
        dtFrom = Format(dtQuotationFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtQuotationTo.Value, "yyyy-MM-dd")
        ls_QuotationNo = cboQuotationNumber.Text

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Acceptance_List", "QuotationDateFrom|QuotationDateTo|QuotationNo|SupplierCode|UserID", dtFrom & "|" & dtTo & "|" & ls_QuotationNo & "|" & cboSupplierCode.Text & "|" & pUser, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub ExcelGrid()
        Try
            Call GridLoad()

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=SupplierQuotationAcceptanceList_" & Format(CDate(Now), "yyyyMMdd_HHmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            gs_Message = ex.Message
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        Dim dfrom As Date
        dfrom = Year(Now) & "-" & Month(Now) & "-01"
        dtQuotationFrom.Value = dfrom
        dtQuotationTo.Value = Now
        'cboSupplierCode.Text = "ALL"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("C040")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "C040")
        Dim dfrom As Date

        gs_Message = ""

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            dfrom = Year(Now) & "-" & Month(Now) & "-01"

            dtQuotationFrom.Value = dfrom
            dtQuotationTo.Value = Now

            'Set last value because trigger from btnBack in SupplierQuotationAcceptDetail.aspx
            If IsNothing(Session("btnBack_SupplierQuotationAcceptDetail")) = False Then
                cboSupplierCode.Text = Split(Session("btnBack_SupplierQuotationAcceptDetail"), "|")(0)
                cboQuotationNumber.Text = Split(Session("btnBack_SupplierQuotationAcceptDetail"), "|")(1)
                Call GridLoad()
            End If
        End If
    End Sub

    Private Sub cboQuotationNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboQuotationNumber.Callback
        Call FillComboQuotationNumber()
        If cboQuotationNumber.Items.Count >= 1 Then
            'If there are more than 1 data, then set the default selected index (ALL)
            cboQuotationNumber.SelectedIndex = 0

            'Set last value because trigger from btnBack in SupplierQuotationAcceptDetail
            If IsNothing(Session("btnBack_SupplierQuotationAcceptDetail")) = False Then
                cboQuotationNumber.SelectedItem = cboQuotationNumber.Items.FindByValue(Split(Session("btnBack_SupplierQuotationAcceptDetail"), "|")(1))
                Session.Remove("btnBack_SupplierQuotationAcceptDetail")
            End If
        End If
    End Sub

    Private Sub cboSupplierCode_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSupplierCode.Callback
        Call FillComboSupplier()
        If cboSupplierCode.Items.Count > 1 Then
            'If there are more than 1 data, then set the default selected index (ALL)
            cboSupplierCode.SelectedIndex = 0
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        Call GridLoad()
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""
        Call GridLoad()
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        Dim ErrMsg As String = ""
        Call ExcelGrid()
    End Sub
#End Region


End Class
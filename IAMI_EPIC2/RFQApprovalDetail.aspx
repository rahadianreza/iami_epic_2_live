﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RFQApprovalDetail.aspx.vb" Inherits="IAMI_EPIC2.RFQApprovalDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
         var dataExist;
        function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            //var price = s.batchEditApi.GetCellValue(e.visibleIndex, "ProposalPricePcs");
            dataExist = s.cpExist;
            if (dataExist == "1") {

                e.cancel = true;
                Grid.CancelEdit();
                Grid.batchEditApi.EndEdit();
            }

//            if (currentColumnName != "AllowCheck") {
//                e.cancel = true;
//            }

            currentEditableVisibleIndex = e.visibleIndex;
        }

        function CheckProcess() {
            
            startIndex = 0;

            var check = "";
            var x = false;

            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                check = parseFloat(Grid.batchEditApi.GetCellValue(i, "AllowCheck"));

                if (check == "1") {
                    x = true;
                }
            }

            if (x == false) {
                toastr.warning('Please select RFQ number', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
            }
        }


    function OnEndCallback(s, e) {
        if (s.cpmessage != "" && s.cpval == 1) {
            if (s.cpmessage == "ErrorMsg" && s.cpval == 1) {
                toastr.error(s.cp_message, 'Error');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }
    }

    function MessageBox(s, e) {
        if (s.cpMessage == "Data Has Been Approved Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            btnApprove.SetEnabled(false);
			//btnReject.SetEnabled(false);
            txtNote.SetEnabled(false);
			//comment 01/04/2019		
            millisecondsToWait = 1000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/RFQApproval.aspx';
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                if (pathArray[1] == "RFQApprovalDetail.aspx") {
                    window.location.href = window.location.origin + '/RFQApproval.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else if (s.cpMessage == "Data Has Been Reject Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            btnApprove.SetEnabled(false);
            //btnReject.SetEnabled(false);
            txtNote.SetEnabled(false);

            millisecondsToWait = 1000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/RFQApproval.aspx';
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                if (pathArray[1] == "RFQApprovalDetail.aspx") {
                    window.location.href = window.location.origin + '/RFQApproval.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else if (s.cpMessage == "Data Has Been Drop Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //btnReject.SetEnabled(false);
            //btnDrop.SetEnabled(false);

            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/RFQApproval.aspx';
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                if (pathArray[1] == "RFQApprovalDetail.aspx") {
                    window.location.href = window.location.origin + '/RFQApproval.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
    }

//    function CheckedChanged(s, e) {
//         //Grid.SetFocusedRowIndex(-1);
//        if (s.GetValue() == -1) s.SetValue(1);

//        for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
//            if (Grid.batchEditApi.GetCellValue(i, "AllowCheck", false) != s.GetValue()) {
//                Grid.batchEditApi.SetCellValue(i, "AllowCheck", s.GetValue());
//            }
//        }
//    }

    function SetHdnField(r) {
        var x = document.getElementById('<%= hdnValue.ClientID %>');
        x.value = r;
        return false;
    }

    function CheckedChanged(s, e) {

        //Grid.SetFocusedRowIndex(-1);
        if (s.GetValue() == -1) s.SetValue(1);

        for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
            if (Grid.batchEditApi.GetCellValue(i, "AllowCheck", false) != s.GetValue()) {
                Grid.batchEditApi.SetCellValue(i, "AllowCheck", s.GetValue());
            }
        }
    }

    function Init_Cb(s, e) {
        if (btnApprove.GetEnabled()==false) {
            checkBox.SetEnabled(false);  
        }
       
    
    }

</script>

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
             <tr style="height: 20px">
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
             <tr>
                <td style=" padding:0px 0px 0px 10px; ">
                    
                    <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="RFQ Set Number">
                    </dx1:ASPxLabel>                 
                    
                </td>
                <td></td>
                <td style="width:220px">
           
            <dx1:ASPxTextBox ID="txtRFQSetNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="200px" ClientInstanceName="txtRFQSetNo" MaxLength="20" 
                        Height="30px" Font-Bold="True" BackColor="LightGray"  ReadOnly="True" >
            </dx1:ASPxTextBox>
           
                    </td>
                <td>
                    <dx1:ASPxTextBox ID="txtRev" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="50px" ClientInstanceName="txtRev" MaxLength="20" 
                                Height="30px" BackColor="LightGray" ReadOnly="True" Font-Bold="True" 
                                ForeColor="Black" HorizontalAlign="Center" >
     
                    </dx1:ASPxTextBox>           
                    </td>
                <td>
                    
                    &nbsp;</td>
                <td>
           
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>    
             <tr style="height: 40px">
                <td style=" padding:0px 0px 0px 10px; width:120px">

                    <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="RFQ Date">
                    </dx1:ASPxLabel>                 

                </td>
                <td></td>
                <td>
           
                <dx:ASPxDateEdit ID="dtDRFQDate" runat="server" Theme="Office2010Black" 
                                        Width="100px" AutoPostBack="false" ClientInstanceName="dtDRFQDate"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                                        BackColor="LightGray" ReadOnly="true">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                    </td>
                <td></td>
                <td>
                    
                    &nbsp;</td>
                <td>
           
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>    
             <tr style="height: 35px">
                <td style=" padding:0px 0px 0px 10px; width:120px">

                    <dx1:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Number">
                    </dx1:ASPxLabel>                 
                    
                </td>
                <td>&nbsp;</td>
                <td>
           
            <dx1:ASPxTextBox ID="txtPRNumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="200px" ClientInstanceName="txtPRNumber" MaxLength="20" 
                        Height="30px" Font-Bold="True" BackColor="LightGray" ReadOnly="True" >
            </dx1:ASPxTextBox>
           
                    </td>
                <td>&nbsp;</td>
                <td>
                    
                    &nbsp;</td>
                <td>
           
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
             <tr style="height: 35px">
                <td style=" padding:0px 0px 0px 10px; width:120px">

                    <dx1:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Dateline Date">
                    </dx1:ASPxLabel>                 
                    
                </td>
                <td>&nbsp;</td>
                <td>
           
                <dx:ASPxDateEdit ID="dtDatelineDate" runat="server" Theme="Office2010Black" 
                                        Width="100px" AutoPostBack="false" ClientInstanceName="dtDatelineDate"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        BackColor="LightGray" ReadOnly="true">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                    </td>
                <td>&nbsp;</td>
                <td>
                    
                    &nbsp;</td>
                <td>
           
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
             <tr style="height: 35px">
                <td style=" padding:0px 0px 0px 10px; width:120px">

                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>
           
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    
                    &nbsp;</td>
                <td>
           
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
             <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                </td>
                <td></td>
                <td>     
                    <dx:ASPxCallback ID="cbProcess" runat="server" 
                        ClientInstanceName="cbProcess">
                    <ClientSideEvents CallbackComplete="MessageBox" />                                           
                    </dx:ASPxCallback>

                     <dx:ASPxCallback ID="cbPrint" runat="server" 
                        ClientInstanceName="cbPrint">
                    <ClientSideEvents CallbackComplete="MessageBox" />                                           
                    </dx:ASPxCallback></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>    
             </table>
    </div>
    <div style="padding: 5px 5px 5px 5px">
          <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="RFQ_Number" OnCustomButtonCallback="Grid_CustomButtonCallback" >
              <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing"  />
              <Columns>
                  <dx:GridViewDataTextColumn Caption="RFQ Number" VisibleIndex="1" Width="300px" 
                      FieldName="RFQ_Number">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="Supplier Code" VisibleIndex="2" Width="300px" 
                      FieldName="Supplier_Code">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="Supplier Name" VisibleIndex="3" 
                      Width="350px" FieldName="Supplier_Name">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="Currency" VisibleIndex="4" 
                      Width="100px" FieldName="Currency_Code">
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                  </dx:GridViewDataTextColumn>

                 <dx:GridViewDataCheckColumn Caption=" " VisibleIndex="0" Width="35px" 
                      FieldName="AllowCheck" Name="AllowCheck">

                       <PropertiesCheckEdit ValueChecked="1" ValueType="System.String" AllowGrayedByClick="false" ValueUnchecked="0" >
                       </PropertiesCheckEdit>

                        <HeaderCaptionTemplate>
                        <dx:ASPxCheckBox ID="checkBox" runat="server" ClientInstanceName="checkBox" ClientSideEvents-Init="Init_Cb"  ClientSideEvents-CheckedChanged="CheckedChanged" ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="" Font-Names="Segoe UI" Font-Size="8pt" ForeColor="White" > 
                        </dx:ASPxCheckBox>
                        </HeaderCaptionTemplate> 

                  </dx:GridViewDataCheckColumn>
                   <dx:GridViewCommandColumn VisibleIndex="5" Caption=" " Width="100px">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="btnPrint" Text=" Print Preview "  />
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                  <%--<dx:GridViewDataHyperLinkColumn FieldName="Print" Caption=" ">
                            <PropertiesHyperLinkEdit Target="_blank" ></PropertiesHyperLinkEdit>
                  </dx:GridViewDataHyperLinkColumn>--%>
              </Columns>
                    <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                    </SettingsEditing>
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="160" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>
             </dx:ASPxGridView>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 0px; height: 100px;">  
             <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">

                    <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Approval Notes">
                    </dx1:ASPxLabel>                 

                </td>
                <td></td>
                <td colspan="2" rowspan="2">
                    <dx1:ASPxMemo ID="txtNote" runat="server" Height="50px" Width="700px"
                        ClientInstanceName="txtNote" Font-Names="Segoe UI" Font-Size="9pt"
                        Theme="Metropolis" MaxLength="300">
                        
                    </dx1:ASPxMemo>
                 </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>    
             <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>                            
             <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <asp:HiddenField ID="hdnValue" runat="server" />
                 </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>                            
             <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>                            
             <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; " colspan="8">

                    <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>

                    &nbsp;<dx:ASPxButton ID="btnApprove" runat="server" Text="Approve" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnApprove" Theme="Default">                        
                        
                        <ClientSideEvents Click="function(s, e) {
                                
                                CheckProcess();
	                            var msg = confirm('Are you sure want to approve this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }
                                SetHdnField('1');
                                Grid.UpdateEdit();
	                            cbProcess.PerformCallback('approve|');
                            }" />
                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                     &nbsp;&nbsp;</td>
            </tr>                            
             <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>                            
        </table>
    </div>
 </div>
</asp:Content>

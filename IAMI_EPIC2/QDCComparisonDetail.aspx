﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="QDCComparisonDetail.aspx.vb" Inherits="IAMI_EPIC2.QDCComparisonDetail" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <script type="text/javascript">
       var currentColumnName;
       function GetMessage(s, e) {

           //alert(s.cpMessage);

           if (s.cpMessage == 'Draft data saved successfull') {
               toastr.success(s.cpMessage, 'Success');
               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;



               if (txtFlag.GetText() == '0') {
                   //alert(s.cpStatus);
                   txtCPNumber.SetText(s.cpCPNo);
                   txtSRNumber.SetText(s.cpSRNo);
                   txtParameter.SetText(s.cpSRNo + '|0|' + txtCPNumber.GetText());
                   // txtParameter.SetText(s.cpSRNo + '' + s.cpRevision + cboCPNumber.GetText());


               }

           }
           else if (s.cpMessage == "Data saved successfull") {
               toastr.success(s.cpMessage, 'Success');
               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;

               btnSubmit.SetEnabled(false);

               millisecondsToWait = 1000;
               setTimeout(function () {
                   window.location.href = "/SupplierRecomendList.aspx";
               }, millisecondsToWait);
           }

           else if (s.cpMessage == null || s.cpMessage == "") {
               Grid.PerformCallback('view|' + txtSRNumber.GetText() + '|0|' + txtCPNumber.GetText());
               Grid_Delivery.PerformCallback('view|' + txtSRNumber.GetText() + '|0|' + txtCPNumber.GetText());
               Grid_Quality.PerformCallback('view|' + txtSRNumber.GetText() + '|0|' + txtCPNumber.GetText());
           }
           else {
               toastr.warning(s.cpMessage, 'Warning');
               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;
           }

           var status = s.cpStatus;
           if (status == "0") {
               btnDraft.SetEnabled(true);
               cbSuppNo.SetEnabled(false);
               memoConsi.SetEnabled(false);
               memoNote.SetEnabled(false);
           }
           else if (status == "1") {
               btnDraft.SetEnabled(false);
               cbSuppNo.SetEnabled(false);
               memoConsi.SetEnabled(false);
               memoNote.SetEnabled(false);
           }

           else if (status == "2") {
               btnDraft.SetEnabled(false);
               cbSuppNo.SetEnabled(false);
               memoConsi.SetEnabled(false);
               memoNote.SetEnabled(false);
           }

           else if (status == "5") {
               btnDraft.SetEnabled(false);
               cbSuppNo.SetEnabled(false);
               memoConsi.SetEnabled(false);
               memoNote.SetEnabled(false);
           }

           else if (status == "4") {
               btnDraft.SetEnabled(true);
               cbSuppNo.SetEnabled(false);
               memoConsi.SetEnabled(false);
               memoNote.SetEnabled(false);
           }
       }

       function OnBatchEditEndEditing(s, e) {
           window.setTimeout(function () {

               var Weight = s.batchEditApi.GetCellValue(e.visibleIndex, "Weight");
               var Point1 = s.batchEditApi.GetCellValue(e.visibleIndex, "Point1");
               var Point2 = s.batchEditApi.GetCellValue(e.visibleIndex, "Point2");
               var Point3 = s.batchEditApi.GetCellValue(e.visibleIndex, "Point3");
               var Point4 = s.batchEditApi.GetCellValue(e.visibleIndex, "Point4");
               var Point5 = s.batchEditApi.GetCellValue(e.visibleIndex, "Point5");


               // Perhitungan Weight * Point
               s.batchEditApi.SetCellValue(e.visibleIndex, "WeightPoint1", (Weight / 100) * Point1);
               s.batchEditApi.SetCellValue(e.visibleIndex, "WeightPoint2", (Weight / 100) * Point2);
               s.batchEditApi.SetCellValue(e.visibleIndex, "WeightPoint3", (Weight / 100) * Point3);
               s.batchEditApi.SetCellValue(e.visibleIndex, "WeightPoint4", (Weight / 100) * Point4);
               s.batchEditApi.SetCellValue(e.visibleIndex, "WeightPoint5", (Weight / 100) * Point5);


               var TotalPointRow0 = s.batchEditApi.GetCellValue(0, "Information");
               var TotalPointRow1 = s.batchEditApi.GetCellValue(1, "Information");
               var TotalPointRow2 = s.batchEditApi.GetCellValue(2, "Information");
               var TotalPointRow3 = s.batchEditApi.GetCellValue(3, "Information");
               var TotalPointRow4 = s.batchEditApi.GetCellValue(4, "Information");
               var TotalPointRow5 = s.batchEditApi.GetCellValue(5, "Information");
               var TotalPointRow6 = s.batchEditApi.GetCellValue(6, "Information");

               var WeightRow0WeightPoint1 = s.batchEditApi.GetCellValue(0, "WeightPoint1");
               var WeightRow1WeightPoint1 = s.batchEditApi.GetCellValue(1, "WeightPoint1");
               var WeightRow2WeightPoint1 = s.batchEditApi.GetCellValue(2, "WeightPoint1");
               var WeightRow3WeightPoint1 = s.batchEditApi.GetCellValue(3, "WeightPoint1");
               var WeightRow4WeightPoint1 = s.batchEditApi.GetCellValue(4, "WeightPoint1");
               var WeightRow5WeightPoint1 = s.batchEditApi.GetCellValue(5, "WeightPoint1");
               var WeightRow6WeightPoint1 = s.batchEditApi.GetCellValue(6, "WeightPoint1");

               var WeightRow0WeightPoint2 = s.batchEditApi.GetCellValue(0, "WeightPoint2");
               var WeightRow1WeightPoint2 = s.batchEditApi.GetCellValue(1, "WeightPoint2");
               var WeightRow2WeightPoint2 = s.batchEditApi.GetCellValue(2, "WeightPoint2");
               var WeightRow3WeightPoint2 = s.batchEditApi.GetCellValue(3, "WeightPoint2");
               var WeightRow4WeightPoint2 = s.batchEditApi.GetCellValue(4, "WeightPoint2");
               var WeightRow5WeightPoint2 = s.batchEditApi.GetCellValue(5, "WeightPoint2");
               var WeightRow6WeightPoint2 = s.batchEditApi.GetCellValue(6, "WeightPoint2");

               var WeightRow0WeightPoint3 = s.batchEditApi.GetCellValue(0, "WeightPoint3");
               var WeightRow1WeightPoint3 = s.batchEditApi.GetCellValue(1, "WeightPoint3");
               var WeightRow2WeightPoint3 = s.batchEditApi.GetCellValue(2, "WeightPoint3");
               var WeightRow3WeightPoint3 = s.batchEditApi.GetCellValue(3, "WeightPoint3");
               var WeightRow4WeightPoint3 = s.batchEditApi.GetCellValue(4, "WeightPoint3");
               var WeightRow5WeightPoint3 = s.batchEditApi.GetCellValue(5, "WeightPoint3");
               var WeightRow6WeightPoint3 = s.batchEditApi.GetCellValue(6, "WeightPoint3");

               var WeightRow0WeightPoint4 = s.batchEditApi.GetCellValue(0, "WeightPoint4");
               var WeightRow1WeightPoint4 = s.batchEditApi.GetCellValue(1, "WeightPoint4");
               var WeightRow2WeightPoint4 = s.batchEditApi.GetCellValue(2, "WeightPoint4");
               var WeightRow3WeightPoint4 = s.batchEditApi.GetCellValue(3, "WeightPoint4");
               var WeightRow4WeightPoint4 = s.batchEditApi.GetCellValue(4, "WeightPoint4");
               var WeightRow5WeightPoint4 = s.batchEditApi.GetCellValue(5, "WeightPoint4");
               var WeightRow6WeightPoint4 = s.batchEditApi.GetCellValue(6, "WeightPoint4");

               var WeightRow0WeightPoint5 = s.batchEditApi.GetCellValue(0, "WeightPoint5");
               var WeightRow1WeightPoint5 = s.batchEditApi.GetCellValue(1, "WeightPoint5");
               var WeightRow2WeightPoint5 = s.batchEditApi.GetCellValue(2, "WeightPoint5");
               var WeightRow3WeightPoint5 = s.batchEditApi.GetCellValue(3, "WeightPoint5");
               var WeightRow4WeightPoint5 = s.batchEditApi.GetCellValue(4, "WeightPoint5");
               var WeightRow5WeightPoint5 = s.batchEditApi.GetCellValue(5, "WeightPoint5");
               var WeightRow6WeightPoint5 = s.batchEditApi.GetCellValue(6, "WeightPoint5");

               if (TotalPointRow6 == 'Total Point') {
                   s.batchEditApi.SetCellValue(6, "WeightPoint1", parseFloat(Number(WeightRow0WeightPoint1)) + parseFloat(Number(WeightRow1WeightPoint1)) + parseFloat(Number(WeightRow2WeightPoint1)) + parseFloat(Number(WeightRow3WeightPoint1)) + parseFloat(Number(WeightRow4WeightPoint1)));
                   s.batchEditApi.SetCellValue(6, "WeightPoint2", parseFloat(Number(WeightRow0WeightPoint2)) + parseFloat(Number(WeightRow1WeightPoint2)) + parseFloat(Number(WeightRow2WeightPoint2)) + parseFloat(Number(WeightRow3WeightPoint2)) + parseFloat(Number(WeightRow4WeightPoint2)));
                   s.batchEditApi.SetCellValue(6, "WeightPoint3", parseFloat(Number(WeightRow0WeightPoint3)) + parseFloat(Number(WeightRow1WeightPoint3)) + parseFloat(Number(WeightRow2WeightPoint3)) + parseFloat(Number(WeightRow3WeightPoint3)) + parseFloat(Number(WeightRow4WeightPoint3)));
                   s.batchEditApi.SetCellValue(6, "WeightPoint4", parseFloat(Number(WeightRow0WeightPoint4)) + parseFloat(Number(WeightRow1WeightPoint4)) + parseFloat(Number(WeightRow2WeightPoint4)) + parseFloat(Number(WeightRow3WeightPoint4)) + parseFloat(Number(WeightRow4WeightPoint4)));
                   s.batchEditApi.SetCellValue(6, "WeightPoint5", parseFloat(Number(WeightRow0WeightPoint5)) + parseFloat(Number(WeightRow1WeightPoint5)) + parseFloat(Number(WeightRow2WeightPoint5)) + parseFloat(Number(WeightRow3WeightPoint5)) + parseFloat(Number(WeightRow4WeightPoint5)));

               } else if (TotalPointRow5 == 'Total Point') {
                   s.batchEditApi.SetCellValue(5, "WeightPoint1", parseFloat(Number(WeightRow0WeightPoint1)) + parseFloat(Number(WeightRow1WeightPoint1)) + parseFloat(Number(WeightRow2WeightPoint1)) + parseFloat(Number(WeightRow3WeightPoint1)));
                   s.batchEditApi.SetCellValue(5, "WeightPoint2", parseFloat(Number(WeightRow0WeightPoint2)) + parseFloat(Number(WeightRow1WeightPoint2)) + parseFloat(Number(WeightRow2WeightPoint2)) + parseFloat(Number(WeightRow3WeightPoint2)));
                   s.batchEditApi.SetCellValue(5, "WeightPoint3", parseFloat(Number(WeightRow0WeightPoint3)) + parseFloat(Number(WeightRow1WeightPoint3)) + parseFloat(Number(WeightRow2WeightPoint3)) + parseFloat(Number(WeightRow3WeightPoint3)));
                   s.batchEditApi.SetCellValue(5, "WeightPoint4", parseFloat(Number(WeightRow0WeightPoint4)) + parseFloat(Number(WeightRow1WeightPoint4)) + parseFloat(Number(WeightRow2WeightPoint4)) + parseFloat(Number(WeightRow3WeightPoint4)));
                   s.batchEditApi.SetCellValue(5, "WeightPoint5", parseFloat(Number(WeightRow0WeightPoint5)) + parseFloat(Number(WeightRow1WeightPoint5)) + parseFloat(Number(WeightRow2WeightPoint5)) + parseFloat(Number(WeightRow3WeightPoint5)));

               } else if (TotalPointRow4 == 'Total Point') {
                   s.batchEditApi.SetCellValue(4, "WeightPoint1", parseFloat(Number(WeightRow0WeightPoint1)) + parseFloat(Number(WeightRow1WeightPoint1)) + parseFloat(Number(WeightRow2WeightPoint1)));
                   s.batchEditApi.SetCellValue(4, "WeightPoint2", parseFloat(Number(WeightRow0WeightPoint2)) + parseFloat(Number(WeightRow1WeightPoint2)) + parseFloat(Number(WeightRow2WeightPoint2)));
                   s.batchEditApi.SetCellValue(4, "WeightPoint3", parseFloat(Number(WeightRow0WeightPoint3)) + parseFloat(Number(WeightRow1WeightPoint3)) + parseFloat(Number(WeightRow2WeightPoint3)));
                   s.batchEditApi.SetCellValue(4, "WeightPoint4", parseFloat(Number(WeightRow0WeightPoint4)) + parseFloat(Number(WeightRow1WeightPoint4)) + parseFloat(Number(WeightRow2WeightPoint4)));
                   s.batchEditApi.SetCellValue(4, "WeightPoint5", parseFloat(Number(WeightRow0WeightPoint5)) + parseFloat(Number(WeightRow1WeightPoint5)) + parseFloat(Number(WeightRow2WeightPoint5)));

               } else if (TotalPointRow3 == 'Total Point') {
                   s.batchEditApi.SetCellValue(3, "WeightPoint1", parseFloat(Number(WeightRow0WeightPoint1)) + parseFloat(Number(WeightRow1WeightPoint1)));
                   s.batchEditApi.SetCellValue(3, "WeightPoint2", parseFloat(Number(WeightRow0WeightPoint2)) + parseFloat(Number(WeightRow1WeightPoint2)));
                   s.batchEditApi.SetCellValue(3, "WeightPoint3", parseFloat(Number(WeightRow0WeightPoint3)) + parseFloat(Number(WeightRow1WeightPoint3)));
                   s.batchEditApi.SetCellValue(3, "WeightPoint4", parseFloat(Number(WeightRow0WeightPoint4)) + parseFloat(Number(WeightRow1WeightPoint4)));
                   s.batchEditApi.SetCellValue(3, "WeightPoint5", parseFloat(Number(WeightRow0WeightPoint5)) + parseFloat(Number(WeightRow1WeightPoint5)));
               } else {
                   s.batchEditApi.SetCellValue(2, "WeightPoint1", parseFloat(Number(WeightRow0WeightPoint1)));
                   s.batchEditApi.SetCellValue(2, "WeightPoint2", parseFloat(Number(WeightRow0WeightPoint2)));
                   s.batchEditApi.SetCellValue(2, "WeightPoint3", parseFloat(Number(WeightRow0WeightPoint3)));
                   s.batchEditApi.SetCellValue(2, "WeightPoint4", parseFloat(Number(WeightRow0WeightPoint4)));
                   s.batchEditApi.SetCellValue(2, "WeightPoint5", parseFloat(Number(WeightRow0WeightPoint5)));
               }

               var Weight0 = s.batchEditApi.GetCellValue(0, "Weight");
               var Weight1 = s.batchEditApi.GetCellValue(1, "Weight");
               var Weight2 = s.batchEditApi.GetCellValue(2, "Weight");
               var Weight3 = s.batchEditApi.GetCellValue(3, "Weight");
               var Weight4 = s.batchEditApi.GetCellValue(4, "Weight");
               var Weight5 = s.batchEditApi.GetCellValue(5, "Weight");
               var Weight6 = s.batchEditApi.GetCellValue(6, "Weight");

               if (TotalPointRow6 == 'Total Point') {
                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) + parseInt(Weight3) + parseInt(Weight4) > 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }

                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) + parseInt(Weight3) + parseInt(Weight4) < 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }
               } else if (TotalPointRow5 == 'Total Point') {
                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) + parseInt(Weight3) > 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }

                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) + parseInt(Weight3) < 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }
               } else if (TotalPointRow4 == 'Total Point') {
                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) > 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }

                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) < 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }
               } else if (TotalPointRow3 == 'Total Point') {
                   if (parseInt(Weight0) + parseInt(Weight1) > 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }

                   if (parseInt(Weight0) + parseInt(Weight1) < 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }
               } else {
                   if (parseInt(Weight0) > 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }

                   if (parseInt(Weight0) < 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }
               }


               if (TotalPointRow6 == 'Total Point') {
                   // Penentuan Symbol
                   var Symbol1 = s.batchEditApi.GetCellValue(6, "WeightPoint1");
                   var Symbol2 = s.batchEditApi.GetCellValue(6, "WeightPoint2");
                   var Symbol3 = s.batchEditApi.GetCellValue(6, "WeightPoint3");
                   var Symbol4 = s.batchEditApi.GetCellValue(6, "WeightPoint4");
                   var Symbol5 = s.batchEditApi.GetCellValue(6, "WeightPoint5");

                   var SymbolPoint1 = 'X';
                   var SymbolPoint2 = 'Δ';
                   var SymbolPoint3 = '⃝';


                   if (Symbol1 >= 0.01 && Symbol1 <= 1.00) {

                       s.batchEditApi.SetCellValue(5, "Notes1", SymbolPoint1);

                   } else if (Symbol1 >= 1.01 && Symbol1 <= 2.00) {

                       s.batchEditApi.SetCellValue(5, "Notes1", SymbolPoint2);

                   } else if (Symbol1 >= 2.01 && Symbol1 <= 3.00) {

                       s.batchEditApi.SetCellValue(5, "Notes1", SymbolPoint3);

                   }

                   if (Symbol2 >= 0.01 && Symbol2 <= 1.00) {

                       s.batchEditApi.SetCellValue(5, "Notes2", SymbolPoint1);

                   } else if (Symbol2 >= 1.01 && Symbol2 <= 2.00) {

                       s.batchEditApi.SetCellValue(5, "Notes2", SymbolPoint2);

                   } else if (Symbol2 >= 2.01 && Symbol1 <= 3.00) {

                       s.batchEditApi.SetCellValue(5, "Notes2", SymbolPoint3);

                   }

                   if (Symbol3 >= 0.01 && Symbol3 <= 1.00) {

                       s.batchEditApi.SetCellValue(5, "Notes3", SymbolPoint1);

                   } else if (Symbol3 >= 1.01 && Symbol3 <= 2.00) {

                       s.batchEditApi.SetCellValue(5, "Notes3", SymbolPoint2);

                   } else if (Symbol3 >= 2.01 && Symbol3 <= 3.00) {

                       s.batchEditApi.SetCellValue(5, "Notes3", SymbolPoint3);

                   }

                   if (Symbol4 >= 0.01 && Symbol4 <= 1.00) {

                       s.batchEditApi.SetCellValue(5, "Notes4", SymbolPoint1);

                   } else if (Symbol4 >= 1.01 && Symbol4 <= 2.00) {

                       s.batchEditApi.SetCellValue(5, "Notes4", SymbolPoint2);

                   } else if (Symbol4 >= 2.01 && Symbol4 <= 3.00) {

                       s.batchEditApi.SetCellValue(5, "Notes4", SymbolPoint3);

                   }

                   if (Symbol5 >= 0.01 && Symbol5 <= 1.00) {

                       s.batchEditApi.SetCellValue(5, "Notes5", SymbolPoint1);

                   } else if (Symbol5 >= 1.01 && Symbol5 <= 2.00) {

                       s.batchEditApi.SetCellValue(5, "Notes5", SymbolPoint2);

                   } else if (Symbol5 >= 2.01 && Symbol5 <= 3.00) {

                       s.batchEditApi.SetCellValue(5, "Notes5", SymbolPoint3);

                   }

               } else if (TotalPointRow5 == 'Total Point') {

                   // Penentuan Symbol
                   var Symbol1 = s.batchEditApi.GetCellValue(5, "WeightPoint1");
                   var Symbol2 = s.batchEditApi.GetCellValue(5, "WeightPoint2");
                   var Symbol3 = s.batchEditApi.GetCellValue(5, "WeightPoint3");
                   var Symbol4 = s.batchEditApi.GetCellValue(5, "WeightPoint4");
                   var Symbol5 = s.batchEditApi.GetCellValue(5, "WeightPoint5");

                   var SymbolPoint1 = 'X';
                   var SymbolPoint2 = 'Δ';
                   var SymbolPoint3 = '⃝';


                   if (Symbol1 >= 0.01 && Symbol1 <= 1.00) {

                       s.batchEditApi.SetCellValue(4, "Notes1", SymbolPoint1);

                   } else if (Symbol1 >= 1.01 && Symbol1 <= 2.00) {

                       s.batchEditApi.SetCellValue(4, "Notes1", SymbolPoint2);

                   } else if (Symbol1 >= 2.01 && Symbol1 <= 3.00) {

                       s.batchEditApi.SetCellValue(4, "Notes1", SymbolPoint3);

                   }

                   if (Symbol2 >= 0.01 && Symbol2 <= 1.00) {

                       s.batchEditApi.SetCellValue(4, "Notes2", SymbolPoint1);

                   } else if (Symbol2 >= 1.01 && Symbol2 <= 2.00) {

                       s.batchEditApi.SetCellValue(4, "Notes2", SymbolPoint2);

                   } else if (Symbol2 >= 2.01 && Symbol1 <= 3.00) {

                       s.batchEditApi.SetCellValue(4, "Notes2", SymbolPoint3);

                   }

                   if (Symbol3 >= 0.01 && Symbol3 <= 1.00) {

                       s.batchEditApi.SetCellValue(4, "Notes3", SymbolPoint1);

                   } else if (Symbol3 >= 1.01 && Symbol3 <= 2.00) {

                       s.batchEditApi.SetCellValue(4, "Notes3", SymbolPoint2);

                   } else if (Symbol3 >= 2.01 && Symbol3 <= 3.00) {

                       s.batchEditApi.SetCellValue(4, "Notes3", SymbolPoint3);

                   }

                   if (Symbol4 >= 0.01 && Symbol4 <= 1.00) {

                       s.batchEditApi.SetCellValue(4, "Notes4", SymbolPoint1);

                   } else if (Symbol4 >= 1.01 && Symbol4 <= 2.00) {

                       s.batchEditApi.SetCellValue(4, "Notes4", SymbolPoint2);

                   } else if (Symbol4 >= 2.01 && Symbol4 <= 3.00) {

                       s.batchEditApi.SetCellValue(4, "Notes4", SymbolPoint3);

                   }

                   if (Symbol5 >= 0.01 && Symbol5 <= 1.00) {

                       s.batchEditApi.SetCellValue(4, "Notes5", SymbolPoint1);

                   } else if (Symbol5 >= 1.01 && Symbol5 <= 2.00) {

                       s.batchEditApi.SetCellValue(4, "Notes5", SymbolPoint2);

                   } else if (Symbol5 >= 2.01 && Symbol5 <= 3.00) {

                       s.batchEditApi.SetCellValue(4, "Notes5", SymbolPoint3);

                   }

               } else if (TotalPointRow4 == 'Total Point') {

                   // Penentuan Symbol
                   var Symbol1 = s.batchEditApi.GetCellValue(4, "WeightPoint1");
                   var Symbol2 = s.batchEditApi.GetCellValue(4, "WeightPoint2");
                   var Symbol3 = s.batchEditApi.GetCellValue(4, "WeightPoint3");
                   var Symbol4 = s.batchEditApi.GetCellValue(4, "WeightPoint4");
                   var Symbol5 = s.batchEditApi.GetCellValue(4, "WeightPoint5");

                   var SymbolPoint1 = 'X';
                   var SymbolPoint2 = 'Δ';
                   var SymbolPoint3 = '⃝';


                   if (Symbol1 >= 0.01 && Symbol1 <= 1.00) {

                       s.batchEditApi.SetCellValue(3, "Notes1", SymbolPoint1);

                   } else if (Symbol1 >= 1.01 && Symbol1 <= 2.00) {

                       s.batchEditApi.SetCellValue(3, "Notes1", SymbolPoint2);

                   } else if (Symbol1 >= 2.01 && Symbol1 <= 3.00) {

                       s.batchEditApi.SetCellValue(3, "Notes1", SymbolPoint3);

                   }

                   if (Symbol2 >= 0.01 && Symbol2 <= 1.00) {

                       s.batchEditApi.SetCellValue(3, "Notes2", SymbolPoint1);

                   } else if (Symbol2 >= 1.01 && Symbol2 <= 2.00) {

                       s.batchEditApi.SetCellValue(3, "Notes2", SymbolPoint2);

                   } else if (Symbol2 >= 2.01 && Symbol1 <= 3.00) {

                       s.batchEditApi.SetCellValue(3, "Notes2", SymbolPoint3);

                   }

                   if (Symbol3 >= 0.01 && Symbol3 <= 1.00) {

                       s.batchEditApi.SetCellValue(3, "Notes3", SymbolPoint1);

                   } else if (Symbol3 >= 1.01 && Symbol3 <= 2.00) {

                       s.batchEditApi.SetCellValue(3, "Notes3", SymbolPoint2);

                   } else if (Symbol3 >= 2.01 && Symbol3 <= 3.00) {

                       s.batchEditApi.SetCellValue(3, "Notes3", SymbolPoint3);

                   }

                   if (Symbol4 >= 0.01 && Symbol4 <= 1.00) {

                       s.batchEditApi.SetCellValue(3, "Notes4", SymbolPoint1);

                   } else if (Symbol4 >= 1.01 && Symbol4 <= 2.00) {

                       s.batchEditApi.SetCellValue(3, "Notes4", SymbolPoint2);

                   } else if (Symbol4 >= 2.01 && Symbol4 <= 3.00) {

                       s.batchEditApi.SetCellValue(3, "Notes4", SymbolPoint3);

                   }

                   if (Symbol5 >= 0.01 && Symbol5 <= 1.00) {

                       s.batchEditApi.SetCellValue(3, "Notes5", SymbolPoint1);

                   } else if (Symbol5 >= 1.01 && Symbol5 <= 2.00) {

                       s.batchEditApi.SetCellValue(3, "Notes5", SymbolPoint2);

                   } else if (Symbol5 >= 2.01 && Symbol5 <= 3.00) {

                       s.batchEditApi.SetCellValue(3, "Notes5", SymbolPoint3);

                   }

               } else if (TotalPointRow3 == 'Total Point') {

                   // Penentuan Symbol
                   var Symbol1 = s.batchEditApi.GetCellValue(3, "WeightPoint1");
                   var Symbol2 = s.batchEditApi.GetCellValue(3, "WeightPoint2");
                   var Symbol3 = s.batchEditApi.GetCellValue(3, "WeightPoint3");
                   var Symbol4 = s.batchEditApi.GetCellValue(3, "WeightPoint4");
                   var Symbol5 = s.batchEditApi.GetCellValue(3, "WeightPoint5");

                   var SymbolPoint1 = 'X';
                   var SymbolPoint2 = 'Δ';
                   var SymbolPoint3 = '⃝';


                   if (Symbol1 >= 0.01 && Symbol1 <= 1.00) {

                       s.batchEditApi.SetCellValue(2, "Notes1", SymbolPoint1);

                   } else if (Symbol1 >= 1.01 && Symbol1 <= 2.00) {

                       s.batchEditApi.SetCellValue(2, "Notes1", SymbolPoint2);

                   } else if (Symbol1 >= 2.01 && Symbol1 <= 3.00) {

                       s.batchEditApi.SetCellValue(2, "Notes1", SymbolPoint3);

                   }

                   if (Symbol2 >= 0.01 && Symbol2 <= 1.00) {

                       s.batchEditApi.SetCellValue(2, "Notes2", SymbolPoint1);

                   } else if (Symbol2 >= 1.01 && Symbol2 <= 2.00) {

                       s.batchEditApi.SetCellValue(2, "Notes2", SymbolPoint2);

                   } else if (Symbol2 >= 2.01 && Symbol1 <= 3.00) {

                       s.batchEditApi.SetCellValue(2, "Notes2", SymbolPoint3);

                   }

                   if (Symbol3 >= 0.01 && Symbol3 <= 1.00) {

                       s.batchEditApi.SetCellValue(2, "Notes3", SymbolPoint1);

                   } else if (Symbol3 >= 1.01 && Symbol3 <= 2.00) {

                       s.batchEditApi.SetCellValue(2, "Notes3", SymbolPoint2);

                   } else if (Symbol3 >= 2.01 && Symbol3 <= 3.00) {

                       s.batchEditApi.SetCellValue(2, "Notes3", SymbolPoint3);

                   }

                   if (Symbol4 >= 0.01 && Symbol4 <= 1.00) {

                       s.batchEditApi.SetCellValue(2, "Notes4", SymbolPoint1);

                   } else if (Symbol4 >= 1.01 && Symbol4 <= 2.00) {

                       s.batchEditApi.SetCellValue(2, "Notes4", SymbolPoint2);

                   } else if (Symbol4 >= 2.01 && Symbol4 <= 3.00) {

                       s.batchEditApi.SetCellValue(2, "Notes4", SymbolPoint3);

                   }

                   if (Symbol5 >= 0.01 && Symbol5 <= 1.00) {

                       s.batchEditApi.SetCellValue(2, "Notes5", SymbolPoint1);

                   } else if (Symbol5 >= 1.01 && Symbol5 <= 2.00) {

                       s.batchEditApi.SetCellValue(2, "Notes5", SymbolPoint2);

                   } else if (Symbol5 >= 2.01 && Symbol5 <= 3.00) {

                       s.batchEditApi.SetCellValue(2, "Notes5", SymbolPoint3);

                   }

               } else {

                   // Penentuan Symbol
                   var Symbol1 = s.batchEditApi.GetCellValue(2, "WeightPoint1");
                   var Symbol2 = s.batchEditApi.GetCellValue(2, "WeightPoint2");
                   var Symbol3 = s.batchEditApi.GetCellValue(2, "WeightPoint3");
                   var Symbol4 = s.batchEditApi.GetCellValue(2, "WeightPoint4");
                   var Symbol5 = s.batchEditApi.GetCellValue(2, "WeightPoint5");

                   var SymbolPoint1 = 'X';
                   var SymbolPoint2 = 'Δ';
                   var SymbolPoint3 = '⃝';


                   if (Symbol1 >= 0.01 && Symbol1 <= 1.00) {

                       s.batchEditApi.SetCellValue(2, "Notes1", SymbolPoint1);

                   } else if (Symbol1 >= 1.01 && Symbol1 <= 2.00) {

                       s.batchEditApi.SetCellValue(1, "Notes1", SymbolPoint2);

                   } else if (Symbol1 >= 2.01 && Symbol1 <= 3.00) {

                       s.batchEditApi.SetCellValue(1, "Notes1", SymbolPoint3);

                   }

                   if (Symbol2 >= 0.01 && Symbol2 <= 1.00) {

                       s.batchEditApi.SetCellValue(1, "Notes2", SymbolPoint1);

                   } else if (Symbol2 >= 1.01 && Symbol2 <= 2.00) {

                       s.batchEditApi.SetCellValue(1, "Notes2", SymbolPoint2);

                   } else if (Symbol2 >= 2.01 && Symbol1 <= 3.00) {

                       s.batchEditApi.SetCellValue(1, "Notes2", SymbolPoint3);

                   }

                   if (Symbol3 >= 0.01 && Symbol3 <= 1.00) {

                       s.batchEditApi.SetCellValue(1, "Notes3", SymbolPoint1);

                   } else if (Symbol3 >= 1.01 && Symbol3 <= 2.00) {

                       s.batchEditApi.SetCellValue(1, "Notes3", SymbolPoint2);

                   } else if (Symbol3 >= 2.01 && Symbol3 <= 3.00) {

                       s.batchEditApi.SetCellValue(1, "Notes3", SymbolPoint3);

                   }

                   if (Symbol4 >= 0.01 && Symbol4 <= 1.00) {

                       s.batchEditApi.SetCellValue(1, "Notes4", SymbolPoint1);

                   } else if (Symbol4 >= 1.01 && Symbol4 <= 2.00) {

                       s.batchEditApi.SetCellValue(1, "Notes4", SymbolPoint2);

                   } else if (Symbol4 >= 2.01 && Symbol4 <= 3.00) {

                       s.batchEditApi.SetCellValue(1, "Notes4", SymbolPoint3);

                   }

                   if (Symbol5 >= 0.01 && Symbol5 <= 1.00) {

                       s.batchEditApi.SetCellValue(1, "Notes5", SymbolPoint1);

                   } else if (Symbol5 >= 1.01 && Symbol5 <= 2.00) {

                       s.batchEditApi.SetCellValue(1, "Notes5", SymbolPoint2);

                   } else if (Symbol5 >= 2.01 && Symbol5 <= 3.00) {

                       s.batchEditApi.SetCellValue(1, "Notes5", SymbolPoint3);

                   }

               }



           }, 10);

       }

       function DraftProcess(s, e) {

           if (cbSuppNo.GetText() == '') {
               cbSuppNo.Focus();
               toastr.warning("Please select Supplier Recommendation!", "Warning");
               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;
               e.processOnServer = false;
               return false;
           }

           if (memoConsi.GetText() == '') {
               memoConsi.Focus();
               toastr.warning("Please Input Consider!", "Warning");
               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;
               e.processOnServer = false;
               return false;
           }

                

               var Weight = Grid.batchEditApi.GetCellValue(e.visibleIndex, "Weight");
               var Point1 = Grid.batchEditApi.GetCellValue(e.visibleIndex, "Point1");
               var Point2 = Grid.batchEditApi.GetCellValue(e.visibleIndex, "Point2");
               var Point3 = Grid.batchEditApi.GetCellValue(e.visibleIndex, "Point3");
               var Point4 = Grid.batchEditApi.GetCellValue(e.visibleIndex, "Point4");
               var Point5 = Grid.batchEditApi.GetCellValue(e.visibleIndex, "Point5");


               // Perhitungan Weight * Point
               Grid.batchEditApi.SetCellValue(e.visibleIndex, "WeightPoint1", (Weight / 100) * Point1);
               Grid.batchEditApi.SetCellValue(e.visibleIndex, "WeightPoint2", (Weight / 100) * Point2);
               Grid.batchEditApi.SetCellValue(e.visibleIndex, "WeightPoint3", (Weight / 100) * Point3);
               Grid.batchEditApi.SetCellValue(e.visibleIndex, "WeightPoint4", (Weight / 100) * Point4);
               Grid.batchEditApi.SetCellValue(e.visibleIndex, "WeightPoint5", (Weight / 100) * Point5);


               var TotalPointRow0 = Grid.batchEditApi.GetCellValue(0, "Information");
               var TotalPointRow1 = Grid.batchEditApi.GetCellValue(1, "Information");
               var TotalPointRow2 = Grid.batchEditApi.GetCellValue(2, "Information");
               var TotalPointRow3 = Grid.batchEditApi.GetCellValue(3, "Information");
               var TotalPointRow4 = Grid.batchEditApi.GetCellValue(4, "Information");
               var TotalPointRow5 = Grid.batchEditApi.GetCellValue(5, "Information");
               var TotalPointRow6 = Grid.batchEditApi.GetCellValue(6, "Information");

               var WeightRow0WeightPoint1 = Grid.batchEditApi.GetCellValue(0, "WeightPoint1");
               var WeightRow1WeightPoint1 = Grid.batchEditApi.GetCellValue(1, "WeightPoint1");
               var WeightRow2WeightPoint1 = Grid.batchEditApi.GetCellValue(2, "WeightPoint1");
               var WeightRow3WeightPoint1 = Grid.batchEditApi.GetCellValue(3, "WeightPoint1");
               var WeightRow4WeightPoint1 = Grid.batchEditApi.GetCellValue(4, "WeightPoint1");
               var WeightRow5WeightPoint1 = Grid.batchEditApi.GetCellValue(5, "WeightPoint1");
               var WeightRow6WeightPoint1 = Grid.batchEditApi.GetCellValue(6, "WeightPoint1");

               var WeightRow0WeightPoint2 = Grid.batchEditApi.GetCellValue(0, "WeightPoint2");
               var WeightRow1WeightPoint2 = Grid.batchEditApi.GetCellValue(1, "WeightPoint2");
               var WeightRow2WeightPoint2 = Grid.batchEditApi.GetCellValue(2, "WeightPoint2");
               var WeightRow3WeightPoint2 = Grid.batchEditApi.GetCellValue(3, "WeightPoint2");
               var WeightRow4WeightPoint2 = Grid.batchEditApi.GetCellValue(4, "WeightPoint2");
               var WeightRow5WeightPoint2 = Grid.batchEditApi.GetCellValue(5, "WeightPoint2");
               var WeightRow6WeightPoint2 = Grid.batchEditApi.GetCellValue(6, "WeightPoint2");

               var WeightRow0WeightPoint3 = Grid.batchEditApi.GetCellValue(0, "WeightPoint3");
               var WeightRow1WeightPoint3 = Grid.batchEditApi.GetCellValue(1, "WeightPoint3");
               var WeightRow2WeightPoint3 = Grid.batchEditApi.GetCellValue(2, "WeightPoint3");
               var WeightRow3WeightPoint3 = Grid.batchEditApi.GetCellValue(3, "WeightPoint3");
               var WeightRow4WeightPoint3 = Grid.batchEditApi.GetCellValue(4, "WeightPoint3");
               var WeightRow5WeightPoint3 = Grid.batchEditApi.GetCellValue(5, "WeightPoint3");
               var WeightRow6WeightPoint3 = Grid.batchEditApi.GetCellValue(6, "WeightPoint3");

               var WeightRow0WeightPoint4 = Grid.batchEditApi.GetCellValue(0, "WeightPoint4");
               var WeightRow1WeightPoint4 = Grid.batchEditApi.GetCellValue(1, "WeightPoint4");
               var WeightRow2WeightPoint4 = Grid.batchEditApi.GetCellValue(2, "WeightPoint4");
               var WeightRow3WeightPoint4 = Grid.batchEditApi.GetCellValue(3, "WeightPoint4");
               var WeightRow4WeightPoint4 = Grid.batchEditApi.GetCellValue(4, "WeightPoint4");
               var WeightRow5WeightPoint4 = Grid.batchEditApi.GetCellValue(5, "WeightPoint4");
               var WeightRow6WeightPoint4 = Grid.batchEditApi.GetCellValue(6, "WeightPoint4");

               var WeightRow0WeightPoint5 = Grid.batchEditApi.GetCellValue(0, "WeightPoint5");
               var WeightRow1WeightPoint5 = Grid.batchEditApi.GetCellValue(1, "WeightPoint5");
               var WeightRow2WeightPoint5 = Grid.batchEditApi.GetCellValue(2, "WeightPoint5");
               var WeightRow3WeightPoint5 = Grid.batchEditApi.GetCellValue(3, "WeightPoint5");
               var WeightRow4WeightPoint5 = Grid.batchEditApi.GetCellValue(4, "WeightPoint5");
               var WeightRow5WeightPoint5 = Grid.batchEditApi.GetCellValue(5, "WeightPoint5");
               var WeightRow6WeightPoint5 = Grid.batchEditApi.GetCellValue(6, "WeightPoint5");

               if (TotalPointRow6 == 'Total Point') {
                   Grid.batchEditApi.SetCellValue(6, "WeightPoint1", parseFloat(Number(WeightRow0WeightPoint1)) + parseFloat(Number(WeightRow1WeightPoint1)) + parseFloat(Number(WeightRow2WeightPoint1)) + parseFloat(Number(WeightRow3WeightPoint1)) + parseFloat(Number(WeightRow4WeightPoint1)));
                   Grid.batchEditApi.SetCellValue(6, "WeightPoint2", parseFloat(Number(WeightRow0WeightPoint2)) + parseFloat(Number(WeightRow1WeightPoint2)) + parseFloat(Number(WeightRow2WeightPoint2)) + parseFloat(Number(WeightRow3WeightPoint2)) + parseFloat(Number(WeightRow4WeightPoint2)));
                   Grid.batchEditApi.SetCellValue(6, "WeightPoint3", parseFloat(Number(WeightRow0WeightPoint3)) + parseFloat(Number(WeightRow1WeightPoint3)) + parseFloat(Number(WeightRow2WeightPoint3)) + parseFloat(Number(WeightRow3WeightPoint3)) + parseFloat(Number(WeightRow4WeightPoint3)));
                   Grid.batchEditApi.SetCellValue(6, "WeightPoint4", parseFloat(Number(WeightRow0WeightPoint4)) + parseFloat(Number(WeightRow1WeightPoint4)) + parseFloat(Number(WeightRow2WeightPoint4)) + parseFloat(Number(WeightRow3WeightPoint4)) + parseFloat(Number(WeightRow4WeightPoint4)));
                   Grid.batchEditApi.SetCellValue(6, "WeightPoint5", parseFloat(Number(WeightRow0WeightPoint5)) + parseFloat(Number(WeightRow1WeightPoint5)) + parseFloat(Number(WeightRow2WeightPoint5)) + parseFloat(Number(WeightRow3WeightPoint5)) + parseFloat(Number(WeightRow4WeightPoint5)));

               } else if (TotalPointRow5 == 'Total Point') {
                   Grid.batchEditApi.SetCellValue(5, "WeightPoint1", parseFloat(Number(WeightRow0WeightPoint1)) + parseFloat(Number(WeightRow1WeightPoint1)) + parseFloat(Number(WeightRow2WeightPoint1)) + parseFloat(Number(WeightRow3WeightPoint1)));
                   Grid.batchEditApi.SetCellValue(5, "WeightPoint2", parseFloat(Number(WeightRow0WeightPoint2)) + parseFloat(Number(WeightRow1WeightPoint2)) + parseFloat(Number(WeightRow2WeightPoint2)) + parseFloat(Number(WeightRow3WeightPoint2)));
                   Grid.batchEditApi.SetCellValue(5, "WeightPoint3", parseFloat(Number(WeightRow0WeightPoint3)) + parseFloat(Number(WeightRow1WeightPoint3)) + parseFloat(Number(WeightRow2WeightPoint3)) + parseFloat(Number(WeightRow3WeightPoint3)));
                   Grid.batchEditApi.SetCellValue(5, "WeightPoint4", parseFloat(Number(WeightRow0WeightPoint4)) + parseFloat(Number(WeightRow1WeightPoint4)) + parseFloat(Number(WeightRow2WeightPoint4)) + parseFloat(Number(WeightRow3WeightPoint4)));
                   Grid.batchEditApi.SetCellValue(5, "WeightPoint5", parseFloat(Number(WeightRow0WeightPoint5)) + parseFloat(Number(WeightRow1WeightPoint5)) + parseFloat(Number(WeightRow2WeightPoint5)) + parseFloat(Number(WeightRow3WeightPoint5)));

               } else if (TotalPointRow4 == 'Total Point') {
                   Grid.batchEditApi.SetCellValue(4, "WeightPoint1", parseFloat(Number(WeightRow0WeightPoint1)) + parseFloat(Number(WeightRow1WeightPoint1)) + parseFloat(Number(WeightRow2WeightPoint1)));
                   Grid.batchEditApi.SetCellValue(4, "WeightPoint2", parseFloat(Number(WeightRow0WeightPoint2)) + parseFloat(Number(WeightRow1WeightPoint2)) + parseFloat(Number(WeightRow2WeightPoint2)));
                   Grid.batchEditApi.SetCellValue(4, "WeightPoint3", parseFloat(Number(WeightRow0WeightPoint3)) + parseFloat(Number(WeightRow1WeightPoint3)) + parseFloat(Number(WeightRow2WeightPoint3)));
                   Grid.batchEditApi.SetCellValue(4, "WeightPoint4", parseFloat(Number(WeightRow0WeightPoint4)) + parseFloat(Number(WeightRow1WeightPoint4)) + parseFloat(Number(WeightRow2WeightPoint4)));
                   Grid.batchEditApi.SetCellValue(4, "WeightPoint5", parseFloat(Number(WeightRow0WeightPoint5)) + parseFloat(Number(WeightRow1WeightPoint5)) + parseFloat(Number(WeightRow2WeightPoint5)));

               } else if (TotalPointRow3 == 'Total Point') {
                   Grid.batchEditApi.SetCellValue(3, "WeightPoint1", parseFloat(Number(WeightRow0WeightPoint1)) + parseFloat(Number(WeightRow1WeightPoint1)));
                   Grid.batchEditApi.SetCellValue(3, "WeightPoint2", parseFloat(Number(WeightRow0WeightPoint2)) + parseFloat(Number(WeightRow1WeightPoint2)));
                   Grid.batchEditApi.SetCellValue(3, "WeightPoint3", parseFloat(Number(WeightRow0WeightPoint3)) + parseFloat(Number(WeightRow1WeightPoint3)));
                   Grid.batchEditApi.SetCellValue(3, "WeightPoint4", parseFloat(Number(WeightRow0WeightPoint4)) + parseFloat(Number(WeightRow1WeightPoint4)));
                   Grid.batchEditApi.SetCellValue(3, "WeightPoint5", parseFloat(Number(WeightRow0WeightPoint5)) + parseFloat(Number(WeightRow1WeightPoint5)));
               } else {
                   Grid.batchEditApi.SetCellValue(2, "WeightPoint1", parseFloat(Number(WeightRow0WeightPoint1)));
                   Grid.batchEditApi.SetCellValue(2, "WeightPoint2", parseFloat(Number(WeightRow0WeightPoint2)));
                   Grid.batchEditApi.SetCellValue(2, "WeightPoint3", parseFloat(Number(WeightRow0WeightPoint3)));
                   Grid.batchEditApi.SetCellValue(2, "WeightPoint4", parseFloat(Number(WeightRow0WeightPoint4)));
                   Grid.batchEditApi.SetCellValue(2, "WeightPoint5", parseFloat(Number(WeightRow0WeightPoint5)));
               }

               var Weight0 = Grid.batchEditApi.GetCellValue(0, "Weight");
               var Weight1 = Grid.batchEditApi.GetCellValue(1, "Weight");
               var Weight2 = Grid.batchEditApi.GetCellValue(2, "Weight");
               var Weight3 = Grid.batchEditApi.GetCellValue(3, "Weight");
               var Weight4 = Grid.batchEditApi.GetCellValue(4, "Weight");
               var Weight5 = Grid.batchEditApi.GetCellValue(5, "Weight");
               var Weight6 = Grid.batchEditApi.GetCellValue(6, "Weight");

               if (TotalPointRow6 == 'Total Point') {
                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) + parseInt(Weight3) + parseInt(Weight4) > 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }

                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) + parseInt(Weight3) + parseInt(Weight4) < 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }
               } else if (TotalPointRow5 == 'Total Point') {
                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) + parseInt(Weight3) > 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }

                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) + parseInt(Weight3) < 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }
               } else if (TotalPointRow4 == 'Total Point') {
                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) > 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }

                   if (parseInt(Weight0) + parseInt(Weight1) + parseInt(Weight2) < 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }
               } else if (TotalPointRow3 == 'Total Point') {
                   if (parseInt(Weight0) + parseInt(Weight1) > 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }

                   if (parseInt(Weight0) + parseInt(Weight1) < 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }
               } else {
                   if (parseInt(Weight0) > 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }

                   if (parseInt(Weight0) < 100) {
                       toastr.warning("Please Input Total Weight Percentage Must Be 100 %..!", "Warning");
                       $('html, body').animate({ scrollTop: 0 }, 'slow');
                       return false;
                   }
               }




                   







           var msg = confirm('Are you sure want to draft this data ?');
           if (msg == false) {
               e.processOnServer = false;
               return;
           }
           else {

               Grid.UpdateEdit();
               Grid_Delivery.UpdateEdit();
               Grid_Quality.UpdateEdit();

               millisecondsToWait = 1000;
               setTimeout(function () {
                   cbDraf.PerformCallback();
               }, millisecondsToWait);

               setTimeout(function () {
                   //Grid.PerformCallback('draft|' + txtSRNumber.GetText() + '|' + txtSRNumber.GetText() + '|' + cboCPNumber.GetText());
                   Grid.PerformCallback('draft|' + txtParameter.GetText());
               }, 100);

               setTimeout(function () {
                   Grid_Delivery.PerformCallback('draft|' + txtParameter.GetText());
               }, 100);
               setTimeout(function () {
                   Grid_Quality.PerformCallback('draft|' + txtParameter.GetText());
               }, 100);

           }
       }

       function warningsave(s, e) {
           // alert("rowIndex = " + e.visibleIndex + "; columnIndex = " + e.focusedColumn.index);

           //untuk Coloumn Code
           if (((e.visibleIndex == 1) && (e.focusedColumn.index == 2)) == '') {
               toastr.warning("Please Input !", "Warning");
               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;
               e.processOnServer = false;
               return false;
           }

       }

       function OnStartEditing(s, e) {
           currentColumnName = e.focusedColumn.fieldName;
           var TotalPointRow0 = s.batchEditApi.GetCellValue(0, "Information");
           var TotalPointRow1 = s.batchEditApi.GetCellValue(1, "Information");
           var TotalPointRow2 = s.batchEditApi.GetCellValue(2, "Information");
           var TotalPointRow3 = s.batchEditApi.GetCellValue(3, "Information");
           var TotalPointRow4 = s.batchEditApi.GetCellValue(4, "Information");
           var TotalPointRow5 = s.batchEditApi.GetCellValue(5, "Information");
           var TotalPointRow6 = s.batchEditApi.GetCellValue(6, "Information");


           if (TotalPointRow6 == 'Total Point') {
               // Disable Column Point - Row Total Point
               if ((e.visibleIndex == 6) && (currentColumnName == 'Point1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 6) && (currentColumnName == 'Point2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 6) && (currentColumnName == 'Point3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 6) && (currentColumnName == 'Point4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 6) && (currentColumnName == 'Point5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Weight - Row Total Point
               if ((e.visibleIndex == 6) && (currentColumnName == 'Weight')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Note - Row Total Point
               if ((e.visibleIndex == 6) && (currentColumnName == 'Notes1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 6) && (currentColumnName == 'Notes2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 6) && (currentColumnName == 'Notes3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 6) && (currentColumnName == 'Notes4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 6) && (currentColumnName == 'Notes5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Point - Row Symbol

               if ((e.visibleIndex == 5) && (currentColumnName == 'Point1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Point2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Point3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Point4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Point5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Weight - Row Symbol

               if ((e.visibleIndex == 5) && (currentColumnName == 'Weight')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Notes - Row Symbol

               if ((e.visibleIndex == 5) && (currentColumnName == 'Notes1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Notes2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Notes3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Notes4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Notes5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

           } else if (TotalPointRow5 == 'Total Point') {
               // Disable Column Point - Row Total Point
               if ((e.visibleIndex == 5) && (currentColumnName == 'Point1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Point2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Point3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Point4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Point5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Weight - Row Total Point
               if ((e.visibleIndex == 5) && (currentColumnName == 'Weight')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Note - Row Total Point
               if ((e.visibleIndex == 5) && (currentColumnName == 'Notes1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Notes2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Notes3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Notes4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 5) && (currentColumnName == 'Notes5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Point - Row Symbol

               if ((e.visibleIndex == 4) && (currentColumnName == 'Point1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Point2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Point3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Point4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Point5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Weight - Row Symbol

               if ((e.visibleIndex == 4) && (currentColumnName == 'Weight')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Notes - Row Symbol

               if ((e.visibleIndex == 4) && (currentColumnName == 'Notes1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Notes2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Notes3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Notes4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Notes5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

           }


           else if (TotalPointRow4 == 'Total Point') {
               // Disable Column Point - Row Total Point
               if ((e.visibleIndex == 4) && (currentColumnName == 'Point1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Point2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Point3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Point4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Point5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Weight - Row Total Point
               if ((e.visibleIndex == 4) && (currentColumnName == 'Weight')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Notes - Row Total Point
               if ((e.visibleIndex == 4) && (currentColumnName == 'Notes1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Notes2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Notes3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Notes4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 4) && (currentColumnName == 'Notes5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Point - Row Symbol

               if ((e.visibleIndex == 3) && (currentColumnName == 'Point1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Point2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Point3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Point4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Point5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Weight - Row Symbol

               if ((e.visibleIndex == 3) && (currentColumnName == 'Weight')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Notes - Row Symbol

               if ((e.visibleIndex == 3) && (currentColumnName == 'Notes1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Notes2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Notes3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Notes4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Notes5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }


           }
           else if (TotalPointRow3 == 'Total Point') {
               // Disable Column Point - Row Total Point
               if ((e.visibleIndex == 3) && (currentColumnName == 'Point1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Point2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Point3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Point4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Point5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Weight - Row Total Point
               if ((e.visibleIndex == 3) && (currentColumnName == 'Weight')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Notes - Row Total Point
               if ((e.visibleIndex == 3) && (currentColumnName == 'Notes1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Notes2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Notes3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Notes4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 3) && (currentColumnName == 'Notes5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Point - Row Symbol

               if ((e.visibleIndex == 2) && (currentColumnName == 'Point1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Point2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Point3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Point4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Point5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Weight - Row Symbol

               if ((e.visibleIndex == 2) && (currentColumnName == 'Weight')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Notes - Row Symbol

               if ((e.visibleIndex == 2) && (currentColumnName == 'Notes1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Notes2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Notes3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Notes4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Notes5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

           }
           else {
               // Disable Column Point - Row Total Point
               if ((e.visibleIndex == 2) && (currentColumnName == 'Point1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Point2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Point3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Point4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Point5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Weight - Row Total Point
               if ((e.visibleIndex == 2) && (currentColumnName == 'Weight')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Notes - Row Total Point
               if ((e.visibleIndex == 2) && (currentColumnName == 'Notes1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Notes2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Notes3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Notes4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 2) && (currentColumnName == 'Notes5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Point - Row Symbol

               if ((e.visibleIndex == 1) && (currentColumnName == 'Point1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 1) && (currentColumnName == 'Point2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 1) && (currentColumnName == 'Point3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 1) && (currentColumnName == 'Point4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 1) && (currentColumnName == 'Point5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Weight - Row Symbol

               if ((e.visibleIndex == 1) && (currentColumnName == 'Weight')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

               // Disable Column Notes - Row Symbol

               if ((e.visibleIndex == 1) && (currentColumnName == 'Notes1')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 1) && (currentColumnName == 'Notes2')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 1) && (currentColumnName == 'Notes3')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 1) && (currentColumnName == 'Notes4')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }
               if ((e.visibleIndex == 1) && (currentColumnName == 'Notes5')) {
                   e.cancel = true;
                   Grid.batchEditApi.EndEdit();
               }

           }



           if ((currentColumnName == 'Information_Code') || (currentColumnName == 'Information')) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();

           } else if ((currentColumnName == 'Supplier1') || (currentColumnName == 'WeightPoint1')) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();

           } else if ((currentColumnName == 'Supplier2') || (currentColumnName == 'WeightPoint2')) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();

           } else if ((currentColumnName == 'Supplier3') || (currentColumnName == 'WeightPoint3')) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();

           } else if ((currentColumnName == 'Supplier4') || (currentColumnName == 'WeightPoint4')) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();

           } else if ((currentColumnName == 'Supplier5') || (currentColumnName == 'WeightPoint5')) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

       }

       function GridLoad() {

           Grid.PerformCallback('gridload|xx|0|' + txtCPNumber.GetText());
           Grid_Delivery.PerformCallback('gridload|xx|0|' + txtCPNumber.GetText());
           Grid_Quality.PerformCallback('gridload|xx|0|' + txtCPNumber.GetText());

       }

       function GridLoadCompleted(s, e) {
           LabelSup1.SetText(s.cpHeaderCaption1);
           LabelSup2.SetText(s.cpHeaderCaption2);
           LabelSup3.SetText(s.cpHeaderCaption3);
           LabelSup4.SetText(s.cpHeaderCaption4);
           LabelSup5.SetText(s.cpHeaderCaption5);

       }

       function OnTabChanging(s, e) {
           var tabName = (pageControl.GetActiveTab()).name;
           e.cancel = !ASPxClientEdit.ValidateGroup('group' + tabName);
       }

    </script>
    <style type="text/css">
        .colwidthbutton
        {
            width: 90px;
        }
        
         .hidden-div
        {
            display:none;
        }        
        
        .rowheight
        {
            height: 35px;
        }
        
        .col1
        {
            width: 10px;
        }
        .colLabel1
        {
            width: 150px;
        }
        .colLabel2
        {
            width: 113px;
        }
        .colInput1
        {
            width: 220px;
        }
        .colInput2
        {
            width: 133px;
        }
        .colSpace
        {
            width: 50px;
        }
        
        .customHeader
        {
            height: 15px;
        }
        .style1
        {
            width: 153px;
        }
        .style2
        {
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
                <tr class="rowheight">
                    <td class="col1">
                    </td>
                    <td class="colLabel1">
                        <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier Recommend No.">
                        </dx1:ASPxLabel>
                    </td>
                    <td style="width:200px" >
                    <dx1:ASPxTextBox ID="txtSRNumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="200px" ClientInstanceName="txtSRNumber" MaxLength="60" 
                        Height="30px" ReadOnly="True" Font-Bold="True" BackColor="Silver" >
            </dx1:ASPxTextBox>
                    
                    </td>
                    <td style="width: 35px">
                        &nbsp;</td>
                    <td class="colSpace">
                    </td>
                    <td class="colLabel2">
                        &nbsp;</td>
                    <td class="colInput2">
                        &nbsp;</td>
                    <td>
                    </td>
                    <td class="hidden-div">

                    <dx1:ASPxTextBox ID="txtParameter" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="200px" ClientInstanceName="txtParameter" MaxLength="20" 
                                Height="30px" ReadOnly="True" Font-Bold="True" 
                                ForeColor="Black" HorizontalAlign="Center" >
     
                    </dx1:ASPxTextBox>           
                    </td>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                        <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="CP Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td colspan="2">
                    <dx1:ASPxTextBox ID="txtCPNumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="200px" ClientInstanceName="txtCPNumber" MaxLength="60" 
                        Height="30px" ReadOnly="True" Font-Bold="True" BackColor="Silver" >
            </dx1:ASPxTextBox>
                    
                    </td>
                    <td>
                    </td>
                    <td class="style1">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                    </td>
                    <td class="hidden-div">
                    <div><dx1:ASPxTextBox ID="txtFlag" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="50px" ClientInstanceName="txtFlag" MaxLength="20" 
                                Height="30px" ReadOnly="True" Font-Bold="True" 
                                ForeColor="Black" HorizontalAlign="Center" >
     
                    </dx1:ASPxTextBox> </div>
                              
                    
                    </td>
                </tr>
                <%--<tr>
                <td></td>
                <td colspan="8">
                    
                </td>
            </tr>--%>
                <tr>
                    <td class="style2">
                    </td>
                    <td class="style2">
                    </td>
                    <td class="style2">

                     <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                         <ClientSideEvents Init="GetMessage" />
                                                        
                </dx:ASPxCallback>

                    </td>
                    <td class="style2">
                    </td>
                    <td class="style2">
                    </td>
                    <td class="style2">
                    </td>
                    <td class="style2">
                    </td>
                    <td class="style2">
                    </td>
                    <td class="style2">
                        &nbsp;
                        &nbsp;&nbsp;<dx:ASPxCallback ID="cbDraf" runat="server" ClientInstanceName="cbDraf">
                         <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
                                                        
                </dx:ASPxCallback>

                     <dx:ASPxCallback ID="cbSubmit" runat="server" ClientInstanceName="cbSubmit">
                         <ClientSideEvents Init="GetMessage" />
                                                        
                </dx:ASPxCallback>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxPageControl ID="pageControl" runat="server" ClientInstanceName="pageControl"
                ActiveTabIndex="0" EnableHierarchyRecreation="True" Width="100%">
                <ClientSideEvents ActiveTabChanging="OnTabChanging" />
                <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                </ActiveTabStyle>
                <TabPages>
                    <dx:TabPage Name="Cost" Text="Cost">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControlTab" runat="server">
                                <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                                    EnableTheming="True" Theme="Office2010Black" 
                                     Width="100%"  
                                    Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Information_Code" >
                                         <ClientSideEvents EndCallback="GridLoadCompleted" 
                                                                BatchEditStartEditing="OnStartEditing" 
                                                                BatchEditEndEditing="OnBatchEditEndEditing" />
                                        <Columns>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="Information_Code" VisibleIndex="0"
                                            Width="0.1px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Information" FieldName="Information" VisibleIndex="1"
                                            Width="300px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Weight (%)" FieldName="Weight" VisibleIndex="2"
                                            Width="110px">
                                            <PropertiesTextEdit>
                                                <MaskSettings Mask="&lt;0..100&gt;" />
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 1" Name="Supplier1" VisibleIndex="3">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup1" ClientInstanceName="LabelSup1" runat="server" Text="Supplier 1"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier1" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point1" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint1" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes1" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 2" Name="Supplier2" VisibleIndex="4">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup2" ClientInstanceName="LabelSup2" runat="server" Text="Supplier 2"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier2" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point2" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint2" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes2" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 3" Name="Supplier3" VisibleIndex="5">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup3" ClientInstanceName="LabelSup3" runat="server" Text="Supplier 3"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                 <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier3" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point3" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint3" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                   <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes3" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 4" Name="Supplier4" VisibleIndex="6">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup4" ClientInstanceName="LabelSup4" runat="server" Text="Supplier 4"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                 <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier4" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point4" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint4" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes4" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 5" Name="Supplier5" VisibleIndex="7">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup5" ClientInstanceName="LabelSup5" runat="server" Text="Supplier 5"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                             <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier5" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point5" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint5" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes5" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>

                 <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" 
                EnableRowHotTrack="True" AllowSelectByRowClick="True" />
                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                    </SettingsEditing>
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="200" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>



             </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                       <dx:TabPage Name="Delivery" Text="Delivery">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl1" runat="server">
                                
                                <dx:ASPxGridView ID="Grid_Delivery" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid_Delivery"
                                    EnableTheming="True" Theme="Office2010Black" 
                                     Width="100%"  
                                    Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Information_Code" >
                                         <ClientSideEvents EndCallback="GridLoadCompleted" 
                                                                BatchEditStartEditing="OnStartEditing" 
                                                                BatchEditEndEditing="OnBatchEditEndEditing" />
                                        <Columns>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="Information_Code" VisibleIndex="0"
                                            Width="0.1px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Information" FieldName="Information" VisibleIndex="1"
                                            Width="300px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Weight (%)" FieldName="Weight" VisibleIndex="2"
                                            Width="110px">
                                            <PropertiesTextEdit>
                                                <MaskSettings Mask="&lt;0..100&gt;" />
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 1" Name="Supplier1" VisibleIndex="3">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup1" ClientInstanceName="LabelSup1" runat="server" Text="Supplier 1"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier1" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point1" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint1" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes1" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 2" Name="Supplier2" VisibleIndex="4">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup2" ClientInstanceName="LabelSup2" runat="server" Text="Supplier 2"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier2" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point2" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint2" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes2" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 3" Name="Supplier3" VisibleIndex="5">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup3" ClientInstanceName="LabelSup3" runat="server" Text="Supplier 3"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                 <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier3" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point3" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint3" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                   <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes3" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 4" Name="Supplier4" VisibleIndex="6">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup4" ClientInstanceName="LabelSup4" runat="server" Text="Supplier 4"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                 <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier4" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point4" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint4" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes4" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 5" Name="Supplier5" VisibleIndex="7">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup5" ClientInstanceName="LabelSup5" runat="server" Text="Supplier 5"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                             <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier5" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point5" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint5" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes5" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>

                 <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" 
                EnableRowHotTrack="True" AllowSelectByRowClick="True" />
                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                    </SettingsEditing>
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="200" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>



             </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                       <dx:TabPage Name="Quality" Text="Quality">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl2" runat="server">
                                <dx:ASPxGridView ID="Grid_Quality" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid_Quality"
                                    EnableTheming="True" Theme="Office2010Black" 
                                     Width="100%"  
                                    Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Information_Code" >
                                         <ClientSideEvents EndCallback="GridLoadCompleted" 
                                                                BatchEditStartEditing="OnStartEditing" 
                                                                BatchEditEndEditing="OnBatchEditEndEditing" />
                                        <Columns>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="Information_Code" VisibleIndex="0"
                                            Width="0.1px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Information" FieldName="Information" VisibleIndex="1"
                                            Width="300px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Weight (%)" FieldName="Weight" VisibleIndex="2"
                                            Width="110px">
                                            <PropertiesTextEdit>
                                                <MaskSettings Mask="&lt;0..100&gt;" />
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 1" Name="Supplier1" VisibleIndex="3">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup1" ClientInstanceName="LabelSup1" runat="server" Text="Supplier 1"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier1" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point1" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint1" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes1" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 2" Name="Supplier2" VisibleIndex="4">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup2" ClientInstanceName="LabelSup2" runat="server" Text="Supplier 2"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier2" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point2" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint2" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes2" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 3" Name="Supplier3" VisibleIndex="5">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup3" ClientInstanceName="LabelSup3" runat="server" Text="Supplier 3"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                 <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier3" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point3" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint3" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                   <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes3" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 4" Name="Supplier4" VisibleIndex="6">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup4" ClientInstanceName="LabelSup4" runat="server" Text="Supplier 4"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                 <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier4" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point4" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint4" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes4" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 5" Name="Supplier5" VisibleIndex="7">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup5" ClientInstanceName="LabelSup5" runat="server" Text="Supplier 5"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                             <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier5" 
                                                    VisibleIndex="0" Width="0.1px">
                                                <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Point" FieldName="Point5" 
                                                    VisibleIndex="1" Width="110px">
                                                   
                                                    <PropertiesTextEdit>
                                                        <MaskSettings Mask="&lt;0..3&gt;" />
                                                    </PropertiesTextEdit>
                                                   
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Weight * Point" FieldName="WeightPoint5" VisibleIndex="2"
                                                    Width="210px">
                                                  
                                                   <PropertiesTextEdit DisplayFormatString="F2"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes5" VisibleIndex="3"
                                                    Width="310px">
                                                  
                                                   
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>

                 <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" 
                EnableRowHotTrack="True" AllowSelectByRowClick="True" />
                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                    </SettingsEditing>
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="200" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>



             </dx:ASPxGridView>
                                
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                   
                </TabPages>
            </dx:ASPxPageControl>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
                  <tr>
                    <td class="col1">
                    </td>
                    <td colspan="8">
                        <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier Recommendation :">
                        </dx1:ASPxLabel>
                    </td>
                    <td colspan="7">
                        &nbsp;</td>
                </tr>
                  <tr>
                    <td>
                    </td>

                    <td colspan="7">
                        
                        <dx1:ASPxComboBox ID="cbSuppNo" runat="server" ClientInstanceName="cbSuppNo" Width="280px"
                        Font-Names="Segoe UI"  TextField="Description" ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" 
                            Height="25px">
                             <ClientSideEvents Validation="" EndCallback="GridLoadCompleted" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Supplier Name" FieldName="Description" Width="200px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    
                    </td>
                    <td class="col1">
                    </td>
                </tr>



                 <tr>
                    <td class="col1">
                    </td>
                    <td colspan="8">
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Consideration :">
                        </dx1:ASPxLabel>
                    </td>
                </tr>

                <tr>
                    <td>
                    </td>

                    <td colspan="7">
                        <dx:ASPxMemo ID="memoConsi" runat="server" Height="45px" Width="100%" ClientInstanceName="memoConsi"
                            MaxLength="100">
                          
                        </dx:ASPxMemo>
                    </td>
                    <td class="col1">
                    </td>
                </tr>
                <tr>
                    <td class="col1">
                    </td>
                    <td colspan="8">
                        <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Note :">
                        </dx1:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>

                    <td colspan="7">
                        <dx:ASPxMemo ID="memoNote" runat="server" Height="45px" Width="100%" ClientInstanceName="memoNote"
                            MaxLength="100">
                            
                        </dx:ASPxMemo>
                    </td>
                    <td class="col1">
                    </td>
                </tr>



                <tr>
                    <td>
                    </td>
                    <td colspan="7" align="right">
                        &nbsp;</td>
                    <td>
                    </td>
                </tr>
                <tr style="height: 20px">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="7">
                        <table>
                            <tr class="rowheight">
                                <td class="colwidthbutton">
                                    <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    <dx1:ASPxButton ID="btnDraft" runat="server" Text="Save" Font-Names="Segoe UI" Font-Size="9pt"
                                        Width="80px" Height="25px" AutoPostBack="False" ClientInstanceName="btnDraft"
                                        Theme="Default">
                                        <ClientSideEvents Click="DraftProcess" />
                                        <Paddings Padding="2px" />
                                    </dx1:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    <dx1:ASPxButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="False" Font-Names="Segoe UI"
                                        Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnPrint" Theme="Default">
                                        <Paddings Padding="2px" />
                                    </dx1:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

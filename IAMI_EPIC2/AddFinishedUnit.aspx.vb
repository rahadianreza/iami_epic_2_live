﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxImageZoom
Imports DevExpress.Web.Data

Public Class AddFinishedUnit
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region


    Function ImageToString(ByVal Img As Image)
        Dim ImgConverter As New ImageConverter()
        Dim ImgBytes As Byte() = ImgConverter.ConvertTo(Img, GetType(Byte()))
        Return Convert.ToBase64String(ImgBytes)
    End Function

    Private Sub FillCombo(pItemCode As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        'ds = clsItemListDB.GetDataGroup(pErr)
        'If pErr = "" Then
        '    For i = 0 To ds.Tables(0).Rows.Count - 1
        '        cboGroupItem.Items.Add(Trim(ds.Tables(0).Rows(i)("Group_Code") & ""))
        '    Next
        'End If


        ds = clsFinishedItemListDB.GetDataUOM(pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboUOM.Items.Add(Trim(ds.Tables(0).Rows(i)("UOM") & ""))
            Next
        End If




    End Sub

    Private Sub up_LoadData(pVariantCode As String)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim clsFinished As New clsFinishedItemList

        'Try
        clsFinished.VariantCode = pVariantCode
        ds = clsFinishedItemListDB.GetDataItem(clsFinished, pErr)
        If pErr = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                txtVariantName.Text = Trim(ds.Tables(0).Rows(0)("VariantName") & "")
                txtMaterialFUNo.Text = Trim(ds.Tables(0).Rows(0)("MaterialFU_No") & "")
                txtType.Text = Trim(ds.Tables(0).Rows(0)("Type") & "")

                cboUOM.SelectedIndex = cboUOM.Items.IndexOf(cboUOM.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("UOM") & "")))

                txtLastIAPrice.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("LastIAPrice")), 0, ds.Tables(0).Rows(0)("LastIAPrice"))


            End If
        Else
            txtVariantName.Text = ""
            txtMaterialFUNo.Text = ""
            txtType.Text = ""
            cboUOM.Text = ""
            txtLastIAPrice.Text = ""

        End If
    End Sub

    Private Function uf_CheckDataIsExist(pitemcode As String) As Boolean
        Dim pErr As String = ""
        Dim ds As New DataSet
        Dim cItemMst As New clsFinishedItemList
        cItemMst.VariantCode = pitemcode
        ds = clsFinishedItemListDB.GetDataItem(cItemMst, pErr)

        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub Show_Error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbSave.JSProperties("cpMessage") = ErrMsg
    End Sub

    Private Sub Show_Delete(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbDelete.JSProperties("cpMessage") = ErrMsg
    End Sub

    Private Sub Show_Clear(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbClear.JSProperties("cpMessage") = ErrMsg
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|A150", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnDelete.SetEnabled(false);" & vbCrLf & _
                         "btnSubmit.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "btnSubmit", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboUOM)
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboUOM)
            End Try
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A150")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user").ToString.ToUpper()
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A150")
        Dim itemcode As String
        GridLoadAttachment()
        itemcode = Request.QueryString("ID") & ""

        Dim ds As New DataSet
        Try
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Mst_FinishedUnit_ListAttachment", "VariantCode|", itemcode, "")

            If ds.Tables(0).Rows.Count > 0 Then
                ASPxBinaryImage2.ContentBytes = ds.Tables(0).Rows(0)("ImageBinary")
            End If
        Catch
        End Try

        If IsPostBack Then
            ucAtc.Enabled = True
        End If

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If itemcode <> "" Then


                FillCombo(itemcode)

                btnClear.Text = "Cancel"

                txtVariantCode.Text = itemcode
                txtVariantCode.BackColor = Color.LightYellow
                up_LoadData(itemcode)
                txtVariantCode.ReadOnly = True
                btnDelete.Enabled = True
                'Else
                '   txtItemCode.Text = "--NEW--"

                'Dim ds As New DataSet
                'ds = clsItemListDB.GenerateNewMaterialNo("", "")
                'txtItemCode.Text = ds.Tables(0).Rows(0)("Material_No").ToString
                'txtItemCode.ReadOnly = True
                'txtItemCode.BackColor = Color.LightGray
            Else
                txtVariantCode.Text = "NEW"
                ScriptManager.RegisterStartupScript(btnAddFile, btnAddFile.GetType(), "btnAddFile", "btnAddFile.SetEnabled(false);", True)
                ScriptManager.RegisterStartupScript(btnDeleteAtc, btnDeleteAtc.GetType(), "btnDeleteAtc", "btnDeleteAtc.SetEnabled(false);", True)
                ucAtc.Enabled = False
            End If

        End If

        cbSave.JSProperties("cpMessage") = ""
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        gs_Back = True
        Response.Redirect("~/FinishedUnitList.aspx")

    End Sub

    Private Sub cbDelete_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDelete.Callback
        Dim ItemList As New clsFinishedItemList
        Dim MsgErr As String = ""

        Try
            Dim i As Integer
            Dim i2 As Integer

            'delete item attachment
            i2 = clsFinishedItemListDB.DeleteAttachment(ItemList, pUser)
            'delete mst item
            i = clsFinishedItemListDB.Delete(ItemList, pUser)

            cbDelete.JSProperties("cpMessage") = "Data Deleted Successfully!"
        Catch ex As Exception
            Show_Delete(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub

    Private Sub up_Save(Optional ByRef pErr As String = "")
        Dim ItemList As New clsFinishedItemList
        'Dim pAction = e.Parameter

        Try




            'If pAction = "save" Then
            Dim PostedFile1 As HttpPostedFile = uploader1.PostedFile
            Dim FileName1 As String = ""
            Dim FileExtension1 As String = ""
            Dim Stream1 As Stream
            Dim Br1 As BinaryReader
            Dim Bytes1 As Byte()

            If Not IsNothing(PostedFile1) Then
                FileName1 = Path.GetFileName(PostedFile1.FileName)
                FileExtension1 = Path.GetExtension(FileName1)
                Stream1 = PostedFile1.InputStream
                Br1 = New BinaryReader(Stream1)
                Bytes1 = Br1.ReadBytes(Stream1.Length)

                If Bytes1.Length > 0 Then
                    ItemList.Picture1 = Bytes1

                End If

                Dim FileSize1 As Single = PostedFile1.ContentLength / 1024 / 1024
                If FileSize1 > 2 Then
                    'show_save(MsgTypeEnum.ErrorMsg, "File size must not exceed 1 MB!")
                    cbSave.JSProperties("cpMessage") = "File size must not exceed 2 MB"
                    pErr = "File size must not exceed 2 MB"
                    Exit Sub
                End If

            End If

            Dim PostedFile2 As HttpPostedFile = uploader2.PostedFile
            Dim FileName2 As String = ""
            Dim FileExtension2 As String = ""
            Dim Stream2 As Stream
            Dim Br2 As BinaryReader
            Dim Bytes2 As Byte()

            If Not IsNothing(PostedFile2) Then
                FileName2 = Path.GetFileName(PostedFile2.FileName)
                FileExtension2 = Path.GetExtension(FileName2)
                Stream2 = PostedFile2.InputStream
                Br2 = New BinaryReader(Stream2)
                Bytes2 = Br2.ReadBytes(Stream2.Length)

                If Bytes2.Length > 0 Then
                    ItemList.Picture2 = Bytes2
                End If
                Dim FileSize2 As Single = PostedFile2.ContentLength / 1024 / 1024
                If FileSize2 > 2 Then
                    'show_save(MsgTypeEnum.ErrorMsg, "File size must not exceed 1 MB!")
                    cbSave.JSProperties("cpMessage") = "File size must not exceed 2 MB"
                    pErr = "File size must not exceed 2 MB"
                    Exit Sub
                End If
            End If

            Dim PostedFile3 As HttpPostedFile = uploader3.PostedFile
            Dim FileName3 As String = ""
            Dim FileExtension3 As String = ""
            Dim Stream3 As Stream
            Dim Br3 As BinaryReader
            Dim Bytes3 As Byte()

            If Not IsNothing(PostedFile3) Then
                FileName3 = Path.GetFileName(PostedFile3.FileName)
                FileExtension3 = Path.GetExtension(FileName3)
                Stream3 = PostedFile3.InputStream
                Br3 = New BinaryReader(Stream3)
                Bytes3 = Br3.ReadBytes(Stream3.Length)

                If Bytes3.Length > 0 Then
                    ItemList.Picture3 = Bytes3
                End If
                Dim FileSize3 As Single = PostedFile3.ContentLength / 1024 / 1024
                If FileSize3 > 2 Then
                    'show_save(MsgTypeEnum.ErrorMsg, "File size must not exceed 1 MB!")
                    cbSave.JSProperties("cpMessage") = "File size must not exceed 2 MB"
                    pErr = "File size must not exceed 2 MB"
                    Exit Sub
                End If
            End If

            Dim i As Integer

            Dim ds As New DataSet
            If txtVariantCode.Text = "NEW" Then
                ds = clsFinishedItemListDB.GenerateNewMaterialNo("", "")
                If ds.Tables(0).Rows.Count > 0 Then
                    ItemList.VariantCode = ds.Tables(0).Rows(0)("VariantCode").ToString()

                End If
            Else
                ItemList.VariantCode = Request.QueryString("ID")
            End If

            ItemList.VariantName = txtVariantName.Text
            ItemList.MaterialFUNo = txtMaterialFUNo.Text
            ItemList.Type = txtType.Text
            'ItemList.UOM = cboUOM.Text
            ItemList.UOM = cboUOM.SelectedItem.GetValue("Code").ToString()

            ItemList.LastIAPrice = txtLastIAPrice.Text



            Dim MsgErr As String = ""

            If Request.QueryString("ID") & "" = "" Then
                i = clsFinishedItemListDB.Insert(ItemList, pUser, MsgErr)
                If MsgErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
                    cbSave.JSProperties("cpItemCode") = ItemList.VariantCode
                Else
                    cbSave.JSProperties("cpMessage") = MsgErr
                End If
            Else
                i = clsFinishedItemListDB.Update(ItemList, pUser, MsgErr)
                If MsgErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                    cbSave.JSProperties("cpItemCode") = ItemList.VariantCode
                Else
                    cbSave.JSProperties("cpMessage") = MsgErr
                End If

            End If



        Catch ex As Exception
            Show_Error(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub

    'Private Sub cbClear_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbClear.Callback
    'If txtItemCode.ReadOnly = False Then
    '    cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"
    'Else
    '    cbClear.JSProperties("cpMessage") = "Data Cancel Successfully!"
    '    up_LoadData(txtItemCode.Text)
    'End If
    'End Sub


    Private Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        If txtVariantCode.ReadOnly = False Then
            If uf_CheckDataIsExist(txtVariantCode.Text) = True Then
                cbSave.JSProperties("cpMessage") = "Data Is Already Exist!"
                Exit Sub
            End If
        End If
        Dim errmsg As String = ""

        up_Save(errmsg)

        If errmsg = "File size must not exceed 2 MB" Then
            up_LoadData(txtVariantCode.Text)
        End If
    End Sub

    Private Sub cbSave_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSave.Callback
        ' txtItemCode.Text = "--NEW--"
    End Sub

    Private Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.Click
        If txtVariantCode.ReadOnly = False Then
            cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"
        Else
            cbClear.JSProperties("cpMessage") = "Data Cancel Successfully!"
            up_LoadData(txtVariantCode.Text)
        End If
    End Sub


    'atc 'untuk upload image
    Private Sub GridLoadAttachment()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", RevNo As String = ""

        Try
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Mst_FinishedUnit_ListAttachment ", "VariantCode|", txtVariantCode.Text, ErrMsg)

            If ErrMsg = "" Then

                GridAtc.DataSource = ds.Tables(0)
                GridAtc.DataBind()
                'showImage.Value = ds.Tables(0).Rows(0)("ImageBinary").ToString

                If ds.Tables(0).Rows.Count = 0 Then
                    DisplayMessage(MsgTypeEnum.Info, "There is no attachment to show!", GridAtc)
                End If
            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridAtc)
            End If

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridAtc)
        End Try
    End Sub

    'SAVE ATTACHMENT
    Private Sub SaveAttachment(ByVal pPartNo As String, ByVal pFileName As String, ByVal pBinaryImage As Object, pFileSize As Single)
        Dim ls_SQL As String = "", UserLogin As String = Session("user")




        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                'sp belum ada
                ls_SQL = "sp_Mst_FinishedUnit_Insert_Attachment "
                '& _
                '         "'" & pMaterialNo & "','" & pFileName & "'," & _
                '         "" & pBinaryImage & ",'" & UserLogin & "'" '& _
                '"'" & pFileSize & "'"

                Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("VariantCode", pPartNo)
                sqlComm.Parameters.AddWithValue("FileName", pFileName)
                sqlComm.Parameters.AddWithValue("ImageBinary", pBinaryImage)
                sqlComm.Parameters.AddWithValue("FileSize", pFileSize)
                sqlComm.Parameters.AddWithValue("UserID", UserLogin)
                sqlComm.ExecuteNonQuery()
                sqlComm.Dispose()

                sqlTran.Commit()
            End Using
        End Using
    End Sub

    Private Sub DeleteAttachment(ByVal pPartNo As String, ByVal pFileName As String)
        Dim ls_SQL As String = ""
        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                ls_SQL = "EXEC sp_Mst_FinishedUnit_DeleteAttachment " & _
                         "'" & pPartNo & "','" & pFileName & "'"

                Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                sqlComm.CommandType = CommandType.Text
                sqlComm.ExecuteNonQuery()
                sqlComm.Dispose()

                sqlTran.Commit()
            End Using
        End Using

    End Sub

    Private Sub GridAtc_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles GridAtc.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub GridAtc_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridAtc.CustomCallback
        Call GridLoadAttachment()
    End Sub

    Private Sub GridAtc_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridAtc.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "FileName" Then
            'Editable
            e.Cell.BackColor = Color.LemonChiffon
        Else
            'Non-Editable
            e.Cell.BackColor = Color.White
        End If
    End Sub

    Private Sub ucAtc_FileUploadComplete(sender As Object, e As DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs) Handles ucAtc.FileUploadComplete

        If e.IsValid = False Then
            ucAtc.JSProperties("cpType") = "3"
            ucAtc.JSProperties("cpMessage") = "Invalid file!"
        Else
            Dim ItemCode As String = Request.QueryString("ID") & ""
            If ItemCode <> "NEW" Then
                'Dim PostedFile1 As HttpPostedFile = uploader1.PostedFile
                Dim FileName1 As String = ""
                Dim FileExtension1 As String = ""
                Dim Stream1 As Stream
                Dim Br1 As BinaryReader
                Dim Bytes1 As Byte()
                'FileName1 = e.UploadedFile.FileName
                If Not IsNothing(e.UploadedFile.FileName) Then
                    Try
                        Using conn As New SqlConnection(Sconn.Stringkoneksi)
                            conn.Open()
                            Dim sql2 = "select isnull(MAX(SequenceNo),0)+1 as SequenceNo from Mst_FinishedUnit_Attachment where VariantCode='" & ItemCode & "'"
                            Dim cmd2 As New SqlCommand(sql2, conn)
                            cmd2.CommandType = CommandType.Text
                            Dim da2 As New SqlDataAdapter(cmd2)
                            Dim ds2 As New DataSet
                            da2.Fill(ds2)
                            If ds2.Tables(0).Rows.Count > 0 Then
                                FileName1 = "IMAGE" & CInt(ds2.Tables(0).Rows(0)("SequenceNo")).ToString("D2") & "_" & e.UploadedFile.FileName
                            Else
                                FileName1 = "IMAGE" & (ds2.Tables(0).Rows.Count + 1).ToString("D2") & "_" & e.UploadedFile.FileName
                            End If

                        End Using

                    Catch ex As Exception
                        ucAtc.JSProperties("cpType") = "3"
                        ucAtc.JSProperties("cpMessage") = Err.Description
                    End Try


                    FileExtension1 = e.UploadedFile.ContentType
                    Stream1 = e.UploadedFile.FileContent
                    Br1 = New BinaryReader(Stream1)
                    Bytes1 = Br1.ReadBytes(Stream1.Length)
                    'Dim fileStream = New FileStream
                    'fileStream.Write(e.UploadedFile.FileBytes, 0, e.UploadedFile.FileBytes.Length)
                    ' fileStream = e.UploadedFile.FileContent
                    'Dim bytes As Byte() = e.UploadedFile.FileBytes
                    'Stream1 =


                    Dim BinaryImage As Object
                    If Bytes1.Length > 0 Then
                        BinaryImage = Bytes1

                    End If

                    Dim FileSize1 As Single = e.UploadedFile.ContentLength / 1024 / 1024
                    If FileSize1 > 2 Then
                        'show_save(MsgTypeEnum.ErrorMsg, "File size must not exceed 1 MB!")
                        ucAtc.JSProperties("cpMessage") = "File size must not exceed 2 MB"

                        Exit Sub
                    End If

                    Try
                        Using conn As New SqlConnection(Sconn.Stringkoneksi)
                            conn.Open()
                            Dim Sql = "Select sum(FileSize) FileSize from Mst_FinishedUnit_Attachment where VariantCode='" & ItemCode & "' Group By VariantCode"
                            Dim cmd As New SqlCommand(Sql, conn)
                            cmd.CommandType = CommandType.Text
                            Dim da As New SqlDataAdapter(cmd)
                            Dim ds As New DataSet
                            da.Fill(ds)


                            If ds.Tables(0).Rows.Count > 0 Then
                                Dim FileSizeDB = ds.Tables(0).Rows(0)("FileSize")
                                If FileSize1 > (10 - FileSizeDB) Then
                                    ucAtc.JSProperties("cpType") = "3"
                                    ucAtc.JSProperties("cpMessage") = "File size limit max 10 MB, remain size (" & Math.Round(10 - FileSizeDB, 5) & ") MB"
                                    Exit Sub
                                End If
                            End If



                        End Using

                        ucAtc.JSProperties("cpType") = "1"
                        ucAtc.JSProperties("cpMessage") = "Item " & e.UploadedFile.FileName & " has been uploaded successfully!"
                        Call SaveAttachment(ItemCode, FileName1, BinaryImage, FileSize1)
                    Catch ex As Exception
                        ucAtc.JSProperties("cpType") = "3"
                        ucAtc.JSProperties("cpMessage") = Err.Description
                    End Try

                End If
            End If

        End If
    End Sub

    'untuk upload image CALLBACK
    Private Sub cb_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cb.Callback
        Select Case e.Parameter
            Case "DeleteAttachment"
                Dim iData As Integer = 0
                Dim param As String = hf.Get("DeleteAttachment")
                'If param <> "" Then param = Strings.Left(param, Len(param) - 1)

                'IDENTIFY THE NUMBER OF DATA
                For idx As Integer = 1 To Len(param)
                    If Strings.Mid(param, idx, 1) = "|" Then
                        iData = iData + 1
                    End If
                Next idx

                'DELETE ATTACHMENT
                For idx As Integer = 0 To iData - 1

                    Call DeleteAttachment(txtVariantCode.Text, Split(param, "|")(idx))
                Next idx

                cb.JSProperties("cpActionAfter") = "DeleteAtc"
                DisplayMessage(MsgTypeEnum.Success, "Data deleted successfully!", cb)


        End Select
    End Sub

    'untuk popup image
    Protected Sub callbackPanel_Callback(ByVal source As Object, ByVal e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase)
        Dim ItemCode As String = Request.QueryString("ID") & ""
        edBinaryImage.Value = FindImage(ItemCode, e.Parameter, "ImageBinary")
    End Sub

    Private Function FindImage(ByVal PartNo As String, ByVal SeqNo As Integer, ByVal field As String) As Byte()
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String

                sql = "SELECT " & field & " FROM dbo.Mst_FinishedUnit_Attachment WHERE " & field & " IS NOT NULL AND VariantCode = '" & PartNo & "' and SequenceNo = " & SeqNo & " "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds.Tables(0).Rows(0).Item("" & field & "")
                Else
                    Return Nothing
                End If

            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Class
﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data

Public Class LabourDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
    Dim strProvince As String = ""
    Dim strRegion As String = ""
    Dim strPeriod As String = ""

#End Region
    Private Sub FillCombo(pProvinsi)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = ClsLabourDB.GetDataProvinsi(pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboProvince.Items.Add(Trim(ds.Tables(0).Rows(i)("Province") & ""))
            Next
        End If

        ds = ClsLabourDB.GetDataRegion(pProvinsi, "-", pUser, pErr)
        If pErr = "" Then
            cboRegion.DataSource = ds
            cboRegion.DataBind()
        End If


        ds = ClsLabourDB.GetDataSectoral(pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboSectoral.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If



    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A070")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        ' statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        Dim str As String = Request.QueryString("ID")
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If str <> "" Then
                btnSubmit.Text = "Update"

                ScriptManager.RegisterStartupScript(cboProvince, cboProvince.GetType(), "cboProvince", "cboProvince.SetEnabled(false);", True)
                ScriptManager.RegisterStartupScript(cboRegion, cboRegion.GetType(), "cboRegion", "cboRegion.SetEnabled(false);", True)
                ScriptManager.RegisterStartupScript(txtPeriod, txtPeriod.GetType(), "txtPeriod", "txtPeriod.SetEnabled(false);", True)
                ScriptManager.RegisterStartupScript(btnClear, btnClear.GetType(), "btnClear", "btnClear.SetEnabled(false);", True)

                Session("Province") = str.Split("|")(0)
                Session("Region") = str.Split("|")(1)
                Session("Period") = str.Split("|")(2)

                'untuk fill combobox dari database
                FillCombo(Session("Province"))

                Dim ds As DataSet = ClsLabourDB.getDetail(Session("Province"), Session("Region"), Session("Period"), pUser)
                If ds.Tables(0).Rows.Count > 0 Then

                    cboProvince.SelectedIndex = cboProvince.Items.IndexOf(cboProvince.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Province") & "")))
                    cboRegion.SelectedIndex = cboRegion.Items.IndexOf(cboRegion.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Region") & "")))
                    txtPeriod.Text = ds.Tables(0).Rows(0)("Period").ToString()
                    txtUMR.Text = ds.Tables(0).Rows(0)("UMR").ToString()
                    txtEstimatedUMSP.Text = ds.Tables(0).Rows(0)("EstimatedUMSP").ToString()
                    cboSectoral.SelectedIndex = cboSectoral.Items.IndexOf(cboSectoral.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Sectoral") & "")))
                    txtUMSP.Text = ds.Tables(0).Rows(0)("UMSP").ToString()
                    'txtSectoralTemp.Text = ds.Tables(0).Rows(0)("SectoralDesc").ToString()
                End If

            Else
                ' btnSubmit.Visible = True
                ' btnUpdate.Visible = False
                cboProvince.Enabled = True
                cboRegion.Enabled = True
                txtPeriod.Enabled = True
                txtPeriod.Number = Today.Date.Year
            End If
        End If

    End Sub

    'filter region per province
    Private Sub up_FillComboRegion(pProvince As String, Optional ByRef pErr As String = "")
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsLabourDB.GetDataRegion(pProvince, "-", pUser, pmsg)
        If pmsg = "" Then
            cboRegion.DataSource = ds
            cboRegion.DataBind()
        End If
    End Sub
    Private Sub cboRegion_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboRegion.Callback
        Dim pProvince As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        up_FillComboRegion(pProvince, errmsg)

    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Labour.aspx")
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"

    End Sub

    Protected Sub cbSave_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSave.Callback
        Dim pErr As String = ""
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim Province As String, Region As String, Period As String
        Dim UMSP As Decimal = txtUMSP.Text
        Dim UMR As Decimal = txtUMR.Text
        Dim EstimatedUMR As Decimal = txtEstimatedUMSP.Text
        If pFunction = "Save" Then
            If IsNothing(Request.QueryString("ID")) Then
                ClsLabourDB.InsertLabor(cboProvince.Value, cboRegion.Value, txtPeriod.Text, UMR, EstimatedUMR, cboSectoral.Value, UMSP, pUser, pErr)
                If pErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
                    cbSave.JSProperties("cpButtonText") = "Update"
                Else
                    cbSave.JSProperties("cpMessage") = pErr

                End If
            Else
                Province = Session("Province")
                Region = Session("Region")
                Period = Session("Period")
                Dim Sectoral As String = cboSectoral.SelectedItem.GetValue("Code").ToString()
                ClsLabourDB.UpdateLabor(Province, Region, Period, txtUMR.Text, EstimatedUMR, Sectoral, txtUMSP.Text, pUser, pErr)
                If pErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                    cbSave.JSProperties("cpButtonText") = "Update"
                End If
            End If

        End If

    End Sub
End Class
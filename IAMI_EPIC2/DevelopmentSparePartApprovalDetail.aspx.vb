﻿Imports DevExpress.XtraPrinting
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxPopupControl

Public Class DevelopmentSparePartApprovalDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region


#Region "Procedure"
    Private Sub up_GridLoad(pProjectName As String)
        Dim ErrMsg As String = ""
        'Dim Pro As List(Of clsLabor)
        Dim ds As New DataSet
        ds = ClsIADGSparePartDB.getListDevelopmentPartDetail(pProjectName, pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()

        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_ExcelGridAuto(Optional ByRef pErr As String = "")
        Try
            up_GridLoad(Session("ProjectID"))

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=DevelopmentSparePartApprovalDetail" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using
            ps.Dispose()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("O020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "O020")
        Session("ProjectID") = Request.QueryString("ID").Split("|")(0)

        If Not Page.IsCallback And Not Page.IsPostBack Then
            up_GridLoad(Session("ProjectID"))
        End If

        Dim ds As New DataSet
        ds = ClsIADGSparePartDB.checkStatusApproval(Session("ProjectID"))

        Dim script As String = ""
        If ds.Tables(0).Rows(0)("Status") = "1" Then
            script = "btnApprove.SetEnabled(false);"
            ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", script, True)
            Grid.Enabled = False
        Else
            script = "btnApprove.SetEnabled(true);"
            ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", script, True)
            Grid.Enabled = True
        End If


    End Sub
    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        up_GridLoad(Session("ProjectID"))
    End Sub

    Protected Sub BtnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        'cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        Dim ErrMsg As String = ""
        'up_Excel(ErrMsg)
        up_ExcelGridAuto(ErrMsg)
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        gs_Back = True
        Response.Redirect("DevelopmentSparePartApproval.aspx")
    End Sub

    Protected Sub cbApprove_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbApprove.Callback
        Dim pFunction As String = e.Parameter.Split("|")(0)
        Dim ds As New DataSet
        Dim pErr As String = ""

        If pFunction = "approve" Then
            ds = ClsIADGSparePartDB.developmentPartDetailApproval_Approve(Session("ProjectID"), pUser, pErr)

            If pErr = "" Then
                cbApprove.JSProperties("cpMessage") = "Data Approved Successfully!"
            Else
                cbApprove.JSProperties("cpMessage") = pErr
            End If

        End If
        
    End Sub
End Class
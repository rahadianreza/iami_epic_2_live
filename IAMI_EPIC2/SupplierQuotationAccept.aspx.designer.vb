﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class SupplierQuotationAccept

    '''<summary>
    '''ASPxLabel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel1 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboSupplierCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplierCode As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''txtSupplierName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSupplierName As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''ASPxLabel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel2 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''dtQuotationFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtQuotationFrom As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''ASPxLabel4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel4 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''dtQuotationTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtQuotationTo As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''ASPxLabel3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel3 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboQuotationNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboQuotationNumber As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''btnShow control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnShow As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''btnExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExcel As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''GridExporter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridExporter As Global.DevExpress.Web.ASPxGridView.Export.ASPxGridViewExporter

    '''<summary>
    '''Grid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Grid As Global.DevExpress.Web.ASPxGridView.ASPxGridView

    '''<summary>
    '''det control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents det As Global.DevExpress.Web.ASPxGridView.GridViewCommandColumnCustomButton

    '''<summary>
    '''Master property.
    '''</summary>
    '''<remarks>
    '''Auto-generated property.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As IAMI_EPIC2.Site
        Get
            Return CType(MyBase.Master, IAMI_EPIC2.Site)
        End Get
    End Property
End Class

﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class PRApproval
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fcategroy As String
    Dim fprtype As String

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("B020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "B020")
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            Dim dfrom As Date
            If gs_Back = False Then
                dfrom = Year(Now) & "-" & Month(Now) & "-01"
                ReqDate_From.Value = dfrom
                ReqDate_To.Value = Now
                cboPRType.SelectedIndex = 0
                cboDepartment.SelectedIndex = 0
                cboSection.SelectedIndex = 0
                cboStatus.SelectedIndex = 1
                up_FillComboSection("")

                up_GridHeader("1")
            Else
                ReqDate_From.Value = gs_dtFromBack
                ReqDate_To.Value = gs_dtToBack
                cboPRType.SelectedIndex = gs_PRType
                cboDepartment.SelectedIndex = gs_Department
                cboSection.SelectedIndex = gs_Section 'not defined txtSection cek bind combo nya
                cboStatus.SelectedIndex = 1

                Dim ds As New DataSet
                ds = ClsPRApprovalDB.GetList(Format(ReqDate_From.Value, "yyyy-MM-dd"), Format(ReqDate_To.Value, "yyyy-MM-dd"), gs_PRTypeCode, gs_DepartmentCode, gs_SectionCode, "", cboStatus.Text)

                Grid.DataSource = ds
                Grid.DataBind()

                gs_Back = False
            End If
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_GridHeader(Optional ByRef pLoad As String = "")
        Dim ErrMsg As String = ""
        Dim dtFrom, dtTo As String
        Dim vPRType As String = ""
        Dim vDepartment As String = ""
        Dim vSection As String = ""
        Dim vApproval As String = ""
        Dim ls_Back As Boolean = False

        dtFrom = Format(ReqDate_From.Value, "yyyy-MM-dd")
        dtTo = Format(ReqDate_To.Value, "yyyy-MM-dd")

        If cboPRType.Text <> "" Then
            vPRType = cboPRType.SelectedItem.GetValue("Code").ToString()
        End If

        If cboDepartment.Text <> "" Then
            vDepartment = cboDepartment.SelectedItem.GetValue("Code").ToString()
        End If

        If txtSection.Text = "" Or txtSection.Text = "00" Then
            vSection = "00"
        Else
            vSection = txtSection.Text
        End If
        'If txtSection.Text <> "" Or txtSection.Text <> "00" Then
        '    vSection = cboSection.SelectedItem.GetValue("Code").ToString()
        'End If

        If pLoad = "1" Then
            vSection = "xx"
        End If


        'Grid.Columns.Item(9).Visible = False
        'Grid.Columns.Item(10).Visible = False
        'Grid.Columns.Item(11).Visible = False
        'Grid.Columns.Item(12).Visible = False
        'Grid.Columns.Item(13).Visible = False

        For i = 0 To 4
            Grid.Columns.Item(9 + i).Visible = False
        Next

        Dim ds As New DataSet
        ds = ClsPRApprovalDB.GetHeaderApproval(dtFrom, dtTo, vPRType, vDepartment, vSection, pUser, ErrMsg)

        If ErrMsg = "" Then

            For i = 0 To ds.Tables(0).Rows.Count - 1
                Grid.Columns.Item(9 + i).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                Grid.Columns.Item(9 + i).Visible = True
            Next

        End If
    End Sub

    Private Sub up_FillComboSection(pDepartment As String)
        Dim ds As New DataSet
        Dim pmsg As String = ""


        ds = ClsPRApprovalDB.GetFilterComboSection(pDepartment, pmsg)
        If pmsg = "" Then
            cboSection.DataSource = ds
            cboSection.DataBind()
        End If
    End Sub

    Private Sub up_GridLoad()
        Dim ErrMsg As String = ""
        Dim dtFrom, dtTo As String
        Dim vPRType As String = ""
        Dim vDepartment As String = ""
        Dim vSection As String = ""
        Dim vApproval As String = ""
        Dim vStatus As String = ""
        Dim ls_Back As Boolean = False

        dtFrom = Format(ReqDate_From.Value, "yyyy-MM-dd")
        dtTo = Format(ReqDate_To.Value, "yyyy-MM-dd")

        If cboPRType.Text <> "" Then
            vPRType = cboPRType.SelectedItem.GetValue("Code").ToString()
        End If

      
        If cboDepartment.Text <> "" Then
            vDepartment = cboDepartment.SelectedItem.GetValue("Code").ToString()
        End If

        If txtSection.Text = "" Or txtSection.Text = "00" Then
            vSection = "00"
        Else
            vSection = txtSection.Text
        End If
        'If cboSection.Text <> "" Then
        '    vSection = cboSection.SelectedItem.GetValue("Code").ToString()
        'End If

        'If cboApproval.Text <> "" Then
        '    vApproval = cboApproval.Text
        'End If


        Dim ds As New DataSet
        Dim i As Integer

        'ds = ClsPRApprovalDB.GetApprovalPosition(, ErrMsg)
        'For i = 0 To ds.Tables(0).Rows.Count - 1
        '    Grid.Columns.Item(6 + i).Caption = ds.Tables(0).Rows(i)("Description")
        '    Grid.Columns.Item(6 + i).Visible = True
        'Next

        ds = ClsPRApprovalDB.GetList(dtFrom, dtTo, vPRType, vDepartment, vSection, pUser, cboStatus.Text, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub


    Private Sub up_Excel(Optional ByRef pErr As String = "")

        Try
            'If cboGroupItem.Text <> "" Then
            '    gs_FilterGroup = cboGroupItem.SelectedItem.GetValue("Group_Code").ToString()
            'ElseIf cboGroupItem.Text = "" Then
            '    gs_FilterGroup = ""
            'End If
            'If cboCategory.Text <> "" Then
            '    gs_FilterCategory = cboCategory.Text 'cboCategory.SelectedItem.GetValue("Group_Code").ToString()
            'Else
            '    gs_FilterCategory = ""
            'End If
            'If cboLastSupplier.Text <> "" Then
            '    gs_FilterSupplier = cboLastSupplier.Text 'cboLastSupplier.SelectedItem.GetValue("Group_Code").ToString()
            'End If
            'If cboPRType.Text <> "" Then
            '    gs_FilterPRType = cboPRType.Text 'cboPRType.SelectedItem.GetValue("Group_Code").ToString()
            'End If

            up_GridLoad()

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=PRApproval" & Format(Date.Now, "ddMMyyyyHHmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            pErr = ex.Message
        End Try
    End Sub
#End Region

#Region "Control Event"
    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs)
        up_GridLoad()
    End Sub

    'Protected Sub btnShowData_Click(sender As Object, e As EventArgs) Handles btnShowData.Click
    '    up_GridLoad()

    '    gs_dtFromBack = ReqDate_From.Value
    '    gs_dtToBack = ReqDate_To.Value
    '    gs_PRType = cboPRType.SelectedIndex
    '    gs_PRTypeCode = cboPRType.SelectedItem.GetValue("Code").ToString()
    '    gs_Department = cboDepartment.SelectedIndex
    '    gs_DepartmentCode = cboDepartment.SelectedItem.GetValue("Code").ToString()
    '    gs_Section = cboSection.SelectedIndex
    '    gs_SectionCode = cboSection.SelectedItem.GetValue("Code").ToString()
    'End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pDateFrom As Date = Split(e.Parameters, "|")(1)
        Dim pDateTo As Date = Split(e.Parameters, "|")(2)
        Dim pPRType As String = Split(e.Parameters, "|")(3)
        Dim pDepartment As String = Split(e.Parameters, "|")(4)
        Dim pSection As String = Split(e.Parameters, "|")(5)

        Dim vDateFrom As String = Format(pDateFrom, "yyyy-MM-dd")
        Dim vDateTo As String = Format(pDateTo, "yyyy-MM-dd")

        gs_DateFrom = vDateFrom
        gs_DateTo = vDateTo
        gs_PRTypeCode = pPRType
        gs_DepartmentCode = pDepartment
        gs_SectionCode = pSection

        gs_dtFromBack = pDateFrom
        gs_dtToBack = pDateTo

        gs_PRType = cboPRType.SelectedIndex
        gs_Department = cboDepartment.SelectedIndex
        gs_Section = cboSection.SelectedIndex

        If pFunction = "gridload" Then

            up_GridHeader()
            Dim ds As New DataSet
            ds = ClsPRApprovalDB.GetList(vDateFrom, vDateTo, pPRType, pDepartment, pSection, pUser, cboStatus.Text)

            Grid.DataSource = ds
            Grid.DataBind()
        End If
    End Sub


    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        Dim ErrMsg As String = ""
        up_Excel()
        'up_ExcelGridAuto(ErrMsg)

        If ErrMsg <> "" Then
            cbMessage.JSProperties("cpMessage") = ErrMsg
        Else
            cbMessage.JSProperties("cpMessage") = "Download Excel Successfully"
        End If
    End Sub

    Private Sub cboSection_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSection.Callback
        Dim pFilter As String = Split(e.Parameter, "|")(0)
        up_FillComboSection(pFilter)
        If cboSection.Items.Count > 0 Then
            cboSection.SelectedIndex = 0
        End If
    End Sub

#End Region


End Class
﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data

Public Class Material_Detail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""

    Dim strMaterialCode As String = ""
    Dim strMaterialName As String = ""
    Dim strCountry As String = ""
    Dim strMaterialType As String = ""
    Dim strGroupItem As String = ""
    Dim strCategory As String = ""
#End Region

    Public Sub FillCombo(Optional ByRef pMaterialType = "", Optional ByRef pGroupItem = "")
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsMst_MaterialDB.GetDataCombo("MaterialType", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboMaterialType.Items.Add(Trim(ds.Tables(0).Rows(i)("MaterialTypeCode") & ""))
            Next
        End If

        Dim dsGroup As DataSet
        dsGroup = clsMst_MaterialDB.GetDataCombo("GroupItem", pMaterialType, pErr)
        If pErr = "" Then
            ' For i = 0 To dsGroup.Tables(0).Rows.Count - 1
            cboGroupItem.DataSource = dsGroup
            cboGroupItem.DataBind()  'Items.Add(Trim(dsGroup.Tables(0).Rows(i)("GroupItem") & ""))
            'Next
        End If

        Dim dsCategory As DataSet
        dsCategory = clsMst_MaterialDB.GetDataCombo("Category", pGroupItem, pErr)
        If pErr = "" Then
            cboCategory.DataSource = dsCategory
            cboCategory.DataBind()
            'For i = 0 To dsCategory.Tables(0).Rows.Count - 1
            '    cboCategory.Items.Add(Trim(dsCategory.Tables(0).Rows(i)("Category") & ""))
            'Next
        End If

        Dim dsCountry As DataSet
        dsCountry = clsMst_MaterialDB.GetDataCombo("Country", pErr)
        If pErr = "" Then
            For i = 0 To dsCountry.Tables(0).Rows.Count - 1
                cboCountryCls.Items.Add(Trim(dsCountry.Tables(0).Rows(i)("Country_Cls") & ""))
            Next
        End If

        Dim dsUOM As DataSet
        dsUOM = clsMst_MaterialDB.GetDataCombo("UOM", pErr)
        If pErr = "" Then
            For i = 0 To dsUOM.Tables(0).Rows.Count - 1
                cboUOM.Items.Add(Trim(dsUOM.Tables(0).Rows(i)("UOM") & ""))
            Next
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A160")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        ' statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        Dim str As String = Request.QueryString("ID")
        Dim script As String = ""
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If str <> "" Then
                btnSubmit.Text = "Update"
                script = "txtMaterialCode.SetEnabled(false);"

                '"txtMaterialName.SetEnabled(false);" & vbCrLf & _
                '"cboMaterialType.SetEnabled(false);" & vbCrLf & _
                '"cboGroupItem.SetEnabled(false);" & vbCrLf & _
                '"cboCategory.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(txtMaterialCode, txtMaterialCode.GetType(), "txtMaterialCode", script, True)
                ScriptManager.RegisterStartupScript(btnClear, btnClear.GetType(), "btnClear", "btnClear.SetEnabled(false);", True)

                strMaterialCode = str.Split("|")(0)
                strMaterialType = str.Split("|")(1)
                strGroupItem = str.Split("|")(2)
                FillCombo(strMaterialType, strGroupItem)

                Dim ds As DataSet = clsMst_MaterialDB.getDetailMaterial(strMaterialCode)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtMaterialCode.Text = Trim(ds.Tables(0).Rows(0)("Material_Code") & "")
                    txtMaterialName.Text = Trim(ds.Tables(0).Rows(0)("Material_Name") & "")
                    cboCountryCls.SelectedIndex = cboCountryCls.Items.IndexOf(cboCountryCls.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Country_Cls") & "")))
                    cboMaterialType.SelectedIndex = cboMaterialType.Items.IndexOf(cboMaterialType.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("MaterialTypeCode") & "")))
                    cboGroupItem.SelectedIndex = cboGroupItem.Items.IndexOf(cboGroupItem.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("GroupItem") & "")))
                    cboCategory.SelectedIndex = cboCategory.Items.IndexOf(cboCategory.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Category") & "")))
                    cboUOM.SelectedIndex = cboUOM.Items.IndexOf(cboUOM.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("UOM") & "")))

                End If
            Else
                script = "txtMaterialCode.SetEnabled(true);"
                '"txtMaterialName.SetEnabled(true);" & vbCrLf & _
                '"cboMaterialType.SetEnabled(true);" & vbCrLf & _
                '"cboGroupItem.SetEnabled(true);" & vbCrLf & _
                '"cboCategory.SetEnabled(true);"

                ScriptManager.RegisterStartupScript(txtMaterialCode, txtMaterialCode.GetType(), "txtMaterialCode", script, True)
            End If
        End If

    End Sub

    Private Sub cboGroupItem_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupItem.Callback
        Dim pMaterialType As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsMst_MaterialDB.GetDataCombo("GroupItem", pMaterialType, pmsg)
        If pmsg = "" Then
            cboGroupItem.DataSource = ds
            cboGroupItem.DataBind()

        End If

    End Sub

    Private Sub cboCategory_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCategory.Callback
        Dim pGroupItem As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsMst_MaterialDB.GetDataCombo("Category", pGroupItem, pmsg)
        If pmsg = "" Then
            cboCategory.DataSource = ds
            cboCategory.DataBind()

        End If

    End Sub

    ' Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
    '    Response.Redirect("Material_List.aspx")
    'End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"

    End Sub

    Protected Sub cbSave_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSave.Callback
        Dim pErr As String = ""
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim vMaterialCode As String = Split(e.Parameter, "|")(1)
        Dim vMaterialName As String = Split(e.Parameter, "|")(2)
        Dim vMaterialType As String = Split(e.Parameter, "|")(3)
        Dim vGroupItem As String = Split(e.Parameter, "|")(4)
        Dim vCategory As String = Split(e.Parameter, "|")(5)
        Dim vCountry As String = Split(e.Parameter, "|")(6)
        Dim vUOM As String = IIf(Split(e.Parameter, "|")(7) = Nothing, "", Split(e.Parameter, "|")(7))

        If pFunction = "Save" Then
            'If IsNothing(Request.QueryString("ID")) Then
            clsMst_MaterialDB.InsertDataMaterial(vMaterialCode, vMaterialName, vMaterialType, vGroupItem, vCategory, vCountry, vUOM, pUser, pErr)
            If pErr = "" Then
                cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
                cbSave.JSProperties("cpButtonText") = "Update"
            Else
                cbSave.JSProperties("cpMessage") = pErr
            End If
        Else
            clsMst_MaterialDB.UpdateDataMaterial(vMaterialCode, vMaterialName, vMaterialType, vGroupItem, vCategory, vCountry, vUOM, pUser, pErr)
            If pErr = "" Then
                cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                cbSave.JSProperties("cpButtonText") = "Update"
            Else
                cbSave.JSProperties("cpMessage") = pErr
            End If
            'If IsNothing(Request.QueryString("ID")) Then
            '    clsMst_MaterialDB.UpdateDataMaterial(vMaterialCode, vMaterialName, vMaterialType, vGroupItem, vCategory, pUser, pErr)
            '    If pErr = "" Then
            '        cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
            '        cbSave.JSProperties("cpButtonText") = "Update"
            '    Else
            '        cbSave.JSProperties("cpMessage") = pErr

            '    End If
            'Else
            '    Dim str As String = Request.QueryString("ID")
            '    strMaterialCode = str.Split("|")(0)
            '    strMaterialName = str.Split("|")(1)
            '    strMaterialType = str.Split("|")(2)
            '    strGroupItem = str.Split("|")(3)
            '    strCategory = str.Split("|")(4)


            '    clsAll_MaterialDB.UpdateDataRubber(vMaterialType, vSupplier, vPeriod, vMaterialCode, vGroupItem, vCategory, vCurrency, vPrice, pUser, pErr)
            '    If pErr = "" Then
            '        cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
            '        cbSave.JSProperties("cpButtonText") = "Update"
            '    End If
            'End If
        End If
    End Sub
End Class
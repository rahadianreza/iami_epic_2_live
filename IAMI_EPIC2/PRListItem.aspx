﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"  MaintainScrollPositionOnPostback="true"
    CodeBehind="PRListItem.aspx.vb" Inherits="IAMI_EPIC2.PRListItem" ViewStateMode="Enabled" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
      window.onload = function () {
          var GetDocumentScrollTop = function () {
              var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollTop > 0;
              if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                  if (ASPx.Browser.MacOSMobilePlatform)
                      return window.pageYOffset;
                  else if (ASPx.Browser.WebKitFamily)
                      return document.documentElement.scrollTop || document.body.scrollTop;
                  return document.body.scrollTop;
              }
              else
                  return document.documentElement.scrollTop;
          };
          var _aspxGetDocumentScrollTop = function () {
              if (__aspxWebKitFamily) {
                  if (__aspxMacOSMobilePlatform)
                      return window.pageYOffset;
                  else
                      return document.documentElement.scrollTop || document.body.scrollTop;
              }
              else
                  return document.documentElement.scrollTop;
          }
          if (window._aspxGetDocumentScrollTop) {
              window._aspxGetDocumentScrollTop = _aspxGetDocumentScrollTop;
              window.ASPxClientUtils.GetDocumentScrollTop = _aspxGetDocumentScrollTop;
          } else {
              window.ASPx.GetDocumentScrollTop = GetDocumentScrollTop;
              window.ASPxClientUtils.GetDocumentScrollTop = GetDocumentScrollTop;
          }
          /* Begin -> Correct ScrollLeft  */
          var GetDocumentScrollLeft = function () {
              var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollLeft > 0;
              if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                  if (ASPx.Browser.MacOSMobilePlatform)
                      return window.pageXOffset;
                  else if (ASPx.Browser.WebKitFamily)
                      return document.documentElement.scrollLeft || document.body.scrollLeft;
                  return document.body.scrollLeft;
              }
              else
                  return document.documentElement.scrollLeft;
          };
          var _aspxGetDocumentScrollLeft = function () {
              if (__aspxWebKitFamily) {
                  if (__aspxMacOSMobilePlatform)
                      return window.pageXOffset;
                  else
                      return document.documentElement.scrollLeft || document.body.scrollLeft;
              }
              else
                  return document.documentElement.scrollLeft;
          }
          if (window._aspxGetDocumentScrollLeft) {
              window._aspxGetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
              window.ASPxClientUtils.GetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
          } else {
              window.ASPx.GetDocumentScrollLeft = GetDocumentScrollLeft;
              window.ASPxClientUtils.GetDocumentScrollLeft = GetDocumentScrollLeft;
          }
          /* End -> Correct ScrollLeft  */
      };
    </script>
    <script type="text/javascript">

        function GetMessage(s, e) {
            lblCount.SetText(ASPxDataView1.cpCount);
        }

        function DataViewLoad() {
            ASPxDataView1.PerformCallback('refresh');
        }


        //var keyValue;
        function OnMoreInfoClick(s, key) {
            callbackPanel.SetContentHtml("");
            popup.ShowAtElement(s.mainElement);
        }

        function popup_Shown(value) {
            callbackPanel.PerformCallback(value);
        }

        function SetSelection(s, e) {
            if (cboPRType.GetText() == '') {
                toastr.warning('Please Select PR Type!', 'Warning');
                cboPRType.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            if (s.cpMessage == '' || s.cpMessage == undefined) {
                e.processOnServer = false;
                return;
            }

            if (s.cpMessage == 'Click Show Data First!') {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (s.cpMessage == 'No Data to Draft, Please Select Material No !') {

                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else {
                window.location.href = 'PRListDetail.aspx?MatNo=' + s.cpMessage;
            }

        }

        function FilterGroupItem() {
            cboGroupItem.PerformCallback('filter2|' + cboPRType.GetValue());
            lblCount.SetText('0');
        }

        function FilterCategory() {
            cboCategory.PerformCallback('filter|' + cboGroupItem.GetValue() + '|' + cboPRType.GetValue());
        }

        function OnComboBoxKeyDown(s, e) {
            // 'Delete' button key code = 46

            if (e.htmlEvent.keyCode == 46 || e.htmlEvent.keyCode == 8)
                s.SetSelectedIndex(-1);
            //alert(s.GetValue());
        }

    </script>
    <style type="text/css">
        .templateTable
        {
            border-collapse: collapse;
            width: 100%;
        }
        .templateTable td
        {
            border: solid 1px #C2D4DA;
            padding: 6px;
        }
        .templateTable td.value
        {
            font-weight: bold;
        }
        .imageCell
        {
            width: 210px;
            height: 190px;
        }
        .style1
        {
            height: 50px;
        }
        .hidden-div
        {
            display: none;
        }
        .setMargin
        {
            margin-top: -10px;
        }
        
        .aspxImage
        {
            border-radius: 5px;
            Width:160px; 
            Height:110px;
        }
        
        /* Add a hover effect (blue shadow) */
        .aspxImage:hover 
        {
          
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
         
        }
        
              
        .container {
          position: relative;
         
          color: white;
        }
        
        .checkbox-left {
          position: absolute;
          transform: translate(-10%, -10%);
        }
        


       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black;" border="0px">
                <tr style="height: 2px" >
                    <td ></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td></td>
                    <td></td>
                   
                </tr>
                <tr style="height: 10px">
                    <td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="PR Type">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="width: 110px">
                        <dx1:ASPxComboBox ID="cboPRType" runat="server" ClientInstanceName="cboPRType" Width="150px"
                            Font-Names="Segoe UI" DataSourceID="SqlDataSource2" TextField="Code" ValueField="Code"
                            TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDownList"
                            IncrementalFilteringMode="Contains"  Height="25px">
                            <ClientSideEvents ValueChanged="FilterGroupItem" KeyDown="OnComboBoxKeyDown" />
                            <%--SelectedIndexChanged="DataViewLoad" --%>
                            <Columns>
                                <dx:ListBoxColumn Caption="PR Type" FieldName="Code" Width="60px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="160px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="width: 100px; padding: 0px 0px 0px 10px">
                        <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Category Item">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="width: 180px">
                        <dx1:ASPxComboBox ID="cboCategory" runat="server" ClientInstanceName="cboCategory"
                            Width="150px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDownList"
                            IncrementalFilteringMode="Contains"  Height="25px">
                            <ClientSideEvents ValueChanged="function(s, e) {
	                            txtCategory.SetText(cboCategory.GetValue());
                            }" />
                            <%-- SelectedIndexChanged="DataViewLoad"--%>
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="width: 70px">
                        <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Keyword">
                        </dx1:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td>
                        <dx1:ASPxTextBox ID="txtKeyword" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="150px" ClientInstanceName="txtKeyword" MaxLength="30" Height="25px">
                            <%--<ClientSideEvents TextChanged="DataViewLoad" />--%>
                        </dx1:ASPxTextBox>
                    </td>
                    <td style="width: 20px">
                        
                    </td>
                </tr>
                <tr style="height:10px">
                    <td style="padding: 0px 0px 0px 10px" class="style1">
                        <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Group Item">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="style1">
                    </td>
                    <td class="style1">
                        <dx1:ASPxComboBox ID="cboGroupItem" runat="server" ClientInstanceName="cboGroupItem"
                            Width="150px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDownList"
                            IncrementalFilteringMode="Contains"  Height="25px">
                            <ClientSideEvents ValueChanged="FilterCategory" KeyDown="OnComboBoxKeyDown" />
                            <%--SelectedIndexChanged="DataViewLoad"--%>
                            <Columns>
                                <dx:ListBoxColumn Caption="Group Item" FieldName="Code" Width="60px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="160px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td class="style1">
                    </td>
                    <td style="width: 100px; padding: 0px 0px 0px 10px">
                        <dx1:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Last Supplier">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="width: 180px">
                        <dx1:ASPxComboBox ID="cboLastSupplier" runat="server" ClientInstanceName="cboLastSupplier"
                            Width="150px" Font-Names="Segoe UI" DataSourceID="SqlDataSource3" TextField="Code"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            DropDownStyle="DropDownList" IncrementalFilteringMode="Contains"
                            Height="25px">
                            <ClientSideEvents KeyDown="OnComboBoxKeyDown" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Last Supplier" FieldName="Code" Width="80px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="160px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="height: 30px">
                    <td style="padding: 0px 0px 0px 10px" colspan="5">
                        <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnBack" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;&nbsp;
                        <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShowData" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {                                                
                                                    if (cboPRType.GetText() == '') {
                                                        //alert('Please Select PR Type!');
                                                        toastr.warning('Please Select PR Type!', 'Warning');
                                                        cboPRType.Focus();
                                                        toastr.options.closeButton = false;
                                                        toastr.options.debug = false;
                                                        toastr.options.newestOnTop = false;
                                                        toastr.options.progressBar = false;
                                                        toastr.options.preventDuplicates = true;
                                                        toastr.options.onclick = null;
                                                        e.processOnServer = false;
                                                        return;
                                                    }
                                                    
	                                                ASPxDataView1.PerformCallback('showdata'+'|'+cboPRType.GetValue()+'|'+cboGroupItem.GetValue()+'|'+cboCategory.GetValue()+'|'+cboLastSupplier.GetValue());
                                                    cbShowMore.PerformCallback();
                                                }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;&nbsp;
                        <dx:ASPxButton ID="btnDraft" runat="server" Text="Create" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnDraft" Theme="Default">
                            <%-- <ClientSideEvents Click="SetSelection" />
                            --%>
                            <ClientSideEvents Click="function(s, e) {
                             
	cbDraft.PerformCallback();
    //alert(s.cpMessage);
   
}" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
                <tr style="height: 2px" >
                    <td ></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td></td>
                    <td></td>
                   
                </tr>
            </table>
        </div>
        <div class="hidden-div">
            <dx:ASPxTextBox ID="txtFilter" runat="server" Width="170px" ClientInstanceName="txtFilter"></dx:ASPxTextBox>
            <dx:ASPxTextBox ID="txtCategory" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="80px" ClientInstanceName="txtCategory" MaxLength="15" ClientVisible="False">
            </dx:ASPxTextBox>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <asp:HiddenField ID="SelectedData" runat="server" />
            <dx:ASPxCallback ID="cbDraft" runat="server" ClientInstanceName="cbDraft">
                <ClientSideEvents EndCallback="SetSelection" />
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbShowMore" runat="server" ClientInstanceName="cbShowMore">
                <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
            </dx:ASPxCallback>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="SELECT 'ALL' Code,'ALL' Description UNION ALL SELECT DISTINCT COALESCE(Par_Code,'')Code,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'GroupItem'">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="SELECT DISTINCT COALESCE(Par_Code,'')Code,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'PRType'">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="SELECT 'ALL' Code,'ALL' Description UNION ALL SELECT DISTINCT COALESCE(Supplier_Code,'')Code,Description = Supplier_Name FROM dbo.Mst_Supplier">
            </asp:SqlDataSource>
        </div>
        <div style="padding: 5px 5px 5px 5px ; ">
            <b>&nbsp; &nbsp; Total Count &nbsp; : </b>
            
            <dx1:ASPxLabel ID="lblCount" runat="server" ClientInstanceName="lblCount" Font-Names="Segoe UI"
                Font-Size="9pt" Font-Bold="true">
            </dx1:ASPxLabel>
        </div>
        <div style="padding: 5px 5px 5px 5px; height:100%;">
            <dx:ASPxDataView ID="ASPxDataView1" ClientInstanceName="ASPxDataView1" runat="server"
                SettingsTableLayout-ColumnCount="5" AllowPaging="False" Width="100%" Border-BorderStyle="Solid"
                Border-BorderWidth="1px" PagerAlign="Justify" Paddings-PaddingLeft="5px" 
                Height="100%" ColumnCount="5">

<Paddings PaddingLeft="5px"></Paddings>

                <ItemStyle Height="1px" BackColor="Transparent"  Border-BorderColor="AliceBlue">
                   
<Border BorderColor="AliceBlue"></Border>
                   
                </ItemStyle>
                <EmptyDataTemplate>
                    <table runat="server">
                        <tr>
                            <td style="height:160px; text-align:center" >
                                 <dx:ASPxLabel ID="lblInfo" runat="server" Font-Names="Segoe UI" Font-Size="20pt"
                                    Font-Bold="true" Text="No matching items found."  />
                                <%--<dx1:ASPxBinaryImage ID="emptyImage" runat="server" AlternateText="No Data Available" EmptyImage-Url="~/Styles/images/Default.png"></dx1:ASPxBinaryImage>--%>
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <table style="margin: auto; color: Black;">
                        <tr style="width: 100%">
                            <td colspan="2" align="left">
                                <%-- <dx:ASPxCheckBox ID="checkBox" runat="server" ClientInstanceName="checkBox"   ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="" Font-Names="Segoe UI" Font-Size="8pt" ForeColor="White" > 
                            </dx:ASPxCheckBox>--%>
                                <div class="checkbox-left">
                                          <dx:ASPxCheckBox ID="cb" runat="server" OnInit="cb_Init" Font-Names="Segoe UI" Font-Size="8pt"
                                                ForeColor="White" ValueType="System.String"   >
                                          </dx:ASPxCheckBox>
                                 </div>
                            </td>
                        </tr>
                        <tr>
                           
                            <td colspan="3" align="left">
                                <div >
                                    <dx:ASPxBinaryImage ID="imageBinary" runat="server" Value='<%# Eval("ImageBinary") %>' ToolTip='<%# Eval("Specification") %>'
                                         EmptyImage-Url="~/Styles/images/Default.png" OnInit="image_Init" CssClass="aspxImage"  >
                                    </dx:ASPxBinaryImage>
                               </div>
                             
                            </td>
                        </tr>
                      <%--  <tr>
                            <td>
                                <hr style="border: solid 2px; margin-top: 8px; margin-bottom: 5px;" />
                            </td>
                        </tr>--%>
                        <tr>
                            <%--<td align="left" width="50px">
                               <dx:ASPxLabel ID="lblMaterialNo" runat="server" Font-Names="Segoe UI" Font-Size="7pt"
                                    Text="Material No"></dx:ASPxLabel>
                            </td>
                            <td width="1px">
                            </td>--%>
                            <td align="center">
                                <dx:ASPxLabel ID="txtMaterialNo" runat="server" Font-Names="Segoe UI" Font-Size="6pt"
                                    Font-Bold="true" Text='<%# Eval("Material_No") %>' CssClass="setMargin" />
                            </td>
                        </tr>
                        <tr>
                            <%-- <td align="left" valign="top">
                                <dx:ASPxLabel ID="lblDescs" runat="server" Font-Names="Segoe UI" Font-Size="7pt"
                                    Text="Description"></dx:ASPxLabel> 
                            </td>
                            <td>
                            </td>--%>
                            <td rowspan="3" align="center">
                                <dx:ASPxLabel ID="txtDescs" runat="server" Font-Names="Segoe UI" Font-Size="6pt"
                                    Font-Bold="true" Text='<%# Eval("Description") %>' />
                            </td>
                        </tr>
                        <tr>
                            <%--<td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>--%>
                        </tr>
                    </table>
                </ItemTemplate>
                
<SettingsTableLayout ColumnCount="5"></SettingsTableLayout>

                <PagerSettings ShowNumericButtons="true">
                    <AllButton Visible="False" />
                    <Summary Visible="false" />
                    <PageSizeItemSettings ShowAllItem="true" />
                </PagerSettings>

<Border BorderStyle="Solid" BorderWidth="1px"></Border>
            </dx:ASPxDataView>
            <%--<div style="text-align:center">--%>
            <table runat="server" style="width:100%">
                <tr>
                <td width="15%"></td>
                <td width="50%" align="center">
                <dx:ASPxButton ID="btnShowMore" runat="server" Text="Show more items..." UseSubmitBehavior="False"
                    RenderMode="Link" Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                    AutoPostBack="False" ClientInstanceName="btnShowMore" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {                                                
                                                    if (cboPRType.GetText() == '') {
                                                        alert('Please Select PR Type!');
                                                        //toastr.warning('Please Select PR Type!', 'Warning');
                                                        cboPRType.Focus();
                                                        //toastr.options.closeButton = false;
                                                        //toastr.options.debug = false;
                                                        //toastr.options.newestOnTop = false;
                                                        //toastr.options.progressBar = false;
                                                        //toastr.options.preventDuplicates = true;
                                                        //toastr.options.onclick = null;
                                                        e.processOnServer = false;
                                                        return;
                                                    }
                                                    var showTop = '10';
                                                    
	                                                ASPxDataView1.PerformCallback('showMore'+'|'+cboPRType.GetValue()+'|'+cboGroupItem.GetValue()+'|'+cboCategory.GetValue()+'|'+cboLastSupplier.GetValue());
                                                    cbShowMore.PerformCallback();
                                                   
                                                }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                </td>
                <td width="20%"></td>
                </tr>
                </table>
            </div>
            <dx:ASPxHiddenField ID="hf1" ClientInstanceName="hf1" runat="server">
            </dx:ASPxHiddenField>
        </div>

        <div style="padding: 5px 5px 5px 5px; position:relative;">
            <dx:ASPxPopupControl ID="popup" ClientInstanceName="popup" runat="server" AllowDragging="true" 
                ShowPageScrollbarWhenModal="false"  Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                HeaderText="Details" Width="550px" Height="280px"   >
                 <ClientSideEvents Shown="function(s, e) {s.UpdatePosition();}" />
                <ContentCollection>
                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                        <dx1:ASPxCallbackPanel ID="callbackPanel" ClientInstanceName="callbackPanel" runat="server"
                            Width="530px" Height="280px" OnCallback="callbackPanel_Callback" RenderMode="Table">
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent1" runat="server">
                                    <table class="InfoTable" style="width: 100%; height: 100px;">
                                        <tr style="height: 25px">
                                            <td width="100px">
                                                <dx:ASPxLabel ID="lblMaterial" ClientInstanceName="lblMaterial" runat="server" Text="Material No"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="8px">
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtMaterialNo" runat="server" Width="200px" Font-Names="Segoe UI"
                                                    Font-Size="9pt" ClientInstanceName="txtDescription" Enabled="false" Border-BorderColor="Black">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td>
                                                <dx:ASPxLabel ID="lblDescription" runat="server" ClientInstanceName="lblDescription"
                                                    Text="Description" Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="8px">
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtDescription" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                    Width="280px" ClientInstanceName="txtDescription" Height="16px" MaxLength="255"
                                                    Enabled="false" Border-BorderColor="Black">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td>
                                                <dx:ASPxLabel ID="lblSpecification" runat="server" ClientInstanceName="lblSpecification"
                                                    Text="Specification" Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="8px">
                                            </td>
                                            <td>
                                                <dx:ASPxMemo ID="txtSpecification" runat="server" Height="50px" Width="400px" ClientInstanceName="txtSpecification"
                                                    Font-Names="Segoe UI" Font-Size="9pt" Theme="Metropolis" MaxLength="500" Enabled="false"
                                                    Border-BorderColor="Black">
                                                </dx:ASPxMemo>
                                            </td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td>
                                                <dx:ASPxLabel ID="lblPRType" runat="server" ClientInstanceName="lblPRType" Text="PR Type"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="8px">
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtPRType" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                    Width="280px" ClientInstanceName="txtPRType" Height="16px" Enabled="false" Border-BorderColor="Black">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td>
                                                <dx:ASPxLabel ID="lblGroupItem" runat="server" ClientInstanceName="lblGroupItem"
                                                    Text="Group Item" Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="8px">
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtGroupItem" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                    Width="280px" ClientInstanceName="txtGroupItem" Height="16px" Enabled="false"
                                                    Border-BorderColor="Black">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td>
                                                <dx:ASPxLabel ID="lblCategory" runat="server" ClientInstanceName="lblCategory" Text="Category Item"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="8px">
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtCategoryItem" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                    Width="280px" ClientInstanceName="txtCategoryItem" Height="16px" Enabled="false"
                                                    Border-BorderColor="Black">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td>
                                                <dx:ASPxLabel ID="lblUOM" runat="server" ClientInstanceName="lblUOM" Text="UOM" Font-Names="Segoe UI"
                                                    Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="8px">
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtUOM" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                    Width="200px" ClientInstanceName="txtUOM" Height="16px" Enabled="false" Border-BorderColor="Black">
                                                </dx:ASPxTextBox>
                                            </td>
                                           
                                        </tr>
                                        <tr style="height:5px">
                                            <td colspan="9"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="3" style="background-color:ActiveCaption">
                                                <dx1:ASPxLabel ID ="lblHistory" runat="server" ClientInstanceName="lblHistory" Text="HISTORY" Font-Bold="true"
                                                               Font-Size="10pt" Font-Names="Segoe UI"  >
                                                </dx1:ASPxLabel>
                                               
                                            </td>
                                        </tr>
                                        <tr style="height:5px">
                                            <td colspan="9"></td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td>
                                                <dx:ASPxLabel ID="lblLastIAPrice" runat="server" ClientInstanceName="lblLastIAPrice" Text="Last IA Price" Font-Names="Segoe UI"
                                                    Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="8px"></td>
                                            <td>
                                                 <dx:ASPxTextBox ID="txtIAPrice" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Height="16px"
                                                    Width="200px" ClientInstanceName="txtIAPrice" MaxLength="18" HorizontalAlign="Right" Border-BorderColor="Black">
                                                    <MaskSettings Mask="<0..10000000g>" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr style="height:25px">
                                            <td>
                                                <dx:ASPxLabel ID="lblLastIADate" runat="server" ClientInstanceName="lblLastIADate" Text="Last IA Date" Font-Names="Segoe UI"
                                                    Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="8px"></td>
                                            <td>
                                                 <dx:ASPxDateEdit ID="dtLastIADate" runat="server" Theme="Office2010Black" Width="120px"
                                                    ClientInstanceName="dtLastIADate" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                                    Font-Names="Segoe UI" Font-Size="9pt" Height="16px" Border-BorderColor="Black"  Enabled="false">
                                                    <CalendarProperties>
                                                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                                        <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                                        </WeekNumberStyle>
                                                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                                        </ButtonStyle>
                                                    </CalendarProperties>
                                                    <ButtonStyle Width="4px" Paddings-Padding="4px">
                                                    </ButtonStyle>
                                                </dx:ASPxDateEdit>
                                            </td>
                                        </tr>
                                        <tr style="height: 25px">
                                            <td>
                                                <dx:ASPxLabel ID="lblLastSupplier" runat="server" ClientInstanceName="lblLastSupplier" Text="Last Supplier" Font-Names="Segoe UI"
                                                    Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="8px">
                                            </td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtLastSupplier" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                    Width="200px" ClientInstanceName="txtLastSupplier" Height="16px" Enabled="false" Border-BorderColor="Black">
                                                </dx:ASPxTextBox>
                                            </td>
                                           
                                        </tr>
                                        <tr style="height:5px">
                                            <td colspan="9"></td>
                                        </tr>
                                    </table>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx1:ASPxCallbackPanel>
                    </dx:PopupControlContentControl>
                </ContentCollection>
                <%-- <ClientSideEvents Shown="popup_Shown" />--%>
            </dx:ASPxPopupControl>
        </div>
    </div>
</asp:Content>

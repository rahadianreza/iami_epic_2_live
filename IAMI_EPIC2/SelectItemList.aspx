﻿<%@ Page Title="IAMI_EPIC2" Page Language="vb" AutoEventWireup="false" CodeBehind="SelectItemList.aspx.vb" Inherits="IAMI_EPIC2.SelectItemList" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" KeyFieldName="StaffID" Theme="Office2010Black"
            Width="100%" Font-Names="Segoe UI" Font-Size="9pt"
            OnAfterPerformCallback="Grid_AfterPerformCallback">
            <ClientSideEvents SelectionChanged="SelectionChanged" />
            <Columns>
                <dx:GridViewDataTextColumn Caption="No" FieldName="No"
                    VisibleIndex="1" Width="20px" Settings-AutoFilterCondition="Contains">
<Settings AutoFilterCondition="Contains"></Settings>

                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="9pt"></HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="9pt"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Material No" FieldName="MaterialNo"
                    VisibleIndex="2" Width="70px" Settings-AutoFilterCondition="Contains">
<Settings AutoFilterCondition="Contains"></Settings>

                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="9pt"></HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="9pt"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description"
                    VisibleIndex="3" Width="150px" Settings-AutoFilterCondition="Contains">
<Settings AutoFilterCondition="Contains"></Settings>

                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="9pt"></HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="9pt"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Specification" FieldName="Specification"
                    VisibleIndex="4" Width="150px" Settings-AutoFilterCondition="Contains">
<Settings AutoFilterCondition="Contains"></Settings>

                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="9pt"></HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="9pt"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Qty" FieldName="Qty"
                    VisibleIndex="5" Width="40px" Settings-AutoFilterCondition="Contains">
<Settings AutoFilterCondition="Contains"></Settings>

                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="9pt"></HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="9pt"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="UOM" FieldName="UOM"
                    VisibleIndex="7" Width="45px" Settings-AutoFilterCondition="Contains">
<Settings AutoFilterCondition="Contains"></Settings>

                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" Font-Size="9pt"></HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Font-Size="9pt"></CellStyle>
                </dx:GridViewDataTextColumn>
            </Columns>

            <SettingsBehavior FilterRowMode="Auto" />
            <Settings ShowFilterRow="true" />
        </dx:ASPxGridView>
    </div> 
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%;">
            <tr>
                <td>
                    <asp:HiddenField ID="SelectedData" runat="server" />   
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td align="right" style="width: 100px">
                    <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" UseSubmitBehavior="false"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="100px" Height="25px" AutoPostBack="false"
                        ClientInstanceName="btnSubmit" Theme="Default">
                        <ClientSideEvents Click="SetSelection" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 1px">
                    &nbsp;</td>
                <td style="width: 100px">
                    <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" UseSubmitBehavior="false"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="100px" Height="25px" AutoPostBack="false"
                        ClientInstanceName="btnCancel" Theme="Default">
                        <ClientSideEvents Click="function close() {
                            window.close();
                        }" />
                    </dx:ASPxButton>
                </td>
            </tr>
            </table>
    </div>
    </form>
</body>
</html>

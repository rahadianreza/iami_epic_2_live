﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddExchangeRate.aspx.vb" Inherits="IAMI_EPIC2.AddExchangeRate" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxImageZoom" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" >
        function Message(paramType, paramMessage) {
            if (paramType == 0) { //Info
                toastr.info(paramMessage, 'Info');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 1) { //Success
                toastr.success(paramMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 2) { //Warning
                toastr.warning(paramMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 3) { //Error
                toastr.error(paramMessage, 'Error');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function formatDateISO(date) {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        function cbAction_CallbackComplete(s, e) {
            var result = e.result;
            if (result == 'insert') {
                Message(s.cp_type, s.cp_message);
                if (s.cp_type == 1) {
                    cboBankCode.SetValue('');
                    txtBankDesc.SetText('');
                    cboCurrencyCode.SetValue('');
                    txtCurrencyDesc.SetText('');
                    dtExchangeRateDate.SetValue(null);
                    txtBuyingRate.SetValue(0);
                    txtMiddleRate.SetValue(0);
                    txtSellingRate.SetValue(0);
                    txtRemarks.SetText('');

                    cboBankCode.Focus();
                }
            } else if (result == 'update') {
                Message(s.cp_type, s.cp_message);
                if (s.cp_type == 1) {
                    millisecondsToWait = 2000;
                    setTimeout(function () {
                        var pathArray = window.location.pathname.split('/');
                        var url = window.location.origin + '/' + pathArray[1] + '/ExchangeRate.aspx'
                        if (pathArray[1] == "AddExchangeRate.aspx") {
                            window.location.href = window.location.origin + '/ExchangeRate.aspx';
                        }
                        else {
                            window.location.href = url;
                        }
                    }, millisecondsToWait);
                }                
            }
        }

        function btnClear_Click(s, e) {
            cboBankCode.SetValue('');
            txtBankDesc.SetText('');
            cboCurrencyCode.SetValue('');
            txtCurrencyDesc.SetText('');
            dtExchangeRateDate.SetValue(null);
            txtBuyingRate.SetValue(0);
            txtMiddleRate.SetValue(0);
            txtSellingRate.SetValue(0);
            txtRemarks.SetText('');

            cboBankCode.Focus();
        }

        function btnSubmit_Click(s, e) {
            var BankCode;
            var CurrencyCode;
            var ExchangeRateDate;
            var BuyingRate;
            var MiddleRate;
            var SellingRate;
            var Remarks;

            if (cboBankCode.GetEnabled() == true) {
                if (cboBankCode.GetText() == '') {
                    Message(2, 'Please select Bank Code!');
                    cboBankCode.Focus();
                    e.processOnServer = false;
                    return;
                } else if (cboCurrencyCode.GetText() == '') {
                    Message(2, 'Please select Currency Code!');
                    cboCurrencyCode.Focus();
                    e.processOnServer = false;
                    return;
                } else if (dtExchangeRateDate.GetDate() == null) {
                    Message(2, 'Please select Exchange Rate Date!');
                    dtExchangeRateDate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtBuyingRate.GetValue() == null) {
                    Message(2, 'Please input Buying Rate!');
                    txtBuyingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtBuyingRate.GetValue() <= 0) {
                    Message(2, 'Please input Buying Rate!');
                    txtBuyingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtMiddleRate.GetValue() == null) {
                    Message(2, 'Please input Middle Rate!');
                    txtMiddleRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtMiddleRate.GetValue() <= 0) {
                    Message(2, 'Please input Middle Rate!');
                    txtMiddleRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtSellingRate.GetValue() == null) {
                    Message(2, 'Please input Selling Rate!');
                    txtSellingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtSellingRate.GetValue() <= 0) {
                    Message(2, 'Please input Selling Rate!');
                    txtSellingRate.Focus();
                    e.processOnServer = false;
                    return;
                }

                BankCode = cboBankCode.GetValue();
                CurrencyCode = cboCurrencyCode.GetValue();
                ExchangeRateDate = formatDateISO(dtExchangeRateDate.GetDate());
                BuyingRate = txtBuyingRate.GetValue();
                MiddleRate = txtMiddleRate.GetValue();
                SellingRate = txtSellingRate.GetValue();
                Remarks = txtRemarks.GetText();
                
                cbAction.PerformCallback('insert|' + BankCode + '|' + CurrencyCode + '|' + ExchangeRateDate + '|' + BuyingRate + '|' + MiddleRate + '|' + SellingRate + '|' + Remarks);
            } else {
                if (txtBuyingRate.GetValue() == null) {
                    Message(2, 'Please input Buying Rate!');
                    txtBuyingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtBuyingRate.GetValue() <= 0) {
                    Message(2, 'Please input Buying Rate!');
                    txtBuyingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtMiddleRate.GetValue() == null) {
                    Message(2, 'Please input Middle Rate!');
                    txtMiddleRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtMiddleRate.GetValue() <= 0) {
                    Message(2, 'Please input Middle Rate!');
                    txtMiddleRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtSellingRate.GetValue() == null) {
                    Message(2, 'Please input Selling Rate!');
                    txtSellingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtSellingRate.GetValue() <= 0) {
                    Message(2, 'Please input Selling Rate!');
                    txtSellingRate.Focus();
                    e.processOnServer = false;
                    return;
                }

                BankCode = cboBankCode.GetValue();
                CurrencyCode = cboCurrencyCode.GetValue();
                ExchangeRateDate = formatDateISO(dtExchangeRateDate.GetDate());
                BuyingRate = txtBuyingRate.GetValue();
                MiddleRate = txtMiddleRate.GetValue();
                SellingRate = txtSellingRate.GetValue();
                Remarks = txtRemarks.GetText();

                cbAction.PerformCallback('update|' + BankCode + '|' + CurrencyCode + '|' + ExchangeRateDate + '|' + BuyingRate + '|' + MiddleRate + '|' + SellingRate + '|' + Remarks);
            }          
        }

        function cboBankCode_Init(s, e) {
            var action;
            var bankCode; 
            action = getURLParameters("action");
            if (action == 'new') {
                cboBankCode.SetText('');
                txtBankDesc.SetText('');
                cboBankCode.SetEnabled(true);
                cboBankCode.Focus();
            } else if (action == 'edit') {
                bankCode = getURLParameters("bankcode");
                cboBankCode.SetText(bankCode);
                txtBankDesc.SetText(cboBankCode.GetSelectedItem().GetColumnText('Description'));
                cboBankCode.SetEnabled(false);
            }                     
        }

        function cboCurrencyCode_Init(s, e) {
            var action;
            var currencyCode;
            action = getURLParameters("action");
            if (action == 'new') {
                cboCurrencyCode.SetText('');
                txtCurrencyDesc.SetText('');
                cboCurrencyCode.SetEnabled(true);
            } else if (action == 'edit') {
                currencyCode = getURLParameters("currencycode");
                cboCurrencyCode.SetText(currencyCode);
                txtCurrencyDesc.SetText(cboCurrencyCode.GetSelectedItem().GetColumnText('Description'));
                cboCurrencyCode.SetEnabled(false);
            }            
        }

        function dtExchangeRateDate_Init(s, e) {
            var action;
            var date;
            action = getURLParameters("action");
            
            if (action == 'new') {
                dtExchangeRateDate.SetDate(null);
                dtExchangeRateDate.SetEnabled(true);
            } else if (action == 'edit') {
               //alert(getURLParameters("date"));
                date = new Date(getURLParameters("date"));
                date.valueOf(getURLParameters("date"));
             dtExchangeRateDate.SetDate(date);
           // dtExchangeRateDate.SetDate(getURLParameters("date"));
                dtExchangeRateDate.SetEnabled(false);
                txtBuyingRate.Focus();
            }            
        }
        
        function cboBankCode_SelectedIndexChanged(s, e) {
            var desc = s.GetSelectedItem().GetColumnText('Description');
            txtBankDesc.SetText(desc);
        }

        function cboBankCode_LostFocus(s, e) {
            var id = s.GetValue();
            if (id == null) {
                txtBankDesc.SetText('');
            }
        }

        function cboCurrencyCode_SelectedIndexChanged(s, e) {
            var desc = s.GetSelectedItem().GetColumnText('Description');
            txtCurrencyDesc.SetText(desc);
        }

        function cboCurrencyCode_LostFocus(s, e) {
            var id = s.GetValue();
            if (id == null) {
                txtCurrencyDesc.SetText('');
            }
        }

        function txtCurrencyDesc_Init(s, e) {
            txtCurrencyDesc.SetEnabled(false);
        }

        function txtBankDesc_Init(s, e) {
            txtBankDesc.SetEnabled(false);
        }

        function getURLParameters(paramName) {
            var sURL = window.document.URL.toString();
            if (sURL.indexOf("?") > 0) {
                var arrParams = sURL.split("?");
                var arrURLParams = arrParams[1].split("&");
                var arrParamNames = new Array(arrURLParams.length);
                var arrParamValues = new Array(arrURLParams.length);

                var i = 0;
                for (i = 0; i < arrURLParams.length; i++) {
                    var sParam = arrURLParams[i].split("=");
                    arrParamNames[i] = sParam[0];
                    if (sParam[1] != "")
                        arrParamValues[i] = unescape(sParam[1]);
                    else
                        arrParamValues[i] = "No Value";
                }

                for (i = 0; i < arrURLParams.length; i++) {
                    if (arrParamNames[i] == paramName) {
                        //alert("Parameter:" + arrParamValues[i]);
                        return arrParamValues[i];
                    }
                }
                return "No Parameters Found";
            }
        }
    </script>
    <style type="text/css">        
        .table-col-space01
        {
            width: 100px;                     
        }
        .table-col-control01
        {
            width: 150px;        
        }   
        .table-col-control02
        {
            width: 100px;
        }
        .table-col-control03
        {
            width: 400px;
        }      
        .table-row-height
        {
            height: 35px      
        }            
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<table style="width: 100%;">
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Bank Code">
            </dx:ASPxLabel>
        </td>
        <td class="table-col-control02"> 
            <dx:ASPxComboBox ID="cboBankCode" runat="server" ClientInstanceName="cboBankCode"
                Width="100px" Font-Names="Segoe UI" DataSourceID="dsBank" TextField="Description"
                ValueField="Code" TextFormatString="{0}" Font-Size="9pt" 
                Theme="Office2010Black" DropDownStyle="DropDown" 
                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                Height="25px">       
                <ClientSideEvents SelectedIndexChanged="cboBankCode_SelectedIndexChanged" LostFocus="cboBankCode_LostFocus" Init="cboBankCode_Init" />              
                <Columns>            
                    <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                    <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="350px" />
                </Columns>
                <ItemStyle Height="10px" Paddings-Padding="4px" >
                    <Paddings Padding="4px"></Paddings>
                </ItemStyle>
                <ButtonStyle Width="5px" Paddings-Padding="4px" >
                    <Paddings Padding="4px"></Paddings>
                </ButtonStyle>
            </dx:ASPxComboBox>
        </td>
        <td class="table-col-control03">
            <dx:ASPxTextBox ID="txtBankDesc" runat="server" Font-Names="Segoe UI" style="margin-left:10px" ClientInstanceName="txtBankDesc"
                Font-Size="9pt" Height="25px" Theme="Office2010Black" Width="340px">
            <ClientSideEvents Init="txtBankDesc_Init" />
            </dx:ASPxTextBox>
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel16" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Currency Code">
            </dx:ASPxLabel>
        </td>
        <td class="table-col-control02"> 
            <dx:ASPxComboBox ID="cboCurrencyCode" runat="server"
                ClientInstanceName="cboCurrencyCode" Width="100px" Font-Names="Segoe UI" 
                DataSourceID="dsCurrency" TextField="Description" ValueField="Code" 
                TextFormatString="{0}" Font-Size="9pt" 
                Theme="Office2010Black" DropDownStyle="DropDown" 
                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                Height="25px">  
            <ClientSideEvents SelectedIndexChanged="cboCurrencyCode_SelectedIndexChanged" LostFocus="cboCurrencyCode_LostFocus" Init="cboCurrencyCode_Init" />  
            <Columns>            
                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="100px" />
            </Columns>
            <ItemStyle Height="10px" Paddings-Padding="4px" >
                <Paddings Padding="4px"></Paddings>
            </ItemStyle>
            <ButtonStyle Width="5px" Paddings-Padding="4px" >
                <Paddings Padding="4px"></Paddings>
            </ButtonStyle>
            </dx:ASPxComboBox>                                                
        </td>
        <td class="table-col-control03">
            <dx:ASPxTextBox ID="txtCurrencyDesc" runat="server" Font-Names="Segoe UI" style="margin-left:10px" ClientInstanceName="txtCurrencyDesc"
                Font-Size="9pt" Height="25px" Theme="Office2010Black" Width="340px">
            <ClientSideEvents Init="txtCurrencyDesc_Init" />
            </dx:ASPxTextBox>
        </td>
        <td>
            <dx:ASPxCallback ID="cbAction" runat="server" ClientInstanceName="cbAction">
                <ClientSideEvents CallbackComplete="cbAction_CallbackComplete" />
            </dx:ASPxCallback>
        </td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Exchange Rate Date">
            </dx:ASPxLabel>
        </td>
        <td colspan="2"> 
            <dx:ASPxDateEdit ID="dtExchangeRateDate" ClientInstanceName="dtExchangeRateDate" runat="server" Font-Names="Segoe UI" HorizontalAlign="Right" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy"
                Font-Size="9pt" Height="25px" Theme="Office2010Black" Width="150px">
            <ClientSideEvents Init="dtExchangeRateDate_Init" />
            </dx:ASPxDateEdit>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Buying Rate">
            </dx:ASPxLabel>
        </td>
        <td colspan="2"> 
            <dx:ASPxTextBox ID="txtBuyingRate" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="150px" Height="25px" ClientInstanceName="txtBuyingRate" MaxLength="11" HorizontalAlign="Right">
                       <MaskSettings Mask="<0..100000000g>.<00..99>" />
             </dx:ASPxTextBox>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01"></td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Middle Rate">
            </dx:ASPxLabel>
        </td>
        <td colspan="2" class="table-row-height">                             
            <dx:ASPxTextBox ID="txtMiddleRate" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="150px" Height="25px" ClientInstanceName="txtMiddleRate" MaxLength="11" HorizontalAlign="Right">
                       <MaskSettings Mask="<0..100000000g>.<00..99>" />
             </dx:ASPxTextBox>
        </td>
        <td class="table-row-height"></td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Selling Rate">
            </dx:ASPxLabel>
        </td>
        <td colspan="2"> 
           
             <dx:ASPxTextBox ID="txtSellingRate" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="150px" Height="25px" ClientInstanceName="txtSellingRate" MaxLength="11" HorizontalAlign="Right">
                       <MaskSettings Mask="<0..100000000g>.<00..99>" />
             </dx:ASPxTextBox>
        </td>
        <td>
            <asp:SqlDataSource ID="dsBank" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"             
                SelectCommand="select rtrim(Par_Code) Code, rtrim(Par_Description) Description from Mst_Parameter where Par_Group = 'Bank'"></asp:SqlDataSource>
        </td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Remarks">
            </dx:ASPxLabel>
        </td>
        <td colspan="2"> 
          
         <dx:ASPxMemo ID="txtRemarks" runat="server" Height="45px" Width="100%" ClientInstanceName="txtRemarks" MaxLength="50">
             </dx:ASPxMemo>     
        </td>
        <td><asp:SqlDataSource ID="dsCurrency" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"             
                SelectCommand="select rtrim(Par_Code) Code, rtrim(Par_Description) Description from Mst_Parameter where Par_Group = 'Currency'"></asp:SqlDataSource>
        </td>
    </tr>
   
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">&nbsp;</td>
        <td class="table-col-control02">&nbsp;</td>
        <td class="table-col-control03">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">&nbsp;</td>
        <td colspan="2"> 
            <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="false"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnBack" Theme="Default" >                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>
            <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="false" style="margin-left:10px"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnClear" Theme="Default" >                        
                <ClientSideEvents Click="btnClear_Click"/>
                <Paddings Padding="2px" />
            </dx:ASPxButton>
            <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="false" style="margin-left:10px"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnSubmit" Theme="Default" >                        
                <ClientSideEvents Click="btnSubmit_Click"  />
                <Paddings Padding="2px" />
            </dx:ASPxButton>                    
        </td>
        <td>&nbsp;</td>        
    </tr>
</table>
</asp:Content>

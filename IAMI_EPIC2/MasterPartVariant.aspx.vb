﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
'Imports Microsoft.Office.Interop.Excel
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class MasterPartVariant
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Functions"
    Private Sub up_Excel(Optional ByRef outError As String = "")
        Try
            Dim ps As New PrintingSystem()

            Dim dt As System.Data.DataTable
            Dim cMasterPartVariant As New ClsMasterPartVariant

            cMasterPartVariant.Type = CboType.Value
            cMasterPartVariant.VariantCode = CboVariantCode.Value
            cMasterPartVariant.PartNo = cboPartNo.Value

            dt = ClsMasterPartVariantDB.getData(cMasterPartVariant, , , outError)

            Grid.DataSource = dt
            Grid.DataBind()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=MasterPartVariant" & Format(Date.Now, "ddMMyyyyHHmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            outError = ex.Message
        End Try
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A180")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A180")

        Grid.JSProperties("cp_type") = ""
        Grid.JSProperties("cp_message") = ""

    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim ErrMsg As String = ""
        up_Excel(ErrMsg)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("~/AddMasterPartVariant.aspx")
    End Sub

    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        Dim cMasterPartVariant As New ClsMasterPartVariant
        Dim pAction As String = ""
        Dim outError As String = ""

        Dim dt As System.Data.DataTable

        cMasterPartVariant.Type = CboType.Value
        cMasterPartVariant.VariantCode = CboVariantCode.Value
        cMasterPartVariant.PartNo = cboPartNo.Value

        dt = ClsMasterPartVariantDB.getData(cMasterPartVariant, , , outError)

        If outError <> "" Then
            cbAction.JSProperties("cp_type") = 3
            cbAction.JSProperties("cp_message") = outError
            Exit Sub
        End If

        Grid.DataSource = dt
        Grid.DataBind()
    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim dt As New DataTable
        Dim cls As New ClsMasterPartVariant
        cls.Type = e.NewValues("Type")
        cls.VariantCode = e.NewValues("VariantCode")
        cls.VariantName = e.NewValues("VariantName")
        cls.MaterialFUSAP = e.NewValues("MaterialFUSAP")
        cls.PartNo = e.NewValues("PartNo")
        cls.Qty = e.NewValues("Qty")
        cls.User = pUser

        ClsMasterPartVariantDB.Update(cls, , , pErr)

        If pErr <> "" Then
            Grid.CancelEdit()
            Grid.JSProperties("cp_disabled") = ""
            Grid.JSProperties("cp_type") = "3"
            Grid.JSProperties("cp_message") = pErr
        Else
            Grid.CancelEdit()
            Grid.JSProperties("cp_disabled") = ""
            Grid.JSProperties("cp_type") = "1"
            Grid.JSProperties("cp_message") = "Update data successfully!"
            dt = ClsMasterPartVariantDB.getData(cls, , , pErr)
            Grid.DataSource = dt
            Grid.DataBind()
        End If
    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim dt As New DataTable
        Dim cls As New ClsMasterPartVariant
        cls.Type = e.Values("Type")
        cls.VariantCode = e.Values("VariantCode")
        cls.PartNo = e.Values("PartNo")

        ClsMasterPartVariantDB.Delete(cls, , , pErr)
        If pErr = "" Then
            Grid.JSProperties("cp_disabled") = ""
            Grid.JSProperties("cp_type") = "1"
            Grid.JSProperties("cp_message") = "Delete data successfully!"
            dt = ClsMasterPartVariantDB.getData(cls, , , pErr)
            Grid.DataSource = dt
            Grid.DataBind()
        Else
            Grid.JSProperties("cp_disabled") = ""
            Grid.JSProperties("cp_type") = "3"
            Grid.JSProperties("cp_message") = pErr
        End If
    End Sub

    Private Sub Grid_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If e.Column.FieldName = "VariantCode" Or e.Column.FieldName = "Type" Or e.Column.FieldName = "PartNo" Or e.Column.FieldName = "PartName" Then
            e.Editor.ReadOnly = True
            e.Editor.ForeColor = Color.Silver
        End If
    End Sub

    Protected Sub Grid_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not Grid.IsNewRowEditing) Then
            Grid.DoRowValidation()
        End If
    End Sub

    Protected Sub Grid_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In Grid.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "VariantName" Then
                If IsNothing(e.NewValues("VariantName")) OrElse e.NewValues("VariantName").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Variant Name !"
                    Grid.JSProperties("cp_disabled") = ""
                    Grid.JSProperties("cp_type") = "2"
                    Grid.JSProperties("cp_message") = "Please Input Variant Name !"
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "MaterialFUSAP" Then
                If IsNothing(e.NewValues("MaterialFUSAP")) OrElse e.NewValues("MaterialFUSAP").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please Input Material FU SAP !"
                    Grid.JSProperties("cp_disabled") = ""
                    Grid.JSProperties("cp_type") = "2"
                    Grid.JSProperties("cp_message") = "Please Input Material FU SAP !"
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Qty" Then
                If IsNothing(e.NewValues("Qty")) OrElse e.NewValues("Qty").ToString.Trim = "" OrElse e.NewValues("Qty").ToString.Trim = 0 Then
                    e.Errors(dataColumn) = "Please Input Qty !"
                    Grid.JSProperties("cp_disabled") = ""
                    Grid.JSProperties("cp_type") = "2"
                    Grid.JSProperties("cp_message") = "Please input Qty !"
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim cMasterPartVariant As New ClsMasterPartVariant
        Dim pAction As String = ""
        Dim outError As String = ""

        Dim dt As System.Data.DataTable

        pAction = Split(e.Parameters, "|")(0)

        If pAction = "load" Then
            cMasterPartVariant.VariantCode = Split(e.Parameters, "|")(1)
            cMasterPartVariant.PartNo = Split(e.Parameters, "|")(2)
            cMasterPartVariant.Type = Split(e.Parameters, "|")(3)

            dt = ClsMasterPartVariantDB.getData(cMasterPartVariant, , , outError)

            If outError <> "" Then
                cbAction.JSProperties("cp_type") = 3
                cbAction.JSProperties("cp_message") = outError
                Exit Sub
            End If

            Grid.DataSource = dt
            Grid.DataBind()

            If Grid.VisibleRowCount > 0 Then
                Grid.JSProperties("cp_disabled") = "N"
            Else
                Grid.JSProperties("cp_disabled") = "Y"
                Grid.JSProperties("cp_message") = "There is no data to show!"
            End If
        End If

    End Sub

#End Region

End Class
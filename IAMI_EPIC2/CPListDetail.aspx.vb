﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO
Imports System.Transactions
Imports System.Web.UI

Public Class CPListDetail
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False

    Dim ls_CPNumber As String = ""
#End Region

#Region "PROCEDURE"
    Private Sub FillComboCENumber()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_FillCombo", "Combo|StartDate|EndDate|PRNumber|FromBtnAdd|UserID|CPNumber", "CE|1900-01-01|1900-01-01|" & cboPRNumber.Text & "|Y|" & pUser & "|" & "", ErrMsg)

        If Request.QueryString.Count > 0 Then
            Try
                ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_FillCombo", "Combo|StartDate|EndDate|PRNumber|FromBtnAdd|UserID|CPNumber", "CE|1900-01-01|1900-01-01|" & Split(Request.QueryString(0), "|")(2) & "|N|" & pUser & "|" & Split(Request.QueryString(0), "|")(0), ErrMsg)

            Catch ex As Exception
                ErrMsg = Err.Description
            End Try
        End If

        If ErrMsg = "" Then
            cboCENumber.DataSource = ds.Tables(0)
            cboCENumber.DataBind()

            'If cboCENumber.Items.Count > 0 Then
            '    cboCENumber.SelectedIndex = 0
            'End If
            If Request.QueryString.Count > 0 Or ds.Tables(0).Rows.Count > 0 Then
                cboCENumber.SelectedIndex = 0
                Call SetInformation(cboCENumber, True)
                'ScriptManager.RegisterStartupScript(cboCENumber, cboCENumber.GetType(), "cboCENumber", "cboCENumber.SetEnabled(false);", True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboCENumber)
        End If
    End Sub

    Private Sub FillComboPRNumber()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        If Request.QueryString.Count > 0 Then
            'FROM DETAIL GRID
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_FillCombo", "Combo", "PR", ErrMsg)
        Else
            'FROM ADD BUTTON
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_FillCombo", "Combo|StartDate|EndDate|PRNumber|FromBtnAdd|UserID", "PR|1900-01-01|1900-01-01||Y|" + pUser, ErrMsg)
        End If


        If ErrMsg = "" Then
            cboPRNumber.DataSource = ds.Tables(0)
            cboPRNumber.DataBind()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboPRNumber)
        End If
    End Sub

    Public Shared Function GetDataLocation(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "SELECT Description FROM VW_Location "

                'If pSupplier <> "00" Then
                '    sql = sql + " AND H.Supplier_Code = '" & pSupplier & "'"
                'End If

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Private Sub up_FillComboLocation()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = GetDataLocation(pErr)
        If pErr = "" Then
            cboLocation.DataSource = ds.Tables(0)
            cboLocation.DataBind()
        End If
    End Sub

    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_List", "PRNumber|CENumber|Rev", cboPRNumber.Text & "|" & cboCENumber.Text & "|" & txtRev.Text, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub SetInformation(ByVal pObj As Object, Optional pCalledFromCPList As Boolean = False)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_SetHeader", "PRNumber|CENumber|Rev", cboPRNumber.Text & "|" & cboCENumber.Text & "|" & txtRev.Text, ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_SetHeader", "PRNumber|CENumber|Rev", Split(Request.QueryString(0), "|")(2) & "|" & cboCENumber.Text & "|" & Split(Request.QueryString(0), "|")(1), ErrMsg)
        End If


        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                pObj.JSProperties("cpCPNumber") = ds.Tables(0).Rows(0).Item("CP_Number").ToString()
                pObj.JSProperties("cpRev") = ds.Tables(0).Rows(0).Item("Rev").ToString()
                pObj.JSProperties("cpCENumber") = ds.Tables(0).Rows(0).Item("CE_Number")
                pObj.JSProperties("cpPRNumber") = ds.Tables(0).Rows(0).Item("PR_Number")
                pObj.JSProperties("cpInvitationDate") = CDate(ds.Tables(0).Rows(0).Item("Invitation_Date")) 'Format(CDate(ds.Tables(0).Rows(0).Item("Invitation_Date")), "yyyy-MM-dd")
                pObj.JSProperties("cpNotes") = ds.Tables(0).Rows(0).Item("Notes").ToString()
                pObj.JSProperties("cpLink") = ds.Tables(0).Rows(0).Item("File_Name")
                pObj.JSProperties("cpCEBudget") = ds.Tables(0).Rows(0).Item("CE_Budget").ToString()
                pObj.JSProperties("cpInvitationTime") = ds.Tables(0).Rows(0).Item("Invitation_Time").ToString()
                pObj.JSProperties("cpTenderLocation") = ds.Tables(0).Rows(0).Item("Tender_Location").ToString()
            Else
                pObj.JSProperties("cpCPNumber") = ""
                pObj.JSProperties("cpRev") = ""
                pObj.JSProperties("cpCENumber") = ""
                pObj.JSProperties("cpPRNumber") = ""
                pObj.JSProperties("cpInvitationDate") = ""
                pObj.JSProperties("cpNotes") = ""
                pObj.JSProperties("cpLink") = ""
                pObj.JSProperties("cpCEBudget") = ""
                pObj.JSProperties("cpInvitationTime") = ""
                pObj.JSProperties("cpTenderLocation") = ""

                Dim dt As Date
                dt = Year(Now) & "-" & Month(Now) & "-01"
                pObj.JSProperties("cpQuotationDate") = dt
            End If

            If pCalledFromCPList = True Then
                'add this parameter to prevent reset value of Notes when callback
                pObj.JSProperties("cpCalledFromCPList") = "1"
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, pObj)
        End If

        Dim ds2 As New DataSet
        Dim ErrMsg2 As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"

        ds2 = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_List", "PRNumber|CENumber|Rev", cboPRNumber.Text & "|" & cboCENumber.Text & "|" & txtRev.Text, ErrMsg2)

        If ErrMsg2 = "" Then
            Grid.DataSource = ds2.Tables(0)
            Grid.DataBind()

            If ds2.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg2, Grid)
        End If
    End Sub

    Private Sub GridLoadMeeting(pCPNumber As String, rev As String)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_ListMeeting", "CP_Number|Rev", pCPNumber & "|" & rev, ErrMsg)

        If ErrMsg = "" Then
            GridInvitation.DataSource = ds.Tables(0)
            GridInvitation.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridInvitation)
        End If
    End Sub

    'unused
    Private Sub LinkFile()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_SetHeader", "PRNumber|CENumber|Rev", Split(Request.QueryString(0), "|")(2) & "|" & cboCENumber.Text & "|" & Split(Request.QueryString(0), "|")(1), ErrMsg)

        IALinkFile.Text = IIf(ds.Tables(0).Rows(0)("File_Name") = "", "No File", ds.Tables(0).Rows(0)("CE_Budget"))
        IABudgetNo.Set("hfLink", IIf(ds.Tables(0).Rows(0)("File_Name") = "", "No File", ds.Tables(0).Rows(0)("File_Name")))

        'IABudgetNo.Text = IIf(ds.Tables(0).Rows(0)("CE_Budget") = "", "No File", ds.Tables(0).Rows(0)("CE_Budget"))
        'LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
        Dim extension As String = ""
        extension = System.IO.Path.GetExtension("~/Files/" & ds.Tables(0).Rows(0)("File_Name"))
        If extension <> ".pdf" Then
            If ds.Tables(0).Rows(0)("File_Name") <> "" Then
                IALinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")

            End If
        End If
    End Sub

    'unused
    Private Sub GenerateCPNumber()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_GenerateCPNumber", "", "", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                txtCPNumber.Text = ds.Tables(0).Rows(0).Item("CP_Number")
                txtCPNumberInvitation.Text = ds.Tables(0).Rows(0).Item("CP_Number")
                txtRevInvitation.Text = "0"
            Else
                txtCPNumber.Text = "IAMI.PURCH/COST/CP/XII/18/001"
                txtCPNumberInvitation.Text = ds.Tables(0).Rows(0).Item("CP_Number")
                txtRevInvitation.Text = "0"
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboCENumber)
        End If
    End Sub
#End Region

#Region "FUNCTIONS"
    Private Function GetAllSupplierForGridHeaderCaption()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "SELECT Sup = dbo.MergeSupplier('" & cboCENumber.Text & "')", "", "", ErrMsg)

        If ErrMsg = "" Then
            retVal = ds.Tables(0).Rows(0).Item("Sup").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboPRNumber)
        End If

        Return retVal
    End Function

    Private Function IsDataExist() As String
        Dim retVal As String = ""
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Exist", "CPNumber", txtCPNumber.Text, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = ds.Tables(0).Rows(0).Item("Ret")
                Else
                    'NOT EXISTS
                    retVal = "N"
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataCPUpdate() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '6'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataSubmitted() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '1'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataExpired(cpnumber As String, rev As String) As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Detail WHERE CP_Number = '" & cpnumber & "' AND Rev = '" & rev & "' AND CAST(Invitation_Date as date) >= cast(getdate() as date)", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataAllSubmitted(cpnumber As String, rev As String) As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "select distinct Supplier_Code from CP_Detail where CP_Number ='" & cpnumber & "' and Rev ='" & rev & "' and (ISNULL(Invitation_Date,'') ='' or ISNULL(Invitation_Time,'') =''  or ISNULL(Location_Meeting,'') ='')", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataApproved() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '2'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataAlreadyInvitation() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_CheckApproval", "CPNumber|Rev", txtCPNumber.Text & "|" & txtRev.Text, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows(0)("Ret") = "1" Then
                    'EXIST
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataAccepted() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '3'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataRejected() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '4'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function ConvertToFormatDate_yyyyMMdd(ByVal pDate As String) As String
        Dim retVal As String = "1900-01-01"
        Dim pYear As String = Split(pDate, "-")(0)
        Dim pMonth As String = Strings.Right("0" & Split(pDate, "-")(1), 2)
        Dim pDay As String = Strings.Right("0" & Split(pDate, "-")(2), 2)

        retVal = pYear & "-" & pMonth & "-" & pDay

        Return retVal
    End Function
#End Region

#Region "EVENTS"
    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|E010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                          "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'Dim dDate As Date
        'dDate = Format(CDate(Now), "yyyy-MM-dd")
        ' dtInvitationDate.Value = dDate

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboCENumber)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "COUNTER PROPOSAL LIST (TENDER) DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E010")
        Dim dDate As Date

        gs_Message = ""

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            'dDate = Format(CDate(Now), "yyyy-MM-dd")
            'dtInvitationDate.Value = dDate
            up_FillComboLocation()
            dDate = Year(Now) & "-" & Month(Now) & "-" & Day(Now)

            If IsDataExist() = "N" Then
                dtInvitationDate.Value = dDate
              
            End If
            If IsNothing(Session("btnAdd_CPList")) = False Then
                txtRev.Text = "0"
            End If

            If Request.QueryString.Count > 0 Then
                Try
                    Session("CPNumber") = Split(Request.QueryString(0), "|")(0)
                    gs_CPNumber = Split(Request.QueryString(0), "|")(0)
                    txtCPNumber.Text = Split(Request.QueryString(0), "|")(0)
                    txtCPNumberInvitation.Text = Split(Request.QueryString(0), "|")(0)
                    txtRev.Text = Split(Request.QueryString(0), "|")(1)
                    cboPRNumber.Value = Split(Request.QueryString(0), "|")(2)
                    txtRevInvitation.Text = 0 'Split(Request.QueryString(0), "|")(1)
                    GridLoadMeeting(Split(Request.QueryString(0), "|")(0), 0)
                    ' cboCENumber.Value = Split(Request.QueryString(0), "|")(3)

                    'check data already invit (all user approved)
                    If IsDataAlreadyInvitation() = True Then 'Or IsDataApproved() = True Or IsDataAccepted() = True Or IsDataRejected() = True Or IsDataCPUpdate() = True Then
                        ScriptManager.RegisterStartupScript(btnInvitTender, btnInvitTender.GetType(), "btnInvitTender", "btnInvitTender.SetEnabled(true);", True)
                    Else

                        ScriptManager.RegisterStartupScript(btnInvitTender, btnInvitTender.GetType(), "btnInvitTender", "btnInvitTender.SetEnabled(false);", True)
                    End If


                    ScriptManager.RegisterStartupScript(txtCPNumber, txtCPNumber.GetType(), "txtCPNumber", "txtCPNumber.SetEnabled(false);", True)
                    ScriptManager.RegisterStartupScript(cboPRNumber, cboPRNumber.GetType(), "cboPRNumber", "cboPRNumber.SetEnabled(false);", True)
                    ScriptManager.RegisterStartupScript(cboCENumber, cboCENumber.GetType(), "cboCENumber", "cboCENumber.SetEnabled(false);", True)

                    If IsDataSubmitted() = True Or IsDataApproved() = True Or IsDataAccepted() = True Or IsDataRejected() = True Or IsDataCPUpdate() = True Then
                        ScriptManager.RegisterStartupScript(dtInvitationDate, dtInvitationDate.GetType(), "dtInvitationDate", "dtInvitationDate.SetEnabled(false);", True)
                        ScriptManager.RegisterStartupScript(cboLocation, cboLocation.GetType(), "cboLocation", "cboLocation.SetEnabled(false);", True)
                        ScriptManager.RegisterStartupScript(txtTime, txtTime.GetType(), "txtTime", "txtTime.SetEnabled(false);", True)
                        ScriptManager.RegisterStartupScript(memoNote, memoNote.GetType(), "memoNote", "memoNote.SetEnabled(false);", True)
                        ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", "btnDraft.SetEnabled(false);", True)
                        ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "btnSubmit", "btnSubmit.SetEnabled(false);", True)
                        ''Grid.Enabled = False
                    Else
                        'btnDraft.Enabled = True
                        'btnSubmit.Enabled = True
                        ScriptManager.RegisterStartupScript(dtInvitationDate, dtInvitationDate.GetType(), "dtInvitationDate", "dtInvitationDate.SetEnabled(true);", True)
                        ScriptManager.RegisterStartupScript(cboLocation, cboLocation.GetType(), "cboLocation", "cboLocation.SetEnabled(true);", True)
                        ScriptManager.RegisterStartupScript(txtTime, txtTime.GetType(), "txtTime", "txtTime.SetEnabled(true);", True)

                        ScriptManager.RegisterStartupScript(memoNote, memoNote.GetType(), "memoNote", "memoNote.SetEnabled(true);", True)
                        ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", "btnDraft.SetEnabled(true);", True)
                        ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "btnSubmit", "btnSubmit.SetEnabled(true);", True)
                    End If

                    If IsDataExpired(Split(Request.QueryString(0), "|")(0), Split(Request.QueryString(0), "|")(1)) = True Then
                        ScriptManager.RegisterStartupScript(btnResendInvitation, btnResendInvitation.GetType(), "btnResendInvitation", "btnResendInvitation.SetEnabled(true);", True)
                    Else
                        ScriptManager.RegisterStartupScript(btnResendInvitation, btnResendInvitation.GetType(), "btnResendInvitation", "btnResendInvitation.SetEnabled(false);", True)
                    End If

                    If IsDataAllSubmitted(Split(Request.QueryString(0), "|")(0), Split(Request.QueryString(0), "|")(1)) = True Then
                        ScriptManager.RegisterStartupScript(btnSubmitInvitation, btnSubmitInvitation.GetType(), "btnSubmitInvitation", "btnSubmitInvitation.SetEnabled(true);", True)
                    Else
                        ScriptManager.RegisterStartupScript(btnSubmitInvitation, btnSubmitInvitation.GetType(), "btnSubmitInvitation", "btnSubmitInvitation.SetEnabled(false);", True)
                    End If

                Catch ex As Exception
                    Response.Redirect("CPListDetail.aspx")
                End Try


                'Remove session after back from ViewCounterProposal.aspx
                If IsNothing(Session("E010_QueryString")) = False Then
                    Session.Remove("E010_QueryString")
                End If

            Else
                txtCPNumber.Text = "--NEW--"
                gs_CPNumber = ""
                ' Call GenerateCPNumber()
                Dim Script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                        "btnPrint.SetEnabled(false);" & vbCrLf & _
                        "btnInvitTender.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "btnSubmit", Script, True)
            End If

        End If
    End Sub

    Private Sub cboPRNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboPRNumber.Callback
        Try
            Call FillComboPRNumber()

            If Request.QueryString.Count > 0 Then
                Try
                    cboPRNumber.Text = Split(Request.QueryString(0), "|")(2)
                    'ScriptManager.RegisterStartupScript(cboPRNumber, cboPRNumber.GetType(), "cboPRNumber", "cboPRNumber.SetEnabled(false);", True)

                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboPRNumber)
                End Try
            End If

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboPRNumber)
        End Try
    End Sub

    Private Sub cboCENumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCENumber.Callback
        Try
            Call FillComboCENumber()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboCENumber)
        End Try
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "ProposalPricePcs" Then
            'Editable
            If IsDataSubmitted() = True Or IsDataApproved() = True Or IsDataAccepted() = True Or IsDataRejected() = True Then
                'Non-Editable
                e.Cell.BackColor = Color.LemonChiffon
            Else
                'Editable
                e.Cell.BackColor = Color.White
            End If
        Else
            'Non-Editable
            e.Cell.BackColor = Color.LemonChiffon
        End If

    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Try
            If e.UpdateValues.Count > 0 Then
                Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
                Dim idx As Integer = 0

                Dim lb_IsDataExist As String = IsDataExist()
                Dim vCPNumber As String = ""

                If lb_IsDataExist = "N" Then
                    'If e.UpdateValues.Count <> Grid.VisibleRowCount Then
                    '    Session("E010_ErrMsg") = "Please fill all of rows on the grid before save!"
                    '    Exit Sub
                    'End If
                End If

                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()

                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                        'If lb_IsDataExist = "N" Then
                        '    ls_SQL = "EXEC sp_CP_Insert_Header " & _
                        '             "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & cboCENumber.Text & "'," & _
                        '             "'" & Format(CDate(dtInvitationDate.Value), "yyyy-MM-dd") & "','0','" & memoNote.Text & "','" & UserLogin & "','" & txtTime.Text & "','" & cboLocation.Value & "'"
                        'Else
                        '    ls_SQL = "EXEC sp_CP_Update_Header " & _
                        '             "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & cboCENumber.Text & "'," & _
                        '             "'" & Format(CDate(dtInvitationDate.Value), "yyyy-MM-dd") & "','0','" & memoNote.Text & "','" & UserLogin & "','" & txtTime.Text & "','" & cboLocation.Value & "'"
                        'End If

                        '#HEADER
                        If lb_IsDataExist = "N" Then
                            ls_SQL = "sp_CP_Insert_Header"
                        Else
                            ls_SQL = "sp_CP_Update_Header"
                        End If

                        Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                        sqlComm.CommandType = CommandType.StoredProcedure

                        If lb_IsDataExist = "N" And txtCPNumber.Text = "--NEW--" Then
                            sqlComm.Parameters.AddWithValue("@CPNumber", ls_CPNumber)
                        Else
                            sqlComm.Parameters.AddWithValue("@CPNumber", txtCPNumber.Text) 'for update
                        End If

                        sqlComm.Parameters.AddWithValue("@Rev", txtRev.Text)
                        sqlComm.Parameters.AddWithValue("@CENumber", cboCENumber.Text)
                        sqlComm.Parameters.AddWithValue("@InvitationDate", Format(CDate(dtInvitationDate.Value), "yyyy-MM-dd"))
                        sqlComm.Parameters.AddWithValue("@CPStatus", "0")
                        sqlComm.Parameters.AddWithValue("@Notes", memoNote.Text)
                        sqlComm.Parameters.AddWithValue("@RegUser", UserLogin)
                        sqlComm.Parameters.AddWithValue("@Invitation_Time", txtTime.Text)
                        sqlComm.Parameters.AddWithValue("@Tender_Location", "")

                        Dim outPutParameter As SqlParameter = New SqlParameter()
                        If lb_IsDataExist = "N" And gs_CPNumber = "" Then

                            outPutParameter.ParameterName = "@CPNumber_Output"
                            outPutParameter.SqlDbType = System.Data.SqlDbType.VarChar
                            outPutParameter.Direction = System.Data.ParameterDirection.Output
                            outPutParameter.Size = 50
                            sqlComm.Parameters.Add(outPutParameter)

                        End If

                        sqlComm.ExecuteNonQuery()
                        If lb_IsDataExist = "N" And gs_CPNumber = "" Then
                            ls_CPNumber = outPutParameter.Value.ToString()
                            gs_CPNumber = outPutParameter.Value.ToString()
                            txtCPNumber.Text = gs_CPNumber
                            Session("CPNumber") = outPutParameter.Value.ToString()
                        Else
                            ls_CPNumber = gs_CPNumber
                        End If

                        sqlComm.Dispose()

                        '#DETAIL
                        If ls_CPNumber <> "" Then
                            For idx = 0 To e.UpdateValues.Count - 1
                                Dim ls_SQLSP As String
                                If lb_IsDataExist = "N" Then
                                    ls_SQLSP = "sp_CP_Insert_Detail "
                                Else
                                    ls_SQLSP = "sp_CP_Update_Detail "
                                End If

                                sqlComm = New SqlCommand(ls_SQLSP, sqlConn, sqlTran)
                                sqlComm.CommandType = CommandType.StoredProcedure

                                If lb_IsDataExist = "N" Then
                                    sqlComm.Parameters.AddWithValue("@CPNumber", ls_CPNumber)
                                Else
                                    sqlComm.Parameters.AddWithValue("@CPNumber", txtCPNumber.Text)
                                End If

                                sqlComm.Parameters.AddWithValue("@Rev", txtRev.Text)
                                sqlComm.Parameters.AddWithValue("@PRNumber", cboPRNumber.Text)
                                sqlComm.Parameters.AddWithValue("@CENumber", cboCENumber.Text)
                                sqlComm.Parameters.AddWithValue("@MaterialNo", e.UpdateValues(idx).NewValues("Material_No").ToString())
                                sqlComm.Parameters.AddWithValue("@Qty", e.UpdateValues(idx).NewValues("Qty").ToString())
                                sqlComm.Parameters.AddWithValue("@ProposalPrice", e.UpdateValues(idx).NewValues("ProposalPricePcs").ToString())
                                sqlComm.Parameters.AddWithValue("@BestPrice", 0)
                                sqlComm.Parameters.AddWithValue("@RegUser", UserLogin)
                                sqlComm.Parameters.AddWithValue("@Invitation_Date", Format(CDate(dtInvitationDate.Value), "yyyy-MM-dd"))

                                'ls_SQL = ls_SQL + _
                                '             "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & cboPRNumber.Text & "','" & cboCENumber.Text & "'," & _
                                '             "'" & e.UpdateValues(idx).NewValues("Material_No").ToString() & "'," & _
                                '             "'" & e.UpdateValues(idx).NewValues("Qty").ToString() & "'," & _
                                '             "'" & e.UpdateValues(idx).NewValues("ProposalPricePcs").ToString() & "'," & _
                                '             "'0'," & _
                                '             "'" & UserLogin & "'," & _
                                '             "'" & Format(CDate(dtInvitationDate.Value), "yyyy-MM-dd") & "'"
                                sqlComm.ExecuteNonQuery()
                                sqlComm.Dispose()
                            Next idx
                            sqlTran.Commit()
                        End If
                    End Using
                End Using

            End If

        Catch ex As Exception
            Session("E010_ErrMsg") = Err.Description
        End Try
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim headerCaption As String = ""
        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        If pfunction = "gridload" Then
            Call GridLoad()

            'SET HEADER CAPTION (SUPPLIER)
            '#Initialize
            Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
            Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
            Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
            Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
            Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
            Grid.Columns("Supplier1").Visible = True
            Grid.Columns("Supplier2").Visible = True
            Grid.Columns("Supplier3").Visible = True
            Grid.Columns("Supplier4").Visible = True
            Grid.Columns("Supplier5").Visible = True
            Grid.Columns("BestPriceSupplier1").Visible = True
            Grid.Columns("BestPriceSupplier2").Visible = True
            Grid.Columns("BestPriceSupplier3").Visible = True
            Grid.Columns("BestPriceSupplier4").Visible = True
            Grid.Columns("BestPriceSupplier5").Visible = True

            '#Set by condition
            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True : Grid.Columns("BestPriceSupplier1").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.Columns("Supplier1").Visible = False
                Grid.Columns("BestPriceSupplier1").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True : Grid.Columns("BestPriceSupplier2").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.Columns("Supplier2").Visible = False
                Grid.Columns("BestPriceSupplier2").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True : Grid.Columns("BestPriceSupplier3").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.Columns("Supplier3").Visible = False
                Grid.Columns("BestPriceSupplier3").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True : Grid.Columns("BestPriceSupplier4").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.Columns("Supplier4").Visible = False
                Grid.Columns("BestPriceSupplier4").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True : Grid.Columns("BestPriceSupplier5").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("Supplier5").Visible = False
                Grid.Columns("BestPriceSupplier5").Visible = False
            End Try
        ElseIf pfunction = "draft" Then
            Call SetInformation(Grid)
        ElseIf pfunction = "save" Then
            Grid.JSProperties("cpSaveData") = "1"
            Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
            Dim idx As Integer = 0

            Try
                cbExist.JSProperties("cpParameter") = "draft"

                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()

                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                        '#HEADER
                        ls_SQL = "sp_CP_Update_Header"


                        Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                        sqlComm.CommandType = CommandType.StoredProcedure
                        sqlComm.Parameters.AddWithValue("@CPNumber", gs_CPNumber) 'for update
                        sqlComm.Parameters.AddWithValue("@Rev", txtRev.Text)
                        sqlComm.Parameters.AddWithValue("@CENumber", cboCENumber.Text)
                        sqlComm.Parameters.AddWithValue("@InvitationDate", Format(CDate(dtInvitationDate.Value), "yyyy-MM-dd"))
                        sqlComm.Parameters.AddWithValue("@CPStatus", "0")
                        sqlComm.Parameters.AddWithValue("@Notes", memoNote.Text)
                        sqlComm.Parameters.AddWithValue("@RegUser", UserLogin)
                        'sqlComm.Parameters.AddWithValue("@Invitation_Time", "") 'use in invitation popup
                        'sqlComm.Parameters.AddWithValue("@Tender_Location", "") 'use in invitation popup

                        sqlComm.ExecuteNonQuery()
                        sqlComm.Dispose()

                        sqlTran.Commit()
                    End Using
                    'ls_SQL = "EXEC sp_CP_Update_Header " & _
                    '            "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & cboCENumber.Text & "'," & _
                    '            "'" & Format(CDate(dtInvitationDate.Value), "yyyy-MM-dd") & "','0','" & memoNote.Text & "','" & UserLogin & "','" & txtTime.Text & "','" & cboLocation.Value & "'"

                End Using

                Call SetInformation(Grid)
                Grid.JSProperties("cpCPNumber") = Session("CPNumber")
                Call DisplayMessage(MsgTypeEnum.Success, "Data updated successfully!", Grid)

            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
            End Try
        ElseIf pfunction = "view" Then
            Try
                'SET HEADER CAPTION (SUPPLIER)
                '#Initialize
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("Supplier1").Visible = True
                Grid.Columns("Supplier2").Visible = True
                Grid.Columns("Supplier3").Visible = True
                Grid.Columns("Supplier4").Visible = True
                Grid.Columns("Supplier5").Visible = True
                Grid.Columns("BestPriceSupplier1").Visible = True
                Grid.Columns("BestPriceSupplier2").Visible = True
                Grid.Columns("BestPriceSupplier3").Visible = True
                Grid.Columns("BestPriceSupplier4").Visible = True
                Grid.Columns("BestPriceSupplier5").Visible = True

                '#Set by condition
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True : Grid.Columns("BestPriceSupplier1").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid.Columns("Supplier1").Visible = False
                    Grid.Columns("BestPriceSupplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True : Grid.Columns("BestPriceSupplier2").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid.Columns("Supplier2").Visible = False
                    Grid.Columns("BestPriceSupplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True : Grid.Columns("BestPriceSupplier3").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid.Columns("Supplier3").Visible = False
                    Grid.Columns("BestPriceSupplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True : Grid.Columns("BestPriceSupplier4").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid.Columns("Supplier4").Visible = False
                    Grid.Columns("BestPriceSupplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True : Grid.Columns("BestPriceSupplier5").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid.Columns("Supplier5").Visible = False
                    Grid.Columns("BestPriceSupplier5").Visible = False
                End Try
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
            End Try
        End If

        Dim ds2 As New DataSet, ErrMsg As String = ""
        ds2 = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_GetIANumber", "PRNumber|CENumber", cboPRNumber.Text & "|" & cboCENumber.Text, ErrMsg)
        If ErrMsg = "" Then
            If ds2.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cpLink") = ds2.Tables(0).Rows(0).Item("File_Name")
                Grid.JSProperties("cpCEBudget") = ds2.Tables(0).Rows(0).Item("CE_Budget").ToString()
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If

        ''After Save Data
        'If e.Parameters = "save" Then
        '    Grid.JSProperties("cpSaveData") = "1"

        '    If IsNothing(Session("E010_ErrMsg")) = True Then
        '        Call SetInformation(Grid)
        '        Grid.JSProperties("cpCPNumber") = gs_CPNumber
        '        Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", Grid)
        '    Else
        '        If Session("E010_ErrMsg") = "Please fill all of rows on the grid before save!" Then
        '            Call DisplayMessage(MsgTypeEnum.Warning, Session("E010_ErrMsg"), Grid)
        '            Session.Remove("E010_ErrMsg")
        '        End If
        '    End If

        'End If


        
    End Sub

    Private Sub cbExist_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbExist.Callback
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        Select Case e.Parameter
            Case "exist"
                Dim ret As String = "N"
                cbExist.JSProperties("cpParameter") = "exist"

                ret = IsDataExist()
                cbExist.JSProperties("cpResult") = ret

            Case "IsSubmitted"
                cbExist.JSProperties("cpParameter") = "IsSubmitted"
                If IsDataSubmitted() = True Then
                    cbExist.JSProperties("cpResult") = "1"
                Else
                    cbExist.JSProperties("cpResult") = "0"
                End If

                If IsDataApproved() = True Then
                    cbExist.JSProperties("cpResult2") = "1"
                Else
                    cbExist.JSProperties("cpResult2") = "0"
                End If

                If IsDataAccepted() = True Then
                    cbExist.JSProperties("cpResult3") = "1"
                Else
                    cbExist.JSProperties("cpResult3") = "0"
                End If

                If IsDataRejected() = True Then
                    cbExist.JSProperties("cpResult4") = "1"
                Else
                    cbExist.JSProperties("cpResult4") = "0"
                End If


            Case "draft"
                Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", cbExist)
            Case "submit"
                Dim ls_SQL As String = "", UserLogin As String = Session("user")
                Dim idx As Integer = 0

                Try
                    cbExist.JSProperties("cpParameter") = "submit"

                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()

                        Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                            ls_SQL = "EXEC sp_CP_Submit " & _
                                     "'" & gs_CPNumber & "','" & txtRev.Text & "'," & _
                                     "'" & cboCENumber.Text & "','" & Format(CDate(dtInvitationDate.Value), "yyyy-MM-dd") & "','1','" & UserLogin & "'"

                            Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()

                            sqlTran.Commit()
                        End Using
                    End Using

                    'cbExist.JSProperties("cpCPNumber") = txtCPNumber.Text
                    'cbExist.JSProperties("cpRev") = txtRev.Text
                    Call SetInformation(cbExist)
                    GridLoadMeeting(gs_CPNumber, txtRev.Text)

                    cbExist.JSProperties("cpCPNumber") = gs_CPNumber
                    Call DisplayMessage(MsgTypeEnum.Success, "Data submitted successfully!", cbExist)
                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                End Try
        End Select
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        If IsNothing(Session("btnAdd_CPList")) = False Then
            Session.Remove("btnAdd_CPList")
        End If

        Try
            If Request.QueryString.Count > 0 Then
                Dim pDateFrom As String = ConvertToFormatDate_yyyyMMdd(Split(Request.QueryString(0), "|")(6)), _
                    pDateTo As String = ConvertToFormatDate_yyyyMMdd(Split(Request.QueryString(0), "|")(7)), _
                    pPRNumber As String = Split(Request.QueryString(0), "|")(8), _
                    pSupplierCode As String = Split(Request.QueryString(0), "|")(9), _
                    pCPNumber As String = Split(Request.QueryString(0), "|")(10), _
                    pRowPos As String = Split(Request.QueryString(0), "|")(11)
                Session("btnBack_CPListDetail") = pDateFrom & "|" & pDateTo & "|" & pPRNumber & "|" & pSupplierCode & "|" & pCPNumber & "|" & pRowPos
            End If

        Catch ex As Exception

        End Try

        Response.Redirect("CPList.aspx")
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click
        If Request.QueryString.Count > 0 Then
            Session("E010_QueryString") = Request.QueryString(0)
        End If

        If txtCPNumber.Text = "--NEW--" Then
            txtCPNumber.Text = gs_CPNumber
        End If

        If txtRev.Text = "" Then
            txtRev.Text = "0"
        End If
        Dim CENo As String
        If cboCENumber.Text = "" Then
            CENo = txtCENumberTemp.Text
        Else
            CENo = cboCENumber.Text
        End If

        Dim PRNo As String
        If cboPRNumber.Text = "" Then
            PRNo = txtPRNumberTemp.Text
        Else
            PRNo = cboPRNumber.Text
        End If

        Session("CP_calledFrom") = "E010"
        Session("E010_paramViewCP") = txtCPNumber.Text & "|" & txtRev.Text & "|" & PRNo & "|" & CENo
        Response.Redirect("ViewCounterProposal.aspx")
    End Sub
#End Region

#Region "Event Invitation"
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        GridInvitation.JSProperties("cp_message") = ErrMsg
        GridInvitation.JSProperties("cp_type") = msgType
        GridInvitation.JSProperties("cp_val") = pVal
    End Sub

    Private Sub GridInvitation_BatchUpdate(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles GridInvitation.BatchUpdate
        Dim ls_Check As String
        Dim ls_SQL As String = "", ls_CP_Number As String = "", ls_Rev As String = "", ls_Supplier As String = "", ls_InvitationTime As DateTime, ls_InvitationDate As String = "", ls_InvitationLocation As String = "", ls_MsgID As String = ""
        Dim iLoop As Long = 0, jLoop As Long = 0
        Dim ls_UserID As String = ""
        If e.UpdateValues.Count = 0 Then
            Exit Sub
        End If
        Dim a As Integer
        a = e.UpdateValues.Count
        Dim pErr As String = ""
        For iLoop = 0 To a - 1
            ls_Check = (e.UpdateValues(iLoop).NewValues("AllowCheck").ToString())
            If ls_Check = True Then ls_Check = "1" Else ls_Check = "0"

            ls_CP_Number = txtCPNumberInvitation.Text
            ls_Rev = txtRevInvitation.Text

            ls_Supplier = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Supplier_Code")), "", e.UpdateValues(iLoop).NewValues("Supplier_Code"))
            ls_InvitationDate = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Invitation_Date")), "", e.UpdateValues(iLoop).NewValues("Invitation_Date"))
            ls_InvitationTime = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Invitation_Time")), "", e.UpdateValues(iLoop).NewValues("Invitation_Time"))
            ls_InvitationLocation = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Location_Meeting")), "", e.UpdateValues(iLoop).NewValues("Location_Meeting"))

            'Dim invitationDate As String
            ls_InvitationDate = Format(CDate(ls_InvitationDate), "yyyy-MM-dd")
            ' ls_InvitationTime = Format(ls_InvitationTime, "yyyy-MM-dd hh:mm")
            Dim ls_InvitationTime_Str As String
            ls_InvitationTime_Str = Format(ls_InvitationTime, "yyyy-MM-dd hh:mm")
            'Dim invitationTime As String
            'ls_InvitationTime = Format(ls_InvitationTime, "yyyy-MM-dd HH:mm:ss")

            Dim InvitationMeeting As New ClsCPList With
                {.CounterProposalCode = ls_CP_Number,
                 .Rev = ls_Rev,
                 .SupplierCode = ls_Supplier,
                 .Invitation_Date = ls_InvitationDate,
                 .Invitation_Time = ls_InvitationTime_Str,
                 .Location_Meeting = ls_InvitationLocation}
            ClsCPListDB.InvitationMeeting(InvitationMeeting, pErr)


            If ls_Check = "1" Then
                ClsCPListDB.resendInvitationMeeting(InvitationMeeting, pErr)
            End If

            If pErr <> "" Then
                Exit For
            End If
        Next iLoop
        GridInvitation.EndUpdate()
    End Sub

    Private Sub GridInvitation_CustomCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridInvitation.CustomCallback
        Dim pAction As String = Split(e.Parameters, "|")(0)
        Try
            Select Case pAction

                Case "loadInvitation"

                    If IsNothing(Session("E010_ErrMsg")) = True Then
                        Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", GridInvitation)
                    Else
                        If Session("E010_ErrMsg") = "Please fill all of rows on the grid before save!" Then
                            Call DisplayMessage(MsgTypeEnum.Warning, Session("E010_ErrMsg"), GridInvitation)
                            Session.Remove("E010_ErrMsg")
                            Exit Sub
                        End If
                    End If

                    Call GridLoadMeeting(txtCPNumberInvitation.Text, 0)

                    If IsDataAllSubmitted(txtCPNumberInvitation.Text, txtRevInvitation.Text) = True Then
                        ScriptManager.RegisterStartupScript(btnSubmitInvitation, btnSubmitInvitation.GetType(), "btnSubmitInvitation", "btnSubmitInvitation.SetEnabled(true);", True)
                    Else
                        ScriptManager.RegisterStartupScript(btnSubmitInvitation, btnSubmitInvitation.GetType(), "btnSubmitInvitation", "btnSubmitInvitation.SetEnabled(false);", True)
                    End If

                    If IsDataExpired(txtCPNumberInvitation.Text, txtRevInvitation.Text) = True Then
                        ScriptManager.RegisterStartupScript(btnResendInvitation, btnResendInvitation.GetType(), "btnResendInvitation", "btnResendInvitation.SetEnabled(true);", True)
                    Else
                        ScriptManager.RegisterStartupScript(btnResendInvitation, btnResendInvitation.GetType(), "btnResendInvitation", "btnResendInvitation.SetEnabled(false);", True)
                    End If


            End Select

        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub

    Protected Sub GridInvitation_CustomJSProperties(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewClientJSPropertiesEventArgs) Handles GridInvitation.CustomJSProperties
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        ' ds = clsRFQApprovalDB.CheckRFQApproval(txtRFQSetNo.Text, txtRev.Text, pUser, ErrMsg)
        If IsDataAllSubmitted(txtCPNumberInvitation.Text, txtRevInvitation.Text) = True Then
            Grid.JSProperties("cpExist") = "0"
        Else
            Grid.JSProperties("cpExist") = "1" 'disable grid
        End If
    End Sub

    Protected Sub cbInvitation_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbInvitation.Callback
        Select Case e.Parameter
            Case "IsSubmitted"
                cbInvitation.JSProperties("cpParameter") = "IsSubmitted"
                If IsDataAllSubmitted(txtCPNumberInvitation.Text, txtRevInvitation.Text) = True Then
                    cbInvitation.JSProperties("cpResult") = "1" 'enabled
                Else
                    cbInvitation.JSProperties("cpResult") = "0"
                End If

                If IsDataApproved() = True Then
                    cbInvitation.JSProperties("cpResult2") = "1"
                Else
                    cbInvitation.JSProperties("cpResult2") = "0"
                End If

                If IsDataAccepted() = True Then
                    cbInvitation.JSProperties("cpResult3") = "1"
                Else
                    cbInvitation.JSProperties("cpResult3") = "0"
                End If

                If IsDataRejected() = True Then
                    cbInvitation.JSProperties("cpResult4") = "1"
                Else
                    cbInvitation.JSProperties("cpResult4") = "0"
                End If
            Case "IsSubmitInvitation"
                cbInvitation.JSProperties("cpParameter") = "IsSubmitInvitation"
                If IsDataAllSubmitted(txtCPNumberInvitation.Text, txtRevInvitation.Text) = True Then
                    cbInvitation.JSProperties("cpResult") = "1" 'enabled
                Else
                    cbInvitation.JSProperties("cpResult") = "0" 'disabled
                End If

        End Select
    End Sub
#End Region

#Region "Row grid updating"

    'Private Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles Grid.RowDeleting
    '    e.Cancel = True
    'End Sub

    Private Sub Grid_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles Grid.RowInserting
        e.Cancel = True
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
    End Sub
#End Region



   
End Class
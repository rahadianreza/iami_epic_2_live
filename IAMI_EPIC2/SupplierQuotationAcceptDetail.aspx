﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="SupplierQuotationAcceptDetail.aspx.vb" Inherits="IAMI_EPIC2.SupplierQuotationAcceptDetail" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        var dataExist;
        var validCurrency;
        var validAllSupplierSubmit;
        var ListSupplierNotSubmit;

        function FillComboRevisionNo() {
            cboRevisionNo.PerformCallback();
            cboRevisionNo.SetEnabled(false);
        }

        function GetInformation() {
            //Get CboRevision Value
            hf.Set("cboRev", cboRevisionNo.GetText());

            window.setTimeout(function () {
                cb.PerformCallback();
            }, 100);

            if (cboRevisionNo.GetText() == (cboRevisionNo.GetItemCount() - 1)) {
                btnAccept.SetEnabled(true);
                btnReject.SetEnabled(true);
            } else {
                btnAccept.SetEnabled(false);
                btnReject.SetEnabled(false);
            }


            window.setTimeout(function () {
                Grid.PerformCallback("load");
            }, 50);

            window.setTimeout(function () {
                GridAtc.PerformCallback();
            }, 50);

            window.setTimeout(function () {
                cbExist.PerformCallback("CurrencyValidation");
            }, 100);

            window.setTimeout(function () {
                cbExist.PerformCallback("AllSupplierSubmitValidation");
            }, 100);
        }

        function SetInformation(s, e) {

            txtQuotationNo.SetText(s.cpQuotationNo);
            txtRFQTitle.SetText(s.cpRFQTitle);
            memoNote.SetText(s.cpNote);
            NoteReject.SetText(s.cpAcceptRejectNotes);

            var dt = s.cpQuotationDate;
            var YearFrom = dt.getFullYear();
            var MonthFrom = dt.getMonth();
            var DateFrom = dt.getDate();
            var newdate = new Date(YearFrom, MonthFrom, DateFrom)
            dtQuotationDate.SetDate(newdate);

            if (s.cpQuotationStatus == "1") {
                //Submit Only
                btnAccept.SetEnabled(true);
                btnReject.SetEnabled(true);
            } else {
                //Draft, Accept, Reject
                btnAccept.SetEnabled(false);
                btnReject.SetEnabled(false);
                NoteReject.SetEnabled(false);
            }
        }

        function ShowConfirmOnLosingChanges() {

        }


        function OnBatchEditStartEditing(s, e) {
            e.cancel = true;
            Grid.batchEditApi.EndEdit();
        }

        function OnBatchEditEndEditing(s, e) {

        }

        function UpdateUploadButton() {
            var isAnyFileSelected = false;
            for (var i = 0; i < ucAtc.GetFileInputCount(); i++) {
                if (ucAtc.GetText(i) != "") { isAnyFileSelected = true; break; }
            }
            //btnUploadViaPostback.SetEnabled(isAnyFileSelected);
            btnAddFile.SetEnabled(isAnyFileSelected);
        }

        function cbExistEndCallback(s, e) {
            if (s.cpParameter == "exist") {
                dataExist = s.cpResult;

            } else if (s.cpParameter == "accept") {

                txtQuotationNo.SetText(s.cpQuotationNo);
                txtRev.SetText(s.cpRevNo);
                memoNote.SetText(s.cpNote);
                //uncomment 
                var dt = s.cpQuotationDate;
                var YearFrom = dt.getFullYear();
                var MonthFrom = dt.getMonth();
                var DateFrom = dt.getDate();
                var newdate = new Date(YearFrom, MonthFrom, DateFrom)
                dtQuotationDate.SetDate(newdate);

                //            if (s.cpType == "0") {
                //                //INFO
                //                toastr.info(s.cpMessage, 'Information');

                //            } else if (s.cpType == "1") {
                //                //SUCCESS
                //                toastr.success(s.cpMessage, 'Success');

                //            } else if (s.cpType == "2") {
                //                //WARNING
                //                toastr.warning(s.cpMessage, 'Warning');

                //            } else if (s.cpType == "3") {
                //                //ERROR
                //                toastr.error(s.cpMessage, 'Error');

                //            }

                //            window.setTimeout(function () {
                //                delete s.cpType;
                //            }, 10);

            } else if (s.cpParameter == "reject") {

                //            if (s.cpType == "0") {
                //                //INFO
                //                toastr.info(s.cpMessage, 'Information');

                //            } else if (s.cpType == "1") {
                //                //SUCCESS
                //                toastr.success(s.cpMessage, 'Success');

                //            } else if (s.cpType == "2") {
                //                //WARNING
                //                toastr.warning(s.cpMessage, 'Warning');

                //            } else if (s.cpType == "3") {
                //                //ERROR
                //                toastr.error(s.cpMessage, 'Error');

                //            }

                //            window.setTimeout(function () {
                //                delete s.cpType;
                //            }, 10);

            } else if (s.cpParameter == "CurrValidation") {
                validCurrency = s.cpValidCurrency;

            } else if (s.cpParameter == "AllSupplierSubmitValidation") {
                validAllSupplierSubmit = s.cpValidAllSupplierSubmit;
                if (validAllSupplierSubmit == "N") {
                    ListSupplierNotSubmit = s.cpListSupplierNotSubmit;
                }
            }
        }



        function Accept() {
            //VALIDATION BEFORE ACCEPT
            if (validAllSupplierSubmit == "N") {
                toastr.warning('Cannot Accept quotation ' + txtQuotationNo.GetText() + '! Supplier ( ' + ListSupplierNotSubmit + ' ) not submit yet.', 'Warning');
                return false;
            }

            if (validCurrency == "N") {
                toastr.warning('Cannot Accept quotation ' + txtQuotationNo.GetText() + '! The currency of all suppliers are different.', 'Warning');
                return false;
            }


            //CONFIRMATION BEFORE ACCEPT
            var C = confirm("Are you sure want to accept the data?");
            if (C == true) {
                cbExist.PerformCallback("accept");
                btnAccept.SetEnabled(false);
                btnReject.SetEnabled(false);
                cboRevisionNo.SetEnabled(false);
                NoteReject.SetEnabled(false);
                $('html, body').animate({ scrollTop: 0 }, 'slow');

            } else {
                toastr.info('There is no data changes!', 'Information');
                return false;
            }
        }

        function Reject() {

            if (NoteReject.GetText() == '') {
                NoteReject.Focus();
                toastr.warning("Please Input Approval /Reject Note first!", "Warning");
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }

            var C = confirm("Are you sure want to reject the data?");

            if (C == true) {
                cbExist.PerformCallback("reject");

                window.setTimeout(function () {
                    btnAccept.SetEnabled(false);
                    btnReject.SetEnabled(false);
                    cboRevisionNo.SetEnabled(false);
                    NoteReject.SetEnabled(false);
                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }, 10);
            } else {
                toastr.info('There is no data changes!', 'Information');
                return;
            }
        }

        function Back() {
            location.replace("SupplierQuotationAccept.aspx");
        }



        function LoadCompleted(s, e) {
            if (s.cpActionAfter == "save") {
                txtQuotationNo.SetText(s.cpQuotationNo);
                memoNote.SetText(s.cpNote);
                //uncomment
                var dt = s.cpQuotationDate;
                var YearFrom = dt.getFullYear();
                var MonthFrom = dt.getMonth();
                var DateFrom = dt.getDate();
                var newdate = new Date(YearFrom, MonthFrom, DateFrom)
                dtQuotationDate.SetDate(newdate);
            }

            if (s.cpType == "0") {
                //INFO
                toastr.info(s.cpMessage, 'Information');

            } else if (s.cpType == "1") {
                //SUCCESS
                toastr.success(s.cpMessage, 'Success');

            } else if (s.cpType == "2") {
                //WARNING
                toastr.warning(s.cpMessage, 'Warning');

            } else if (s.cpType == "3") {
                //ERROR
                toastr.error(s.cpMessage, 'Error');

            }

            window.setTimeout(function () {
                delete s.cpType;
            }, 10);
        }

    </script>
    <style type="text/css">
        .colwidthbutton
        {
            width: 90px;
        }
        
        .rowheight
        {
            height: 35px;
        }
        
        .col1
        {
            width: 10px;
        }
        .col2
        {
            width: 133px;
        }
        .col3
        {
            width: 200px;
        }
        .col4
        {
            width: 10px;
        }
        .col5
        {
            width: 30px;
        }
        .col6
        {
            width: 80px;
        }
        .col7
        {
            width: 133px;
        }
        .col8
        {
            width: 100px;
        }
        
        .customHeader
        {
            height: 32px;
        }
        .style1
        {
            width: 200px;
            height: 35px;
        }
        .style2
        {
            width: 10px;
            height: 35px;
        }
        .style3
        {
            width: 30px;
            height: 35px;
        }
        .style4
        {
            width: 80px;
            height: 35px;
        }
        .style5
        {
            width: 133px;
            height: 35px;
        }
        .style6
        {
            width: 100px;
            height: 35px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
                <tr class="rowheight">
                    <td class="col1">
                    </td>
                    <td class="col2">
                        <dx1:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier Code">
                        </dx1:ASPxLabel>
                    </td>
                    <td colspan="6">
                        <table>
                            <tr>
                                <td class="col2">
                                    <dx:ASPxTextBox ID="txtSupplierCode" runat="server" Width="120px" Height="25px" ClientInstanceName="txtSupplierCode"
                                        BackColor="#CCCCCC" ReadOnly="True">
                                    </dx:ASPxTextBox>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="txtSupplierName" runat="server" Width="300px" Height="25px" ClientInstanceName="txtSupplierName"
                                        BackColor="#CCCCCC" ReadOnly="True">
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="rowheight">
                    <td class="rowheight">
                    </td>
                    <td class="rowheight">
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="RFQ Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="style1">
                        <dx:ASPxTextBox ID="txtRFQNumber" runat="server" Width="200px" Height="25px" ClientInstanceName="txtRFQNumber"
                            BackColor="#CCCCCC" ReadOnly="True">
                        </dx:ASPxTextBox>
                    </td>
                    <td class="style2">
                    </td>
                    <td class="style3">
                    </td>
                    <td class="style4">
                    </td>
                    <td class="style5">
                    </td>
                    <td class="style6">
                    </td>
                    <td class="rowheight">
                    </td>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                        <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="RFQ Title">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtRFQTitle" runat="server" Width="200px" Height="25px" ClientInstanceName="txtRFQTitle"
                            BackColor="#CCCCCC" ReadOnly="True">
                        </dx:ASPxTextBox>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                        <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Quotation Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtQuotationNo" runat="server" Width="200px" Height="25px" ClientInstanceName="txtQuotationNo"
                            BackColor="#CCCCCC" ReadOnly="True">
                        </dx:ASPxTextBox>
                    </td>
                    <td colspan="6">
                        <table>
                            <tr>
                                <td width="10px"></td>
                                <td width="50px">
                                   <%-- <dx:ASPxTextBox ID="txtRevision" runat="server" Width="40px" Height="25px" ClientInstanceName="txtRevision"
                                        BackColor="#CCCCCC" ReadOnly="True">
                                    </dx:ASPxTextBox>--%>
                                    <dx1:ASPxComboBox ID="cboRevisionNo" runat="server" ClientInstanceName="cboRevisionNo"
                                        Width="45px" Font-Names="Segoe UI" TextField="Rev" Enabled="true" ValueField="Rev"
                                        TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px"
                                        AnimationType="None" ForeColor="Black" BackColor="#CCCCCC">
                                        <Columns>
                                            <dx:ListBoxColumn Caption="Rev" FieldName="Rev" Width="45px" />
                                        </Columns>
                                        <ItemStyle Height="10px" Paddings-Padding="4px">
                                            <Paddings Padding="4px"></Paddings>
                                        </ItemStyle>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                                            <Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                        <ClientSideEvents Init="FillComboRevisionNo" ValueChanged="GetInformation" />
                                    </dx1:ASPxComboBox>
                                </td>
                                <td width="10px"></td>
                                <td width="100px">
                                    <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                        Text="Quotation Date">
                                    </dx1:ASPxLabel>
                                </td>
                                
                                <td>
                                    <dx:ASPxDateEdit ID="dtQuotationDate" runat="server" Theme="Office2010Black" ReadOnly="true"
                                        Width="120px" AutoPostBack="false" ClientInstanceName="dtQuotationDate" BackColor="#CCCCCC"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy" Font-Names="Segoe UI"
                                        Font-Size="9pt" Height="25px">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                                        </ButtonStyle>
                                    </dx:ASPxDateEdit>
                                </td>
                            </tr>
                        </table>
                    </td>
                    
                </tr>
                <tr>
                    <td colspan="9">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="8">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                        &nbsp;
                        <dx:ASPxCallback ID="cb" runat="server" ClientInstanceName="cb">
                            <ClientSideEvents EndCallback="SetInformation" />
                        </dx:ASPxCallback>
                        &nbsp;
                        <dx:ASPxCallback ID="cbExist" runat="server" ClientInstanceName="cbExist">
                            <ClientSideEvents EndCallback="cbExistEndCallback" CallbackComplete="LoadCompleted" />
                        </dx:ASPxCallback>
                        &nbsp;
                        <asp:SqlDataSource ID="dsCurr" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="SELECT Par_Code = RTRIM(Par_Code) FROM Mst_Parameter WHERE Par_Group = 'Currency'">
                        </asp:SqlDataSource>
                        &nbsp;
                        <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf">
                        </dx:ASPxHiddenField>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" Width="100%" Heigth="100px" Font-Names="Segoe UI"
                Font-Size="9pt" KeyFieldName="Material_No">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Material No" FieldName="Material_No" FixedStyle="Left"
                        VisibleIndex="0" Width="120px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" VisibleIndex="1"
                        Width="300px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Specification" FieldName="Specification" VisibleIndex="2"
                        Width="300px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Qty" FieldName="Qty" VisibleIndex="3" Width="60px">
                        <PropertiesTextEdit DisplayFormatString="###,###">
                        </PropertiesTextEdit>
                        <CellStyle HorizontalAlign="Right">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="UOM" FieldName="UOM" VisibleIndex="4" Width="50px">
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Currency" FieldName="Currency" VisibleIndex="5"
                        Width="70px">
                        <PropertiesComboBox DataSourceID="dsCurr" TextField="Par_Code" ValueField="Par_Code"
                            DropDownRows="5" Width="65px">
                        </PropertiesComboBox>
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataTextColumn Caption="Price/Pcs" FieldName="Price" VisibleIndex="6"
                        Width="80px">
                        <PropertiesTextEdit DisplayFormatString="###,###" Width="95px">
                            <Style HorizontalAlign="Right">
                                
                            </Style>
                        </PropertiesTextEdit>
                        <CellStyle HorizontalAlign="Right">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice" VisibleIndex="7"
                        Width="100px">
                        <PropertiesTextEdit DisplayFormatString="###,###">
                        </PropertiesTextEdit>
                        <EditCellStyle BackColor="LemonChiffon">
                        </EditCellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Quotation No" FieldName="Quotation_No" VisibleIndex="8"
                        Width="180px">
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowSelectByRowClick="True" AllowSort="False" AllowDragDrop="False"
                    ColumnResizeMode="Control" />
                <SettingsPager Mode="ShowAllRecords">
                </SettingsPager>
                <Settings HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" ShowStatusBar="Hidden" />
                <SettingsEditing Mode="Batch">
                    <BatchEditSettings StartEditAction="DblClick" ShowConfirmOnLosingChanges="False" />
                </SettingsEditing>
                <ClientSideEvents CallbackError="function(s, e) {e.handled = true;}" EndCallback="LoadCompleted"
                    BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing" />
                <Styles Header-Paddings-Padding="2px" EditFormColumnCaption-Paddings-PaddingLeft="0px"
                    EditFormColumnCaption-Paddings-PaddingRight="0px">
                    <Header CssClass="customHeader" HorizontalAlign="Center">
                    </Header>
                </Styles>
            </dx:ASPxGridView>
        </div>
        <br />
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
                <tr>
                    <td class="col1">
                    </td>
                    <td colspan="8">
                        <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Note :">
                        </dx1:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="7">
                        <dx:ASPxMemo ID="memoNote" runat="server" Height="45px" Width="100%" ClientInstanceName="memoNote"
                            BackColor="#CCCCCC" ReadOnly="true">
                        </dx:ASPxMemo>
                    </td>
                    <td class="col1">
                    </td>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="8">
                        <dx1:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Attachment :">
                        </dx1:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="7">
                        <dx:ASPxGridView ID="GridAtc" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridAtc"
                            EnableTheming="True" Theme="Office2010Black" Width="100%" Settings-VerticalScrollableHeight="80"
                            Settings-VerticalScrollBarMode="Visible" Font-Names="Segoe UI" Font-Size="9pt"
                            KeyFieldName="FileName;FilePath">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption=" " Width="30px" FieldName="Chk">
                                </dx:GridViewDataTextColumn>
                                <%--<dx:GridViewDataCheckColumn Caption=" " Name="chk" VisibleIndex="0" Width="30px">
                                <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueType="System.Int32" >
                                </PropertiesCheckEdit>
                            </dx:GridViewDataCheckColumn>--%>
                                <dx:GridViewDataTextColumn Caption="File Type" FieldName="FileType" VisibleIndex="1"
                                    Width="80px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="File Name" FieldName="FileName" VisibleIndex="2"
                                    Width="500px">
                                    <DataItemTemplate>
                                        <dx:ASPxHyperLink ID="linkFileName" runat="server" OnInit="keyFieldLink_Init">
                                        </dx:ASPxHyperLink>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="File Path" FieldName="FilePath" VisibleIndex="3"
                                    Width="0px" Visible="false">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsEditing Mode="Batch">
                                <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                            </SettingsEditing>
                            <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSort="False" />
                            <Settings VerticalScrollableHeight="100" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto">
                            </Settings>
                            <Styles>
                                <Header CssClass="customHeader" HorizontalAlign="Center">
                                </Header>
                            </Styles>
                            <ClientSideEvents CallbackError="function(s, e) {e.handled = true;}" EndCallback="LoadCompleted"
                                BatchEditStartEditing="OnBatchEditStartEditing" />
                        </dx:ASPxGridView>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="rowheight">
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <dx1:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Approval Note :">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="7">
                        <dx:ASPxMemo ID="NoteReject" runat="server" Height="45px" Width="100%" ClientInstanceName="NoteReject" MaxLength="300">
                        </dx:ASPxMemo>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="7">
                        <table>
                            <tr class="rowheight">
                                <td class="colwidthbutton">
                                    <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                                        ClientInstanceName="btnBack" Theme="Default">
                                        <Paddings Padding="2px" />
                                        <ClientSideEvents Click="" />
                                    </dx:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    <dx:ASPxButton ID="btnAccept" runat="server" Text="Accept" Font-Names="Segoe UI"
                                        Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false" ClientInstanceName="btnAccept"
                                        Theme="Default">
                                        <ClientSideEvents Click="Accept" />
                                        <Paddings Padding="2px" />
                                    </dx:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    <dx:ASPxButton ID="btnReject" runat="server" Text="Reject" UseSubmitBehavior="false"
                                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                        ClientInstanceName="btnReject" Theme="Default">
                                        <ClientSideEvents Click="Reject" />
                                        <Paddings Padding="2px" />
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RFQCheekSheet.aspx.vb"
    Inherits="IAMI_EPIC2.RFQCheekSheet" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };
        function MessageBox(s, e) {
            //alert(s.cpmessage);
            if (s.cbMessage == "Download Excel Successfully") {
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cbMessage == "Request Date To must be higher than Request Date From") {
                toastr.success(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }


        function gv_OnCustomButtonClick(s, e) {
            if (e.buttonID == 'detailList') {
                var key = Grid.GetRowKey(e.visibleIndex);
                var keysplit = key.split("|");

                window.location.href = 'RFQCheekSheetDetail.aspx?projectid=' + keysplit[0] + '&groupid=' + keysplit[1] + '&commodity=' + keysplit[2] + '&groupcommodity=' + keysplit[3] + '&VendorCode=' + keysplit[4] + '&NoRFQ=' + keysplit[5] + '&DateRFQ=' + keysplit[6] + '&Pos=1' + '&datefrom=' + ProjectDate_From.GetText() + '&dateto=' + ProjectDate_To.GetText()
            }           
        }
//+'&ProjType=' + cboProjectType.GetValue()
    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        .style4
        {
            width: 24px;
        }
        .style8
        {
            height: 38px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<table style="width: 100%; border: 1px solid black; height: 100px;">
<tr>
<td></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="RFQ Date">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxDateEdit ID="ProjectDate_From" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="ProjectDate_From" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
            <%--        <ClientSideEvents ValueChanged="GridLoad" />--%>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>
            </td>
<td class="style4" align = "center">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="~">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxDateEdit ID="ProjectDate_To" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="ProjectDate_To" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="16px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
         <%--           <ClientSideEvents ValueChanged="GridLoad" />--%>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>
            </td>
<td class="style4">&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Group">
                </dx:ASPxLabel>
                    </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxComboBox ID="cboGroup" runat="server" ClientInstanceName="cboGroup" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Group_ID" ValueField="Group_ID"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                cboCommodity.PerformCallback(cboGroup.GetSelectedItem().GetColumnText(0));
                    cboCommodityGroup.PerformCallback('');
                    }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Group ID" FieldName="Group_ID" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td class="style4">&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Sourcing Group">
                        </dx:ASPxLabel>
                    </td>
<td style="padding: 0px 0px 0px 10px; width: 200px">
                        <dx:ASPxComboBox ID="cboCommodityGroup" runat="server" ClientInstanceName="cboCommodityGroup"
                            Font-Names="Segoe UI" TextField="Group_Comodity" 
        ValueField="Group_Comodity" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" 
        DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" 
        TabIndex="5">
                            <Columns>
                                <dx:ListBoxColumn Caption="Sourcing Group" FieldName="Group_Comodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td>&nbsp;</td>
</tr>
<tr style="height: 2px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td class="style4" align = "center">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Name">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 200px" colspan="3" >


         <%--       <dx:ASPxComboBox ID="cboProjectType" runat="server" ClientInstanceName="cboProjectType"
                    Width="240px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1"  TextField="Description" ValueField="Code"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                  cboProject.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0));
                                  cboGroup.PerformCallback('');
                                  cboCommodity.PerformCallback('');
                                  cboCommodityGroup.PerformCallback('');
                                   }"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>--%>
                        <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                            Font-Names="Segoe UI" TextField="Project_Name" ValueField="Project_ID" TextFormatString="{1}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                cboGroup.PerformCallback(cboProject.GetSelectedItem().GetColumnText(0));
                    cboCommodity.PerformCallback('');
                    cboCommodityGroup.PerformCallback('');
                }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Commodity">
                        </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxComboBox ID="cboCommodity" runat="server" ClientInstanceName="cboCommodity"
                            Font-Names="Segoe UI" TextField="Commodity" ValueField="Commodity" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                            cboCommodityGroup.PerformCallback(cboCommodity.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Commodity" FieldName="Commodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                       <%-- <dx:ASPxLabel ID="ASPxLabel7" runat="server" 
        Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Sourcing Group">
                        </dx:ASPxLabel>--%>
                    </td>
<td style="padding: 0px 0px 0px 10px; width: 200px">
                        &nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr>
<td style="padding: 20px 0px 0px 10px" class="style8" colspan="4">
                <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnShowData" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
                 
                            Grid.PerformCallback('load| ' + '|' + ProjectDate_From.GetValue() + '|' + ProjectDate_To.GetValue() );
                        
                        }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnDownload" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
     <%--    Grid.PerformCallback('load|' + cboProjectType.GetValue() + '|' + ProjectDate_From.GetValue() + '|' + ProjectDate_To.GetValue() );--%>
            </td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>

    <div style="padding: 5px 0px 5px 0px">
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'ProjectType'">
        </asp:SqlDataSource>

         <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="select UserID, UserName from UserSetup">
        </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            
            SelectCommand="SELECT GS.Supplier_Code, MS.Supplier_Name FROM Drawing_Acceptance DA
            INNER JOIN Gate_I_CS_Bidders_List_N_Supplier_Information GS ON DA.Project_ID = GS.Project_ID And DA.Group_ID = GS.Group_ID AND DA.Commodity = GS.Commodity AND DA.Group_Comodity = GS.Group_Comodity
            INNER JOIN Mst_Supplier MS ON GS.Supplier_Code = MS.Supplier_Code 
            WHERE DA.Project_ID = @ProjectCode AND DA.Group_ID = @Group AND DA.Commodity = @Commodity And DA.Group_Comodity = @Group_Comodity ">
                <SelectParameters>
                    <asp:ControlParameter ControlID="cboProject" Name="ProjectCode" 
                        PropertyName="Value" />
                    <asp:ControlParameter ControlID="cboGroup" Name="Group" PropertyName="Value" />
                    <asp:ControlParameter ControlID="cboCommodity" Name="Commodity" 
                        PropertyName="Value" />
                        <asp:ControlParameter ControlID="cboCommodityGroup" Name="Group_Comodity" 
                        PropertyName="Value" />
                </SelectParameters>
        </asp:SqlDataSource>
        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
            <ClientSideEvents CallbackComplete="SetCode" />
        </dx:ASPxCallback>
        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
        </dx:ASPxGridViewExporter>
       <%-- OnStartRowEditing="Grid_StartRowEditing"--%>
       <%--KeyFieldName="Project_ID;Group_ID;Comodity;Group_Comodity;VendorCode;NoRFQ;DateRFQ" --%>
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" KeyFieldName="Project_ID;Group_ID;Comodity;Group_Comodity;VendorCode;NoRFQ;DateRFQ"
            Font-Names="Segoe UI" Font-Size="9pt" OnRowValidating="Grid_RowValidating" 
             OnRowInserting="Grid_RowInserting" OnRowDeleting="Grid_RowDeleting" 
            OnAfterPerformCallback="Grid_AfterPerformCallback">
            <ClientSideEvents EndCallback="OnEndCallback" CustomButtonClick="gv_OnCustomButtonClick" ></ClientSideEvents>
            <Columns>                           
                 <dx:GridViewCommandColumn VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="false" ShowClearFilterButton="true" Width="160px">
                                            <CustomButtons>
                                                <dx:GridViewCommandColumnCustomButton ID="detailList" Text="Detail List">
                                                </dx:GridViewCommandColumnCustomButton>
                                            </CustomButtons>
                                        </dx:GridViewCommandColumn>


                <dx:GridViewDataTextColumn Caption="Supplier Name" FieldName="VendorName" VisibleIndex="2"
                        Width="250px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Supplier Code" FieldName="VendorCode" VisibleIndex="3" 
                    Width="0px" EditFormCaptionStyle-Paddings-Padding="5">
     
            <PropertiesComboBox DataSourceID="SqlDataSource4" TextField="VendorCode" 
                            ValueField="Supplier_Code" Width="180px" TextFormatString="{1}" ClientInstanceName="VendorCode"
                            DropDownStyle="DropDownList" IncrementalFilteringMode="Contains">                            
                            <Columns>
                                <dx:listboxcolumn FieldName="Supplier_Code" Caption="Supplier Code" Width="100px" />
                                <dx:listboxcolumn FieldName="Supplier_Name" Caption="Supplier Name" Width="150px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px" >
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="2px">
                                <Paddings Padding="2px"></Paddings>
                            </ButtonStyle>
                        </PropertiesComboBox>

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                </dx:GridViewDataComboBoxColumn>
                 <dx:GridViewDataTextColumn Caption="No RFQ" FieldName="NoRFQ" VisibleIndex="1"
                        Width="250px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                        <PropertiesTextEdit Width="180px" ClientInstanceName="NoRFQ" MaxLength="30">
                            <Style HorizontalAlign="Left">
                                
                            </Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />

                        <EditFormCaptionStyle>
                        <Paddings Padding="5px"></Paddings>
                        </EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                   
                     <dx:GridViewDataTextColumn Caption="RFQ Date" FieldName="DateRFQ" VisibleIndex="4"
                        Width="250px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                        <PropertiesTextEdit Width="180px" ClientInstanceName="DateRFQ" MaxLength="80">
                            <Style HorizontalAlign="Left">
                                
                            </Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />

                        <EditFormCaptionStyle>
                        <Paddings Padding="5px"></Paddings>
                        </EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>           
             <%--     <dx:GridViewDataTextColumn Caption="RFQ Date" FieldName="DateRFQ" 
                     VisibleIndex="4" Width="90px" EditFormSettings-Visible="false" >
                      <PropertiesTextEdit Width="180px" ClientInstanceName="DateRFQ" MaxLength="80px">
                            <Style HorizontalAlign="Left">
                                
                            </Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                     <PropertiesDateEdit 
                          DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                          EditFormatString="dd MMM yyyy" Width="80px">
                      </PropertiesDateEdit>
                      <Settings AllowAutoFilter="True" />
                      <EditFormCaptionStyle>
                          <Paddings Padding="5px" />
                      </EditFormCaptionStyle>
                      <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                      <Paddings PaddingLeft="5px" />
                      </HeaderStyle>
                 </dx:GridViewDataTextColumn>--%>

                <dx:GridViewDataTextColumn Caption="To" VisibleIndex="6" FieldName="ToRFQ" 
                     EditFormCaptionStyle-Paddings-Padding="5">
                    <Settings AutoFilterCondition="Contains" />

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                     <PropertiesTextEdit MaxLength="20" Width="150px"></PropertiesTextEdit>

                </dx:GridViewDataTextColumn>
             
                <dx:GridViewDataTextColumn Caption="Phone" VisibleIndex="7" FieldName="PhoneRFQ"
                    Width="130px" EditFormCaptionStyle-Paddings-Padding="5">
                    <PropertiesTextEdit MaxLength="50" Width="200px">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Fax" VisibleIndex="8" FieldName="FaxRFQ" 
                     EditFormCaptionStyle-Paddings-Padding="5">
                    <Settings AutoFilterCondition="Contains" />

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                     <PropertiesTextEdit MaxLength="25" Width="180px"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Attn Dept Head" FieldName="AttDeptHeadName" VisibleIndex="9"
                        Width="250px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                 <dx:GridViewDataComboBoxColumn Caption="Attn Dept Head" FieldName="AttDeptHead" VisibleIndex="9"
                    Width="0px" EditFormCaptionStyle-Paddings-Padding="5">
     
            <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="UserName" 
                            ValueField="UserID" Width="145px" TextFormatString="{1}" ClientInstanceName="AttDeptHead"
                            DropDownStyle="DropDownList" IncrementalFilteringMode="Contains">                            
                            <Columns>
                                <dx:listboxcolumn FieldName="UserID" Caption="User ID" Width="100px" />
                                <dx:listboxcolumn FieldName="UserName" Caption="User Name" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px" >
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="2px">
                                <Paddings Padding="2px"></Paddings>
                            </ButtonStyle>
                        </PropertiesComboBox>

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                </dx:GridViewDataComboBoxColumn>
     
                 <dx:GridViewDataTextColumn Caption="Email Dept Head" VisibleIndex="10" 
                     FieldName="EmailDeptHead" EditFormCaptionStyle-Paddings-Padding="5">
                    <Settings AutoFilterCondition="Contains" />

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                       <PropertiesTextEdit MaxLength="25" Width="180px"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn Caption="Attn Project PIC" FieldName="AttProjectPICName" VisibleIndex="11"
                        Width="250px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                  <dx:GridViewDataComboBoxColumn Caption="Attn Project PIC" FieldName="AttProjectPIC" VisibleIndex="11"
                    Width="0px" EditFormCaptionStyle-Paddings-Padding="5">
     
            <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="UserName" 
                            ValueField="UserID" Width="145px" TextFormatString="{1}" ClientInstanceName="AttProjectPIC"
                            DropDownStyle="DropDownList" IncrementalFilteringMode="Contains">                            
                            <Columns>
                                <dx:listboxcolumn FieldName="UserID" Caption="User ID" Width="100px" />
                                <dx:listboxcolumn FieldName="UserName" Caption="User Name" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px" >
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="2px">
                                <Paddings Padding="2px"></Paddings>
                            </ButtonStyle>
                        </PropertiesComboBox>

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                </dx:GridViewDataComboBoxColumn>

                 <dx:GridViewDataTextColumn Caption="Email Project PIC" VisibleIndex="12" 
                     FieldName="EmailProjectPIC" EditFormCaptionStyle-Paddings-Padding="5">
                    <Settings AutoFilterCondition="Contains" />

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                     <PropertiesTextEdit MaxLength="50" Width="200px"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn Caption="Attn Cost PIC" FieldName="AttCostPICName" VisibleIndex="13"
                        Width="250px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                  <dx:GridViewDataComboBoxColumn Caption="Attn Cost PIC" FieldName="AttCostPIC" VisibleIndex="13"
                    Width="0px" EditFormCaptionStyle-Paddings-Padding="5">
     
            <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="UserName" 
                            ValueField="UserID" Width="145px" TextFormatString="{1}" ClientInstanceName="AttCostPIC"
                            DropDownStyle="DropDownList" IncrementalFilteringMode="Contains">                            
                            <Columns>
                                <dx:listboxcolumn FieldName="UserID" Caption="User ID" Width="100px" />
                                <dx:listboxcolumn FieldName="UserName" Caption="User Name" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px" >
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="2px">
                                <Paddings Padding="2px"></Paddings>
                            </ButtonStyle>
                        </PropertiesComboBox>

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                </dx:GridViewDataComboBoxColumn>
   
                 <dx:GridViewDataTextColumn Caption="Email Cost PIC" VisibleIndex="14" 
                     FieldName="EmailCostPIC" EditFormCaptionStyle-Paddings-Padding="5">
                    <Settings AutoFilterCondition="Contains" />

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                       <PropertiesTextEdit MaxLength="25" Width="180px"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                
                <%-- <dx:GridViewDataTextColumn Caption="AppPerson" VisibleIndex="15" 
                     FieldName="AppPerson1" EditFormCaptionStyle-Paddings-Padding="5">
                    <Settings AutoFilterCondition="Contains" />

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                       <PropertiesTextEdit MaxLength="25" Width="180px"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>--%>
      <%--             
                      <dx:GridViewBandColumn Caption="Approve1" VisibleIndex="16" Name="Approve1"  Visible="false" HeaderStyle-HorizontalAlign="Center" >

                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Approval Name" FieldName="ApprovalName1" VisibleIndex="17"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Date" FieldName="ApprovalDate1" VisibleIndex="18"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5" >
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                                    </Columns>
                       </dx:GridViewBandColumn>
                         <dx:GridViewBandColumn Caption="Approve2" VisibleIndex="19" Name="Approve2"  Visible="false" HeaderStyle-HorizontalAlign="Center" >

                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Approval Name" FieldName="ApprovalName2" VisibleIndex="20"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Date" FieldName="ApprovalDate2" VisibleIndex="21"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                                    </Columns>
                       </dx:GridViewBandColumn>
                         <dx:GridViewBandColumn Caption="Approve3" VisibleIndex="22" Name="Approve3"  Visible="false" HeaderStyle-HorizontalAlign="Center" >

                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Approval Name" FieldName="ApprovalName3" VisibleIndex="23"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Date" FieldName="ApprovalDate3" VisibleIndex="24"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                                    </Columns>
                       </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Approve4" VisibleIndex="25" Name="Approve4"  Visible="false" HeaderStyle-HorizontalAlign="Center" >

                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Approval Name" FieldName="ApprovalName4" VisibleIndex="26"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Date" FieldName="ApprovalDate4" VisibleIndex="27"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                                    </Columns>
                       </dx:GridViewBandColumn>
                       <dx:GridViewBandColumn Caption="Approve5" VisibleIndex="28" Name="Approve5"  Visible="false" HeaderStyle-HorizontalAlign="Center" >

                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Approval Name" FieldName="ApprovalName5" VisibleIndex="29"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Date" FieldName="ApprovalDate5" VisibleIndex="30"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                                    </Columns>
                       </dx:GridViewBandColumn>
                       <dx:GridViewBandColumn Caption="Approve6" VisibleIndex="31" Name="Approve6"  Visible="false" HeaderStyle-HorizontalAlign="Center" >

                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Approval Name" FieldName="ApprovalName6" VisibleIndex="32"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Date" FieldName="ApprovalDate6" VisibleIndex="33"
                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Paddings PaddingLeft="6px" />
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                                    </Columns>
                       </dx:GridViewBandColumn>--%>
            </Columns>
                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
        </dx:ASPxGridView>

    </div>
</asp:Content>

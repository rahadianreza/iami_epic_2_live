﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LabourDetail.aspx.vb" Inherits="IAMI_EPIC2.LabourDetail" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;


            //alert(s.cpButtonText);
            btnSubmit.SetText(s.cpButtonText);
            cboProvince.SetEnabled(false);
            cboRegion.SetEnabled(false);
            txtPeriod.SetEnabled(false);
        }
       
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/Labor.aspx";
            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/Labour.aspx';
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                if (pathArray[1] == "LabourDetail.aspx") {
                    window.location.href = window.location.origin + '/Labour.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            cboProvince.SetEnabled(true);
            cboRegion.SetEnabled(true);
            txtPeriod.SetEnabled(true);
            cboProvince.SetText('');
            cboRegion.SetText('');

            var day = new Date();
            var year = day.getFullYear();
            txtPeriod.SetText(year);
            txtUMR.SetText(0);
            cboSectoral.SetText('');
            txtUMSP.SetText(0);
            txtEstimatedUMSP.SetText(0);
            
        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }

    function GridLoad() {
        txtSectoralTemp.SetText(cboSectoral.GetValue());
       
    }


    //filter combobox regioncity berdasarkan province yang dipilih
    function FilterRegion() {
        cboRegion.PerformCallback('filter|' + cboProvince.GetValue());
    }


    function SubmitProcess(s, e) {
        if (s.GetText() == 'Update') {
            if (cboRegion.GetText() == '') {
                toastr.warning('Please Select Region!', 'Warning');
                cboRegion.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            if (txtUMR.GetText() == '' || txtUMR.GetText() == 0) {
                toastr.warning('UMR must greater than zero', 'Warning');
                txtUMR.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            if (txtEstimatedUMSP.GetText() == '' || txtEstimatedUMSP.GetText() == 0) {
                toastr.warning('Estimated UMR must greater than zero', 'Warning');
                txtEstimatedUMSP.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            

//          sectoral jangan buat mandatory
//             else if (cboSectoral.GetText() == '') {
//                toastr.warning('Please Select Sectoral!', 'Warning');
//                cboSectoral.Focus();
//                toastr.options.closeButton = false;
//                toastr.options.debug = false;
//                toastr.options.newestOnTop = false;
//                toastr.options.progressBar = false;
//                toastr.options.preventDuplicates = true;
//                toastr.options.onclick = null;
//                e.processOnServer = false;
//                return;

            } else if (txtUMSP.GetText() == '' || txtUMSP.GetText() == 0) {
                toastr.warning('UMSP must greater than zero', 'Warning');
                txtUMSP.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }
        else {
            if (cboProvince.GetText() == '') {
                toastr.warning('Please Select Province!', 'Warning');
                cboProvince.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;

            } else if (cboRegion.GetText() == '') {
                toastr.warning('Please Select Region!', 'Warning');
                cboRegion.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            } else if (txtPeriod.GetText() == '') {
                toastr.warning('Please input valid period!', 'Warning');
                txtPeriod.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            } else if (txtUMR.GetText() == '' || txtUMR.GetText() == 0) {
                toastr.warning('UMR must greater than zero', 'Warning');
                txtUMR.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
                //sectoral jangan buat mandatory
//            } else if (cboSectoral.GetText() == '' ) {
//                toastr.warning('Please Select Sectoral!', 'Warning');
//                cboSectoral.Focus();
//                toastr.options.closeButton = false;
//                toastr.options.debug = false;
//                toastr.options.newestOnTop = false;
//                toastr.options.progressBar = false;
//                toastr.options.preventDuplicates = true;
//                toastr.options.onclick = null;
//                e.processOnServer = false;
//                return;
            } else if (txtUMSP.GetText() == '' || txtUMSP.GetText() == 0) {
                toastr.warning('UMSP must greater than zero', 'Warning');
                txtUMSP.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }
        cbSave.PerformCallback('Save');

    }

    function OnComboBoxKeyDown(s, e) {
        // 'Delete' button key code = 46

        if (e.htmlEvent.keyCode == 46 || e.htmlEvent.keyCode == 8)
            s.SetSelectedIndex(-1);
        //alert(s.GetValue());
    }

    </script>
    <style type="text/css">
        .td-col-l 
        {
            padding:0px 0px 0px 10px;
            width:120px;
        }
        .td-col-m
        {
            width:20px;
        }
        .td-col-r
        {
            width:200px;
        }
        
        .tr-height
        {
            height: 35px;
        }
            
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr style="height:10px">
            <td colspan="5"></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Province">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m">
                &nbsp;
            </td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboProvince" runat="server" ClientInstanceName="cboProvince"
                    Width="170px" Font-Names="Segoe UI" TextField="ProvinceDesc" ValueField="Province" DataSourceID="SqlDataSource1"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDownList" IncrementalFilteringMode="Contains"  Height="22px">
                     <ClientSideEvents SelectedIndexChanged="FilterRegion" KeyDown="OnComboBoxKeyDown" />   
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Province" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="ProvinceDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                    
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Province, Rtrim(Par_Description) ProvinceDesc  From Mst_Parameter Where Par_Group = 'Provinsi'">
                </asp:SqlDataSource>
            </td>
            <td></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="City / Region">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboRegion" runat="server" ClientInstanceName="cboRegion"
                    Width="170px" Font-Names="Segoe UI" TextField="RegionDesc" ValueField="Region"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDownList"
                    IncrementalFilteringMode="Contains" Height="25px">
                    <ClientSideEvents ValueChanged="function(s, e) {
	txtRegion.SetText(cboRegion.GetValue());
}" KeyDown="OnComboBoxKeyDown"   />
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Region" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="RegionDesc" Width="120px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                    
                </dx:ASPxComboBox>
            </td>
            <td>
               
            </td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="lblPeriod" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
              <dx:ASPxSpinEdit ID="txtPeriod" runat="server" ClientInstanceName="txtPeriod" Font-Size="9pt" NumberType="Integer" MaxLength="4" Width="100px" Height="25px"></dx:ASPxSpinEdit>
            </td>
            <td></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="UMR">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtUMR" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtUMR"  HorizontalAlign="Right">
                     <MaskSettings Mask="<0..100000000g>.<00..99>" />
                </dx:ASPxTextBox>
           
            </td>
            <td></td>
        </tr>
      
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Sectoral">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboSectoral" runat="server" ClientInstanceName="cboSectoral"
                    Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code" DataSourceID="SqlDataSource2"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDownList"
                    IncrementalFilteringMode="Contains"  Height="22px">
                     <ClientSideEvents SelectedIndexChanged="GridLoad" KeyDown="OnComboBoxKeyDown" />
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                   
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Sectoral'">
                </asp:SqlDataSource>
            </td>
            <td></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="UMSP">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtUMSP" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtUMSP" HorizontalAlign="Right">
                   <MaskSettings Mask="<0..100000000g>.<00..99>" />
                </dx:ASPxTextBox>
            </td>
          
            <td></td>
        </tr>
          <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Estimated UMSP">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtEstimatedUMSP" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtEstimatedUMSP"  HorizontalAlign="Right">
                     <MaskSettings Mask="<0..100000000g>.<00..99>" />
                </dx:ASPxTextBox>
            </td>
            <td></td>
        </tr>
       <tr >
           <td class="td-col-l"></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>

       </tr>
        <tr style="height: 25px;">
            <td class="td-col-l"></td>
            <td class="td-col-l"></td>
            <td></td>
            <td style="width: 500px">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack"
                    Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>

                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                   
                    <ClientSideEvents Click="function(s, e) {
	var msg = confirm('Are you sure want to submit this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }
	SubmitProcess(s,e);
    
   
}" />
                   
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
              <%--  <dx:ASPxButton ID="btnUpdate" runat="server" Text="Update" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnUpdate" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>--%>

            </td>
            <td></td>
           
        </tr>
        <tr style="display:none">
            <td colspan="9">
                <dx:ASPxTextBox ID="txtRegion" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="80px" ClientInstanceName="txtRegion" MaxLength="50" >
                                  
                </dx:ASPxTextBox>
            </td>
        </tr>
    </table>
    <div style="display:none">
        <dx:ASPxTextBox ID="txtSectoralTemp" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="80px" ClientInstanceName="txtSectoralTemp" MaxLength="50">                     
         </dx:ASPxTextBox>
    </div>

    <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
           <%-- <ClientSideEvents CallbackComplete="SetCode" />--%>
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
            <ClientSideEvents  EndCallback="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
            <ClientSideEvents Init="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
           <%-- <ClientSideEvents CallbackComplete="MessageDelete" />--%>
        </dx:ASPxCallback>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddPeriodAdjustment.aspx.vb" Inherits="IAMI_EPIC2.AddPeriodAdjustment" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxImageZoom" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" >
        function MessageError(s, e) {
            if (s.cpMessage == "Data Saved Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                txtPeriodID.SetText(s.cpItemCode);
                setTimeout(function (){
                    window.location.href = 'AddPeriodAdjustment.aspx?ID=' + s.cpItemCode;
                }, 1000);
            }
            else if (s.cpMessage == "Data Updated Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                txtPeriodID.SetText(s.cpItemCode);
                setTimeout(function () {
                    window.location.href = 'AddPeriodAdjustment.aspx?ID=' + s.cpItemCode;
                }, 1000);
            }
            else if (s.cpMessage == "Data Clear Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                txtPeriodDescription.SetText('');
                cboPeriodType.SetText('');
                dtPeriodFrom.SetText('');
                dtPeriodTo.SetText('');
                dtPODateFrom.SetText('');
                dtPODateTo.SetText('');
                txtPeriodID.Focus();
            }
            else if (s.cpMessage == "Data Cancel Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Is Already Exist!") {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function MessageDelete(s, e) {
            if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                millisecondsToWait = 2000;
                setTimeout(function () {
                    var pathArray = window.location.pathname.split('/');
                    var url = window.location.origin + '/' + pathArray[1] + '/PeriodAdjustmentList.aspx'
                    if (pathArray[1] == "AddPeriodAdjustment.aspx") {
                        window.location.href = window.location.origin + '/PeriodAdjustmentList.aspx';
                    }
                    else {
                        window.location.href = url;
                    }
                }, millisecondsToWait);
            }
        }
    </script>
    <style type="text/css">
        .tr-height
        {
            height:30px;
        }
        .td-col-l
        {
            padding-left:5px;
            width:120px;
            
        }
        .td-col-m
        {
            width:10px;
        }
        
        .td-col-f
        {
            width:50px;
        }
         .div-hidden
        {
           display:none;
        }
        
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr class="tr-height">
            <td class="td-col-f">
                &nbsp;
            </td>
            <td class="td-col-l">
                &nbsp;
            </td>
            <td class="td-col-m"> 
                &nbsp;
            </td>
            <td > 
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-f">
                &nbsp;
            </td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel14" runat="server" Font-Names="Segoe UI" 
                    Font-Size="9pt" Text="Period ID">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"> 
                &nbsp;
            </td>
            <td > 
               <dx:ASPxTextBox ID="txtPeriodID" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    AutoCompleteType="Disabled" ReadOnly="true" BackColor="#CCCCCC"
                    ClientInstanceName="txtPeriodID" Width="100px" Height="25px" MaxLength="10">
                </dx:ASPxTextBox>                            
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-f">
                &nbsp;
            </td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" 
                    Font-Size="9pt" Text="Period Description">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"> 
                &nbsp;
            </td>
            <td > 
               <dx:ASPxTextBox ID="txtPeriodDescription" runat="server" Font-Names="Segoe UI" 
                    Font-Size="9pt" AutoCompleteType="Disabled" 
                    Width="280px" ClientInstanceName="txtPeriodDescription" Height="25px" 
                    MaxLength="200">
                </dx:ASPxTextBox>                           
            </td>
            <td >
                &nbsp;
            </td>
            <td >
                &nbsp;
            </td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-f">
                &nbsp;
            </td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Period Type">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"> 
                &nbsp;
            </td>
            <td> 
               <dx:aspxcombobox ID="cboPeriodType" runat="server" ClientInstanceName="cboPeriodType"
                    Width="100px" Font-Names="Segoe UI" 
                    DataSourceID="SqlDataSource1" TextField="Description"
                    ValueField="Code" TextFormatString="{0}" Font-Size="9pt" 
                    Theme="Office2010Black" DropDownStyle="DropDown" 
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                    Height="25px">             
                    <Columns>            
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="100px" />
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Visible="false" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px" >
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px" >
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:aspxcombobox>                                   
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-f">&nbsp;</td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Period From">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"> &nbsp;</td>
            <td> 
                <dx:aspxdateedit ID="dtPeriodFrom" runat="server" Theme="Office2010Black" 
                    Width="100px" ClientInstanceName="dtPeriodFrom"
                    EditFormatString="MMM-yy" DisplayFormatString="MMM-yy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                    EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                    </CalendarProperties>
                    <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                </dx:aspxdateedit>                       
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-m">
                &nbsp;
            </td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" 
                    Font-Size="9pt" Text="Period To">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"> 
                &nbsp;
            </td>
            <td> 
                <dx:aspxdateedit ID="dtPeriodTo" runat="server" Theme="Office2010Black" 
                    Width="100px" ClientInstanceName="dtPeriodTo"
                    EditFormatString="MMM-yy" DisplayFormatString="MMM-yy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                    EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                    </CalendarProperties>
                    <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                </dx:aspxdateedit>                       
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-f">
                &nbsp;
            </td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI"  Width="100px" Font-Size="9pt" Text="PO Date From">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"> 
                &nbsp;
            </td>
            <td> 
                <dx:aspxdateedit ID="dtPODateFrom" runat="server" Theme="Office2010Black" 
                    Width="100px" ClientInstanceName="dtPODateFrom"
                    EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                    EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                    </CalendarProperties>
                    <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                </dx:aspxdateedit>                       
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-f">
                &nbsp;
            </td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Font-Names="Segoe UI"  
                    Width="100px" Font-Size="9pt" Text="PO Date To">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"> 
                &nbsp;
            </td>
            <td> 
                <dx:aspxdateedit ID="dtPODateTo" runat="server" Theme="Office2010Black" 
                    Width="100px" ClientInstanceName="dtPODateTo"
                    EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                    EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                    </CalendarProperties>
                    <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                </dx:aspxdateedit>                       
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
         </tr>
         <tr class="tr-height">
            <td class="td-col-f">
                &nbsp;
             </td>
            <td class="td-col-l">
                &nbsp;
            </td>
            <td class="td-col-m"> 
                &nbsp;
            </td>
            <td > 
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
         </tr>
    </table>
<br/>
<br/>
    <tr style="height:25px">
        <td style="width:50px">&nbsp;</td>
        <td style="width:50px">&nbsp;</td>
        <td style="width:150px">&nbsp;</td>
        <td style="width:20px">&nbsp;</td>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <td style="width:500px"> 
            <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnBack" Theme="Default" >                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>
            &nbsp;&nbsp;
            <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnClear" Theme="Default" >                        
                <ClientSideEvents Click="function(s, e) {
                    var msg = confirm('Are you sure want to cancel this data ?');                
                    if (msg == false) {
                        e.processOnServer = false;
                        return;
                    }	
                }" />
                <Paddings Padding="2px" />
            </dx:ASPxButton>
            &nbsp;&nbsp;
            <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" AutoPostBack="False"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnDelete" Theme="Default" Enabled="False"  >                        
                <ClientSideEvents Click="function(s, e) {	                                                            
                    var msg = confirm('Are you sure want to delete this data ?');                
                    if (msg == false) {
                        e.processOnServer = false;
                        return;
                    }        
			        cbDelete.PerformCallback();           
                }" />
                <Paddings Padding="2px" />
            </dx:ASPxButton>
            &nbsp;&nbsp;
            <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnSubmit" Theme="Default" >                        
                <ClientSideEvents Click="function(s, e) {
	                if (cboPeriodType.GetText() == '') {
                            toastr.warning('Please Select Period Type!', 'Warning');
                            cboPeriodType.Focus();
                            toastr.options.closeButton = false;
                            toastr.options.debug = false;
                            toastr.options.newestOnTop = false;
                            toastr.options.progressBar = false;
                            toastr.options.preventDuplicates = true;
                            toastr.options.onclick = null;
		                    e.processOnServer = false;
                            return;
	                    }  

                    else if (txtPeriodDescription.GetText() == ''){    
                            toastr.warning('Please Input Period Description!', 'Warning');
                            txtPeriodDescription.Focus();
                            toastr.options.closeButton = false;
                            toastr.options.debug = false;
                            toastr.options.newestOnTop = false;
                            toastr.options.progressBar = false;
                            toastr.options.preventDuplicates = true;
                            toastr.options.onclick = null;
		                    e.processOnServer = false;
                            return;
                        }

                    else if (dtPeriodFrom.GetText() == ''){    
                            toastr.warning('Please Input Period From!', 'Warning');
                            dtPeriodFrom.Focus();
                            toastr.options.closeButton = false;
                            toastr.options.debug = false;
                            toastr.options.newestOnTop = false;
                            toastr.options.progressBar = false;
                            toastr.options.preventDuplicates = true;
                            toastr.options.onclick = null;
		                    e.processOnServer = false;
                            return;
                        }

                    else if (dtPeriodTo.GetText() == ''){    
                            toastr.warning('Please Input Period To!', 'Warning');
                            dtPeriodTo.Focus();
                            toastr.options.closeButton = false;
                            toastr.options.debug = false;
                            toastr.options.newestOnTop = false;
                            toastr.options.progressBar = false;
                            toastr.options.preventDuplicates = true;
                            toastr.options.onclick = null;
		                    e.processOnServer = false;
                            return;
                        }

                    else if (dtPODateFrom.GetText() == ''){    
                            toastr.warning('Please Input PO Date From!', 'Warning');
                            dtPODateFrom.Focus();
                            toastr.options.closeButton = false;
                            toastr.options.debug = false;
                            toastr.options.newestOnTop = false;
                            toastr.options.progressBar = false;
                            toastr.options.preventDuplicates = true;
                            toastr.options.onclick = null;
		                    e.processOnServer = false;
                            return;
                        }

                     else if (dtPODateTo.GetText() == ''){    
                            toastr.warning('Please Input PO Date To!', 'Warning');
                            dtPODateTo.Focus();
                            toastr.options.closeButton = false;
                            toastr.options.debug = false;
                            toastr.options.newestOnTop = false;
                            toastr.options.progressBar = false;
                            toastr.options.preventDuplicates = true;
                            toastr.options.onclick = null;
		                    e.processOnServer = false;
                            return;
                        }
                                    
                        var msg = confirm('Are you sure want to save this data ?');                
                        if (msg == false) {
                                e.processOnServer = false;
                                return;
                        }
                        cbSave.PerformCallback('Submit');      
                    }"  />
                <Paddings Padding="2px" />
            </dx:ASPxButton>
        </td>
	    <td>
            <br />
            <br />
            <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf"></dx:ASPxHiddenField>
        </td>
        <td colspan="6">              
            <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
                <ClientSideEvents Init="MessageError" />                                           
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                <ClientSideEvents Init="MessageError" />                                           
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
                <ClientSideEvents Init="MessageDelete" />                                           
            </dx:ASPxCallback>
        </td>
        <td>
            <dx:ASPxCallback ID="cb" runat="server" ClientInstanceName="cb">
                <ClientSideEvents 
                            
                    CallbackComplete="LoadCompleted"
                />
            </dx:ASPxCallback>
        </td>
        <td>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                SelectCommand="Select Rtrim(Par_Code) AS Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Period'"></asp:SqlDataSource>
            </td>
        </td>
    </tr>
</asp:Content>

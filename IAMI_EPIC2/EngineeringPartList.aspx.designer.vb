﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class EngineeringPartList

    '''<summary>
    '''ASPxLabel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel1 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboProject control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboProject As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''btnShowData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnShowData As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''btnDownload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDownload As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''btnUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpload As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''SqlDataSource1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSource1 As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''cbupload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbupload As Global.DevExpress.Web.ASPxCallback.ASPxCallback

    '''<summary>
    '''SqlDataSource2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSource2 As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''GridExporter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridExporter As Global.DevExpress.Web.ASPxGridView.Export.ASPxGridViewExporter

    '''<summary>
    '''Grid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Grid As Global.DevExpress.Web.ASPxGridView.ASPxGridView

    '''<summary>
    '''Master property.
    '''</summary>
    '''<remarks>
    '''Auto-generated property.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As IAMI_EPIC2.Site
        Get
            Return CType(MyBase.Master, IAMI_EPIC2.Site)
        End Get
    End Property
End Class

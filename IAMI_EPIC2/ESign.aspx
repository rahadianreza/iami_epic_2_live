﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ESign.aspx.vb" Inherits="IAMI_EPIC2.ESign" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxImageZoom" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="img/computer-41.ico" rel="SHORTCUT ICON" type="image/icon" />
	 <script src="Scripts/toastr.js"></script>
    <link rel="Stylesheet" type="text/css" href="content/toastr.css" />
    <link rel="Stylesheet" href="content/toastr.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css"/>
     <script src="js/bootstrap/bootstrap.min.js"></script>
     <script src="Scripts/jquery-1.6.3.min.js"></script>
    <style type="text/css">
        .messagealert {
            width: 100%;
            position: fixed;
             top:0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage(message, messagetype) {
            var cssclass;
            switch (messagetype) {
                case 'Success':
                    cssclass = 'alert-success'
                    break;
                case 'Error':
                    cssclass = 'alert-danger'
                    break;
                case 'Warning':
                    cssclass = 'alert-warning'
                    break;
                default:
                    cssclass = 'alert-info'
            }
            $('#alert_container').append('<div id="alert_div" style="margin:0,0.5%; -webkit-box-shadow: 3px 4px 6px #999;" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + messagetype + '!</strong> <span>' + message + '</span></div>');

            setTimeout(function () {
                $("#alert_div").fadeTo(2000, 500).slideUp(500, function () {
                    $("#alert_div").remove();
                });
            }, 5000); //5000=5 seconds
        }
    </script>		 
    <script type="text/javascript">

        function Upload() {

            cbSave.PerformCallback();
        }


         
        function Close() {
            window.close();
        }

        function Message(s, e) {
            if (s.cpMessage == "Data Updated Successfully!") {

                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + 'ESign.aspx' + s.cpPrm;
//                alert(url);               
                window.location.href = url;                
            }
        }

        function ShowPreview(input) {
            
            if (input.files && input.files[0]) {
                var ImageDir = new FileReader();
                ImageDir.onload = function (e) {
                    $('#imgSign').attr('src', e.target.result);
                }
                ImageDir.readAsDataURL(input.files[0]);
            }
			//*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}*,:before,:after{color:#000!important;text-shadow:none!important;background:transparent!important;-webkit-box-shadow:none!important;box-shadow:none!important}</style>

        }   
    </script>
   
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding: 5px 5px 5px 5px">   
        <table>
            <tr style="height:25px">
                <td style="width:50px">                            
                </td>            
                <td style="width:500px">                            
                    <dx:ASPxBinaryImage ID="imgSign" ClientInstanceName="imgSign" runat="server" IsPng="True" ImageAlign="Middle" >
                        <EmptyImage AlternateText="e Signing" 
                             ToolTip="Image" >
                        </EmptyImage>
                    </dx:ASPxBinaryImage>           
                </td>
                <td style="width:50px">                            
                    </td> 
            </tr>
            
            <tr style="height:25px">                
                <td style="width:50px"> &nbsp;</td>
                <td style="width:300px">                            
                    <asp:FileUpload ID="uploader1" runat="server" Font-Names="Segoe UI"
                        Font-Size="9pt" Height="20px" Width="270px" 
                        Accept=".png,.jpg,.jpeg,.gif" />
                </td>    
                <td style="width:50px"> &nbsp; 
    
                <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">                                        
					 <ClientSideEvents  EndCallback="Message" />																				 
                </dx:ASPxCallback>

                </td>        
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="message" runat="server" ></asp:Label>
                </td>
            </tr> 
            
            <tr style="height:25px">
                <td style="width:50px"> &nbsp; </td>
                <td style="width:300px">
                    <dx:ASPxLabel ID="ASPxLabel15" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="*Available image format : jpg, png, bmp" Font-Italic="True">
                    </dx:ASPxLabel>
                </td>
                <td style="width:50px"> &nbsp; </td>
            </tr> 

            <tr style="height:25px">                
                <td style="width:50px"> &nbsp;</td>
                <td style="width:300px"> 
               <dx:ASPxButton ID="btnSave" runat="server" Text="Save" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                        ClientInstanceName="btnSave" Theme="Default">                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>    
                <td style="width:50px"> &nbsp; </td>        
            </tr>
            <tr> 
                <td colspan="4">
                    <div class="messagealert" id="alert_container"></div>
                </td>
            </tr> 
        </table>                                 
        
    </div>
    </form>
</body>
</html>

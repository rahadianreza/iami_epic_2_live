﻿Imports DevExpress.XtraPrinting
Imports DevExpress.Web.ASPxGridView
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Transactions

Public Class Gate_III_SuplierSelection
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim pErr As String = ""

    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim grpcomodity As String = ""
    Dim partno As String = ""
    Dim py As String = ""
    Dim pty As String = ""
    Dim sDS As String = ""
    Dim pg As String = ""
    Dim supp As String = ""
    Dim appid As String = ""
#End Region

#Region "Initialization"

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            up_GridLoad("0")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")

        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        up_GridLoad("1")

        prj = Request.QueryString("projectid")
        grp = Request.QueryString("groupid")
        comm = Request.QueryString("commodity")
        grpcomodity = Request.QueryString("groupcomodity")
        partno = Request.QueryString("partno")
        pg = Request.QueryString("pagetype")
        appid = Request.QueryString("appid")

        If appid = "approve" Then
            sGlobal.getMenu("M030")
        Else
            sGlobal.getMenu("M010")
        End If

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If Not String.IsNullOrEmpty(prj) Then
                cboProject.Value = prj
                cboGroup.Value = grp
                cboCommodity.Value = comm
                cboGroupCommodity.Value = grpcomodity
                cboPartNo.Value = partno

                QCDMR()
                up_GridLoad("0")
                Detail()

                cboProject.Enabled = False
                cboGroup.Enabled = False
                cboCommodity.Enabled = False
                cboGroupCommodity.Enabled = False
                cboPartNo.Enabled = False

                If clsQuotationSupplierDB.checkingApproveGate3(prj, grp, comm, grpcomodity, partno, pUser) <> 0 Then
                    btnApprove.Visible = False
                Else
                    btnApprove.Visible = True
                End If
            Else
                btnApprove.Visible = False
            End If

            'QCDMR()

            If clsQuotationSupplierDB.GateIIISupplierStatusApproval(prj, grp, comm, grpcomodity, partno) > 0 Then
                btnApprove.ClientEnabled = False
            Else
                btnApprove.ClientEnabled = True
            End If



            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboPartNo, cboProject.Value, cboGroup.Value, cboCommodity.Value, "R", statusAdmin, pUser)
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_Excel()
        up_GridLoad("1")

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ItemListSourcing" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub up_FillComboGroupCommodityPIC(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _projid As String, _grpid As String, _comm As String, _type As String, _
                           pAdminStatus As String, pUserID As String, _
                           Optional ByRef pGroup As String = "", _
                           Optional ByRef pParent As String = "", _
                           Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsGateICheckSheetDB.FillComboProject("9", "", _projid, _grpid, _comm, _type, pUserID, statusAdmin, pmsg)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsQuotationSupplierDB.getSupplierSelectionGrid(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboPartNo.Value, pUser, pErr)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()

            If Not Ses Is Nothing Then
                If Grid.Columns.Count > 1 Then
                    For v As Integer = 0 To Ses.Tables(0).Columns.Count - 1
                        Grid.Columns.Remove(Grid.Columns(Ses.Tables(0).Columns(v).ColumnName))
                    Next
                End If

                For y As Integer = 0 To Ses.Tables(0).Columns.Count - 1
                    'For i As Integer = 0 To Ses.Tables(0).Rows.Count - 1
                    'Next
                    Dim col As GridViewDataTextColumn = New GridViewDataTextColumn()
                    col.Caption = Ses.Tables(0).Columns(y).ToString()
                    col.FieldName = Ses.Tables(0).Columns(y).ColumnName
                    If Ses.Tables(0).Columns(y).ColumnName <> "flag" Then
                        If Ses.Tables(0).Columns(y).ColumnName = "Descriptions" Then
                            col.EditFormSettings.Caption = ""
                            col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                        Else
                            If y >= 3 Then
                                col.EditFormSettings.Caption = Ses.Tables(0).Columns(y).ColumnName
                                col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True
                            Else
                                col.EditFormSettings.Caption = ""
                                col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                            End If
                           
                        End If


                        col.Settings.AutoFilterCondition = AutoFilterCondition.Contains
                    End If
                    Grid.Columns.Add(col)
                Next

                Grid.Styles.Header.HorizontalAlign = HorizontalAlign.Center
                Grid.Columns(1).Width = 200
                Grid.Columns(2).Visible = False

                For i As Integer = 3 To Grid.Columns.Count - 1
                    Grid.Columns(i).Width = 300
                Next
            End If
        End If
    End Sub
#End Region

#Region "Event Control"
    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
            QCDMR()
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize

    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.VisibleIndex < 0) Then
            Exit Sub
        End If
        If Grid.GetRowValues(e.VisibleIndex, "flag") = "0" Or pg = "approve" Or clsQuotationSupplierDB.checkingApproveGate3(prj, grp, comm, grpcomodity, partno, pUser) <> 0 Then
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                e.Visible = False
            End If
        End If

        If clsQuotationSupplierDB.checkingApproveGate3(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, cboPartNo.Value, pUser) <> 0 Then
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                e.Visible = False
            End If
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else
            up_GridLoad("1")
        End If
    End Sub

    Public Function checkingCbo() As Boolean
        If String.IsNullOrEmpty(cboProject.Value) = True Then
            cbSave.JSProperties("cpMessage") = "Project Name cannot be empty!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return False
        ElseIf String.IsNullOrEmpty(cboGroup.Value) = True Then
            cbSave.JSProperties("cpMessage") = "Group ID cannot be empty!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return False
        ElseIf String.IsNullOrEmpty(cboCommodity.Value) = True Then
            cbSave.JSProperties("cpMessage") = "Commodity cannot be empty!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return False
        Else
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return True
        End If
    End Function

    Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProject As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroup, pProject, "", "", "1", statusAdmin, pUser, "")
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboPartNo_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboPartNo.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboPartNo, cboProject.Value, cboGroup.Value, cboCommodity.Value, "R", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboPartNo, cboProject.Value, cboGroup.Value, cboCommodity.Value, "R", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboGroupCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        End If
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True

        Dim Ses As DataSet
        Ses = clsQuotationSupplierDB.getSupplierSelectionGrid(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboPartNo.Value, pUser, pErr)
        Using Scope As New TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10))
            For y As Integer = 2 To Ses.Tables(0).Columns.Count - 1
                clsQuotationSupplierDB.InsertSupplierSelection(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboPartNo.Value, Ses.Tables(0).Columns(y).ColumnName, e.Keys("Descriptions").ToString.Substring(0, 1), e.NewValues(y - 1), pUser, cboGroupCommodity.Value, pErr)
            Next

            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
                Scope.Complete()
            End If
        End Using
    End Sub

    Private Sub cbMessage_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbMessage.Callback
        'nothing
    End Sub

    Private Sub cbComplete_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbComplete.Callback
        'nothing
    End Sub

    Private Sub cbStatus_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbStatus.Callback
        'nothing
    End Sub

    Public Sub QCDMR()
        Dim dsQCDMR As DataSet = clsQuotationSupplierDB.QCDMR(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboPartNo.Value, pUser, pErr)

        If Not dsQCDMR Is Nothing Then

            GridQCDMR.DataSource = dsQCDMR
            GridQCDMR.DataBind()

            If GridQCDMR.Columns.Count > 1 Then
                For v As Integer = 0 To dsQCDMR.Tables(0).Columns.Count - 1
                    GridQCDMR.Columns.Remove(GridQCDMR.Columns(dsQCDMR.Tables(0).Columns(v).ColumnName))
                Next
            End If

            For y As Integer = 0 To dsQCDMR.Tables(0).Columns.Count - 1
                'For i As Integer = 0 To Ses.Tables(0).Rows.Count - 1
                'Next
                Dim col As GridViewDataTextColumn = New GridViewDataTextColumn()
                col.Caption = dsQCDMR.Tables(0).Columns(y).ToString()
                col.FieldName = dsQCDMR.Tables(0).Columns(y).ColumnName
                If dsQCDMR.Tables(0).Columns(y).ColumnName <> "flag" Then
                    If dsQCDMR.Tables(0).Columns(y).ColumnName = "QCDMR Analysis" Then
                        col.EditFormSettings.Caption = ""
                    Else
                        col.EditFormSettings.Caption = dsQCDMR.Tables(0).Columns(y).ColumnName
                    End If

                    col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True
                    col.Settings.AutoFilterCondition = AutoFilterCondition.Contains
                End If
                GridQCDMR.Columns.Add(col)
            Next

            GridQCDMR.Styles.Header.HorizontalAlign = HorizontalAlign.Center
            GridQCDMR.Columns(0).Width = 200

            For i As Integer = 1 To GridQCDMR.Columns.Count - 1
                GridQCDMR.Columns(i).Width = 200

            Next

            'For i As Integer = 1 To Grid.Columns.Count - 1
            '    GridQCDMR.Columns(i).Width = 300
            'Next
        End If
    End Sub

    Public Sub Detail()
        Dim dsDetail As DataSet = clsQuotationSupplierDB.Detail(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboPartNo.Value, pUser, pErr)
        GridDetail.DataSource = dsDetail
        GridDetail.DataBind()
    End Sub
#End Region

    Protected Sub btnShowData_Click(sender As Object, e As EventArgs) Handles btnShowData.Click
        QCDMR()
        up_GridLoad("0")
        Detail()

        up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
        up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
        up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        up_FillComboGroupCommodityPIC(cboPartNo, cboProject.Value, cboGroup.Value, cboCommodity.Value, "R", statusAdmin, pUser)
    End Sub

    Private Sub GridDetail_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridDetail.CellEditorInitialize
        If e.Column.FieldName = "UserName" Or e.Column.FieldName = "Project_Name" Or e.Column.FieldName = "Part_No" Or e.Column.FieldName = "Part_Name" _
            Or e.Column.FieldName = "PriceGuideLine" Or e.Column.FieldName = "Quotation" Or e.Column.FieldName = "CostPercentage" Or e.Column.FieldName = "IDR" Then

            e.Editor.Enabled = False
        End If
    End Sub

    Private Sub GridDetail_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles GridDetail.CommandButtonInitialize
        If (e.VisibleIndex < 0) Then
            Exit Sub
        End If
        'If GridDetail.GetRowValues(e.VisibleIndex, "RECYN").ToString <> "" Then
        If pg = "approve" Or clsQuotationSupplierDB.checkingApproveGate3(prj, grp, comm, grpcomodity, partno, pUser) <> 0 Then
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                e.Visible = False
            End If
        End If

        If clsQuotationSupplierDB.checkingApproveGate3(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, cboPartNo.Value, pUser) <> 0 Then
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                e.Visible = False
            End If
        End If
    End Sub

    Private Sub GridDetail_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridDetail.RowUpdating
        e.Cancel = True
        clsQuotationSupplierDB.InsertSelectedSupplier(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, e.NewValues("RECYN"), e.Keys("Vendor_Code"), cboPartNo.Value, pErr)

        If pErr = "" Then
            Detail()
            GridDetail.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
        End If
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        clsQuotationSupplierDB.InsertApproveGate3(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, cboPartNo.Value, appid, pErr)

        If pErr = "" Then
            Detail()
            GridDetail.CancelEdit()
            cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboPartNo, cboProject.Value, cboGroup.Value, cboCommodity.Value, "R", statusAdmin, pUser)
            up_GridLoad("")
            btnApprove.Visible = False
        End If
    End Sub
End Class
﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data

Public Class Material_PriceDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
   
    Dim strMaterialCode As String = ""
    Dim strPriceType As String = ""
    Dim strSupplier As String = ""
    Dim strPeriod As String = ""
#End Region
    Public Sub FillCombo(pPriceType)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsMst_MaterialDB.GetDataCombo("MaterialPriceType", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPriceType.Items.Add(Trim(ds.Tables(0).Rows(i)("MaterialPriceType") & ""))
            Next
        End If

        Dim dsSupp As DataSet
        dsSupp = clsMst_MaterialDB.GetDataCombo("Supplier", pPriceType, pErr)
        If pErr = "" Then
            cboSupplier.DataSource = dsSupp
            cboSupplier.DataBind()
        End If

        Dim dsMC As DataSet
        dsMC = clsMst_MaterialDB.GetDataCombo("MasterMaterial", pErr)
        If pErr = "" Then
            For i = 0 To dsMC.Tables(0).Rows.Count - 1
                cboMaterialCode.Items.Add(Trim(dsMC.Tables(0).Rows(i)("Material_Code") & ""))
            Next
        End If

        Dim dscurr As DataSet
        dscurr = clsMst_MaterialDB.GetDataCombo("Currency", pErr)
        If pErr = "" Then
            For i = 0 To dscurr.Tables(0).Rows.Count - 1
                cboCurrency.Items.Add(Trim(dscurr.Tables(0).Rows(i)("Currency") & ""))
            Next
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A170")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        ' statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        Dim str As String = Request.QueryString("ID")
        Dim script As String = ""
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If str <> "" Then
                btnSubmit.Text = "Update"

                script = "cboMaterialCode.SetEnabled(false);" & vbCrLf & _
                         "cboPriceType.SetEnabled(false);" & vbCrLf & _
                         "cboSupplier.SetEnabled(false);" & vbCrLf & _
                         "dtPeriod.SetEnabled(false);"


                ScriptManager.RegisterStartupScript(cboMaterialCode, cboMaterialCode.GetType(), "cboMaterialCode", script, True)
                ScriptManager.RegisterStartupScript(btnClear, btnClear.GetType(), "btnClear", "btnClear.SetEnabled(false);", True)

                strMaterialCode = str.Split("|")(0)
                strPriceType = str.Split("|")(1)
                strSupplier = str.Split("|")(2)
                strPeriod = str.Split("|")(3)

                'untuk fill combobox dari database
                FillCombo(strPriceType)

                Dim ds As DataSet = clsMst_MaterialDB.getDetailMaterial_Price(strMaterialCode, strPriceType, strSupplier, strPeriod, pUser)
                If ds.Tables(0).Rows.Count > 0 Then
                    cboMaterialCode.SelectedIndex = cboMaterialCode.Items.IndexOf(cboMaterialCode.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Material_Code") & "")))
                    txtMaterialType.Text = ds.Tables(0).Rows(0)("MaterialTypeDesc").ToString
                    cboPriceType.SelectedIndex = cboPriceType.Items.IndexOf(cboPriceType.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Price_Type") & "")))
                    cboSupplier.SelectedIndex = cboSupplier.Items.IndexOf(cboSupplier.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Supplier_Code") & "")))
                    dtPeriod.Value = CDate(ds.Tables(0).Rows(0)("Period").ToString())
                    cboCurrency.SelectedIndex = cboCurrency.Items.IndexOf(cboCurrency.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Currency_Code") & "")))
                    txtPrice.Text = ds.Tables(0).Rows(0)("Price").ToString()

                End If

            Else
                script = "cboMaterialCode.SetEnabled(true);" & vbCrLf & _
                         "cboPriceType.SetEnabled(true);" & vbCrLf & _
                         "cboSupplier.SetEnabled(true);" & vbCrLf & _
                         "dtPeriod.SetEnabled(true);"

                ScriptManager.RegisterStartupScript(cboMaterialCode, cboMaterialCode.GetType(), "cboMaterialCode", script, True)
                dtPeriod.Value = Now

            End If
        End If

    End Sub

    Private Sub up_FillComboSupplier(pPriceType As String, Optional ByRef pErr As String = "")
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsAll_MaterialDB.GetDataCombo("Supplier", pPriceType, pmsg)
        If pmsg = "" Then
            cboSupplier.DataSource = ds
            cboSupplier.DataBind()
        End If
    End Sub
    Private Sub cboSupplier_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSupplier.Callback
        Dim pMaterialType As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        up_FillComboSupplier(pMaterialType, errmsg)

    End Sub

    Private Sub cboMaterialCode_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboMaterialCode.Callback
        Dim errmsg As String = ""
        Dim ds As New DataSet
        Dim pMaterialCd As String = Split(e.Parameter, "|")(0)
        ds = clsMst_MaterialDB.GetDataCombo("BindMaterialCode", pMaterialCd, errmsg)
        If errmsg = "" Then
            txtMaterialType.Text = ds.Tables(0).Rows(0)("Material_Type").ToString
            cboMaterialCode.JSProperties("cpGetValue1") = ds.Tables(0).Rows(0)("Material_Name").ToString()
            cboMaterialCode.JSProperties("cpGetValue2") = ds.Tables(0).Rows(0)("MaterialTypeDesc").ToString()

        End If

    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Material_Price.aspx")
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"

    End Sub

    Protected Sub cbSave_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSave.Callback
        Dim pErr As String = ""
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim vMaterialCode As String = cboMaterialCode.Value
        Dim vPriceType As String = cboPriceType.Value
        Dim vSupplier As String = cboSupplier.Value
        Dim vPeriod As String = Format(dtPeriod.Value, "yyyy-MM") & "-01"

        Dim vCurrency As String = cboCurrency.Value
        Dim vPrice As Decimal = txtPrice.Text

        If pFunction = "Save" Then
            'If IsNothing(Request.QueryString("ID")) Then

            clsMst_MaterialDB.InsertDataMaterial_Price(vMaterialCode, vPriceType, vSupplier, vPeriod, vCurrency, vPrice, pUser, pErr)
            If pErr = "" Then
                cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
                cbSave.JSProperties("cpButtonText") = "Update"
            Else
                cbSave.JSProperties("cpMessage") = pErr

            End If
        Else
            If IsNothing(Request.QueryString("ID")) Then
                clsMst_MaterialDB.UpdateDataMaterial_Price(vMaterialCode, vPriceType, vSupplier, vPeriod, vCurrency, vPrice, pUser, pErr)
                If pErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                    cbSave.JSProperties("cpButtonText") = "Update"
                End If
            Else
                Dim str As String = Request.QueryString("ID")
                strMaterialCode = str.Split("|")(0)
                strPriceType = str.Split("|")(1)
                strSupplier = str.Split("|")(2)
                strPeriod = str.Split("|")(3)

                clsMst_MaterialDB.UpdateDataMaterial_Price(strMaterialCode, strPriceType, strSupplier, strPeriod, vCurrency, vPrice, pUser, pErr)
                If pErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                    cbSave.JSProperties("cpButtonText") = "Update"
                End If


            End If


        End If

    End Sub
End Class
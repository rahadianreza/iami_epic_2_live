﻿Imports DevExpress.XtraCharts
Imports System.Drawing
Imports DevExpress.XtraCharts.Web
Imports DevExpress.Web.ASPxCallbackPanel
Imports System.Data.SqlClient

Public Class Material_Chart
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A170")
        Master.SiteTitle = "Material Chart"
        pUser = Session("user")

        If Not Page.IsCallback And Not Page.IsPostBack Then
            Dim dfrom As Date
            dfrom = Year(Now) & "-01-01"
            Period_From.Value = dfrom
            Period_To.Value = Now


        End If
    End Sub

    Private Sub cboGroupItem_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupItem.Callback
        Dim pMaterialType As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsMst_MaterialDB.GetDataCombo("GroupItem", pMaterialType, pmsg)
        If pmsg = "" Then
            cboGroupItem.DataSource = ds
            cboGroupItem.DataBind()

        End If

    End Sub

    Private Sub cboGroupItemTo_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupItemTo.Callback
        Dim pMaterialType As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsMst_MaterialDB.GetDataCombo("GroupItem", pMaterialType, pmsg)
        If pmsg = "" Then
            cboGroupItemTo.DataSource = ds
            cboGroupItemTo.DataBind()

        End If

    End Sub

    Protected Sub ASPxCallbackPanel1_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel1.Callback
        Dim wbc As New WebChartControl()
        'Dim ds As New DataSet
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pMaterialType As String = Split(e.Parameter, "|")(1)
        Dim pCurrency As String = Split(e.Parameter, "|")(2)
        Dim pSupplier As String = cboSupplier.Value
        Dim PeriodFrom As Date = Period_From.Value
        Dim PeriodTo As Date = Period_To.Value
        Dim pGroupItem As String = cboGroupItem.Value
        Dim pGroupItem2 As String = cboGroupItemTo.Value


        If pFunction = "show" Then
            Dim pErr As String = ""
            Dim ds As DataSet = CreateData(PeriodFrom, PeriodTo, pSupplier, pMaterialType, pGroupItem, pGroupItem2, pCurrency, pErr)

            If pErr <> "" Then
                Exit Sub
            End If

            If ds.Tables(0).Rows.Count = 0 Then
                ASPxCallbackPanel1.JSProperties("cpMessage") = "There's no data to show!"
                Exit Sub
            Else
                ASPxCallbackPanel1.JSProperties("cpMessage") = "OK"
            End If

            wbc.DataSource = ds
            wbc.EmptyChartText.Text = "NO DATA AVAILABLE ON CHART"
            ' wbc.EmptyChartText.Font("Tahoma, 18pt")

            Dim chartTitle1 As New ChartTitle()
            ' Define the text for the titles. 
            'Dim CurrencyCvt As String = cboCurrencyCode.Text & " / IDR"
            chartTitle1.Text = ds.Tables(0).Rows(0)("ChartTitle").ToString
            chartTitle1.Alignment = StringAlignment.Near
            chartTitle1.Dock = ChartTitleDockStyle.Top
            chartTitle1.Antialiasing = True
            chartTitle1.Font = New Font("Segoe UI", 16, FontStyle.Bold)
            wbc.Titles.Add(chartTitle1)

            If pMaterialType = "RUBBER" Or pMaterialType = "STEEL" Then
                'diagram swift plot
                Dim swiftPlotSeries As Series = New Series(ds.Tables(0).Rows(0)("Group1").ToString, ViewType.SwiftPlot)
                CType(swiftPlotSeries.View, SwiftPlotSeriesView).Color = Color.Orange
                Dim swiftPlotSeries2 As Series = New Series(ds.Tables(0).Rows(0)("Group2").ToString, ViewType.SwiftPlot)
                CType(swiftPlotSeries2.View, SwiftPlotSeriesView).Color = Color.Purple
                Dim swiftPlotSeries3 As Series = New Series(ds.Tables(0).Rows(0)("Group3").ToString, ViewType.SwiftPlot)
                CType(swiftPlotSeries3.View, SwiftPlotSeriesView).Color = Color.Green
                Dim swiftPlotSeries4 As Series = New Series(ds.Tables(0).Rows(0)("Group4").ToString, ViewType.SwiftPlot)
                CType(swiftPlotSeries4.View, SwiftPlotSeriesView).Color = Color.Brown
                Dim swiftPlotSeries5 As Series = New Series(ds.Tables(0).Rows(0)("Group5").ToString, ViewType.SwiftPlot)
                CType(swiftPlotSeries5.View, SwiftPlotSeriesView).Color = Color.Brown

                If pGroupItem2 <> "" Then
                    wbc.Series.AddRange(New Series() {swiftPlotSeries, swiftPlotSeries2, swiftPlotSeries3, swiftPlotSeries4, swiftPlotSeries5})
                    swiftPlotSeries.ArgumentDataMember = "MonthYear"
                    swiftPlotSeries.ValueDataMembers(0) = "Price1"
                    swiftPlotSeries2.ArgumentDataMember = "MonthYear"
                    swiftPlotSeries2.ValueDataMembers(0) = "Price2"
                    swiftPlotSeries3.ArgumentDataMember = "MonthYear"
                    swiftPlotSeries3.ValueDataMembers(0) = "Price3"
                    swiftPlotSeries4.ArgumentDataMember = "MonthYear"
                    swiftPlotSeries4.ValueDataMembers(0) = "Price4"
                    swiftPlotSeries5.ArgumentDataMember = "MonthYear"
                    swiftPlotSeries5.ValueDataMembers(0) = "LowestPrice"
                Else
                    wbc.Series.AddRange(New Series() {swiftPlotSeries, swiftPlotSeries2, swiftPlotSeries5})
                    swiftPlotSeries.ArgumentDataMember = "MonthYear"
                    swiftPlotSeries.ValueDataMembers(0) = "Price1"
                    swiftPlotSeries2.ArgumentDataMember = "MonthYear"
                    swiftPlotSeries2.ValueDataMembers(0) = "Price2"
                    swiftPlotSeries5.ArgumentDataMember = "MonthYear"
                    swiftPlotSeries5.ValueDataMembers(0) = "LowestPrice"
                End If

                Dim spd As SwiftPlotDiagram = New SwiftPlotDiagram()
                CType(spd.AxisX, SwiftPlotDiagramAxisX).Label.TextColor = Color.Yellow
                CType(spd.AxisX, SwiftPlotDiagramAxisX).Label.Angle = 270


                'wbc.AppearanceName = "Dark"
                wbc.CrosshairEnabled = DevExpress.Utils.DefaultBoolean.True
                'position legend 
                wbc.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Center
                wbc.Legend.AlignmentVertical = LegendAlignmentVertical.TopOutside
                wbc.Legend.Direction = LegendDirection.LeftToRight
                'CType(WebChartControl1.Diagram, XYDiagram)


                wbc.DataBind()
            Else
                'diagram swift plot
                Dim swiftPlotSeries As Series = New Series(ds.Tables(0).Rows(0)("Group_ItemDesc").ToString, ViewType.SwiftPlot)
                CType(swiftPlotSeries.View, SwiftPlotSeriesView).Color = Color.Red
                Dim swiftPlotSeries2 As Series = New Series(ds.Tables(0).Rows(0)("Legend_Applied2").ToString, ViewType.SwiftPlot)
                CType(swiftPlotSeries2.View, SwiftPlotSeriesView).Color = Color.Purple
                Dim swiftPlotSeries3 As Series = New Series(ds.Tables(0).Rows(0)("Legend_Applied3").ToString, ViewType.SwiftPlot)
                CType(swiftPlotSeries3.View, SwiftPlotSeriesView).Color = Color.Green
                Dim swiftPlotSeries4 As Series = New Series(ds.Tables(0).Rows(0)("Legend_Applied4").ToString, ViewType.SwiftPlot)
                CType(swiftPlotSeries4.View, SwiftPlotSeriesView).Color = Color.Brown
                wbc.Series.AddRange(New Series() {swiftPlotSeries, swiftPlotSeries2, swiftPlotSeries3, swiftPlotSeries4})

                swiftPlotSeries.ArgumentDataMember = "MonthYear"
                swiftPlotSeries.ValueDataMembers(0) = "AvgPriceMonth"
                swiftPlotSeries2.ArgumentDataMember = "MonthYear"
                swiftPlotSeries2.ValueDataMembers(0) = "Forecast"
                swiftPlotSeries3.ArgumentDataMember = "MonthYear"
                swiftPlotSeries3.ValueDataMembers(0) = "AvgPrice6Month"
                swiftPlotSeries4.ArgumentDataMember = "MonthYear"
                swiftPlotSeries4.ValueDataMembers(0) = "LowestPrice"

                'wbc.AppearanceName = "Dark"
                wbc.CrosshairEnabled = DevExpress.Utils.DefaultBoolean.True
                'position legend 
                wbc.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Center
                wbc.Legend.AlignmentVertical = LegendAlignmentVertical.TopOutside
                wbc.Legend.Direction = LegendDirection.LeftToRight
                wbc.DataBind()
            End If

            
            'wbc.Width = ASPxCallbackPanel1.Width.Values
            'wbc.Height = ASPxCallbackPanel1.Height.Value

            wbc.Width = (TryCast(sender, ASPxCallbackPanel)).Width
            wbc.Height = (TryCast(sender, ASPxCallbackPanel)).Height

            TryCast(sender, ASPxCallbackPanel).Controls.Add(wbc)
        End If
    End Sub

    Private Function CreateData(pPeriodFrom As Date, pPeriodTo As Date, pSupplier As String, _
                                pMaterialType As String, pGroupItem As String, pGroupItemTo As String, _
                                pCurrency As String, Optional ByRef MsgError As String = "") As DataSet
        Dim ds As New DataSet
        'Dim pErr As String = ""
        Dim PeriodFrom, PeriodTo
        Try
            PeriodFrom = Format(pPeriodFrom, "yyyy-MM-dd")
            PeriodTo = Format(pPeriodTo, "yyyy-MM-dd")

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Chart", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@PeriodFrom", PeriodFrom)
                cmd.Parameters.AddWithValue("@PeriodTo", PeriodTo)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplier)
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@GroupItem", pGroupItem)
                cmd.Parameters.AddWithValue("@GroupItemTo", pGroupItemTo)
                cmd.Parameters.AddWithValue("@CurrencyCode", pCurrency)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                Return ds
            End Using

        Catch ex As Exception
            MsgError = ex.Message
            Return Nothing
        End Try
    End Function

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Material_Price.aspx")
    End Sub
End Class
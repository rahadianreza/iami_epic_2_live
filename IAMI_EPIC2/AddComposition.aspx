﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddComposition.aspx.vb" Inherits="IAMI_EPIC2.AddComposition" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxImageZoom" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<script type="text/javascript" >
   
    function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtSequence.SetText(s.cpItemCode);
            setTimeout(function (){
                window.location.href = 'AddComposition.aspx?ID=' + s.cpItemCode;
            }, 1000);

        }
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtSequence.SetText(s.cpItemCode);
            setTimeout(function () {
                window.location.href = 'AddComposition.aspx?ID=' + s.cpItemCode;
            }, 1000);


        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtSequence.SetText('');
            txtParentItemQty.SetText('0');
            cboParentItem.SetText('');
            cboParentItemUOM.SetText('');
            txtSequence.Focus();

        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }


    function MessageDelete(s, e) {
        
        if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/ItemList.aspx";
            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/CompositionList.aspx'
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                //alert(window.location.host);
                if (pathArray[1] == "AddComposition.aspx") {
                    window.location.href = window.location.origin + '/CompositionList.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }

   
function OnBatchEditStartEditing(s, e) {
        currentColumnName = e.focusedColumn.fieldName;
        
       
    }

    function InitGridAtc(s, e) {
       
    }

    function LoadCompleted(s, e) {
        
        
       
    }

    function OnFileUploadStart(s, e) {
        btnAddFile.SetEnabled(false);
    }

    function OnTextChanged(s, e) {
      
    }

    function OnFileUploadComplete(s, e) {
       
      
       
        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }

        window.setTimeout(function () {
            delete s.cpType;
        }, 10);
    }
    function AddFile(){
       
    }

    function OnGetSelectedFieldValues(selectedValues) {
        //VALIDATE WHEN THERE IS NO SELECTED DATA
        if (selectedValues.length == 0) {
            toastr.warning('There is no selected data to delete!', 'Warning');
            return;
        }

        //GET FILE NAME
        var sValues;
        var result;
        result = "";

        for (i = 0; i < selectedValues.length; i++) {
            sValues = "";
            for (j = 0; j < selectedValues[i].length; j++) {
                sValues = sValues + selectedValues[i][j];
            }
            result = result + sValues + "|";
            //alert(result);
        }

        //CONFIRMATION
		//result = result.substring(0, result.length - 1);
		var C = confirm("Are you sure want to delete selected attachment (" + result.substring(0, result.length - 1) + ") ?");
        if (C == false) {
            return;
        }

        //KEEP FILE NAME INTO HIDDENFIELD
        hf.Set("DeleteAttachment", result);

        //EXECUTE TO DATABASE AFTER DELAY 300ms
        window.setTimeout(function () {
            
            cb.PerformCallback("DeleteAttachment");
        }, 300);
    }

    function DeleteAttachment() {
       
        //cbExist.PerformCallback('IsSubmitted');
        window.setTimeout(function () {
         
            if (btnSubmit.GetEnabled() == true) {
                hf.Set("DeleteAttachment", "");
                GridAtc.GetSelectedFieldValues("FileName", OnGetSelectedFieldValues);
            }

        }, 500);
    }
	

    if (s.cpActionAfter == "DeleteAtc") {
        GridAtc.PerformCallback();
    }

    var keyValue;
    function OnMoreInfoClick(element, key) {
        callbackPanel.SetContentHtml("");
        popup.ShowAtElement(element);
        keyValue = key;
    }

    function popup_Shown(s, e) {
        callbackPanel.PerformCallback(keyValue);
    }
</script>
    <style type="text/css">
        .tr-height
        {
            height:30px;
        }
        .td-col-l
        {
            padding-left:5px;
            width:120px;
            
        }
        .td-col-m
        {
            width:10px;
        }
        
        .td-col-f
        {
            width:50px;
        }
         .div-hidden
        {
           display:none;
        }
        
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
 <tr class="tr-height">
        <td class="td-col-f">&nbsp;</td>
        <%--<td style="width:50px">&nbsp;</td>--%>
        <td class="td-col-l">
            &nbsp;</td>
        <td class="td-col-m"> &nbsp;</td>
        <td > 
         <div class="div-hidden"> 
            <dx:ASPxTextBox ID="txtSequence" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Width="170px" ClientInstanceName="txtSequence" MaxLength="20"  ReadOnly="true" BackColor="#CCCCCC">
               
            </dx:ASPxTextBox>
            </div>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
 </tr>
 <tr class="tr-height">
        <td class="td-col-f">&nbsp;</td>
       <%-- <td style="width:50px">&nbsp;</td>--%>
        <td class="td-col-l">
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Parent Item Code">
            </dx:ASPxLabel>
        </td>
        <td class="td-col-m"> &nbsp;</td>
        <td> 
           <dx:aspxcombobox ID="cboParentItem" runat="server" ClientInstanceName="cboParentItem"
                            Width="150px" Font-Names="Segoe UI" 
                            DataSourceID="SqlDataSource1" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px"> 
                            <ClientSideEvents SelectedIndexChanged="ParentItemCode" />           
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                             </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:aspxcombobox>
                                                
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
 </tr>
 
 <tr class="tr-height">
        <td class="td-col-f">&nbsp;</td>
        <td class="td-col-l">
            <dx:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Parent Item Qty">
            </dx:ASPxLabel>
        </td>
        <td class="td-col-m"> &nbsp;</td>
        <td> 
        <dx:ASPxTextBox ID="txtParentItemQty" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                Width="130px" ClientInstanceName="txtParentItemQty" MaxLength="15" 
                >            
            <MaskSettings IncludeLiterals="DecimalSymbol" Mask="<0..99999g>" />            
            </dx:ASPxTextBox>
          
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
 </tr>
 
 <tr class="tr-height">
        <td class="td-col-f">&nbsp;</td>
       <%-- <td style="width:50px">&nbsp;</td>--%>
        <td class="td-col-l">
            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Parent Item UOM">
            </dx:ASPxLabel>
        </td>
        <td class="td-col-m"> &nbsp;</td>
        <td> 
            <%--                    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="ASPxCallback1">
                    <ClientSideEvents EndCallback="function (s, e) {
                        if (txtCardNo.GetEnabled(true)) {
                            txtRefundID.SetText(s.cpRefundID);
                        } else {
                            txtRefundID.SetText('');
                        }
                    }" CallbackComplete="OnEndCallback" />
                    </dx:ASPxCallback>--%>
           <dx:aspxcombobox ID="cboParentItemUOM" runat="server" ClientInstanceName="cboParentItemUOM"
                            Width="150px" Font-Names="Segoe UI" 
                            DataSourceID="SqlDataSource2" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">   
                            <ClientSideEvents SelectedIndexChanged="ParentItemUOM" />            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                             </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:aspxcombobox>
                                                
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
 </tr>
 
 </table>
 <br/>

<br />
 <tr style="height:25px;" >					   
  <td style="width:50px">&nbsp;</td>
        <td style="width:50px">&nbsp;</td>
        <td style="width:150px">
            &nbsp;</td>
        <td style="width:20px"> &nbsp;</td>
        <td style="width:50px"> 
       
        <table style="display:none">
        <tr style="height:25px">
        <td style="width:300px">                            
        <dx:ASPxBinaryImage ID="imgCover1" ClientInstanceName="ClientBinaryImage" Width="250" Height="250" ShowLoadingImage="true" runat="server">     
        </dx:ASPxBinaryImage>           
        </td>
        <td style="width:50px"> &nbsp; </td>
        <td style="width:200px">
           <dx:ASPxBinaryImage ID="imgCover2" ClientInstanceName="ClientBinaryImage" Width="250" Height="250" ShowLoadingImage="true" runat="server">     
        </dx:ASPxBinaryImage>        
        </td>
        <td style="width:50px"> &nbsp; </td>
        <td style="width:200px">
              <dx:ASPxBinaryImage ID="imgCover3" ClientInstanceName="ClientBinaryImage" Width="250" Height="250" ShowLoadingImage="true" runat="server">     
        </dx:ASPxBinaryImage>   
        </td>
		<td style="width:300px">                            
        <dx:ASPxBinaryImage ID="ASPxBinaryImage2" ClientInstanceName="ClientBinaryImage" Width="250" Height="250" ShowLoadingImage="true" runat="server">     
        </dx:ASPxBinaryImage>           
        </td>	 
        </tr>
        </table>
        </td>
        <td>
        
            &nbsp;</td>
        <td>
        
            &nbsp;</td>
 </tr>

 <tr style="height:25px">
        <td style="width:50px">&nbsp;</td>
        <td style="width:50px">&nbsp;</td>
        <td style="width:150px">
            &nbsp;</td>
        <td style="width:20px"> &nbsp;</td>
        <td style="width:50px"> 
       
        <table style="display:none">
        <tr style="height:25px">
        <td style="width:150px">                            
            <asp:FileUpload ID="uploader1" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Height="20px" Width="250px" Accept=".png,.jpg,.jpeg,.gif" />
        </td>
        <td style="width:150px">
     
            <asp:FileUpload ID="uploader2" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Height="20px" Width="250px" Accept=".png,.jpg,.jpeg,.gif"/>
        </td>
        <td style="width:150px">
                
            <asp:FileUpload ID="uploader3" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Height="20px" Width="250px" Accept=".png,.jpg,.jpeg,.gif"/>
        </td>
        </tr>
        </table>
        </td>
        <td>
        
            &nbsp;</td>
        <td>
        
            &nbsp;</td>
 </tr>


 
 <tr style="height:25px">
        <td style="width:50px">&nbsp;</td>
        <td style="width:50px">&nbsp;</td>
        <td style="width:150px">&nbsp;</td>
        <td style="width:20px"> &nbsp;</td>
        <td style="width:500px"> 
                    <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp;&nbsp;
                  <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnClear" Theme="Default" >                        
                        <ClientSideEvents Click="function(s, e) {
                                var msg = confirm('Are you sure want to cancel this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }	

                                //cbClear.PerformCallback();
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp;&nbsp;
                           <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnDelete" Theme="Default" Enabled="False"  >                        
                               <ClientSideEvents Click="function(s, e) {	                                                            
                                var msg = confirm('Are you sure want to delete this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }
                            
								cbDelete.PerformCallback();
                            
                            }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp;&nbsp;

                    <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnSubmit" Theme="Default" >                        
                        <ClientSideEvents Click="function(s, e) {
                                    
                                    //alert(cboCategory.GetSelectedIndex());

	                                if (cboParentItem.GetText() == '') {
                                            toastr.warning('Please Select Parent Item Code!', 'Warning');
                                            cboParentItem.Focus();
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
		                                    e.processOnServer = false;
                                            return;
	                                    }  

                                    else if (txtParentItemQty.GetText() == 0){    
                                            toastr.warning('Please Input Parent Item Qty &gt; 0 !', 'Warning');
                                            txtParentItemQty.Focus();
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
		                                    e.processOnServer = false;
                                            return;
                                            }

                                    else if (cboParentItemUOM.GetText() == ''){    
                                            toastr.warning('Please Select Parent Item UOM!', 'Warning');
                                            cboParentItemUOM.Focus();
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
		                                    e.processOnServer = false;
                                            return;
                                        }
                                  

                                
                                    
                                    var msg = confirm('Are you sure want to save this data ?');                
                                    if (msg == false) {
                                         e.processOnServer = false;
                                         return;
                                    }
                                        cbSave.PerformCallback('Submit');      
                            }"  />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
        </td>
	 <td>
        <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf"></dx:ASPxHiddenField>
    </td>
    <td colspan="6">              
                <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
                    <ClientSideEvents Init="MessageError" />                                           
                </dx:ASPxCallback>
    </td>
    <td>
    
        &nbsp;</td>
 
 
    <td colspan="6">              
                <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
                    <ClientSideEvents CallbackComplete="MessageDelete" />                                           
                </dx:ASPxCallback>
    </td>
    <td>
    
        &nbsp;</td>

    <td colspan="6">
    
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                    <ClientSideEvents Init="MessageError" />                                           
                </dx:ASPxCallback>

  <%--              <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                    <ClientSideEvents CallbackComplete="MessageError" />                                           
                </dx:ASPxCallback>--%>
    
           
            </td>
    <td>
    
        <dx:ASPxCallback ID="cb" runat="server" ClientInstanceName="cb">
                        <ClientSideEvents 
                            
                            CallbackComplete="LoadCompleted"
                        />
                    </dx:ASPxCallback></td>
        <td>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'UOM'"></asp:SqlDataSource>
&nbsp;<asp:SqlDataSource ID="SqlDataSource1" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="Select Rtrim(Par_Code) AS Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialCategory'"></asp:SqlDataSource>
<%--                    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="ASPxCallback1">
                    <ClientSideEvents EndCallback="function (s, e) {
                        if (txtCardNo.GetEnabled(true)) {
                            txtRefundID.SetText(s.cpRefundID);
                        } else {
                            txtRefundID.SetText('');
                        }
                    }" CallbackComplete="OnEndCallback" />
                    </dx:ASPxCallback>--%>
        </td>
        <td style="width:20px">&nbsp;</td>
 </tr>
 </table>
</asp:Content>

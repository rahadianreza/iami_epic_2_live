﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data

Public Class PRApprovalDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False

#End Region

#Region "Initialization"

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|B020", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnApprove.SetEnabled(false);" & vbCrLf & _
                         "btnReject.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub PRApprovalDetail_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboDepartment)
            End Try
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim prno As String

        Master.SiteTitle = "PURCHASE REQUEST APPROVAL DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "B020")
        prno = Request.QueryString("ID") & ""

        If AuthUpdate = False Then
            btnApprove.Enabled = False
            btnReject.Enabled = False
        End If

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            txtPRNo.Text = prno
            up_FillCombo()
            up_LoadData(prno)
        End If

        cbApprove.JSProperties("cpMessage") = ""
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_FillCombo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsPRAcceptanceDB.GetComboData("PRBudget", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPRBudget.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("PRType", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPRType.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("Department", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboDepartment.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("Section", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboSection.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("CostCenter", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboCostCenter.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If
    End Sub

    Private Sub up_LoadData(pPRNo As String)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim cPRList As New ClsPRApproval


        Try
            cPRList.PRNumber = pPRNo
            ds = ClsPRApprovalDB.GetDataPR(cPRList, pUser, pErr)
            If pErr = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    dtDate.Value = ds.Tables(0).Rows(0)("PR_Date")
    
                    dtRequestPO.Value = ds.Tables(0).Rows(0)("Req_POIssueDate")
                    cboPRBudget.SelectedIndex = cboPRBudget.Items.IndexOf(cboPRBudget.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Budget_Code") & "")))
                    cboPRType.SelectedIndex = cboPRType.Items.IndexOf(cboPRType.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("PRType_Code") & "")))
                    cboDepartment.SelectedIndex = cboDepartment.Items.IndexOf(cboDepartment.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Department_Code") & "")))
                    cboSection.SelectedIndex = cboSection.Items.IndexOf(cboSection.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Section_Code") & "")))
                    cboCostCenter.SelectedIndex = cboCostCenter.Items.IndexOf(cboCostCenter.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("CostCenter") & "")))
                    txtProject.Text = ds.Tables(0).Rows(0)("Project")
                    txtRev.Text = ds.Tables(0).Rows(0)("Rev")
                    Dim PRStatus As String

                    If IsDBNull(ds.Tables(0).Rows(0)("Urgent_Status")) Then
                        chkUrgent.Checked = False
                        cbApprove.JSProperties("cpUrgent") = "0"
                    ElseIf ds.Tables(0).Rows(0)("Urgent_Status").ToString.ToUpper = "YES" Then
                        chkUrgent.Checked = True
                        cbApprove.JSProperties("cpUrgent") = "1"
                    ElseIf ds.Tables(0).Rows(0)("Urgent_Status").ToString.ToUpper = "NO" Then
                        chkUrgent.Checked = False
                        cbApprove.JSProperties("cpUrgent") = "0"
                    End If

                    If Not IsDBNull(ds.Tables(0).Rows(0)("Req_POIssueDate")) Then
                        dtRequestPO.Value = ds.Tables(0).Rows(0)("Req_POIssueDate")
                    End If

                    If IsDBNull(ds.Tables(0).Rows(0)("Urgent_Note")) Then
                        txtUrgentNote.Text = ""
                    Else
                        txtUrgentNote.Text = ds.Tables(0).Rows(0)("Urgent_Note")
                    End If

                    'txtUrgentNote.Enabled = False
                    'chkUrgent.Enabled = False

                    If IsDBNull(ds.Tables(0).Rows(0)("AppNoteUser")) Then
                        txtApprovalNote.Text = ""
                    Else
                        txtApprovalNote.Text = ds.Tables(0).Rows(0)("AppNoteUser")
                    End If

                    If IsDBNull(ds.Tables(0).Rows(0)("PR_Status")) Then
                        PRStatus = "0"""
                    Else
                        PRStatus = ds.Tables(0).Rows(0)("PR_Status")
                    End If

                    Dim ButtonStatus As String

                    ButtonStatus = ds.Tables(0).Rows(0)("Status_Button")


                    If ButtonStatus = "1" Then
                        btnApprove.Enabled = False
                        btnReject.Enabled = False
                    End If

                   


                    Grid.DataSource = ds
                    Grid.DataBind()
                End If
            End If
        Catch ex As Exception
            cbApprove.JSProperties("cpMessage") = ex.Message
        End Try
    End Sub

    Private Sub up_Print()
        'Response.Redirect("~/ViewPRAcceptance.aspx")
        Session("PRNumber") = txtPRNo.Text
        Session("Action") = "2"
        If chkUrgent.Checked Then
            Response.Redirect("~/ViewPRListDetailUrgent.aspx")

        Else
            Response.Redirect("~/ViewPRListDetail.aspx")
        End If
    End Sub

    Private Sub up_Approve()
        Dim PRApproval As New ClsPRApproval
        Dim MsgErr As String = ""
        Dim i As Integer

        PRApproval.PRNumber = txtPRNo.Text
        PRApproval.ApprovalNote = txtApprovalNote.Text
        PRApproval.Revision = txtRev.Text
        PRApproval.UrgentNote = txtUrgentNote.Text
        If chkUrgent.Value = "0" Then
            PRApproval.UrgentCls = "No"
            PRApproval.ReqPOIssueDate = ""
        ElseIf chkUrgent.Value = "1" Then
            PRApproval.UrgentCls = "Yes"
            PRApproval.ReqPOIssueDate = Format(dtRequestPO.Value, "yyyy-MM-dd")
        End If

        ClsPRApprovalDB.ApprovePRFull(PRApproval, pUser, MsgErr)

        If MsgErr = "" Then
            cbApprove.JSProperties("cpMessage") = "Data Has Been Approved Successfully"
        Else
            cbApprove.JSProperties("cpMessage") = MsgErr
        End If
    End Sub

    Private Sub up_Reject()
        Dim PRApproval As New ClsPRApproval
        Dim MsgErr As String = ""
        Dim i As Integer

        PRApproval.PRNumber = txtPRNo.Text
        PRApproval.ApprovalNote = txtApprovalNote.Text
        i = ClsPRApprovalDB.Reject(PRApproval, pUser, MsgErr)
        If MsgErr = "" Then
            cbReject.JSProperties("cpMessage") = "Data Has Been Reject Successfully"
        Else
            cbReject.JSProperties("cpMessage") = MsgErr
        End If
    End Sub
#End Region

#Region "Control Event"
    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        up_Print()
    End Sub
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        gs_Back = True
        Response.Redirect("~/PRApproval.aspx")
    End Sub

    Private Sub cbApprove_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbApprove.Callback

        up_Approve()
    End Sub
#End Region

 
    Private Sub cbReject_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbReject.Callback
        up_Reject()
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

End Class
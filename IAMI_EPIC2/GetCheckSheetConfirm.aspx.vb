﻿Imports DevExpress.Web.ASPxGridView
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Data.SqlClient

Public Class GetCheckSheetConfirm
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String

    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim py As String = ""
    Dim grpCommodity As String = ""
    Dim pagetype As String = ""
    Dim buyer As String = ""
#End Region

#Region "Initilization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        'sGlobal.getMenu("J020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        py = Request.QueryString("projecttype")
        prj = Request.QueryString("projectid")
        grp = Request.QueryString("groupid")
        comm = Request.QueryString("commodity")
        grpCommodity = Request.QueryString("groupcommodity")
        pagetype = Request.QueryString("pagetype")
        buyer = Request.QueryString("buyer")

        If pagetype = "submit" Or pagetype = "submit1" Then
            sGlobal.getMenu("J010")
        ElseIf pagetype = "release" Then
            sGlobal.getMenu("J020")
        Else
            sGlobal.getMenu("J030")
        End If

        Dim Spec As String,
               Bidderlis As String,
               Sourcing As String,
               reject As String

        If pagetype = "release" Then
           
            Spec = "btnSpecInformation.SetVisible(false);"
            ScriptManager.RegisterStartupScript(btnSpecInformation, btnSpecInformation.GetType(), "btnSpecInformation", Spec, True)

            Bidderlis = "btnBiddersList.SetVisible(false);"
            ScriptManager.RegisterStartupScript(btnBiddersList, btnBiddersList.GetType(), "btnBiddersList", Bidderlis, True)

            Sourcing = "btnSourcingSchedule.SetVisible(false);"
            ScriptManager.RegisterStartupScript(btnSourcingSchedule, btnSourcingSchedule.GetType(), "btnSourcingSchedule", Sourcing, True)

            reject = "btnReject.SetVisible(true);"
            ScriptManager.RegisterStartupScript(btnReject, btnReject.GetType(), "btnReject", reject, True)


        Else
            Spec = "btnSpecInformation.SetVisible(true);"
            ScriptManager.RegisterStartupScript(btnSpecInformation, btnSpecInformation.GetType(), "btnSpecInformation", Spec, True)

            Bidderlis = "btnBiddersList.SetVisible(true);"
            ScriptManager.RegisterStartupScript(btnBiddersList, btnBiddersList.GetType(), "btnBiddersList", Bidderlis, True)

            Sourcing = "btnSourcingSchedule.SetVisible(true);"
            ScriptManager.RegisterStartupScript(btnSourcingSchedule, btnSourcingSchedule.GetType(), "btnSourcingSchedule", Sourcing, True)

            reject = "btnReject.SetVisible(false);"
            ScriptManager.RegisterStartupScript(btnReject, btnReject.GetType(), "btnReject", reject, True)

        End If

        specInformation()
        bidderList()
        SourcingSchedule()
        up_GridLoad("")
        DummyHeadertoGrid()
    End Sub


    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub DummyHeadertoGrid()
        Dim ds As DataSet
        Dim clsgatespec As New clsGateSpecInformation
        ds = clsGateICheckSheetDB.GetDateSpecInformationHeaderToGrid(prj, grp, comm, pUser, pUser, statusAdmin, clsgatespec)

        If Not ds Is Nothing Then
            GridHeaderSpecInformation.Settings.ShowColumnHeaders = False

            GridHeaderSpecInformation.DataSource = ds.Tables(0)
            GridHeaderSpecInformation.DataBind()
        End If
        
    End Sub

    Private Sub up_Excel()
        specInformation()
        bidderList()
        SourcingSchedule()
        up_GridLoad("1")
        DummyHeadertoGrid()

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter
        Dim link2 As New PrintableComponentLink(ps)
        link2.Component = GridExporter2
        Dim link3 As New PrintableComponentLink(ps)
        link3.Component = GridExporter3
        Dim link4 As New PrintableComponentLink(ps)
        link4.Component = GridExporter4
        Dim link5 As New PrintableComponentLink(ps)
        link5.Component = GridExporter5

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1, link5, link2, link3, link4})
        compositeLink.PrintingSystem.ExportOptions.Xlsx.SheetName = "a,b"
        compositeLink.CreateDocument()

        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Transfer-Encoding", "binary")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ItemListSourcing" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else

            up_GridLoad("1")
        End If
    End Sub

    Private Sub up_FillComboProjectType(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                              pAdminStatus As String, pUserID As String, _
                              Optional ByRef pGroup As String = "", _
                              Optional ByRef pParent As String = "", _
                              Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = FillCombo(pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Public Shared Function FillCombo(pUserID As String, pAdminStatus As String, _
                                   Optional ByRef pGroup As String = "", _
                                   Optional ByRef pParent As String = "", _
                                   Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pAdminStatus = "1" Then
                    sql = "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description  " & vbCrLf & _
                            "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' AND Par_Code IN ('PT01','PT02') " & vbCrLf

                    If pParent <> "ALL" And pParent <> "" Then
                        sql = sql + "AND COALESCE(Par_ParentCode,'') = '" & pParent & "'"
                    End If

                Else
                    sql = sql + "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description " & vbCrLf & _
                        "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf & _
                        "AND Par_Code = (SELECT COALESCE(" & pGroup & "_Code,'')" & pGroup & "_Code FROM dbo.UserSetup WHERE UserID = '" & pUserID & "') AND Par_Code IN ('PT01','PT02')"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Private Sub up_FillComboGroupCommodityPIC(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _prjtype As String, _projid As String, _grpid As String, _comm As String, _type As String, _
                            pAdminStatus As String, pUserID As String, _
                            Optional ByRef pGroup As String = "", _
                            Optional ByRef pParent As String = "", _
                            Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsGateICheckSheetDB.FillComboProject("2", _prjtype, _projid, _grpid, _comm, _type, pUserID, statusAdmin, pmsg)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_FillComboProject(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                              pAdminStatus As String, pUserID As String, projtype As String, _
                              Optional ByRef pGroup As String = "", _
                              Optional ByRef pParent As String = "", _
                              Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsGateICheckSheetDB.FillComboProject("2", projtype, "", "", "", "P", pUserID, statusAdmin, pmsg)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsGateICheckSheetDB.getHeaderGrid(prj, grp, comm, IIf(String.IsNullOrEmpty(buyer), pUser, buyer), pUser, grpCommodity, statusAdmin)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()

            Dim ds2 As New DataSet
            ds2 = clsGateICheckSheetDB.GetRateJPY_NewGuidePrice(prj)

            If ds2.Tables(0).Rows.Count > 0 Then
                Grid.Columns("New_Guide_Price_Jpn").Caption = ds2.Tables(0).Rows(0)("Rate_YEN_IDR").ToString()
            Else
                Grid.Columns("New_Guide_Price_Jpn").Caption = "New Guide Price JPN=Rp "
            End If

        End If
    End Sub

    Protected Sub Grid_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True

    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""

        clsGateICheckSheetDB.InsertHeader(prj, grp, comm, e.NewValues("Part_No"), e.NewValues("Drawing_ISZ_SPLR"), pUser, "2")

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
            up_GridLoad("")
        End If

    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True

    End Sub

    Public Function checkingCbo() As Boolean
        If String.IsNullOrEmpty(py) = True Then
            cbSave.JSProperties("cpMessage") = "Project Type cannot be empty!"
            Return False
        ElseIf String.IsNullOrEmpty(prj) = True Then
            cbSave.JSProperties("cpMessage") = "Project Name cannot be empty!"
             Return False
        ElseIf String.IsNullOrEmpty(grp) = True Then
            cbSave.JSProperties("cpMessage") = "Group ID cannot be empty!"
           Return False
        ElseIf String.IsNullOrEmpty(comm) = True Then
            cbSave.JSProperties("cpMessage") = "Commodity cannot be empty!"
           Return False
        Else
          Return True
        End If
    End Function

    Protected Sub btnComplete_Click(sender As Object, e As EventArgs) Handles btnComplete.Click
        Dim pErr As String = ""
        If checkingCbo() = True Then
            clsGateICheckSheetDB.InsertGateStatus(prj, grp, comm, grpCommodity, "C", pUser, statusAdmin, pErr)
            cbSave.JSProperties("cpMessage") = "Data Completed!"
            btnComplete.ClientEnabled = False

            specInformation()
            bidderList()
            SourcingSchedule()
            up_GridLoad("1")
            DummyHeadertoGrid()
        End If
    End Sub
#End Region

#Region "Event Control"

    Private Sub Grid_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If Not Grid.IsNewRowEditing Then
            If e.Column.FieldName = "Part_No" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Protected Sub btnSpecInformation_Click(sender As Object, e As EventArgs) Handles btnSpecInformation.Click
        If checkingCbo() = True Then
            Response.Redirect("GateSpecInformation.aspx?project_type=" + py + "&project_id=" + prj + "&group_id=" + grp + "&comm=" + comm + "&page=J020&buyer=" + pUser + "&groupcommodity=" + grpCommodity + "&pagetype=" + pagetype)
        End If

    End Sub

    Private Sub cbSpecInformation_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSpecInformation.Callback
        cbSpecInformation.JSProperties("cp_SpecInformation") = "true"


    End Sub

    Protected Sub btnBiddersList_Click(sender As Object, e As EventArgs) Handles btnBiddersList.Click
        If checkingCbo() = True Then
            Response.Redirect("BiddersList.aspx?project_type=" + py + "&project_id=" + prj + "&group_id=" + grp + "&comm=" + comm + "&page=J020&buyer=" + pUser + "&groupcommodity=" + grpCommodity + "&pagetype=" + pagetype)
        End If
    End Sub

    Protected Sub btnSourcingSchedule_Click(sender As Object, e As EventArgs) Handles btnSourcingSchedule.Click
        If checkingCbo() = True Then
            Response.Redirect("GateSourcingSchedule.aspx?project_type=" + py + "&project_id=" + prj + "&group_id=" + grp + "&comm=" + comm + "&page=J020&buyer=" + pUser + "&groupcommodity=" + grpCommodity + "&pagetype=" + pagetype)
        End If
    End Sub

    Private Sub cbComplete_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbComplete.Callback
        If clsGateICheckSheetDB.CheckGateStatus(prj, grp, comm, pUser, grpCommodity) = "Completed" Then
            cbComplete.JSProperties("cp_Complete") = "true"
        Else
            cbComplete.JSProperties("cp_Complete") = "false"
        End If
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.VisibleIndex < 0) Then
            Exit Sub
        End If
        'masih draft /release dari gate I release 
        If clsGateICheckSheetDB.CheckGateStatus(prj, grp, comm, pUser, grpCommodity) <> "Completed" Then
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                e.Visible = False
            End If

        End If
    End Sub

    Protected Sub btnBacktoList_Click(sender As Object, e As EventArgs) Handles btnBacktoList.Click
        If pagetype = "submit" Or pagetype = "submit1" Then
            Response.Redirect("Gate_I_List.aspx?projecttype=" + py + "&projectid=" + prj + "&groupid=" + grp + "&commodity=" + comm + "&groupcomodity=" + grpCommodity)
        ElseIf pagetype = "release" Then
            Response.Redirect("Gate_I_List_Release.aspx?projecttype=" + py + "&projectid=" + prj + "&groupid=" + grp + "&commodity=" + comm + "&groupcomodity=" + grpCommodity)
        ElseIf pagetype = "confirm" Then
            Response.Redirect("Gate_I_List_Confirm.aspx?projecttype=" + py + "&projectid=" + prj + "&groupid=" + grp + "&commodity=" + comm + "&groupcomodity=" + grpCommodity)
        End If
    End Sub

    Protected Sub GridSourcing_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridSourcing.HtmlDataCellPrepared
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        e.Cell.BackColor = Color.LemonChiffon
        With e.DataColumn
            'If e.GetValue("Code_Activity").ToString() = "CA001" Or e.GetValue("Code_Activity").ToString() = "CA002" Then
            '    e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
            '    e.Cell.Attributes.Add("readonly", "true")
            '    e.Cell.BackColor = Color.LemonChiffon

            'End If

            If .FieldName = "Date_Actual" Then
                If Not IsDBNull(e.GetValue("Due_Date_Plan")) And Not IsDBNull(e.GetValue("Date_Actual")) Then
                    If DateDiff(DateInterval.Day, CDate(e.GetValue("Due_Date_Plan")), CDate(e.GetValue("Date_Actual"))) > 0 Then
                        e.Cell.BackColor = Color.Red
                    End If
                End If
            End If

        End With
    End Sub

    'lemon chiffon color grid
    Protected Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If clsGateICheckSheetDB.CheckGateStatus(prj, grp, comm, pUser, grpCommodity) <> "Completed" Then
            e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
            e.Cell.BackColor = Color.LemonChiffon
        End If

    End Sub

    Protected Sub GridCostControl_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridCostControl.HtmlDataCellPrepared
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Protected Sub GridBidderList_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridBidderList.HtmlDataCellPrepared
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Private Sub cbReject_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbReject.Callback
        Dim pErr As String = ""
        If checkingCbo() = True Then
            clsGateICheckSheetDB.RejectReleased(prj, grp, comm, grpCommodity, buyer, txtRejectNotes.Text, pUser, pErr)
            If pErr = "" Then
                cbReject.JSProperties("cpMessage") = "Data Has Been Reject Successfully"
            Else
                cbReject.JSProperties("cpMessage") = pErr
            End If

            specInformation()
            bidderList()
            SourcingSchedule()
            up_GridLoad("1")
            DummyHeadertoGrid()
        End If

    End Sub

#End Region

#Region "Detail"
    Private Sub specInformation()
        getExistsData()
        getGridDepartment(GridCostControl, "CC")
    End Sub

    Private Sub bidderList()
        up_GridLoadBidderList("")
    End Sub

    Private Sub SourcingSchedule()
        up_GridLoadSourcingSchedule("")
    End Sub

    Private Sub getExistsData()
        Dim ds As DataSet
        Dim clsgatespec As New clsGateSpecInformation
        ds = clsGateICheckSheetDB.GetDateSpecInformation(prj, grp, comm, buyer, pUser, grpCommodity, statusAdmin, clsgatespec, "1")

        'txtTitle.Text = clsgatespec.Title
        cboBaseDrawing.Value = clsgatespec.BaseDrawing
        cboSOR.Value = clsgatespec.SOR
        txtMaterial.Text = clsgatespec.Material
        txtProcess.Text = clsgatespec.Process
        cboDevSchedule.Value = clsgatespec.CheckDevelopmentSchedule
        txtAdditionalInformation.Text = clsgatespec.AdditionalInformation
        KHSAttendance.Value = CDate(IIf(String.IsNullOrEmpty(clsgatespec.KSHAttendance), "1990-01-01", clsgatespec.KSHAttendance))
    End Sub

    Private Sub getGridDepartment(_gridView As ASPxGridView, _department As String)
        Dim ds As DataSet = clsGateICheckSheetDB.GetGridDepartment(prj, grp, comm, buyer, _department, pUser, grpCommodity, statusAdmin, "1")
        _gridView.DataSource = ds
        _gridView.DataBind()
    End Sub

    Private Sub up_GridLoadBidderList(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsGateICheckSheetDB.GetGridBidderList(prj, grp, comm, buyer, pUser, grpCommodity, statusAdmin, "1")
        If ErrMsg = "" Then
            GridBidderList.DataSource = Ses
            GridBidderList.DataBind()
        End If
    End Sub

    Private Sub up_GridLoadSourcingSchedule(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsGateICheckSheetDB.GetGridSourcingSchedule(prj, grp, comm, buyer, pUser, grpCommodity, statusAdmin, "1")
        If ErrMsg = "" Then
            GridSourcing.DataSource = Ses
            GridSourcing.DataBind()
        End If
    End Sub
#End Region



End Class
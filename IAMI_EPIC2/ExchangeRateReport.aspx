﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ExchangeRateReport.aspx.vb" Inherits="IAMI_EPIC2.ExchangeRateReport" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        
        //Function for Message
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;

                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;

                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;

                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

            }

            if (s.cp_disabled == "N") {
                btnExcel.SetEnabled(true);
            } else if (s.cp_disabled == "Y") {
                btnExcel.SetEnabled(false);
            }
        }

        function BatchEditStartEditing(s, e) {
            if (e.focusedColumn.fieldName == "CEPlanning") {
                e.cancel = false;
            } else {
                e.cancel = true;
            }
        }  

        function GetPeriodAdj(s,e)
        {
           // alert(s.cpPeriodType);
            txtPeriodType.SetText(s.cpPeriodType);
            txtPeriodDescription.SetText(s.cpPeriodDescs);
            dtDateFrom.SetValue(s.cpDateFrom);
            dtDateTo.SetValue(s.cpDateTo);

        }

	</script>
    <style type="text/css">        
        .hidden-div
        {
            display:none;
        }
        .style1
        {
            height: 30px;
        }
        .style2
        {
            height: 50px;
        }
        .style3
        {
            height: 20px;
        }
    </style>       
    <style type="text/css">
        .tr-all-height
        {
              height:10px;
        }
        
        .td-col-left 
        {
            width:100px;
            padding:0px 0px 0px 10px;
            
        }
        
        .td-col-left2 
        {
            width:140px;
        }
        
        .td-col-mid
        {
            width:10px;
            
        }
        .td-col-mid2
        {
            width:40px;
            
        }
        .td-col-right
        {
            width:200px;
          
        }
        .td-col-free
        {
            width:20px;
            
        }
        
        .div-pad-fil
        {
            padding: 5px 5px 5px 5px;
        }
        
        .td-margin
        {
            padding:10px;
        }
        
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="div-pad-fil">
        <div class="div-pad-fil">
            <table style="width:100%; border: 1px solid black; height:100px">
                <tr style="height:20px">
                    <td class="td-col-left"></td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-left2"></td>
                    <td colspan="5"></td>
                </tr>
                <tr style="height:35px">
                    <td class="td-col-left">
                         <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="Period ID">
                        </dx1:aspxlabel>   
                    </td>
                    <td class="td-col-mid">&nbsp;</td>
                    <td class="td-col-left2" style="height:25px"  >
                         <dx1:ASPxComboBox ID="cboPeriodID" runat="server" ClientInstanceName="cboPeriodID"
                                Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="PeriodID"
                                ValueField="PeriodID" TextFormatString="{0}" Font-Size="9pt" 
                                Theme="Office2010Black" DropDownStyle="DropDownList" 
                                IncrementalFilteringMode="Contains" Height="25px">                            
                                <ClientSideEvents ValueChanged="function(s, e) {
	cbPeriod.PerformCallback();
}" />                           <Columns>            
                                <dx:ListBoxColumn Caption="Code" FieldName="PeriodID" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Period_Description" Width="250px" />
                                </Columns>
                                        <ItemStyle Height="10px" Paddings-Padding="4px" >

    <Paddings Padding="4px"></Paddings>
                                </ItemStyle>
                                <ButtonStyle Width="5px" Paddings-Padding="4px" >
    <Paddings Padding="4px"></Paddings>
                                </ButtonStyle>

                            </dx1:ASPxComboBox>
                         
                    </td>
                    
                    <td class="td-col-right" colspan="5">
                        <dx:ASPxTextBox ID="txtPeriodType" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                     Width="150px" ClientInstanceName="txtPeriodType" Height="25px" 
                                     Theme="Office2010Silver" BackColor="gainsboro" ReadOnly="true">
                     </dx:ASPxTextBox>
                    </td>
                    <td colspan="5"></td>
                   
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left; td-margin">
                         <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text=" ">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-left2" colspan="5">
                        <dx:ASPxTextBox ID="txtPeriodDescription" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                             Width="350px" ClientInstanceName="txtPeriodDescription" Height="25px" 
                             Theme="Office2010Silver" BackColor="gainsboro"  ReadOnly="true">
                         </dx:ASPxTextBox>
                    </td>

                    <td colspan="5"></td>
                   
                    
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left; td-margin">
                         <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="Period From">
                        </dx1:ASPxLabel>
                    </td>
                     <td class="td-col-mid">&nbsp;</td>
                     <td class="td-col-left2">
                        <dx:aspxdateedit ID="dtDateFrom" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateFrom"
                                        EditFormatString="MMM yyyy" DisplayFormatString="MMM yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom" ReadOnly="true" BackColor="Gainsboro">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:aspxdateedit>  
                     </td>
                     <td class="td-col-mid2">
                      <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="To">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-right">
                        <dx:aspxdateedit ID="dtDateTo" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateTo"
                                        EditFormatString="MMM yyyy" DisplayFormatString="MMM yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom" ReadOnly="true" BackColor="Gainsboro">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                    </dx:aspxdateedit>
                    </td>
                    <td colspan="5"></td>
                </tr>

                <tr>
                    <td colspan="13" style="height:30px; padding: 0px 0px 10px 10px; ">
                        <dx:ASPxButton ID="btnRefresh" runat="server" AutoPostBack="False" 
                            ClientInstanceName="btnRefresh" Font-Names="Segoe UI" Font-Size="9pt" 
                            Height="25px" Text="Show Data" Theme="Default" UseSubmitBehavior="False" 
                            Width="80px">
                            <ClientSideEvents Click="function(s, e) {
                                if (cboPeriodID.GetText()==''){
                                    //alert(cboPeriodID.GetText());
                                    toastr.warning('Please Select Period ID', 'Warning');
                                    toastr.options.closeButton = false;
                                    toastr.options.debug = false;
                                    toastr.options.newestOnTop = false;
                                    toastr.options.progressBar = false;
                                    toastr.options.preventDuplicates = true;
                                    toastr.options.onclick = null;
                                    return false;
                                }
                                Grid.PerformCallback('load|');
                                s.cp_val = 0;
                                s.cp_message = '';
                            }" />
                        </dx:ASPxButton>
                     &nbsp;
                        <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnExcel" Theme="Default">
                              <ClientSideEvents Click="function(s, e) {                                         
                                                            Grid.PerformCallback('excel');
                                                            lblInfo.SetText('');
                                                        }" />
                        </dx:ASPxButton>
                    &nbsp;

                    </td>
                   
                </tr>
                <tr>
                    <td colspan="10" style="height: 20px">
                         <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                <dx:ASPxCallback ID="cbPeriod" runat="server"  ClientInstanceName="cbPeriod">
                    <ClientSideEvents EndCallback="GetPeriodAdj" />
                </dx:ASPxCallback>
                 <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT PeriodID=RIGHT('00'+CAST(SequenceNo AS VARCHAR(10)),2),Period_Description FROM dbo.Mst_PeriodAdjustment">
                </asp:SqlDataSource>
                    </td>
                </tr>
                </table>    
        </div>
        <div style="padding: 5px 5px 5px 5px">
            
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" Width="100%" 
                Font-Names="Segoe UI" Font-Size="9pt" >
                <ClientSideEvents EndCallback="OnEndCallback" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption=" " FieldName="RNo" VisibleIndex="1" Width="50px">
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                        </CellStyle>
                        <Settings AllowAutoFilter="False" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Period" FieldName="Period" VisibleIndex="2" Width="150px">
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                        </CellStyle>
                        <Settings AllowAutoFilter="False" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn Caption="Currency" Name="Currency" VisibleIndex="3">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="USD/Rp" FieldName="CurrentUSD" VisibleIndex="0"
                                Width="120px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                                <Settings AllowAutoFilter="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Yen/Rp" FieldName="CurrentJPN" VisibleIndex="1"
                                Width="120px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                                <Settings AllowAutoFilter="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Baht/Rp" FieldName="CurrentTHB" VisibleIndex="2"
                                Width="120px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                                <Settings AllowAutoFilter="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Eur/Rp" FieldName="CurrentEUR" VisibleIndex="3"
                                Width="120px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                                <Settings AllowAutoFilter="False" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewDataTextColumn Caption=" " FieldName="RNo" VisibleIndex="4" Width="50px">
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                        </CellStyle>
                        <Settings AllowAutoFilter="False" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Period" FieldName="PeriodPrev" VisibleIndex="5"
                        Width="150px">
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                        </CellStyle>
                        <Settings AllowAutoFilter="False" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn Caption="Currency" Name="Currency" VisibleIndex="6">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="USD/Rp" FieldName="PrevUSD" VisibleIndex="0"
                                Width="120px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                                <Settings AllowAutoFilter="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Yen/Rp" FieldName="PrevJPN" VisibleIndex="1"
                                Width="120px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                                <Settings AllowAutoFilter="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Baht/Rp" FieldName="PrevTHB" VisibleIndex="2"
                                Width="120px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                                <Settings AllowAutoFilter="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Eur/Rp" FieldName="PrevEUR" VisibleIndex="3"
                                Width="120px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                                <Settings AllowAutoFilter="False" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                </Columns>
                <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                </SettingsPager>
                <Settings ShowFilterRow="True" VerticalScrollableHeight="300" HorizontalScrollBarMode="Auto"
                    VerticalScrollBarMode="Auto"></Settings>
                <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                    EditFormColumnCaption-Paddings-PaddingRight="10px">
                    <Header HorizontalAlign="Center">
                        <Paddings Padding="2px"></Paddings>
                    </Header>
                    <EditFormColumnCaption>
                        <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
                    </EditFormColumnCaption>
                </Styles>
            </dx:ASPxGridView>
        </div>
    
    </div>
</asp:Content>

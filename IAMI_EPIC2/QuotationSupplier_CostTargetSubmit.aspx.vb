﻿Imports DevExpress.XtraPrinting
Imports DevExpress.Web.ASPxGridView
Imports System.IO

Public Class QuotationSupplier_CostTargetSubmit
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim pErr As String = ""

    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim grpcomm As String = ""
    Dim py As String = ""
    Dim flag As Integer = 0
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("L020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            prj = Request.QueryString("projectid")
            grp = Request.QueryString("groupid")
            comm = Request.QueryString("commodity")
            grpcomm = Request.QueryString("groupcomodity")

            cboProject.Value = prj
            cboGroup.Value = grp
            cboCommodity.Value = comm
            cboGroupCommodity.Value = grpcomm

            cboProject.ClientEnabled = False
            cboGroup.ClientEnabled = False
            cboCommodity.ClientEnabled = False
            cboGroupCommodity.ClientEnabled = False
            cboProject.BackColor = System.Drawing.Color.LightGray
            cboGroup.BackColor = System.Drawing.Color.LightGray
            cboCommodity.BackColor = System.Drawing.Color.LightGray
            cboGroupCommodity.BackColor = System.Drawing.Color.LightGray
            cboProject.ForeColor = System.Drawing.Color.Black
            cboGroup.ForeColor = System.Drawing.Color.Black
            cboCommodity.ForeColor = System.Drawing.Color.Black
            cboGroupCommodity.ForeColor = System.Drawing.Color.Black

            If clsQuotationSupplierDB.CompleteStatus(prj, grp, comm, grpcomm, pUser) <> "0" Then
                btnComplete.ClientEnabled = False
            End If

            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_Excel()
        up_GridLoad("1")

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=QuotationSupplier_TargetSubmit" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub up_FillComboGroupCommodityPIC(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _projid As String, _grpid As String, _comm As String, _type As String, _
                           pAdminStatus As String, pUserID As String, _
                           Optional ByRef pGroup As String = "", _
                           Optional ByRef pParent As String = "", _
                           Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsGateICheckSheetDB.FillComboProject("7", "", _projid, _grpid, _comm, _type, pUserID, statusAdmin, pmsg)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsQuotationSupplierDB.getHeaderCostTarget(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, pUser)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()

            If Not Ses Is Nothing Then
                For x As Integer = 0 To Ses.Tables(0).Rows.Count - 1
                    GetQuotation(Ses.Tables(0).Rows(x).Item("Project_ID").ToString(), Ses.Tables(0).Rows(x).Item("Group_ID").ToString(), Ses.Tables(0).Rows(x).Item("Commodity").ToString(), Ses.Tables(0).Rows(x).Item("Group_Comodity").ToString())
                Next
            End If
        End If
    End Sub
#End Region

#Region "Event Control"
    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.VisibleIndex < 0) Then
            Exit Sub
        End If

        If clsQuotationSupplierDB.CompleteStatus(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, pUser) <> "0" Then
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                e.Visible = False
            End If
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else

            up_GridLoad("1")
        End If
    End Sub

    Public Function checkingCbo() As Boolean
        If String.IsNullOrEmpty(cboProject.Value) = True Then
            cbSave.JSProperties("cpMessage") = "Project Name cannot be empty!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return False
        ElseIf String.IsNullOrEmpty(cboGroup.Value) = True Then
            cbSave.JSProperties("cpMessage") = "Group ID cannot be empty!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return False
        ElseIf String.IsNullOrEmpty(cboCommodity.Value) = True Then
            cbSave.JSProperties("cpMessage") = "Commodity cannot be empty!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return False
        Else
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return True
        End If
    End Function

    Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProject As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroup, pProject, "", "", "1", statusAdmin, pUser, "")
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboGroupCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        End If
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        clsQuotationSupplierDB.UpdateQuotationSupplierCostTarget(cboProject.Value, cboGroup.Value, cboCommodity.Value, e.Keys("Part_No"), cboGroupCommodity.Value, e.NewValues("TargetCostPart"), e.NewValues("TargetToolingCost"), pUser, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
        End If
    End Sub

    Protected Sub btnComplete_Click(sender As Object, e As EventArgs) Handles btnComplete.Click
        clsQuotationSupplierDB.CompleteConfirmSubmitUser("1", cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, pUser, pErr)

        If pErr <> "" Then
            'show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            cbSave.JSProperties("cpMessage") = pErr
            up_GridLoad("")

            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        Else
            'show_error(MsgTypeEnum.Success, "Data Saved Successfully!", 1)
            cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
            up_GridLoad("")

            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cbMessage_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbMessage.Callback

    End Sub

    Private Sub cbComplete_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbComplete.Callback
        If clsQuotationSupplierDB.CompleteStatus(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, pUser) = "1" Then
            cbComplete.JSProperties("cp_statusComplete") = "false"
            cbComplete.JSProperties("cp_statusConfirm") = "false"
        ElseIf clsQuotationSupplierDB.CompleteStatus(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, pUser) = "2" Then
            cbComplete.JSProperties("cp_statusComplete") = "false"
            cbComplete.JSProperties("cp_statusConfirm") = "true"
        ElseIf clsQuotationSupplierDB.CompleteStatus(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, pUser) = "3" Then
            cbComplete.JSProperties("cp_statusComplete") = "false"
            cbComplete.JSProperties("cp_statusConfirm") = "false"
        Else
            cbComplete.JSProperties("cp_statusComplete") = "true"
            cbComplete.JSProperties("cp_statusConfirm") = "false"
        End If

        cbComplete.JSProperties("cp_TransNo") = clsQuotationSupplierDB.GetTransNo(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, pUser)
    End Sub
#End Region


    Private Sub btnConfirm_Click(sender As Object, e As System.EventArgs) Handles btnConfirm.Click
        clsQuotationSupplierDB.CompleteConfirmSubmitUser("3", cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, pUser, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            show_error(MsgTypeEnum.Success, "Saved data successfully!", 1)
        End If
    End Sub


    Public Sub GetQuotation(_projectid As String, _groupid As String, _commodity As String, _groupcomodity As String)
        Dim ds As DataSet = clsQuotationSupplierDB.getHeaderCostTarget(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, pUser)

        For y As Integer = 15 To ds.Tables(0).Columns.Count - 1
            Dim colCheck As Object = Grid.Columns(ds.Tables(0).Columns(y).ColumnName.Replace("VendorPartCost ", ""))
            If colCheck IsNot Nothing Then
                Return
            End If
        Next
     

        If flag < 1 Then
            Dim bandColumn As GridViewBandColumn

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                For y As Integer = 15 To ds.Tables(0).Columns.Count - 1
                    If ds.Tables(0).Columns(y).ColumnName.Contains("VendorPartCost") Then
                        bandColumn = New GridViewBandColumn()
                        bandColumn.Caption = ds.Tables(0).Columns(y).ColumnName.Replace("VendorPartCost ", "")
                        bandColumn.VisibleIndex = 15
                        bandColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                        bandColumn.VisibleIndex = 2
                    End If

                    Dim col As GridViewDataTextColumn = New GridViewDataTextColumn()
                    col.Caption = IIf(ds.Tables(0).Columns(y).ColumnName.Contains("VendorPartCost"), "Part Cost", "Tolling Cost")
                    col.FieldName = ds.Tables(0).Columns(y).ColumnName
                    col.PropertiesTextEdit.DisplayFormatString = "#,##0.00"
                    col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                    col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                    bandColumn.Columns.Add(col)

                    If ds.Tables(0).Columns(y).ColumnName.Contains("VendorToolingCost") And i = 0 Then
                        Grid.Columns.Add(bandColumn)
                    End If
                Next
            Next

            flag += 1
        End If
    End Sub

    Protected Sub btnBacktoList_Click(sender As Object, e As EventArgs) Handles btnBacktoList.Click
        Response.Redirect("QuotationSupplier_CostTargetSubmit_List.aspx?projectid=" + cboProject.Value + "&groupid=" + cboGroup.Value + "&commodity=" + cboCommodity.Value + "&groupcomodity=" + cboGroupCommodity.Value)
    End Sub
End Class
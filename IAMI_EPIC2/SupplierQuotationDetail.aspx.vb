﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxGridView
Imports System.IO
Imports System.Transactions
Imports System.Web.UI

Public Class SupplierQuotationDetail
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    
#End Region

#Region "PROCEDURE"
    Private Sub GetSupplier()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_GetSupplier", "UserID", Session("user"), ErrMsg)

        If ErrMsg = "" Then
            txtSupplierCode.Text = ds.Tables(0).Rows(0).Item("Supplier_Code").ToString().Trim()
            txtSupplierName.Text = ds.Tables(0).Rows(0).Item("Supplier_Name").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub FillComboRFQNumber()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_FillCombo", "SQA|SupplierCode|Condition|RFQNumber|QuotationNo", "0|" & txtSupplierCode.Text & "|1||", ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_FillCombo", "SQA|SupplierCode|Condition|RFQNumber|QuotationNo", "0|" & txtSupplierCode.Text & "|2|" & Split(Request.QueryString(0), "|")(1) & "|" & Split(Request.QueryString(0), "|")(0), ErrMsg)
        End If

        If ErrMsg = "" Then
            cboRFQNumber.DataSource = ds.Tables(0)
            cboRFQNumber.DataBind()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboRFQNumber)
        End If
    End Sub

    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_List", "Source|SupplierCode|RFQNo|Rev", "1|" & txtSupplierCode.Text & "|" & cboRFQNumber.Text & "|", ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_List", "Source|SupplierCode|RFQNo|Rev", "2|" & txtSupplierCode.Text & "|" & cboRFQNumber.Text & "|" & txtRev.Text, ErrMsg)
        End If

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub GridLoadAttachment()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", RevNo As String = ""

        Try
            Dim dscheck As DataSet
            dscheck = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Exist", "RFQNumber|SupplierCode", cboRFQNumber.Text & "|" & txtSupplierCode.Text, ErrMsg)
            If dscheck.Tables(0).Rows.Count > 0 Then
                If dscheck.Tables(0).Rows(0).Item("Ret").ToString() = "1" Then
                    If txtRev.Text = "" Then
                        RevNo = hf.Get("RevisionNo")
                    Else
                        RevNo = txtRev.Text
                    End If

                    ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_ListAttachment", "SQA|QuotationNo|RevisionNo|RFQNumber", "0|" & txtQuotationNo.Text & "|" & RevNo & "|" & cboRFQNumber.Text, ErrMsg)

                    If ErrMsg = "" Then
                        GridAtc.DataSource = ds.Tables(0)
                        GridAtc.DataBind()
                    End If
                Else
                    GridAtc.DataSource = Nothing
                    GridAtc.DataBind()
                End If

            Else
                GridAtc.DataSource = Nothing
                GridAtc.DataBind()
            End If



            'ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_ListAttachment", "SQA|QuotationNo|RevisionNo|RFQNumber", "0|" & txtQuotationNo.Text & "|" & RevNo & "|" & cboRFQNumber.Text, ErrMsg)

            'If ErrMsg = "" Then

            '    GridAtc.DataSource = ds.Tables(0)
            '    GridAtc.DataBind()
            'Else
            '    GridAtc.DataSource = Nothing
            '    GridAtc.DataBind()
            'End If

            'If ds.Tables(0).Rows.Count = 0 Then
            '    'DisplayMessage(MsgTypeEnum.Info, "There is no attachment to show!", GridAtc)
            'End If
            'Else
            'DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridAtc)
            'End If

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridAtc)
        End Try
    End Sub

    Private Sub SetInformation(ByVal pObj As Object)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_SetHeader", "SQA|Source|SupplierCode|RFQNo", "0|1|" & txtSupplierCode.Text & "|" & cboRFQNumber.Text, ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_SetHeader", "SQA|Source|SupplierCode|RFQNo", "0|2|" & txtSupplierCode.Text & "|" & cboRFQNumber.Text, ErrMsg)
        End If


        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                pObj.JSProperties("cpQuotationNo") = ds.Tables(0).Rows(0).Item("Quotation_No").ToString()
                If Request.QueryString.Count > 0 Then
                    pObj.JSProperties("cpRevNo") = Split(Request.QueryString(0), "|")(2) 'ds.Tables(0).Rows(0).Item("Rev").ToString()

                    If ds.Tables(0).Rows(0).Item("LastRev") <> Split(Request.QueryString(0), "|")(2) Then
                        pObj.JSProperties("cpDisabledButton") = "1"
                    Else
                        'CHECK: The data already submitted or not
                        If IsDataSubmitted() = True Then
                            pObj.JSProperties("cpDisabledButton") = "1"
                        Else
                            pObj.JSProperties("cpDisabledButton") = "0"
                        End If
                    End If
                Else
                    pObj.JSProperties("cpRevNo") = "0"
                    pObj.JSProperties("cpDisabledButton") = "0"
                End If

                'CHECK: The data already exist or not
                If IsDataExist() = True Then
                    pObj.JSProperties("cpDisabledHeader") = "1"
                Else
                    pObj.JSProperties("cpDisabledHeader") = "0"
                End If

                ' dtQuotationDate.Value = ds.Tables(0).Rows(0).Item("Quotation_Date")
                pObj.JSProperties("cpQuotationDate") = ds.Tables(0).Rows(0).Item("Quotation_Date")
                pObj.JSProperties("cpRFQTitle") = ds.Tables(0).Rows(0).Item("RFQ_Title").ToString()
                pObj.JSProperties("cpNote") = ds.Tables(0).Rows(0).Item("Notes").ToString()
                Session("C030_RFQSet") = ds.Tables(0).Rows(0).Item("RFQ_Set").ToString()

            Else
                pObj.JSProperties("cpQuotationNo") = ""
                pObj.JSProperties("cpRevNo") = "0"
                pObj.JSProperties("cpRFQTitle") = ""
                pObj.JSProperties("cpNote") = ""
                pObj.JSProperties("cpDisabledButton") = "0"
                Session("C030_RFQSet") = ""

                Dim dt As Date
                dt = Year(Now) & "-" & Month(Now) & "-01"
                dtQuotationDate.Text = dt
                pObj.JSProperties("cpQuotationDate") = dt
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, pObj)
        End If
    End Sub

    Private Sub SaveAttachment(ByVal pQuotationNo As String, ByVal pRevisionNo As String, ByVal pRFQNumber As String, ByVal pFileName As String, ByVal pFilePath As String, ByVal pFileSize As Single)
        Dim ls_SQL As String = "", UserLogin As String = Session("user")

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                ls_SQL = "EXEC sp_Quotation_Insert_Attachment " & _
                         "'" & pQuotationNo & "','" & pRevisionNo & "','" & pRFQNumber & "'," & _
                         "'" & pFileName & "','" & pFilePath & "','" & UserLogin & "'," & _
                         "'" & pFileSize & "'"

                Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                sqlComm.CommandType = CommandType.Text
                sqlComm.ExecuteNonQuery()
                sqlComm.Dispose()

                sqlTran.Commit()
            End Using
        End Using
    End Sub

    Private Sub DeleteAttachment(ByVal pDeleteOption As String, Optional pQuotationNo As String = "", Optional pRevisionNo As String = "", Optional pRFQNumber As String = "", Optional pFileName As String = "", Optional pSeqNo As Integer = 1)
        Select Case pDeleteOption
            Case "database"
                Dim ls_SQL As String = ""

                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()

                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                        ls_SQL = "EXEC sp_Quotation_DeleteAttachment" & _
                                 "'" & pQuotationNo & "','" & pRevisionNo & "','" & pRFQNumber & "','" & pFileName & "'," & pSeqNo & ""

                        Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                        sqlComm.CommandType = CommandType.Text
                        sqlComm.ExecuteNonQuery()
                        sqlComm.Dispose()

                        sqlTran.Commit()
                    End Using
                End Using

            Case "file"
                Dim sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                sqlConn.Open()

                Dim ds As New DataSet
                Dim folderPath As String = Server.MapPath("~/Attachments/")
                ds = GetDataSource(CmdType.SQLScript, "SELECT REPLACE(FilePath,LEFT(FIlePath,14),'') FileName FROM Quotation_Attachment WHERE Quotation_No='" & pQuotationNo & "' AND Rev='" & pRevisionNo & "' AND RFQ_Number='" & pRFQNumber & "' AND FileName='" & pFileName & "' AND Seq_No = '" & pSeqNo & "'")
                If ds.Tables(0).Rows.Count > 0 Then
                    If System.IO.File.Exists(folderPath & ds.Tables(0).Rows(0)("FileName").ToString().Trim()) = True Then
                        System.IO.File.Delete(folderPath & ds.Tables(0).Rows(0)("FileName").ToString().Trim())
                    End If
                End If

                ds.Dispose()
                sqlConn.Close()
        End Select
    End Sub
#End Region

#Region "FUNCTION"
    Private Function IsDataExist() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Exist", "SupplierCode|RFQNumber", txtSupplierCode.Text & "|" & cboRFQNumber.Text, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item("Ret").ToString() = "0" Then
                        'NOT EXIST
                        retVal = False
                    Else
                        'EXIST
                        retVal = True
                    End If
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cb)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataSubmitted() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Submitted", "SupplierCode|RFQNumber|Rev", txtSupplierCode.Text & "|" & cboRFQNumber.Text & "|" & txtRev.Text, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item("Ret").ToString() = "0" Then
                        'DATA NOT EXIST >> NOT SUBMITTED / DRAFT
                        retVal = False
                    Else
                        'DATA EXIST >> SUBMITTED
                        retVal = True
                    End If
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cb)
                Return retVal
            End If
        End Using

        Return retVal
    End Function
#End Region

#Region "EVENTS"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If IsNothing(Session("user")) = False Then
            Try
                Call GetSupplier()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboRFQNumber)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "SUPPLIER QUOTATION DETAIL"

        Dim dt As Date

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            dt = Year(Now) & "-" & Month(Now) & "-" & Day(Now)
            If IsDataExist() = False Then
                dtQuotationDate.Value = dt
            End If

            ScriptManager.RegisterStartupScript(btnAddFile, btnAddFile.GetType(), "btnAddFile", "btnAddFile.SetEnabled(false);", True)
        End If

        If Request.QueryString.Count > 0 Then
            Try
                txtQuotationNo.Text = Split(Request.QueryString(0), "|")(0)
                cboRFQNumber.Value = Split(Request.QueryString(0), "|")(1)
                txtRev.Text = Split(Request.QueryString(0), "|")(2)

                'CHECK: The data already submitted or not
                If IsDataSubmitted() = True Then
                    btnDraft.Enabled = False
                    btnSubmit.Enabled = False
                    Dim script As String = ""
                    script = "txtQuotationNo.SetEnabled(false);" & vbCrLf & _
                             "cboRFQNumber.SetEnabled(false);"
                    ScriptManager.RegisterStartupScript(txtQuotationNo, txtQuotationNo.GetType(), "txtQuotationNo", script, True)
					ucAtc.Enabled = False
                    btnDeleteAtc.Enabled = False
                Else
                    btnDraft.Enabled = True
                    btnSubmit.Enabled = True

                    Dim script As String = ""
                    script = "txtQuotationNo.SetEnabled(false);" & vbCrLf & _
                             "cboRFQNumber.SetEnabled(false);"
                    ScriptManager.RegisterStartupScript(txtQuotationNo, txtQuotationNo.GetType(), "txtQuotationNo", script, True)
                    ucAtc.Enabled = True
                    btnDeleteAtc.Enabled = True						   
                End If

                

            Catch ex As Exception
                Response.Redirect("SupplierQuotationDetail.aspx")
            End Try
        End If
    End Sub

    Private Sub SupplierQuotationDetail_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        'Session.Remove("C030_DataExist")
    End Sub

    Private Sub cb_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cb.Callback
        Select Case e.Parameter
            Case "DeleteAttachment"
                Dim iData As Integer = 0
                Dim iDataSeqNo As Integer = 0
                Dim param As String = hf.Get("DeleteAttachment")
                Dim paramSeqNo As String = hf.Get("SeqNo")

                'If param <> "" Then param = Strings.Left(param, Len(param) - 1)

                'IDENTIFY THE NUMBER OF DATA FILE NAME
                For idx As Integer = 1 To Len(param)
                    If Strings.Mid(param, idx, 1) = "|" Then
                        iData = iData + 1
                    End If
                Next idx

                For idx2 As Integer = 1 To Len(paramSeqNo)
                    If Strings.Mid(paramSeqNo, idx2, 1) = "|" Then
                        iDataSeqNo = iDataSeqNo + 1
                    End If
                Next idx2
                'DELETE ATTACHMENT
                For idx As Integer = 0 To iData - 1
                    For idx2 As Integer = 0 To iDataSeqNo - 1
                        'file
                        Call DeleteAttachment("file", txtQuotationNo.Text, txtRev.Text, cboRFQNumber.Text, Split(param, "|")(idx), Split(paramSeqNo, "|")(idx2))

                        'database
                        Call DeleteAttachment("database", txtQuotationNo.Text, txtRev.Text, cboRFQNumber.Text, Split(param, "|")(idx), Split(paramSeqNo, "|")(idx2))
                    Next idx2
                Next idx

                cb.JSProperties("cpActionAfter") = "DeleteAtc"
                DisplayMessage(MsgTypeEnum.Success, "Data deleted successfully!", cb)

            Case Else
                Call SetInformation(cb)
        End Select
    End Sub

    'Private Sub cb_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cb.Callback
    '    Select Case e.Parameter
    '        Case "DeleteAttachment"
    '            Dim iData As Integer = 0
    '            Dim param As String = hf.Get("DeleteAttachment")
    '            'If param <> "" Then param = Strings.Left(param, Len(param) - 1)

    '            'IDENTIFY THE NUMBER OF DATA
    '            For idx As Integer = 1 To Len(param)
    '                If Strings.Mid(param, idx, 1) = "|" Then
    '                    iData = iData + 1
    '                End If
    '            Next idx

    '            'DELETE ATTACHMENT
    '            For idx As Integer = 0 To iData - 1
    '                'file
    '                Call DeleteAttachment("file", txtQuotationNo.Text, txtRev.Text, cboRFQNumber.Text, Split(param, "|")(idx))

    '                'database
    '                Call DeleteAttachment("database", txtQuotationNo.Text, txtRev.Text, cboRFQNumber.Text, Split(param, "|")(idx))
    '            Next idx

    '            cb.JSProperties("cpActionAfter") = "DeleteAtc"
    '            DisplayMessage(MsgTypeEnum.Success, "Data deleted successfully!", cb)

    '        Case Else
    '            Call SetInformation(cb)
    '    End Select
    'End Sub

    Private Sub cbExist_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbExist.Callback
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        Select Case e.Parameter
            Case "exist"
                cbExist.JSProperties("cpParameter") = "exist"

                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()
                    Dim sqlComm
                    Dim sql = "sp_QuotationHeader_checkQuotationNoPerSupplier"
                    sqlComm = New SqlCommand(sql, sqlConn)
                    sqlComm.CommandType = CommandType.StoredProcedure
                    sqlComm.Parameters.AddWithValue("RFQNumber", cboRFQNumber.Text)
                    sqlComm.Parameters.AddWithValue("SupplierCode", txtSupplierCode.Text)
                    sqlComm.Parameters.AddWithValue("QuotationNo", txtQuotationNo.Text)

                    Dim da As New SqlDataAdapter(sqlComm)
                    Dim ds1 As New DataSet
                    da.Fill(ds1)

                    If ds1.Tables(0).Rows.Count > 0 Then
                        Call DisplayMessage(MsgTypeEnum.ErrorMsg, "Quotaion No already exists", cbExist)
                       
                    End If
                End Using

                ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Exist", "RFQNumber|SupplierCode", cboRFQNumber.Text & "|" & txtSupplierCode.Text, ErrMsg)

                If ErrMsg = "" Then
                    If ds.Tables(0).Rows.Count > 0 Then

                        cbExist.JSProperties("cpResult") = ds.Tables(0).Rows(0).Item("Ret").ToString()
                    Else
                        cbExist.JSProperties("cpResult") = "0"
                            End If
                        Else
                            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                        End If

            Case "IsSubmitted"
                        cbExist.JSProperties("cpParameter") = "IsSubmitted"
                        If IsDataSubmitted() = True Then
                            cbExist.JSProperties("cpResult") = "1"
                        Else
                            cbExist.JSProperties("cpResult") = "0"
                        End If

            Case "draft"
                        Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
                        Dim idx As Integer = 0

                        Try
                            cbExist.JSProperties("cpParameter") = "draft"

                            Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                                sqlConn.Open()

                                Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                                    If IsNothing(Session("C030_RFQSet")) = False Then
                                        ls_RFQSet = Session("C030_RFQSet").ToString()
                                    End If

                                    '#HEADER
                                    ls_SQL = "EXEC sp_Quotation_Update_Header " & _
                                             "'0'," & _
                                             "'" & txtQuotationNo.Text & "','" & txtRev.Text & "','" & ls_RFQSet & "','" & cboRFQNumber.Text & "','" & txtSupplierCode.Text & "'," & _
                                             "'" & txtRFQTitle.Text & "','" & Format(CDate(dtQuotationDate.Value), "yyyy-MM-dd") & "','" & memoNote.Text & "','0','" & UserLogin & "'"
                                    Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                                    sqlComm.CommandType = CommandType.Text
                                    sqlComm.ExecuteNonQuery()
                                    sqlComm.Dispose()

                                    ''#DETAIL
                                    'ls_SQL = "EXEC sp_Quotation_Update_Detail '0','" & txtQuotationNo.Text & "','" & txtRev.Text & "','" & cboRFQNumber.Text & "'"
                                    'sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                                    'sqlComm.CommandType = CommandType.Text
                                    'sqlComm.ExecuteNonQuery()
                                    'sqlComm.Dispose()

                                    ''#ATTACHMENT
                                    'ls_SQL = "EXEC sp_Quotation_Update_Attachment '" & txtRev.Text & "','" & cboRFQNumber.Text & "'"
                                    'sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                                    'sqlComm.CommandType = CommandType.Text
                                    'sqlComm.ExecuteNonQuery()
                                    'sqlComm.Dispose()

                                    sqlTran.Commit()
                                End Using
                            End Using

                            Call SetInformation(cbExist)
                            Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", cbExist)

                        Catch ex As Exception
                            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                        End Try

            Case "submit"
                        Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
                        Dim idx As Integer = 0

                cbExist.JSProperties("cpParameter") = "submit"

                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()
                    Dim sqlComm
                    Dim sql = "sp_QuotationAttachment_checkData"
                    sqlComm = New SqlCommand(sql, sqlConn)
                    sqlComm.CommandType = CommandType.StoredProcedure
                    sqlComm.Parameters.AddWithValue("RFQNumber", cboRFQNumber.Text)
                    sqlComm.Parameters.AddWithValue("QuotationNo", txtQuotationNo.Text)
                    sqlComm.Parameters.AddWithValue("QuotationRev", txtRev.Text)


                    Dim da As New SqlDataAdapter(sqlComm)
                    Dim ds1 As New DataSet
                    da.Fill(ds1)

                    If ds1.Tables(0).Rows.Count < 1 Then
                        cbExist.JSProperties("cpResultSubmit") = "N"
                        Call DisplayMessage(MsgTypeEnum.Warning, "Please Upload Document Attachment First", cbExist)
                        Exit Sub
                    End If
                End Using



                Try
                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()

                        Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                            If IsNothing(Session("C030_RFQSet")) = False Then
                                ls_RFQSet = Session("C030_RFQSet").ToString()
                            End If

                            ls_SQL = "EXEC sp_Quotation_Update "
                            ls_SQL = ls_SQL + "'1','1','0'," & _
                                              "'" & txtQuotationNo.Text & "','" & txtRev.Text & "','" & ls_RFQSet & "','" & cboRFQNumber.Text & "','" & txtSupplierCode.Text & "'," & _
                                              "'" & txtRFQTitle.Text & "','" & Format(CDate(dtQuotationDate.Value), "yyyy-MM-dd") & "','" & memoNote.Text & "','0','" & UserLogin & "'"

                            Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()

                            sqlTran.Commit()
                        End Using
                    End Using

                    cbExist.JSProperties("cpResultSubmit") = "Y"
                    Call SetInformation(cbExist)

                    Call DisplayMessage(MsgTypeEnum.Success, "Data submitted successfully!", cbExist)

                Catch ex As Exception

                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)

                End Try

        End Select

    End Sub

    Private Sub cboRFQNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboRFQNumber.Callback
        Call FillComboRFQNumber()
        If cboRFQNumber.Items.Count > 1 Then
            'If there are more than 1 data, then set the default selected index (ALL)
            If Request.QueryString.Count > 0 Then
                cboRFQNumber.SelectedIndex = 0
            End If
        End If
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Try
            If e.UpdateValues.Count > 0 Then
                Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
                Dim idx As Integer = 0

                Dim lb_IsDataExist As Boolean = IsDataExist()

                If lb_IsDataExist = False Then
                    If e.UpdateValues.Count <> Grid.VisibleRowCount Then
                        Session("C030_ErrMsg") = "Please fill all of rows on the grid before save!"
                        Exit Sub
                    End If
                End If

                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()

                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                        If IsNothing(Session("C030_RFQSet")) = False Then
                            ls_RFQSet = Session("C030_RFQSet").ToString()
                        End If


                        '#HEADER
                        If lb_IsDataExist = False Then
                            ls_SQL = "EXEC sp_Quotation_Insert_Header " & _
                                     "'" & txtQuotationNo.Text & "','" & txtRev.Text & "','" & ls_RFQSet & "','" & cboRFQNumber.Text & "','" & txtSupplierCode.Text & "'," & _
                                     "'" & txtRFQTitle.Text & "','" & Format(CDate(dtQuotationDate.Value), "yyyy-MM-dd") & "','" & memoNote.Text & "','0','" & UserLogin & "'"
                        Else
                            ls_SQL = "EXEC sp_Quotation_Update_Header " & _
                                     "'0'," & _
                                     "'" & txtQuotationNo.Text & "','" & txtRev.Text & "','" & ls_RFQSet & "','" & cboRFQNumber.Text & "','" & txtSupplierCode.Text & "'," & _
                                     "'" & txtRFQTitle.Text & "','" & Format(CDate(dtQuotationDate.Value), "yyyy-MM-dd") & "','" & memoNote.Text & "','0','" & UserLogin & "'"
                        End If
                        Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                        sqlComm.CommandType = CommandType.Text
                        sqlComm.ExecuteNonQuery()
                        sqlComm.Dispose()


                        '#DETAIL
                        For idx = 0 To e.UpdateValues.Count - 1
                            If lb_IsDataExist = False Then
                                ls_SQL = "EXEC sp_Quotation_Insert_Detail " & _
                                          "'" & txtQuotationNo.Text & "','" & txtRev.Text & "','" & cboRFQNumber.Text & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Material_No").ToString() & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Qty").ToString() & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Currency").ToString() & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Price").ToString() & "'," & _
                                          "'" & UserLogin & "'"
                            Else
                                ls_SQL = "EXEC sp_Quotation_Update_Detail " & _
                                          "'0'," & _
                                          "'" & txtQuotationNo.Text & "','" & txtRev.Text & "','" & cboRFQNumber.Text & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Material_No").ToString() & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Qty").ToString() & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Currency").ToString() & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Price").ToString() & "'," & _
                                          "'" & UserLogin & "'"
                            End If

                            sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()
                        Next idx
                        'If lb_IsDataExist = False Then
                        '    'Response.Redirect("SupplierQuotation.aspx")
                        '    Call SetInformation(Grid)
                        '    Call DisplayMessage(MsgTypeEnum.Success, "Data save successfully!", Grid)
                        'Else
                        '    Call DisplayMessage(MsgTypeEnum.ErrorMsg, "error aja!", Grid)
                        'End If


                        ''#ATTACHMENT
                        'If lb_IsDataExist = True Then
                        '    ls_SQL = "EXEC sp_Quotation_Update_Attachment '" & txtRev.Text & "','" & cboRFQNumber.Text & "'"
                        '    sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                        '    sqlComm.CommandType = CommandType.Text
                        '    sqlComm.ExecuteNonQuery()
                        '    sqlComm.Dispose()
                        'End If

                        sqlTran.Commit()
                    End Using
                End Using


                ''INSERT REMAINING
                'Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                '    sqlConn.Open()

                '    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                '        ls_SQL = "EXEC sp_Quotation_Insert_Remaining '" & cboRFQNumber.Text & "', '" & txtRev.Text & "', '" & txtSupplierCode.Text & "'"
                '        Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                '        sqlComm.CommandType = CommandType.Text
                '        sqlComm.ExecuteNonQuery()
                '        sqlComm.Dispose()

                '        sqlTran.Commit()
                '    End Using
                'End Using
            End If

        Catch ex As Exception
            Session("C030_ErrMsg") = Err.Description
        End Try
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "Price" Then
            If IsDataSubmitted() = True Then
                'Non-Editable
                e.Cell.BackColor = Color.LemonChiffon
            Else
                'Editable
                e.Cell.BackColor = Color.White
            End If
        ElseIf e.DataColumn.FieldName = "Material_No" Then
            e.Cell.BackColor = Color.AliceBlue
            '    Dim contentUrl As String = String.Format("{0}", e.GetValue("Material_No"))
            '    Session("MaterialNo") = e.GetValue("Material_No")
            '    'Link.ClientSideEvents.Click = String.Format("function(s, e) {{ OnMoreInfoClick('{0}'); }}", contentUrl)
            '    'e.Cell.Attributes.Add("onmouseover", String.Format("OnMoreInfoClick({0})", "'" & contentUrl & "'"))
            '    e.Cell.Attributes.Add("onmouseout", String.Format("OnMouseLeave({0})", "'" & contentUrl & "'"))
            '    e.Cell.Attributes.Add("onclick", String.Format("OnMoreInfoClick({0})", "'" & contentUrl & "'"))

        Else
            'Non-Editable
            e.Cell.BackColor = Color.LemonChiffon
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Select Case e.Parameters
            Case "load"
                Call GridLoad()
                Grid.JSProperties("cpActionAfter") = "gridload"

            Case "save"
                If IsNothing(Session("C030_ErrMsg")) = False Then
                    If Session("C030_ErrMsg") = "Please fill all of rows on the grid before save!" Then
                        Grid.JSProperties("cpCompleteInput") = "N"
                        GoTo endCase
                    End If
                End If
                Call GridLoad()
                Grid.JSProperties("cpActionAfter") = "save"
                Grid.JSProperties("cpCompleteInput") = "Y"
                Call SetInformation(Grid)
                Call DisplayMessage(MsgTypeEnum.Success, "Data save successfully!", Grid)


        End Select

endCase:
        If IsNothing(Session("C030_ErrMsg")) = False Then
            Call DisplayMessage(MsgTypeEnum.ErrorMsg, Session("C030_ErrMsg"), Grid)
            Session.Remove("C030_ErrMsg")
        End If
    End Sub

    Private Sub GridAtc_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles GridAtc.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub GridAtc_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridAtc.CustomCallback
        Call GridLoadAttachment()
    End Sub

    Private Sub GridAtc_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridAtc.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "FileType" Or e.DataColumn.FieldName = "FileName" Then

            'Editable
            e.Cell.BackColor = Color.LemonChiffon
        Else
            'Non-Editable
            e.Cell.BackColor = Color.White
        End If
    End Sub

    Private Sub ucAtc_FileUploadComplete(sender As Object, e As DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs) Handles ucAtc.FileUploadComplete
        If e.IsValid = False Then
            ucAtc.JSProperties("cpType") = "3"
            ucAtc.JSProperties("cpMessage") = "Invalid file!"
        Else
            Dim QuotationNo As String = hf.Get("QuotationNo")
            QuotationNo = Replace(QuotationNo, "/", "")
            QuotationNo = Replace(QuotationNo, "~", "")
            QuotationNo = Replace(QuotationNo, "\", "")
            QuotationNo = Replace(QuotationNo, " ", "")
            QuotationNo = Regex.Replace(QuotationNo, "[\[\]\\\^\$\.:\|\?\*\+\(\)\{\}%,;><!@#&\-\+/]", "")


            Dim serverPath As String = Server.MapPath("~/Attachments/")
            Dim fileName As String = QuotationNo & "_" & Format(CDate(Now), "yyyyMMdd_hhmmss") & ".pdf" 'e.UploadedFile.FileName
            Dim fullPath As String = String.Format("{0}{1}", serverPath, fileName)
            Dim SizeFile As Single = e.UploadedFile.ContentLength / 1024 / 1024

            Try
                Using conn As New SqlConnection(Sconn.Stringkoneksi)
                    conn.Open()
                    Dim Sql = "Select sum(FileSize) FileSize from Quotation_Attachment where Quotation_No='" & hf.Get("QuotationNo") & "' Group By Quotation_No"
                    Dim cmd As New SqlCommand(Sql, conn)
                    cmd.CommandType = CommandType.Text
                    Dim da As New SqlDataAdapter(cmd)
                    Dim ds As New DataSet
                    da.Fill(ds)


                    If SizeFile > 2 Then
                        ucAtc.JSProperties("cpType") = "3"
                        ucAtc.JSProperties("cpMessage") = "File size must not exceed 2 MB"
                        Exit Sub
                    End If
                    If ds.Tables(0).Rows.Count > 0 Then
                        '    FileSizeDB = 10
                        '    If SizeFile > FileSizeDB Then
                        '        ucAtc.JSProperties("cpType") = "3"
                        '        ucAtc.JSProperties("cpMessage") = "File size limit max 10 MB"
                        '        Exit Sub
                        '    End If
                        'Else
                        Dim FileSizeDB = ds.Tables(0).Rows(0)("FileSize")
                        If SizeFile > (10 - FileSizeDB) Then
                            ucAtc.JSProperties("cpType") = "3"
                            ucAtc.JSProperties("cpMessage") = "File size limit max 10 MB, remain size (" & Math.Round(10 - FileSizeDB, 5) & ") MB"
                            Exit Sub
                        End If
                    End If


                End Using
            Catch ex As Exception
                'ucAtc.JSProperties("cpType") = "3"
                'ucAtc.JSProperties("cpMessage") = ex.Message
            End Try


            Try
                'CHECK EXISTING FOLDER (Attachment)
                If (Not System.IO.Directory.Exists(serverPath)) Then
                    System.IO.Directory.CreateDirectory(serverPath)
                End If


                'SAVE ATTACHMENT
                e.UploadedFile.SaveAs(fullPath)

                ucAtc.JSProperties("cpType") = "1"
                ucAtc.JSProperties("cpMessage") = "Quotation " & e.UploadedFile.FileName & " has been uploaded successfully!"

                Call SaveAttachment(hf.Get("QuotationNo"), hf.Get("RevisionNo"), hf.Get("RFQNumber"), e.UploadedFile.FileName, "~/Attachments/" & fileName, SizeFile)

            Catch ex As Exception
                ucAtc.JSProperties("cpType") = "3"
                ucAtc.JSProperties("cpMessage") = Err.Description
            End Try
        End If
    End Sub

    Protected Sub keyFieldLink_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim link As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
        Dim container As GridViewDataItemTemplateContainer = TryCast(link.NamingContainer, GridViewDataItemTemplateContainer)
        'link.ClientSideEvents.Click = String.Format("function(s, e) {{ OnMoreInfoClick(" & container.Grid.ClientInstanceName & ", '{0}'); }}")

        If container.ItemIndex >= 0 Then
            link.Text = Split(container.KeyValue, "|")(0)
            link.Target = "_blank"
            link.NavigateUrl = Split(container.KeyValue, "|")(1)
        End If
    End Sub


    'untuk popup image
    Protected Sub callbackPanel_Callback(ByVal source As Object, ByVal e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase)
        Dim ItemCode As String = Session("MaterialNo") 'Request.QueryString("ID") & ""
        Dim matno As String = e.Parameter 'Request.QueryString("ID") & ""
        edBinaryImage.Value = FindImage(matno, "ImageBinary")
    End Sub

    Private Function FindImage(ByVal MaterialNo As String, ByVal field As String) As Byte()
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String

                sql = "SELECT " & field & " FROM dbo.MstItem_Attachment WHERE " & field & " IS NOT NULL AND Material_No = '" & MaterialNo & "' "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds.Tables(0).Rows(0).Item("" & field & "")
                Else
                    Return Nothing
                End If

            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    'ini jika pake event click untuk show image ..
    Protected Sub hyperLink_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)

        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        'Dim rowVisibleIndex As Integer = templateContainer.VisibleIndex
        'Dim MatNo As String = templateContainer.Grid.GetRowValues(rowVisibleIndex, "Material_No").ToString()
        Dim contentUrl As String = String.Format("{0}", templateContainer.KeyValue)
        Session("MaterialNo") = templateContainer.KeyValue
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ OnMoreInfoClick('{0}'); }}", contentUrl) 
    End Sub



#End Region



End Class
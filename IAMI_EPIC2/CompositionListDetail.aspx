﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CompositionListDetail.aspx.vb" Inherits="IAMI_EPIC2.CompositionListDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }
   
   

   

</script>
<style type="text/css">
    .hidden-div
    {
        display: none;
    }
        
    .tr-all-height
    {
        height: 30px;
    }
        
    .td-col-left
    {
        width: 100px;
        padding: 0px 0px 0px 10px;
    }
        
    .td-col-left2
    {
        width: 140px;
    }
        
        
    .td-col-mid
    {
        width: 10px;
    }
    .td-col-right
    {
        width: 200px;
    }
    .td-col-free
    {
        width: 20px;
    }
        
    .div-pad-fil
    {
        padding: 5px 5px 5px 5px;
    }
        
    .td-margin
    {
        padding: 10px;
    }
    .style1
    {
        width: 118px;
        padding: 0px 0px 0px 10px;
    }
    .style2
    {
        width: 100px;
        padding: 0px 0px 0px 10px;
        height: 30px;
    }
    .style3
    {
        width: 10px;
        height: 30px;
    }
    .style4
    {
        width: 200px;
        height: 30px;
    }
    .style5
    {
        width: 20px;
        height: 30px;
    }
    .style6
    {
        width: 118px;
        padding: 0px 0px 0px 10px;
        height: 30px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">  
            <tr style="height:20px">
                <td class="style1"></td>
                <td class="td-col-mid"></td>
                <td class="td-col-right"></td>

                <td class="td-col-free"></td>
                <td class="td-col-left"></td>
                <td class="td-col-mid"></td>
                <td class="td-col-right"></td>

                <td class="td-col-free"></td>
                <td class="td-col-left"></td>
                <td class="td-col-mid"></td>
                <td class="td-col-right"></td>

            </tr>      
            <tr class="tr-all-height">
                <td class="style6">
            <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Text="Parent Item Code">
            </dx1:ASPxLabel>
                </td>
                <td class="style3"></td>
                <td class="style4">
           <dx1:aspxcombobox ID="cboParentItem" runat="server" ClientInstanceName="cboParentItem"
                            Width="150px" Font-Names="Segoe UI" 
                            DataSourceID="SqlDataSource1" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" Enabled=false 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px"> 
                            <ClientSideEvents SelectedIndexChanged="ParentItemCode" />           
                            <Columns>            
                            <dx1:listboxcolumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx1:listboxcolumn Caption="Description" FieldName="Description" Width="120px" />
                             </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:aspxcombobox>
                                                
                </td>
                <td class="style5"></td>
                <td class="style2">
                    </td>
                <td class="style3"></td>
                <td class="style4">
           
                    </td>                
                <td class="style5"></td>   
                <td class="style2">
                    </td>    
                <td class="style3"></td>    
                <td class="style4">
                    </td>                     
            </tr> 
            <tr class="tr-all-height">
                <td class="style1">
            <dx1:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Parent Item Qty">
            </dx1:ASPxLabel>
                </td>
                <td class="td-col-mid"></td>
                <td class="td-col-right">
          
        <dx1:ASPxTextBox ID="txtParentItemQty" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                Width="130px" ClientInstanceName="txtParentItemQty" MaxLength="15" ReadOnly="True" 
                >            
            <MaskSettings IncludeLiterals="DecimalSymbol" Mask="<0..99999g>" />            
            </dx1:ASPxTextBox>
          
                </td>
                <td class="td-col-free"></td>                
                <td class="td-col-left">
                    &nbsp;</td>
                <td class="td-col-mid"></td>
                <td class="td-col-right">
           <div class="hidden-div">
                    <dx1:ASPxTextBox ID="txtParent" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="150px" ClientInstanceName="txtParent" MaxLength="60" 
                                Height="25px" Text="--NEW--" BackColor="#CCCCCC" ReadOnly="True" >
                    </dx1:ASPxTextBox>
           </div>
                </td>
                <td colspan="4"></td>
                
            </tr>              
            <tr class="tr-all-height">
                <td class="style1">
            <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Text="Parent Item UOM">
            </dx1:ASPxLabel>
                </td>
                <td class="td-col-mid">&nbsp;</td>
                <td class="td-col-right">
          
           <dx1:aspxcombobox ID="cboParentItemUOM" runat="server" ClientInstanceName="cboParentItemUOM"
                            Width="150px" Font-Names="Segoe UI" 
                            DataSourceID="SqlDataSource2" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" Enabled=false 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">   
                            <ClientSideEvents SelectedIndexChanged="ParentItemUOM" />            
                            <Columns>            
                            <dx1:listboxcolumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx1:listboxcolumn Caption="Description" FieldName="Description" Width="120px" />
                             </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:aspxcombobox>
                                                
                </td>
                <td class="td-col-free">&nbsp;</td>                
                <td class="td-col-left">
                    &nbsp;</td>
                <td class="td-col-mid">&nbsp;</td>
                <td class="td-col-right">
            <div class="hidden-div">
                    <dx1:ASPxTextBox ID="txtPRNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="150px" ClientInstanceName="txtPRNo" MaxLength="60" 
                                Height="25px" Text="--NEW--" BackColor="#CCCCCC" ReadOnly="True" >
                    </dx1:ASPxTextBox>
           </div>
                </td>
                <td colspan="4">&nbsp;</td>
                
            </tr>              
            <tr style="height:20px" >
                <td colspan="11" >
                    
                </td>
             
            </tr>    
        </table>
    </div>
    <div class="hidden-div" >
        <dx:ASPxTextBox ID="txtFilter" runat="server" Width="60px" ClientInstanceName="txtFilter">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="txtFilter2" runat="server" Width="100px" ClientInstanceName="txtFilter2">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="txtNewPRNo" runat="server" Width="60px" 
            ClientInstanceName="txtNewPRNo">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="tmpDepartment" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                        Width="100px" ClientInstanceName="tmpDepartment" MaxLength="15" 
                        Theme="Office2010Silver">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="tmpCostCenter" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                        Width="100px" ClientInstanceName="tmpCostCenter" MaxLength="15" 
                        Theme="Office2010Silver">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="tmpSection" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                        Width="100px" ClientInstanceName="tmpSection" MaxLength="15" 
                        Theme="Office2010Silver">
        </dx:ASPxTextBox>
    </div>


    <div style="padding: 5px 5px 5px 5px">
                    <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
                    
                </dx:ASPxCallback>
                <asp:SqlDataSource ID="SqlDataSource6" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'MaterialCode' and ([Par_ParentCode] = @Par_ParentCode)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtParent" Name="Par_ParentCode" 
                            PropertyName="Text" Type="String" />
                    </SelectParameters>
                    </asp:SqlDataSource>
                     <asp:SqlDataSource ID="SqlDataSource3" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Currency'">
                    
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'UOM'">
                    
                    </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'UOM'"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="Select Rtrim(Par_Code) AS Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialCategory'"></asp:SqlDataSource>
                    <dx:ASPxGridView runat="server" ClientInstanceName="Grid" 
                        KeyFieldName="ChildCompositionSequenceNo" AutoGenerateColumns="False" Theme="Office2010Black" 
                        Width="100%" EnableTheming="True" Font-Names="Segoe UI" Font-Size="9pt" 
                        ID="Grid" OnRowValidating="Grid_RowValidating" 
                        OnStartRowEditing="Grid_StartRowEditing" 
                        OnRowInserting="Grid_RowInserting" 
                        OnRowDeleting="Grid_RowDeleting" 
                        OnAfterPerformCallback="Grid_AfterPerformCallback">
                        <ClientSideEvents EndCallback="OnEndCallback" />
                        <Columns>
                            <dx:GridViewCommandColumn ShowEditButton="True" ShowClearFilterButton="True" 
                                ShowInCustomizationForm="True" Width="100px" VisibleIndex="0" 
                                ShowDeleteButton="True" ShowNewButtonInHeader="True">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Child Item Code" FieldName="ChildItemCode" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource6" TextField="Description" 
                                                ValueField="ID" Width="145px" TextFormatString="{1}" ClientInstanceName="ChildItemCode"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="ID" Caption="ID" Width="60px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="150px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                       
                                        </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataComboBoxColumn FieldName="CurrencyCode" 
                                ShowInCustomizationForm="True" Width="150px" Caption="Currency" 
                                VisibleIndex="4">
                                <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="Description" 
                                                ValueField="ID" Width="145px" TextFormatString="{1}" ClientInstanceName="ChildItemCode"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="ID" Caption="ID" Width="60px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="150px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                       
                                        </dx:GridViewDataComboBoxColumn>
                              
                                 <dx:GridViewDataComboBoxColumn FieldName="ChildItemUOM" 
                                ShowInCustomizationForm="True" Width="150px" Caption="Child Item UOM" 
                                VisibleIndex="5">
                                <PropertiesComboBox DataSourceID="SqlDataSource4" TextField="Description" 
                                                ValueField="ID" Width="145px" TextFormatString="{1}" ClientInstanceName="ChildItemUOM"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="ID" Caption="ID" Width="60px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="150px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                       
                                        </dx:GridViewDataComboBoxColumn>

                            <dx:GridViewDataTextColumn FieldName="ChildItemQty" 
                                ShowInCustomizationForm="True" Width="150px" Caption="Child Item Qty" 
                                VisibleIndex="6">

                                 <PropertiesTextEdit  ClientInstanceName="ChildItemQty" DisplayFormatString="###" >
                        <MaskSettings AllowMouseWheel="False" Mask="<0..99999g>" IncludeLiterals="DecimalSymbol" ></MaskSettings>
                     </PropertiesTextEdit>

                               
                                <Settings AutoFilterCondition="Contains">
                                </Settings>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px">
                                </Paddings>
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>

                              <dx:GridViewDataTextColumn FieldName="Price" 
                                ShowInCustomizationForm="True" Width="150px" Caption="Price" 
                                VisibleIndex="7">
                                     
                                 <PropertiesTextEdit  ClientInstanceName="Price" DisplayFormatString="###,###" >
                                
                        <MaskSettings AllowMouseWheel="False" Mask="<0..99999g>" IncludeLiterals="DecimalSymbol" ></MaskSettings>
                     </PropertiesTextEdit>
                      <CellStyle HorizontalAlign="Right"></CellStyle>

                               
                                <Settings AutoFilterCondition="Contains">
                                </Settings>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px">
                                </Paddings>
                                </HeaderStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CreateUser" 
                                ShowInCustomizationForm="True" Width="150px" Caption="Register By" 
                                VisibleIndex="8">
                                <PropertiesTextEdit Width="150px" ClientInstanceName="CreateUser">
                                </PropertiesTextEdit>
                                <Settings AllowAutoFilter="False">
                                </Settings>
                                <EditFormSettings Visible="False">
                                </EditFormSettings>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px">
                                </Paddings>
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn FieldName="CreateDate" 
                                ShowInCustomizationForm="True" Width="100px" Caption="Register Date" 
                                VisibleIndex="9">
                                <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" 
                                    EditFormatString="dd MMM yyyy" EditFormat="Custom">
                                </PropertiesDateEdit>
                                <Settings AllowAutoFilter="False">
                                </Settings>
                                <EditFormSettings Visible="False">
                                </EditFormSettings>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px">
                                </Paddings>
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                </CellStyle>
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn FieldName="UpdateUser" ShowInCustomizationForm="True" 
                                Width="150px" Caption="Update By" VisibleIndex="10">
                                <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateUser">
                                </PropertiesTextEdit>
                                <Settings AllowAutoFilter="False">
                                </Settings>
                                <EditFormSettings Visible="False">
                                </EditFormSettings>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px">
                                </Paddings>
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn FieldName="UpdateDate" 
                                ShowInCustomizationForm="True" Width="100px" Caption="Update Date" 
                                VisibleIndex="11">
                                <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" 
                                    EditFormatString="dd MMM yyyy" EditFormat="Custom">
                                </PropertiesDateEdit>
                                <Settings AllowAutoFilter="False">
                                </Settings>
                                <EditFormSettings Visible="False">
                                </EditFormSettings>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px">
                                </Paddings>
                                </HeaderStyle>
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                </CellStyle>
                            </dx:GridViewDataDateColumn>
                        </Columns>
                        <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True">
                        </SettingsBehavior>
                        <SettingsPager AlwaysShowPager="True">
                        </SettingsPager>
                        <SettingsEditing Mode="PopupEditForm">
                        </SettingsEditing>
                        <Settings ShowFilterRow="True" VerticalScrollableHeight="260" 
                            HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto">
                        </Settings>
                        <SettingsText ConfirmDelete="Are you sure want to delete ?">
                        </SettingsText>
                        <SettingsPopup>
                            <EditForm Width="320px" HorizontalAlign="WindowCenter" 
                                VerticalAlign="WindowCenter">
                            </EditForm>
                        </SettingsPopup>
                        <Styles>
                            <Header>
                                <Paddings Padding="2px">
                                </Paddings>
                            </Header>
                            <CommandColumnItem ForeColor="Orange">
                            </CommandColumnItem>
                            <EditFormColumnCaption Font-Names="Segoe UI" Font-Size="9pt">
                                <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingRight="15px" 
                                    PaddingBottom="5px">
                                </Paddings>
                            </EditFormColumnCaption>
                        </Styles>
                        <Templates>
                            <EditForm>
                                <div style="padding: 15px 15px 15px 15px">
                                    <dx:ContentControl ID="ContentControl1" runat="server">
                                        <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                        </dx:ASPxGridViewTemplateReplacement>
                                    </dx:ContentControl>
                                </div>
                                <div style="text-align: left; padding: 5px 5px 5px 15px">
                                    <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                    </dx:ASPxGridViewTemplateReplacement>
                                    <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                    </dx:ASPxGridViewTemplateReplacement>
                                </div>
                            </EditForm>
                        </Templates>
                    </dx:ASPxGridView>
    </div>

    <div style="padding: 5px 5px 5px 5px">
    <table style="width: 100%; border: 0px"> 
                        
            <tr style="display:none">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr style="height: 2px">
                <td style=" padding:0px 0px 0px 10px" colspan="3">
               

                    &nbsp;&nbsp;
                    <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>

                    &nbsp;&nbsp;

                    
                    </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
    </table>
    </div>
</div>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="ItemList.aspx.vb" Inherits="IAMI_EPIC2.ItemList" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function MessageError(s, e) {
            //alert(s.cpmessage);
            if (s.cpmessage == "Download Excel Successfully") {
                toastr.success(s.cpmessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpmessage == null || s.cpmessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpmessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function ItemCodeValidation(s, e) {
            if (ItemCode.GetValue() == null) {
                e.isValid = false;
            }
        }

        function GridHeader(s, e) {
            Grid.PerformCallback('gridheader|' + cboGroupItem.GetSelectedItem().GetColumnText(0) + '|' + cboCategory.GetSelectedItem().GetColumnText(0) + '|' + cboPRType.GetSelectedItem().GetColumnText(0) + '|' + cboLastSupplier.GetText());
        }

        function OnComboBoxKeyDown(s, e) {
            // 'Delete' button key code = 46

            if (e.htmlEvent.keyCode == 46 || e.htmlEvent.keyCode == 8)
                s.SetSelectedIndex(-1);
            //alert(s.GetValue());
        }


    </script>
    <style type="text/css">
        .style1
        {
            width: 82px;
        }
        .style5
        {
            height: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding: 5px 5px 5px 5px">
            <%--         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%"
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="ItemCode" DataSourceID="SqlDataSource1">--%>
            <table style="width: 100%; border: 1px solid black" >
                <tr style="height: 20px">
                    <td style="width:100px">
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td width="200px">
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td align="left" width="100px">
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td style="width: 200px">
                        &nbsp;
                    </td>
                    <td >
                        &nbsp;
                    </td>
                    <td style="width: 600px">
                        &nbsp;
                    </td>
                </tr>
                <tr style="height: 10px" >
                    <td width="160px" style="padding: 0px 0px 0px 10px" class="style1">
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Group Item">
                        </dx1:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td>
                        <dx1:ASPxComboBox ID="cboGroupItem" runat="server" ClientInstanceName="cboGroupItem"
                            Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Group_Name"
                            ValueField="Group_Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            DropDownStyle="DropDownList" IncrementalFilteringMode="Contains" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboCategory.PerformCallback('filter|' + cboGroupItem.GetSelectedItem().GetColumnText(0));
	                            //Grid.PerformCallback('gridheader|' + cboGroupItem.GetSelectedItem().GetColumnText(0) + '|' + cboCategory.GetSelectedItem().GetColumnText(0) + '|' + cboPRType.GetSelectedItem().GetColumnText(0) + '|' + cboLastSupplier.GetText());
                            }" KeyDown="OnComboBoxKeyDown" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Group Code" FieldName="Group_Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Group Name" FieldName="Group_Name" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td align="left" style="width: 100px" >
                        <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="PR Type">
                        </dx1:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="width: 10px">
                        <dx1:ASPxComboBox ID="cboPRType" runat="server" ClientInstanceName="cboPRType" Width="120px"
                            Font-Names="Segoe UI" DataSourceID="SqlDataSource3" TextField="Description" ValueField="Code"
                            TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDownList"
                            IncrementalFilteringMode="Contains" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="GridHeader"  KeyDown="OnComboBoxKeyDown" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="Select 'ALL' As Code, 'ALL' As Description Union ALL Select Supplier_Code As Code, Supplier_Name As Description From Mst_Supplier">
                        </asp:SqlDataSource>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    
                </tr>
                <tr style="height: 40px">
                    <td style="padding: 0px 0px 0px 10px" class="style1">
                        <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Category Item">
                        </dx1:ASPxLabel>
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td>
                        <dx1:ASPxComboBox ID="cboCategory" runat="server" ClientInstanceName="cboCategory"
                            Width="120px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDownList"
                            IncrementalFilteringMode="Contains" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="GridHeader" Init="function(s, e) {
	                                cboCategory.PerformCallback('load|' + '00');
}" KeyDown="OnComboBoxKeyDown" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td align="left">
                        <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Last Supplier">
                        </dx1:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="width: 10px">
                        <dx1:ASPxComboBox ID="cboLastSupplier" runat="server" ClientInstanceName="cboLastSupplier"
                            Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource2" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            DropDownStyle="DropDownList" IncrementalFilteringMode="Contains" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="GridHeader" KeyDown="OnComboBoxKeyDown" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td>
                        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="Select '00' As Code, 'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRType'">
                        </asp:SqlDataSource>
                    </td>
                     <td>
                        <dx:ASPxCallback ID="cbExcel" runat="server" ClientInstanceName="cbExcel">
                            <ClientSideEvents Init="MessageError" />
                        </dx:ASPxCallback>
                    </td>
                   
                </tr>
                <tr style="height: 8px">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        
                        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                   
                    </td>
                    <td>
                    </td>
                    <td align="left" class="style5">
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td>
                        <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                            <ClientSideEvents Init="MessageError" />
                        </dx:ASPxCallback>
                    </td>
                    <td>
                        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="Select '00' As Code, 'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Category'">
                        </asp:SqlDataSource>
                    </td>
                    <td>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                        SelectCommand="Select '00' As Group_Code, 'ALL' Group_Name UNION ALL Select RTRIM(Par_Code) As Group_Code, RTRIM(Par_Description) As Group_Name From Mst_Parameter Where Par_Group = 'GroupItem'">
                    </asp:SqlDataSource>
                    </td>
                </tr>
                <tr style="height: 25px">
                    <td style="padding: 0px 0px 0px 10px" class="style1" colspan="9">
                        <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnRefresh" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
                            
                            //alert(cboGroupItem.GetSelectedIndex());
                            if (cboGroupItem.GetSelectedIndex() &lt; 0) {
                                toastr.warning('Please select Group Item', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;                            
                            }
                            else if (cboCategory.GetSelectedIndex() &lt; 0) {
                                toastr.warning('Please select Category', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;                            
                            }
                            else if (cboPRType.GetSelectedIndex() &lt; 0) {
                                toastr.warning('Please select PR Type', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;                            
                            }
                            else if (cboLastSupplier.GetSelectedIndex() &lt; 0) {
                                toastr.warning('Please select Last Supplier', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;                            
                            }
                       
	                        Grid.PerformCallback('gridload|' + cboGroupItem.GetSelectedItem().GetColumnText(0) + '|' + cboCategory.GetSelectedItem().GetColumnText(0) + '|' + cboPRType.GetSelectedItem().GetColumnText(0) + '|' + cboLastSupplier.GetText());
                        }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;<dx:ASPxButton ID="BtnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="BtnDownload" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
	                            cbExcel.PerformCallback();
                            }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;
                        <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="false" Font-Names="Segoe UI"
                            Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnAdd" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                </tr>
                <tr style="height: 0px">
                    <td style="padding: 0px 0px 0px 10px" class="style1" colspan="9">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Size="9pt" Font-Names="Segoe UI"
                KeyFieldName="ItemCode">
                <%--              <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm"/>
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
            </SettingsPopup>--%>
                <%--             <dx:GridViewCommandColumn FixedStyle="Left"
                        VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                        ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                        <HeaderStyle Paddings-PaddingLeft="3px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" >
                            <Paddings PaddingLeft="3px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewCommandColumn>--%>
                <ClientSideEvents CustomButtonClick="function(s, e) {            
                            if(e.buttonID == 'edit'){
                     
                                var rowKey = Grid.GetRowKey(e.visibleIndex);
                             
	                            window.location.href= 'AddItemMaster.aspx?ID=' + rowKey;
                            }
}"></ClientSideEvents>
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                    HorizontalScrollBarMode="Auto" />
                <ClientSideEvents CustomButtonClick="function(s, e) {            
                            if(e.buttonID == 'edit'){
                     
                                var rowKey = Grid.GetRowKey(e.visibleIndex);
                             
	                            window.location.href= 'AddItemMaster.aspx?ID=' + rowKey;
                            }
            }" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Material Code" FieldName="ItemCode" VisibleIndex="5"
                        Settings-AutoFilterCondition="Contains">
                        <PropertiesTextEdit MaxLength="50" Width="90px" ClientInstanceName="ItemCode">
                            <ClientSideEvents Validation="ItemCodeValidation" />
                            <Style HorizontalAlign="Left"></Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains"></Settings>
                        <EditFormSettings VisibleIndex="1" />
                        <EditFormSettings VisibleIndex="1"></EditFormSettings>
                        <FilterCellStyle Paddings-PaddingRight="4px">
                            <Paddings PaddingRight="4px"></Paddings>
                        </FilterCellStyle>
                        <HeaderStyle Paddings-PaddingLeft="8px" HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="8px"></Paddings>
                        </HeaderStyle>
                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ItemName" Caption="Description" VisibleIndex="6"
                        Width="250px">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Specification" Caption="Specification" VisibleIndex="7"
                        Width="500px">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="GroupItem" Caption="GroupItem" VisibleIndex="9"
                        Width="150px">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="CategoryItem" Caption="CategoryItem" VisibleIndex="10"
                        Width="150px">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PRType" Caption="PRType" VisibleIndex="8" Width="150px">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LastIAPrice" Caption="Last IA Price" VisibleIndex="11">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LastSupplier" Caption="Last Supplier" Width="180px" VisibleIndex="12">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="UOM" Caption="UOM" VisibleIndex="13">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="CreateUser" Caption="Register By" VisibleIndex="17">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Register Date" FieldName="CreateDate" VisibleIndex="22"
                        Width="150px" Settings-AllowAutoFilter ="False"  > 
                       
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="UpdateUser" Caption="Update By" VisibleIndex="23">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="UpdateDate" Caption="Update Date" VisibleIndex="24"
                        Width="150px" Settings-AllowAutoFilter ="False" >
                       
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewCommandColumn VisibleIndex="4" ButtonType="Link" Caption=" " FixedStyle="Left">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                    
                </Columns>
                <SettingsBehavior ColumnResizeMode="Control" />
                <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                </SettingsPager>
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                    HorizontalScrollBarMode="Auto" />
                <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>
                    <EditFormColumnCaption>
                        <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
                    </EditFormColumnCaption>
                </Styles>
            </dx:ASPxGridView>
        </div>
    </div>
    <%--    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="SELECT ItemCode, ItemName FROM ItemMaster"></asp:SqlDataSource>--%>
</asp:Content>

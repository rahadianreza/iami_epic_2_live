﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO
Imports System.Drawing
Imports OfficeOpenXml

'Imports System.Drawing

Public Class Comparison
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fRefresh As Boolean = False
    Dim PrjID As String = ""
    Dim Group As String = ""
    Dim commodity As String = ""
    Dim Groupcommodity As String = ""
    Dim PartNo As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("L010")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            PrjID = Request.QueryString("projectid")
            Group = Request.QueryString("groupid")
            commodity = Request.QueryString("commodity")
            Groupcommodity = Request.QueryString("groupcommodity")
            PartNo = Request.QueryString("PartNo")

            FillCombo("", "", "", "", 1)
            FillCombo(PrjID, "", "", "", 2)
            FillCombo(PrjID, Group, "", "", 3)
            FillCombo(PrjID, Group, commodity, "", 4)
            FillCombo(PrjID, Group, commodity, Groupcommodity, 5)
            cboProject.Value = PrjID
            cboGroup.Value = Group
            cboCommodity.Value = commodity
            cboCommodityGroup.Value = Groupcommodity
            cboPartNo.Value = PartNo


            cboProject.ClientEnabled = False
            cboGroup.ClientEnabled = False
            cboCommodity.ClientEnabled = False
            cboCommodityGroup.ClientEnabled = False
            cboPartNo.ClientEnabled = False
        End If
    End Sub
#End Region

#Region "Control Event"
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("ComparisonList.aspx")
    End Sub
    Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)

        ds = clsComparisonDB.getProjectID(Project, "", "", "", 2, pUser)
        If pmsg = "" Then
            cboGroup.DataSource = ds
            cboGroup.DataBind()
        End If
    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)
        Dim Group As String = Split(e.Parameter, "|")(1)

        ds = clsComparisonDB.getProjectID(Project, Group, "", "", 3, pUser)
        If pmsg = "" Then
            cboCommodity.DataSource = ds
            cboCommodity.DataBind()
        End If
    End Sub
    Private Sub cboCommodityGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodityGroup.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)
        Dim Group As String = Split(e.Parameter, "|")(1)
        Dim Commodity As String = Split(e.Parameter, "|")(2)

        ds = clsComparisonDB.getProjectID(Project, Group, Commodity, "", 4, pUser)
        If pmsg = "" Then
            cboCommodityGroup.DataSource = ds
            cboCommodityGroup.DataBind()
        End If
    End Sub
    Private Sub cboPartNo_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboPartNo.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)
        Dim Group As String = Split(e.Parameter, "|")(1)
        Dim Commodity As String = Split(e.Parameter, "|")(2)
        Dim CommodityGroup As String = Split(e.Parameter, "|")(3)

        ds = clsComparisonDB.getProjectID(Project, Group, Commodity, CommodityGroup, 5, pUser)
        If pmsg = "" Then
            cboPartNo.DataSource = ds
            cboPartNo.DataBind()
        End If
    End Sub


    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
        End If

    End Sub
    Protected Sub GridTooling_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridTooling.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoadTooling(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
        End If

    End Sub
    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "Description" Then
            If e.CellValue = "PARTS" Then
                e.Cell.Font.Bold = True
            End If
        End If
        If e.DataColumn.FieldName = "Description" Then
            If e.CellValue = "Total Sub" Then
                e.Cell.Font.Bold = True
                Session("bold") = 1
            End If
        End If
        If Session("bold") = 1 Then
            e.Cell.Font.Bold = True
        End If
        'If e.DataColumn.FieldName = "Description" Then
        '    e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        'End If
    End Sub
    Private Sub GridTooling_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridTooling.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "Description" Then
            If e.CellValue = "Total" Then
                e.Cell.Font.Bold = True
                Session("boldTooling") = 1
            End If
        End If
        If Session("boldTooling") = 1 Then
            e.Cell.Font.Bold = True
        End If
    End Sub
    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim Project As String = Split(e.Parameters, "|")(1)
        Dim Group As String = Split(e.Parameters, "|")(2)
        Dim Commodity As String = Split(e.Parameters, "|")(3)
        Dim CommodityGroup As String = Split(e.Parameters, "|")(4)
        Dim PartNo As String = Split(e.Parameters, "|")(5)
        Dim pdataHeader As New List(Of clsComparison)
        Dim No As Integer = 0
        Select Case pFunction
            Case "load"
                Dim pProjectID As String = Split(e.Parameters, "|")(1)
                up_GridLoad(Project, Group, Commodity, CommodityGroup, PartNo)
                pdataHeader = clsComparisonDB.getHeaderName(Project, Group, Commodity, CommodityGroup, PartNo)
                For x = 1 To 6
                    Grid.Columns("Supplier" & x).Caption() = ""
                    Grid.Columns("Supplier" & x).Visible = False
                Next

                For Each xdata In pdataHeader
                    No = No + 1
                    Grid.Columns("Supplier" & No).Visible = True
                    Grid.Columns("Supplier" & No).Caption() = xdata.Description
                Next
        End Select
    End Sub
    Private Sub GridTooling_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridTooling.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim Project As String = Split(e.Parameters, "|")(1)
        Dim Group As String = Split(e.Parameters, "|")(2)
        Dim Commodity As String = Split(e.Parameters, "|")(3)
        Dim CommodityGroup As String = Split(e.Parameters, "|")(4)
        Dim PartNo As String = Split(e.Parameters, "|")(5)
        Dim pdataHeader As New List(Of clsComparison)
        Dim No As Integer = 0
        Select Case pFunction
            Case "load"
                Dim pProjectID As String = Split(e.Parameters, "|")(1)
                up_GridLoadTooling(Project, Group, Commodity, CommodityGroup, PartNo)
                pdataHeader = clsComparisonDB.getHeaderName(Project, Group, Commodity, CommodityGroup, PartNo)
                For x = 1 To 6
                    GridTooling.Columns("Supplier" & x).Caption() = ""
                    GridTooling.Columns("Supplier" & x).Visible = False
                Next

                For Each xdata In pdataHeader
                    No = No + 1
                    GridTooling.Columns("Supplier" & No).Visible = True
                    GridTooling.Columns("Supplier" & No).Caption() = xdata.Description
                Next
        End Select
    End Sub
    Private Sub up_Excel()
        Dim ds As New DataSet
        Dim prowheader As Integer
        Dim ErrMsg As String = ""
        Dim Ses As New List(Of clsComparison)
        Dim SesT As New List(Of clsComparison)
        Dim pdataHeader As New List(Of clsComparison)
        Ses = clsComparisonDB.getlist(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value, ErrMsg)
        pdataHeader = clsComparisonDB.getHeaderName(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)

        prowheader = pdataHeader.Count
        If ErrMsg <> "" Then
            Exit Sub
        End If
        SesT = clsComparisonDB.getlistTooling(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value, ErrMsg)

        Using Pck As New ExcelPackage
            Dim ws As ExcelWorksheet = Pck.Workbook.Worksheets.Add("Parts")
            With ws
                Dim irowunit As Integer = 0
                .Cells(1, 1, 1, 1).Value = "Description"
                .Cells(1, 2, 1, 2).Value = "Guide Price"
                For Each xdata In pdataHeader
                    .Cells(1, 3 + irowunit, 1, 3 + irowunit).Value = xdata.Description
                    irowunit = irowunit + 1
                Next

                irowunit = 1
                For Each Parts In Ses
                    Dim r As Integer = irowunit + 1
                    .Cells(r, 1, r, 1).Value = Parts.Description
                    .Cells(r, 2, r, 2).Value = IIf(Parts.GuidePrice = 0, "", Parts.GuidePrice)
                    If prowheader >= 1 Then .Cells(r, 3, r, 3).Value = IIf(Parts.Supplier1 = 0, "", Parts.Supplier1)
                    If prowheader >= 2 Then .Cells(r, 4, r, 4).Value = IIf(Parts.Supplier2 = 0, "", Parts.Supplier2)
                    If prowheader >= 3 Then .Cells(r, 5, r, 5).Value = IIf(Parts.Supplier3 = 0, "", Parts.Supplier3)
                    If prowheader >= 4 Then .Cells(r, 6, r, 6).Value = IIf(Parts.Supplier4 = 0, "", Parts.Supplier4)
                    If prowheader >= 5 Then .Cells(r, 7, r, 7).Value = IIf(Parts.Supplier5 = 0, "", Parts.Supplier5)
                    If prowheader >= 6 Then .Cells(r, 8, r, 8).Value = IIf(Parts.Supplier6 = 0, "", Parts.Supplier6)
                    irowunit = irowunit + 1
                Next
                .Column(1).Width = 23
                .Column(2).Width = 20
                .Column(3).Width = 20
                .Column(4).Width = 20
                .Column(5).Width = 20
                .Column(6).Width = 20
                .Column(7).Width = 20
                .Column(8).Width = 20

                Dim rg As OfficeOpenXml.ExcelRange = .Cells(1, 1, irowunit, 2 + prowheader)
                rg.Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Font.Name = "Tahoma"
                rg.Style.Font.Size = 10

                Dim rf As OfficeOpenXml.ExcelRange = .Cells(2, 2, irowunit, 2 + prowheader)
                rf.Style.Numberformat.Format = "#,##0"

                Dim rgHeader As OfficeOpenXml.ExcelRange = .Cells(1, 1, 1, 2 + prowheader)
                rgHeader.Style.Fill.PatternType = Style.ExcelFillStyle.Solid
                rgHeader.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue)
                rgHeader.Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                rgHeader.Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                rgHeader.Style.WrapText = True
            End With

            ws = Pck.Workbook.Worksheets.Add("Tooling")
            With ws
                Dim irowunit As Integer = 0
                .Cells(1, 1, 1, 1).Value = "Description"
                .Cells(1, 2, 1, 2).Value = "Guide Price"
                For Each xdata In pdataHeader
                    .Cells(1, 3 + irowunit, 1, 3 + irowunit).Value = xdata.Description
                    irowunit = irowunit + 1
                Next

                irowunit = 1
                For Each Tools In SesT
                    Dim r As Integer = irowunit + 1
                    .Cells(r, 1, r, 1).Value = Tools.Description
                    .Cells(r, 2, r, 2).Value = IIf(Tools.GuidePrice = 0, "", Tools.GuidePrice)
                    If prowheader >= 1 Then .Cells(r, 3, r, 3).Value = IIf(Tools.Supplier1 = 0, "", Tools.Supplier1)
                    If prowheader >= 2 Then .Cells(r, 4, r, 4).Value = IIf(Tools.Supplier2 = 0, "", Tools.Supplier2)
                    If prowheader >= 3 Then .Cells(r, 5, r, 5).Value = IIf(Tools.Supplier3 = 0, "", Tools.Supplier3)
                    If prowheader >= 4 Then .Cells(r, 6, r, 6).Value = IIf(Tools.Supplier4 = 0, "", Tools.Supplier4)
                    If prowheader >= 5 Then .Cells(r, 7, r, 7).Value = IIf(Tools.Supplier5 = 0, "", Tools.Supplier5)
                    If prowheader >= 6 Then .Cells(r, 8, r, 8).Value = IIf(Tools.Supplier6 = 0, "", Tools.Supplier6)
                    irowunit = irowunit + 1
                Next
                .Column(1).Width = 23
                .Column(2).Width = 20
                .Column(3).Width = 20
                .Column(4).Width = 20
                .Column(5).Width = 20
                .Column(6).Width = 20
                .Column(7).Width = 20
                .Column(8).Width = 20

                Dim rg As OfficeOpenXml.ExcelRange = .Cells(1, 1, irowunit, 2 + prowheader)
                rg.Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Font.Name = "Tahoma"
                rg.Style.Font.Size = 10

                Dim rf As OfficeOpenXml.ExcelRange = .Cells(2, 2, irowunit, 2 + prowheader)
                rf.Style.Numberformat.Format = "#,##0"

                Dim rgHeader As OfficeOpenXml.ExcelRange = .Cells(1, 1, 1, 2 + prowheader)
                rgHeader.Style.Fill.PatternType = Style.ExcelFillStyle.Solid
                rgHeader.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue)
                rgHeader.Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                rgHeader.Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                rgHeader.Style.WrapText = True
            End With

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment; filename=Comparison_" & Format(Date.Now, "yyyy-MM-dd") & ".xlsx")

            Dim stream As MemoryStream = New MemoryStream(Pck.GetAsByteArray())

            Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length)
            Response.Flush()
            Response.Close()
        End Using

    End Sub


    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Procedure"
    Private Sub FillCombo(ByVal proj As String, ByVal group As String, ByVal commodity As String, ByVal commodityGroup As String, pstatus As Integer)
        Dim ds As New DataSet
        Dim pmsg As String = ""

        If pstatus = 1 Then
            ds = clsComparisonDB.getProjectID(proj, "", "", "", 1, pUser)
            If pmsg = "" Then
                cboProject.DataSource = ds
                cboProject.DataBind()
            End If
        End If

        If pstatus = 2 Then
            ds = clsComparisonDB.getProjectID(proj, "", "", "", 2, pUser)
            If pmsg = "" Then
                cboGroup.DataSource = ds
                cboGroup.DataBind()
            End If
        End If

        If pstatus = 3 Then
            ds = clsComparisonDB.getProjectID(proj, group, "", "", 3, pUser)
            If pmsg = "" Then
                cboCommodity.DataSource = ds
                cboCommodity.DataBind()
            End If
        End If

        If pstatus = 4 Then
            ds = clsComparisonDB.getProjectID(proj, group, commodity, "", 4, pUser)
            If pmsg = "" Then
                cboCommodityGroup.DataSource = ds
                cboCommodityGroup.DataBind()
            End If
        End If
        If pstatus = 5 Then
            ds = clsComparisonDB.getProjectID(proj, group, commodity, commodityGroup, 5, pUser)
            If pmsg = "" Then
                cboPartNo.DataSource = ds
                cboPartNo.DataBind()
            End If
        End If


    End Sub
    Private Sub up_GridLoad(ByVal pProjectID As String, ByVal pGroup As String, ByVal pComodity As String, ByVal pComodityGroup As String, ByVal PartNo As String)
        Dim ErrMsg As String = ""
        Dim Ses As New List(Of clsComparison)
        Session("bold") = 0
        Ses = clsComparisonDB.getlist(IIf(pProjectID = Nothing, "", pProjectID), IIf(pGroup = Nothing, "", pGroup), IIf(pComodity = Nothing, "", pComodity), IIf(pComodityGroup = Nothing, "", pComodityGroup), IIf(PartNo = Nothing, "", PartNo), ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub
    Private Sub up_GridLoadTooling(ByVal pProjectID As String, ByVal pGroup As String, ByVal pComodity As String, ByVal pComodityGroup As String, ByVal PartNo As String)
        Dim ErrMsg As String = ""
        Dim Ses As New List(Of clsComparison)
        Session("boldTooling") = 0
        Ses = clsComparisonDB.getlistTooling(IIf(pProjectID = Nothing, "", pProjectID), IIf(pGroup = Nothing, "", pGroup), IIf(pComodity = Nothing, "", pComodity), IIf(pComodityGroup = Nothing, "", pComodityGroup), IIf(PartNo = Nothing, "", PartNo), ErrMsg)
        If ErrMsg = "" Then
            GridTooling.DataSource = Ses
            GridTooling.DataBind()
        End If
    End Sub
#End Region




End Class
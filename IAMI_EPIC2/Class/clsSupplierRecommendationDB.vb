﻿Imports System.Data.SqlClient
Public Class clsSupplierRecommendationDB

    Public Shared Function GetListMaster(pUser As String, pClsSR As clsSupplierRecommendation, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SupplierRecommendation_GetList]"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("SRDateFrom", pClsSR.FromDate)
                cmd.Parameters.AddWithValue("SRDateTo", pClsSR.ToDate)
                cmd.Parameters.AddWithValue("Supplier", pClsSR.SupplierRecommend)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function GetHeaderSRList(pDateFrom As String, pDateTo As String, pUser As String, _
                                          Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "select Approval_Name from (Select * From SR_Header) ph " & vbCrLf
               
                sql = sql + "inner join SR_Approval pa on ph.SR_Number = pa.SR_Number " & vbCrLf & _
                      "inner join Mst_ApprovalSetup ap on pa.Approval_ID = ap.Approval_ID and ap.Approval_Group='SR'  " & vbCrLf & _
                      "where Cast(SR_Date as Date) >= '" & pDateFrom & "' And Cast(SR_Date as Date) <= '" & pDateTo & "' "

                sql = sql + "group by Approval_Level,Approval_Name"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function GetListMasterApproval(pClsSR As clsSupplierRecommendation, pUser As String, pStatus As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SupplierRecommendation_GetListApproval]"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("SRDateFrom", pClsSR.FromDate)
                cmd.Parameters.AddWithValue("SRDateTo", pClsSR.ToDate)
                cmd.Parameters.AddWithValue("Supplier", pClsSR.SupplierRecommend)
                cmd.Parameters.AddWithValue("UserID", pUser)
                cmd.Parameters.AddWithValue("Status", pStatus)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    'Public Shared Function GetDataSupplier(pCPNumber As String, Optional ByRef pErr As String = "") As DataSet
    '    Dim ds As New DataSet
    '    Dim sql As String
    '    Try
    '        Using con As New SqlConnection(Sconn.Stringkoneksi)
    '            con.Open()

    '            sql = "SELECT   DISTINCT A.Supplier_Code  as Code, b.Supplier_Name  As Description " & vbCrLf & _
    '                  "FROM  CP_Detail a INNER JOIN Mst_Supplier b ON b.Supplier_Code=a.Supplier_Code" & vbCrLf & _
    '                  "Where  CP_Number=@CPNumber "

    '            'If pSupplier <> "00" Then
    '            '    sql = sql + " AND H.Supplier_Code = '" & pSupplier & "'"
    '            'End If

    '            Dim cmd As New SqlCommand(sql, con)
    '            cmd.CommandType = CommandType.Text
    '            cmd.Parameters.AddWithValue("CPNumber", pCPNumber)

    '            Dim da As New SqlDataAdapter(cmd)
    '            da.Fill(ds)
    '            da.Dispose()
    '            cmd.Dispose()

    '        End Using

    '        Return ds

    '    Catch ex As Exception
    '        pErr = ex.Message
    '        Return Nothing
    '    End Try
    'End Function

    Public Shared Function GetDataSupplierRecommendation(pCPNumber As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "SELECT   DISTINCT A.Supplier_Code  as Code, b.Supplier_Name  As Description " & vbCrLf & _
                      "FROM  CP_Detail a INNER JOIN Mst_Supplier b ON b.Supplier_Code=a.Supplier_Code" & vbCrLf & _
                      "Where  CP_Number=@CPNumber "

                'If pSupplier <> "00" Then
                '    sql = sql + " AND H.Supplier_Code = '" & pSupplier & "'"
                'End If

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("CPNumber", pCPNumber)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboCPNo(pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_SR_GetCPNumber '" + pUser + "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetMaxSRNo(pMonth As Integer, pYear As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_SR_GetMaxSRNumber " & pMonth & "," & pYear & " "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateHeader(ByVal pCost As clsSupplierRecommendation, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SR_Update_Header]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
               
                cmd.Parameters.AddWithValue("SR_Number", pCost.SRNumber)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("CPNumber", pCost.CPNumber)
                cmd.Parameters.AddWithValue("SRDate", pCost.SRDate)
                cmd.Parameters.AddWithValue("SRStatus", pCost.SRStatus)
                cmd.Parameters.AddWithValue("Supplier_Recomm", pCost.SupplierRecommend)
                cmd.Parameters.AddWithValue("Consideration", pCost.Consideration)
                cmd.Parameters.AddWithValue("Notes", pCost.Notes)
                cmd.Parameters.AddWithValue("RegUser", pUser)
               
                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Sub InsertHeader(ByVal pCost As clsSupplierRecommendation, pUser As String, Optional ByRef SRNumberOutput As String = "", Optional ByRef pErr As String = "")
        Dim sql As String
        Dim cmd As SqlCommand
        Dim ds As New DataSet

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SR_Insert_Header]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure

                'cmd.Parameters.AddWithValue("SR_Number", pCost.SRNumber)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("CPNumber", pCost.CPNumber)
                cmd.Parameters.AddWithValue("SRDate", pCost.SRDate)
                cmd.Parameters.AddWithValue("SRStatus", pCost.SRStatus)
                cmd.Parameters.AddWithValue("Supplier_Recomm", pCost.SupplierRecommend)
                cmd.Parameters.AddWithValue("Consideration", pCost.Consideration)
                cmd.Parameters.AddWithValue("Notes", pCost.Notes)
                cmd.Parameters.AddWithValue("RegUser", pUser)

                Dim outPutParameter As SqlParameter = New SqlParameter()
                outPutParameter.ParameterName = "SR_Number"
                outPutParameter.SqlDbType = System.Data.SqlDbType.VarChar
                outPutParameter.Direction = System.Data.ParameterDirection.Output
                outPutParameter.Size = 50
                cmd.Parameters.Add(outPutParameter)
                'con.Open()
                cmd.ExecuteNonQuery()
                SRNumberOutput = outPutParameter.Value.ToString()
                cmd.Dispose()
            End Using


        Catch ex As Exception
            pErr = ex.Message

        End Try
    End Sub

    Public Shared Function UpdateDetailQCD(ByVal pCost As clsSupplierRecommendation, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SR_Update_DetailQCD]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("SR_Number", pCost.SRNumber)
                cmd.Parameters.AddWithValue("CP_Number", pCost.CPNumber)
                cmd.Parameters.AddWithValue("GroupTab", pCost.GroupType)
                cmd.Parameters.AddWithValue("Information_Code", pCost.Information_Code)
                cmd.Parameters.AddWithValue("Information", pCost.Information)
                cmd.Parameters.AddWithValue("Weight", pCost.Weight)
                cmd.Parameters.AddWithValue("Supplier1", pCost.Supplier1)
                cmd.Parameters.AddWithValue("Point1", pCost.Point1)
                cmd.Parameters.AddWithValue("WeightPoint1", pCost.WeightPoint1)
                cmd.Parameters.AddWithValue("Notes1", pCost.Notes1)
                cmd.Parameters.AddWithValue("Supplier2", pCost.Supplier2)
                cmd.Parameters.AddWithValue("Point2", pCost.Point2)
                cmd.Parameters.AddWithValue("WeightPoint2", pCost.WeightPoint2)
                cmd.Parameters.AddWithValue("Notes2", pCost.Notes2)
                cmd.Parameters.AddWithValue("Supplier3", pCost.Supplier3)
                cmd.Parameters.AddWithValue("Point3", pCost.Point3)
                cmd.Parameters.AddWithValue("WeightPoint3", pCost.WeightPoint3)
                cmd.Parameters.AddWithValue("Notes3", pCost.Notes3)
                cmd.Parameters.AddWithValue("Supplier4", pCost.Supplier4)
                cmd.Parameters.AddWithValue("Point4", pCost.Point4)
                cmd.Parameters.AddWithValue("WeightPoint4", pCost.WeightPoint4)
                cmd.Parameters.AddWithValue("Notes4", pCost.Notes4)
                cmd.Parameters.AddWithValue("Supplier5", pCost.Supplier5)
                cmd.Parameters.AddWithValue("Point5", pCost.Point5)
                cmd.Parameters.AddWithValue("WeightPoint5", pCost.WeightPoint5)
                cmd.Parameters.AddWithValue("Notes5", pCost.Notes5)
                cmd.Parameters.AddWithValue("RegUser", pUser)


                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDetailQCD(ByVal pCost As clsSupplierRecommendation, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SR_Insert_DetailQCD]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("SR_Number", pCost.SRNumber)
                cmd.Parameters.AddWithValue("CP_Number", pCost.CPNumber)
                cmd.Parameters.AddWithValue("GroupTab", pCost.GroupType)
                cmd.Parameters.AddWithValue("Information_Code", pCost.Information_Code)
                cmd.Parameters.AddWithValue("Information", pCost.Information)
                cmd.Parameters.AddWithValue("Weight", pCost.Weight)
                cmd.Parameters.AddWithValue("Supplier1", pCost.Supplier1)
                cmd.Parameters.AddWithValue("Point1", pCost.Point1)
                cmd.Parameters.AddWithValue("WeightPoint1", pCost.WeightPoint1)
                cmd.Parameters.AddWithValue("Notes1", pCost.Notes1)
                cmd.Parameters.AddWithValue("Supplier2", pCost.Supplier2)
                cmd.Parameters.AddWithValue("Point2", pCost.Point2)
                cmd.Parameters.AddWithValue("WeightPoint2", pCost.WeightPoint2)
                cmd.Parameters.AddWithValue("Notes2", pCost.Notes2)
                cmd.Parameters.AddWithValue("Supplier3", pCost.Supplier3)
                cmd.Parameters.AddWithValue("Point3", pCost.Point3)
                cmd.Parameters.AddWithValue("WeightPoint3", pCost.WeightPoint3)
                cmd.Parameters.AddWithValue("Notes3", pCost.Notes3)
                cmd.Parameters.AddWithValue("Supplier4", pCost.Supplier4)
                cmd.Parameters.AddWithValue("Point4", pCost.Point4)
                cmd.Parameters.AddWithValue("WeightPoint4", pCost.WeightPoint4)
                cmd.Parameters.AddWithValue("Notes4", pCost.Notes4)
                cmd.Parameters.AddWithValue("Supplier5", pCost.Supplier5)
                cmd.Parameters.AddWithValue("Point5", pCost.Point5)
                cmd.Parameters.AddWithValue("WeightPoint5", pCost.WeightPoint5)
                cmd.Parameters.AddWithValue("Notes5", pCost.Notes5)
                cmd.Parameters.AddWithValue("RegUser", pUser)


                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function UpdateDetail(ByVal pCost As clsSupplierRecommendation, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SR_Update_Detail]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("SR_Number", pCost.SRNumber)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("CP_Number", pCost.CPNumber)
                cmd.Parameters.AddWithValue("GroupTab", pCost.GroupType)
                cmd.Parameters.AddWithValue("Information_Code", pCost.Information_Code)
                cmd.Parameters.AddWithValue("Information", pCost.Information)
                cmd.Parameters.AddWithValue("Supplier1", pCost.Supplier1)
                cmd.Parameters.AddWithValue("Quotation1", pCost.Quotation1)
                cmd.Parameters.AddWithValue("Final1", pCost.Final1)
                cmd.Parameters.AddWithValue("Supplier2", pCost.Supplier2)
                cmd.Parameters.AddWithValue("Quotation2", pCost.Quotation2)
                cmd.Parameters.AddWithValue("Final2", pCost.Final2)
                cmd.Parameters.AddWithValue("Supplier3", pCost.Supplier3)
                cmd.Parameters.AddWithValue("Quotation3", pCost.Quotation3)
                cmd.Parameters.AddWithValue("Final3", pCost.Final3)
                cmd.Parameters.AddWithValue("Supplier4", pCost.Supplier4)
                cmd.Parameters.AddWithValue("Quotation4", pCost.Quotation4)
                cmd.Parameters.AddWithValue("Final4", pCost.Final4)
                cmd.Parameters.AddWithValue("Supplier5", pCost.Supplier5)
                cmd.Parameters.AddWithValue("Quotation5", pCost.Quotation5)
                cmd.Parameters.AddWithValue("Final5", pCost.Final5)
                cmd.Parameters.AddWithValue("RegUser", pUser)


                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDetail(ByVal pCost As clsSupplierRecommendation, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SR_Insert_Detail]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("SR_Number", pCost.SRNumber)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("CP_Number", pCost.CPNumber)
                cmd.Parameters.AddWithValue("GroupTab", pCost.GroupType)
                cmd.Parameters.AddWithValue("Information_Code", pCost.Information_Code)
                cmd.Parameters.AddWithValue("Information", pCost.Information)
                cmd.Parameters.AddWithValue("Supplier1", pCost.Supplier1)
                cmd.Parameters.AddWithValue("Quotation1", pCost.Quotation1)
                cmd.Parameters.AddWithValue("Final1", pCost.Final1)
                cmd.Parameters.AddWithValue("Supplier2", pCost.Supplier2)
                cmd.Parameters.AddWithValue("Quotation2", pCost.Quotation2)
                cmd.Parameters.AddWithValue("Final2", pCost.Final2)
                cmd.Parameters.AddWithValue("Supplier3", pCost.Supplier3)
                cmd.Parameters.AddWithValue("Quotation3", pCost.Quotation3)
                cmd.Parameters.AddWithValue("Final3", pCost.Final3)
                cmd.Parameters.AddWithValue("Supplier4", pCost.Supplier4)
                cmd.Parameters.AddWithValue("Quotation4", pCost.Quotation4)
                cmd.Parameters.AddWithValue("Final4", pCost.Final4)
                cmd.Parameters.AddWithValue("Supplier5", pCost.Supplier5)
                cmd.Parameters.AddWithValue("Quotation5", pCost.Quotation5)
                cmd.Parameters.AddWithValue("Final5", pCost.Final5)
                cmd.Parameters.AddWithValue("RegUser", pUser)


                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetSRData(pSRNumber As String, pRevision As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_SR_GetDataHeader '" & pSRNumber & "','" & pRevision & "'" 

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetSRDataApproval(pSRNumber As String, pRevision As Integer, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_SR_GetDataHeaderApproval '" & pSRNumber & "','" & pRevision & "','" & pUser & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function SubmitData(ByVal pCost As clsSupplierRecommendation, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SR_Submit]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("SRNumber", pCost.SRNumber)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("CPNumber", pCost.CPNumber)
                cmd.Parameters.AddWithValue("SRStatus", "1")
                cmd.Parameters.AddWithValue("RegUser", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Approval(ByVal pCost As clsSupplierRecommendation, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SRApproval_Approve]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("SRNumber", pCost.SRNumber)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("ApprovalNotes", pCost.Notes)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataSupplier(pCPNumber As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()


                sql = "sp_SR_GetDataSupplier"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@CPNumber", pCPNumber)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

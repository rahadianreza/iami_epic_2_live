﻿Imports System.Data.SqlClient

Public Class ClsCrudgeOilDB
    Public Shared Function getGrid(Period_From As String, Period_To As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_CrudgeOil_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Period_From", Period_From)
                cmd.Parameters.AddWithValue("@Period_To", Period_To)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getDetail(OilType As String, Period As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_CrudgeOil_Detail_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@OilType", OilType)
                cmd.Parameters.AddWithValue("@Period", Period)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

  
    Public Shared Function Insert(OilType As String, Period As String, UoM As String, Price As Decimal, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_CrudgeOil_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@OilType", OilType)
                cmd.Parameters.AddWithValue("@Period", Period)
                'cmd.Parameters.AddWithValue("@PeriodMonthly", PeriodMonthly)
                cmd.Parameters.AddWithValue("@UoM", UoM)
                cmd.Parameters.AddWithValue("@Price", Price)
                cmd.Parameters.AddWithValue("@Register_By", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Update(OilType As String, Period As String, UoM As String, Price As Decimal, _
                                             pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_CrudgeOil_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@OilType", OilType)
                cmd.Parameters.AddWithValue("@Period", Period)
                'cmd.Parameters.AddWithValue("@PeriodMonthly", PeriodMonthly)
                cmd.Parameters.AddWithValue("@UoM", UoM)
                cmd.Parameters.AddWithValue("@Price", Price)
                cmd.Parameters.AddWithValue("@Update_By", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteCrudgeOil(OilType As String, Period As String, _
                                             pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_CrudgeOil_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@OilType", OilType)
                cmd.Parameters.AddWithValue("@Period", Period)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

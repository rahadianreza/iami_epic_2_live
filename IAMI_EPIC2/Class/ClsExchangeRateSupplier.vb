﻿Public Class ClsExchangeRateSupplier

#Region "Properties"
    Private _SupplierCode As String
    Public Property SupplierCode() As String
        Get
            Return _SupplierCode
        End Get
        Set(ByVal value As String)
            _SupplierCode = value
        End Set
    End Property

    Private _CurrencyCode As String
    Public Property CurrencyCode() As String
        Get
            Return _CurrencyCode
        End Get
        Set(ByVal value As String)
            _CurrencyCode = value
        End Set
    End Property

    Private _ExchangeRateDateFrom As String
    Public Property ExchangeRateDateFrom() As String
        Get
            Return _ExchangeRateDateFrom
        End Get
        Set(ByVal value As String)
            _ExchangeRateDateFrom = value
        End Set
    End Property

    Private _ExchangeRateDateTo As String
    Public Property ExchangeRateDateTo() As String
        Get
            Return _ExchangeRateDateTo
        End Get
        Set(ByVal value As String)
            _ExchangeRateDateTo = value
        End Set
    End Property

    Private _ExchangeRateDate As String
    Public Property ExchangeRateDate() As String
        Get
            Return _ExchangeRateDate
        End Get
        Set(ByVal value As String)
            _ExchangeRateDate = value
        End Set
    End Property

    Private _BuyingRate As Double
    Public Property BuyingRate() As Double
        Get
            Return _BuyingRate
        End Get
        Set(ByVal value As Double)
            _BuyingRate = value
        End Set
    End Property

    Private _MiddleRate As Double
    Public Property MiddleRate() As Double
        Get
            Return _MiddleRate
        End Get
        Set(ByVal value As Double)
            _MiddleRate = value
        End Set
    End Property

    Private _SellingRate As Double
    Public Property SellingRate() As Double
        Get
            Return _SellingRate
        End Get
        Set(ByVal value As Double)
            _SellingRate = value
        End Set
    End Property

    Private _Remarks As String
    Public Property Remarks() As String
        Get
            Return _Remarks
        End Get
        Set(ByVal value As String)
            _Remarks = value
        End Set
    End Property

    Private _UserID As String
    Public Property UserID() As String
        Get
            Return _UserID
        End Get
        Set(ByVal value As String)
            _UserID = value
        End Set
    End Property
#End Region

End Class
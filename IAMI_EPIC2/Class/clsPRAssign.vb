﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 04 Oct 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################


Public Class clsPRAssign
    Public Property RequestDateFrom As String
    Public Property RequestDateTo As String
    Public Property PRType As String
    Public Property ApprovalStatus As String
    Public Property Department As String
    Public Property Section As String
    Public Property Revision As String
    Public Property PRNumber As String
    Public Property PRDate As String
    Public Property PRBudget As String
    Public Property Project As String
    Public Property UrgentCls As String
    Public Property UrgentNote As String
    Public Property ApprovalNote As String
    Public Property ReqPOIssueDate As String

    Public Property PIC As String
    Public Property PICPO As String
    Public Property PICPO2 As String
    Public Property PICPO3 As String
    Public Property TenderStatus As String

    Public Property DeptHeadAppName As String
    Public Property DeptHeadAppDate As String

    Public Property GeneralManagerAppName As String
    Public Property GeneralManagerAppDate As String

End Class

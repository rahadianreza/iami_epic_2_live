﻿
Public Class clsPartItemList
    Public Property Part_No As String
    Public Property Part_Name As String
    Public Property EPL_Part_No As String
    Public Property pVariant As String
    Public Property UOM As String

    Public Property Picture1 As Object
    Public Property Picture2 As Object
    Public Property Picture3 As Object

    Public Property LastIAPrice As Double
    Public Property LastSupplier As String
    Public Property CreateDate As String
    Public Property CreateUser As String
    Public Property UpdateDate As String
    Public Property UpdateUser As String
End Class

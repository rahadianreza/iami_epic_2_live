﻿Imports System.Data.SqlClient
Public Class clsSupplierRecommendationApprovalDB
    Public Shared Function ApprovePRFull(ByVal pSRApproval As clsSupplierRecommendationApproval, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_SRApproval_Approve]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("SRNumber", pSRApproval.SRNumber)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("ApprovalNotes", pSRApproval.ApprovalNote)
                cmd.Parameters.AddWithValue("Rev", pSRApproval.Revision)


                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function Reject(ByVal PRApprove As clsSupplierRecommendationApproval, pUser As String, Optional ByRef pErr As String = "") As Integer
        Try
            Dim sql As String
            Dim i As Integer

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_Reject_SR"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("SRNumber", PRApprove.SRNumber)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("ApprovalNotes", PRApprove.ApprovalNote)

                i = cmd.ExecuteNonQuery

            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

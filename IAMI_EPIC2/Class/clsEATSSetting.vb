﻿Public Class ClsEATSSetting
    Public Property CompanyName As String
    Public Property Address As String
    Public Property Phone1 As String
    Public Property Phone2 As String
    Public Property Fax As String
    Public Property Email As String
    Public Property ContactPerson As String
    Public Property GSTActiveStatus As String
    Public Property PendingActiveStatus As String
    Public Property AndonYellowBalance As Integer
    Public Property AndonRedBalance As Integer
End Class

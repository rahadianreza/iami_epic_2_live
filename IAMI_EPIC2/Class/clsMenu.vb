﻿Public Class clsMenu
    Private m_CatererID As String
    Private m_MenuID As String
    Private m_MenuName As String
    Private m_Calorie As String
    Private m_Price As String
    Private m_Category As String
    Private m_GST As String
    Private m_TaxCode As String
    Private m_Description As String
    Private m_Active As String
    Private m_POS As String
    Private m_CreateUser As String
    Private m_Picture As Object

    Public Property RecommendedStatus As String
    Public Property SpicyStatus As String


    Public Property CatererID As String
        Get
            Return m_CatererID
        End Get
        Set(value As String)
            m_CatererID = value
        End Set
    End Property

    Public Property MenuID As String
        Get
            Return m_MenuID
        End Get
        Set(value As String)
            m_MenuID = value
        End Set
    End Property

    Public Property MenuName As String
        Get
            Return m_MenuName
        End Get
        Set(value As String)
            m_MenuName = value
        End Set
    End Property

    Public Property Calorie As String
        Get
            Return m_Calorie
        End Get
        Set(value As String)
            m_Calorie = value
        End Set
    End Property


    Public Property Price As String
        Get
            Return m_Price
        End Get
        Set(value As String)
            m_Price = value
        End Set
    End Property

    Public Property MenuCategory As String
        Get
            Return m_Category
        End Get
        Set(value As String)
            m_Category = value
        End Set
    End Property

    Public Property GST As String
        Get
            Return m_GST
        End Get
        Set(value As String)
            m_GST = value
        End Set
    End Property

    Public Property TaxCode As String
        Get
            Return m_TaxCode
        End Get
        Set(value As String)
            m_TaxCode = value
        End Set
    End Property

    Public Property Description As String
        Get
            Return m_Description
        End Get
        Set(value As String)
            m_Description = value
        End Set
    End Property

    Public Property MenuType As String
        Get
            Return m_POS
        End Get
        Set(value As String)
            m_POS = value
        End Set
    End Property

    Public Property ActiveStatus As String
        Get
            Return m_Active
        End Get
        Set(value As String)
            m_Active = value
        End Set
    End Property

    Public Property CreateUser As String
        Get
            Return m_CreateUser
        End Get
        Set(value As String)
            m_CreateUser = value
        End Set
    End Property

    Public Property Picture As Object
        Get
            Return m_Picture
        End Get
        Set(value As Object)
            m_Picture = value
        End Set
    End Property
End Class

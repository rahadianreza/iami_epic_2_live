﻿Imports System.Data.SqlClient

Public Class clsMst_MaterialDB
    '*********************************************************
    '           ALL MATERIAL (STEEL , LME , RUBBER , PLASTIC)
    '*********************************************************
    Public Shared Function GetListMaterial(MaterialType As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", MaterialType)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getDetailMaterial(pMaterialCode As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Detail_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDataMaterial(pMaterialCode As String, pMaterialName As String, pMaterialType As String, pGroupItem As String, pCategory As String, pCountry As String, pUOM As String, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@MaterialName", pMaterialName)
                cmd.Parameters.AddWithValue("@CountryCls", pCountry)
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@GroupItem", pGroupItem)
                cmd.Parameters.AddWithValue("@Category", pCategory)
                cmd.Parameters.AddWithValue("@UOM", pUOM)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateDataMaterial(pMaterialCode As String, pMaterialName As String, pMaterialType As String, pGroupItem As String, pCategory As String, pCountry As String, pUOM As String, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Update", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@MaterialName", pMaterialName)
                cmd.Parameters.AddWithValue("@CountryCls", pCountry)
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@GroupItem", pGroupItem)
                cmd.Parameters.AddWithValue("@Category", pCategory)
                cmd.Parameters.AddWithValue("@UOM", pUOM)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteDataMaterial(pMaterialCode As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Delete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    '*************************'
    'GET DESCRIPTION OF COMBO
    '*************************
    Public Shared Function GetDataCombo(pComboType As String, Optional ByVal pComboValue As String = "ALL", Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try

            Dim sQuery As String = "sp_Mst_Material_GetDataCombo"
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sQuery, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Type", pComboType)
                cmd.Parameters.AddWithValue("ComboValue", pComboValue)
                Dim da As New SqlDataAdapter(cmd)

                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    '*************************************************
    ' MATERIAL PRICE
    '*************************************************
    Public Shared Function GetListMaterial_Price(PeriodFrom As String, PeriodTo As String, _
                                                MaterialType As String, Supplier As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Price_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@PeriodFrom", PeriodFrom)
                cmd.Parameters.AddWithValue("@PeriodTo", PeriodTo)
                cmd.Parameters.AddWithValue("@MaterialType", MaterialType)
                cmd.Parameters.AddWithValue("@SupplierCode", Supplier)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getDetailMaterial_Price(pMaterialCode As String, pPriceType As String, pSupplierCode As String, pPeriod As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Detail_Price", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@PriceType", pPriceType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDataMaterial_Price(pMaterialCode As String, pPriceType As String, pSupplierCode As String, pPeriod As String, _
                                             pCurrency As String, pPrice As Decimal, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Price_Insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@PriceType", pPriceType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@Currency", pCurrency)
                cmd.Parameters.AddWithValue("@Price", pPrice)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateDataMaterial_Price(pMaterialCode As String, pPriceType As String, pSupplierCode As String, pPeriod As String, _
                                            pCurrency As String, pPrice As Decimal, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Price_Update", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@PriceType", pPriceType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@Currency", pCurrency)
                cmd.Parameters.AddWithValue("@Price", pPrice)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteDataMaterial_Price(pMaterialCode As String, pPriceType As String, pSupplierCode As String, pPeriod As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Price_Delete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@PriceType", pPriceType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
   
End Class

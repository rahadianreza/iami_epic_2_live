﻿
Public Class ClsPeriodAdjustment
    Public Property PeriodID As String
    Public Property PeriodDescription As String
    Public Property PeriodType As String
    Public Property PeriodFrom As String
    Public Property PeriodTo As String
    Public Property PODateFrom As String
    Public Property PODateTo As String
    Public Property RegisterBy As String
    Public Property RegisterDate As String
    Public Property UpdateBy As String
    Public Property UpdateDate As String
End Class

﻿Imports System.Data.SqlClient

Public Class ClsMaterial_SteelDB

    Public Shared Function GetGroupItem(ByVal MaterialCode As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_GetMaterialCode", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("MaterialCode", MaterialCode)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetCategory(ByVal GroupItem As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_GetCategory", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("GroupItem", GroupItem)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetSupplier(ByVal MaterialType As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_GetSupplierMaterial", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("MaterialType", MaterialType)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getlistds(ByVal pFrom As String, ByVal pTo As String, ByVal Type As String, Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Mst_MaterialSteel_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Type", Type)
                cmd.Parameters.AddWithValue("PeriodFrom", pFrom)
                cmd.Parameters.AddWithValue("PeriodTo", pTo)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getlistdsDetail(ByVal pPeriod As String, ByVal pSupplier As String, ByVal Type As String, ByVal pMaterial As String, Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Mst_MaterialSteelDetail_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Type", Type)
                cmd.Parameters.AddWithValue("Period", pPeriod)
                cmd.Parameters.AddWithValue("Supplier", pSupplier)
                cmd.Parameters.AddWithValue("Material", pMaterial)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal Type As String, ByVal Supplier As String, ByVal Period As String, ByVal Material As String, ByVal GroupItem As String, ByVal Category As String, ByVal Currency As String, ByVal Price As Decimal, ByVal Status As String, ByVal UserID As String, Optional ByRef pErr As String = "") As Integer
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String = ""
                sql = "sp_Mst_MaterialSteel_Ins_Upd_Del"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Type", Type)
                cmd.Parameters.AddWithValue("Supplier", Supplier)
                cmd.Parameters.AddWithValue("Period", Period)
                cmd.Parameters.AddWithValue("Material", Material)
                cmd.Parameters.AddWithValue("GroupItem", GroupItem)
                cmd.Parameters.AddWithValue("Category", Category)
                cmd.Parameters.AddWithValue("Currency", Currency)
                cmd.Parameters.AddWithValue("Price", Price)
                cmd.Parameters.AddWithValue("Status", Status)
                cmd.Parameters.AddWithValue("UserID", UserID)
                i = cmd.ExecuteNonQuery
                Return i
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Delete(ByVal RateClass As String, ByVal VaKind As String, ByVal Period As String, ByVal UserID As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Electricity_Ins_Upd_Del"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RateClass", RateClass)
                cmd.Parameters.AddWithValue("VaKind", VaKind)
                cmd.Parameters.AddWithValue("Period", Period)
                cmd.Parameters.AddWithValue("RegulerPostPaid", 0)
                cmd.Parameters.AddWithValue("PrePostPaid", 0)
                cmd.Parameters.AddWithValue("Status", "3")
                cmd.Parameters.AddWithValue("UserID", UserID)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function ChartSource(ByVal RateClass As String, ByVal VAKind As String, ByVal pFrom As String, ByVal pTo As String, Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Electricity_Chart"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RateClass", RateClass)
                cmd.Parameters.AddWithValue("VaKind", VAKind)
                cmd.Parameters.AddWithValue("PeriodFrom", pFrom)
                cmd.Parameters.AddWithValue("PeriodTo", pTo)

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

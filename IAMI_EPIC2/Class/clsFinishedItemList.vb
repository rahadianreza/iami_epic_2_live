﻿
Public Class clsFinishedItemList
    Public Property VariantCode As String
    Public Property VariantName As String
    Public Property MaterialFUNo As String
    Public Property Type As String
    Public Property UOM As String

    Public Property Picture1 As Object
    Public Property Picture2 As Object
    Public Property Picture3 As Object

    Public Property LastIAPrice As Double
    Public Property CreateDate As String
    Public Property CreateUser As String
    Public Property UpdateDate As String
    Public Property UpdateUser As String
End Class

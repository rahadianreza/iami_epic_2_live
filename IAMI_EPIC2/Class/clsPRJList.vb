﻿Public Class ClsPRJList
    Public Property ProjectDateFrom As String
    Public Property ProjectDateTo As String
    Public Property ProjectType As String

    Public Property ProjectId As String
    Public Property PeojectName As String
    Public Property ProjectDate As String
    Public Property ProjectStatus As String
    Public Property StartYear As String
    Public Property RegisterBy As String
    Public Property RegisterDate As String
    Public Property UpdateBy As String
    Public Property UpdateDate As String

    Public Property VehicleCode As String
    Public Property Year1 As Decimal
    Public Property Year2 As Decimal
    Public Property Year3 As Decimal
    Public Property TotalVolumeRfq As Integer
    Public Property Remarks As String
End Class

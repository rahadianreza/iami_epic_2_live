﻿Public Class ClsPurchasedPart
    Public Property Purchased_Part_Code As String
    Public Property Purchased_Part_Name As String
    Public Property Specification As String
    Public Property UoM As String
    Public Property Price As Decimal
    Public Property RegisterBy As String
    Public Property RegisterDate As Date
    Public Property UpdateBy As String
    Public Property UpdateDate As Date
End Class

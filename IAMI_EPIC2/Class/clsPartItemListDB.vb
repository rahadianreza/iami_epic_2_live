﻿
Imports System.Data.SqlClient

Public Class clsPartItemListDB

    Public Shared Function GenerateNewMaterialNo(pGroup As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String

                'sql = "Select 'MTRL' + Right(Isnull(Right(Max(Material_No),6),0) + 1000001,6)  As Material_No From Mst_Item"
                sql = "sp_PartItemList_GenerateNewPartNo"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Digit", 10)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataForExcel(pGroup As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String

                sql = "Select * From Mst_Item"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetList(Optional ByRef pErr As String = "") As List(Of clsPartItemList)
        Dim sql As String
        Dim cmd As SqlCommand

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_PartItemMaster_GetList]"
                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure


                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)

                Dim list As New List(Of clsPartItemList)
                For i = 0 To dt.Rows.Count - 1
                    Dim updatedate As String
                    If IsDBNull(dt.Rows(i).Item("UpdateDate")) Then
                        updatedate = ""
                    Else
                        updatedate = dt.Rows(i).Item("UpdateDate")
                    End If

                    Dim itemdata As New clsPartItemList With {.Part_No = Trim(dt.Rows(i).Item("Part_No") & ""),
                                                          .Part_Name = Trim(dt.Rows(i).Item("Part_Name") & ""),
                                                          .EPL_Part_No = Trim(dt.Rows(i).Item("EPL_Part_No") & ""),
                                                          .pVariant = Trim(dt.Rows(i).Item("Variant") & ""),
                                                          .UOM = dt.Rows(i).Item("UOM"),
                                                          .LastIAPrice = IIf(IsDBNull(dt.Rows(i).Item("LastIAPrice")), 0, dt.Rows(i).Item("LastIAPrice")),
                                                          .LastSupplier = IIf(IsDBNull(dt.Rows(i).Item("LastSupplier")), "", dt.Rows(i).Item("LastSupplier")),
                                                          .CreateDate = IIf(IsDBNull(dt.Rows(i).Item("RegisterDate")), "", dt.Rows(i).Item("RegisterDate")),
                                                          .CreateUser = dt.Rows(i).Item("RegisterUser"),
                                                          .UpdateUser = IIf(IsDBNull(dt.Rows(i).Item("UpdateUser")), "", dt.Rows(i).Item("UpdateUser")),
                                                          .UpdateDate = IIf(IsDBNull(dt.Rows(i).Item("UpdateDate")), "", dt.Rows(i).Item("UpdateDate"))}

                    list.Add(itemdata)
                Next

                Return list
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'Public Shared Function GetTable(Optional ByRef pErr As String = "") As DataTable
    '    Dim sql As String
    '    Dim cmd As SqlCommand

    '    Try
    '        Using con As New SqlConnection(Sconn.Stringkoneksi)
    '            con.Open()

    '            sql = "[sp_ItemMaster_GetList]"
    '            cmd = New SqlCommand(sql, con)
    '            cmd.CommandType = CommandType.StoredProcedure
    '            Dim da As New SqlDataAdapter(cmd)
    '            Dim dt As New DataTable
    '            da.Fill(dt)


    '            Return dt
    '        End Using

    '    Catch ex As Exception
    '        pErr = ex.Message
    '        Return Nothing
    '    End Try
    'End Function

    Public Shared Function GetDataLastSupplier(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As LastSupplier From "
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataPRType(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As Code, Par_Description As Description From Mst_Parameter Where Par_Group = 'PRType'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataCategoryFromItem(pItem As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Category' And Par_ParentCode = (Select GroupItem From Mst_Item Where Material_No = '" & pItem & "')"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetFilterDataCategory(pGroup As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pGroup = "" Then
                    sql = "Select '00' As Code, 'ALL' Description UNION ALL Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Category'"
                Else
                    sql = "Select '00' As Code, 'ALL' Description UNION ALL Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Category' And Par_ParentCode = '" & pGroup & "'"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataCategory(pItem As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Category' And Par_ParentCode = '" & pItem & "'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataGroupFromItem(pItem As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As Group_Code, Par_Description As Group_Name From Mst_Parameter Where Par_Group = 'GroupItem' And Par_ParentCode = (Select PRType From Mst_Item Where Material_No = '" & pItem & "')"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataGroup(pItem As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As Group_Code, Par_Description As Group_Name From Mst_Parameter Where Par_Group = 'GroupItem' And Par_ParentCode = '" & pItem & "'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataUOM(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As UOM From Mst_Parameter Where Par_Group = 'UOM'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataItem(ByVal pObj As clsPartItemList, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = " SELECT Part_No,Part_Name,EPL_Part_No,Variant,UOM,LastIAPrice,LastSupplier FROM  dbo.Mst_PartItem " & vbCrLf

                If pObj.Part_No <> "" Then
                    sql = sql + " where Part_No = @Part_No "
                End If
                Dim cmd As New SqlCommand(sql, con)
                If pObj.Part_No <> "" Then
                    cmd.Parameters.AddWithValue("@Part_No", pObj.Part_No)
                End If
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                If pObj.Part_No <> "" Then
                    cmd.Parameters.Clear()
                End If
                cmd.Dispose()
            End Using

            Return ds
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try

    End Function

    Public Shared Function Insert(ByVal pItem As clsPartItemList, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Ins]"

                sql = "INSERT INTO Mst_PartItem  " & vbCrLf & _
                      "(Part_No,Part_Name,EPL_Part_No,Variant,UOM, RegisterDate, RegisterUser "

                If pItem.Picture1 IsNot Nothing Then
                    sql = sql + ", Picture1 "
                End If

                If pItem.Picture2 IsNot Nothing Then
                    sql = sql + ", Picture2 "
                End If

                If pItem.Picture3 IsNot Nothing Then
                    sql = sql + ", Picture3 "
                End If

                sql = sql + " ) VALUES  " & vbCrLf & _
                      "(@Part_No,@Part_Name,@EPL_Part_No,@Variant,@UOM,GETDATE(), UPPER(@UserId) "

                If pItem.Picture1 IsNot Nothing Then
                    sql = sql + ", @Picture1 "
                End If

                If pItem.Picture2 IsNot Nothing Then
                    sql = sql + ", @Picture2 "
                End If

                If pItem.Picture3 IsNot Nothing Then
                    sql = sql + ", @Picture3 "
                End If

                sql = sql + ") "
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                'cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("Part_No", pItem.Part_No)
                cmd.Parameters.AddWithValue("Part_Name", pItem.Part_Name)
                cmd.Parameters.AddWithValue("EPL_Part_No", pItem.EPL_Part_No)
                cmd.Parameters.AddWithValue("Variant", pItem.pVariant)
                cmd.Parameters.AddWithValue("UOM", pItem.UOM)
                cmd.Parameters.AddWithValue("UserId", pUser)


                If pItem.Picture1 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture1", pItem.Picture1)
                End If
                If pItem.Picture2 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture2", pItem.Picture2)
                End If
                If pItem.Picture3 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture3", pItem.Picture3)
                End If

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Update(ByVal pItem As clsPartItemList, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Upd]"
                sql = "UPDATE Mst_PartItem " & vbCrLf & _
                      "SET Part_Name = @Part_Name,EPL_Part_No = @EPL_Part_No,Variant = @Variant,UOM = @UOM,UpdateUser = UPPER(@UserId), UpdateDate = GETDATE() "

                If pItem.Picture1 IsNot Nothing Then
                    sql = sql + ", Picture1 = @Picture1 "
                End If
                If pItem.Picture2 IsNot Nothing Then
                    sql = sql + ", Picture2 = @Picture2 "
                End If
                If pItem.Picture3 IsNot Nothing Then
                    sql = sql + ", Picture3 = @Picture3 "
                End If

                sql = sql + "WHERE Part_No = @Part_No"
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("Part_No", pItem.Part_No)
                cmd.Parameters.AddWithValue("Part_Name", pItem.Part_Name)
                cmd.Parameters.AddWithValue("EPL_Part_No", pItem.EPL_Part_No)
                cmd.Parameters.AddWithValue("Variant", pItem.pVariant)
                cmd.Parameters.AddWithValue("UOM", pItem.UOM)
                'cmd.Parameters.AddWithValue("LastIAPrice", pItem.LastIAPrice)
                'cmd.Parameters.AddWithValue("LastSupplier", pItem.LastSupplier)
                cmd.Parameters.AddWithValue("UserId", pUser)
                If pItem.Picture1 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture1", pItem.Picture1)
                End If
                If pItem.Picture2 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture2", pItem.Picture2)
                End If
                If pItem.Picture3 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture3", pItem.Picture3)
                End If

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Delete(ByVal pItem As clsPartItemList, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Del]"
                sql = "Delete From Mst_PartItem Where Part_No = '" & pItem.Part_No & "'"
                cmd = New SqlCommand(sql, con)

                'cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("Part_No", pItem.Part_No)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteAttachment(ByVal pItem As clsPartItemList, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Del]"
                sql = "sp_MstPartItem_DeleteAttachment_All "
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Part_No", pItem.Part_No)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

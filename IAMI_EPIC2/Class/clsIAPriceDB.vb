﻿
Imports System.Data.SqlClient

Public Class clsIAPriceDB

    Public Shared Function GetHeaderIAList(pDateFrom As String, pDateTo As String, pUser As String, _
                                        Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "select Approval_Name from (Select * From IA_Header) ph " & vbCrLf

                sql = sql + "inner join IA_Approval pa on ph.IA_Number = pa.IA_Number " & vbCrLf & _
                      "inner join Mst_ApprovalSetup ap on pa.Approval_ID = ap.Approval_ID and ap.Approval_Group='IA'  " & vbCrLf & _
                      "where Cast(IA_Date as Date) >= '" & pDateFrom & "' And Cast(IA_Date as Date) <= '" & pDateTo & "' "

                sql = sql + "group by Approval_Name"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function



    Public Shared Function ApproveIAFull(ByVal IAAproval As clsIAPrice, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_IAApproval_Approve]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("IANumber", IAAproval.IAPriceNo)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("ApprovalNotes", IAAproval.ApprovalNote)
                cmd.Parameters.AddWithValue("Rev", IAAproval.Revision)


                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function Reject(ByVal IAAproval As clsIAPrice, pUser As String, Optional ByRef pErr As String = "") As Integer
        Try
            Dim sql As String
            Dim i As Integer

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_Reject_IA"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("IANumber", IAAproval.IAPriceNo)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("ApprovalNotes", IAAproval.ApprovalNote)

                i = cmd.ExecuteNonQuery

            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function



    Public Shared Function GetList(pUser As String, pClsIAPrice As clsIAPrice, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "[sp_IAPrice_GetList]"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("IAPriceDateFrom", pClsIAPrice.IAPriceDateFrom)
                cmd.Parameters.AddWithValue("IAPriceDateTo", pClsIAPrice.IAPriceDateTo)
                cmd.Parameters.AddWithValue("IAPriceNo", pClsIAPrice.IAPriceNo)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetListApproval(pClsIAPrice As clsIAPrice, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "[sp_IAPrice_GetListApproval]"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("IAPriceDateFrom", pClsIAPrice.IAPriceDateFrom)
                cmd.Parameters.AddWithValue("IAPriceDateTo", pClsIAPrice.IAPriceDateTo)
                cmd.Parameters.AddWithValue("IAPriceNo", pClsIAPrice.IAPriceNo)
                cmd.Parameters.AddWithValue("Status", pClsIAPrice.StatusApproval)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetHeaderIAPrice(pIANo As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select * From IA_Header Where IA_Number = '" & pIANo & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboCEData(pDateFrom As String, pDateTo As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_IAPrice_GetCENumber '" & pDateFrom & "','" & pDateTo & "','" & pUser & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboData(pDateFrom As String, pDateTo As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select 'ALL' IA_Number" & vbCrLf & _
                      "UNION ALL" & vbCrLf & _
                      "SELECT DISTINCT A.IA_Number FROM IA_Header A LEFT JOIN dbo.CE_Header B ON B.CE_Number = A.CE_Number LEFT JOIN dbo.RFQ_Set C ON C.RFQ_Set = B.RFQ_Set" & vbCrLf & _
                      "LEFT JOIN PR_Header D ON D.PR_Number = C.PR_Number Where D.PIC_Assign = '" & pUser & "' AND A.IA_Date >= '" & pDateFrom & "' And A.IA_Date <= '" & pDateTo & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboDataApproval(pDateFrom As String, pDateTo As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "Select 'ALL' IA_Number" & vbCrLf & _
                '      "UNION ALL" & vbCrLf & _
                '      "SELECT DISTINCT A.IA_Number FROM IA_Header A LEFT JOIN dbo.CE_Header B ON B.CE_Number = A.CE_Number LEFT JOIN dbo.RFQ_Set C ON C.RFQ_Set = B.RFQ_Set" & vbCrLf & _
                '      "LEFT JOIN PR_Header D ON D.PR_Number = C.PR_Number Where A.IAStatus ='1' and  D.PIC_Assign = '" & pUser & "' AND A.IA_Date >= '" & pDateFrom & "' And A.IA_Date <= '" & pDateTo & "'"

                sql = "sp_IAPrice_Approval_GetComboIANumber"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("DateFrom", pDateFrom)
                cmd.Parameters.AddWithValue("DateTo", pDateTo)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetSourceDetailList(pCENumber As String, pSupplier1 As String, pSupplier2 As String, pHeadSupplier1 As String, pHeadSupplier2 As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select	Material_No, Description, Specification, UOM, Qty, " & vbCrLf & _
                      "'' As Selected_Supplier, '' As Reference_Supplier, " & pHeadSupplier1 & ", " & pHeadSupplier2 & " " & vbCrLf & _
                      "From " & vbCrLf & _
                      "(     " & vbCrLf & _
                      "Select CP.Supplier_Code, CP.Material_No, IM.Description, IM.Specification, IM.UOM, CP.Qty, CP.Proposal_Price     " & vbCrLf & _
                      ",CP.Supplier_Code + '-Q' As Supplier_CodeQuotation, Quotation_Price     " & vbCrLf & _
                      "From (    " & vbCrLf & _
                      "Select H.CP_Number, H.Rev, H.CE_Number, Supplier_Code, Quotation_No, Material_No, Qty, Proposal_Price      " & vbCrLf & _
                      "From CP_Header H     " & vbCrLf & _
                      "Inner Join CP_Detail D On H.CP_Number = D.CP_Number And H.Rev = D.Rev And H.CE_Number = D.CE_Number      " & vbCrLf & _
                      "Where H.CE_Number = '" & pCENumber & "'     " & vbCrLf & _
                      ") CP     " & vbCrLf & _
                      "Left Join (     " & vbCrLf & _
                      "Select H.Quotation_No, H.RFQ_Set, H.Rev, H.Supplier_Code, CE_Number, D.Material_No, D.Qty, D.Price As Quotation_Price      " & vbCrLf & _
                      "From Quotation_Header H Inner Join Quotation_Detail D      " & vbCrLf & _
                      "On H.Quotation_No = D.Quotation_No And H.RFQ_Number = D.RFQ_Number And H.Rev = D.Rev     " & vbCrLf & _
                      "Left Join (Select * From CE_Header Where CE_Status = '2' And ISNULL(CE_Budget,'') <> '') C On C.RFQ_Set = H.RFQ_Set      " & vbCrLf & _
                      "Where Quotation_Status = '3'     " & vbCrLf & _
                      ") QU On QU.Quotation_No = CP.Quotation_No And QU.Supplier_Code = CP.Supplier_Code And QU.Material_No = CP.Material_No     " & vbCrLf & _
                      "Left Join Mst_Item IM On IM.Material_No = CP.Material_No      " & vbCrLf & _
                      ") MP     " & vbCrLf & _
                      "PIVOT      " & vbCrLf & _
                      "(     " & vbCrLf & _
                      "     SUM(Proposal_Price)     " & vbCrLf & _
                      "     FOR [Supplier_Code] IN (" & pSupplier1 & ")        " & vbCrLf & _
                      ")     " & vbCrLf & _
                      "AS PVT1     " & vbCrLf & _
                      "PIVOT     " & vbCrLf & _
                      "(     " & vbCrLf & _
                      "     SUM(Quotation_Price)     " & vbCrLf & _
                      "     FOR [Supplier_CodeQuotation] IN (" & pSupplier2 & ")     " & vbCrLf & _
                      ")     " & vbCrLf & _
                      "AS PVT2     " & vbCrLf & _
                      "GROUP BY Material_No,Description, Specification, UOM, Qty     " & vbCrLf & _
                      "  "

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                'cmd.Parameters.AddWithValue("CENumber", pCENumber)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetMaxIANo(pMonth As Integer, pYear As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_IA_GetMaxIANumber " & pMonth & " , " & pYear & " "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function UpdateHeader(ByVal pCost As clsIAPrice, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_IA_Update_Header]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure


                cmd.Parameters.AddWithValue("IA_Number", pCost.IAPriceNo)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("IADate", pCost.IAPriceDate)
                cmd.Parameters.AddWithValue("IAStatus", pCost.IAStatus)
                cmd.Parameters.AddWithValue("Descs", pCost.Description)
                cmd.Parameters.AddWithValue("Notes", pCost.Note)
                cmd.Parameters.AddWithValue("RegUser", pUser)
                cmd.Parameters.AddWithValue("IAValidDate", pCost.IAValidDate)
                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Sub InsertHeader(ByVal pCost As clsIAPrice, pUser As String, Optional ByRef pIAPriceNoOutput As String = "", Optional ByRef pErr As String = "")
        Dim sql As String
        Dim cmd As SqlCommand
        Dim ds As New DataSet

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_IA_Insert_Header]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure

                'cmd.Parameters.AddWithValue("IA_Number", pCost.IAPriceNo)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("IADate", pCost.IAPriceDate)
                cmd.Parameters.AddWithValue("IAStatus", pCost.IAStatus)
                cmd.Parameters.AddWithValue("Descs", pCost.Description)
                cmd.Parameters.AddWithValue("Notes", pCost.Note)
                cmd.Parameters.AddWithValue("RegUser", pUser)
                cmd.Parameters.AddWithValue("IAValidDate", pCost.IAValidDate)


                Dim outPutParameter As SqlParameter = New SqlParameter()
                outPutParameter.ParameterName = "IA_Number"
                outPutParameter.SqlDbType = System.Data.SqlDbType.VarChar
                outPutParameter.Direction = System.Data.ParameterDirection.Output
                outPutParameter.Size = 50
                cmd.Parameters.Add(outPutParameter)

                'con.Open()
                cmd.ExecuteNonQuery()
                pIAPriceNoOutput = outPutParameter.Value.ToString()
                cmd.Dispose()
            End Using


        Catch ex As Exception
            pErr = ex.Message
            ''Return Nothing
        End Try
    End Sub

    Public Shared Function UpdateDetail(ByVal pCost As clsIAPrice, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_IA_Update_Detail]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("IA_Number", pCost.IAPriceNo)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("Material_No", pCost.MaterialNo)
                cmd.Parameters.AddWithValue("Qty", pCost.Qty)
                cmd.Parameters.AddWithValue("Selected_Supplier", pCost.SelectedSupplier)
                cmd.Parameters.AddWithValue("Reference_Supplier", pCost.ReferenceSupplier)
                cmd.Parameters.AddWithValue("RegUser", pUser)


                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDetail(ByVal pCost As clsIAPrice, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_IA_Insert_Detail]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("IA_Number", pCost.IAPriceNo)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("Material_No", pCost.MaterialNo)
                cmd.Parameters.AddWithValue("Qty", pCost.Qty)
                cmd.Parameters.AddWithValue("Selected_Supplier", pCost.SelectedSupplier)
                cmd.Parameters.AddWithValue("Reference_Supplier", pCost.ReferenceSupplier)
                cmd.Parameters.AddWithValue("RegUser", pUser)


                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function SubmitData(ByVal pCost As clsIAPrice, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_IA_Submit]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("IANumber", pCost.IAPriceNo)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("IAStatus", "1")
                cmd.Parameters.AddWithValue("RegUser", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetIAData(pIANumber As String, pRevision As Integer, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_IA_GetDataHeader '" & pIANumber & "','" & pRevision & "','" & pUser & "' "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function GetDataSupplier(pCENo As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Distinct Supplier_Code As Code From CP_Header H  " & vbCrLf & _
                      "Inner Join CP_Detail D " & vbCrLf & _
                      "On H.CE_Number = D.CE_Number " & vbCrLf & _
                      "Where H.CE_Number = @CENumber"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("CENumber", pCENo)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function CheckApprovalIA(pIANumber As String, pRev As String, pUser As String, Optional ByRef pErr As String = "") As Boolean
        Dim dt As New DataTable
        Dim sql As String
        Dim iBool As Boolean = True

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_IAPrice_Approval_CheckApproval"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("IANumber", pIANumber)
                cmd.Parameters.AddWithValue("Rev", pRev)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)

                If dt.Rows.Count > 0 Then
                    iBool = False
                End If

            End Using

            Return iBool

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DisabledForm(pIANumber As String, pRev As String, Optional ByRef pErr As String = "") As DataSet
        Dim dt As New DataTable
        Dim sql As String
        Dim iBool As Boolean = True

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select * FROM IA_Header a WHERE a.IAStatus<>'0' AND IA_Number='" & pIANumber & "' AND Revision_No = '" & pRev & "' "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

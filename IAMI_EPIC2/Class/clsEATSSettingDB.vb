﻿Imports System.Data.SqlClient

Public Class ClsEATSSettingDB
    Shared Function GetData()
        Try
            Dim pConStr As String = Sconn.Stringkoneksi
            Using cn As New SqlConnection(pConStr)
                Dim q As String = "select * from EATSSetting"
                Dim cmd As New SqlCommand(q, cn)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim com As New ClsEATSSetting
                If dt.Rows.Count > 0 Then
                    With dt.Rows(0)
                        com.CompanyName = .Item("CompanyName") & ""
                        com.Address = .Item("Address") & ""
                        com.Phone1 = .Item("Phone1") & ""
                        com.Phone2 = .Item("Phone2") & ""
                        com.Fax = .Item("Fax") & ""
                        com.Email = .Item("Email") & ""
                        com.ContactPerson = .Item("ContactPerson") & ""
                        com.GSTActiveStatus = .Item("GSTActiveStatus") & ""
                        com.PendingActiveStatus = .Item("PendingActiveStatus") & ""
                        com.AndonYellowBalance = .Item("AndonYellowBalance")
                        com.AndonRedBalance = .Item("AndonRedBalance")
                    End With
                End If
                Return com
            End Using
        Catch ex As Exception
            Throw New Exception("clsEATSSettingDB.GetData error: " & ex.Message)
        End Try
    End Function

    Shared Function Update(ByVal pCompany As ClsEATSSetting) As Integer
        Try
            Dim pConStr As String = Sconn.Stringkoneksi
            Using cn As New SqlConnection(pConStr)
                cn.Open()
                Dim q As String =
                    "Delete from EATSSetting " & vbCrLf &
                    "Insert into EATSSetting (ID, CompanyName, Address, Phone1, Phone2, Fax, Email, ContactPerson, " & vbCrLf &
                    "GSTActiveStatus, PendingActiveStatus, AndonYellowBalance, AndonRedBalance " & vbCrLf &
                    ") values (" & vbCrLf &
                    "1, @CompanyName, @Address, @Phone1, @Phone2, @Fax, @Email, @ContactPerson, " & vbCrLf &
                    "@GSTActiveStatus, @PendingActiveStatus, @AndonYellowBalance, @AndonRedBalance )" & vbCrLf
                Dim cmd As New SqlCommand(q, cn)
                cmd.CommandType = CommandType.Text
                With cmd.Parameters
                    .AddWithValue("CompanyName", pCompany.CompanyName)
                    .AddWithValue("Address", pCompany.Address)
                    .AddWithValue("Phone1", pCompany.Phone1)
                    .AddWithValue("Phone2", pCompany.Phone2)
                    .AddWithValue("Fax", pCompany.Fax)
                    .AddWithValue("Email", pCompany.Email)
                    .AddWithValue("ContactPerson", pCompany.ContactPerson)

                    .AddWithValue("GSTActiveStatus", pCompany.GSTActiveStatus)
                    .AddWithValue("PendingActiveStatus", pCompany.PendingActiveStatus)
                    .AddWithValue("AndonYellowBalance", pCompany.AndonYellowBalance)
                    .AddWithValue("AndonRedBalance", pCompany.AndonRedBalance)
                End With
                Dim i As Integer
                i = cmd.ExecuteNonQuery

                Return i
            End Using
        Catch ex As Exception
            Throw New Exception("clsEATSSettingDB.Update error: " & ex.Message)
        End Try
    End Function

    Public Shared Function getDataTableEATSSetting(Optional ByRef pErr As String = "") As DataTable
        Dim dt As New DataTable
        Dim pConStr As String = Sconn.Stringkoneksi
        Try
            Using cn As New SqlConnection(pConStr)
                Dim q As String = "sp_EATSSetting_Sel"
                Dim cmd As New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = q
                cmd.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter(cmd)                
                da.Fill(dt)
                da.Dispose()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            pErr = "clsEATSSettingDB.getDataTableEATSSetting error: " & ex.Message
            Return Nothing
        End Try
        Return dt
    End Function
End Class

﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 03 Oct 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################

Imports System.Data.SqlClient

Public Class clsPRAssignDB
    Public Shared Function GetList(pDateFrom As String, pDateTo As String, pPRType As String, pDepartment As String, pSection As String, pStatus As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = " Select PH.*, Isnull(Dept_ApprovedStatus,'0') As DeptStatus, Isnull(GM_ApprovedStatus,'0') As ManagerStatus, Isnull(Acceptance_ApprovedStatus,'0') As AcceptanceStatus,  DepartmentName, SectionName, " & vbCrLf & _
                '                            " REPLACE(ISNULL(CONVERT(DATE, Dept_ApprovedDate), ''), '1900-01-01', '') DeptDate, " & vbCrLf & _
                '                            " REPLACE(ISNULL(CONVERT(DATE, GM_ApprovedDate), ''), '1900-01-01', '') ManagerDate, " & vbCrLf & _
                '                            " REPLACE(ISNULL(CONVERT(DATE, Acceptance_ApprovedDate), ''), '1900-01-01', '') AcceptanceDate, " & vbCrLf & _
                '                            " PRBudgetName, PRTypeName, Urgent_Status " & vbCrLf & _
                '                            " From (Select * From PR_Header Where PR_Status = '3') PH  " & vbCrLf & _
                '                            " Left Join  " & vbCrLf & _
                '                            " (Select Par_Code, Par_Description As DepartmentName From Mst_Parameter Where Par_Group = 'Department') DG " & vbCrLf & _
                '                            " On PH.Department_Code = DG.Par_Code " & vbCrLf & _
                '                            " Left Join  " & vbCrLf & _
                '                            " (Select Par_Code, Par_Description As SectionName From Mst_Parameter Where Par_Group = 'Section') SC " & vbCrLf & _
                '                            " On PH.Section_Code = SC.Par_Code " & vbCrLf & _
                '                            " Left Join  " & vbCrLf & _
                '                            " (Select Par_Code, Par_Description As PRBudgetName From Mst_Parameter Where Par_Group = 'PRBudget') PB " & vbCrLf & _
                '                            " On PH.Budget_Code = PB.Par_Code " & vbCrLf & _
                '                            " Left Join  " & vbCrLf & _
                '                            " (Select Par_Code, Par_Description As PRTypeName From Mst_Parameter Where Par_Group = 'PRType') PT " & vbCrLf & _
                '                            " On PH.PRType_Code = PT.Par_Code " & vbCrLf & _
                '                            " Where PH.PR_Date >= '" & pDateFrom & "' And PH.PR_Date <= '" & pDateTo & "' "

                'If pPRType <> "00" And pPRType <> "0" Then
                '    sql = sql + " And PRType_Code = '" & pPRType & "'"
                'End If

                'If pDepartment <> "00" And pDepartment <> "0" Then
                '    sql = sql + " And PH.Department_Code = '" & pDepartment & "'"
                'End If

                'If pSection <> "0000" And pSection <> "0" Then
                '    sql = sql + " And PH.Section_Code = '" & pSection & "'"
                'End If

                sql = "sp_PRAssign_List"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRDateFrom", pDateFrom)
                cmd.Parameters.AddWithValue("PRDateTo", pDateTo)
                cmd.Parameters.AddWithValue("PRType", pPRType)
                cmd.Parameters.AddWithValue("Department", pDepartment)
                cmd.Parameters.AddWithValue("Section", pSection)
                cmd.Parameters.AddWithValue("Status", pStatus)
                'cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataPR(pObj As IAMI_EPIC2.clsPRAcceptance, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = " Select ROW_NUMBER() OVER(ORDER BY PD.Material_No) As No, PH.*, PD.Material_No, PD.Qty, PD.Remarks, IM.Description, IM.Specification, UOMDescription   " & vbCrLf & _
                        " From (Select * From PR_Header Where ISNULL(PR_Status,'0') IN ('3','5') ) PH Inner Join PR_Detail PD On PH.PR_Number = PD.PR_Number And PH.Rev = PD.Rev  " & vbCrLf & _
                        " Left Join Mst_Item IM On IM.Material_No = PD.Material_No " & vbCrLf & _
                        " Left Join " & vbCrLf & _
                        " (Select Rtrim(Par_Code) Code, Rtrim(Par_Description) UOMDescription  From Mst_Parameter Where Par_Group = 'UOM') UO " & vbCrLf & _
                        " On UO.Code = IM.UOM " & vbCrLf & _
                        " Where PH.PR_Number = @PRNo And PH.Rev = @Revision "

                Dim cmd As New SqlCommand(sql, con)
                cmd.Parameters.AddWithValue("PRNo", pObj.PRNumber)
                cmd.Parameters.AddWithValue("Revision", pObj.Revision)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()

                cmd.Dispose()
            End Using

            Return ds
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataDetail(ByVal pObj As clsPRAcceptance, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = " Select PH.*, PD.ItemCode, PD.Qty, PD.Remarks, IM.Description  " & vbCrLf & _
                        " From PRHeader PH Inner Join PRDetail PD On PH.PRNo = PD.PRNo " & vbCrLf & _
                        " Left Join ItemMaster IM On IM.ItemCode = PD.ItemCode " & vbCrLf & _
                        " Where PH.PRNo = @PRNo "

                Dim cmd As New SqlCommand(sql, con)
                cmd.Parameters.AddWithValue("@PRNo", pObj.PRNumber)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()

                cmd.Dispose()
            End Using

            Return ds
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Assign(ByVal pPRAssign As clsPRAssign, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Update PR_Header " & vbCrLf & _
                      "Set PR_Status = '5', PIC_Assign = UPPER(@PIC) ,PIC_PO = UPPER(@PIC_PO),PIC_PO_2 = UPPER(@PIC_PO2),PIC_PO_3 = UPPER(@PIC_PO3), Tender_Status = @Tender" & vbCrLf & _
                      "Where PR_Number = @PRNumber And Rev = @Rev"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("PRNumber", pPRAssign.PRNumber)
                cmd.Parameters.AddWithValue("Tender", pPRAssign.TenderStatus)
                cmd.Parameters.AddWithValue("PIC", pPRAssign.PIC)
                cmd.Parameters.AddWithValue("Rev", pPRAssign.Revision)
                cmd.Parameters.AddWithValue("PIC_PO", pPRAssign.PICPO)
                cmd.Parameters.AddWithValue("PIC_PO2", pPRAssign.PICPO2)
                cmd.Parameters.AddWithValue("PIC_PO3", pPRAssign.PICPO3)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

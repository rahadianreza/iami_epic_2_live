﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 04 Oct 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################

Public Class clsCostEstimation
    Public Property QuotationDateFrom As String
    Public Property QuotationDateTo As String
    Public Property CENumber As String
    Public Property CEDate As Date
    Public Property CEStatus As String
    Public Property CEDateFrom As String
    Public Property CEDateTo As String
    Public Property CEBudget As String
    Public Property CENotes As String
    Public Property Revision As Integer
    Public Property SkipNotes As String
    Public Property QuotationNumber As String
    Public Property QuotationDate As String
    Public Property RFQSetNumber As String
    Public Property RFQNumber As String
    Public Property RFQDate As String
    Public Property SkipBudget As String
    Public Property Supplier As String
    Public Property Price As Double
    Public Property MaterialNo As String
    Public Property Qty As Integer
    Public Property Remarks As String
    Public Property ApprovalNotes As String
    Public Property StatusApproval As String
End Class

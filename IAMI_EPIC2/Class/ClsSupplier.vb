﻿Public Class ClsSupplier
    Public Property Supplier_Code As String
    Public Property Supplier_Name As String
    Public Property Address As String
    Public Property Phone As String
    Public Property Fax As String
    Public Property Kota_Kabupaten As String
    Public Property Country As String
    Public Property Contact_Person As String
    Public Property Email As String
    Public Property Period_Code As String
    Public Property Language_Code As String
    Public Property SupplierType As String
    Public Property RegisterBy As String
    Public Property RegisterDate As String
    Public Property UpdateBy As String
    Public Property UpdateDate As String
End Class

﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. IA Price
'Parameter      :  
'Created By     : Agus
'Created Date   : 15 Dec 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################


Public Class clsIAPrice
    Public Property IAPriceDateFrom As String
    Public Property IAPriceDateTo As String
    Public Property IAPriceNo As String
    Public Property IAPriceDate As String
    Public Property CENumber As String
    Public Property MaterialNo As String
    Public Property Description As String
    Public Property Note As String
    Public Property ApprovalNote As String
    Public Property UoM As String
    Public Property Qty As Integer
    Public Property Currency As String
    Public Property Revision As String
    Public Property IAStatus As String

    Public Property Supplier1Quotation As String
    Public Property Supplier1FinalPrice As Double
    Public Property Supplier2Quotation As String
    Public Property Supplier2FinalPrice As Double
    Public Property Supplier3Quotation As String
    Public Property Supplier3FinalPrice As Double
    Public Property Supplier4Quotation As String
    Public Property Supplier4FinalPrice As Double
    Public Property Supplier5Quotation As String
    Public Property Supplier5FinalPrice As Double

    Public Property SelectedSupplier As String
    Public Property ReferenceSupplier As String

    Public Property IAValidDate As String
    Public Property StatusApproval As String

End Class

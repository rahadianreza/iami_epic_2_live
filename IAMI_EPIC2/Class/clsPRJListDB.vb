﻿Imports System.Data.SqlClient

Public Class clsPRJListDB
#Region "Header"
    Public Shared Function getHeaderGrid(_prjdatefrom As String, _prjdateto As String, _prjtype As String, pUser As String, _
                                           pStatus As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_PRJList_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_Date_From", _prjdatefrom)
                cmd.Parameters.AddWithValue("@Project_Date_To", _prjdateto)
                cmd.Parameters.AddWithValue("@Project_Type", _prjtype)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Sub InsertHeaderData(_prjname As String, _prjdate As String, _prjtype As String, _startPeriod As String, _endPeriod As String, _
                                            _rateUSD As Double, _rateYEN As Double, _rateBATH As Double, _otssample As String, _
                                            _soptiming As String, pUser As String, Optional ByRef pErr As String = "", Optional ByRef ProjectIDOutput As String = "")

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_PRJList_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_Name", _prjname)
                cmd.Parameters.AddWithValue("@Project_Date", _prjdate)
                cmd.Parameters.AddWithValue("@Project_Type", _prjtype)
                cmd.Parameters.AddWithValue("@Start_Period", _startPeriod)
                cmd.Parameters.AddWithValue("@End_Period", _endPeriod)
                cmd.Parameters.AddWithValue("@Rate_USD_IDR", _rateUSD)
                cmd.Parameters.AddWithValue("@Rate_YEN_IDR", _rateYEN)
                cmd.Parameters.AddWithValue("@Rate_BATH_IDR", _rateBATH)
                cmd.Parameters.AddWithValue("@Shec_OTS_Sample", _otssample)
                cmd.Parameters.AddWithValue("@Shec_SOP_Timing", _soptiming)
                cmd.Parameters.AddWithValue("@Register_By", pUser)
                'cmd.Parameters.AddWithValue("@Project_ID_Out", ProjectIDOutput)
                Dim outPutParameter As SqlParameter = New SqlParameter()
                outPutParameter.ParameterName = "@Project_ID_Out"
                outPutParameter.SqlDbType = System.Data.SqlDbType.VarChar
                outPutParameter.Direction = System.Data.ParameterDirection.Output
                outPutParameter.Size = 10
                cmd.Parameters.Add(outPutParameter)

                con.Open()
                cmd.ExecuteNonQuery()
                ProjectIDOutput = outPutParameter.Value.ToString()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Throw New Exception(ex.Message)
        End Try

    End Sub

    Public Shared Sub UpdateHeaderData(_prjid As String, _prjname As String, _prjdate As String, _
                                           _rateUSD As Double, _rateYEN As Double, _rateBATH As Double, _otssample As String, _
                                           _soptiming As String, pUser As String, Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_PRJList_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Project_Name", _prjname)
                cmd.Parameters.AddWithValue("@Project_Date", _prjdate)
                cmd.Parameters.AddWithValue("@Rate_USD_IDR", _rateUSD)
                cmd.Parameters.AddWithValue("@Rate_YEN_IDR", _rateYEN)
                cmd.Parameters.AddWithValue("@Rate_BATH_IDR", _rateBATH)
                cmd.Parameters.AddWithValue("@Shec_OTS_Sample", _otssample)
                cmd.Parameters.AddWithValue("@Shec_SOP_Timing", _soptiming)
                cmd.Parameters.AddWithValue("@Update_By", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Throw New Exception(ex.Message)
        End Try

    End Sub

    Public Shared Sub DeleteHeaderData(_prjid As String, pUser As String, Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_PRJList_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_Id", _prjid)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            'Throw New Exception(ex.Message)
        End Try

    End Sub

    Public Shared Function FillCombo(pUserID As String, pAdminStatus As String, _
                                     Optional ByRef pGroup As String = "", _
                                     Optional ByRef pParent As String = "", _
                                     Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pAdminStatus = "1" Then
                    sql = "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description  " & vbCrLf & _
                            "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf

                    If pParent <> "ALL" And pParent <> "" Then
                        sql = sql + "AND COALESCE(Par_ParentCode,'') = '" & pParent & "'"
                    End If

                Else
                    sql = sql + "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description " & vbCrLf & _
                        "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf & _
                        "AND Par_Code = (SELECT COALESCE(" & pGroup & "_Code,'')" & pGroup & "_Code FROM dbo.UserSetup WHERE UserID = '" & pUserID & "')"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboOnlyPT01PT02(pUserID As String, pAdminStatus As String, _
                                     Optional ByRef pGroup As String = "", _
                                     Optional ByRef pParent As String = "", _
                                     Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pAdminStatus = "1" Then
                    sql = "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description  " & vbCrLf & _
                            "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' AND Par_Code IN ('PT01', 'PT02') " & vbCrLf

                    If pParent <> "ALL" And pParent <> "" Then
                        sql = sql + "AND COALESCE(Par_ParentCode,'') = '" & pParent & "'"
                    End If

                Else
                    sql = sql + "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description " & vbCrLf & _
                        "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf & _
                        "AND Par_Code = (SELECT COALESCE(" & pGroup & "_Code,'')" & pGroup & "_Code FROM dbo.UserSetup WHERE UserID = '" & pUserID & " AND Par_Code IN ('PT01', 'PT02')')"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboWithALL(pUserID As String, pAdminStatus As String, _
                                    Optional ByRef pGroup As String = "", _
                                    Optional ByRef pParent As String = "", _
                                    Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pAdminStatus = "1" Then
                    sql = "SELECT 'ALL' Code, 'ALL' Description UNION ALL SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description  " & vbCrLf & _
                            "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf

                    If pParent <> "ALL" And pParent <> "" Then
                        sql = sql + "AND COALESCE(Par_ParentCode,'') = '" & pParent & "'"
                    End If

                Else
                    sql = sql + "SELECT 'ALL' Code, 'ALL' Description UNION ALL SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description " & vbCrLf & _
                        "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf & _
                        "AND Par_Code = (SELECT COALESCE(" & pGroup & "_Code,'')" & pGroup & "_Code FROM dbo.UserSetup WHERE UserID = '" & pUserID & "')"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboOnlyPT01PT02WithALL(pUserID As String, pAdminStatus As String, _
                                    Optional ByRef pGroup As String = "", _
                                    Optional ByRef pParent As String = "", _
                                    Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pAdminStatus = "1" Then
                    sql = "SELECT 'ALL' Code, 'ALL' Description UNION ALL SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description  " & vbCrLf & _
                            "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' AND Par_Code IN ('PT01', 'PT02') " & vbCrLf

                    If pParent <> "ALL" And pParent <> "" Then
                        sql = sql + "AND COALESCE(Par_ParentCode,'') = '" & pParent & "'"
                    End If

                Else
                    sql = sql + "SELECT 'ALL' Code, 'ALL' Description UNION ALL SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description " & vbCrLf & _
                        "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf & _
                        "AND Par_Code = (SELECT COALESCE(" & pGroup & "_Code,'')" & pGroup & "_Code FROM dbo.UserSetup WHERE UserID = '" & pUserID & " AND Par_Code IN ('PT01', 'PT02')')"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboProjectTypeAcceptance(type As String, pic As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "sp_ItemListSourcing_Drawing_Acc_Combo"

                'sql = "SELECT 'ALL' as Code, 'ALL' as Description  UNION ALL SELECT DISTINCT B.Project_Type as Code, C.Par_Description as Description " & _
                '        "FROM Item_List_Sourcing A " & _
                '        "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID " & _
                '        "INNER JOIN Mst_Parameter C ON C.Par_Code = B.Project_Type " & _
                '        "WHERE Status_Drawing = '1' AND  Par_Group = 'ProjectType' AND A.PIC = (SELECT UserInitial FROM UserSetup WHERE UserID='" + pic + "') "
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Type", type)
                cmd.Parameters.AddWithValue("@Param1", pic)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function FillComboProjectTypeGateISheet(pic As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "SELECT 'ALL' as Code, 'ALL' as Description " & _
                        "UNION ALL " & _
                        "SELECT DISTINCT B.Project_Type as Code, C.Par_Description as Description " & _
                        "FROM Item_List_Sourcing A " & _
                        "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID " & _
                        "INNER JOIN Mst_Parameter C ON C.Par_Code = B.Project_Type " & _
                        "WHERE Status_Drawing = '1' AND  Par_Group = 'ProjectType' AND A.PIC = '" + pic + "' " & _
                        "AND EXISTS(SELECT TOP 1 1 FROM Drawing_Acceptance DA WHERE DA.Project_ID = A.Project_ID AND DA.Group_ID = A.Group_ID AND " & _
                        " DA.Commodity = A.Commodity AND DA.Group_Comodity = A.Group_Comodity)"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboProjectTypeGateISheetApproval(pic As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "SELECT 'ALL' as Code, 'ALL' as Description " & _
                        "UNION ALL " & _
                        "SELECT DISTINCT B.Project_Type as Code, C.Par_Description as Description " & _
                        "FROM Item_List_Sourcing A " & _
                        "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID " & _
                        "INNER JOIN Mst_Parameter C ON C.Par_Code = B.Project_Type " & _
                        "WHERE Status_Drawing = '1' AND  Par_Group = 'ProjectType' " & _
                        "AND EXISTS(SELECT TOP 1 1 FROM Drawing_Acceptance DA WHERE DA.Project_ID = A.Project_ID AND DA.Group_ID = A.Group_ID AND " & _
                        " DA.Commodity = A.Commodity AND DA.Group_Comodity = A.Group_Comodity)"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboProjectTypeGrouping(pic As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()


                sql = "SELECT DISTINCT B.Project_Type as Code, C.Par_Description as Description " & _
                        "FROM Item_List_Sourcing A " & _
                        "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID " & _
                        "INNER JOIN Mst_Parameter C ON C.Par_Code = B.Project_Type " & _
                        "WHERE Status_Drawing = '1' AND Status_Acceptance_date IS NOT NULL AND Par_Group = 'ProjectType' AND A.PIC = '" + pic + "' "
                    


                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function FillComboProject(projtype As String, pUserID As String, pAdminStatus As String, _
                                     Optional ByRef pGroup As String = "", _
                                     Optional ByRef pParent As String = "", _
                                     Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "Select Project_ID, Project_Name From Proj_Header Where Project_Type='" + projtype + "'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboProjectWithALL(projtype As String, pUserID As String, pAdminStatus As String, _
                                     Optional ByRef pGroup As String = "", _
                                     Optional ByRef pParent As String = "", _
                                     Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "SELECT 'ALL' Project_ID, 'ALL' Project_Name UNION ALL Select Project_ID, Project_Name From Proj_Header Where 1 = CASE WHEN Project_Type = '" + projtype + "' THEN 1 WHEN '" + projtype + "' = 'ALL' THEN 1 ELSE 0 END"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function CheckDatauploaded(prjid As String, pErr As String) As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "SELECT COUNT(*) FROM Item_List_Sourcing WHERE Project_ID = '" + prjid + "'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboProjectISheet(projtype As String, pUserID As String, pAdminStatus As String, _
                                    Optional ByRef pGroup As String = "", _
                                    Optional ByRef pParent As String = "", _
                                    Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                If projtype = "" Or projtype = "ALL" Then
                    sql = "SELECT 'ALL' as Project_ID, 'ALL' as Project_Name UNION ALL " & _
                          "SELECT DISTINCT A.Project_ID, B.Project_Name " & _
                          "FROM Drawing_Acceptance A " & _
                          "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID  WHERE Buyer = '" + pUserID + "'"
                Else
                    sql = "SELECT 'ALL' as Project_ID, 'ALL' as Project_Name UNION ALL " & _
                          "SELECT DISTINCT A.Project_ID, B.Project_Name " & _
                          "FROM Drawing_Acceptance A " & _
                          "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID  WHERE Project_Type='" + projtype + "' AND Buyer = '" + pUserID + "'"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboProjectISheetRelease(projtype As String, pUserID As String, pAdminStatus As String, _
                                   Optional ByRef pGroup As String = "", _
                                   Optional ByRef pParent As String = "", _
                                   Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                If projtype = "" Or projtype = "ALL" Then
                    sql = "SELECT 'ALL' as Project_ID, 'ALL' as Project_Name UNION ALL " & _
                          "SELECT DISTINCT A.Project_ID, B.Project_Name " & _
                          "FROM Drawing_Acceptance A " & _
                          "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID  "
                Else
                    sql = "SELECT 'ALL' as Project_ID, 'ALL' as Project_Name UNION ALL " & _
                          "SELECT DISTINCT A.Project_ID, B.Project_Name " & _
                          "FROM Drawing_Acceptance A " & _
                          "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID  WHERE Project_Type='" + projtype + "'"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboProjectISheetApproval(projtype As String, pUserID As String, pAdminStatus As String, _
                                   Optional ByRef pGroup As String = "", _
                                   Optional ByRef pParent As String = "", _
                                   Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                If projtype = "" Or projtype = "ALL" Then
                    sql = "SELECT 'ALL' as Project_ID, 'ALL' as Project_Name UNION ALL " & _
                          "SELECT DISTINCT A.Project_ID, B.Project_Name " & _
                          "FROM Drawing_Acceptance A " & _
                          "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID"
                Else
                    sql = "SELECT 'ALL' as Project_ID, 'ALL' as Project_Name UNION ALL " & _
                          "SELECT DISTINCT A.Project_ID, B.Project_Name " & _
                          "FROM Drawing_Acceptance A " & _
                          "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID  WHERE Project_Type='" + projtype + "'"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataPRJ(_prjid As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_PRJData_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_Id", _prjid)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
#End Region

#Region "Detail"
    Public Shared Function getDetailGrid(_prjid As String, pUser As String, _
                                           pStatus As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_PRJListDetail_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetStartYear(_projid As String) As String
        Dim ds As New DataSet
        Try
            'sp_PRJListDetail_GenerateYear]
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT start_year FROM Proj_Header WHERE Project_ID = '" & _projid & "'", con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds.Tables(0).Rows(0)(0).ToString()
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDetail(ByVal pSes As ClsPRJList, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_PRJListDetail_Ins"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", pSes.ProjectId)
                cmd.Parameters.AddWithValue("@Vehicle_Code", pSes.VehicleCode)
                cmd.Parameters.AddWithValue("@Year1", pSes.Year1)
                cmd.Parameters.AddWithValue("@Year2", pSes.Year2)
                cmd.Parameters.AddWithValue("@Year3", pSes.Year3)
                cmd.Parameters.AddWithValue("@Total_Volume_Rfq", pSes.TotalVolumeRfq)
                cmd.Parameters.AddWithValue("@Remarks", pSes.Remarks)
                cmd.Parameters.AddWithValue("@Register_By", pSes.RegisterBy)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function UpdateDetail(ByVal pSes As ClsPRJList, newVehicleCode As String, ValueYear As Decimal, Year As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_PRJListDetail_Upd"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", pSes.ProjectId)
                cmd.Parameters.AddWithValue("@Vehicle_Code", pSes.VehicleCode)
                cmd.Parameters.AddWithValue("@New_Vehicle_Code", newVehicleCode)
                cmd.Parameters.AddWithValue("@Year", Year)
                cmd.Parameters.AddWithValue("@ValueYear", ValueYear)
                'cmd.Parameters.AddWithValue("@Year1", pSes.Year1)
                'cmd.Parameters.AddWithValue("@Year2", pSes.Year2)
                'cmd.Parameters.AddWithValue("@Year3", pSes.Year3)
                cmd.Parameters.AddWithValue("@Total_Volume_Rfq", pSes.TotalVolumeRfq)
                cmd.Parameters.AddWithValue("@Remarks", IIf(String.IsNullOrEmpty(pSes.Remarks), DBNull.Value, pSes.Remarks))
                cmd.Parameters.AddWithValue("@Update_By", pSes.RegisterBy)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function DeleteDetail(ByVal pSes As ClsPRJList, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_PRJListDetail_Del"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", pSes.ProjectId)
                cmd.Parameters.AddWithValue("@Vehicle_Code", pSes.VehicleCode)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

#End Region

    Public Shared Function getVariant(_prjid As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT Variant_Name FROM Item_List_Sourcing_Variant WHERE Project_ID = '" & _prjid & "'", con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function GetYear(_projid As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Dim sql As String = "sp_PRJListDetail_GenerateYear"

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", _projid)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

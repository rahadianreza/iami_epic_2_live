﻿Public Class ClsIADGSparePart
    Public Property ProjectYear As String
    Public Property ProjectID As String
    Public Property PartNo As String
    Public Property PartName As String
    Public Property Model As String
    Public Property NewMulti As String
    Public Property SampleIADGCompare As String
    Public Property SampleUOM As String
    Public Property VolumePerMonth As String
    Public Property CostTarget As String
    Public Property Priority As String

End Class

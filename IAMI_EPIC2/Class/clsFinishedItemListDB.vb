﻿
Imports System.Data.SqlClient

Public Class clsFinishedItemListDB

    Public Shared Function GenerateNewMaterialNo(pGroup As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String

                'sql = "Select 'MTRL' + Right(Isnull(Right(Max(Material_No),6),0) + 1000001,6)  As Material_No From Mst_Item"
                sql = "sp_FinishedItemList_GenerateNewPartNo"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Digit", 10)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataForExcel(pGroup As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String

                sql = "Select * From Mst_FinishedUnit"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetList(pVariant As String, pSupplier As String, Optional ByRef pErr As String = "") As List(Of clsFinishedItemList)
        Dim sql As String
        Dim cmd As SqlCommand

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_FinishedUnit_GetList]"
                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Variant", pVariant)
                cmd.Parameters.AddWithValue("Type", pSupplier)

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)

                Dim list As New List(Of clsFinishedItemList)
                For i = 0 To dt.Rows.Count - 1
                    Dim updatedate As String
                    If IsDBNull(dt.Rows(i).Item("UpdateDate")) Then
                        updatedate = ""
                    Else
                        updatedate = dt.Rows(i).Item("UpdateDate")
                    End If

                    Dim itemdata As New clsFinishedItemList With {.VariantCode = Trim(dt.Rows(i).Item("VariantCode") & ""),
                                                          .VariantName = Trim(dt.Rows(i).Item("VariantName") & ""),
                                                          .MaterialFUNo = Trim(dt.Rows(i).Item("MaterialFU_No") & ""),
                                                          .Type = Trim(dt.Rows(i).Item("Type") & ""),
                                                          .UOM = dt.Rows(i).Item("UOM"),
                                                          .LastIAPrice = IIf(IsDBNull(dt.Rows(i).Item("LastIAPrice")), 0, dt.Rows(i).Item("LastIAPrice")),
                                                          .CreateDate = IIf(IsDBNull(dt.Rows(i).Item("RegisterDate")), "", dt.Rows(i).Item("RegisterDate")),
                                                          .CreateUser = dt.Rows(i).Item("RegisterUser"),
                                                          .UpdateUser = IIf(IsDBNull(dt.Rows(i).Item("UpdateUser")), "", dt.Rows(i).Item("UpdateUser")),
                                                          .UpdateDate = IIf(IsDBNull(dt.Rows(i).Item("UpdateDate")), "", dt.Rows(i).Item("UpdateDate"))}

                    list.Add(itemdata)
                Next

                Return list
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'Public Shared Function GetTable(Optional ByRef pErr As String = "") As DataTable
    '    Dim sql As String
    '    Dim cmd As SqlCommand

    '    Try
    '        Using con As New SqlConnection(Sconn.Stringkoneksi)
    '            con.Open()

    '            sql = "[sp_ItemMaster_GetList]"
    '            cmd = New SqlCommand(sql, con)
    '            cmd.CommandType = CommandType.StoredProcedure
    '            Dim da As New SqlDataAdapter(cmd)
    '            Dim dt As New DataTable
    '            da.Fill(dt)


    '            Return dt
    '        End Using

    '    Catch ex As Exception
    '        pErr = ex.Message
    '        Return Nothing
    '    End Try
    'End Function

    Public Shared Function GetDataLastSupplier(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As LastSupplier From "
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataPRType(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As Code, Par_Description As Description From Mst_Parameter Where Par_Group = 'PRType'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataCategoryFromItem(pItem As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Category' And Par_ParentCode = (Select GroupItem From Mst_Item Where Material_No = '" & pItem & "')"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetFilterDataCategory(pGroup As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pGroup = "" Then
                    sql = "Select '00' As Code, 'ALL' Description UNION ALL Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Category'"
                Else
                    sql = "Select '00' As Code, 'ALL' Description UNION ALL Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Category' And Par_ParentCode = '" & pGroup & "'"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataCategory(pItem As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Category' And Par_ParentCode = '" & pItem & "'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataGroupFromItem(pItem As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As Group_Code, Par_Description As Group_Name From Mst_Parameter Where Par_Group = 'GroupItem' And Par_ParentCode = (Select PRType From Mst_Item Where Material_No = '" & pItem & "')"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataGroup(pItem As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As Group_Code, Par_Description As Group_Name From Mst_Parameter Where Par_Group = 'GroupItem' And Par_ParentCode = '" & pItem & "'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataUOM(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As UOM From Mst_Parameter Where Par_Group = 'UOM'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataItem(ByVal pObj As clsFinishedItemList, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = " SELECT VariantCode,VariantName,MaterialFU_No,Type,UOM, 0 LastIAPrice FROM  dbo.Mst_FinishedUnit " & vbCrLf

                If pObj.VariantCode <> "" Then
                    sql = sql + " where VariantCode = @VariantCode "
                End If
                Dim cmd As New SqlCommand(sql, con)
                If pObj.VariantCode <> "" Then
                    cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)
                End If
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                If pObj.VariantCode <> "" Then
                    cmd.Parameters.Clear()
                End If
                cmd.Dispose()
            End Using

            Return ds
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try

    End Function

    Public Shared Function Insert(ByVal pItem As clsFinishedItemList, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Ins]"

                sql = "INSERT INTO Mst_FinishedUnit  " & vbCrLf & _
                      "(VariantCode,VariantName,MaterialFU_No,Type,UOM, RegisterDate, RegisterUser "

                If pItem.Picture1 IsNot Nothing Then
                    sql = sql + ", Picture1 "
                End If

                If pItem.Picture2 IsNot Nothing Then
                    sql = sql + ", Picture2 "
                End If

                If pItem.Picture3 IsNot Nothing Then
                    sql = sql + ", Picture3 "
                End If

                sql = sql + " ) VALUES  " & vbCrLf & _
                      "(@VariantCode,@VariantName,@MaterialFU_No,@Type,@UOM,GETDATE(), UPPER(@UserId) "

                If pItem.Picture1 IsNot Nothing Then
                    sql = sql + ", @Picture1 "
                End If

                If pItem.Picture2 IsNot Nothing Then
                    sql = sql + ", @Picture2 "
                End If

                If pItem.Picture3 IsNot Nothing Then
                    sql = sql + ", @Picture3 "
                End If

                sql = sql + ") "
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                'cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("VariantCode", pItem.VariantCode)
                cmd.Parameters.AddWithValue("VariantName", pItem.VariantName)
                cmd.Parameters.AddWithValue("MaterialFU_No", pItem.MaterialFUNo)
                cmd.Parameters.AddWithValue("Type", pItem.Type)
                cmd.Parameters.AddWithValue("UOM", pItem.UOM)
                cmd.Parameters.AddWithValue("UserId", pUser)


                If pItem.Picture1 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture1", pItem.Picture1)
                End If
                If pItem.Picture2 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture2", pItem.Picture2)
                End If
                If pItem.Picture3 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture3", pItem.Picture3)
                End If

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Update(ByVal pItem As clsFinishedItemList, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Upd]"
                sql = "UPDATE Mst_FinishedUnit " & vbCrLf & _
                      "SET VariantName = @VariantName,MaterialFU_No = @MaterialFU_No,Type = @Type,UOM = @UOM,UpdateUser = UPPER(@UserId), UpdateDate = GETDATE() "

                If pItem.Picture1 IsNot Nothing Then
                    sql = sql + ", Picture1 = @Picture1 "
                End If
                If pItem.Picture2 IsNot Nothing Then
                    sql = sql + ", Picture2 = @Picture2 "
                End If
                If pItem.Picture3 IsNot Nothing Then
                    sql = sql + ", Picture3 = @Picture3 "
                End If

                sql = sql + "WHERE VariantCode = @VariantCode"
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("VariantCode", pItem.VariantCode)
                cmd.Parameters.AddWithValue("VariantName", pItem.VariantName)
                cmd.Parameters.AddWithValue("MaterialFU_No", pItem.MaterialFUNo)
                cmd.Parameters.AddWithValue("Type", pItem.Type)
                cmd.Parameters.AddWithValue("UOM", pItem.UOM)
                'cmd.Parameters.AddWithValue("LastIAPrice", pItem.LastIAPrice)
                'cmd.Parameters.AddWithValue("LastSupplier", pItem.LastSupplier)
                cmd.Parameters.AddWithValue("UserId", pUser)
                If pItem.Picture1 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture1", pItem.Picture1)
                End If
                If pItem.Picture2 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture2", pItem.Picture2)
                End If
                If pItem.Picture3 IsNot Nothing Then
                    cmd.Parameters.AddWithValue("Picture3", pItem.Picture3)
                End If

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Delete(ByVal pItem As clsFinishedItemList, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Del]"
                sql = "Delete From Mst_FinishedUnit Where VariantCode = '" & pItem.VariantCode & "'"
                cmd = New SqlCommand(sql, con)

                'cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("VariantCode", pItem.VariantCode)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteAttachment(ByVal pItem As clsFinishedItemList, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Del]"
                sql = "sp_Mst_FinishedUnit_DeleteAttachment_All"
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("VariantCode", pItem.VariantCode)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

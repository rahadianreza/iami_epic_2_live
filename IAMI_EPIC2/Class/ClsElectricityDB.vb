﻿Imports System.Data.SqlClient

Public Class ClsElectricityDB
    Public Shared Function getVAKind(ByVal RateClass As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Electricity_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RateClass", RateClass)
                cmd.Parameters.AddWithValue("VaKind", "")
                cmd.Parameters.AddWithValue("PeriodFrom", "")
                cmd.Parameters.AddWithValue("PeriodTo", "")
                cmd.Parameters.AddWithValue("Status", "2")
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getVAKindAdd(ByVal RateClass As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Electricity_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RateClass", RateClass)
                cmd.Parameters.AddWithValue("VaKind", "")
                cmd.Parameters.AddWithValue("PeriodFrom", "")
                cmd.Parameters.AddWithValue("PeriodTo", "")
                cmd.Parameters.AddWithValue("Status", "3")
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getlistds(ByVal pFrom As String, ByVal pTo As String, ByVal RateClass As String, ByVal VAKind As String,  Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Electricity_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RateClass", RateClass)
                cmd.Parameters.AddWithValue("VaKind", VAKind)
                cmd.Parameters.AddWithValue("PeriodFrom", pFrom)
                cmd.Parameters.AddWithValue("PeriodTo", pTo)
                cmd.Parameters.AddWithValue("Status", "1")
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal RateClass As String, ByVal VaKind As String, ByVal Period As String, ByVal RegulerPostPaid As Decimal, ByVal RegulerPostPaidWBP As Decimal, ByVal RegulerPostPaidLWBP As Decimal, ByVal PrePostPaid As Decimal, ByVal Status As String, ByVal UserID As String, Optional ByRef pErr As String = "") As Integer
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String = ""
                sql = "sp_Electricity_Ins_Upd_Del"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RateClass", RateClass)
                cmd.Parameters.AddWithValue("VaKind", VaKind)
                cmd.Parameters.AddWithValue("Period", Period)
                cmd.Parameters.AddWithValue("RegulerPostPaid", RegulerPostPaid)
                cmd.Parameters.AddWithValue("RegulerPostPaidWBP", RegulerPostPaidWBP)
                cmd.Parameters.AddWithValue("RegulerPostPaidLWBP", RegulerPostPaidLWBP)
                cmd.Parameters.AddWithValue("PrePostPaid", PrePostPaid)
                cmd.Parameters.AddWithValue("Status", Status)
                cmd.Parameters.AddWithValue("UserID", UserID)
                i = cmd.ExecuteNonQuery
                Return i
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Delete(ByVal RateClass As String, ByVal VaKind As String, ByVal Period As String, ByVal UserID As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Electricity_Ins_Upd_Del"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RateClass", RateClass)
                cmd.Parameters.AddWithValue("VaKind", VaKind)
                cmd.Parameters.AddWithValue("Period", Period)
                cmd.Parameters.AddWithValue("RegulerPostPaid", 0)
                cmd.Parameters.AddWithValue("PrePostPaid", 0)
                cmd.Parameters.AddWithValue("Status", "3")
                cmd.Parameters.AddWithValue("UserID", UserID)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function
    
    Public Shared Function ChartSource(ByVal RateClass As String, ByVal VAKind As String, ByVal pFrom As String, ByVal pTo As String, Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Electricity_Chart"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RateClass", RateClass)
                cmd.Parameters.AddWithValue("VaKind", VAKind)
                cmd.Parameters.AddWithValue("PeriodFrom", pFrom)
                cmd.Parameters.AddWithValue("PeriodTo", pTo)

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getVAKind_Chart(ByVal RateClass As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Electricity_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RateClass", RateClass)
                cmd.Parameters.AddWithValue("VaKind", "")
                cmd.Parameters.AddWithValue("PeriodFrom", "")
                cmd.Parameters.AddWithValue("PeriodTo", "")
                cmd.Parameters.AddWithValue("Status", "3")
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 03 Oct 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################

Imports System.Data.SqlClient

Public Class clsCostEstimationAcceptanceUserDB

    Public Shared Function Approve(ByVal pCost As clsCostEstimationAcceptanceUser, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_CostEstimation_Approve]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("ApprovalNotes", pCost.ApprovalNotes)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Reject(ByVal pCost As clsCostEstimationAcceptanceUser, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_CostEstimation_Reject]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function SubmitData(ByVal pCost As clsCostEstimationAcceptanceUser, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_CostEstimation_Submit]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("Rev", pCost.Revision)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateFile(ByVal pCENumber As String, pPath As String, pFile As String, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Update CE_Header Set File_Path = @FilePath, File_Name = @FileName, Update_By = UPPER(@UserId), Update_Date = GETDATE() " & vbCrLf & _
                      "Where CE_Number = @CENumber "

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text

                cmd.Parameters.AddWithValue("CENumber", pCENumber)
                cmd.Parameters.AddWithValue("FilePath", pPath)
                cmd.Parameters.AddWithValue("FileName", pFile)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function UpdateHeader(ByVal pCost As clsCostEstimationAcceptanceUser, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_CostEstimationHeader_Upd]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("RFQSet", pCost.RFQSetNumber)
                cmd.Parameters.AddWithValue("Revision", pCost.Revision)
                cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("CEStatus", pCost.CEStatus)
                cmd.Parameters.AddWithValue("SkipNotes", pCost.SkipNotes)
                cmd.Parameters.AddWithValue("SkipBudget", pCost.SkipBudget)
                cmd.Parameters.AddWithValue("CENotes", pCost.CENotes)
                cmd.Parameters.AddWithValue("CEBudget", pCost.CEBudget)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertHeader(ByVal pCost As clsCostEstimationAcceptanceUser, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_CostEstimationHeader_Ins]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("RFQSet", pCost.RFQSetNumber)
                cmd.Parameters.AddWithValue("Revision", pCost.Revision)
                cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("CEDate", pCost.CEDate)
                cmd.Parameters.AddWithValue("CEStatus", pCost.CEStatus)
                cmd.Parameters.AddWithValue("SkipNotes", pCost.SkipNotes)
                cmd.Parameters.AddWithValue("SkipBudget", pCost.SkipBudget)
                cmd.Parameters.AddWithValue("CENotes", pCost.CENotes)
                cmd.Parameters.AddWithValue("CEBudget", pCost.CEBudget)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateDetail(ByVal pCost As clsCostEstimationAcceptanceUser, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_CostEstimationDetail_Upd]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("Revision", pCost.Revision)
                cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("MaterialNo", pCost.MaterialNo)
                cmd.Parameters.AddWithValue("Qty", pCost.Qty)
                cmd.Parameters.AddWithValue("Price", pCost.Price)
                cmd.Parameters.AddWithValue("Remarks", pCost.Remarks)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDetail(ByVal pCost As clsCostEstimationAcceptanceUser, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_CostEstimationDetail_Ins]"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("Revision", pCost.Revision)
                cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("MaterialNo", pCost.MaterialNo)
                cmd.Parameters.AddWithValue("Qty", pCost.Qty)
                cmd.Parameters.AddWithValue("Price", pCost.Price)
                cmd.Parameters.AddWithValue("Remarks", pCost.Remarks)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function GetMaxCENo(pMonth As Integer, pYear As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select RIGHT(Isnull(RTRIM(Max(RIGHT(CE_Number,3))),0) + 1001,3) As CE_Number " & vbCrLf & _
                      "From CE_Header Where Month(CE_Date) = " & pMonth & " And Year(CE_Date) = " & pYear & " "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

   
    Public Shared Function GetListMaster(pClsCE As clsCostEstimationAcceptanceUser, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_CostEstimationAcceptanceUser_GetList]"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CEDateFrom", pClsCE.CEDateFrom)
                cmd.Parameters.AddWithValue("CEDateTo", pClsCE.CEDateTo)
                cmd.Parameters.AddWithValue("CENumber", pClsCE.CENumber)
                cmd.Parameters.AddWithValue("UserID", pUser)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetListDetail(pRFQSet As String, pSupplier As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = " "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetList(pRFQSet As String, pSupplier As String, pHeadSupplier As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "Select CE_Number, Item_Code, Qty, Price_Estimation, Remarks From CE_Detail Where CE_Number = '" & pCENumber & "'"

                ' sql = "SELECT Material_No As Item_Code , Description, Specification, UOM, Qty, PriceEst, 0 As Amount, '' As Remarks, " & pHeadSupplier & " " & vbCrLf & _
                      ' "FROM ( " & vbCrLf & _
                      ' "Select QH.Supplier_Code, QD.Material_No, IM.Description, IM.UOM, IM.Specification, ISNULL(CE.PriceEst,0) PriceEst, QD.Price, QD.Qty From Quotation_Header QH " & vbCrLf & _
                      ' "Inner Join Quotation_Detail QD  " & vbCrLf & _
                      ' "On QH.Quotation_No = QD.Quotation_No " & vbCrLf & _
                      ' "And QH.Rev = QD.Rev And QH.RFQ_Number = QD.RFQ_Number " & vbCrLf & _
                      ' "Left Join (Select H.CE_Number, RFQ_Set, Material_No, Price_Estimation As PriceEst From CE_Header H Inner Join CE_Detail D On H.CE_Number = D.CE_Number Where CE_Status = '2') CE " & vbCrLf & _
                      ' "On CE.RFQ_Set = QH.RFQ_Set And CE.Material_No = QD.Material_No " & vbCrLf & _
                      ' "Left Join Mst_Item IM On IM.Material_No = QD.Material_No " & vbCrLf & _
                      ' "Where QH.RFQ_Set = '" & pRFQSet & "' And QH.Quotation_Status = '3' " & vbCrLf & _
                      ' " ) as s " & vbCrLf & _
                      ' " PIVOT " & vbCrLf & _
                      ' " ( " & vbCrLf & _
                      ' "    SUM(Price) " & vbCrLf & _
                      ' "     FOR [Supplier_Code] IN (" & pSupplier & ") " & vbCrLf & _
                      ' " ) AS pvt "
				sql = "sp_CE_GetList"					 

                Dim cmd As New SqlCommand(sql, con)
				cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSet)											 
				Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetCEData(pCENumber As String, pRevision As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_CE_GetCEData"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CENumber", pCENumber)
                cmd.Parameters.AddWithValue("Revision", pRevision)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetCEApprovalData(pCENumber As String, pRevision As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select CH.CE_Number, CH.Revision_No, CH.CE_Date, Isnull(CH.CE_Status,'0') As CE_Status, Skip_Budget, Isnull(Skip_Notes,'') Skip_Notes, Isnull(CE_Notes,'') CE_Notes, Isnull(CE_Budget,'') CE_Budget, Revision_No, CH.RFQ_Set, RS.PR_Number  " & vbCrLf & _
                      "From CE_Header CH Left Join RFQ_Set RS On RS.RFQ_Set = CH.RFQ_Set " & vbCrLf & _
                      "Where CE_Number = '" & pCENumber & "' And CH.Revision_No = " & pRevision & "" & vbCrLf & _
                      ""

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
	'unused in ce acceptance user detail
    Public Shared Function GetListCEApprovalData(pCENumber As String, pSupplier As String, pHeadSupplier As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "SELECT Material_No As Item_Code , Description, Specification, UOM, Qty, PriceEst, PriceEst * Qty As Amount,  Remarks, " & pHeadSupplier & " " & vbCrLf & _
                      "FROM ( " & vbCrLf & _
                      "Select	CH.CE_Number, CH.Revision_No, CH.CE_Date, CH.RFQ_Set, Skip_Budget, CE_Status, " & vbCrLf & _
                      "CD.Material_No, CD.Qty, IM.Description, IM.Specification, IM.UOM, CD.Remarks, CD.Price_Estimation As PriceEst, " & vbCrLf & _
                      "QH.Supplier_Code, Price " & vbCrLf & _
                      "From CE_Header CH " & vbCrLf & _
                      "Inner Join CE_Detail CD On CH.CE_Number = CD.CE_Number AND CH.Revision_No = CD.Revision_No  " & vbCrLf & _
                      "Left Join ( " & vbCrLf & _
                      "Select H.Quotation_No, RFQ_Set, Material_No, Qty, Price, Supplier_Code  " & vbCrLf & _
                      "From Quotation_Header H " & vbCrLf & _
                      "Inner Join Quotation_Detail D" & vbCrLf & _
                      "On H.Quotation_No = D.Quotation_No And H.Rev = D.Rev And H.RFQ_Number = D.RFQ_Number " & vbCrLf & _
                      "Where Quotation_Status = '3' " & vbCrLf & _
                      ") " & vbCrLf & _
                      "QH On QH.RFQ_Set = CH.RFQ_Set " & vbCrLf & _
                      "Left Join Mst_Item IM On IM.Material_No = CD.Material_No " & vbCrLf & _
                      "Where CH.CE_Number = '" & pCENumber & "' " & vbCrLf & _
                      ") as s  " & vbCrLf & _
                      "PIVOT " & vbCrLf & _
                      "(" & vbCrLf & _
                      "SUM(Price) " & vbCrLf & _
                      "FOR [Supplier_Code] IN (" & pSupplier & ") " & vbCrLf & _
                      ") AS pvt " & vbCrLf & _
                      ""

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetListCEData(pCENumber As String, pRev As String, pRFQSetNo As String, Optional ByRef pErr As String = "") As DataSet
        'parameter not used pSupplier As String, pHeadSupplier As String
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                ' sql = "SELECT Material_No As Item_Code , Description, Specification, UOM, Qty, PriceEst, PriceEst * Qty As Amount,  Remarks, " & pHeadSupplier & " " & vbCrLf & _
                ' "FROM ( " & vbCrLf & _
                ' "Select	CH.CE_Number, CH.Revision_No, CH.CE_Date, CH.RFQ_Set, Skip_Budget, CE_Status, " & vbCrLf & _
                ' "CD.Material_No, CD.Qty, IM.Description, IM.Specification, IM.UOM, CD.Remarks, CD.Price_Estimation As PriceEst, " & vbCrLf & _
                ' "QH.Supplier_Code, Price " & vbCrLf & _
                ' "From CE_Header CH " & vbCrLf & _
                ' "Inner Join CE_Detail CD On CH.CE_Number = CD.CE_Number AND CH.Revision_No = CD.Revision_No  " & vbCrLf & _
                ' "Left Join ( " & vbCrLf & _
                ' "Select H.Quotation_No, RFQ_Set, Material_No, Qty, Price, Supplier_Code  " & vbCrLf & _
                ' "From Quotation_Header H " & vbCrLf & _
                ' "Inner Join Quotation_Detail D" & vbCrLf & _
                ' "On H.Quotation_No = D.Quotation_No And H.Rev = D.Rev And H.RFQ_Number = D.RFQ_Number " & vbCrLf & _
                ' "Where Quotation_Status = '3' " & vbCrLf & _
                ' ") " & vbCrLf & _
                ' "QH On QH.RFQ_Set = CH.RFQ_Set " & vbCrLf & _
                ' "Left Join Mst_Item IM On IM.Material_No = CD.Material_No " & vbCrLf & _
                ' "Where CH.CE_Number = '" & pCENumber & "' " & vbCrLf & _
                ' ") as s  " & vbCrLf & _
                ' "PIVOT " & vbCrLf & _
                ' "(" & vbCrLf & _
                ' "SUM(Price) " & vbCrLf & _
                ' "FOR [Supplier_Code] IN (" & pSupplier & ") " & vbCrLf & _
                ' ") AS pvt " & vbCrLf & _
                ' ""

                '13-03-2019
                sql = "sp_CE_GetListCEData"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CENumber", pCENumber)
                cmd.Parameters.AddWithValue("Revision", pRev)
                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSetNo)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'Public Shared Function GetDataDetail(pRFQSet As String, Optional ByRef pErr As String = "") As DataSet
    '    Try
    '        Dim sql As String = ""

    '        Using con As New SqlConnection(Sconn.Stringkoneksi)
    '            con.Open()

    '            sql = "Select QH.Supplier_Code, QD.Material_No, IM.Description, IM.UOM, IM.Specification, ISNULL(CE.PriceEst,0) PriceEst, QD.Price, QD.Qty From Quotation_Header QH " & vbCrLf & _
    '                  "Inner Join Quotation_Detail QD  " & vbCrLf & _
    '                  "On QH.Quotation_No = QD.Quotation_No " & vbCrLf & _
    '                  "And QH.Revision_No = QD.Revision_No And QH.RFQ_Number = QD.RFQ_Number " & vbCrLf & _
    '                  "Left Join (Select H.CE_Number, RFQ_Set, H.RFQ_Number, Material_No, Price_Estimation As PriceEst From CE_Header H Inner Join CE_Detail D On H.CE_Number = D.CE_Number Where CE_Status <> '3') CE " & vbCrLf & _
    '                  "On CE.RFQ_Number = QH.RFQ_Number And CE.RFQ_Set = QH.RFQ_Set And CE.Material_No = QD.Material_No " & vbCrLf & _
    '                  "Left Join Mst_Item IM On IM.Material_No = QD.Material_No " & vbCrLf & _
    '                  "Where QH.RFQ_Set = '" & pRFQSet & "' And QH.Quotation_Status <> '3' "

    '            Dim cmd As New SqlCommand(sql, con)
    '            Dim da As New SqlDataAdapter(cmd)
    '            Dim ds As New DataSet
    '            da.Fill(ds)

    '            Return ds

    '        End Using
    '    Catch ex As Exception
    '        pErr = ex.Message
    '        Return Nothing
    '    End Try
    'End Function

    Public Shared Function GetComboCEApproval(pDateFrom As String, pDateTo As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select 'ALL' CE_Number UNION ALL Select CE_Number From CE_Header Where CE_Date >= '" & pDateFrom & "' And CE_Date <= '" & pDateTo & "' And CE_Status = 1"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds


        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboData(pDateFrom As String, pDateTo As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "Select 'ALL' CE_Number UNION ALL Select CE_Number From CE_Header Where CE_Date >= '" & pDateFrom & "' And CE_Date <= '" & pDateTo & "'"
                sql = "sp_CEAcceptance_GetComboData"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("DateFrom", pDateFrom)
                cmd.Parameters.AddWithValue("DateTo", pDateTo)
                cmd.Parameters.AddWithValue("UserID", pUser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboPRNo(pPRNo As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select DISTINCT PR_Number As Code " & vbCrLf & _
                      " From RFQ_Set S  " & vbCrLf & _
                      " Inner Join (Select * From RFQ_Header Where RFQ_Status = '2') H On H.RFQ_Set = S.RFQ_Set " & vbCrLf & _
                      " Inner Join (Select Quotation_No, RFQ_Set, RFQ_Number From Quotation_Header Where Quotation_Status = '3') Q " & vbCrLf & _
                      " On Q.RFQ_Set = H.RFQ_Set And Q.RFQ_Number = H.RFQ_Number " & vbCrLf & _
                      " Left Join (Select CE_Number, RFQ_Set From CE_Header Where CE_Status <> '3') C  " & vbCrLf & _
                      " On Q.RFQ_Set = C.RFQ_Set   " & vbCrLf & _
                      " Where CE_Number Is NULL "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboBudgetNo(pUserID As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_CEAcceptance_GetComboIABudget"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("UserID", pUserID)
                Dim da As New SqlDataAdapter(cmd)

                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboRFQSetNo(pPRNo As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select QH.RFQ_Set As Code From RFQ_Set RS " & vbCrLf & _
                      "Left Join Quotation_Header QH On RS.RFQ_Set = QH.RFQ_Set " & vbCrLf & _
                      "Left Join CE_Header CH On CH.RFQ_Set = QH.RFQ_Set " & vbCrLf & _
                      "Where PR_Number = '" & pPRNo & "'  AND CH.RFQ_Set IS NULL " & vbCrLf & _
                      "Group By QH.RFQ_Set " & vbCrLf & _
                      "Order By QH.RFQ_Set "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataSupplier(pRFQSetNo As String, pSupplier As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                ' sql = "Select H.Supplier_Code As Code, Supplier_Name As Description " & vbCrLf & _
                      ' "From RFQ_Header H " & vbCrLf & _
                      ' "Left Join Mst_Supplier S On H.Supplier_Code = S.Supplier_Code " & vbCrLf & _
                      ' "Where RFQ_Set = @RFQSetNo And RFQ_Status = '2'"

                'If pSupplier <> "00" Then
                '    sql = sql + " AND H.Supplier_Code = '" & pSupplier & "'"
                'End If

				sql = "sp_CE_GetDataSupplier"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSetNo)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboSupplier(pRFQSetNo As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select '00' Code, 'ALL' Description  " & vbCrLf & _
                      "Union All  " & vbCrLf & _
                      "Select H.Supplier_Code As Code, Supplier_Name As Description " & vbCrLf & _
                      "From RFQ_Header H " & vbCrLf & _
                      "Left Join Mst_Supplier S On H.Supplier_Code = S.Supplier_Code " & vbCrLf & _
                      "Where RFQ_Set = @RFQSetNo"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSetNo)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pTerm As clsCostEstimationAcceptanceUser, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "Insert Into CE_Detail (Item_Code, Qty, Remarks, Record_New) Values (@ItemCode, @Qty, @Remarks, '1')"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("ItemCode", pTerm.MaterialNo)
                cmd.Parameters.AddWithValue("Qty", pTerm.Qty)
                cmd.Parameters.AddWithValue("Remarks", pTerm.Remarks)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function
    Public Shared Function CheckIABudget(pCENumber As String, pIABudget As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "Select CE_Budget from CE_Header WHERE CE_Budget = '" & pIABudget & "' AND CE_Budget<>'' " ' AND CE_Number='" & pCENumber & "'

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetCEStatus(pCENumber As String, pRev As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "Select * from CE_Header WHERE CE_Status='2' AND ISNULL(FileName_Acceptance,'')='' AND ISNULL(CE_Budget,'')='' AND CE_Number= '" & pCENumber & "' AND Revision_No='" & pRev & "' " ' AND CE_Number='" & pCENumber & "'

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

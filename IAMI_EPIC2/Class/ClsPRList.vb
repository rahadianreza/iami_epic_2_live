﻿Public Class ClsPRList
    Public Property RequestDateFrom As String
    Public Property RequestDateTo As String
    Public Property PRType As String    
    Public Property Department As String
    Public Property Section As String

    Public Property PRNumber As String
    Public Property PRDate As String
    Public Property PRBudget As String
    Public Property MaterialNo As String
    Public Property Qty As Integer
    Public Property CostCenter As String
    Public Property Project As String
    Public Property Urgent As String
    Public Property UrgentNote As String
    Public Property ReqPOIssueDate As String
    Public Property Tender As String
    Public Property StatusCls As String
    Public Property Remarks As String
    Public Property DeptPreparedBy As String
    Public Property DeptPreparedDate As String
    Public Property DeptApprovedBy As String
    Public Property DeptApprovedDate As String
    Public Property GMApprovedBy As String
    Public Property GMApprovedDate As String
    Public Property DirApprovedBy As String
    Public Property DirApprovedDate As String
    Public Property CCApprovedBy As String
    Public Property CCApprovedDate As String
End Class

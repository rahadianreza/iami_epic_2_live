﻿Imports System.Data.SqlClient

Public Class ClsMasterPartVariantDB
    Public Shared Function getData(ByVal pObj As ClsMasterPartVariant, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As DataTable
        Dim dt As New DataTable
        Dim cmd As SqlCommand
        Dim da As SqlDataAdapter
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_PartVariant_Sel"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)
                    cmd.Parameters.AddWithValue("@PartNo", pObj.PartNo)
                    da = New SqlDataAdapter(cmd)
                    da.Fill(dt)
                    da.Dispose()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                End Using
            Else

            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterPartVariant.getDataTable: " & ex.Message
            Return Nothing
        End Try
        Return dt
    End Function

    Public Shared Function getDataDetail(Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As DataTable
        Dim dt As New DataTable
        Dim cmd As SqlCommand
        Dim da As SqlDataAdapter
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_PartVariant_SelDetail"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    da = New SqlDataAdapter(cmd)
                    da.Fill(dt)
                    da.Dispose()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                End Using
            Else

            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterPartVariant.getDataDetail: " & ex.Message
            Return Nothing
        End Try
        Return dt
    End Function

    Public Shared Function CheckData(ByVal pObj As ClsMasterPartVariant, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As DataTable
        Dim dt As New DataTable
        Dim cmd As SqlCommand
        Dim da As SqlDataAdapter
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "Select * From Mst_PartVariant Where Type=@Type and VariantCode=@VariantCode"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)
                    da = New SqlDataAdapter(cmd)
                    da.Fill(dt)
                    da.Dispose()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                End Using
            Else

            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterPartVariant.getDataTable: " & ex.Message
            Return Nothing
        End Try
        Return dt
    End Function

    Public Shared Function insert(ByVal pObj As ClsMasterPartVariant, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As Boolean
        Dim result As Boolean = False
        Dim cmd As SqlCommand
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_PartVariant_Ins"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)
                    cmd.Parameters.AddWithValue("@VariantName", pObj.VariantName)
                    cmd.Parameters.AddWithValue("@Material", pObj.MaterialFUSAP)
                    cmd.Parameters.AddWithValue("@PartNo", pObj.PartNo)
                    cmd.Parameters.AddWithValue("@Qty", CDbl(pObj.Qty))
                    cmd.Parameters.AddWithValue("@UserID", pObj.User)
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                    result = True
                End Using
            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterPartVariant.insert: " & ex.Message
            Return False
        End Try
        Return result
    End Function

    Public Shared Function Update(ByVal pObj As ClsMasterPartVariant, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As Boolean
        Dim result As Boolean = False
        Dim cmd As SqlCommand
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_PartVariant_Upd"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)
                    cmd.Parameters.AddWithValue("@VariantName", pObj.VariantName)
                    cmd.Parameters.AddWithValue("@Material", pObj.MaterialFUSAP)
                    cmd.Parameters.AddWithValue("@PartNo", pObj.PartNo)
                    cmd.Parameters.AddWithValue("@Qty", CDbl(pObj.Qty))
                    cmd.Parameters.AddWithValue("@UserID", pObj.User)
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                    result = True
                End Using
            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterPartVariant.Update: " & ex.Message
            Return False
        End Try
        Return result
    End Function

    Public Shared Function Delete(ByVal pObj As ClsMasterPartVariant, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As Boolean
        Dim result As Boolean = False
        Dim cmd As SqlCommand
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_PartVariant_Del"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)
                    cmd.Parameters.AddWithValue("@PartNo", pObj.PartNo)
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                    result = True
                End Using
            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterPartVariant.Delete: " & ex.Message
            Return False
        End Try
        Return result
    End Function
End Class

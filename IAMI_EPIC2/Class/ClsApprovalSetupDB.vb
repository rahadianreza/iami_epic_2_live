﻿Imports System.Data.SqlClient

Public Class ClsApprovalSetupDB

    Public Shared Function getlist(ByVal pGroupID As String, Optional ByRef pErr As String = "") As List(Of ClsApprovalSetup)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_ApprovalSetup_Sel"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
              
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsApprovalSetup)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New ClsApprovalSetup With {
                        .Approval_Group = Trim(dt.Rows(i).Item("Approval_Group") & ""),
                        .Approval_ID = Trim(dt.Rows(i).Item("Approval_ID") & ""),
                        .Approval_Level = Trim(dt.Rows(i).Item("Approval_Level") & ""),
                        .Approval_Name = Trim(dt.Rows(i).Item("Approval_Name") & ""),
                        .RegisterBy = Trim(dt.Rows(i).Item("RegisterBy") & ""),
                        .RegisterDate = Trim(dt.Rows(i).Item("RegisterDate") & ""),
                        .UpdateBy = Trim(dt.Rows(i).Item("UpdateBy") & ""),
                        .UpdateDate = Trim(dt.Rows(i).Item("UpdateDate") & "")
                        }
                    list.Add(ss)
                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pSes As ClsApprovalSetup, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_ApprovalSetup_Ins"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Approval_Group", pSes.Approval_Group)
                cmd.Parameters.AddWithValue("Approval_ID", pSes.Approval_ID)
                cmd.Parameters.AddWithValue("Approval_Level", pSes.Approval_Level)
                cmd.Parameters.AddWithValue("Approval_Name", pSes.Approval_Name)
                cmd.Parameters.AddWithValue("UserID", pSes.RegisterBy)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function Update(ByVal pSes As ClsApprovalSetup, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_ApprovalSetup_Upd"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Approval_Group", pSes.Approval_Group)
                cmd.Parameters.AddWithValue("Approval_ID", pSes.Approval_ID)
                cmd.Parameters.AddWithValue("Approval_Level", pSes.Approval_Level)
                cmd.Parameters.AddWithValue("Approval_Name", pSes.Approval_Name)
                cmd.Parameters.AddWithValue("UserID", pSes.RegisterBy)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function Delete(ByVal pSes As ClsApprovalSetup, ByVal pGroupID As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_ApprovalSetup_Del"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Approval_Group", pSes.Approval_Group)
                cmd.Parameters.AddWithValue("Approval_ID", pSes.Approval_ID)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function isExist(ByVal pID As String, ByVal pGroupID As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_ApprovalSetup_Exist"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Approval_Group", pGroupID)
                cmd.Parameters.AddWithValue("Approval_ID", pID)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count <> 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

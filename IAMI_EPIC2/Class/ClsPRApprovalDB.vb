﻿Imports System.Data.SqlClient

Public Class ClsPRApprovalDB

    Public Shared Function GetFilterComboSection(pDepartment As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pDepartment = "" Then
                    sql = "Select '00' As Code, 'ALL' Description UNION ALL Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Section'"
                Else
                    sql = "Select '00' As Code, 'ALL' Description UNION ALL Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Section' And Par_ParentCode = '" & pDepartment & "'"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetHeaderApproval(pDateFrom As String, pDateTo As String, pPRType As String, pDepartment As String, pSection As String, _
                                   pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "select Approval_Name from (Select * From PR_Header Where PR_Status = '1') ph " & vbCrLf & _
                      "inner join PR_Approval pa on ph.PR_Number = pa.PR_Number " & vbCrLf & _
                      "inner join Mst_ApprovalSetup ap on pa.Approval_ID = ap.Approval_ID  " & vbCrLf & _
                      "and case when ph.Urgent_Status = 'Yes' then 'PR Urgent' Else 'PR' end = ap.Approval_Group " & vbCrLf & _
                      "where PR_Date >= '" & pDateFrom & "' And PR_Date <= '" & pDateTo & "' "

                If pPRType <> "00" Then
                    sql = sql + " and prtype_code = '" & pPRType & "' "
                End If

                If pDepartment <> "00" Then
                    sql = sql + " and department_code = '" & pDepartment & "' "
                End If

                If pSection <> "00" Then
                    sql = sql + "and section_code = '" & pSection & "' "
                End If

                sql = sql + "group by Approval_Name"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

            End Using

            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetList(pDateFrom As String, pDateTo As String, pPRType As String, pDepartment As String, pSection As String, _
                                   pUser As String, pStatus As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRApproval_Sel"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRDateFrom", pDateFrom)
                cmd.Parameters.AddWithValue("PRDateTo", pDateTo)
                cmd.Parameters.AddWithValue("PRType", pPRType)
                cmd.Parameters.AddWithValue("Department", pDepartment)
                cmd.Parameters.AddWithValue("Section", pSection)
                cmd.Parameters.AddWithValue("UserID", pUser)
                cmd.Parameters.AddWithValue("Status", pStatus)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetListScript(pDateFrom As String, pDateTo As String, pPRType As String, pDepartment As String, pSection As String, _
                                   pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select MT.*, " & vbCrLf & _
                      "Isnull(AppPerson01,'') AppPerson01, REPLACE(ISNULL(CONVERT(DATE, AppDate01), ''), '1900-01-01', '')  AppDate01, " & vbCrLf & _
                      "Isnull(AppPerson02,'') AppPerson02, REPLACE(ISNULL(CONVERT(DATE, AppDate02), ''), '1900-01-01', '')  AppDate02, " & vbCrLf & _
                      "Isnull(AppPerson03,'') AppPerson03, REPLACE(ISNULL(CONVERT(DATE, AppDate03), ''), '1900-01-01', '')  AppDate03, " & vbCrLf & _
                      "Isnull(AppPerson04,'') AppPerson04, REPLACE(ISNULL(CONVERT(DATE, AppDate04), ''), '1900-01-01', '')  AppDate04, " & vbCrLf & _
                      "Isnull(AppPerson05,'') AppPerson05, REPLACE(ISNULL(CONVERT(DATE, AppDate05), ''), '1900-01-01', '')  AppDate05  " & vbCrLf & _
                      "From ( " & vbCrLf & _
                      "SELECT A.PR_Number,PR_Date,PRBudget = B.Par_Description,PRType = C.Par_Description,Department = D.Par_Description, " & vbCrLf & _
                      "Section = E.Par_Description,COALESCE(Project,'') Project,COALESCE(Urgent_Status,'') Urgent_Status " & vbCrLf & _
                      "FROM (Select * From PR_Header Where ISNULL(PR_Status,'0') = '1') A " & vbCrLf & _
                      "Left Join " & vbCrLf & _
                      "(SELECT Par_Group,Par_Code,Par_Description FROM Mst_Parameter WHERE Par_Group = 'PRBudget') B " & vbCrLf & _
                      "ON A.Budget_Code = B.Par_Code " & vbCrLf & _
                      "Left Join " & vbCrLf & _
                      "(SELECT Par_Group,Par_Code,Par_Description FROM Mst_Parameter WHERE Par_Group = 'PRType') C " & vbCrLf & _
                      "ON A.PRType_Code = C.Par_Code " & vbCrLf & _
                      "Left Join " & vbCrLf & _
                      "(SELECT Par_Group,Par_Code,Par_Description FROM Mst_Parameter WHERE Par_Group = 'Department') D " & vbCrLf & _
                      "ON A.Department_Code = D.Par_Code " & vbCrLf & _
                      "Left Join " & vbCrLf & _
                      "(SELECT Par_Group,Par_Code,Par_Description FROM Mst_Parameter WHERE Par_Group = 'Section') E " & vbCrLf & _
                      "ON A.Section_Code = E.Par_Code	     " & vbCrLf & _
                      "WHERE PR_Date >= '" & pDateFrom & "' AND PR_Date <= '" & pDateTo & "' " & vbCrLf & _
                      "And  Exists (Select PR_Number From PR_Approval Where ISNULL(Approval_Status,'0') = '0' And Approval_Person = '" & pUser & "'  AND PR_Number = A.PR_Number ) " & vbCrLf & _
                      ") MT " & vbCrLf & _
                      "Left Join " & vbCrLf & _
                      "(Select PR_Number, Approval_ID, Approval_Person As AppPerson01, Approval_Date As AppDate01 From PR_Approval Where Approval_ID = '1') AA " & vbCrLf & _
                      "On AA.PR_Number = MT.PR_Number " & vbCrLf & _
                      "Left Join " & vbCrLf & _
                      "(Select PR_Number, Approval_ID, Approval_Person As AppPerson02, Approval_Date As AppDate02 From PR_Approval Where Approval_ID = '2') AB " & vbCrLf & _
                      "On AB.PR_Number = MT.PR_Number " & vbCrLf & _
                      "Left Join " & vbCrLf & _
                      "(Select PR_Number, Approval_ID, Approval_Person As AppPerson03, Approval_Date As AppDate03 From PR_Approval Where Approval_ID = '3') AC " & vbCrLf & _
                      "On AC.PR_Number = MT.PR_Number " & vbCrLf & _
                      "Left Join " & vbCrLf & _
                      "(Select PR_Number, Approval_ID, Approval_Person As AppPerson04, Approval_Date As AppDate04 From PR_Approval Where Approval_ID = '4') AD " & vbCrLf & _
                      "On AD.PR_Number = MT.PR_Number " & vbCrLf & _
                      "Left Join " & vbCrLf & _
                      "(Select PR_Number, Approval_ID, Approval_Person As AppPerson05, Approval_Date As AppDate05 From PR_Approval Where Approval_ID = '5') AE " & vbCrLf & _
                      "On AE.PR_Number = MT.PR_Number " & vbCrLf & _
                      " "

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text


                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataPR(pObj As IAMI_EPIC2.ClsPRApproval, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRApprovalDetail_Sel"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pObj.PRNumber)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function ApprovePRFull(ByVal pPRApproval As ClsPRApproval, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_PRApproval_Approve]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pPRApproval.PRNumber)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("ApprovalNotes", pPRApproval.ApprovalNote)
                cmd.Parameters.AddWithValue("Rev", pPRApproval.Revision)
                cmd.Parameters.AddWithValue("UrgentStatus", pPRApproval.UrgentCls)
                cmd.Parameters.AddWithValue("ReqPOIssueDate", pPRApproval.ReqPOIssueDate)
                cmd.Parameters.AddWithValue("UrgentNote", pPRApproval.UrgentNote)


                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function Reject(ByVal PRApprove As ClsPRApproval, pUser As String, Optional ByRef pErr As String = "") As Integer
        Try
            Dim sql As String
            Dim i As Integer

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_Reject_PR"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", PRApprove.PRNumber)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("ApprovalNotes", PRApprove.ApprovalNote)

                i = cmd.ExecuteNonQuery

            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

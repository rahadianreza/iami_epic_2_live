﻿Imports System.Data.SqlClient

Public Class clsQuotationSupplierDB
    'Header
    Public Shared Function getHeaderGrid(_prjid As String, _grpid As String, _comm As String, _pic As String, _groupcommodity As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Quotation_Supplier_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", _prjid)
                cmd.Parameters.AddWithValue("@GroupID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                cmd.Parameters.AddWithValue("@PIC", _pic)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupcommodity)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getHeaderAcceptanceGrid(_prjid As String, _grpid As String, _comm As String, _pic As String, _groupcommodity As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Quotation_Supplier_Acceptance_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", _prjid)
                cmd.Parameters.AddWithValue("@GroupID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                cmd.Parameters.AddWithValue("@PIC", _pic)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupcommodity)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function checkingApproveGate3(_prjid As String, _grpid As String, _comm As String, _groupcommodity As String, _partNo As String, pUser As String, Optional ByRef pErr As String = "") As String

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT COUNT(*) FROM Gate_III_CS_Approval WHERE Project_ID = '" + _prjid + "' AND Group_ID = '" + _grpid + "' AND Commodity = '" + _comm + "' AND  Group_Comodity = '" + _groupcommodity + "' AND Part_No = '" + _partNo + "' AND Approval_Status = '1'", con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds.Tables(0).Rows(0)(0).ToString()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertApproveGate3(prjid As String, grpid As String, comm As String, groupComodity As String, partno As String, approvalid As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_III_Approve_Insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", groupComodity)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Approval_ID", approvalid)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertQuotationSupplier(prjid As String, grpid As String, comm As String, _
                                            partno As String, overhead As String, profit As String, _
                                            transportcharge As Decimal, sellingprice As Decimal, currency As String, addCost As Decimal, _
                                            mterialcheck As String, purchasedcheck As String, manufacturingcheck As String, pUser As String, groupComodity As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Quotation_Supplier_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", prjid)
                cmd.Parameters.AddWithValue("@GroupID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@OverHead", overhead)
                cmd.Parameters.AddWithValue("@Profit", profit)
                cmd.Parameters.AddWithValue("@Transportation_Charge", transportcharge)
                cmd.Parameters.AddWithValue("@Selling_Price", sellingprice)
                cmd.Parameters.AddWithValue("@Additional_Cost", addCost)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@Material_Check", IIf(String.IsNullOrEmpty(mterialcheck), "0", mterialcheck))
                cmd.Parameters.AddWithValue("@Purchased_Check", IIf(String.IsNullOrEmpty(purchasedcheck), "0", purchasedcheck))
                cmd.Parameters.AddWithValue("@Manufacturing_Check", IIf(String.IsNullOrEmpty(manufacturingcheck), "0", manufacturingcheck))
                cmd.Parameters.AddWithValue("@Group_Comodity", groupComodity)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertSelectedSupplier(prjid As String, grpid As String, comm As String, groupComodity As String, _
                                                  recYN As String, vendor_code As String, partno As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_III_Update", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", groupComodity)
                cmd.Parameters.AddWithValue("@RecYN", recYN)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendor_code)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'MaterialCost
    Public Shared Function getHeaderMaterialCost(_prjid As String, _grpid As String, _comm As String, _partno As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_MaterialCost_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", _prjid)
                cmd.Parameters.AddWithValue("@GroupID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertQuotationSupplierMaterialCost(prjid As String, grpid As String, comm As String, _
                                            partno As String, vendorcd As String, ctgrymaterial As String, localimport As String, country As String, mtrlcode As String, _
                                            d_tck As Decimal, d_len As Decimal, d_width As Decimal, d_diam As Decimal, d_density As Decimal, d_funit As Decimal, d_weight As Decimal, qtyusg As Decimal, currency As String, rate As Decimal, _
                                            unit As Decimal, unit_cst As Decimal, amount As Decimal, scWeight As Decimal, scUnPrice As String, grpcomm As String, adjsmt As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_MaterialCost_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Category_Material", ctgrymaterial)
                cmd.Parameters.AddWithValue("@Local_Import", localimport)
                cmd.Parameters.AddWithValue("@Country", country)
                cmd.Parameters.AddWithValue("@Material_Code", mtrlcode)
                cmd.Parameters.AddWithValue("@Dimension_Thickness", d_tck)
                cmd.Parameters.AddWithValue("@Dimension_Length", d_len)
                cmd.Parameters.AddWithValue("@Dimension_Width", d_width)
                cmd.Parameters.AddWithValue("@Dimension_Diameter", d_diam)
                cmd.Parameters.AddWithValue("@Dimension_Density", d_density)
                cmd.Parameters.AddWithValue("@Dimension_FactorUnit", d_funit)
                cmd.Parameters.AddWithValue("@Dimension_Weight", d_weight)
                cmd.Parameters.AddWithValue("@Qty_Usage", qtyusg)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@Rate", IIf(String.IsNullOrEmpty(rate), DBNull.Value, rate))
                cmd.Parameters.AddWithValue("@Unit", IIf(String.IsNullOrEmpty(unit), DBNull.Value, unit))
                cmd.Parameters.AddWithValue("@Unit_Cost", unit_cst)
                cmd.Parameters.AddWithValue("@Amount", amount)
                cmd.Parameters.AddWithValue("@Scrap_Weight", scWeight)
                cmd.Parameters.AddWithValue("@Scrap_Unit_Price", scUnPrice)
                cmd.Parameters.AddWithValue("@Register_By", pUser)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpcomm)
                cmd.Parameters.AddWithValue("@AdjustmentType", adjsmt)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateQuotationSupplierMaterialCost(prjid As String, grpid As String, comm As String, _
                                           partno As String, vendorcd As String, ctgrymaterial As String, localimport As String, country As String, mtrlcode As String, _
                                           d_tck As Decimal, d_len As Decimal, d_width As Decimal, d_diam As Decimal, d_density As Decimal, d_funit As Decimal, d_weight As Decimal, qtyusg As Decimal, currency As String, rate As Decimal, _
                                           unit As Decimal, unit_cst As Decimal, amount As Decimal, scWeight As Decimal, scUnPrice As String, grpcomm As String, adjsmt As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_MaterialCost_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Category_Material", ctgrymaterial)
                cmd.Parameters.AddWithValue("@Local_Import", localimport)
                cmd.Parameters.AddWithValue("@Country", country)
                cmd.Parameters.AddWithValue("@Material_Code", mtrlcode)
                cmd.Parameters.AddWithValue("@Dimension_Thickness", d_tck)
                cmd.Parameters.AddWithValue("@Dimension_Length", d_len)
                cmd.Parameters.AddWithValue("@Dimension_Width", d_width)
                cmd.Parameters.AddWithValue("@Dimension_Diameter", d_diam)
                cmd.Parameters.AddWithValue("@Dimension_Density", d_density)
                cmd.Parameters.AddWithValue("@Dimension_FactorUnit", d_funit)
                cmd.Parameters.AddWithValue("@Dimension_Weight", d_weight)
                cmd.Parameters.AddWithValue("@Qty_Usage", qtyusg)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@Rate", rate)
                cmd.Parameters.AddWithValue("@Unit", unit)
                cmd.Parameters.AddWithValue("@Unit_Cost", unit_cst)
                cmd.Parameters.AddWithValue("@Amount", amount)
                cmd.Parameters.AddWithValue("@Scrap_Weight", scWeight)
                cmd.Parameters.AddWithValue("@Scrap_Unit_Price", IIf(String.IsNullOrEmpty(scUnPrice), 0, scUnPrice))
                cmd.Parameters.AddWithValue("@Update_By", pUser)
                cmd.Parameters.AddWithValue("@Group_Commodity", grpcomm)
                cmd.Parameters.AddWithValue("@AdjustmentType", adjsmt)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteQuotationSupplierMaterialCost(_prjid As String, _grpid As String, _comm As String, _partno As String, _
                                                               ctgrymaterial As String, materialcode As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_MaterialCost_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", _prjid)
                cmd.Parameters.AddWithValue("@GroupID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                cmd.Parameters.AddWithValue("@Category_Material", ctgrymaterial)
                cmd.Parameters.AddWithValue("@Material_Code", materialcode)
                con.Open()
                cmd.ExecuteNonQuery()

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function fillComboPartNo(prjid As String, grpid As String, comm As String, grpcomm As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT Part_No, Part_Name FROM Item_List_Sourcing WHERE Project_ID = '" + prjid + "' AND Group_ID = '" + grpid + "' AND Commodity = '" + comm + "' AND Group_Comodity = '" + grpcomm + "'", con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getProjectType(projectid As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT Project_Type FROM Proj_Header WHERE Project_ID = '" + projectid + "'", con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds.Tables(0).Rows(0)(0).ToString()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getMaterialCodeCombo(category As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim sql As String = "sp_QuotationSupplier_MaterialCost_GetMaterialCodeCombo"

                'Dim cmd As New SqlCommand("SELECT Material_Code, Material_Name FROM (SELECT DISTINCT Category_Material, Material_Code, Material_Name " & _
                '"FROM Mst_Material_Part  " & _
                '"UNION ALL " & _
                '       "SELECT 'PT01', A.Part_No, A.Part_Name  " & _
                '       "FROM Quotation_Supplier_Project A " & _
                '       "INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID " & _
                '       "WHERE B.Project_Type = 'PT01' AND A.Accept_Reject = '1' AND A.Vendor_Code = '" + pUser + "' " & _
                '       ") A WHERE A.Category_Material = '" + category + "'", con)
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@CategoryMaterial", category)
                cmd.Parameters.AddWithValue("@VendorCode", pUser)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function checkingStatus(projectid As String, grpid As String, comm As String, partno As String, vendorcd As String, grpComodity As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT ISNULL(Accept_Reject,1) FROM Quotation_Supplier_Project WHERE Project_ID = '" + projectid + "' " & _
                                          "AND Group_ID = '" + grpid + "' AND Commodity='" + comm + "' AND Group_Comodity = '" + grpComodity + "' AND Part_No = '" + partno + "' AND Vendor_Code = '" + vendorcd + "'", con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds.Tables(0).Rows(0)(0).ToString()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'Purchased Part Cost
    Public Shared Function getHeaderPurchasedCost(_prjid As String, _grpid As String, _comm As String, _partno As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_PurchasePartCost_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertQuotationSupplierPurchasedCost(prjid As String, grpid As String, comm As String, _
                                           partno As String, vendorcd As String, ppartcode As String, qtyusg As Integer, UoM As String, _
                                           currency As String, localimport As String, country As String, unit_cst As Decimal, amount As Decimal, grpcomm As String, adjstment As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_PurchasePartCost_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@PartNo", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Purchased_Part_Code", ppartcode)
                cmd.Parameters.AddWithValue("@Qty_Usage", qtyusg)
                cmd.Parameters.AddWithValue("@UoM", UoM)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@Local_Import", localimport)
                cmd.Parameters.AddWithValue("@Country", country)
                cmd.Parameters.AddWithValue("@Unit_Cost", unit_cst)
                cmd.Parameters.AddWithValue("@Amount", amount)
                cmd.Parameters.AddWithValue("@Register_By", pUser)
                cmd.Parameters.AddWithValue("@Group_Commodity", grpcomm)
                cmd.Parameters.AddWithValue("@AdjustmentType", adjstment)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateQuotationSupplierPurchasedCost(prjid As String, grpid As String, comm As String, _
                                           partno As String, vendorcd As String, ppartcode As String, qtyusg As Integer, UoM As String, _
                                           currency As String, localimport As String, country As String, unit_cst As Decimal, amount As Decimal, grpcomm As String, adjstment As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_PurchasePartCost_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@PartNo", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Purchased_Part_Code", ppartcode)
                cmd.Parameters.AddWithValue("@Qty_Usage", qtyusg)
                cmd.Parameters.AddWithValue("@UoM", UoM)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@Local_Import", localimport)
                cmd.Parameters.AddWithValue("@Country", country)
                cmd.Parameters.AddWithValue("@Unit_Cost", unit_cst)
                cmd.Parameters.AddWithValue("@Amount", amount)
                cmd.Parameters.AddWithValue("@Update_By", pUser)
                cmd.Parameters.AddWithValue("@Group_Commodity", grpcomm)
                cmd.Parameters.AddWithValue("@AdjustmentType", adjstment)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteQuotationSupplierPurchasedCost(_prjid As String, _grpid As String, _comm As String, _partno As String, ppartcode As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_PurchasePartCost_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                cmd.Parameters.AddWithValue("@Purchased_Part_Code", ppartcode)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'Manufacturing Cost
    Public Shared Function getHeaderManufacturingCost(_prjid As String, _grpid As String, _comm As String, _partno As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_ManufacturingCost_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertQuotationSupplierManufacturingCost(prjid As String, grpid As String, comm As String, _
                                           partno As String, vendorcd As String, processing As String, mused As String, personnel As String, ECostrate As Integer, ECostMinute As Integer, _
                                           ECostChargeHour As Decimal, MCostRate As Integer, MCostMinutes As Integer, MCostChargeHour As Decimal, LCostRate As Integer, _
                                           LCostMinute As Integer, LCostChargeHour As Decimal, AmountMechineLabor As Decimal, currency As String, groupcommodity As String, adjustmentType As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_ManufacturingCost_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Processing", processing)
                cmd.Parameters.AddWithValue("@Machinery_Used", mused)
                cmd.Parameters.AddWithValue("@Personnel", personnel)
                cmd.Parameters.AddWithValue("@Equipment_Cost_Rate", ECostrate)
                cmd.Parameters.AddWithValue("@Equipment_Cost_Minute", ECostMinute)
                cmd.Parameters.AddWithValue("@Equipment_Cost_Charge_hour", ECostChargeHour)
                cmd.Parameters.AddWithValue("@Machine_Cost_Rate", MCostRate)
                cmd.Parameters.AddWithValue("@Machine_Cost_Minute", MCostMinutes)
                cmd.Parameters.AddWithValue("@Machine_Cost_Charge_hour", MCostChargeHour)
                cmd.Parameters.AddWithValue("@Labour_Cost_Rate", LCostRate)
                cmd.Parameters.AddWithValue("@Labour_Cost_Minute", LCostMinute)
                cmd.Parameters.AddWithValue("@Labour_Cost_Charge_Hour", LCostChargeHour)
                cmd.Parameters.AddWithValue("@Amount_Machine_Labour", AmountMechineLabor)
                cmd.Parameters.AddWithValue("@Register_By", pUser)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@Group_Commodity", groupcommodity)
                cmd.Parameters.AddWithValue("@AdjustmentType", adjustmentType)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateQuotationSupplierManufacturingCost(prjid As String, grpid As String, comm As String, _
                                           partno As String, vendorcd As String, processing As String, mused As String, personnel As String, ECostrate As Integer, ECostMinute As Integer, _
                                           ECostChargeHour As Decimal, MCostRate As Integer, MCostMinutes As Integer, MCostChargeHour As Decimal, LCostRate As Integer, _
                                           LCostMinute As Integer, LCostChargeHour As Decimal, AmountMechineLabor As Decimal, currency As String, groupcommodity As String, adjustmentType As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_ManufacturingCost_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Processing", processing)
                cmd.Parameters.AddWithValue("@Machinery_Used", mused)
                cmd.Parameters.AddWithValue("@Personnel", personnel)
                cmd.Parameters.AddWithValue("@Equipment_Cost_Rate", ECostrate)
                cmd.Parameters.AddWithValue("@Equipment_Cost_Minute", ECostMinute)
                cmd.Parameters.AddWithValue("@Equipment_Cost_Charge_hour", ECostChargeHour)
                cmd.Parameters.AddWithValue("@Machine_Cost_Rate", MCostRate)
                cmd.Parameters.AddWithValue("@Machine_Cost_Minute", MCostMinutes)
                cmd.Parameters.AddWithValue("@Machine_Cost_Charge_hour", MCostChargeHour)
                cmd.Parameters.AddWithValue("@Labour_Cost_Rate", LCostRate)
                cmd.Parameters.AddWithValue("@Labour_Cost_Minute", LCostMinute)
                cmd.Parameters.AddWithValue("@Labour_Cost_Charge_Hour", LCostChargeHour)
                cmd.Parameters.AddWithValue("@Amount_Machine_Labour", AmountMechineLabor)
                cmd.Parameters.AddWithValue("@Update_By", pUser)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@Group_Commodity", groupcommodity)
                cmd.Parameters.AddWithValue("@AdjustmentType", adjustmentType)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteQuotationSupplierManufacturingCost(_prjid As String, _grpid As String, _comm As String, _partno As String, processing As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_ManufacturingCost_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                cmd.Parameters.AddWithValue("@Processing", processing)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'Packing Charge
    Public Shared Function getHeaderPackingChargeCost(_prjid As String, _grpid As String, _comm As String, _partno As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_PackingCharge_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertQuotationSupplierPackingChargeCost(prjid As String, grpid As String, comm As String, _
                                           partno As String, vendorcd As String, palletspec As String, qtypallet As Integer, qtyPartPallet As Integer, _
                                           pCharge As Integer, currency As String, adjustmenttype As String, groupcommodity As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_PackingCharge_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Pallet_Specification", palletspec)
                cmd.Parameters.AddWithValue("@Qty_Pallet", qtypallet)
                cmd.Parameters.AddWithValue("@Qty_Part_Pallet", qtyPartPallet)
                cmd.Parameters.AddWithValue("@Packing_Charge", pCharge)
                cmd.Parameters.AddWithValue("@Register_By", pUser)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@AdjustmentType", adjustmenttype)
                cmd.Parameters.AddWithValue("@Group_Commodity", groupcommodity)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateQuotationSupplierPackingChargeCost(prjid As String, grpid As String, comm As String, _
                                           partno As String, vendorcd As String, palletspec As String, qtypallet As Integer, qtyPartPallet As Integer, _
                                           pCharge As Integer, currency As String, adjustmenttype As String, groupcommodity As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_PackingCharge_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Pallet_Specification", palletspec)
                cmd.Parameters.AddWithValue("@Qty_Pallet", qtypallet)
                cmd.Parameters.AddWithValue("@Qty_Part_Pallet", qtyPartPallet)
                cmd.Parameters.AddWithValue("@Packing_Charge", pCharge)
                cmd.Parameters.AddWithValue("@Update_By", pUser)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@AdjustmentType", adjustmenttype)
                cmd.Parameters.AddWithValue("@Group_Commodity", groupcommodity)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteQuotationSupplierPackingChargeCost(_prjid As String, _grpid As String, _comm As String, _partno As String, palletspec As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_PackingCharge_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                cmd.Parameters.AddWithValue("@Pallet_Specification", palletspec)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'Tooling Cost
    Public Shared Function getHeaderToolingCost(_prjid As String, _grpid As String, _comm As String, _partno As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_TollingCost_Detail_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertQuotationSupplierToolingCost(prjid As String, grpid As String, comm As String, _
                                           partno As String, vendorcd As String, tname As String, mfgcost As Integer, aexpanse As Integer, _
                                           remarks As String, adjustmentType As String, Currency As String, grpcomm As String, amount As Decimal, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_TollingCost_Detail_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Tooling_Name", tname)
                cmd.Parameters.AddWithValue("@Mfg_Cost", mfgcost)
                cmd.Parameters.AddWithValue("@Administrative_Expense", aexpanse)
                cmd.Parameters.AddWithValue("@Remarks", remarks)
                cmd.Parameters.AddWithValue("@Register_By", pUser)
                cmd.Parameters.AddWithValue("@AdjustmentType", adjustmentType)
                cmd.Parameters.AddWithValue("@Currency", Currency)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpcomm)
                cmd.Parameters.AddWithValue("@Amount", IIf(String.IsNullOrEmpty(amount), 0, amount))
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateQuotationSupplierToolingCost(prjid As String, grpid As String, comm As String, _
                                           partno As String, vendorcd As String, tname As String, mfgcost As Integer, aexpanse As Integer, _
                                           remarks As String, adjustmentType As String, Currency As String, grpcomm As String, amount As Decimal, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_TollingCost_Detail_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Tooling_Name", tname)
                cmd.Parameters.AddWithValue("@Mfg_Cost", mfgcost)
                cmd.Parameters.AddWithValue("@Administrative_Expense", aexpanse)
                cmd.Parameters.AddWithValue("@Remarks", remarks)
                cmd.Parameters.AddWithValue("@Update_By", pUser)
                cmd.Parameters.AddWithValue("@AdjustmentType", adjustmentType)
                cmd.Parameters.AddWithValue("@Currency", Currency)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpcomm)
                cmd.Parameters.AddWithValue("@Amount", IIf(String.IsNullOrEmpty(amount), 0, amount))
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteQuotationSupplierToolingCost(_prjid As String, _grpid As String, _comm As String, _partno As String, tname As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_TollingCost_Detail_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                cmd.Parameters.AddWithValue("@Tooling_Name", tname)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertQuotationSupplierToolingCost_Header(prjid As String, grpid As String, comm As String, _
                                          partno As String, vendorcd As String, interestyear As Integer, dperiod As Integer, totalinterest As Integer, _
                                          qtyperunit As Integer, ppunitmonth As Integer, totaldepreciationperiod As Decimal, totalamountinterest As Decimal, _
                                          totaltoolingcost As Decimal, dCost As Decimal, grpcommodity As String, type As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_TollingCost_Header_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Interest_Year", interestyear)
                cmd.Parameters.AddWithValue("@Depreciation_Period", dperiod)
                cmd.Parameters.AddWithValue("@Total_Interest", totalinterest)
                cmd.Parameters.AddWithValue("@Qty_PerUnit", qtyperunit)
                cmd.Parameters.AddWithValue("@Project_Prod_unit_Month", ppunitmonth)
                cmd.Parameters.AddWithValue("@Total_Depreciation_Period", totaldepreciationperiod)
                cmd.Parameters.AddWithValue("@Total_Amount_Interest", totalamountinterest)
                cmd.Parameters.AddWithValue("@Total_Tooling_Cost", totaltoolingcost)
                cmd.Parameters.AddWithValue("@Decpreciation_Cost", dCost)
                cmd.Parameters.AddWithValue("@Register_By", pUser)
                cmd.Parameters.AddWithValue("@Group_Commodity", grpcommodity)
                cmd.Parameters.AddWithValue("@Type", type)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'get summart totalcost
    Public Shared Function getHeaderToolingCost_amount(_prjid As String, _grpid As String, _comm As String, _partno As String, pUser As String, Optional ByRef pErr As String = "") As Decimal

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT ISNULL(SUM(Amount),0) as Total " & _
                                          "FROM Quotation_Tooling_Cost_Detail  " & _
                                          "WHERE Project_ID = '" + _prjid + "' AND Group_ID = '" + _grpid + "' AND Commodity = '" + _comm + "' AND Part_No = '" + _partno + "' AND Vendor_Code = '" + pUser + "'", con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds.Tables(0).Rows(0)(0).ToString()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'get header

    Public Shared Function getHeaderToolingCost_Data(_prjid As String, _grpid As String, _comm As String, _partno As String, _type As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_TollingCost_Header_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                cmd.Parameters.AddWithValue("@Type", _type)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'update complete
    Public Shared Function Completed(prjid As String, grpid As String, comm As String, grpCommodity As String, _
                                     pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_Complete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpCommodity)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'check complete
    Public Shared Function checkingStatusComplete(projectid As String, grpid As String, comm As String, partno As String, vendorcd As String, grpComodity As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT ISNULL(Complete,0) FROM Quotation_Supplier_Project WHERE Project_ID = '" + projectid + "' " & _
                                          "AND Group_ID = '" + grpid + "' AND Commodity='" + comm + "' AND Group_Comodity = '" + grpComodity + "' AND Part_No = '" + partno + "' AND Vendor_Code = '" + vendorcd + "'", con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds.Tables(0).Rows(0)(0).ToString()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function checkingStatus_Header(projectid As String, grpid As String, comm As String, vendorcd As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT ISNULL(Accept_Reject,0) FROM Quotation_Supplier_Project WHERE Project_ID = '" + projectid + "' " & _
                                          "AND Group_ID = '" + grpid + "' AND Commodity='" + comm + "' AND Vendor_Code = '" + vendorcd + "'", con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds.Tables(0).Rows(0)(0).ToString()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function checkingStatusComplete_Header(projectid As String, grpid As String, comm As String, vendorcd As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT ISNULL(Complete,0) FROM Quotation_Supplier_Project WHERE Project_ID = '" + projectid + "' " & _
                                          "AND Group_ID = '" + grpid + "' AND Commodity='" + comm + "' AND Vendor_Code = '" + vendorcd + "'", con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds.Tables(0).Rows(0)(0).ToString()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'get status
    Public Shared Function getStatus_Header(projectid As String, grpid As String, comm As String, vendorcd As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT ISNULL(Status_QuotationSupplier,'') FROM vw_QuotationSupplier_Status WHERE Project_ID = '" + projectid + "' " & _
                                          "AND Group_ID = '" + grpid + "' AND Commodity='" + comm + "' AND Vendor_Code = '" + vendorcd + "'", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getRemarks_Header(projectid As String, grpid As String, comm As String, vendorcd As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT TOP 1 Remarks FROM Quotation_Supplier_Project WHERE Project_ID = '" + projectid + "' " & _
                                          "AND Group_ID = '" + grpid + "' AND Commodity='" + comm + "' AND Vendor_Code = '" + vendorcd + "'", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'update accept and reject
    Public Shared Function AcceptAndReject(type As String, prjid As String, grpid As String, comm As String, remarks As String, _
                                    pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_Accept_Reject", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@type", type)
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                cmd.Parameters.AddWithValue("@Remarks", remarks)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function RejectOnRow(prjid As String, grpid As String, comm As String, remarks As String, vendorcd As String, _
                                  pUser As String, partno As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_RejectOnRow", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Remarks", remarks)
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'cost target submit
    Public Shared Function UpdateQuotationSupplierCostTarget(prjid As String, grpid As String, comm As String, _
                                           partno As String, groupComodity As String, targetpartcost As Integer, targetcoolingcost As Integer, _
                                           pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SupplierQuotation_CostTargetSubmit_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Group_Comodity", groupComodity)
                cmd.Parameters.AddWithValue("@TargetPartCost", targetpartcost)
                cmd.Parameters.AddWithValue("@TargetCollingCost", targetcoolingcost)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateQuotationSupplierCostFinal(prjid As String, grpid As String, comm As String, _
                                          partno As String, vendorcd As String, finalpricepart As Integer, finalpricetooling As Integer, _
                                          pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SupplierQuotation_CostFinalSubmit_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcd)
                cmd.Parameters.AddWithValue("@Cost_Final_Price_Part", finalpricepart)
                cmd.Parameters.AddWithValue("@Cost_Final_Price_Tooling", finalpricetooling)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getHeaderCostTarget(_prjid As String, _grpid As String, _comm As String, _grpComm As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SupplierQuotation_CostTargetSubmit_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", _grpComm)
                'cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getHeaderCostFinal(_prjid As String, _grpid As String, _comm As String, _grpComm As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SupplierQuotation_CostFinalSubmit_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", _grpComm)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getHeaderCostConfirm(_prjid As String, _grpid As String, _comm As String, _grpComm As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SupplierQuotation_CostConfirm_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", _grpComm)
                'cmd.Parameters.AddWithValue("@Vendor_Code", pUser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'complete user
    Public Shared Function CompleteConfirmSubmitUser(type As String, prjid As String, grpid As String, comm As String, groupcomodity As String, _
                                          pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SupplierQuotation_CostTargetSubmit_Complete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@type", type)
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", groupcomodity)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function CompleteStatus(prjid As String, grpid As String, comm As String, groupcommodity As String, _
                                         pUser As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_CostTargetSubmit_Status", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", groupcommodity)
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function CompleteStatusFinalSubmit(prjid As String, grpid As String, comm As String, groupcommodity As String, _
                                         pUser As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_CostFinalSubmit_Status", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", groupcommodity)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetTransNo(prjid As String, grpid As String, comm As String, groupcomodity As String, _
                                            pUser As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT Cost_Target_No_Trans FROM Quotation_Supplier_Project WHERE Project_ID = '" + prjid + "' AND Group_ID = '" + grpid + "' AND Commodity = '" + comm + "' AND Group_Comodity = '" + groupcomodity + "'", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'gate III suplier selection
    Public Shared Function getSupplierSelectionGrid(_prjid As String, _grpid As String, _comm As String, _partno As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_III_SuplierSelection", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function QCDMR(_prjid As String, _grpid As String, _comm As String, _partno As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_III_SuplierSelection_QCDMR", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Detail(_prjid As String, _grpid As String, _comm As String, _partno As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_III_SuplierSelection_Detail", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertSupplierSelection(prjid As String, grpid As String, comm As String, _
                                           partno As String, vendorcode As String, header As String, value As String, _
                                           pUser As String, grpComodity As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_SupplierSelection_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                cmd.Parameters.AddWithValue("@Part_No", partno)
                cmd.Parameters.AddWithValue("@Vendor_Code", vendorcode)
                cmd.Parameters.AddWithValue("@header", header)
                cmd.Parameters.AddWithValue("@value", value)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpComodity)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GateIIISupplierStatusApproval(prjid As String, grpid As String, comm As String, _
                                           grpComodity As String, partno As String, Optional ByRef pErr As String = "") As Integer
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim squery As String = "SELECT COUNT(*) FROM Gate_III_CS_Approval WHERE Project_ID = '" + prjid + "' AND Group_ID = '" + grpid + "' AND Commodity = '" + comm + "' AND Group_Comodity = '" + grpComodity + "' AND Part_No = '" + partno + "' AND Approval_Status = 1"
                Dim cmd As New SqlCommand(squery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function QuotationSupplierList(_prjid As String, _grpid As String, _comm As String, _grpCommodity As String, _buyer As String, _status As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", _grpCommodity)
                cmd.Parameters.AddWithValue("@Buyer", _buyer)
                cmd.Parameters.AddWithValue("@Status", _status)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function QuotationSupplierAcceptanceList(_prjid As String, _grpid As String, _comm As String, _grpCommodity As String, _buyer As String, _status As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_QuotationSupplier_Acceptance_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", _grpCommodity)
                cmd.Parameters.AddWithValue("@Buyer", _buyer)
                cmd.Parameters.AddWithValue("@Status", _status)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function QuotationSupplierGate3List(_prjid As String, _grpid As String, _comm As String, _grpCommodity As String, _partno As String, _status As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_III_SUpplierSelection_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", _grpCommodity)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                cmd.Parameters.AddWithValue("@Status", _status)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function QuotationSupplierTargetSubmitList(_prjid As String, _grpid As String, _comm As String, _grpCommodity As String, _status As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SupplierQuotation_CostTargetSubmit_List_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", _grpCommodity)
                cmd.Parameters.AddWithValue("@Status", _status)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function QuotationSupplierFinalSubmitList(_prjid As String, _grpid As String, _comm As String, _grpCommodity As String, _status As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SupplierQuotation_CostFinalSubmit_List_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", _grpCommodity)
                cmd.Parameters.AddWithValue("@Status", _status)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function QuotationSupplierCostConfirmList(_prjid As String, _grpid As String, _comm As String, _grpCommodity As String, _status As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SupplierQuotation_CostConfirm_List_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Group_Comodity", _grpCommodity)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                cmd.Parameters.AddWithValue("@Status", _status)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

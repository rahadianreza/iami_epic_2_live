﻿Imports System.Data.SqlClient

Public Class ClsIADGSparePartDB
    Public Shared Function getListIADDGSparePart(Project As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_IADG_SparePart_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project", Project)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function insertData(_clsIADGSparePart As ClsIADGSparePart, pUser As String, Optional ByRef pErr As String = "") As Integer

        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_IADG_SparePart_Insert"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", _clsIADGSparePart.ProjectID)
                cmd.Parameters.AddWithValue("@ProjectYear", _clsIADGSparePart.ProjectYear)
                cmd.Parameters.AddWithValue("@PartNo", _clsIADGSparePart.PartNo)
                cmd.Parameters.AddWithValue("@PartName", _clsIADGSparePart.PartName)
                cmd.Parameters.AddWithValue("@Model", _clsIADGSparePart.Model)
                cmd.Parameters.AddWithValue("@NewMulti", _clsIADGSparePart.NewMulti)
                cmd.Parameters.AddWithValue("@SampleIADGCompare", _clsIADGSparePart.SampleIADGCompare)
                cmd.Parameters.AddWithValue("@SampleUOM", _clsIADGSparePart.SampleUOM)
                cmd.Parameters.AddWithValue("@VolumePerMonth", _clsIADGSparePart.VolumePerMonth)
                cmd.Parameters.AddWithValue("@CostTarget", _clsIADGSparePart.CostTarget)
                cmd.Parameters.AddWithValue("@Priority", _clsIADGSparePart.Priority)
                cmd.Parameters.AddWithValue("@UserID", pUser)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn

            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function


    Public Shared Function checkModelProject(Model As String, Optional ByRef pErr As String = "") As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT COUNT(*) FROM dbo.Proj_Header WHERE Project_Type IN ('PT01','PT02') AND Project_Name='" & Model & "'", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                If rtn = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getExistsProjectID(pProjectID As String, pProjectYear As String, pPartNo As String, pModel As String, pActionType As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_IADG_SparePart_TempUpload", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", IIf(String.IsNullOrEmpty(pProjectID), "", pProjectID))
                cmd.Parameters.AddWithValue("@Project_Year", IIf(String.IsNullOrEmpty(pProjectYear), "", pProjectYear))
                cmd.Parameters.AddWithValue("@Part_No", IIf(String.IsNullOrEmpty(pPartNo), "", pPartNo))
                cmd.Parameters.AddWithValue("@Model", IIf(String.IsNullOrEmpty(pModel), "", pModel))

                cmd.Parameters.AddWithValue("@Register_By", pUser)
                cmd.Parameters.AddWithValue("@actionType", pActionType)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'DEVELOPMENT PART APPROVAL
#Region "Development part"
    Public Shared Function getListDevelopmentPart(Project As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_IADG_DevelopmentPart_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project", Project)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getListDevelopmentPartDetail(Project As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_IADG_DevelopmentPartDetail_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project", Project)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function developmentPartDetail_CompleteStatus(Project As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_IADG_DevelopmentPart_Complete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", Project)
                cmd.Parameters.AddWithValue("@UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Public Shared Function developmentPartDetail_bindForm(ProjectYear As String, ProjectID As String, PartNo As String, Model As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_IADG_DevelopmentPartDetail_BindForm", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectYear", ProjectYear)
                cmd.Parameters.AddWithValue("@ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("@PartNo", PartNo)
                cmd.Parameters.AddWithValue("@Model", Model)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function developmentPartDetail_Update(ProjectYear As String, ProjectID As String, PartNo As String, Model As String, FIGNo As String, _
                                                        KeyNo As Integer, TargetPrice As Decimal, TargetConsumption As Decimal, _
                                                        pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_IADG_DevelopmentPartDetail_Update", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectYear", ProjectYear)
                cmd.Parameters.AddWithValue("@ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("@PartNo", PartNo)
                cmd.Parameters.AddWithValue("@Model", Model)
                cmd.Parameters.AddWithValue("@FigNo", FIGNo)
                cmd.Parameters.AddWithValue("@KeyNo", KeyNo)
                cmd.Parameters.AddWithValue("@TargetPrice", TargetPrice)
                cmd.Parameters.AddWithValue("@TargetConsumption", TargetConsumption)
                cmd.Parameters.AddWithValue("@UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function checkStatusComplete(Project As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_IADG_DevelopmentPart_checkStatus", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", Project)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


#End Region


    'DEVELOPMENT PART APPROVAL FUNCTION
#Region "Development Part Approval"
    Public Shared Function GetListApproval(pProjectID As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_IADG_DevelopmentPart_Approval_List"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", pProjectID)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    '
    Public Shared Function GetHeaderApprovalName(pProjectID As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_IADG_DevelopmentPart_Approval_getHeaderApprovalName"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", pProjectID)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function checkStatusApproval(Project As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_IADG_DevelopmentPart_StatusApproval", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", Project)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function developmentPartDetailApproval_Approve(Project As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_IADG_DevelopmentPart_Approval_Approve", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", Project)
                cmd.Parameters.AddWithValue("@UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

End Class

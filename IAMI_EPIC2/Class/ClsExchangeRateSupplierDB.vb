﻿Imports System.Data.SqlClient
Public Class ClsExchangeRateSupplierDB
    Public Shared Function getDataTableExchangeRateSupplier(ByVal pObj As ClsExchangeRateSupplier, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As DataTable
        Dim dt As New DataTable
        Dim cmd As SqlCommand
        Dim da As SqlDataAdapter
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_ExchangeRateSupplier_Sel"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@ExchangeRateDateFrom", CDate(pObj.ExchangeRateDateFrom))
                    cmd.Parameters.AddWithValue("@ExchangeRateDateTo", CDate(pObj.ExchangeRateDateTo))
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Currency", pObj.CurrencyCode)
                    da = New SqlDataAdapter(cmd)
                    da.Fill(dt)
                    da.Dispose()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                End Using
            Else
                sql = "sp_ExchangeRateSupplier_Sel"

                cmd = New SqlCommand
                cmd.Connection = pCon
                cmd.CommandText = sql
                If Not pTrans Is Nothing Then
                    cmd.Transaction = pTrans
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ExchangeRateDateFrom", CDate(pObj.ExchangeRateDateFrom))
                cmd.Parameters.AddWithValue("@ExchangeRateDateTo", CDate(pObj.ExchangeRateDateTo))
                cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                cmd.Parameters.AddWithValue("@Currency", pObj.CurrencyCode)
                da = New SqlDataAdapter(cmd)
                da.Fill(dt)
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
            End If
        Catch ex As Exception
            pErrMsg = "clsExchangeRateSupplierDB.getDataTableExchangeRate: " & ex.Message
            Return Nothing
        End Try
        Return dt
    End Function

    Public Shared Function getDataTableExchangeRateSupplierDetail(ByVal pObj As ClsExchangeRateSupplier, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As DataTable
        Dim dt As New DataTable
        Dim cmd As SqlCommand
        Dim da As SqlDataAdapter
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_ExchangeRateSupplier_Sel_Detail"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Currency", pObj.CurrencyCode)
                    cmd.Parameters.AddWithValue("@ExchangeRateDate", CDate(pObj.ExchangeRateDate))
                    da = New SqlDataAdapter(cmd)
                    da.Fill(dt)
                    da.Dispose()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                End Using
            Else
                sql = "sp_ExchangeRateSupplier_Sel_Detail"

                cmd = New SqlCommand
                cmd.Connection = pCon
                cmd.CommandText = sql
                If Not pTrans Is Nothing Then
                    cmd.Transaction = pTrans
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                cmd.Parameters.AddWithValue("@Currency", pObj.CurrencyCode)
                cmd.Parameters.AddWithValue("@ExchangeRateDate", CDate(pObj.ExchangeRateDate))
                da = New SqlDataAdapter(cmd)
                da.Fill(dt)
                da.Dispose()
                cmd.Parameters.Clear()
                cmd.Dispose()
            End If
        Catch ex As Exception
            pErrMsg = "clsExchangeRateSupplierDB.getDataTableExchangeRateDetail: " & ex.Message
            Return Nothing
        End Try
        Return dt
    End Function

    Public Shared Function insert(ByVal pObj As ClsExchangeRateSupplier, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As Boolean
        Dim result As Boolean = False
        Dim cmd As SqlCommand
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_ExchangeRateSupplier_Ins"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Currency", pObj.CurrencyCode)
                    cmd.Parameters.AddWithValue("@ExchangeRateDate", CDate(pObj.ExchangeRateDate))
                    cmd.Parameters.AddWithValue("@BuyingRate", CDbl(pObj.BuyingRate))
                    cmd.Parameters.AddWithValue("@MiddleRate", CDbl(pObj.MiddleRate))
                    cmd.Parameters.AddWithValue("@SellingRate", CDbl(pObj.SellingRate))
                    cmd.Parameters.AddWithValue("@Remarks", pObj.Remarks)
                    cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                    result = True
                End Using
            Else
                sql = "sp_ExchangeRateSupplier_Ins"

                cmd = New SqlCommand
                cmd.Connection = pCon
                cmd.CommandText = sql
                If Not pTrans Is Nothing Then
                    cmd.Transaction = pTrans
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                cmd.Parameters.AddWithValue("@Currency", pObj.CurrencyCode)
                cmd.Parameters.AddWithValue("@ExchangeRateDate", CDate(pObj.ExchangeRateDate))
                cmd.Parameters.AddWithValue("@BuyingRate", CDbl(pObj.BuyingRate))
                cmd.Parameters.AddWithValue("@MiddleRate", CDbl(pObj.MiddleRate))
                cmd.Parameters.AddWithValue("@SellingRate", CDbl(pObj.SellingRate))
                cmd.Parameters.AddWithValue("@Remarks", pObj.Remarks)
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()
                cmd.Dispose()
                result = True
            End If
        Catch ex As Exception
            pErrMsg = "clsExchangeRateSupplierDB.insert: " & ex.Message
            Return False
        End Try
        Return result
    End Function

    Public Shared Function update(ByVal pObj As ClsExchangeRateSupplier, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As Boolean
        Dim result As Boolean = False
        Dim cmd As SqlCommand
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_ExchangeRateSupplier_Upd"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Currency", pObj.CurrencyCode)
                    cmd.Parameters.AddWithValue("@ExchangeRateDate", CDate(pObj.ExchangeRateDate))
                    cmd.Parameters.AddWithValue("@BuyingRate", CDbl(pObj.BuyingRate))
                    cmd.Parameters.AddWithValue("@MiddleRate", CDbl(pObj.MiddleRate))
                    cmd.Parameters.AddWithValue("@SellingRate", CDbl(pObj.SellingRate))
                    cmd.Parameters.AddWithValue("@Remarks", pObj.Remarks)
                    cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                    result = True
                End Using
            Else
                sql = "sp_ExchangeRateSupplier_Upd"

                cmd = New SqlCommand
                cmd.Connection = pCon
                cmd.CommandText = sql
                If Not pTrans Is Nothing Then
                    cmd.Transaction = pTrans
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                cmd.Parameters.AddWithValue("@Currency", pObj.CurrencyCode)
                cmd.Parameters.AddWithValue("@ExchangeRateDate", CDate(pObj.ExchangeRateDate))
                cmd.Parameters.AddWithValue("@BuyingRate", CDbl(pObj.BuyingRate))
                cmd.Parameters.AddWithValue("@MiddleRate", CDbl(pObj.MiddleRate))
                cmd.Parameters.AddWithValue("@SellingRate", CDbl(pObj.SellingRate))
                cmd.Parameters.AddWithValue("@Remarks", pObj.Remarks)
                cmd.Parameters.AddWithValue("@UserID", pObj.UserID)
                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()
                cmd.Dispose()
                result = True
            End If
        Catch ex As Exception
            pErrMsg = "clsExchangeRateSupplierDB.update: " & ex.Message
            Return False
        End Try
        Return result
    End Function

    Public Shared Function delete(ByVal pObj As ClsExchangeRateSupplier, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As Boolean
        Dim result As Boolean = False
        Dim cmd As SqlCommand
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_ExchangeRateSupplier_Del"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Currency", pObj.CurrencyCode)
                    cmd.Parameters.AddWithValue("@ExchangeRateDate", CDate(pObj.ExchangeRateDate))
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                    result = True
                End Using
            Else
                sql = "sp_ExchangeRateSupplier_Upd"

                cmd = New SqlCommand
                cmd.Connection = pCon
                cmd.CommandText = sql
                If Not pTrans Is Nothing Then
                    cmd.Transaction = pTrans
                End If
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                cmd.Parameters.AddWithValue("@Currency", pObj.CurrencyCode)
                cmd.Parameters.AddWithValue("@ExchangeRateDate", CDate(pObj.ExchangeRateDate))
                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()
                cmd.Dispose()
                result = True
            End If
        Catch ex As Exception
            pErrMsg = "clsExchangeRateSupplierDB.delete: " & ex.Message
            Return False
        End Try
        Return result
    End Function

    Public Shared Function GetPeriodAdjustment(pPeriodID As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "EXEC sp_ReportExchangeRateSupplier_GetPeriodAdj '" & pPeriodID & "' "

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetList(pDateFrom As String, pDateTo As String, pPeriodID As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String
                sql = "sp_ReportExchangeRateSupplier_GetList"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@PeriodFrom", pDateFrom)
                cmd.Parameters.AddWithValue("@PeriodTo", pDateTo)
                cmd.Parameters.AddWithValue("@PeriodID", pPeriodID)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class
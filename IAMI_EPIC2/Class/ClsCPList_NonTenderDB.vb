﻿Imports System.Data.SqlClient

Public Class ClsCPList_NonTenderDB
    Public Shared Function GetData(ByVal pCPList As ClsCPList_NonTender, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_CPList_Sel"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("DtFrom", pCPList.QuotationDateFrom)
                cmd.Parameters.AddWithValue("DtTo", pCPList.QuotationDateTo)
                cmd.Parameters.AddWithValue("Supplier", pCPList.Supplier)
                cmd.Parameters.AddWithValue("Counter", pCPList.CounterProposal)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

﻿Imports System.Data.SqlClient

Public Class ClsRFQCheekSheetDB

    Public Shared Function getlistApproveHeader(Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Sel_Name_Approve"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getlistds(ByVal pFrom As String, ByVal pTo As String, ByVal ProjectType As String, ByVal ProjectID As String, _
                                 ByVal Group As String, ByVal Comodity As String, ByVal Group_Comodity As String, ByVal UserID As String, Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("From", pFrom)
                cmd.Parameters.AddWithValue("To", pTo)
                'cmd.Parameters.AddWithValue("ProjectType", ProjectType)
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", Group_Comodity)
                cmd.Parameters.AddWithValue("UserID", UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getlist(ByVal pFrom As String, ByVal pTo As String, ByVal ProjectType As String, ByVal ProjectID As String, _
                                   ByVal Group As String, ByVal Comodity As String, ByVal Group_Comodity As String, ByVal UserID As String, Optional ByRef perr As String = "") As List(Of ClsRFQCheekSheet)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("From", pFrom)
                cmd.Parameters.AddWithValue("To", pTo)
                'cmd.Parameters.AddWithValue("ProjectType", ProjectType)
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", Group_Comodity)
                cmd.Parameters.AddWithValue("UserID", UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsRFQCheekSheet)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New ClsRFQCheekSheet
                    ss.NoRFQ = Trim(dt.Rows(i).Item("NoRFQ") & "")
                    ss.Project_ID = Trim(dt.Rows(i).Item("Project_ID") & "")
                    ss.Project_Name = Trim(dt.Rows(i).Item("Project_Name") & "")
                    ss.Group_ID = Trim(dt.Rows(i).Item("Group_ID") & "")
                    ss.Comodity = Trim(dt.Rows(i).Item("Comodity") & "")
                    ss.Group_Comodity = Trim(dt.Rows(i).Item("Group_Comodity") & "")
                    ss.DateRFQ = Format(dt.Rows(i).Item("DateRFQ"), "dd-MM-yyyy")
                    ss.ToRFQ = Trim(dt.Rows(i).Item("ToRFQ") & "")
                    ss.PhoneRFQ = Trim(dt.Rows(i).Item("PhoneRFQ") & "")
                    ss.FaxRFQ = Trim(dt.Rows(i).Item("FaxRFQ") & "")
                    ss.AttDeptHead = Trim(dt.Rows(i).Item("AttDeptHead") & "")
                    ss.AttDeptHeadName = Trim(dt.Rows(i).Item("AttDeptHeadName") & "")
                    ss.EmailDeptHead = Trim(dt.Rows(i).Item("Email_Dept_Head") & "")
                    ss.AttProjectPIC = Trim(dt.Rows(i).Item("AttProjectPIC") & "")
                    ss.AttProjectPICName = Trim(dt.Rows(i).Item("AttProjectPICName") & "")
                    ss.EmailProjectPIC = Trim(dt.Rows(i).Item("EmailProjectPIC") & "")
                    ss.AttCostPIC = Trim(dt.Rows(i).Item("AttCostPIC") & "")
                    ss.AttCostPICName = Trim(dt.Rows(i).Item("AttCostPICName") & "")
                    ss.EmailCostPIC = Trim(dt.Rows(i).Item("EmailCostPIC") & "")
                    ss.VendorCode = Trim(dt.Rows(i).Item("VendorCode") & "")
                    ss.VendorName = Trim(dt.Rows(i).Item("VendorName") & "")
                    ss.ApprovalName1 = Trim(dt.Rows(i).Item("AppPerson1") & "")
                    ss.ApprovalName2 = Trim(dt.Rows(i).Item("AppPerson2") & "")
                    ss.ApprovalName3 = Trim(dt.Rows(i).Item("AppPerson3") & "")
                    ss.ApprovalName4 = Trim(dt.Rows(i).Item("AppPerson4") & "")
                    ss.ApprovalName5 = Trim(dt.Rows(i).Item("AppPerson5") & "")
                    ss.ApprovalName6 = Trim(dt.Rows(i).Item("AppPerson6") & "")
                    If dt.Rows(i).Item("AppDate1").ToString = "" Then
                        ss.ApprovalDate1 = ""
                    Else
                        ss.ApprovalDate1 = Format(dt.Rows(i).Item("AppDate1"), "dd MMM yyyy")
                    End If
                    If dt.Rows(i).Item("AppDate2").ToString = "" Then
                        ss.ApprovalDate2 = ""
                    Else
                        ss.ApprovalDate2 = Format(dt.Rows(i).Item("AppDate2"), "dd MMM yyyy")
                    End If
                    If dt.Rows(i).Item("AppDate3").ToString = "" Then
                        ss.ApprovalDate3 = ""
                    Else
                        ss.ApprovalDate3 = Format(dt.Rows(i).Item("AppDate3"), "dd MMM yyyy")
                    End If
                    If dt.Rows(i).Item("AppDate4").ToString = "" Then
                        ss.ApprovalDate4 = ""
                    Else
                        ss.ApprovalDate4 = Format(dt.Rows(i).Item("AppDate4"), "dd MMM yyyy")
                    End If
                    If dt.Rows(i).Item("AppDate5").ToString = "" Then
                        ss.ApprovalDate5 = ""
                    Else
                        ss.ApprovalDate5 = Format(dt.Rows(i).Item("AppDate5"), "dd MMM yyyy")
                    End If
                    If dt.Rows(i).Item("AppDate6").ToString = "" Then
                        ss.ApprovalDate6 = ""
                    Else
                        ss.ApprovalDate6 = Format(dt.Rows(i).Item("AppDate6"), "dd MMM yyyy")
                    End If
                    list.Add(ss)

                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getlistdsApproval(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal Group_Comodity As String, ByVal UserID As String, Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Sel_Approval"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", Group_Comodity)
                cmd.Parameters.AddWithValue("UserID", UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getlistApproval(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal Group_Comodity As String, ByVal UserID As String, Optional ByRef perr As String = "") As List(Of ClsRFQCheekSheet)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Sel_Approval"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", Group_Comodity)
                cmd.Parameters.AddWithValue("UserID", UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsRFQCheekSheet)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New ClsRFQCheekSheet With {
                        .NoRFQ = Trim(dt.Rows(i).Item("NoRFQ") & ""),
                        .Project_ID = Trim(dt.Rows(i).Item("Project_ID") & ""),
                        .Project_Name = Trim(dt.Rows(i).Item("Project_Name") & ""),
                        .Group_ID = Trim(dt.Rows(i).Item("Group_ID") & ""),
                        .Comodity = Trim(dt.Rows(i).Item("Comodity") & ""),
                        .Group_Comodity = Trim(dt.Rows(i).Item("Group_Comodity") & ""),
                        .DateRFQ = Format(dt.Rows(i).Item("DateRFQ"), "dd-MM-yyyy"),
                        .ToRFQ = Trim(dt.Rows(i).Item("ToRFQ") & ""),
                        .PhoneRFQ = Trim(dt.Rows(i).Item("PhoneRFQ") & ""),
                        .FaxRFQ = Trim(dt.Rows(i).Item("FaxRFQ") & ""),
                        .AttDeptHead = Trim(dt.Rows(i).Item("AttDeptHead") & ""),
                        .AttDeptHeadName = Trim(dt.Rows(i).Item("AttDeptHeadName") & ""),
                        .EmailDeptHead = Trim(dt.Rows(i).Item("EmailDeptHead") & ""),
                        .AttProjectPIC = Trim(dt.Rows(i).Item("AttProjectPIC") & ""),
                        .AttProjectPICName = Trim(dt.Rows(i).Item("AttProjectPICName") & ""),
                        .EmailProjectPIC = Trim(dt.Rows(i).Item("EmailProjectPIC") & ""),
                        .AttCostPIC = Trim(dt.Rows(i).Item("AttCostPIC") & ""),
                        .AttCostPICName = Trim(dt.Rows(i).Item("AttCostPICName") & ""),
                        .EmailCostPIC = Trim(dt.Rows(i).Item("EmailCostPIC") & ""),
                           .VendorCode = Trim(dt.Rows(i).Item("VendorCode") & ""),
                    .VendorName = Trim(dt.Rows(i).Item("VendorName") & "")
                                             }
                    list.Add(ss)

                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function isExist(ByVal pProjectID As String, ByVal pGroup As String, ByVal pComodity As String, ByVal pGroupComodity As String, ByVal pVendorCode As String, ByVal UserID As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Exist"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", pProjectID)
                cmd.Parameters.AddWithValue("Group", pGroup)
                cmd.Parameters.AddWithValue("Comodity", pComodity)
                cmd.Parameters.AddWithValue("Group_Comodity", pGroupComodity)
                cmd.Parameters.AddWithValue("VendorCode", pVendorCode)
                cmd.Parameters.AddWithValue("UserID", UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count <> 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pSes As ClsRFQCheekSheet, Optional ByRef pErr As String = "") As Integer
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                cmd = New SqlCommand("sp_Gate_I_CS_RFQ_Insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", pSes.Project_ID)
                cmd.Parameters.AddWithValue("Group", pSes.Group_ID)
                cmd.Parameters.AddWithValue("Comodity", pSes.Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", pSes.Group_Comodity)
                cmd.Parameters.AddWithValue("VendorCode", pSes.VendorCode)
                cmd.Parameters.AddWithValue("To_RFQ", pSes.ToRFQ)
                cmd.Parameters.AddWithValue("Phone_RFQ", pSes.PhoneRFQ)
                cmd.Parameters.AddWithValue("Fax_RFQ", pSes.FaxRFQ)
                cmd.Parameters.AddWithValue("Att_Dept_Head", pSes.AttDeptHead)
                cmd.Parameters.AddWithValue("Att_Project_PIC", pSes.AttProjectPIC)
                cmd.Parameters.AddWithValue("Att_Cost_PIC", pSes.AttCostPIC)
                cmd.Parameters.AddWithValue("UserID", pSes.UserID)
                i = cmd.ExecuteNonQuery
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function Update(ByVal pSes As ClsRFQCheekSheet, Optional ByRef pErr As String = "") As Integer
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                cmd = New SqlCommand("sp_Gate_I_CS_RFQ_Update", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", pSes.Project_ID)
                cmd.Parameters.AddWithValue("Group", pSes.Group_ID)
                cmd.Parameters.AddWithValue("Comodity", pSes.Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", pSes.Group_Comodity)
                cmd.Parameters.AddWithValue("VendorCode", pSes.VendorCode)
                cmd.Parameters.AddWithValue("NoRfQ", pSes.NoRFQ)
                cmd.Parameters.AddWithValue("To_RFQ", pSes.ToRFQ)
                cmd.Parameters.AddWithValue("Phone_RFQ", pSes.PhoneRFQ)
                cmd.Parameters.AddWithValue("Fax_RFQ", pSes.FaxRFQ)
                cmd.Parameters.AddWithValue("Att_Dept_Head", pSes.AttDeptHead)
                cmd.Parameters.AddWithValue("Att_Project_PIC", pSes.AttProjectPIC)
                cmd.Parameters.AddWithValue("Att_Cost_PIC", pSes.AttCostPIC)
                cmd.Parameters.AddWithValue("UserID", pSes.UserID)
                i = cmd.ExecuteNonQuery
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function UpdateApproval(ByVal pSes As ClsRFQCheekSheet, Optional ByRef pErr As String = "") As Integer
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                cmd = New SqlCommand("sp_Gate_I_CS_RFQ_Update_Approval", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", pSes.Project_ID)
                cmd.Parameters.AddWithValue("Group", pSes.Group_ID)
                cmd.Parameters.AddWithValue("Comodity", pSes.Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", pSes.Group_Comodity)
                cmd.Parameters.AddWithValue("VendorCode", pSes.VendorCode)
                cmd.Parameters.AddWithValue("UserID", pSes.UserID)
                i = cmd.ExecuteNonQuery
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function Delete(ByVal pSes As ClsRFQCheekSheet, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Del"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", pSes.Project_ID)
                cmd.Parameters.AddWithValue("Group", pSes.Group_ID)
                cmd.Parameters.AddWithValue("Comodity", pSes.Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", pSes.Group_Comodity)
                cmd.Parameters.AddWithValue("VendorCode", pSes.VendorCode)
                cmd.Parameters.AddWithValue("NoRfQ", pSes.NoRFQ)
                cmd.Parameters.AddWithValue("UserID", pSes.UserID)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function
    Public Shared Function getProjectID(ProjectType As String, ProjectID As String, GroupID As String, Commodity As String, Status As String, UserID As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_CS_RFQ_ComboFilter", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_Type", IIf(ProjectType = Nothing, "", ProjectType))
                cmd.Parameters.AddWithValue("Project_ID", IIf(ProjectID = Nothing, "", ProjectID))
                cmd.Parameters.AddWithValue("Group_ID", IIf(GroupID = Nothing, "", GroupID))
                cmd.Parameters.AddWithValue("Commodity", IIf(Commodity = Nothing, "", Commodity))
                cmd.Parameters.AddWithValue("Status", Status)
                cmd.Parameters.AddWithValue("UserID", UserID)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getProjectIDApproval(ProjectID As String, GroupID As String, Commodity As String, Status As String, UserID As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_CS_RFQ_ComboFilter_Approval", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", IIf(ProjectID = Nothing, "", ProjectID))
                cmd.Parameters.AddWithValue("Group_ID", IIf(GroupID = Nothing, "", GroupID))
                cmd.Parameters.AddWithValue("Commodity", IIf(Commodity = Nothing, "", Commodity))
                cmd.Parameters.AddWithValue("Status", Status)
                cmd.Parameters.AddWithValue("UserID", UserID)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function isExistinQoutation(ByVal pProjectID As String, ByVal pGroup As String, ByVal pComodity As String, ByVal pGroupComodity As String, ByVal pVendorCode As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Exist_CreateFromSupplier"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", pProjectID)
                cmd.Parameters.AddWithValue("Group", pGroup)
                cmd.Parameters.AddWithValue("Comodity", pComodity)
                cmd.Parameters.AddWithValue("Group_Comodity", pGroupComodity)
                cmd.Parameters.AddWithValue("VendorCode", IIf(pVendorCode = Nothing, "", pVendorCode))
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count <> 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getDetailHeader(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal Group_Comodity As String, ByVal Vendor_Code As String, Optional ByRef perr As String = "") As DataTable
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Sel_Header"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", Group_Comodity)
                cmd.Parameters.AddWithValue("VendorCode", Vendor_Code)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Return dt
            End Using

        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getDetaillist(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal Group_Comodity As String, Optional ByRef perr As String = "") As List(Of ClsRFQCheekSheet)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Sel_Detail_PartNo"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", Group_Comodity)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsRFQCheekSheet)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New ClsRFQCheekSheet With {
                        .No = i + 1,
                        .Upc = Trim(dt.Rows(i).Item("Upc") & ""),
                        .Fna = Trim(dt.Rows(i).Item("Fna") & ""),
                        .Dwg_No = Trim(dt.Rows(i).Item("Dwg_No") & ""),
                        .Part_No = Trim(dt.Rows(i).Item("Part_No") & ""),
                        .Part_Name = Trim(dt.Rows(i).Item("Part_Name") & ""),
                        .Qty = Trim(dt.Rows(i).Item("Qty") & ""),
                        .VolumeYears = Trim(dt.Rows(i).Item("VolumeYears") & ""),
                        .Remarks = Trim(dt.Rows(i).Item("Remarks") & "")
                        }
                    list.Add(ss)

                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getDetaillistds(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal Group_Comodity As String, Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_Sel_Detail_PartNo"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", Group_Comodity)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                'Dim list As New List(Of ClsRFQCheekSheet)
                'For i = 0 To dt.Rows.Count - 1
                '    Dim ss As New ClsRFQCheekSheet With {
                '        .No = i + 1,
                '        .Upc = Trim(dt.Rows(i).Item("Upc") & ""),
                '        .Fna = Trim(dt.Rows(i).Item("Fna") & ""),
                '        .Dwg_No = Trim(dt.Rows(i).Item("Dwg_No") & ""),
                '        .Part_No = Trim(dt.Rows(i).Item("Part_No") & ""),
                '        .Part_Name = Trim(dt.Rows(i).Item("Part_Name") & ""),
                '        .Qty = Trim(dt.Rows(i).Item("Qty") & ""),
                '        .VolumeYears = Trim(dt.Rows(i).Item("VolumeYears") & ""),
                '        .Remarks = Trim(dt.Rows(i).Item("Remarks") & "")
                '        }
                '    list.Add(ss)

                'Next
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function SubmitRFQCheckSheet(ByVal pSes As ClsRFQCheekSheet, Optional ByRef pErr As String = "") As Integer
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                cmd = New SqlCommand("sp_Gate_I_CS_RFQ_Submit", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", pSes.Project_ID)
                cmd.Parameters.AddWithValue("Group", pSes.Group_ID)
                cmd.Parameters.AddWithValue("Comodity", pSes.Comodity)
                cmd.Parameters.AddWithValue("Group_Comodity", pSes.Group_Comodity)
                cmd.Parameters.AddWithValue("VendorCode", pSes.VendorCode)
                cmd.Parameters.AddWithValue("RFQNo", pSes.NoRFQ)
                cmd.Parameters.AddWithValue("UserID", pSes.UserID)
                i = cmd.ExecuteNonQuery
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function isSubmitted(ByVal pProjectID As String, ByVal pGroup As String, ByVal pComodity As String, ByVal pGroupComodity As String, ByVal pVendorCode As String, ByVal RFQNo As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_I_CS_RFQ_CheckSubmitData"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", pProjectID)
                cmd.Parameters.AddWithValue("Group", pGroup)
                cmd.Parameters.AddWithValue("Comodity", pComodity)
                cmd.Parameters.AddWithValue("Group_Comodity", pGroupComodity)
                cmd.Parameters.AddWithValue("VendorCode", pVendorCode)
                cmd.Parameters.AddWithValue("RFQNo", RFQNo)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows(0)("RetVal").ToString = "1" Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

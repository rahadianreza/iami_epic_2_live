﻿Imports System.Data.SqlClient
Public Class ClsMenuSetDB

    Public Shared Function GetComboCaterer() As List(Of ClsMenuSet)
        Using ConnectionData As New SqlClient.SqlConnection(Sconn.Stringkoneksi)
            ConnectionData.Open()

            Dim cmd As New SqlCommand
            Dim dt As New DataSet

            dt.Clear()
            dt = New DataSet
            cmd = New SqlCommand("[dbo].[sp_MenuSetGetComboCarter]", ConnectionData)
            cmd.CommandType = CommandType.StoredProcedure

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)

            Dim pCoba As New List(Of ClsMenuSet)

            If dt.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dt.Tables(0).Rows.Count - 1
                    Dim pCB As New ClsMenuSet With {.CatererID = Trim(dt.Tables(0).Rows(i)("CatererID")),
                                                 .CatererName = Trim(dt.Tables(0).Rows(i)("CatererName"))}
                    pCoba.Add(pCB)
                Next

            End If
            Return pCoba

        End Using

    End Function

    Public Shared Function GetComboMenu(ByVal pCatererID As String, Optional ByRef pErr As String = "") As List(Of ClsMenuSet)

        Using ConnectionData As New SqlClient.SqlConnection(Sconn.Stringkoneksi)
            ConnectionData.Open()

            Dim cmd As New SqlCommand
            Dim dt As New DataSet

            dt.Clear()
            dt = New DataSet
            cmd = New SqlCommand("[dbo].[sp_MenuSetGetComboMenu]", ConnectionData)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.AddWithValue("CatererID", pCatererID)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)

            Dim pCoba As New List(Of ClsMenuSet)

            If dt.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dt.Tables(0).Rows.Count - 1
                    Dim pCB As New ClsMenuSet With {.CboMenuId = Trim(dt.Tables(0).Rows(i)("MenuID")),
                                                 .CboMenuName = Trim(dt.Tables(0).Rows(i)("MenuName"))}
                    pCoba.Add(pCB)
                Next

            End If
            Return pCoba

        End Using

    End Function

    Public Shared Function getlistMenu(ByVal pCatererID As String, ByVal pMenuID As String, Optional ByRef perr As String = "") As List(Of ClsMenuSet)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_MenuSetGetGrid"
                Dim cmd As New SqlCommand(sql, conn)

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("CatererID", pCatererID)
                cmd.Parameters.AddWithValue("MenuID", pMenuID)

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsMenuSet)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New ClsMenuSet With {
                        .CatererID = Trim(dt.Rows(i).Item("CatererID") & ""),
                        .MenuID = Trim(dt.Rows(i).Item("MenuID") & ""),
                        .SetMenuID = Trim(dt.Rows(i).Item("SetMenuID") & ""),
                        .MenuName = Trim(dt.Rows(i).Item("MenuName") & "")
                                                   }
                    list.Add(ss)

                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pMenuSet As ClsMenuSet, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_MenuSet_Ins"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CatererID", pMenuSet.CatererID)
                cmd.Parameters.AddWithValue("MenuID", pMenuSet.MenuID)
                cmd.Parameters.AddWithValue("SetMenuID", pMenuSet.SetMenuID)


                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function Update(ByVal pMenuSet As ClsMenuSet, Optional ByRef pErr As String = "") As Integer
        '    Try
        '        Using conn As New SqlConnection(Sconn.Stringkoneksi)
        '            conn.Open()
        '            Dim sql As String = ""
        '            sql = "sp_MenuSet_Upd"

        '            Dim cmd As New SqlCommand(sql, conn)
        '            cmd.CommandType = CommandType.StoredProcedure
        '            cmd.Parameters.AddWithValue("CatererID", pMenuSet.CatererID)
        '            cmd.Parameters.AddWithValue("MenuID", pMenuSet.MenuID)
        '            cmd.Parameters.AddWithValue("SetMenuID", pMenuSet.SetMenuID)
        '            cmd.Parameters.AddWithValue("SetMenuID", pMenuSet.SetMenuID)

        '            Dim rtn As Integer = cmd.ExecuteNonQuery
        '            Return rtn
        '        End Using
        '    Catch ex As Exception
        '        pErr = ex.Message
        '        Return 0
        '    End Try
    End Function

    Public Shared Function Delete(ByVal pMenuSet As ClsMenuSet, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_MenuSet_Del"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CatererID", pMenuSet.CatererID)
                cmd.Parameters.AddWithValue("MenuID", pMenuSet.MenuID)
                cmd.Parameters.AddWithValue("SetMenuID", pMenuSet.SetMenuID)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function isExist(ByVal pCatererID As String, ByVal pMenuID As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_MenuSet_Exist"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CatererID", pCatererID)
                cmd.Parameters.AddWithValue("MenuID", pMenuID)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count <> 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

﻿Imports System.Data.SqlClient

Public Class ClsLabourDB
    Public Shared Function getListLabor(PeriodFrom As String, PeriodTo As String, Province As String, City As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Labour_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@PeriodFrom", PeriodFrom)
                cmd.Parameters.AddWithValue("@PeriodTo", PeriodTo)
                cmd.Parameters.AddWithValue("@Province", Province)
                cmd.Parameters.AddWithValue("@City", City)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getDetail(pProvince As String, pRegion As String, pPeriod As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Labour_Detail_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Province", pProvince)
                cmd.Parameters.AddWithValue("@Region", pRegion)
                cmd.Parameters.AddWithValue("@Period", pPeriod)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function InsertLabor(pProvince As String, pRegion As String, pPeriod As String, _
                                              pUMR As Decimal, pEstimatedUMSP As Decimal, pSectoral As String, pUMSP As Decimal, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Labour_Detail_Insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Province", pProvince)
                cmd.Parameters.AddWithValue("@Region", pRegion)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@UMR", pUMR)

                cmd.Parameters.AddWithValue("@Sectoral", pSectoral)
                cmd.Parameters.AddWithValue("@UMSP", pUMSP)
                cmd.Parameters.AddWithValue("@EstimatedUMSP", pEstimatedUMSP)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateLabor(pProvince As String, pRegion As String, pPeriod As String, _
                                              pUMR As Decimal, pEstimatedUMSP As Decimal, pSectoral As String, pUMSP As Decimal, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Labour_Detail_Update", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Province", pProvince)
                cmd.Parameters.AddWithValue("@Region", pRegion)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@UMR", pUMR)

                cmd.Parameters.AddWithValue("@Sectoral", pSectoral)
                cmd.Parameters.AddWithValue("@UMSP", pUMSP)
                cmd.Parameters.AddWithValue("@EstimatedUMSP", pEstimatedUMSP)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteLaborData(pProvince As String, pRegion As String, pPeriod As String, _
                                             pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Labour_Delete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Province", pProvince)
                cmd.Parameters.AddWithValue("@Region", pRegion)
                cmd.Parameters.AddWithValue("@Period", pPeriod)

                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function



    Public Shared Function GetDataProvinsi(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try

            Dim sQuery As String = ""
            sQuery = "Select Rtrim(Par_Code) Province, Rtrim(Par_Description) ProvinceDesc  From Mst_Parameter Where Par_Group = 'Provinsi'"

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sQuery, con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function GetDataRegion(pProvince As String, pType As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            If pProvince = "ALL" Then
                pProvince = ""
            End If

            Dim sQuery As String = ""
            If pProvince = "" Then
                sQuery = "SELECT 'ALL' Region ,'ALL' RegionDesc UNION ALL SELECT Par_Code as Region,Par_Description as RegionDesc FROM dbo.Mst_Parameter WHERE Par_Group='KABUPATEN' "
            Else
                If pType = "ALL" Then
                    sQuery = "SELECT 'ALL' Region ,'ALL' RegionDesc UNION ALL SELECT Par_Code as Region,Par_Description as RegionDesc FROM dbo.Mst_Parameter WHERE Par_Group='KABUPATEN' AND Par_ParentCode='" & pProvince & "'"
                Else
                    sQuery = "SELECT Par_Code as Region,Par_Description as RegionDesc FROM dbo.Mst_Parameter WHERE Par_Group='KABUPATEN' AND Par_ParentCode='" & pProvince & "'"
                End If

            End If

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sQuery, con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataSectoral(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try

            Dim sQuery As String = ""
            sQuery = "Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Sectoral'"

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sQuery, con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


End Class

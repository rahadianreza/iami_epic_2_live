﻿Imports System.Data.SqlClient
Public Class ClsChangePassworddb
    Public Shared Function Update(ByVal pSes As Cls_UserSetup, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_ChangePassword"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Password", pSes.Password)
                cmd.Parameters.AddWithValue("UserID", pSes.UserID)
                cmd.Parameters.AddWithValue("PasswordHint", pSes.PasswordHint)
                cmd.Parameters.AddWithValue("SecurityQuestion", pSes.SecurityQuestion)
                cmd.Parameters.AddWithValue("SecurityAnswer", pSes.SecurityAnswer)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function
End Class

﻿Imports System.Data.SqlClient

Public Class cls_MasterToolingDB
    Public Shared Function getGrid(ToolingId As String, PartNo As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_MasterTooling_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ToolingId", IIf(String.IsNullOrEmpty(ToolingId), "", ToolingId))
                cmd.Parameters.AddWithValue("@Part_No", IIf(String.IsNullOrEmpty(PartNo), "", PartNo))
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getGridDepreciation(PartNo As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_MasterToolingDepreciation_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Part_No", IIf(String.IsNullOrEmpty(PartNo), "", PartNo))
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertMasterTooling(ToolingId As String, ToolingName As String, AssetNo As String, ContractNo As String, Additional1 As String, Additional2 As String, PartNo As String, PartName As String, _
                                               UoM As String, Qty As Integer, numberofprocess As String, Dimension As String, weight As Decimal, price As Decimal, model As String, groupId As String, purchasedYear As Integer, _
                                               investationNo As String, registerby As String, updateby As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_MasterTooling_Ins"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Tooling_ID", ToolingId)
                cmd.Parameters.AddWithValue("@Tooling_Name", ToolingName)
                cmd.Parameters.AddWithValue("@Asset_No", AssetNo)
                cmd.Parameters.AddWithValue("@Contract_No", ContractNo)
                cmd.Parameters.AddWithValue("@Additional_1", Additional1)
                cmd.Parameters.AddWithValue("@Additional_2", Additional2)
                cmd.Parameters.AddWithValue("@Part_No", PartNo)
                cmd.Parameters.AddWithValue("@Part_Name", PartName)
                cmd.Parameters.AddWithValue("@UoM", UoM)
                cmd.Parameters.AddWithValue("@Qty", Qty)
                cmd.Parameters.AddWithValue("@Number_Of_Process", numberofprocess)
                cmd.Parameters.AddWithValue("@Dimension", Dimension)
                cmd.Parameters.AddWithValue("@Weight", weight)
                cmd.Parameters.AddWithValue("@Price", price)
                cmd.Parameters.AddWithValue("@Model", model)
                cmd.Parameters.AddWithValue("@Group_ID", groupId)
                cmd.Parameters.AddWithValue("@Purchased_Year", purchasedYear)
                cmd.Parameters.AddWithValue("@Investation_No", investationNo)
                cmd.Parameters.AddWithValue("@Register_By", registerby)
                cmd.Parameters.AddWithValue("@Update_By", updateby)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function UpdateMasterTooling(ToolingId As String, ToolingName As String, AssetNo As String, ContractNo As String, Additional1 As String, Additional2 As String, PartNo As String, PartName As String, _
                                               UoM As String, Qty As Integer, numberofprocess As String, Dimension As String, weight As Decimal, price As Decimal, model As String, groupId As String, purchasedYear As Integer, _
                                               investationNo As String, registerby As String, updateby As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_MasterTooling_Upd"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Tooling_ID", ToolingId)
                cmd.Parameters.AddWithValue("@Tooling_Name", ToolingName)
                cmd.Parameters.AddWithValue("@Asset_No", AssetNo)
                cmd.Parameters.AddWithValue("@Contract_No", ContractNo)
                cmd.Parameters.AddWithValue("@Additional_1", Additional1)
                cmd.Parameters.AddWithValue("@Additional_2", Additional2)
                cmd.Parameters.AddWithValue("@Part_No", PartNo)
                cmd.Parameters.AddWithValue("@Part_Name", PartName)
                cmd.Parameters.AddWithValue("@UoM", UoM)
                cmd.Parameters.AddWithValue("@Qty", Qty)
                cmd.Parameters.AddWithValue("@Number_Of_Process", numberofprocess)
                cmd.Parameters.AddWithValue("@Dimension", Dimension)
                cmd.Parameters.AddWithValue("@Weight", weight)
                cmd.Parameters.AddWithValue("@Price", price)
                cmd.Parameters.AddWithValue("@Model", model)
                cmd.Parameters.AddWithValue("@Group_ID", groupId)
                cmd.Parameters.AddWithValue("@Purchased_Year", purchasedYear)
                cmd.Parameters.AddWithValue("@Investation_No", investationNo)
                cmd.Parameters.AddWithValue("@Register_By", registerby)
                cmd.Parameters.AddWithValue("@Update_By", updateby)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function
End Class

﻿Imports System.Data.SqlClient

Public Class clsSummaryPRDB
    Public Shared Function GetList(pDateFrom As String, pDateTo As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String
                sql = "sp_Summary_PR"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("DateFrom", pDateFrom)
                cmd.Parameters.AddWithValue("DateTo", pDateTo)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

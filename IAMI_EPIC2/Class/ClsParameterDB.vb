﻿Imports System.Data.SqlClient

Public Class ClsParameterDB

    Public Shared Function getlist(ByVal a As String, ByVal pGroupID As String, ByVal pParentGroupID As String, Optional ByRef perr As String = "") As List(Of ClsParameter)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Parameter_Sel"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ID", a)
                cmd.Parameters.AddWithValue("ParGroup", pGroupID)
                cmd.Parameters.AddWithValue("ParentGroup", pParentGroupID)

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsParameter)
                For i = 0 To dt.Rows.Count - 1
                    If pGroupID = "GroupItem" Or pGroupID = "Category" Or pGroupID = "Kabupaten" Or pGroupID = "Section" Or pGroupID = "CostCenter" _
                        Or pGroupID = "MaterialGroupCategory" Or pGroupID = "MaterialGroupItem" Or pGroupID = "MaterialCode" Or pGroupID = "VAKind" Then
                        Dim ss As New ClsParameter With {
                        .General_Code = Trim(dt.Rows(i).Item("General_Code") & ""),
                        .General_Name = Trim(dt.Rows(i).Item("General_Name") & ""),
                        .Parent_Code = Trim(dt.Rows(i).Item("ParentName") & ""),
                        .RegisterBy = Trim(dt.Rows(i).Item("RegisterBy") & ""),
                        .RegisterDate = Trim(dt.Rows(i).Item("RegisterDate") & ""),
                        .UpdateBy = Trim(dt.Rows(i).Item("UpdateBy") & ""),
                        .UpdateDate = Trim(dt.Rows(i).Item("UpdateDate") & "")
                        }
                        list.Add(ss)
                        'ElseIf pGroupID = "MaterialGroupCategory" Then
                        '    Dim ss As New ClsParameter With {
                        '    .General_Code = Trim(dt.Rows(i).Item("General_Code") & ""),
                        '    .General_Name = Trim(dt.Rows(i).Item("General_Name") & ""),
                        '    .Parent_Code = Trim(dt.Rows(i).Item("General_ParentCode") & ""),
                        '    .RegisterBy = Trim(dt.Rows(i).Item("RegisterBy") & ""),
                        '    .RegisterDate = Trim(dt.Rows(i).Item("RegisterDate") & ""),
                        '    .UpdateBy = Trim(dt.Rows(i).Item("UpdateBy") & ""),
                        '    .UpdateDate = Trim(dt.Rows(i).Item("UpdateDate") & "")
                        '    }
                        '    list.Add(ss)
                    Else
                        Dim ss As New ClsParameter With {
                        .General_Code = Trim(dt.Rows(i).Item("General_Code") & ""),
                        .General_Name = Trim(dt.Rows(i).Item("General_Name") & ""),
                        .RegisterBy = Trim(dt.Rows(i).Item("RegisterBy") & ""),
                        .RegisterDate = Trim(dt.Rows(i).Item("RegisterDate") & ""),
                        .UpdateBy = Trim(dt.Rows(i).Item("UpdateBy") & ""),
                        .UpdateDate = Trim(dt.Rows(i).Item("UpdateDate") & "")
                        }
                        list.Add(ss)
                    End If
                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pSes As ClsParameter, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Parameter_Ins"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ParGroup", pSes.Cls_GroupID)
                cmd.Parameters.AddWithValue("GeneralCode", pSes.General_Code)
                cmd.Parameters.AddWithValue("GeneralName", pSes.General_Name)
                cmd.Parameters.AddWithValue("ParentCode", pSes.Parent_Code)
                cmd.Parameters.AddWithValue("UserID", pSes.RegisterBy)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function Update(ByVal pSes As ClsParameter, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Parameter_Upd"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ParGroup", pSes.Cls_GroupID)
                cmd.Parameters.AddWithValue("GeneralCode", pSes.General_Code)
                cmd.Parameters.AddWithValue("GeneralName", pSes.General_Name)
                cmd.Parameters.AddWithValue("ParentCode", pSes.Parent_Code)
                cmd.Parameters.AddWithValue("UserID", pSes.RegisterBy)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function Delete(ByVal pSes As ClsParameter, ByVal pGroupID As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Parameter_Del"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ParGroup", pGroupID)
                cmd.Parameters.AddWithValue("GeneralCode", pSes.General_Code)
                cmd.Parameters.AddWithValue("ParentCode", pSes.Parent_Code)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function isExist(ByVal pGeneralCode As String, ByVal pGroupID As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Parameter_Exist"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure                
                cmd.Parameters.AddWithValue("ParGroup", pGroupID)
                cmd.Parameters.AddWithValue("GeneralCode", pGeneralCode)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count <> 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

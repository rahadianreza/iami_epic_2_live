﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 24 Sep 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################

Public Class clsItemList
    Public Property ItemCode As String
    Public Property SAPNumber As String
    Public Property ItemName As String
    Public Property Specification As String
    Public Property LastIAPrice As Double
    Public Property LastSupplier As String
    Public Property UOM As String

    Public Property GroupItem As String
    Public Property CategoryItem As String
    Public Property PRType As String

    Public Property Picture01 As Byte

    Public Property Picture1 As Object
    Public Property Picture2 As Object
    Public Property Picture3 As Object

    Public Property CreateDate As String
    Public Property CreateUser As String
    Public Property UpdateDate As String
    Public Property UpdateUser As String
End Class

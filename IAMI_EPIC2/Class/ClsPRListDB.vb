﻿Imports System.Data.SqlClient

Public Class ClsPRListDB
    Public Shared Function GetListItem(pGroupItem As String, pCategory As String, pLastSupplier As String, pKeyWord As String, pPRType As String, ByVal pTypeTrx As Integer, ByVal pPRNo As String, Optional ByRef pShow As Integer = 0, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRList_GetListItem"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("GroupItem", pGroupItem)
                cmd.Parameters.AddWithValue("Category", pCategory)
                cmd.Parameters.AddWithValue("LastSupplier", pLastSupplier)
                cmd.Parameters.AddWithValue("Keyword", pKeyWord)
                cmd.Parameters.AddWithValue("PRTypeCd", pPRType)
                cmd.Parameters.AddWithValue("TypeTrx", pTypeTrx)
                cmd.Parameters.AddWithValue("PRNo", pPRNo)
                cmd.Parameters.AddWithValue("ShowMore", pShow)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetList(pDateFrom As String, pDateTo As String, pPRType As String, pDepartment As String, _
                                   pSection As String, Optional ByRef pStatus As String = "", Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRList_Sel"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRDateFrom", pDateFrom)
                cmd.Parameters.AddWithValue("PRDateTo", pDateTo)
                cmd.Parameters.AddWithValue("PRType", pPRType)
                cmd.Parameters.AddWithValue("Department", pDepartment)
                cmd.Parameters.AddWithValue("Section", pSection)
                cmd.Parameters.AddWithValue("Status", pStatus)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function uf_ModifyMaterial(pPRNo As String, pPRType As String) As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRListItem_Modif"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pPRNo)
                cmd.Parameters.AddWithValue("PRType", pPRType)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataPR(pObj As IAMI_EPIC2.ClsPRList, pPRNo As String, pUserID As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "sp_PRListDetail_Sel"
                sql = "sp_PRDetail_Sel"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pPRNo)
                'cmd.Parameters.AddWithValue("UserID", pUserID)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataPRDetail(pObj As IAMI_EPIC2.ClsPRList, pPRNo As String, pUserID As String, pRev As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRListDetail_Sel2"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pPRNo)
                cmd.Parameters.AddWithValue("UserID", pUserID)
                cmd.Parameters.AddWithValue("Rev", pRev)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    '20191211
    Public Shared Sub PRHeader_Insert(ByVal PRList As ClsPRList, pUser As String, pType As String, Optional ByRef PRNumberOutput As String = "", Optional ByRef pErr As String = "")
        Dim sql As String
        Dim ds As New DataSet

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRHeader_Ins"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Type", pType)

                cmd.Parameters.AddWithValue("PRNo", PRList.PRNumber)
                cmd.Parameters.AddWithValue("PRDate", PRList.PRDate)
                cmd.Parameters.AddWithValue("Budget", PRList.PRBudget)
                cmd.Parameters.AddWithValue("PRType", PRList.PRType)
                cmd.Parameters.AddWithValue("Department", PRList.Department)
                cmd.Parameters.AddWithValue("Section", PRList.Section)
                cmd.Parameters.AddWithValue("CostCenter", PRList.CostCenter)
                cmd.Parameters.AddWithValue("Project", PRList.Project)
                cmd.Parameters.AddWithValue("UrgentStatus", PRList.Urgent)
                cmd.Parameters.AddWithValue("UrgentNote", PRList.UrgentNote)
                cmd.Parameters.AddWithValue("PODate", PRList.ReqPOIssueDate)
                cmd.Parameters.AddWithValue("PRStatus", PRList.StatusCls)
                cmd.Parameters.AddWithValue("Rev", "1") 'not used
                cmd.Parameters.AddWithValue("User", pUser)


                Dim outPutParameter As SqlParameter = New SqlParameter()
                outPutParameter.ParameterName = "PRNo_Output"
                outPutParameter.SqlDbType = System.Data.SqlDbType.VarChar
                outPutParameter.Direction = System.Data.ParameterDirection.Output
                outPutParameter.Size = 50
                cmd.Parameters.Add(outPutParameter)

                'con.Open()
                cmd.ExecuteNonQuery()
                PRNumberOutput = outPutParameter.Value.ToString()
                cmd.Dispose()

            End Using

        Catch ex As Exception
            pErr = ex.Message
            ' Return Nothing
        End Try
    End Sub

    Public Shared Function PRDetail_InsUpd(ByVal PRList As ClsPRList, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRDetail_InsUpd"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", PRList.PRNumber)
                cmd.Parameters.AddWithValue("MaterialNo", PRList.MaterialNo)
                cmd.Parameters.AddWithValue("Qty", PRList.Qty)
                cmd.Parameters.AddWithValue("Remarks", PRList.Remarks)
                cmd.Parameters.AddWithValue("Rev", "0")
                cmd.Parameters.AddWithValue("User", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function LoadNewItem(ByVal pPRNo As String, ByVal pMaterial As String, Optional ByVal pDraft As String = "") As DataSet
        Try
            Dim sql As String
            Dim MatArray() As String = Split(pMaterial, ",")
            Dim MatList As New List(Of String)
            Dim MaterialNo As String = ""
            Dim kutip As String = "'"
            Dim koma As String = ","

            For i = 0 To UBound(MatArray)
                If MaterialNo = "" Then
                    MaterialNo = kutip
                Else
                    MaterialNo = MaterialNo + koma + kutip
                End If
                MaterialNo = MaterialNo + MatArray(i) + kutip
            Next

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "SELECT ROW_NUMBER() OVER(ORDER BY Material_No) No,Material_No,Description," & vbCrLf & _
                '    "Specification,Qty,B.Par_Description UOM,Remarks = ''" & vbCrLf & _
                '    "FROM dbo.Mst_Item A" & vbCrLf & _
                '    "LEFT JOIN (SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'UOM') B ON A.UOM = B.Par_Code" & vbCrLf & _
                '    "WHERE Material_No IN (" & MaterialNo & ")"

                sql = " SELECT ROW_NUMBER() OVER(ORDER BY PR.Material_No) No,* FROM " & vbCrLf & _
                    " ( " & vbCrLf & _
                    " 	SELECT A.Material_No,B.Description,B.Specification,ISNULL(A.Qty,1) Qty,C.Par_Description UOM,Remarks " & vbCrLf & _
                    " 	FROM dbo.PR_Detail A " & vbCrLf & _
                    " 	LEFT JOIN dbo.Mst_Item B ON A.Material_No = B.Material_No  " & vbCrLf & _
                    " 	LEFT JOIN (SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'UOM') C ON B.UOM = C.Par_Code " & vbCrLf & _
                    " 	WHERE A.PR_Number = '" & pPRNo & "' AND A.Material_No IN (" & MaterialNo & ")  " & vbCrLf & _
                    "   AND Rev = (SELECT Rev FROM dbo.PR_Header WHERE PR_Number = '" & pPRNo & "' AND PR_Status = '0')" & vbCrLf & _
                    " 	UNION " & vbCrLf & _
                    " 	SELECT Material_No,Description, " & vbCrLf & _
                    " 	Specification,ISNULL(Qty,1) Qty,B.Par_Description UOM,Remarks = '' " & vbCrLf & _
                    " 	FROM dbo.Mst_Item A " & vbCrLf

                sql = sql + " 	LEFT JOIN (SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'UOM') B ON A.UOM = B.Par_Code " & vbCrLf & _
                            " 	WHERE Material_No IN (" & MaterialNo & ") " & vbCrLf & _
                            "   AND A.Material_No NOT IN ( " & vbCrLf & _
                            " 		SELECT A.Material_No  " & vbCrLf & _
                            "  		FROM dbo.PR_Detail A  " & vbCrLf & _
                            "  		LEFT JOIN dbo.Mst_Item B ON A.Material_No = B.Material_No   " & vbCrLf & _
                            "  		LEFT JOIN (SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'UOM') C ON B.UOM = C.Par_Code  " & vbCrLf & _
                            "  		WHERE A.PR_Number = '" & pPRNo & "' " & vbCrLf & _
                            "       AND Rev = (SELECT Rev FROM dbo.PR_Header WHERE PR_Number = '" & pPRNo & "' AND PR_Status = '0'))" & vbCrLf & _
                            " ) PR "

                If pDraft <> "" Then
                    'sql = "SELECT ROW_NUMBER() OVER(ORDER BY Material_No) No,Material_No,Description," & vbCrLf & _
                    '"Specification,Qty,B.Par_Description UOM,Remarks = ''" & vbCrLf & _
                    '"FROM dbo.Mst_Item A" & vbCrLf & _
                    '"LEFT JOIN (SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'UOM') B ON A.UOM = B.Par_Code" & vbCrLf & _
                    '"WHERE Material_No IN (" & MaterialNo & ") AND Material_No NOT IN (" & pDraft & ")"

                    sql = " SELECT ROW_NUMBER() OVER(ORDER BY PR.Material_No) No,* FROM " & vbCrLf & _
                    " ( " & vbCrLf & _
                    " 	SELECT A.Material_No,B.Description,B.Specification,ISNULL(A.Qty,1) Qty,C.Par_Description UOM,Remarks " & vbCrLf & _
                    " 	FROM dbo.PR_Detail A " & vbCrLf & _
                    " 	LEFT JOIN dbo.Mst_Item B ON A.Material_No = B.Material_No  " & vbCrLf & _
                    " 	LEFT JOIN (SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'UOM') C ON B.UOM = C.Par_Code " & vbCrLf & _
                    " 	WHERE A.PR_Number = '" & gs_GeneratePRNo & "' AND A.Material_No IN (" & MaterialNo & ")  " & vbCrLf & _
                    " 	UNION " & vbCrLf & _
                    " 	SELECT Material_No,Description, " & vbCrLf & _
                    " 	Specification,ISNULL(Qty,1) Qty,B.Par_Description UOM,Remarks = '' " & vbCrLf & _
                    " 	FROM dbo.Mst_Item A " & vbCrLf

                    sql = sql + " 	LEFT JOIN (SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'UOM') B ON A.UOM = B.Par_Code " & vbCrLf & _
                                " 	WHERE Material_No IN (" & MaterialNo & ") " & vbCrLf & _
                                "   AND A.Material_No NOT IN ( " & vbCrLf & _
                                " 		SELECT A.Material_No  " & vbCrLf & _
                                "  		FROM dbo.PR_Detail A  " & vbCrLf & _
                                "  		LEFT JOIN dbo.Mst_Item B ON A.Material_No = B.Material_No   " & vbCrLf & _
                                "  		LEFT JOIN (SELECT * FROM dbo.Mst_Parameter WHERE Par_Group = 'UOM') C ON B.UOM = C.Par_Code  " & vbCrLf & _
                                "  		WHERE A.PR_Number = '" & gs_GeneratePRNo & "') " & vbCrLf & _
                                " AND Material_No NOT IN (" & pDraft & ")" & vbCrLf & _
                                " ) PR "
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    'unused
    Public Shared Function GetNoUrut(ByVal pDepartment As String, ByVal pPRDateM As String, ByVal pPRDateY As String, Optional ByVal pUrut As String = "") As String
        Try
            Dim sql As String

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_GetNoUrut_PR"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Department", pDepartment)
                cmd.Parameters.AddWithValue("PRMonth", pPRDateM)
                cmd.Parameters.AddWithValue("PRYear", pPRDateY)
                cmd.Parameters.AddWithValue("UrutDraft", pUrut)

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Return Trim(dt.Rows(0).Item("NoU"))
            End Using

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Sub PRSubmit(ByVal PRNo As String, ByVal PRDate As String, pUser As String, pRev As String, Optional ByRef pNewPO As String = "", Optional ByRef pErr As String = "")
        Dim sql As String
        Dim ds As New DataSet

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PR_Submit"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", PRNo)
                cmd.Parameters.AddWithValue("PRDate", PRDate)
                cmd.Parameters.AddWithValue("User", pUser)
                'cmd.Parameters.AddWithValue("NewPRNo", pNewPO)
                cmd.Parameters.AddWithValue("Rev", pRev)

                Dim outPutParameter As SqlParameter = New SqlParameter()
                outPutParameter.ParameterName = "NewPRNo"
                outPutParameter.SqlDbType = System.Data.SqlDbType.VarChar
                outPutParameter.Direction = System.Data.ParameterDirection.Output
                outPutParameter.Size = 50
                cmd.Parameters.Add(outPutParameter)

                'con.Open()
                cmd.ExecuteNonQuery()
                pNewPO = outPutParameter.Value.ToString()
                cmd.Dispose()
            End Using

        Catch ex As Exception
            pErr = ex.Message
        End Try
    End Sub

    Public Shared Function uf_PRModif_Upd(ByVal PRNo As String, ByVal MatNo As String) As Integer
        Dim sql As String
        Dim i As Integer
        Dim MatArray() As String = Split(MatNo, ",")
        Dim MatList As New List(Of String)
        Dim MaterialNo As String = ""
        Dim kutip As String = "'"
        Dim koma As String = ","

        For i = 0 To UBound(MatArray)
            If MaterialNo = "" Then
                MaterialNo = kutip
            Else
                MaterialNo = MaterialNo + koma + kutip
            End If
            MaterialNo = MaterialNo + MatArray(i) + kutip
        Next

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "DELETE dbo.PR_Detail WHERE PR_Number = '" & PRNo & "' AND Material_No NOT IN (" & MaterialNo & ")" & vbCrLf & _
                      "AND Rev = (SELECT Rev FROM dbo.PR_Header WHERE PR_Number = '" & PRNo & "' AND PR_Status = '0')"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataCategory(pParentCode As String, pGroupCode As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "SELECT 'ALL' Code,'ALL' Description,1 No " & vbCrLf & _
                      "UNION ALL " & vbCrLf & _
                      "Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description,2 No " & vbCrLf & _
                      "From Mst_Parameter Where Par_Group = '" & pGroupCode & "' " & vbCrLf

                If pParentCode = "ALL" Then
                    sql = sql + "And Par_ParentCode IN (SELECT Par_Code FROM dbo.Mst_Parameter WHERE Par_Group = 'GroupItem' AND Par_ParentCode = '" & pErr & "')" & vbCrLf
                Else
                    sql = sql + "And Par_ParentCode = '" & pParentCode & "'" & vbCrLf
                End If

                sql = sql + "ORDER BY No,Code "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function PRDelete(ByVal PRNo As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PR_Delete"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", PRNo)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetRev(pPRNo As String) As Integer
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "EXEC sp_PRListDetail_GetLastRev  '" & pPRNo & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("Rev")
            Else
                Return "0"
            End If
            
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetRev_Submit(pPRNo As String) As Integer
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "SELECT Rev FROM dbo.PR_Header WHERE PR_Status = 1 AND PR_Number = '" & pPRNo & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using

            Return ds.Tables(0).Rows(0)("Rev")

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetStatusUser(pUserID As String, Optional ByRef pErr As String = "") As String
        Dim dt As New DataTable
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "select AdminStatus=case when UserType IN (1,3) THEN 1 ELSE 0 END from UserSetup WHERE UserID = '" & pUserID & "'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)

                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0).Item("AdminStatus")
                Else
                    Return ""
                End If

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillCombo(pUserID As String, pAdminStatus As String, _
                                     Optional ByRef pGroup As String = "", _
                                     Optional ByRef pParent As String = "", _
                                     Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pAdminStatus = "1" Then
                    sql = "SELECT 'ALL' Code,'ALL' Description " & vbCrLf & _
                            "UNION ALL " & vbCrLf & _
                            "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description  " & vbCrLf & _
                            "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf

                    If pParent <> "ALL" And pParent <> "" Then
                        sql = sql + "AND COALESCE(Par_ParentCode,'') = '" & pParent & "'"
                    End If

                Else
                    sql = sql + "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description " & vbCrLf & _
                        "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf & _
                        "AND Par_Code = (SELECT COALESCE(" & pGroup & "_Code,'')" & pGroup & "_Code FROM dbo.UserSetup WHERE UserID = '" & pUserID & "')"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboSection(pUserID As String, pAdminStatus As String, _
                                    Optional ByRef pGroup As String = "", _
                                    Optional ByRef pParent As String = "", _
                                    Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()


                sql = "SELECT 'ALL' Code,'ALL' Description " & vbCrLf & _
                      "UNION ALL " & vbCrLf & _
                      "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description  " & vbCrLf & _
                      "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf

                If pParent <> "ALL" And pParent <> "" Then
                    sql = sql + "AND COALESCE(Par_ParentCode,'') = '" & pParent & "'"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function FillComboDetail(pUserID As String, pAdminStatus As String, _
                                     Optional ByRef pGroup As String = "", _
                                     Optional ByRef pParent As String = "", _
                                     Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pAdminStatus = "1" Then
                    If pGroup <> "CostCenter" Then
                        sql = "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description  " & vbCrLf & _
                                "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf

                        If pParent <> "ALL" And pParent <> "" Then
                            sql = sql + "AND COALESCE(Par_ParentCode,'') = '" & pParent & "'"
                        End If
                    Else
                        sql = "SELECT RTRIM(par.Par_Code) As Code, RTRIM(x.Par_Description) As Description  " & vbCrLf & _
                              "FROM Mst_Parameter par " & vbCrLf & _
                              "LEFT JOIN Mst_Parameter x on x.Par_Code=par.Par_ParentCode " & vbCrLf & _
                              "Where par.Par_Group = '" & pGroup & "' " & vbCrLf

                        If pParent <> "ALL" And pParent <> "" Then
                            sql = sql + "AND COALESCE(par.Par_ParentCode,'') = '" & pParent & "'"
                        End If
                    End If



                Else
                    If pGroup <> "CostCenter" Then
                        sql = sql + "SELECT RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description " & vbCrLf & _
                            "FROM Mst_Parameter Where Par_Group = '" & pGroup & "' " & vbCrLf & _
                            "AND Par_Code = (SELECT COALESCE(" & pGroup & "_Code,'')" & pGroup & "_Code FROM dbo.UserSetup WHERE UserID = '" & pUserID & "')"
                    Else
                        sql = sql + "SELECT RTRIM(par.Par_Code) As Code, RTRIM(x.Par_Description) As Description " & vbCrLf & _
                            "FROM Mst_Parameter par " & vbCrLf & _
                            "left join Mst_parameter x on x.Par_Code=par.Par_ParentCode " & vbCrLf & _
                            "Where par.Par_Group = '" & pGroup & "' " & vbCrLf & _
                            "AND par.Par_ParentCode = (SELECT COALESCE(Department_Code,'')Department_Code FROM dbo.UserSetup WHERE UserID = '" & pUserID & "')"
                    End If
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetHeaderPRList(pDateFrom As String, pDateTo As String, pPRType As String, _
                                           pDepartment As String, pSection As String, pUser As String, _
                                           pStatus As String, Optional ByRef pErr As String = "") As DataSet

        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "sp_PRListHeader_Sel"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRDateFrom", pDateFrom)
                cmd.Parameters.AddWithValue("PRDateTo", pDateTo)
                cmd.Parameters.AddWithValue("PRType", pPRType)
                cmd.Parameters.AddWithValue("Department", pDepartment)
                cmd.Parameters.AddWithValue("Section", pSection)
                cmd.Parameters.AddWithValue("Status", pStatus)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    '15/04/2019
    Public Shared Function GetListItem_CheckExist(pGroupItem As String, pCategory As String, pLastSupplier As String, pKeyWord As String, pPRType As String, ByVal pTypeTrx As Integer, ByVal pPRNo As String, ByVal pMaterialNo As String, Optional ByRef pShow As Integer = 0, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRList_GetListItem_checkExist"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("GroupItem", pGroupItem)
                cmd.Parameters.AddWithValue("Category", pCategory)
                cmd.Parameters.AddWithValue("LastSupplier", pLastSupplier)
                cmd.Parameters.AddWithValue("Keyword", pKeyWord)
                cmd.Parameters.AddWithValue("PRTypeCd", pPRType)
                cmd.Parameters.AddWithValue("TypeTrx", pTypeTrx)
                cmd.Parameters.AddWithValue("PRNo", pPRNo)
                cmd.Parameters.AddWithValue("MaterialNo", pMaterialNo)
                cmd.Parameters.AddWithValue("ShowMore", pShow)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    '16/04/2019 
    Public Shared Function ShowDetailItems(ByVal pMaterialNo As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRList_ShowDetailItems"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("MaterialNo", pMaterialNo)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

﻿Imports System.Data.SqlClient

Public Class ClsPeriodAdjustmentDB
    Public Shared Function GeneratePeriodID(ByVal pPeriodType As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                Dim sql As String = ""
                sql = "sp_PeriodAdjustment_GenerateID"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PeriodType", pPeriodType)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataItem(ByVal pObj As ClsPeriodAdjustment, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                Dim sql As String = ""
                sql = "sp_PeriodAdjustment_GetItem "

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PeriodID", pObj.PeriodID)
                cmd.CommandTimeout = 1200
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataList(Optional ByRef pErr As String = "") As List(Of ClsPeriodAdjustment)
        Dim sql As String
        Dim cmd As SqlCommand

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PeriodAdjustment_GetList"
                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 1200
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)

                Dim list As New List(Of ClsPeriodAdjustment)
                For i = 0 To dt.Rows.Count - 1
                    Dim itemdata As New ClsPeriodAdjustment With {.PeriodID = dt.Rows(i).Item("PeriodID"),
                                                                  .PeriodDescription = Trim(dt.Rows(i).Item("PeriodDescription") & ""),
                                                                  .PeriodType = Trim(dt.Rows(i).Item("PeriodType") & ""),
                                                                  .PeriodFrom = IIf(IsDBNull(dt.Rows(i).Item("PeriodFrom")), 0, dt.Rows(i).Item("PeriodFrom")),
                                                                  .PeriodTo = IIf(IsDBNull(dt.Rows(i).Item("PeriodTo")), 0, dt.Rows(i).Item("PeriodTo")),
                                                                  .PODateFrom = IIf(IsDBNull(dt.Rows(i).Item("PODateFrom")), 0, dt.Rows(i).Item("PODateFrom")),
                                                                  .PODateTo = IIf(IsDBNull(dt.Rows(i).Item("PODateTo")), 0, dt.Rows(i).Item("PODateTo")),
                                                                  .RegisterBy = IIf(IsDBNull(dt.Rows(i).Item("RegisterBy")), "", dt.Rows(i).Item("RegisterBy")),
                                                                  .RegisterDate = IIf(IsDBNull(dt.Rows(i).Item("RegisterDate")), "", dt.Rows(i).Item("RegisterDate")),
                                                                  .UpdateBy = IIf(IsDBNull(dt.Rows(i).Item("UpdateBy")), "", dt.Rows(i).Item("UpdateBy")),
                                                                  .UpdateDate = IIf(IsDBNull(dt.Rows(i).Item("UpdateDate")), "", dt.Rows(i).Item("UpdateDate"))}

                    list.Add(itemdata)
                Next

                Return list
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pObj As ClsPeriodAdjustment, pUser As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                Dim sql As String = ""
                sql = "sp_PeriodAdjustment_Ins"

                Dim cmd As SqlCommand
                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 1200
                cmd.Parameters.AddWithValue("PeriodID", pObj.PeriodID)
                cmd.Parameters.AddWithValue("PeriodDescription", pObj.PeriodDescription)
                cmd.Parameters.AddWithValue("PeriodType", pObj.PeriodType)
                cmd.Parameters.AddWithValue("PeriodFrom", pObj.PeriodFrom)
                cmd.Parameters.AddWithValue("PeriodTo", pObj.PeriodTo)
                cmd.Parameters.AddWithValue("PODateFrom", pObj.PODateFrom)
                cmd.Parameters.AddWithValue("PODateTo", pObj.PODateTo)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim rtn As Integer = cmd.ExecuteNonQuery
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Update(ByVal pObj As ClsPeriodAdjustment, pUser As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                Dim sql As String = ""
                sql = "sp_PeriodAdjustment_Upd"

                Dim cmd As SqlCommand
                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 1200
                cmd.Parameters.AddWithValue("PeriodID", pObj.PeriodID)
                cmd.Parameters.AddWithValue("PeriodDescription", pObj.PeriodDescription)
                cmd.Parameters.AddWithValue("PeriodType", pObj.PeriodType)
                cmd.Parameters.AddWithValue("PeriodFrom", pObj.PeriodFrom)
                cmd.Parameters.AddWithValue("PeriodTo", pObj.PeriodTo)
                cmd.Parameters.AddWithValue("PODateFrom", pObj.PODateFrom)
                cmd.Parameters.AddWithValue("PODateTo", pObj.PODateTo)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim rtn As Integer = cmd.ExecuteNonQuery
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Delete(ByVal pObj As ClsPeriodAdjustment, Optional ByRef pErr As String = "") As Integer
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                Dim sql As String = ""
                sql = "sp_PeriodAdjustment_Del"

                Dim cmd As SqlCommand
                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 1200
                cmd.Parameters.AddWithValue("PeriodID", pObj.PeriodID)

                Dim rtn As Integer = cmd.ExecuteNonQuery
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function isExist(ByVal pPeriodID As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_PeriodAdjustment_Exists"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 1200
                cmd.Parameters.AddWithValue("PeriodID", pPeriodID)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count <> 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

﻿Public Class clsQCDMR
    Public Property seqnogroup As String
    Public Property ID As String
    Public Property Aspect As String
    Public Property Item As String
    Public Property VendorCode1 As String
    Public Property VendorCode2 As String
    Public Property VendorCode3 As String
    Public Property VendorCode4 As String
    Public Property VendorCode5 As String
    Public Property VendorCode6 As String
    Public Property pValue1 As Decimal
    Public Property pValue2 As Decimal
    Public Property pValue3 As Decimal
    Public Property pValue4 As Decimal
    Public Property pValue5 As Decimal
    Public Property pValue6 As Decimal
    Public Property pValueSubTotal1 As Decimal
    Public Property pValueSubTotal2 As Decimal
    Public Property pValueSubTotal3 As Decimal
    Public Property pValueSubTotal4 As Decimal
    Public Property pValueSubTotal5 As Decimal
    Public Property pValueSubTotal6 As Decimal
    Public Property SupplierName As String
    Public Property Project_ID As String
    Public Property Group As String
    Public Property Comodity As String
    Public Property ComodityGroup As String
    Public Property PartNo As String
    Public Property UserID As String

End Class

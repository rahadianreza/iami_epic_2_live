﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 18 Oct 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################

Public Class clsRFQUpdate
    Public Property RFQSetNumber As String
    Public Property RFQNumber As String
    Public Property RFQDate As String
    Public Property RFQTitle As String
    Public Property PRNumber As String

    Public Property PRDate As String
    Public Property Supplier As String
    Public Property Deadline As String
    Public Property Department As String
    Public Property Rev As Integer

    Public Property Currency As String
    Public Property Section As String
    Public Property PRType As String
    Public Property Project As String
    Public Property Urgent As String
    Public Property Tender As String

    Public Property MaterialNo As String
    Public Property Qty As Integer

End Class

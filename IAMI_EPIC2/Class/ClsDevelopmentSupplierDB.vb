﻿Imports System.Data.SqlClient

Public Class ClsDevelopmentSupplierDB

    'DEVELOPMENT SUPPLIER
#Region "Development part"
    Public Shared Function getListDevelopmentSupplier(Project As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_DevelopmentSupplier_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project", Project)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getListDevelopmentSupplierDetail(Project As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_DevelopmentSupplierDetail_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project", Project)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DevelopmentSupplierDetail_Insert(ProjectYear As String, ProjectID As String, PartNo As String, Model As String, _
                                                            Supplier1 As String, Supplier2 As String, Supplier3 As String, Supplier4 As String, _
                                                            Supplier5 As String, Supplier6 As String, _
                                                            pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_DevelopmentSupplierDetail_Insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectYear", ProjectYear)
                cmd.Parameters.AddWithValue("@ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("@PartNo", PartNo)
                cmd.Parameters.AddWithValue("@Model", Model)
                cmd.Parameters.AddWithValue("@Supplier1", IIf(String.IsNullOrEmpty(Supplier1), DBNull.Value, Supplier1))
                cmd.Parameters.AddWithValue("@Supplier2", IIf(String.IsNullOrEmpty(Supplier2), DBNull.Value, Supplier2))
                cmd.Parameters.AddWithValue("@Supplier3", IIf(String.IsNullOrEmpty(Supplier3), DBNull.Value, Supplier3))
                cmd.Parameters.AddWithValue("@Supplier4", IIf(String.IsNullOrEmpty(Supplier4), DBNull.Value, Supplier4))
                cmd.Parameters.AddWithValue("@Supplier5", IIf(String.IsNullOrEmpty(Supplier5), DBNull.Value, Supplier5))
                cmd.Parameters.AddWithValue("@Supplier6", IIf(String.IsNullOrEmpty(Supplier6), DBNull.Value, Supplier6))
                cmd.Parameters.AddWithValue("@UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Public Shared Function DevelopmentSupplierDetail_SubmitProcess(Project As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_DevelopmentSupplier_SubmitStatus", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", Project)
                cmd.Parameters.AddWithValue("@UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Public Shared Function DevelopmentSupplierDetail_bindForm(ProjectYear As String, ProjectID As String, PartNo As String, Model As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_DevelopmentSupplierDetail_BindForm", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectYear", ProjectYear)
                cmd.Parameters.AddWithValue("@ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("@PartNo", PartNo)
                cmd.Parameters.AddWithValue("@Model", Model)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function checkStatusSubmit(Project As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_DevelopmentSupplierDetail_CheckStatus", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", Project)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataSupplier(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try

            Dim sQuery As String = ""
            sQuery = "Select Supplier_Code As Code, Supplier_Name As Description From Mst_Supplier"

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sQuery, con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

#End Region


End Class

﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 03 Oct 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################

Imports System.Data.SqlClient

Public Class clsPRAcceptanceDB
    Public Shared Function GetList(pDateFrom As String, pDateTo As String, pPRType As String, pDepartment As String, pSection As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRAcceptance_List"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRDateFrom", pDateFrom)
                cmd.Parameters.AddWithValue("PRDateTo", pDateTo)
                cmd.Parameters.AddWithValue("PRType", pPRType)
                cmd.Parameters.AddWithValue("Department", pDepartment)
                cmd.Parameters.AddWithValue("Section", pSection)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataPR(pObj As IAMI_EPIC2.clsPRAcceptance, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = " Select ROW_NUMBER() OVER(ORDER BY PD.Material_No) As No, PH.*, PD.Material_No, PD.Qty, PD.Remarks, IM.Description, IM.Specification, UOMDescription   " & vbCrLf & _
                '        " From (Select * From PR_Header Where ISNULL(PR_Status,'0') = '2') PH Inner Join PR_Detail PD On PH.PR_Number = PD.PR_Number And PH.Rev = PD.Rev " & vbCrLf & _
                '        " Left Join Mst_Item IM On IM.Material_No = PD.Material_No " & vbCrLf & _
                '        " Left Join " & vbCrLf & _
                '        " (Select Rtrim(Par_Code) Code, Rtrim(Par_Description) UOMDescription  From Mst_Parameter Where Par_Group = 'UOM') UO " & vbCrLf & _
                '        " On UO.Code = IM.UOM " & vbCrLf & _
                '        " Where PH.PR_Number = @PRNo And PH.Rev = @Revision "

                sql = "sp_PRAcceptance_GetDataPR"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pObj.PRNumber)
                cmd.Parameters.AddWithValue("Revision", pObj.Revision)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()

                cmd.Dispose()
            End Using

            Return ds
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataHeader(ByVal pObj As clsPRAcceptance, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = " Select * From PRHeader " & vbCrLf & _
                      " Where PH.PRNo = @PRNo "


                Dim cmd As New SqlCommand(sql, con)
                cmd.Parameters.AddWithValue("@PRNo", pObj.PRNumber)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()

                cmd.Dispose()
            End Using

            Return ds
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataDetail(ByVal pObj As clsPRAcceptance, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = " Select PH.*, PD.ItemCode, PD.Qty, PD.Remarks, IM.Description  " & vbCrLf & _
                        " From PRHeader PH Inner Join PRDetail PD On PH.PRNo = PD.PRNo " & vbCrLf & _
                        " Left Join Mst_Item IM On IM.Material_No = PD.Material_No " & vbCrLf & _
                        " Where PH.PRNo = @PRNo "

                Dim cmd As New SqlCommand(sql, con)
                cmd.Parameters.AddWithValue("@PRNo", pObj.PRNumber)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()

                cmd.Dispose()
            End Using

            Return ds
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboData(pCriteria As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = '" & pCriteria & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function ApprovePRApproval(ByVal pPRAcceptance As clsPRAcceptance, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Update PR_Approval Set Approval_Status = 1 " & vbCrLf & _
                      "Where PR_Number = @PRNumber And Approval_Person = @AppPerson"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("PRNumber", pPRAcceptance.PRNumber)
                cmd.Parameters.AddWithValue("AppPerson", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateStatusPR(ByVal pPRAcceptance As clsPRAcceptance, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Update PR_Header Set PR_Status = 2 " & vbCrLf & _
                      "Where PR_Number = @PRNumber" & vbCrLf & _
                      ""

                sql = sql + "Update PR_Approval Set Approval_Status = NULL, Approval_Date = NULL " & vbCrLf & _
                            "Where PR_Number = @PRNumber And Approval_Person = '@UserId " & vbCrLf & _
                            ""

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("PRNumber", pPRAcceptance.PRNumber)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function ApproveFull(ByVal pPRAcceptance As clsPRAcceptance, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_PRAcceptance_Approve]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pPRAcceptance.PRNumber)
                cmd.Parameters.AddWithValue("AcceptanceNote", pPRAcceptance.AcceptanceNote)
                cmd.Parameters.AddWithValue("UrgentStatus", pPRAcceptance.UrgentCls)
                cmd.Parameters.AddWithValue("ReqPOIssueDate", pPRAcceptance.ReqPOIssueDate)
                cmd.Parameters.AddWithValue("UrgentNote", pPRAcceptance.UrgentNote)
                cmd.Parameters.AddWithValue("Rev", pPRAcceptance.Revision)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Approve(ByVal pPRAcceptance As clsPRAcceptance, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Update PR_Header Set PR_Status = 3 "

                If pPRAcceptance.UrgentCls = "Yes" Then
                    sql = sql & ", Req_POIssueDate = @ReqPOIssueDate ,Urgent_Status = @UrgentStatus, Urgent_Note = @UrgentNote, Acceptance_Note = @AcceptanceNote  "
                End If

                sql = sql & "Where PR_Number = @PRNumber"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("PRNumber", pPRAcceptance.PRNumber)
                cmd.Parameters.AddWithValue("AcceptanceNote", pPRAcceptance.AcceptanceNote)

                If pPRAcceptance.UrgentCls = "Yes" Then
                    cmd.Parameters.AddWithValue("UrgentStatus", "Yes")
                    cmd.Parameters.AddWithValue("ReqPOIssueDate", pPRAcceptance.ReqPOIssueDate)
                    cmd.Parameters.AddWithValue("UrgentNote", pPRAcceptance.UrgentNote)
                Else
                    cmd.Parameters.AddWithValue("UrgentStatus", "No")
                    'cmd.Parameters.AddWithValue("ReqPOIssueDate", pPRAcceptance.ReqPOIssueDate)
                    'cmd.Parameters.AddWithValue("UrgentNote", pPRAcceptance.UrgentNote)
                End If

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Reject(ByVal pPRAcceptance As clsPRAcceptance, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "Update PR_Header Set PR_Status = 2 Where PR_Number = @PRNumber"
                sql = "sp_PRAcceptance_Reject"
                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pPRAcceptance.PRNumber)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("RejectNote", pPRAcceptance.AcceptanceNote)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataForExcel(pDateFrom As String, pDateTo As String, pPRType As String, pApproval As String, pDepartment As String, pSection As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String

                sql = " Select PH.*, Isnull(Dept_ApprovedStatus,'0') As DeptStatus, Isnull(GM_ApprovedStatus,'0') As ManagerStatus, Isnull(Acceptance_ApprovedStatus,'0') As AcceptanceStatus,  DepartmentName, SectionName, " & vbCrLf & _
                                            " REPLACE(ISNULL(CONVERT(DATE, Dept_ApprovedDate), ''), '1900-01-01', '') DeptDate, " & vbCrLf & _
                                            " REPLACE(ISNULL(CONVERT(DATE, GM_ApprovedDate), ''), '1900-01-01', '') ManagerDate, " & vbCrLf & _
                                            " REPLACE(ISNULL(CONVERT(DATE, Acceptance_ApprovedDate), ''), '1900-01-01', '') AcceptanceDate, " & vbCrLf & _
                                            " PRBudgetName, PRTypeName, Urgent_Status " & vbCrLf & _
                                            " From PR_Header PH  " & vbCrLf & _
                                            " Left Join  " & vbCrLf & _
                                            " (Select Par_Code, Par_Description As DepartmentName From Mst_Parameter Where Par_Group = 'Department') DG " & vbCrLf & _
                                            " On PH.Department_Code = DG.Par_Code " & vbCrLf & _
                                            " Left Join  " & vbCrLf & _
                                            " (Select Par_Code, Par_Description As SectionName From Mst_Parameter Where Par_Group = 'Section') SC " & vbCrLf & _
                                            " On PH.Section_Code = SC.Par_Code " & vbCrLf & _
                                            " Left Join  " & vbCrLf & _
                                            " (Select Par_Code, Par_Description As PRBudgetName From Mst_Parameter Where Par_Group = 'PRBudget') PB " & vbCrLf & _
                                            " On PH.Budget_Code = PB.Par_Code " & vbCrLf & _
                                            " Left Join  " & vbCrLf & _
                                            " (Select Par_Code, Par_Description As PRTypeName From Mst_Parameter Where Par_Group = 'PRType') PT " & vbCrLf & _
                                            " On PH.PRType_Code = PT.Par_Code " & vbCrLf & _
                                            " Where PH.PR_Date >= '" & pDateFrom & "' And PH.PR_Date <= '" & pDateTo & "' "

                If pPRType <> "" Then
                    sql = sql + " And PRType_Code = '" & pPRType & "'"
                End If

                If pDepartment <> "" Then
                    sql = sql + " And PH.Department_Code = '" & pDepartment & "'"
                End If

                If pSection <> "" Then
                    sql = sql + " And PH.Section_Code = '" & pSection & "'"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 03 Oct 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################


Public Class clsPRAcceptance
    Public Property RequestDateFrom As String
    Public Property RequestDateTo As String
    Public Property PRType As String
    Public Property ApprovalStatus As String
    Public Property Department As String
    Public Property Section As String
    Public Property Revision As Integer
    Public Property PRNumber As String
    Public Property PRDate As String
    Public Property PRBudget As String
    Public Property Project As String
    Public Property UrgentCls As String
    Public Property UrgentNote As String
    Public Property ApprovalNote As String
    Public Property ReqPOIssueDate As String

    Public Property DeptHeadAppName As String
    Public Property DeptHeadAppStatus As String
    Public Property DeptHeadAppDate As String

    Public Property GeneralManagerAppName As String
    Public Property GeneralManagerAppStatus As String
    Public Property GeneralManagerAppDate As String
    Public Property AcceptanceAppName As String
    Public Property AcceptanceAppStatus As String
    Public Property AcceptanceAppDate As String
    Public Property AcceptanceNote As String



End Class

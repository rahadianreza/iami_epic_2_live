﻿Imports System.Data.SqlClient
Public Class ClsMasterBudgetInflationDB
    Public Shared Function getData(ByVal pObj As ClsMasterBudgetInflation, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As DataTable
        Dim dt As New DataTable
        Dim cmd As SqlCommand
        Dim da As SqlDataAdapter
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_BudgetInflation_Sel"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)
                    da = New SqlDataAdapter(cmd)
                    da.Fill(dt)
                    da.Dispose()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                End Using
            Else

            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterBudgetInflation.getDataTable: " & ex.Message
            Return Nothing
        End Try
        Return dt
    End Function

    Public Shared Function getDataDetail(ByVal pObj As ClsMasterBudgetInflation, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As DataTable
        Dim dt As New DataTable
        Dim cmd As SqlCommand
        Dim da As SqlDataAdapter
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_BudgetInflation_SelDetail"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    da = New SqlDataAdapter(cmd)
                    da.Fill(dt)
                    da.Dispose()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                End Using
            Else

            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterBudgetInflation.getDataDetail: " & ex.Message
            Return Nothing
        End Try
        Return dt
    End Function

    Public Shared Function CheckData(ByVal pObj As ClsMasterBudgetInflation, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As DataTable
        Dim dt As New DataTable
        Dim cmd As SqlCommand
        Dim da As SqlDataAdapter
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "Select * From mst_BudgetInflation Where SupplierCode=@SupplierCode and Type=@Type"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    ' cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)

                    da = New SqlDataAdapter(cmd)
                    da.Fill(dt)
                    da.Dispose()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                End Using
            Else

            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterBudgetInflation.getDataTable: " & ex.Message
            Return Nothing
        End Try
        Return dt
    End Function

    Public Shared Function insert(ByVal pObj As ClsMasterBudgetInflation, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As Boolean
        Dim result As Boolean = False
        Dim cmd As SqlCommand
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_BudgetInflation_Ins"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)
                    cmd.Parameters.AddWithValue("@Material", CDbl(pObj.Material))
                    cmd.Parameters.AddWithValue("@Process", CDbl(pObj.Process))
                    cmd.Parameters.AddWithValue("@UserID", pObj.User)
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                    result = True
                End Using
            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterBudgetInflation.insert: " & ex.Message
            Return False
        End Try
        Return result
    End Function

    Public Shared Function Update(ByVal pObj As ClsMasterBudgetInflation, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As Boolean
        Dim result As Boolean = False
        Dim cmd As SqlCommand
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_BudgetInflation_Upd"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)
                    cmd.Parameters.AddWithValue("@Material", CDbl(pObj.Material))
                    cmd.Parameters.AddWithValue("@Process", CDbl(pObj.Process))
                    cmd.Parameters.AddWithValue("@UserID", pObj.User)
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                    result = True
                End Using
            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterBudgetInflation.Update: " & ex.Message
            Return False
        End Try
        Return result
    End Function

    Public Shared Function Delete(ByVal pObj As ClsMasterBudgetInflation, Optional ByVal pCon As SqlConnection = Nothing, Optional ByVal pTrans As SqlTransaction = Nothing, Optional ByRef pErrMsg As String = "") As Boolean
        Dim result As Boolean = False
        Dim cmd As SqlCommand
        Dim sql As String
        Try
            If pCon Is Nothing Then
                Using con = New SqlConnection(Sconn.Stringkoneksi)
                    con.Open()

                    sql = "sp_BudgetInflation_Del"

                    cmd = New SqlCommand
                    cmd.Connection = con
                    cmd.CommandText = sql
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@SupplierCode", pObj.SupplierCode)
                    cmd.Parameters.AddWithValue("@Type", pObj.Type)
                    cmd.Parameters.AddWithValue("@VariantCode", pObj.VariantCode)
                    cmd.ExecuteNonQuery()
                    cmd.Parameters.Clear()
                    cmd.Dispose()
                    result = True
                End Using
            End If
        Catch ex As Exception
            pErrMsg = "ClsMasterBudgetInflation.Delete: " & ex.Message
            Return False
        End Try
        Return result
    End Function
End Class

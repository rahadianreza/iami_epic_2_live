﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 03 Oct 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################

Imports System.Data.SqlClient

Public Class clsRFQCancellationDB

  
    Public Shared Function GetList(pDateFrom As String, pDateTo As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_RFQCancellation_GetList"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQDateFrom", pDateFrom)
                cmd.Parameters.AddWithValue("RFQDateTo", pDateTo)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataRFQ(pRFQSetNo As String, pRevision As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                ' sql = "Select	S.RFQ_Set, H.RFQ_Number, S.Rev, S.RFQ_DueDate, S.RFQ_Date, RFQ_Status, PR_Number,  H.Supplier_Code, Supplier_Name " & vbCrLf & _
                ' "From RFQ_Set S  " & vbCrLf & _
                ' "Inner Join RFQ_Header H On S.RFQ_Set = H.RFQ_Set AND S.Rev = H.Rev " & vbCrLf & _
                ' "Left Join Mst_Supplier R On R.Supplier_Code = H.Supplier_Code " & vbCrLf & _
                ' "Where S.RFQ_Set = @RFQSetNo And S.Rev = @Revision "
                sql = "sp_RFQ_GetDataRFQCancellation"


                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSetNo)
                cmd.Parameters.AddWithValue("Revision", pRevision)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataDetailRFQ(pRFQSetNo As String, pRev As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "SELECT DISTINCT '0' AllowCheck, " & vbCrLf _
                    & "       H.RFQ_Set, " & vbCrLf _
                    & "       H.RFQ_Number, " & vbCrLf _
                    & "       H.Rev, " & vbCrLf _
                    & "       H.Supplier_Code, " & vbCrLf _
                    & "       R.Supplier_Name " & vbCrLf _
                    & "FROM   RFQ_Header H " & vbCrLf _
                    & "       LEFT JOIN Mst_Supplier R " & vbCrLf _
                    & "            ON  R.Supplier_Code = H.Supplier_Code " & vbCrLf _
                    & "            LEFT JOIN RFQ_Approval AS A ON A.RFQ_Set = H.RFQ_Set AND A.Rev = H.Rev AND A.RFQ_Number = H.RFQ_Number " & vbCrLf _
                    & "WHERE ISNULL(H.RFQ_Status,'') not in ('6','7') AND NOT EXISTS(  Select * FROM Quotation_Header AS qh WHERE qh.RFQ_Set = H.RFQ_Set AND qh.RFQ_Number = H.RFQ_Number) and  H.RFQ_Set = @RFQSetNo AND H.Rev = @Revision " & vbCrLf _
                    & ""
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSetNo)
                cmd.Parameters.AddWithValue("Revision", pRev)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="pclsRFQCancellation"></param>
    ''' <param name="pUser"></param>
    ''' <param name="pErr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function void(ByVal pclsRFQCancellation As clsRFQCancellation, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_RFQVoid]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNumber", pclsRFQCancellation.RFQSetNumber)
                cmd.Parameters.AddWithValue("RFQNumber", pclsRFQCancellation.RFQNumber)
                cmd.Parameters.AddWithValue("CancelNotes", pclsRFQCancellation.ApproveNotes)
                cmd.Parameters.AddWithValue("UserId", pUser)
                'cmd.Parameters.AddWithValue("Revision", pclsRFQCancellation.Revision)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As SqlException
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Cancellation(ByVal pclsRFQCancellation As clsRFQCancellation, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_RFQCancellation]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNumber", pclsRFQCancellation.RFQSetNumber)
                cmd.Parameters.AddWithValue("RFQNumber", pclsRFQCancellation.RFQNumber)
                cmd.Parameters.AddWithValue("CancelNotes", pclsRFQCancellation.ApproveNotes)
                cmd.Parameters.AddWithValue("UserId", pUser)
                'cmd.Parameters.AddWithValue("Revision", pclsRFQCancellation.Revision)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As SqlException
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

﻿Imports System.Data.SqlClient


Public Class clsItemListSourcingDB
    Public Shared Function getHeaderGrid(_prjid As String, pUser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_ItemListSourcing_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getHeaderGridDrawing(_prjid As String, _groupid As String, _commodity As String, _pic As String, _filterDrawing As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_ItemListSourcing_Drawing_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", IIf(String.IsNullOrEmpty(_prjid), "DUMMY", _prjid))
                cmd.Parameters.AddWithValue("@Group_ID", IIf(String.IsNullOrEmpty(_groupid), DBNull.Value, _groupid))
                cmd.Parameters.AddWithValue("@Commodity", IIf(String.IsNullOrEmpty(_commodity), DBNull.Value, _commodity))
                cmd.Parameters.AddWithValue("@PIC", IIf(String.IsNullOrEmpty(_pic), DBNull.Value, _pic))
                cmd.Parameters.AddWithValue("@filterDrawing", IIf(String.IsNullOrEmpty(_filterDrawing), DBNull.Value, _filterDrawing))
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getHeaderGridDrawingAcc(_prjid As String, _groupid As String, _commodity As String, _pic As String, _filterDrawing As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_ItemListSourcing_Drawing_Acc_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", IIf(String.IsNullOrEmpty(_prjid), "DUMMY", _prjid))
                cmd.Parameters.AddWithValue("@Group_ID", IIf(String.IsNullOrEmpty(_groupid), DBNull.Value, _groupid))
                cmd.Parameters.AddWithValue("@Commodity", IIf(String.IsNullOrEmpty(_commodity), DBNull.Value, _commodity))
                cmd.Parameters.AddWithValue("@PIC", IIf(String.IsNullOrEmpty(_pic), DBNull.Value, _pic))
                cmd.Parameters.AddWithValue("@filterDrawing", IIf(String.IsNullOrEmpty(_filterDrawing), DBNull.Value, _filterDrawing))
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getHeaderGridDrawingGrouping(_prjid As String, _groupid As String, _commodity As String, _pic As String, _filterDrawing As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_ItemListSourcing_Drawing_Grouping_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", IIf(String.IsNullOrEmpty(_prjid), "DUMMY", _prjid))
                cmd.Parameters.AddWithValue("@Group_ID", IIf(String.IsNullOrEmpty(_groupid), DBNull.Value, _groupid))
                cmd.Parameters.AddWithValue("@Commodity", IIf(String.IsNullOrEmpty(_commodity), DBNull.Value, _commodity))
                cmd.Parameters.AddWithValue("@PIC", IIf(String.IsNullOrEmpty(_pic), DBNull.Value, _pic))
                cmd.Parameters.AddWithValue("@filterDrawing", IIf(String.IsNullOrEmpty(_filterDrawing), DBNull.Value, _filterDrawing))
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function insertData(_clsItemListSourcing As clsItemListSourcing, pUser As String, Optional ByRef pErr As String = "") As Integer

        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_ItemListSourcing_Ins"
                Dim listvariant As String = _clsItemListSourcing.Variant_Name
                Dim removecomma As String = listvariant.Remove(listvariant.Length - 1)

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _clsItemListSourcing.Project_ID)
                cmd.Parameters.AddWithValue("@Part_No", _clsItemListSourcing.Part_No)
                cmd.Parameters.AddWithValue("@Group_ID", _clsItemListSourcing.Group_ID)
                cmd.Parameters.AddWithValue("@Commodity", _clsItemListSourcing.Commodity)
                cmd.Parameters.AddWithValue("@Upc", _clsItemListSourcing.Upc)
                cmd.Parameters.AddWithValue("@Fna", _clsItemListSourcing.Fna)
                cmd.Parameters.AddWithValue("@Dtl_Upc", _clsItemListSourcing.Dtl_Upc)
                cmd.Parameters.AddWithValue("@Dtl_Fna", _clsItemListSourcing.Dtl_Fna)
                cmd.Parameters.AddWithValue("@Usg_Seq", _clsItemListSourcing.Usg_Seq)
                cmd.Parameters.AddWithValue("@Lvl", _clsItemListSourcing.Lvl)
                cmd.Parameters.AddWithValue("@Hand", _clsItemListSourcing.Hand)
                cmd.Parameters.AddWithValue("@D_C", _clsItemListSourcing.D_C)
                cmd.Parameters.AddWithValue("@Dwg_No", _clsItemListSourcing.Dwg_No)
                cmd.Parameters.AddWithValue("@Part_No_9_Digit", IIf(String.IsNullOrEmpty(_clsItemListSourcing.Part_No_9_Digit), DBNull.Value, _clsItemListSourcing.Part_No_9_Digit))
                cmd.Parameters.AddWithValue("@Part_Name", _clsItemListSourcing.Part_Name)
                cmd.Parameters.AddWithValue("@Qty", _clsItemListSourcing.Qty)
                cmd.Parameters.AddWithValue("@Variant1", _clsItemListSourcing.Variant1)
                cmd.Parameters.AddWithValue("@Variant2", _clsItemListSourcing.Variant2)
                cmd.Parameters.AddWithValue("@Variant3", _clsItemListSourcing.Variant3)
                cmd.Parameters.AddWithValue("@Variant4", _clsItemListSourcing.Variant4)
                cmd.Parameters.AddWithValue("@Variant5", _clsItemListSourcing.Variant5)
                cmd.Parameters.AddWithValue("@Variant6", _clsItemListSourcing.Variant6)
                cmd.Parameters.AddWithValue("@Variant7", _clsItemListSourcing.Variant7)
                cmd.Parameters.AddWithValue("@Variant8", _clsItemListSourcing.Variant8)
                cmd.Parameters.AddWithValue("@Variant9", _clsItemListSourcing.Variant9)
                cmd.Parameters.AddWithValue("@Variant10", _clsItemListSourcing.Variant10)
                cmd.Parameters.AddWithValue("@Variant11", _clsItemListSourcing.Variant11)
                cmd.Parameters.AddWithValue("@Variant12", _clsItemListSourcing.Variant12)
                cmd.Parameters.AddWithValue("@Variant13", _clsItemListSourcing.Variant13)
                cmd.Parameters.AddWithValue("@Variant14", _clsItemListSourcing.Variant14)
                cmd.Parameters.AddWithValue("@Variant15", _clsItemListSourcing.Variant15)
                cmd.Parameters.AddWithValue("@Variant16", _clsItemListSourcing.Variant16)
                cmd.Parameters.AddWithValue("@Variant17", _clsItemListSourcing.Variant17)
                cmd.Parameters.AddWithValue("@Variant18", _clsItemListSourcing.Variant18)
                cmd.Parameters.AddWithValue("@Variant19", _clsItemListSourcing.Variant19)
                cmd.Parameters.AddWithValue("@Variant20", _clsItemListSourcing.Variant20)
                cmd.Parameters.AddWithValue("@Variant21", _clsItemListSourcing.Variant21)
                cmd.Parameters.AddWithValue("@Variant22", _clsItemListSourcing.Variant22)
                cmd.Parameters.AddWithValue("@Variant23", _clsItemListSourcing.Variant23)
                cmd.Parameters.AddWithValue("@Variant24", _clsItemListSourcing.Variant24)
                cmd.Parameters.AddWithValue("@Variant25", _clsItemListSourcing.Variant25)
                cmd.Parameters.AddWithValue("@Variant26", _clsItemListSourcing.Variant26)
                cmd.Parameters.AddWithValue("@Variant27", _clsItemListSourcing.Variant27)
                cmd.Parameters.AddWithValue("@Variant28", _clsItemListSourcing.Variant28)
                cmd.Parameters.AddWithValue("@Variant29", _clsItemListSourcing.Variant29)
                cmd.Parameters.AddWithValue("@Variant30", _clsItemListSourcing.Variant30)
                cmd.Parameters.AddWithValue("@Variant31", _clsItemListSourcing.Variant31)
                cmd.Parameters.AddWithValue("@Variant32", _clsItemListSourcing.Variant32)
                cmd.Parameters.AddWithValue("@Variant33", _clsItemListSourcing.Variant33)
                cmd.Parameters.AddWithValue("@Variant34", _clsItemListSourcing.Variant34)
                cmd.Parameters.AddWithValue("@Variant35", _clsItemListSourcing.Variant35)
                cmd.Parameters.AddWithValue("@Variant36", _clsItemListSourcing.Variant36)
                cmd.Parameters.AddWithValue("@PIC", _clsItemListSourcing.PIC)
                cmd.Parameters.AddWithValue("@Guide_Price", _clsItemListSourcing.Guide_Price)
                cmd.Parameters.AddWithValue("@Jpn_Supplier", _clsItemListSourcing.Jpn_Supplier)
                cmd.Parameters.AddWithValue("@Currency", _clsItemListSourcing.Currency)
                cmd.Parameters.AddWithValue("@Drawing_Receive", _clsItemListSourcing.Drawing_Receive)
                cmd.Parameters.AddWithValue("@Latest_Cost_Report", _clsItemListSourcing.Latest_Cost_Report)
                cmd.Parameters.AddWithValue("@Part_No_Oes", _clsItemListSourcing.Part_No_Oes)
                'cmd.Parameters.AddWithValue("@Purchasing_Group", _clsItemListSourcing.Purchasing_Group)
                cmd.Parameters.AddWithValue("@SentToBuyer", _clsItemListSourcing.SentToBuyer)
                cmd.Parameters.AddWithValue("@BuyerDrawingCheck", _clsItemListSourcing.BuyerDrawingCheck)
                cmd.Parameters.AddWithValue("@Register_By", _clsItemListSourcing.Register_By)
                cmd.Parameters.AddWithValue("@VariantHeader", removecomma)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function getOrderVariant(_variantName As String, _prjid As String, _clsItemListSourcing As clsItemListSourcing, Optional ByRef pErr As String = "") As DataSet

        Dim removecomma As String = _variantName.Remove(_variantName.Length - 1)
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Item_List_sourcing_GetVariantOrder", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@VariantName", removecomma)
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Part_No", _clsItemListSourcing.Part_No)
                cmd.Parameters.AddWithValue("@Group_ID", _clsItemListSourcing.Group_ID)
                cmd.Parameters.AddWithValue("@Commodity", _clsItemListSourcing.Commodity)
                cmd.Parameters.AddWithValue("@Upc", _clsItemListSourcing.Upc)
                cmd.Parameters.AddWithValue("@Fna", _clsItemListSourcing.Fna)
                cmd.Parameters.AddWithValue("@Dtl_Upc", _clsItemListSourcing.Dtl_Upc)
                cmd.Parameters.AddWithValue("@Dtl_Fna", _clsItemListSourcing.Dtl_Fna)
                cmd.Parameters.AddWithValue("@Usg_Seq", _clsItemListSourcing.Usg_Seq)
                cmd.Parameters.AddWithValue("@Lvl", _clsItemListSourcing.Lvl)
                cmd.Parameters.AddWithValue("@Hand", _clsItemListSourcing.Hand)
                cmd.Parameters.AddWithValue("@D_C", _clsItemListSourcing.D_C)
                cmd.Parameters.AddWithValue("@Dwg_No", _clsItemListSourcing.Dwg_No)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getVariantPT01(projid As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_ItemListSourcing_GetVariantPT01", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_Id", projid)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getProjectType(projectid As String, Optional ByRef pErr As String = "") As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT Project_type FROM Proj_Header WHERE Project_ID ='" + projectid + "'", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getGroupCommodityPIC(_param As String, _param2 As String, _param3 As String, _type As String, _withALL As String, _step As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_ItemListSource_GetGroupComodityPIC", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Parameter", _param)
                cmd.Parameters.AddWithValue("@Parameter2", _param2)
                cmd.Parameters.AddWithValue("@Parameter3", _param3)
                cmd.Parameters.AddWithValue("@type", _type)
                cmd.Parameters.AddWithValue("@withALL", _withALL)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                cmd.Parameters.AddWithValue("@Step", _step)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getDataEPL(projid As String, puser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_ItemListSourcing_GetDataEPL", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", projid)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertStatusDrawing(Project_ID As String, Part_No As String, Upc As String, Fna As String, Dtl_Upc As String, _
                                               Dtl_Fna As String, Usg_Seq As String, Qty As String, D_C As String, pUser As String, _
                                               Status_Drawing As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_ItemListSourcingDrawing_Ins"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@No", "")
                cmd.Parameters.AddWithValue("@Project_ID", Project_ID)
                cmd.Parameters.AddWithValue("@Part_No", Part_No)
                cmd.Parameters.AddWithValue("@Upc", Upc)
                cmd.Parameters.AddWithValue("@Fna", Fna)
                cmd.Parameters.AddWithValue("@Epl_Part_No", "")
                cmd.Parameters.AddWithValue("@Dtl_Upc", Dtl_Upc)
                cmd.Parameters.AddWithValue("@Dtl_Fna", Dtl_Fna)
                cmd.Parameters.AddWithValue("@Usg_Seq", Usg_Seq)
                cmd.Parameters.AddWithValue("@Qty", Qty)
                cmd.Parameters.AddWithValue("@D_C", D_C)
                cmd.Parameters.AddWithValue("@Body_Color", "")
                cmd.Parameters.AddWithValue("@Extr_Color", "")
                cmd.Parameters.AddWithValue("@Status_Drawing", Status_Drawing)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function CheckingStatusDrawing(Project_ID As String, Part_No As String, Upc As String, Fna As String, Dtl_Upc As String, _
                                              Dtl_Fna As String, Usg_Seq As String, Qty As String, D_C As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "SELECT COUNT(*) FROM Item_List_Sourcing WHERE Project_ID= '" + Project_ID + "' AND Part_No = '" + Part_No + "' AND Upc = '" + Upc + "' AND Fna = '" + Fna + "' AND Dtl_Upc = '" + Dtl_Upc + "' AND Dtl_Fna = '" + Dtl_Fna + "' AND Usg_Seq = '" + Usg_Seq + "' AND Qty = '" + Qty + "' AND D_C = '" + D_C + "'"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function CheckingEPL(Project_ID As String, Part_No As String, Upc As String, Fna As String, Dtl_Upc As String, _
                                            Dtl_Fna As String, Usg_Seq As String, Qty As String, D_C As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "SELECT COUNT(*) FROM Engineering_Part_List WHERE Project_ID= '" + Project_ID + "' AND Part_No = '" + Part_No + "' AND Upc = '" + Upc + "' AND Fna = '" + Fna + "' AND Dtl_Upc = '" + Dtl_Upc + "' AND Dtl_Fna = '" + Dtl_Fna + "' AND Usg_Seq = '" + Usg_Seq + "' AND Qty = '" + Qty + "' AND D_C = '" + D_C + "'"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function getHeaderGridAcceptance(_prjid As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_DrawingAcceptance_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDrawingAcceptance(_clsItemListSourcing As clsItemListSourcing, drawingComplete As String, pUser As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_DrawingAcceptance_Ins"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _clsItemListSourcing.Project_ID)
                cmd.Parameters.AddWithValue("@Group_ID", _clsItemListSourcing.Group_ID)
                cmd.Parameters.AddWithValue("@Commodity", _clsItemListSourcing.Commodity)
                cmd.Parameters.AddWithValue("@Drawing_Complete", drawingComplete)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                cmd.Parameters.AddWithValue("@Part_No", _clsItemListSourcing.Part_No)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function InsertDrawingGrouping(_clsItemListSourcing As clsItemListSourcing, groupCommodity As String, pUser As String, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_ItemListSourcing_Grouping"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _clsItemListSourcing.Project_ID)
                cmd.Parameters.AddWithValue("@Group_ID", _clsItemListSourcing.Group_ID)
                cmd.Parameters.AddWithValue("@Commodity", _clsItemListSourcing.Commodity)
                cmd.Parameters.AddWithValue("@GroupCommodity", groupCommodity)
                cmd.Parameters.AddWithValue("@Part_No", _clsItemListSourcing.Part_No)
                cmd.Parameters.AddWithValue("@UserId", pUser)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function checkStatusDrawingComplete(No As String, Project_ID As String, Part_No As String, Upc As String, Fna As String, Epl_Part_No As String, Dtl_Upc As String, Dtl_Fna As String, Usg_Seq As String, Qty As String, D_C As String, Optional ByRef pErr As String = "") As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT CASE WHEN ISNULL(Status_Drawing,'') = '' THEN '0' ELSE '1' END FROM Engineering_Part_List WHERE No = '" + No + "' AND Project_ID = '" + Project_ID + "' AND Part_No = '" + Part_No + "' AND Upc = '" + Upc + "' AND Fna = '" + Fna + "' AND Epl_Part_No = '" + Epl_Part_No + "' AND Dtl_Upc = '" + Dtl_Upc + "' AND Dtl_Fna = '" + Dtl_Fna + "' AND Usg_Seq = '" + Usg_Seq + "' AND Qty = '" + Qty + "' AND D_C = '" + D_C + "'", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function checkStatusDrawingAcceptance(projectid As String, groupid As String, commodity As String, partno As String, Optional ByRef pErr As String = "") As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT CASE WHEN ISNULL(Status_Acceptance_date,'') = '' THEN '0' ELSE '1' END FROM Item_List_Sourcing WHERE Project_ID ='" + projectid + "' AND Group_ID = '" + groupid + "' AND Commodity = '" + commodity + "' AND Part_No = '" + partno + "'", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function checkStatusGrouping(projectid As String, groupid As String, commodity As String, partno As String, Optional ByRef pErr As String = "") As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT CASE WHEN ISNULL(Group_Comodity,'') = '' THEN '0' ELSE '1' END cnt FROM Item_List_Sourcing WHERE Project_ID ='" + projectid + "' AND Group_ID = '" + groupid + "' AND Commodity = '" + commodity + "' AND Part_No = '" + partno + "'", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function countGroupCommodity(projectid As String, groupid As String, commodity As String, Optional ByRef pErr As String = "") As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT COUNT(DISTINCT Group_Comodity) FROM Item_List_Sourcing WHERE Project_ID = '" + projectid + "' AND Group_ID = '" + groupid + "' AND Commodity = '" + commodity + "' AND Group_Comodity IS NOT NULL", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function checkCurrency(currency As String, Optional ByRef pErr As String = "") As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT COUNT(*) FROM Mst_Parameter WHERE Par_Code = '" + currency + "' AND Par_Group = 'Currency'", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                If rtn = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function checkUserPIC(user As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("SELECT COUNT(*) FROM UserSetup WHERE UserInitial = '" + user + "'", con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                If rtn = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getExistsTempUpload(_prjid As String, _grpid As String, _comd As String, _partno As String, _Upc As String, _Fna As String, _Dtl_Upc As String, _
                                               _Dtl_Fna As String, _UsgSeq As String, _Qty As String, _
                                               _actionType As String, _puser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_ItemListSourcing_TempUpload", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", IIf(String.IsNullOrEmpty(_prjid), "", _prjid))
                cmd.Parameters.AddWithValue("@Group_ID", IIf(String.IsNullOrEmpty(_grpid), "", _grpid))
                cmd.Parameters.AddWithValue("@Commodity", IIf(String.IsNullOrEmpty(_comd), "", _comd))
                cmd.Parameters.AddWithValue("@Part_No", IIf(String.IsNullOrEmpty(_partno), "", _partno))
                cmd.Parameters.AddWithValue("@Upc", IIf(String.IsNullOrEmpty(_Upc), "", _Upc))
                cmd.Parameters.AddWithValue("@Fna", IIf(String.IsNullOrEmpty(_Fna), "", _Fna))
                cmd.Parameters.AddWithValue("@Dtl_Upc", IIf(String.IsNullOrEmpty(_Dtl_Upc), "", _Dtl_Upc))
                cmd.Parameters.AddWithValue("@Dtl_Fna", IIf(String.IsNullOrEmpty(_Dtl_Fna), "", _Dtl_Fna))
                cmd.Parameters.AddWithValue("@Usg_Seq", IIf(String.IsNullOrEmpty(_UsgSeq), "", _UsgSeq))
                cmd.Parameters.AddWithValue("@Qty", IIf(String.IsNullOrEmpty(_Qty), DBNull.Value, _Qty))
                cmd.Parameters.AddWithValue("@Register_By", _puser)
                cmd.Parameters.AddWithValue("@actionType", _actionType)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getGateIIIApproval(_prjid As String, _grpid As String, _comd As String, _groupCommodity As String, _partno As String, _puser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_GATE_III_Approval", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comd)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupCommodity)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                cmd.Parameters.AddWithValue("@UserId", _puser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function getGateIList(_prjid As String, _grpid As String, _comd As String, _groupCommodity As String, _puser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comd)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupCommodity)
                cmd.Parameters.AddWithValue("@Buyer", _puser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function getGateIListReleased(_prjid As String, _grpid As String, _comd As String, _groupCommodity As String, _puser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_List_Released", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comd)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupCommodity)
                cmd.Parameters.AddWithValue("@Buyer", _puser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getGateIListConfirm(_prjid As String, _grpid As String, _comd As String, _groupCommodity As String, _puser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_List_Confirm", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comd)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupCommodity)
                cmd.Parameters.AddWithValue("@Buyer", _puser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getGateIListApproval(_prjid As String, _grpid As String, _comd As String, _groupCommodity As String, _puser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_List_Approval", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comd)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupCommodity)
                cmd.Parameters.AddWithValue("@UserId", _puser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getApprovalPerson(_prjid As String, _grpid As String, _comd As String, _groupCommodity As String, _puser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_Approval_Status", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comd)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupCommodity)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getApprovalGate3Person(_prjid As String, _grpid As String, _comd As String, _groupCommodity As String, _partno As String, _puser As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_III_Approval_Status", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comd)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupCommodity)
                cmd.Parameters.AddWithValue("@Part_No", _partno)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

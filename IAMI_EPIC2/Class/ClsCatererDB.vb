﻿Imports System.Data.SqlClient

Public Class ClsCatererDB
    Public Shared Function GetList(Optional ByRef pErr As String = "") As List(Of ClsCaterer)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Caterer_Sel"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)

                Dim list As New List(Of ClsCaterer)
                For i = 0 To dt.Rows.Count - 1
                    Dim loc As New ClsCaterer With {.CatererID = Trim(dt.Rows(i).Item("CatererID") & ""),
                                                    .CatererName = Trim(dt.Rows(i).Item("CatererName") & ""),
                                                    .Address = Trim(dt.Rows(i).Item("Address") & ""),
                                                    .Phone1 = Trim(dt.Rows(i).Item("Phone1") & ""),
                                                    .Phone2 = Trim(dt.Rows(i).Item("Phone2") & ""),
                                                    .Fax = Trim(dt.Rows(i).Item("Fax") & ""),
                                                    .Email = Trim(dt.Rows(i).Item("Email") & ""),
                                                    .GSTNo = Trim(dt.Rows(i).Item("GSTNo") & ""),
                                                    .ContactPerson = Trim(dt.Rows(i).Item("ContactPerson") & ""),
                                                    .CompanyRegistrationNo = Trim(dt.Rows(i).Item("CompanyRegistrationNo") & ""),
                                                    .ServedLocationID = Trim(dt.Rows(i).Item("ServedLocationID") & ""),
                                                    .ActiveStatus = Trim(dt.Rows(i).Item("ActiveStatus") & "")}

                    list.Add(loc)
                Next
                Return list
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pLoc As ClsCaterer, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Caterer_Ins"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CatererID", pLoc.CatererID)
                cmd.Parameters.AddWithValue("CatererName", pLoc.CatererName)
                cmd.Parameters.AddWithValue("Address", pLoc.Address)
                cmd.Parameters.AddWithValue("Phone1", pLoc.Phone1)
                If pLoc.Phone2 <> "" Then
                    cmd.Parameters.AddWithValue("Phone2", pLoc.Phone2)
                Else
                    cmd.Parameters.AddWithValue("Phone2", DBNull.Value)
                End If
                If pLoc.Fax <> "" Then
                    cmd.Parameters.AddWithValue("Fax", pLoc.Fax)
                Else
                    cmd.Parameters.AddWithValue("Fax", DBNull.Value)
                End If
                cmd.Parameters.AddWithValue("Email", pLoc.Email)
                cmd.Parameters.AddWithValue("CompanyRegistrationNo", pLoc.CompanyRegistrationNo)
                cmd.Parameters.AddWithValue("GSTNo", pLoc.GSTNo)
                cmd.Parameters.AddWithValue("ContactPerson", pLoc.ContactPerson)
                If pLoc.ServedLocationID = "" Then
                    cmd.Parameters.AddWithValue("ServedLocationID", DBNull.Value)
                Else
                    cmd.Parameters.AddWithValue("ServedLocationID", pLoc.ServedLocationID)
                End If
                If pLoc.ActiveStatus = "Active" Then
                    cmd.Parameters.AddWithValue("ActiveStatus", "1")
                Else
                    cmd.Parameters.AddWithValue("ActiveStatus", "0")
                End If
                cmd.Parameters.AddWithValue("UserID", pLoc.CreateUser)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function Update(ByVal pLoc As ClsCaterer, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Caterer_Upd"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CatererID", pLoc.CatererID)
                cmd.Parameters.AddWithValue("CatererName", pLoc.CatererName)
                cmd.Parameters.AddWithValue("Address", pLoc.Address)
                cmd.Parameters.AddWithValue("Phone1", pLoc.Phone1)
                If pLoc.Phone2 <> "" Then
                    cmd.Parameters.AddWithValue("Phone2", pLoc.Phone2)
                Else
                    cmd.Parameters.AddWithValue("Phone2", DBNull.Value)
                End If
                If pLoc.Fax <> "" Then
                    cmd.Parameters.AddWithValue("Fax", pLoc.Fax)
                Else
                    cmd.Parameters.AddWithValue("Fax", DBNull.Value)
                End If
                cmd.Parameters.AddWithValue("Email", pLoc.Email)
                cmd.Parameters.AddWithValue("CompanyRegistrationNo", pLoc.CompanyRegistrationNo)
                cmd.Parameters.AddWithValue("GSTNo", pLoc.GSTNo)
                cmd.Parameters.AddWithValue("ContactPerson", pLoc.ContactPerson)
                If pLoc.ServedLocationID = "" Then
                    cmd.Parameters.AddWithValue("ServedLocationID", DBNull.Value)
                Else
                    cmd.Parameters.AddWithValue("ServedLocationID", pLoc.ServedLocationID)
                End If
                If pLoc.ActiveStatus = "0" Then
                    cmd.Parameters.AddWithValue("ActiveStatus", pLoc.ActiveStatus)
                ElseIf pLoc.ActiveStatus = "1" Then
                    cmd.Parameters.AddWithValue("ActiveStatus", pLoc.ActiveStatus)
                ElseIf pLoc.ActiveStatus = "Active" Then
                    cmd.Parameters.AddWithValue("ActiveStatus", "1")
                ElseIf pLoc.ActiveStatus = "Inactive" Then
                    cmd.Parameters.AddWithValue("ActiveStatus", "0")
                End If
                cmd.Parameters.AddWithValue("UserID", pLoc.UpdateUser)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function Delete(ByVal pLoc As ClsCaterer, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Caterer_Del"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CatererID", pLoc.CatererID)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function isExist(ByVal pCatererID As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Caterer_Exist"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CatererID", pCatererID)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count <> 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

﻿Imports System.Data.SqlClient

Public Class clsComparisonDB
    Public Shared Function getlistds(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal Group_Comodity As String, ByVal PartNo As String, ByVal UserID As String, Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_II_CS_Comparison_Sel_List"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("ComodityGroup", Group_Comodity)
                cmd.Parameters.AddWithValue("Part_No", PartNo)
                cmd.Parameters.AddWithValue("UserId", UserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getlist(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal ComodityGroup As String, ByVal Part_No As String, Optional ByRef perr As String = "") As List(Of clsComparison)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                Dim TSupplier1 As Decimal = 0, TSupplier2 As Decimal = 0, TSupplier3 As Decimal = 0, TSupplier4 As Decimal = 0, TSupplier5 As Decimal = 0, TSupplier6 As Decimal = 0, TGuidePrice As Decimal = 0
                Dim TSuppliersub1 As Decimal = 0, TSuppliersub2 As Decimal = 0, TSuppliersub3 As Decimal = 0, TSuppliersub4 As Decimal = 0, TSuppliersub5 As Decimal = 0, TSuppliersub6 As Decimal = 0, TGuidePricesub As Decimal = 0
                Dim TDepreciation1 As Decimal = 0, TDepreciation2 As Decimal = 0, TDepreciation3 As Decimal = 0, TDepreciation4 As Decimal = 0, TDepreciation5 As Decimal = 0, TDepreciation6 As Decimal = 0, TGuidePriceDepreciation As Decimal = 0
                sql = "sp_Gate_II_CS_Comparison_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("ComodityGroup", ComodityGroup)
                cmd.Parameters.AddWithValue("Part_No", Part_No)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of clsComparison)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New clsComparison With {
                        .Description = Trim(dt.Rows(i).Item("Description") & ""),
                        .GuidePrice = IIf(dt.Rows(i).Item("GuidePrice").ToString = "", 0, dt.Rows(i).Item("GuidePrice")),
                        .Supplier1 = IIf(dt.Rows(i).Item("Supplier1").ToString = "", 0, dt.Rows(i).Item("Supplier1")),
                        .Supplier2 = IIf(dt.Rows(i).Item("Supplier2").ToString = "", 0, dt.Rows(i).Item("Supplier2")),
                        .Supplier3 = IIf(dt.Rows(i).Item("Supplier3").ToString = "", 0, dt.Rows(i).Item("Supplier3")),
                        .Supplier4 = IIf(dt.Rows(i).Item("Supplier4").ToString = "", 0, dt.Rows(i).Item("Supplier4")),
                        .Supplier5 = IIf(dt.Rows(i).Item("Supplier5").ToString = "", 0, dt.Rows(i).Item("Supplier5")),
                        .Supplier6 = IIf(dt.Rows(i).Item("Supplier6").ToString = "", 0, dt.Rows(i).Item("Supplier6"))
                        }

                    If Trim(dt.Rows(i).Item("No")) = 4 Then
                        TGuidePriceDepreciation = ss.GuidePrice
                        TDepreciation1 = ss.Supplier1
                        TDepreciation2 = ss.Supplier2
                        TDepreciation3 = ss.Supplier3
                        TDepreciation4 = ss.Supplier4
                        TDepreciation5 = ss.Supplier5
                        TDepreciation6 = ss.Supplier6
                    ElseIf Trim(dt.Rows(i).Item("No")) >= 6 And Trim(dt.Rows(i).Item("No")) <= 13 Then
                        TGuidePricesub = TGuidePricesub + ss.GuidePrice
                        TSuppliersub1 = TSuppliersub1 + ss.Supplier1
                        TSuppliersub2 = TSuppliersub2 + ss.Supplier2
                        TSuppliersub3 = TSuppliersub3 + ss.Supplier3
                        TSuppliersub4 = TSuppliersub4 + ss.Supplier4
                        TSuppliersub5 = TSuppliersub5 + ss.Supplier5
                        TSuppliersub6 = TSuppliersub6 + ss.Supplier6
                    ElseIf Trim(dt.Rows(i).Item("No")) = 14 Then
                        TGuidePrice = ss.GuidePrice
                        TSupplier1 = ss.Supplier1
                        TSupplier2 = ss.Supplier2
                        TSupplier3 = ss.Supplier3
                        TSupplier4 = ss.Supplier4
                        TSupplier6 = ss.Supplier5
                        TSupplier6 = ss.Supplier6
                    End If

                    list.Add(ss)
                Next
                ' Total sub
                Dim xy As New clsComparison With {
                       .Description = "Total Sub",
                       .GuidePrice = TGuidePricesub,
                       .Supplier1 = TSuppliersub1,
                       .Supplier2 = TSuppliersub2,
                       .Supplier3 = TSuppliersub3,
                       .Supplier4 = TSuppliersub4,
                       .Supplier5 = TSuppliersub5,
                       .Supplier6 = TSuppliersub6
                       }
                list.Add(xy)

                ' Total dep
                Dim xx As New clsComparison With {
                       .Description = "Total Dep",
                       .GuidePrice = TGuidePricesub,
                       .Supplier1 = (TSupplier1 + TSuppliersub1) / IIf(TDepreciation1 = 0, 1, TDepreciation1),
                       .Supplier2 = (TSupplier2 + TSuppliersub2) / IIf(TDepreciation2 = 0, 1, TDepreciation2),
                       .Supplier3 = (TSupplier3 + TSuppliersub3) / IIf(TDepreciation3 = 0, 1, TDepreciation3),
                       .Supplier4 = (TSupplier4 + TSuppliersub4) / IIf(TDepreciation4 = 0, 1, TDepreciation4),
                       .Supplier5 = (TSupplier5 + TSuppliersub5) / IIf(TDepreciation5 = 0, 1, TDepreciation5),
                .Supplier6 = (TSupplier6 + TSuppliersub6) / IIf(TDepreciation6 = 0, 1, TDepreciation6)
                       }
                list.Add(xx)
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getlistTooling(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal ComodityGroup As String, ByVal Part_No As String, Optional ByRef perr As String = "") As List(Of clsComparison)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                Dim TSuppliersub1 As Decimal = 0, TSuppliersub2 As Decimal = 0, TSuppliersub3 As Decimal = 0, TSuppliersub4 As Decimal = 0, TSuppliersub5 As Decimal = 0, TSuppliersub6 As Decimal = 0, TGuidePricesub As Decimal = 0
                sql = "sp_Gate_II_CS_Comparison_Tooling_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("ComodityGroup", ComodityGroup)
                cmd.Parameters.AddWithValue("Part_No", Part_No)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of clsComparison)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New clsComparison With {
                        .Description = Trim(dt.Rows(i).Item("Description") & ""),
                        .GuidePrice = IIf(dt.Rows(i).Item("GuidePrice").ToString = "", 0, dt.Rows(i).Item("GuidePrice")),
                        .Supplier1 = IIf(dt.Rows(i).Item("Supplier1").ToString = "", 0, dt.Rows(i).Item("Supplier1")),
                        .Supplier2 = IIf(dt.Rows(i).Item("Supplier2").ToString = "", 0, dt.Rows(i).Item("Supplier2")),
                        .Supplier3 = IIf(dt.Rows(i).Item("Supplier3").ToString = "", 0, dt.Rows(i).Item("Supplier3")),
                        .Supplier4 = IIf(dt.Rows(i).Item("Supplier4").ToString = "", 0, dt.Rows(i).Item("Supplier4")),
                        .Supplier5 = IIf(dt.Rows(i).Item("Supplier5").ToString = "", 0, dt.Rows(i).Item("Supplier5")),
                        .Supplier6 = IIf(dt.Rows(i).Item("Supplier6").ToString = "", 0, dt.Rows(i).Item("Supplier6"))
                        }
                    TGuidePricesub = TGuidePricesub + ss.GuidePrice
                    TSuppliersub1 = TSuppliersub1 + ss.Supplier1
                    TSuppliersub2 = TSuppliersub2 + ss.Supplier2
                    TSuppliersub3 = TSuppliersub3 + ss.Supplier3
                    TSuppliersub4 = TSuppliersub4 + ss.Supplier4
                    TSuppliersub5 = TSuppliersub5 + ss.Supplier5
                    TSuppliersub6 = TSuppliersub6 + ss.Supplier6


                    list.Add(ss)
                Next
                ' Total sub
                Dim xy As New clsComparison With {
                       .Description = "Total",
                       .GuidePrice = TGuidePricesub,
                       .Supplier1 = TSuppliersub1,
                       .Supplier2 = TSuppliersub2,
                       .Supplier3 = TSuppliersub3,
                       .Supplier4 = TSuppliersub4,
                       .Supplier5 = TSuppliersub5,
                       .Supplier6 = TSuppliersub6
                       }
                list.Add(xy)

                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getProjectID(ByVal ProjectID As String, ByVal GroupID As String, Comodity As String, ByVal ComodityGroup As String, Status As String, puser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_II_CS_Comparison_ComboFilter", con)
                cmd.CommandType = CommandType.StoredProcedure
                'cmd.Parameters.AddWithValue("Project_Type", IIf(ProjectType = Nothing, "", ProjectType))
                cmd.Parameters.AddWithValue("Project_ID", IIf(ProjectID = Nothing, "", ProjectID))
                cmd.Parameters.AddWithValue("Group_ID", IIf(GroupID = Nothing, "", GroupID))
                cmd.Parameters.AddWithValue("Comodity", IIf(Comodity = Nothing, "", Comodity))
                cmd.Parameters.AddWithValue("ComodityGroup", IIf(ComodityGroup = Nothing, "", ComodityGroup))
                cmd.Parameters.AddWithValue("Status", Status)
                cmd.Parameters.AddWithValue("UserID", puser)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getHeaderName(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal ComodityGroup As String, ByVal Part_No As String, Optional ByRef perr As String = "") As List(Of clsComparison)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_II_CS_Comparison_Name_Header"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", ProjectID)
                cmd.Parameters.AddWithValue("Group_ID", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("ComodityGroup", ComodityGroup)
                cmd.Parameters.AddWithValue("Part_No", Part_No)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of clsComparison)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New clsComparison With {
                        .Description = Trim(dt.Rows(i).Item("Supplier_Name") & "")
                        }
                    list.Add(ss)
                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

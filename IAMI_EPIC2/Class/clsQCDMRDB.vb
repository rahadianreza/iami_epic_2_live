﻿Imports System.Data.SqlClient

Public Class clsQCDMRDB
    Public Shared Function getlistds(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal Group_Comodity As String, ByVal PartNo As String, ByVal pUser As String, Optional ByRef perr As String = "") As DataSet
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_III_CS_QCDMR_Sel_List"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("ComodityGroup", Group_Comodity)
                cmd.Parameters.AddWithValue("Part_No", PartNo)
                cmd.Parameters.AddWithValue("UserID", pUser)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataSet
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getlist(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal ComodityGroup As String, ByVal Part_No As String, Optional ByRef perr As String = "") As List(Of clsQCDMR)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""

                sql = "sp_Gate_III_CS_QCDMR_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("ComodityGroup", ComodityGroup)
                cmd.Parameters.AddWithValue("Part_No", Part_No)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of clsQCDMR)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New clsQCDMR With {
                        .seqnogroup = Trim(dt.Rows(i).Item("seqnogroup") & ""),
                        .ID = Trim(dt.Rows(i).Item("ID") & ""),
                        .Aspect = Trim(dt.Rows(i).Item("Aspect") & ""),
                        .Item = Trim(dt.Rows(i).Item("Item") & ""),
                        .VendorCode1 = Trim(dt.Rows(i).Item("vendorCode1") & ""),
                        .pValue1 = IIf(dt.Rows(i).Item("pValue1").ToString = "", 0, dt.Rows(i).Item("pValue1")),
                        .pValueSubTotal1 = IIf(dt.Rows(i).Item("pValueSubTotal1").ToString = "", 0, dt.Rows(i).Item("pValueSubTotal1")),
                        .VendorCode2 = Trim(dt.Rows(i).Item("vendorCode2") & ""),
                        .pValue2 = IIf(dt.Rows(i).Item("pValue2").ToString = "", 0, dt.Rows(i).Item("pValue2")),
                        .pValueSubTotal2 = IIf(dt.Rows(i).Item("pValueSubTotal2").ToString = "", 0, dt.Rows(i).Item("pValueSubTotal2")),
                        .VendorCode3 = Trim(dt.Rows(i).Item("vendorCode3") & ""),
                        .pValue3 = IIf(dt.Rows(i).Item("pValue3").ToString = "", 0, dt.Rows(i).Item("pValue3")),
                        .pValueSubTotal3 = IIf(dt.Rows(i).Item("pValueSubTotal3").ToString = "", 0, dt.Rows(i).Item("pValueSubTotal3")),
                        .VendorCode4 = Trim(dt.Rows(i).Item("vendorCode4") & ""),
                        .pValue4 = IIf(dt.Rows(i).Item("pValue4").ToString = "", 0, dt.Rows(i).Item("pValue4")),
                        .pValueSubTotal4 = IIf(dt.Rows(i).Item("pValueSubTotal4").ToString = "", 0, dt.Rows(i).Item("pValueSubTotal4")),
                        .VendorCode5 = Trim(dt.Rows(i).Item("vendorCode5") & ""),
                        .pValue5 = IIf(dt.Rows(i).Item("pValue5").ToString = "", 0, dt.Rows(i).Item("pValue5")),
                        .pValueSubTotal5 = IIf(dt.Rows(i).Item("pValueSubTotal5").ToString = "", 0, dt.Rows(i).Item("pValueSubTotal5")),
                        .VendorCode6 = Trim(dt.Rows(i).Item("vendorCode6") & ""),
                        .pValue6 = IIf(dt.Rows(i).Item("pValue6").ToString = "", 0, dt.Rows(i).Item("pValue6")),
                    .pValueSubTotal6 = IIf(dt.Rows(i).Item("pValueSubTotal6").ToString = "", 0, dt.Rows(i).Item("pValueSubTotal6"))
                        }

                    list.Add(ss)
                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function getProjectID(ProjectID As String, GroupID As String, Comodity As String, ComodityGroup As String, Status As String, ByVal puserID As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_III_CS_QCDMR__ComboFilter", con)
                cmd.CommandType = CommandType.StoredProcedure
                'cmd.Parameters.AddWithValue("Project_Type", IIf(ProjectType = Nothing, "", ProjectType))
                cmd.Parameters.AddWithValue("Project_ID", IIf(ProjectID = Nothing, "", ProjectID))
                cmd.Parameters.AddWithValue("Group_ID", IIf(GroupID = Nothing, "", GroupID))
                cmd.Parameters.AddWithValue("Comodity", IIf(Comodity = Nothing, "", Comodity))
                cmd.Parameters.AddWithValue("ComodityGroup", IIf(ComodityGroup = Nothing, "", ComodityGroup))
                cmd.Parameters.AddWithValue("Status", Status)
                cmd.Parameters.AddWithValue("UserID", puserID)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function getHeaderName(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal ComodityGroup As String, ByVal Part_No As String, Optional ByRef perr As String = "") As List(Of clsQCDMR)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Gate_III_CS_QCDMR_Name_Header"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", ProjectID)
                cmd.Parameters.AddWithValue("Group_ID", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("ComodityGroup", ComodityGroup)
                cmd.Parameters.AddWithValue("Part_No", Part_No)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of clsQCDMR)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New clsQCDMR With {
                        .SupplierName = Trim(dt.Rows(i).Item("Supplier_Name") & "")
                        }
                    list.Add(ss)
                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function Update(ByVal pSes As clsQCDMR, Optional ByRef pErr As String = "") As Integer
        Dim cmd As SqlCommand
        Dim i As Integer
        Dim PvendorCode As String = ""
        Dim pvalue As Decimal
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                For xy = 1 To 6
                    If xy = 1 Then
                        PvendorCode = pSes.VendorCode1
                        pvalue = pSes.pValue1
                    ElseIf xy = 2 Then
                        PvendorCode = pSes.VendorCode2
                        pvalue = pSes.pValue2
                    ElseIf xy = 3 Then
                        PvendorCode = pSes.VendorCode3
                        pvalue = pSes.pValue3
                    ElseIf xy = 4 Then
                        PvendorCode = pSes.VendorCode4
                        pvalue = pSes.pValue4
                    ElseIf xy = 5 Then
                        PvendorCode = pSes.VendorCode5
                        pvalue = pSes.pValue5
                    ElseIf xy = 6 Then
                        PvendorCode = pSes.VendorCode6
                        pvalue = pSes.pValue6
                    End If
                    If PvendorCode <> "" Then
                        cmd = New SqlCommand("sp_Gate_III_CS_QCDMR_Update", con)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.AddWithValue("Project_ID", pSes.Project_ID)
                        cmd.Parameters.AddWithValue("Group", pSes.Group)
                        cmd.Parameters.AddWithValue("Comodity", pSes.Comodity)
                        cmd.Parameters.AddWithValue("ComodityGroup", pSes.ComodityGroup)
                        cmd.Parameters.AddWithValue("VendorCode", PvendorCode)
                        cmd.Parameters.AddWithValue("PartNo", pSes.PartNo)
                        cmd.Parameters.AddWithValue("ID", pSes.ID)
                        cmd.Parameters.AddWithValue("Value", pvalue)
                        cmd.Parameters.AddWithValue("UserID", pSes.UserID)
                        i = cmd.ExecuteNonQuery
                    End If

                Next

            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function GetLoad(ByVal ProjectID As String, ByVal Group As String, ByVal Comodity As String, ByVal ComodityGroup As String, ByVal Part_No As String, Optional ByRef perr As String = "") As DataSet
        Dim sql As String
        Try
            Using con As New SqlClient.SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "sp_Gate_III_CS_QCDMR_Sel"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ProjectID", ProjectID)
                cmd.Parameters.AddWithValue("Group", Group)
                cmd.Parameters.AddWithValue("Comodity", Comodity)
                cmd.Parameters.AddWithValue("ComodityGroup", ComodityGroup)
                cmd.Parameters.AddWithValue("Part_No", Part_No)
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)
                Return ds
            End Using
        Catch ex As Exception
            perr = ex.Message.ToString
            Return Nothing
        End Try
    End Function

End Class

﻿Imports System.Data.SqlClient
Imports IAMIEngine

Public Class Cls_UserSetup_DB
  
    Public Shared Function GetCaterer(ByVal pCatererID As String) As ClsCaterer
        Using cn As New SqlConnection(Sconn.Stringkoneksi)
            cn.Open()
            Dim q As String = "select * from Caterer where CatererID = @CatererID"
            Dim cmd As New SqlCommand(q, cn)
            cmd.Parameters.AddWithValue("CatererID", pCatererID)
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                Return Nothing
            Else
                With dt.Rows(0)
                    Dim Cat As New ClsCaterer
                    Cat.CatererID = .Item("CatererID")
                    Cat.CatererName = .Item("CatererName")
                    Return Cat
                End With
            End If
        End Using
    End Function

    Public Shared Function GetData(ByVal pUserID As String) As Cls_UserSetup
        Using cn As New SqlConnection(Sconn.Stringkoneksi)
            Dim sql As String
            sql = "sp_UserSetup_sel"
            Dim cmd As New SqlCommand(sql, cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("UserID", pUserID)
            Dim da As New SqlDataAdapter(Cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Dim User As New Cls_UserSetup With {
                        .AppID = dt.Rows(0)("AppID"),
                        .UserID = dt.Rows(0)("UserID"),
                        .UserName = dt.Rows(0)("UserName"),
                        .UserType = dt.Rows(0)("UserType") & "",
                        .Password = dt.Rows(0)("Password"),
                        .CatererID = (dt.Rows(0)("CatererID") & "").ToString.Trim,
                        .Description = dt.Rows(0)("Description") & "",
                        .Locked = dt.Rows(0)("Locked") & "",
                        .PasswordHint = dt.Rows(0)("PasswordHint") & "",
                        .SecurityQuestion = dt.Rows(0)("SecurityQuestion") & "",
                        .SecurityAnswer = dt.Rows(0)("SecurityAnswer") & "",
                        .AdminStatus = dt.Rows(0)("AdminStatus") & "",
                        .Supplier = dt.Rows(0)("Supplier_Code") & "",
                        .Department = dt.Rows(0)("Department_Code") & "",
                        .Section = dt.Rows(0)("Section_Code") & "",
                        .JobPos = dt.Rows(0)("Job_Position") & "",
                        .RegisterDate = dt.Rows(0)("CreateDate") & "",
                        .LastUpdate = dt.Rows(0)("UpdateDate") & ""}

                Dim enc As New clsDESEncryption("TOS")
                Dim pwd As String = enc.DecryptData(User.Password)

                Return User
            Else
                Return Nothing
            End If
        End Using
    End Function


    Public Shared Function GetMenuDS(ByVal pUserID As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using cn As New SqlConnection(Sconn.Stringkoneksi)
                Dim sql As String
                sql = "sp_UserSetup_GetMenuDS"
                Dim cmd As New SqlCommand(sql, cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("UserID", pUserID)
                Dim da As New SqlDataAdapter(Cmd)
                Dim dsMenu As New DataSet
                da.Fill(dsMenu)
                Return dsMenu
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetValidate(ByVal pUserID As String, Optional ByRef pErr As String = "") As DataSet
        Try
            pErr = ""
            Using cn As New SqlConnection(Sconn.Stringkoneksi)
                cn.Open()
                Dim sql As String = ""
                sql = "sp_UserSetup_GetValidate"
                Dim cmd As New SqlCommand(sql, cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("UserID", pUserID)
                Dim ds As New DataSet
                Dim da As New SqlDataAdapter(Cmd)
                da.Fill(ds)
                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pUser As Cls_UserSetup, Optional ByRef pErr As String = "") As Integer
        Try
            pErr = ""
            Using cn As New SqlConnection(Sconn.Stringkoneksi)
                cn.Open()
                Dim sql As String
                sql = "sp_UserSetup_InsUpt"
                Dim cmd As New SqlCommand(sql, cn)
                cmd.CommandType = CommandType.StoredProcedure
                With cmd.Parameters
                    .AddWithValue("AppID", "P01")
                    .AddWithValue("UserID", pUser.UserID)
                    .AddWithValue("UserName", pUser.UserName)
                    .AddWithValue("UserType", pUser.AdminStatus)
                    .AddWithValue("Password", pUser.Password)
                    .AddWithValue("Description", pUser.Description)
                    .AddWithValue("User_Email", pUser.Email)
                    .AddWithValue("CatererID", pUser.CatererID)
                    .AddWithValue("Locked", pUser.Locked)
                    .AddWithValue("Supplier", pUser.Supplier)
                    .AddWithValue("Department", pUser.Department)
                    .AddWithValue("Section", pUser.Section)
                    .AddWithValue("JobPos", pUser.JobPos)
                    .AddWithValue("PIC_PO", pUser.PIC_PO)
                End With
                Return Cmd.ExecuteNonQuery
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try

    End Function

    Public Shared Function Update(ByVal pUser As Cls_UserSetup, Optional ByRef pErr As String = "") As Integer
        Try
            Using cn As New SqlConnection(Sconn.Stringkoneksi)
                cn.Open()
                Dim sql As String
                sql = "sp_UserSetup_Upt"
                Dim cmd As New SqlCommand(Sql, cn)
                cmd.CommandType = CommandType.StoredProcedure
                With Cmd.Parameters
                    .AddWithValue("UserID", pUser.UserID)
                    .AddWithValue("UserName", pUser.UserName)
                    .AddWithValue("UserType", pUser.UserType)
                    .AddWithValue("Password", pUser.Password)
                    .AddWithValue("Description", pUser.Description)
                    .AddWithValue("CatererID", pUser.CatererID)
                    .AddWithValue("Locked", pUser.Locked)
                    .AddWithValue("Supplier", pUser.Supplier)
                    .AddWithValue("Department", pUser.Department)
                    .AddWithValue("Section", pUser.Section)
                    .AddWithValue("JobPos", pUser.JobPos)
                End With
                Return Cmd.ExecuteNonQuery
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try

    End Function

    Public Shared Function Delete(ByVal pUser As Cls_UserSetup, Optional ByRef pErr As String = "") As Integer
        pErr = ""
        Try
            Using cn As New SqlConnection(Sconn.Stringkoneksi)
                cn.Open()
                Dim sql As String = _
                    "DELETE UserPrivilege WHERE UserID=@UserID " & vbCrLf & _
                    "DELETE UserSetup where UserID=@UserID " & vbCrLf & _
                    "DELETE UserApproval where UserID=@UserID "
                Dim Cmd As New SqlCommand(sql, cn)
                With Cmd.Parameters
                    .AddWithValue("UserID", pUser.UserID)
                End With
                Return Cmd.ExecuteNonQuery
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function GetPicture(pID As String) As Object
        Using Cn As New SqlConnection(Sconn.Stringkoneksi)
            Cn.Open()
            Dim q As String = "SELECT User_Sign FROM dbo.UserSetup WHERE UserID = '" & Trim(pID) & "'"
            Dim cmd As New SqlCommand(q, Cn)

            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            If dt.Rows.Count = 0 Then
                Return Nothing
            Else
                If Not IsDBNull(dt.Rows(0)("User_Sign")) Then
                    Return dt.Rows(0)("User_Sign")
                Else
                    Return Nothing
                End If
            End If
        End Using
    End Function

    Public Shared Function Update_E_Sign(ByVal pSign As Cls_UserSetup, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "UPDATE dbo.UserSetup SET User_Sign = @PicSign, UpdateUser = UPPER(@User), UpdateDate = GETDATE() WHERE UserID = @UserID"
                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("PicSign", pSign.Pic_Sign)
                cmd.Parameters.AddWithValue("User", pUser)
                cmd.Parameters.AddWithValue("UserID", pSign.UserID)

                i = cmd.ExecuteNonQuery
            End Using

            Return i
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getlist(ByVal a As String, Optional ByRef perr As String = "") As DataTable
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "SELECT A.Approval_Group,A.Approval_ID,A.Approval_Level,A.Approval_Name, Case When UPPER(B.USerID) = UPPER(@UserID) then '1' else '0' end isApproval" & vbCrLf & _
                    "FROM dbo.Mst_ApprovalSetup A LEFT JOIN ( Select * from  UserApproval where UserID = @UserID) B on A.Approval_ID = B.Approval_ID and A.Approval_Group = B.Approval_Group" & vbCrLf & _
                    "Order By  A.Approval_Group,A.Approval_ID"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("UserID", a)

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Return dt
            End Using
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function SaveApproval(ByVal pUserP As clsApprovalSetupUser, Optional ByRef pErr As String = "") As Integer
        Try
            pErr = ""
            Using cn As New SqlConnection(Sconn.Stringkoneksi)
                cn.Open()
                Dim sql As String = " IF NOT EXISTS (" &
                    "   SELECT * FROM UserApproval WHERE UserID =@UserID And Approval_Group=@Approval_Group and Approval_ID = @Approval_ID)" & vbCrLf &
                    "   BEGIN " & vbCrLf &
                    "       INSERT INTO dbo.UserApproval ( Approval_Group ,Approval_ID,UserID) " & vbCrLf &
                    "       VALUES( @Approval_Group, @Approval_ID, UPPER(@UserID) )" & vbCrLf &
                    "   END ELSE BEGIN " & vbCrLf

                sql = sql +
                    "       DELETE FROM dbo.UserApproval " & vbCrLf &
                    "       WHERE UserID =@UserID And Approval_Group=@Approval_Group and Approval_ID = @Approval_ID " & vbCrLf &
                    "   END "
                Dim Cmd As New SqlCommand(sql, cn)
                With Cmd.Parameters
                    .AddWithValue("UserID", pUserP.UserID)
                    .AddWithValue("Approval_Group", pUserP.Approval_Group)
                    .AddWithValue("Approval_ID", pUserP.Approval_ID)
                 End With
                Return Cmd.ExecuteNonQuery
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try

    End Function

End Class



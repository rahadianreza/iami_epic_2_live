﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 03 Oct 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################

Imports System.Data.SqlClient

Public Class clsRFQApprovalDB

    Public Shared Function GetHeaderApproval(pDateFrom As String, pDateTo As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Approval_Name " & vbCrLf & _
                      "  From ( " & vbCrLf & _
                      "  Select Distinct S.RFQ_Set From RFQ_Set S " & vbCrLf & _
                      "  Inner Join RFQ_Header H On S.RFQ_Set = H.RFQ_Set " & vbCrLf & _
                      "  Where RFQ_Status = '1' And CAST(RFQ_Date AS DATE) BETWEEN '" & pDateFrom & "' AND '" & pDateTo & "' " & vbCrLf & _
                      "  ) RH " & vbCrLf & _
                      "  Inner Join RFQ_Approval RA on RH.RFQ_Set = RA.RFQ_Set " & vbCrLf & _
                      "  inner join (Select * From Mst_ApprovalSetup Where Approval_Group = 'RFQ') AP on RA.Approval_ID = AP.Approval_ID  " & vbCrLf & _
                      "  Group By Approval_Name"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using



        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetList(pDateFrom As String, pDateTo As String, pStatus As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_RFQApproval_GetList"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQDateFrom", pDateFrom)
                cmd.Parameters.AddWithValue("RFQDateTo", pDateTo)
                cmd.Parameters.AddWithValue("Status", pStatus)
                cmd.Parameters.AddWithValue("UserID", pUser)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataRFQ(pRFQSetNo As String, pRevision As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                ' sql = "Select	S.RFQ_Set, H.RFQ_Number, S.Rev, S.RFQ_DueDate, S.RFQ_Date, RFQ_Status, PR_Number,  H.Supplier_Code, Supplier_Name " & vbCrLf & _
                      ' "From RFQ_Set S  " & vbCrLf & _
                      ' "Inner Join RFQ_Header H On S.RFQ_Set = H.RFQ_Set AND S.Rev = H.Rev " & vbCrLf & _
                      ' "Left Join Mst_Supplier R On R.Supplier_Code = H.Supplier_Code " & vbCrLf & _
                      ' "Where S.RFQ_Set = @RFQSetNo And S.Rev = @Revision "
				sql = "sp_RFQ_GetDataRFQApproval"
				
				
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSetNo)
                cmd.Parameters.AddWithValue("Revision", pRevision)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataDetailRFQ(pRFQSetNo As String, pRev As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "SELECT DISTINCT '0' AllowCheck, H.RFQ_Set, H.RFQ_Number, H.Rev, H.Supplier_Code,R.Supplier_Name,'Print Preview' as [Print] FROM RFQ_Approval A INNER JOIN  RFQ_Header H ON H.RFQ_Set = A.RFQ_Set AND H.Rev = A.Rev AND H.RFQ_Number = A.RFQ_Number  LEFT JOIN Mst_Supplier R ON R.Supplier_Code = H.Supplier_Code" & vbCrLf & _
                '      "Where ISNULL(A.Approval_Status,'') =''  and RFQ_Status <>'6' and  H.RFQ_Set = @RFQSetNo AND H.Rev = @Revision  "

                sql = "sp_RFQApproval_GetDataDetailRFQ"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSetNo)
                cmd.Parameters.AddWithValue("Revision", pRev)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="pClsRFQApproval"></param>
    ''' <param name="pUser"></param>
    ''' <param name="pErr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Void(ByVal pClsRFQApproval As clsRFQApproval, pUser As String, pRejectNotes As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_RFQVoid_Proc]"
                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNumber", pClsRFQApproval.RFQSetNumber)
                cmd.Parameters.AddWithValue("RFQNumber", pClsRFQApproval.RFQNumber)
                cmd.Parameters.AddWithValue("VoidNotes", pClsRFQApproval.ApproveNotes)
                cmd.Parameters.AddWithValue("UserId", pUser)


                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="pClsRFQApproval"></param>
    ''' <param name="pUser"></param>
    ''' <param name="pErr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Drop(ByVal pClsRFQApproval As clsRFQApproval, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Update RFQ_Header Set RFQ_Status = 5 Where RFQ_Number = @RFQNumber"
                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("RFQNumber", pClsRFQApproval.RFQNumber)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Approve(ByVal pClsRFQApproval As clsRFQApproval, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "[sp_RFQApproval_Approve]"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNumber", pClsRFQApproval.RFQSetNumber)
                cmd.Parameters.AddWithValue("RFQNumber", pClsRFQApproval.RFQNumber)
                cmd.Parameters.AddWithValue("ApprovalNotes", pClsRFQApproval.ApproveNotes)
                cmd.Parameters.AddWithValue("UserId", pUser)
                'cmd.Parameters.AddWithValue("Revision", pClsRFQApproval.Revision)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function CheckRFQApproval(pRFQSet As String, pRevision As String, userid As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_RFQApproval_checkApproval"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSet)
                cmd.Parameters.AddWithValue("rev", pRevision)
                cmd.Parameters.AddWithValue("UserID", userid)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

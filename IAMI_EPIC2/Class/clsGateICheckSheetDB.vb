﻿Imports System.Data.SqlClient

Public Class clsGateICheckSheetDB

#Region "Gate Header"
    Public Shared Function getHeaderGrid(_prjid As String, _grpid As String, _commodity As String, pic As String, pUser As String, _
                                        grpComodity As String, pStatus As String, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_Check_Sheet_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@PIC", pic)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpComodity)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertHeader(_prjid As String, _grpid As String, _commodity As String, _partno As String, _drwz As String, _
                                        pUser As String, pType As String, _
                                         Optional ByVal _qty As String = "0", Optional ByVal _new_guide_price As Double = 0, _
                                         Optional ByRef pErr As String = ""
                                        ) As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_Check_Sheet_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@PartNo", _partno)
                cmd.Parameters.AddWithValue("@Drawing_ISZ_SPLR", _drwz)
                cmd.Parameters.AddWithValue("@Qty", _qty)
                cmd.Parameters.AddWithValue("@New_Guide_Price", _new_guide_price)
                cmd.Parameters.AddWithValue("@Type", pType)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
#End Region

#Region "Gate Spec Information"

    Public Shared Function GetDateSpecInformation(_prjid As String, _grpid As String, _commodity As String, buyer As String, pUser As String, _
                                    grpCommodity As String, pStatus As String, _cls As clsGateSpecInformation, Optional ByVal pSource As String = "0", Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_GateSpecInformation_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpCommodity)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                cmd.Parameters.AddWithValue("@Source", pSource)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()

                _cls.Title = ds.Tables(0).Rows(0)("Title").ToString()
                _cls.BaseDrawing = ds.Tables(0).Rows(0)("Base_Drawing").ToString()
                _cls.SOR = ds.Tables(0).Rows(0)("SOR").ToString()
                _cls.Material = ds.Tables(0).Rows(0)("Material").ToString()
                _cls.Process = ds.Tables(0).Rows(0)("Process").ToString()
                _cls.CheckDevelopmentSchedule = ds.Tables(0).Rows(0)("Check_Development_Schedule").ToString()
                _cls.AdditionalInformation = ds.Tables(0).Rows(0)("Additional_Information").ToString()
                _cls.KSHAttendance = ds.Tables(0).Rows(0)("KSH_Attendance").ToString()

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDateSpecInformationHeaderToGrid(_prjid As String, _grpid As String, _commodity As String, buyer As String, pUser As String, _
                                    pStatus As String, _cls As clsGateSpecInformation, Optional ByRef pErr As String = "") As DataSet

        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_GateSpecInformation_HeadertoGrid", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertGateSpecInformation(_prjid As String, _grpid As String, _commodity As String, _basedrw As String, _sor As String, _
                                                     _mtrial As String, _prcs As String, _chkDevstatus As String, _addinfo As String, _kshattd As String, buyer As String, grpCommodity As String, pUser As String, _
                                                    pStatus As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_GateSpecInformation_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Base_Drawing", _basedrw)
                cmd.Parameters.AddWithValue("@SOR", _sor)
                cmd.Parameters.AddWithValue("@Material", _mtrial)
                cmd.Parameters.AddWithValue("@Process", _prcs)
                cmd.Parameters.AddWithValue("@Check_Development_Schedule", _chkDevstatus)
                cmd.Parameters.AddWithValue("@Additional_Information", _addinfo)
                cmd.Parameters.AddWithValue("@KSH_Attendance", _kshattd)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpCommodity)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function


    Public Shared Function GetDepartment(_department As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SpecInformation_GetDepartment", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Department_Code", _department)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetGridDepartment(_projid As String, _grpid As String, _comm As String, buyer As String, _department As String, pUser As String, _
                                 grpCommodity As String, pStatus As String, Optional ByVal pSource As String = "0", Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SpecInformation_Department_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _projid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpCommodity)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                cmd.Parameters.AddWithValue("@Source", pSource)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDepartment(_prjid As String, _grpid As String, _commodity As String, _department As String, _groupcommodity As String, buyer As String, _userid As String, pUser As String, _
                                                  pStatus As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SpecInformation_Department_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Department_Code", _department)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupcommodity)
                cmd.Parameters.AddWithValue("@UserId", _userid)
                cmd.Parameters.AddWithValue("@RegisterBy", pUser)
                cmd.Parameters.AddWithValue("@Buyer", buyer)

                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function

    Public Shared Function UpdateDepartment(_prjid As String, _grpid As String, _commodity As String, _Olddepartment As String, _groupcommodity As String, buyer As String, _Olduserid As String, pUser As String, _
                                                  pStatus As String, _newDept As String, _newUser As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SpecInformation_Department_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Department_Code", _Olddepartment)
                cmd.Parameters.AddWithValue("@Group_Comodity", _groupcommodity)
                cmd.Parameters.AddWithValue("@UserId", _Olduserid)
                cmd.Parameters.AddWithValue("@RegisterBy", pUser)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@NewDepartment_Code", _newDept)
                cmd.Parameters.AddWithValue("@NewUser_ID", _newUser)

                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function

    Public Shared Function DeleteDepartment(_prjid As String, _grpid As String, _commodity As String, _department As String, buyer As String, _userid As String, pUser As String, _
                                                grpCommodity As String, pStatus As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_SpecInformation_Department_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Department_Code", _department)
                cmd.Parameters.AddWithValue("@UserId", _userid)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpCommodity)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function

    Public Shared Function GetGridBidderList(_projid As String, _grpid As String, _comm As String, buyer As String, pUser As String, _
                              grpComodity As String, pStatus As String, Optional ByVal pSource As String = "0", Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_BiddersList_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _projid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpComodity)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                cmd.Parameters.AddWithValue("@Source", pSource)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertBiddersList(_prjid As String, _grpid As String, _commodity As String, buyer As String, _sequenceNo As Integer, _suppliercode As String, _
                                             _mainParts As String, _mainCustomer As String, _explanationMeetings As String, _remarks As String, grpCommodity As String, pUser As String, _
                                                 pStatus As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_BiddersList_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Sequence", _sequenceNo)
                cmd.Parameters.AddWithValue("@Supplier_Code", _suppliercode)
                cmd.Parameters.AddWithValue("@Main_Parts", _mainParts)
                cmd.Parameters.AddWithValue("@Main_Customer", _mainCustomer)
                cmd.Parameters.AddWithValue("@Explanation_meetings", IIf(String.IsNullOrEmpty(_explanationMeetings), "", _explanationMeetings))
                cmd.Parameters.AddWithValue("@Remarks", IIf(String.IsNullOrEmpty(_remarks), DBNull.Value, _remarks))
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpCommodity)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function


    Public Shared Function DeleteBiddersList(_prjid As String, _grpid As String, _commodity As String, _sequenceNo As Integer, buyer As String, pUser As String, _
                                              grpCommodity As String, pStatus As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_BiddersList_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Sequence", _sequenceNo)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpCommodity)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function


    Public Shared Function GetGridSourcingSchedule(_projid As String, _grpid As String, _comm As String, buyer As String, pUser As String, _
                              grpComodity As String, pStatus As String, Optional ByVal pSource As String = "0", Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_SourcingSchedule_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _projid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _comm)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpComodity)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                cmd.Parameters.AddWithValue("@Source", pSource)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertSourcingSchedule(_prjid As String, _grpid As String, _commodity As String, buyer As String, _codeactivity As String, _duedateplan As String, _
                                             _dateActual As String, _comment As String, pUser As String, _
                                               grpComodity As String, pStatus As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_SourcingSchedule_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Code_Activity", _codeactivity)
                cmd.Parameters.AddWithValue("@Due_Date_Plan", _duedateplan)
                cmd.Parameters.AddWithValue("@Date_Actual", _dateActual)
                cmd.Parameters.AddWithValue("@Comment", IIf(_comment = Nothing, "", _comment))
                cmd.Parameters.AddWithValue("@Update_By", pUser)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpComodity)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function

    Public Shared Function DeleteSourcingSchedule(_prjid As String, _grpid As String, _commodity As String, buyer As String, _codeactivity As String, pUser As String, _
                                              grpComodity As String, pStatus As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_SourcingSchedule_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Code_Activity", _codeactivity)
                cmd.Parameters.AddWithValue("@Buyer", buyer)
                cmd.Parameters.AddWithValue("@Group_Comodity", grpComodity)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function

    Public Shared Function CheckGateStatus(_prjid As String, _grpid As String, _comm As String, buyer As String, grpcomm As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_DrawingAcceptance_CheckGateStatus", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", IIf(String.IsNullOrEmpty(_prjid), "", _prjid))
                cmd.Parameters.AddWithValue("@Group_ID", IIf(String.IsNullOrEmpty(_grpid), "", _grpid))
                cmd.Parameters.AddWithValue("@Commodity", IIf(String.IsNullOrEmpty(_comm), "", _comm))
                cmd.Parameters.AddWithValue("@Buyer", IIf(String.IsNullOrEmpty(buyer), "", buyer))
                cmd.Parameters.AddWithValue("@Group_Comodity", grpcomm)
                con.Open()
                Dim rtn As String = cmd.ExecuteScalar().ToString()
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertGateStatus(_prjid As String, _grpid As String, _commodity As String, _GroupCommodity As String, _gateStatus As String, pUser As String, _
                                                pStatus As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_DrawingAcceptance_GateStatus", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Group_Commodity", _GroupCommodity)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                cmd.Parameters.AddWithValue("@type", _gateStatus)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function


    Public Shared Function InsertGateSheetApproval(_prjid As String, _grpid As String, _commodity As String, _GroupCommodity As String, pUser As String, _
                                               pStatus As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_Check_Sheet_Approval", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Group_Comodity", _GroupCommodity)
                cmd.Parameters.AddWithValue("@UserId", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function

    Public Shared Function FillComboProject(_type As String, projtype As String, prjid As String, grpid As String, comm As String, combofor As String, pUserID As String, pAdminStatus As String, _
                                     Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_ComboBoxStep", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@type", _type)
                cmd.Parameters.AddWithValue("@Project_Type", projtype)
                cmd.Parameters.AddWithValue("@Project_ID", prjid)
                cmd.Parameters.AddWithValue("@Group_ID", grpid)
                cmd.Parameters.AddWithValue("@Commodity", comm)
                cmd.Parameters.AddWithValue("@ComboFor", combofor)
                cmd.Parameters.AddWithValue("@UserId", pUserID)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetMainPartNCustomer(pSupplierCode As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_GetValueMainPartNCustomer", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertReleased(_prjid As String, _grpid As String, _commodity As String, _GroupCommodity As String, _buyer As String, _
                                          _Date As String, _StartTime As String, _FinishTime As String, _MeetingRoom As String, pUser As String, pStatus As String, _
                                          Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_Released_Submit", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Group_Commodity", _GroupCommodity)
                cmd.Parameters.AddWithValue("@Buyer", _buyer)
                cmd.Parameters.AddWithValue("@Date", _Date)
                cmd.Parameters.AddWithValue("@StartTime", _StartTime)
                cmd.Parameters.AddWithValue("@FinishTime", _FinishTime)
                cmd.Parameters.AddWithValue("@MeetingRoom", _MeetingRoom)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                cmd.Parameters.AddWithValue("@StatusRelease", pStatus)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function

    Public Shared Function CheckStatusRelease(_prjid As String, _grpid As String, _commodity As String, _GroupCommodity As String, _buyer As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_List_Released_CheckStatus", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Group_Commodity", _GroupCommodity)
                cmd.Parameters.AddWithValue("@Buyer", _buyer)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function CheckUserApprovalGateI(Optional ByRef pErr As String = "") As DataTable
        Dim dt As New DataTable
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_Confirm_GetUserApproval", con)
                cmd.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return dt

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function RejectReleased(_prjid As String, _grpid As String, _commodity As String, _GroupCommodity As String, _buyer As String, _
                                          _RejectNotes As String, pUser As String, _
                                          Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_Released_Reject", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", _prjid)
                cmd.Parameters.AddWithValue("@Group_ID", _grpid)
                cmd.Parameters.AddWithValue("@Commodity", _commodity)
                cmd.Parameters.AddWithValue("@Group_Commodity", _GroupCommodity)
                cmd.Parameters.AddWithValue("@Buyer", _buyer)
                cmd.Parameters.AddWithValue("@RejectNotes", _RejectNotes)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                Return Nothing
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return ex.Message
        End Try
    End Function

    Public Shared Function GetRateJPY_NewGuidePrice(pProjectID As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Gate_I_GetRate_New_Guide_Price", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ProjectID", pProjectID)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    '
    Public Shared Function CheckExistsData(pProjectID As String, pGroupID As String, pComm As String, pBuyer As String, _
                                           pGroupComm As String, pBaseDrawing As String, pSOR As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_GateSpecInformation_CheckExistsData", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Project_ID", pProjectID)
                cmd.Parameters.AddWithValue("@Group_ID", pGroupID)
                cmd.Parameters.AddWithValue("@Commodity", pComm)
                cmd.Parameters.AddWithValue("@Buyer", pBuyer)
                cmd.Parameters.AddWithValue("@GroupCommodity", pGroupComm)
                cmd.Parameters.AddWithValue("@BaseDrawing", pBaseDrawing)
                cmd.Parameters.AddWithValue("@SOR", pSOR)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
#End Region

End Class

﻿Imports System.Data.SqlClient

Public Class ClsSupplierDB

    Public Shared Function getlist(ByVal a As String, Optional ByRef perr As String = "") As List(Of ClsSupplier)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Supplier_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("ID", a)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsSupplier)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New ClsSupplier With {
                        .Supplier_Code = Trim(dt.Rows(i).Item("Supplier_Code") & ""),
                        .Supplier_Name = Trim(dt.Rows(i).Item("Supplier_Name") & ""),
                        .Address = Trim(dt.Rows(i).Item("Address") & ""),
                        .Phone = Trim(dt.Rows(i).Item("Phone") & ""),
                        .Fax = Trim(dt.Rows(i).Item("Fax") & ""),
                        .Kota_Kabupaten = Trim(dt.Rows(i).Item("Kota_Kabupaten") & ""),
                        .Country = Trim(dt.Rows(i).Item("Country") & ""),
                        .Contact_Person = Trim(dt.Rows(i).Item("Contact_Person") & ""),
                        .Email = Trim(dt.Rows(i).Item("Email") & ""),
                        .Period_Code = Trim(dt.Rows(i).Item("Period_Code") & ""),
                        .Language_Code = Trim(dt.Rows(i).Item("Language_Code") & ""),
                        .SupplierType = Trim(dt.Rows(i).Item("SupplierType") & ""),
                        .RegisterBy = Trim(dt.Rows(i).Item("RegisterBy") & ""),
                        .RegisterDate = Trim(dt.Rows(i).Item("RegisterDate") & ""),
                        .UpdateBy = Trim(dt.Rows(i).Item("UpdateBy") & ""),
                        .UpdateDate = Trim(dt.Rows(i).Item("UpdateDate") & "")
                        }
                    list.Add(ss)

                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pSes As ClsSupplier, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Supplier_Ins"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("SupplierCode", pSes.Supplier_Code)
                cmd.Parameters.AddWithValue("SupplierName", pSes.Supplier_Name)
                cmd.Parameters.AddWithValue("Address", pSes.Address)
                cmd.Parameters.AddWithValue("Phone", pSes.Phone)
                cmd.Parameters.AddWithValue("Fax", pSes.Fax)
                cmd.Parameters.AddWithValue("KotaKabupaten", pSes.Kota_Kabupaten)
                cmd.Parameters.AddWithValue("Country", pSes.Country)
                cmd.Parameters.AddWithValue("ContactPerson", pSes.Contact_Person)
                cmd.Parameters.AddWithValue("Email", pSes.Email)
                cmd.Parameters.AddWithValue("PeriodCode", pSes.Period_Code)
                cmd.Parameters.AddWithValue("LanguageCode", pSes.Language_Code)
                cmd.Parameters.AddWithValue("SupplierType", pSes.SupplierType)
                cmd.Parameters.AddWithValue("UserID", pSes.RegisterBy)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function Update(ByVal pSes As ClsSupplier, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Supplier_Upd"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("SupplierCode", pSes.Supplier_Code)                
                cmd.Parameters.AddWithValue("LanguageCode", pSes.Language_Code)                
                cmd.Parameters.AddWithValue("UserID", pSes.RegisterBy)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function Delete(ByVal pSes As ClsSupplier, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Supplier_Del"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("SupplierCode", pSes.Supplier_Code)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function isExist(ByVal pSupplierCode As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Supplier_Exist"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("SupplierCode", pSupplierCode)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count <> 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

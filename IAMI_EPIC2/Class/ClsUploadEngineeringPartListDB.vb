﻿Imports System.Data.SqlClient

Public Class ClsUploadEngineeringPartListDB
    Public Shared Function Insert(ByVal No As Integer, ByVal pProjectID As String, ByVal pVariant As String, ByVal UserID As String, Optional ByRef pErr As String = "") As Integer
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                cmd = New SqlCommand("sp_Engineering_Part_List_Variant_insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("No", No)
                cmd.Parameters.AddWithValue("Project_ID", pProjectID)
                cmd.Parameters.AddWithValue("Variant", pVariant)
                cmd.Parameters.AddWithValue("Register_By", UserID)

                i = cmd.ExecuteNonQuery
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function DeleteVariantAndEnggeneer(ByVal PProjectID As String, Optional ByRef pErr As String = "") As Integer
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                cmd = New SqlCommand("sp_Engineering_Part_List_Delete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", PProjectID)
                i = cmd.ExecuteNonQuery
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function SelCount(ByVal pProjectID As String, Optional ByRef pErr As String = "") As Boolean
        Dim cmd As SqlCommand

        SelCount = False
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                cmd = New SqlCommand("sp_Engineering_Part_List_Variant_SelCount", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", pProjectID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    SelCount = True
                End If
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function SelCompare(ByVal No As Integer, ByVal pProjectID As String, ByVal pVariant As String, Optional ByRef pErr As String = "") As Boolean
        Dim cmd As SqlCommand

        SelCompare = False
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                cmd = New SqlCommand("sp_Engineering_Part_List_Variant_SelExist", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("No", No)
                cmd.Parameters.AddWithValue("Project_ID", pProjectID)
                cmd.Parameters.AddWithValue("Variant", pVariant)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count > 0 Then
                    SelCompare = True
                End If
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

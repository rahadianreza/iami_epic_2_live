﻿Imports System.Data.SqlClient

Public Class clsAll_MaterialDB
    '****************************************
    '           MATERIAL RUBBER
    '****************************************
    Public Shared Function getListRubber(PeriodFrom As String, PeriodTo As String, MaterialType As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Rubber_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@PeriodFrom", PeriodFrom)
                cmd.Parameters.AddWithValue("@PeriodTo", PeriodTo)
                cmd.Parameters.AddWithValue("@MaterialType", MaterialType)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getDetailRubber(pMaterialType As String, pSupplierCode As String, pPeriod As String, pMaterialCode As String, _
                                           pGroupItem As String, pCategory As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Rubber_Detail_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@GroupItem", pGroupItem)
                cmd.Parameters.AddWithValue("@Category", pCategory)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function InsertDataRubber(pMaterialType As String, pSupplierCode As String, pPeriod As String, _
                                              pMaterialCode As String, pGroupItem As String, pCategory As String, pCurr As String, pPrice As Decimal, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Rubber_Insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)

                cmd.Parameters.AddWithValue("@GroupItem", pGroupItem)
                cmd.Parameters.AddWithValue("@Category", pCategory)
                cmd.Parameters.AddWithValue("@Price", pPrice)
                cmd.Parameters.AddWithValue("@CurrencyCode", pCurr)

                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateDataRubber(pMaterialType As String, pSupplierCode As String, pPeriod As String, _
                                              pMaterialCode As String, pGroupItem As String, pCategory As String, pCurr As String, pPrice As Decimal, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Rubber_Update", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)

                cmd.Parameters.AddWithValue("@GroupItem", pGroupItem)
                cmd.Parameters.AddWithValue("@Category", pCategory)
                cmd.Parameters.AddWithValue("@Price", pPrice)
                cmd.Parameters.AddWithValue("@CurrencyCode", pCurr)

                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteDataRubber(pMaterialType As String, pSupplier As String, pPeriod As String, _
                                             pMaterialCode As String, pGroup As String, pCategory As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Rubber_Delete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@Supplier", pSupplier)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@GroupItem", pGroup)
                cmd.Parameters.AddWithValue("@Category", pCategory)

                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    '***********************************************
    '               MATERIAL PLASTIC
    '***********************************************
    Public Shared Function getListPlastic(PeriodFrom As String, PeriodTo As String, MaterialType As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Plastic_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@PeriodFrom", PeriodFrom)
                cmd.Parameters.AddWithValue("@PeriodTo", PeriodTo)
                cmd.Parameters.AddWithValue("@MaterialType", MaterialType)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getDetailPlastic(pMaterialType As String, pSupplierCode As String, pPeriod As String, pMaterialCode As String, pGroupItem As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Plastic_Detail_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@GroupItemCode", pGroupItem)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function InsertDataPlastic(pMaterialType As String, pSupplierCode As String, pPeriod As String, _
                                              pMaterialCode As String, pGroupItem As String, pCurr As String, pPrice As Decimal, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Plastic_Insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)

                cmd.Parameters.AddWithValue("@GroupItem", pGroupItem)
                'cmd.Parameters.AddWithValue("@Category", pCategory)
                cmd.Parameters.AddWithValue("@Price", pPrice)
                cmd.Parameters.AddWithValue("@CurrencyCode", pCurr)

                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateDataPlastic(pMaterialType As String, pSupplierCode As String, pPeriod As String, _
                                              pMaterialCode As String, pGroupItem As String, pCurr As String, pPrice As Decimal, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Plastic_Update", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)

                cmd.Parameters.AddWithValue("@GroupItem", pGroupItem)
                'cmd.Parameters.AddWithValue("@Category", pCategory)
                cmd.Parameters.AddWithValue("@Price", pPrice)
                cmd.Parameters.AddWithValue("@CurrencyCode", pCurr)

                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteDataPlastic(pMaterialType As String, pSupplier As String, pPeriod As String, _
                                             pMaterialCode As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Plastic_Delete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@Supplier", pSupplier)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    '*********************************
    'MATERIAL LME
    '*********************************
    Public Shared Function getListLME(PeriodFrom As String, PeriodTo As String, MaterialType As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_LME_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@PeriodFrom", PeriodFrom)
                cmd.Parameters.AddWithValue("@PeriodTo", PeriodTo)
                cmd.Parameters.AddWithValue("@MaterialType", MaterialType)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getDetailLME(pMaterialType As String, pSupplierCode As String, pPeriod As String, _
                                        pMaterialCode As String, pGroupItem As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_LME_Detail_List", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@GroupItemCode", pGroupItem)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDataLME(pMaterialType As String, pSupplierCode As String, pPeriod As String, _
                                              pMaterialCode As String, pGroupItem As String, pCurr As String, pPrice As Decimal, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_LME_Insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)

                cmd.Parameters.AddWithValue("@GroupItem", pGroupItem)
                ' cmd.Parameters.AddWithValue("@Category", pCategory)
                cmd.Parameters.AddWithValue("@Price", pPrice)
                cmd.Parameters.AddWithValue("@CurrencyCode", pCurr)

                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateDataLME(pMaterialType As String, pSupplierCode As String, pPeriod As String, _
                                              pMaterialCode As String, pGroupItem As String, pCurr As String, pPrice As Decimal, _
                                              pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_LME_Update", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@SupplierCode", pSupplierCode)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)

                cmd.Parameters.AddWithValue("@GroupItem", pGroupItem)
                'cmd.Parameters.AddWithValue("@Category", pCategory)
                cmd.Parameters.AddWithValue("@Price", pPrice)
                cmd.Parameters.AddWithValue("@CurrencyCode", pCurr)

                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteDataLME(pMaterialType As String, pSupplier As String, pPeriod As String, _
                                             pMaterialCode As String, pGroupItem As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_LME_Delete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@MaterialType", pMaterialType)
                cmd.Parameters.AddWithValue("@Supplier", pSupplier)
                cmd.Parameters.AddWithValue("@Period", pPeriod)
                cmd.Parameters.AddWithValue("@MaterialCode", pMaterialCode)
                cmd.Parameters.AddWithValue("@GroupItemCode", pGroupItem)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    '*************************'
    'GET DESCRIPTION OF COMBO
    '*************************
    Public Shared Function GetDataCombo(pComboType As String, Optional ByVal pComboValue As String = "ALL", Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try

            Dim sQuery As String = "sp_Mst_Material_GetDataCombo"
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sQuery, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Type", pComboType)
                cmd.Parameters.AddWithValue("ComboValue", pComboValue)
                Dim da As New SqlDataAdapter(cmd)

                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

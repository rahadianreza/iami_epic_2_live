﻿Imports System.Data.SqlClient

Public Class ClsPurchasedPartDB
    Public Shared Function getlist(Optional ByRef perr As String = "") As List(Of ClsPurchasedPart)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = "sp_Purchased_Part_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsPurchasedPart)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New ClsPurchasedPart With {
                        .Purchased_Part_Code = Trim(dt.Rows(i).Item("Purchased_Part_Code") & ""),
                        .Purchased_Part_Name = Trim(dt.Rows(i).Item("Purchased_Part_Name") & ""),
                        .Specification = Trim(dt.Rows(i).Item("Specification") & ""),
                        .UoM = Trim(dt.Rows(i).Item("UoM") & ""),
                        .Price = Trim(dt.Rows(i).Item("Price") & ""),
                        .RegisterBy = Trim(dt.Rows(i).Item("Register_By") & ""),
                        .RegisterDate = dt.Rows(i).Item("Register_Date"),
                        .UpdateBy = Trim(dt.Rows(i).Item("Update_By") & ""),
                        .UpdateDate = dt.Rows(i).Item("Update_Date")
                        }
                        list.Add(ss)
                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(ByVal pSes As ClsPurchasedPart, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = "sp_Purchased_Part_Insert"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Purchased_Part_Name", pSes.Purchased_Part_Name)
                cmd.Parameters.AddWithValue("Specification", pSes.Specification)
                cmd.Parameters.AddWithValue("UoM", pSes.UoM)
                cmd.Parameters.AddWithValue("Price", pSes.Price)
                cmd.Parameters.AddWithValue("UserID", pSes.RegisterBy)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function
    Public Shared Function Update(ByVal pSes As ClsPurchasedPart, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = "sp_Purchased_Part_Update"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Purchased_Part_Code", pSes.Purchased_Part_Code)
                cmd.Parameters.AddWithValue("Purchased_Part_Name", pSes.Purchased_Part_Name)
                cmd.Parameters.AddWithValue("Specification", pSes.Specification)
                cmd.Parameters.AddWithValue("UoM", pSes.UoM)
                cmd.Parameters.AddWithValue("Price", pSes.Price)
                cmd.Parameters.AddWithValue("UserID", pSes.RegisterBy)

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function
    Public Shared Function Delete(ByVal pSes As ClsPurchasedPart, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = "sp_Purchased_Part_Delete"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Purchased_Part_Code", pSes.Purchased_Part_Code)
                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function
    Public Shared Function isExist(ByVal Purchased_Part_Name As String, Optional ByRef pErr As String = "") As Boolean
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Purchased_Part_Exist"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Purchased_Part_Name", Purchased_Part_Name)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                If ds.Tables(0).Rows.Count <> 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

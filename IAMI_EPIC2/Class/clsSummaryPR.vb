﻿Public Class clsSummaryPR
    Public Property Department As String
    Public Property Section As String
    Public Property Group As String
    Public Property PRNo As String
    Public Property NoIA As String
    Public Property Description As String
    Public Property Specification As String
    Public Property Qty As Double
    Public Property UOM As String
    Public Property TotalAmount As Double
End Class

﻿Public Class clsGateSpecInformation
    Public Property Title As String
    Public Property BaseDrawing As String
    Public Property SOR As String
    Public Property Material As String
    Public Property Process As String
    Public Property CheckDevelopmentSchedule As String
    Public Property AdditionalInformation As String
    Public Property KSHAttendance As String
End Class

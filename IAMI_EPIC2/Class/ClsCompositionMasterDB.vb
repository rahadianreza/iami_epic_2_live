﻿
Imports System.Data.SqlClient

Public Class ClsCompositionMasterDB

    Public Shared Function GenerateNewMaterialNo(Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String

                'sql = "Select 'MTRL' + Right(Isnull(Right(Max(Material_No),6),0) + 1000001,6)  As Material_No From Mst_Item"
                sql = "sp_GetMaxSequenceNoCompositionHeader"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataForExcel(pGroup As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Dim sql As String

                sql = "SELECT 	CompositionSequenceNo,	ParentItemCode,	''ParentItemDescs,	ParentItemQty,	ParentItemUOM,	RegisterUser,	RegisterDate,	UpdateUser,	UpdateDate FROM dbo.Mst_CompositionHeader"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    '1
    Public Shared Function GetList(Optional ByRef pErr As String = "") As List(Of ClsCompositionMaster)
        Dim sql As String
        Dim cmd As SqlCommand

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "sp_CompositionHeader_GetList"
                sql = "sp_Mst_Composition_Material_GetList"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)

                Dim list As New List(Of ClsCompositionMaster)
                For i = 0 To dt.Rows.Count - 1
                    Dim updatedate As String
                    If IsDBNull(dt.Rows(i).Item("UpdateDate")) Then
                        updatedate = ""
                    Else
                        updatedate = dt.Rows(i).Item("UpdateDate")
                    End If

                    Dim itemdata As New ClsCompositionMaster With {.CompositionSequenceNo = dt.Rows(i).Item("CompositionSequenceNo"),
                                                          .ParentItemCode = Trim(dt.Rows(i).Item("ParentItemCode") & ""),
                                                          .ParentItemDescs = Trim(dt.Rows(i).Item("ParentItemDescs") & ""),
                                                          .ParentItemQty = Trim(dt.Rows(i).Item("ParentItemQty") & ""),
                                                          .ParentItemUOM = Trim(dt.Rows(i).Item("ParentItemUOM") & ""),
                                                          .CreateDate = IIf(IsDBNull(dt.Rows(i).Item("RegisterDate")), "", dt.Rows(i).Item("RegisterDate")),
                                                          .CreateUser = dt.Rows(i).Item("RegisterUser"),
                                                          .UpdateUser = IIf(IsDBNull(dt.Rows(i).Item("UpdateUser")), "", dt.Rows(i).Item("UpdateUser")),
                                                          .UpdateDate = IIf(IsDBNull(dt.Rows(i).Item("UpdateDate")), "", dt.Rows(i).Item("UpdateDate"))}

                    list.Add(itemdata)
                Next

                Return list
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataUOM(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As UOM From Mst_Parameter Where Par_Group = 'UOM'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataItemParent(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Par_Code As MaterialCategory From Mst_Parameter Where Par_Group = 'MaterialCategory'"
                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataItem(ByVal pObj As ClsCompositionMaster, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = " SELECT 	CompositionSequenceNo,	ParentItemCode,	''ParentItemDescs,	ParentItemQty,	ParentItemUOM,	RegisterUser,	RegisterDate,	UpdateUser,	UpdateDate FROM dbo.Mst_CompositionHeader" & vbCrLf

                If pObj.CompositionSequenceNo <> 0 Then
                    sql = sql + " where CompositionSequenceNo = @CompositionSequenceNo "
                End If
                Dim cmd As New SqlCommand(sql, con)
                If pObj.CompositionSequenceNo <> 0 Then
                    cmd.Parameters.AddWithValue("@CompositionSequenceNo", pObj.CompositionSequenceNo)
                End If
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                If pObj.CompositionSequenceNo <> 0 Then
                    cmd.Parameters.Clear()
                End If
                cmd.Dispose()
            End Using

            Return ds
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try

    End Function

    Public Shared Function Insert(ByVal pItem As ClsCompositionMaster, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Ins]"

                sql = "INSERT INTO Mst_CompositionHeader  " & vbCrLf & _
                      "(CompositionSequenceNo,	ParentItemCode,	ParentItemQty,	ParentItemUOM,	RegisterDate,RegisterUser "


                sql = sql + " ) VALUES  " & vbCrLf & _
                      "(@CompositionSequenceNo,	@ParentItemCode,@ParentItemQty,	@ParentItemUOM,GETDATE(), UPPER(@UserId) "


                sql = sql + ") "
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                'cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("CompositionSequenceNo", pItem.CompositionSequenceNo)
                cmd.Parameters.AddWithValue("ParentItemCode", pItem.ParentItemCode)
                cmd.Parameters.AddWithValue("ParentItemQty", pItem.ParentItemQty)
                cmd.Parameters.AddWithValue("ParentItemUOM", pItem.ParentItemUOM)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Update(ByVal pItem As ClsCompositionMaster, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Upd]"
                sql = "UPDATE Mst_CompositionHeader " & vbCrLf & _
                      "SET CompositionSequenceNo = @CompositionSequenceNo,	ParentItemCode = @ParentItemCode,ParentItemQty = @ParentItemQty,ParentItemUOM = @ParentItemUOM,UpdateUser = UPPER(@UserId), UpdateDate = GETDATE() "


                sql = sql + "WHERE CompositionSequenceNo = @CompositionSequenceNo"
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("CompositionSequenceNo", pItem.CompositionSequenceNo)
                cmd.Parameters.AddWithValue("ParentItemCode", pItem.ParentItemCode)
                cmd.Parameters.AddWithValue("ParentItemQty", pItem.ParentItemQty)
                cmd.Parameters.AddWithValue("ParentItemUOM", pItem.ParentItemUOM)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Delete(ByVal pItem As ClsCompositionMaster, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Del]"
                sql = "Delete From Mst_CompositionHeader Where CompositionSequenceNo = '" & pItem.CompositionSequenceNo & "'"
                cmd = New SqlCommand(sql, con)

                'cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("CompositionSequenceNo", pItem.CompositionSequenceNo)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    ' --------------------------------------------Detail------------------------------------------------------

    Public Shared Function Detail_Getlist(ByVal SeqNo As String, Optional ByRef perr As String = "") As List(Of ClsCompositionMaster)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_CompositionDetail_Gridload"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CompositionSequenceNo", SeqNo)
               

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsCompositionMaster)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New ClsCompositionMaster With {
                        .ChildCompositionSequenceNo = dt.Rows(i).Item("DetailCompositionSequenceNo"),
                        .ChildItemCode = Trim(dt.Rows(i).Item("ChildItemCode") & ""),
                        .ChildItemQty = dt.Rows(i).Item("ChildItemQty"),
                        .ChildItemUOM = Trim(dt.Rows(i).Item("ChildItemUOM") & ""),
                        .CurrencyCode = Trim(dt.Rows(i).Item("CurrencyCode") & ""),
                        .Price = dt.Rows(i).Item("Price"),
                        .CreateUser = Trim(dt.Rows(i).Item("RegisterUser") & ""),
                        .CreateDate = Trim(dt.Rows(i).Item("RegisterDate") & ""),
                        .UpdateUser = Trim(dt.Rows(i).Item("UpdateUser") & ""),
                        .UpdateDate = Trim(dt.Rows(i).Item("UpdateDate") & "")
                        }
                        list.Add(ss)
                  
                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function InsertDetail(ByVal pSes As ClsCompositionMaster, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_CompositionDetail_Ins"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CompositionSequenceNo", pSes.CompositionSequenceNo)
                cmd.Parameters.AddWithValue("ParentItemCode", pSes.ParentItemCode)
                cmd.Parameters.AddWithValue("ChildItemCode", pSes.ChildItemCode)
                cmd.Parameters.AddWithValue("ChildItemQty", pSes.ChildItemQty)
                cmd.Parameters.AddWithValue("CurrencyCode", pSes.CurrencyCode)
                cmd.Parameters.AddWithValue("ChildItemUOM", pSes.ChildItemUOM)
                cmd.Parameters.AddWithValue("Price", pSes.Price)
                cmd.Parameters.AddWithValue("RegisterUser", pSes.CreateUser)
              

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function


    Public Shared Function UpdateDetail(ByVal pSes As ClsCompositionMaster, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_CompositionDetail_Upd"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CompositionSequenceNo", pSes.CompositionSequenceNo)
                cmd.Parameters.AddWithValue("ChildCompositionSequenceNo", pSes.ChildCompositionSequenceNo)
                cmd.Parameters.AddWithValue("ParentItemCode", pSes.ParentItemCode)
                cmd.Parameters.AddWithValue("ChildItemCode", pSes.ChildItemCode)
                cmd.Parameters.AddWithValue("ChildItemQty", pSes.ChildItemQty)
                cmd.Parameters.AddWithValue("CurrencyCode", pSes.CurrencyCode)
                cmd.Parameters.AddWithValue("ChildItemUOM", pSes.ChildItemUOM)
                cmd.Parameters.AddWithValue("Price", pSes.Price)
                cmd.Parameters.AddWithValue("UserID", pSes.CreateUser)


                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

    Public Shared Function DeleteDetail(ByVal pSes As ClsCompositionMaster, Optional ByRef pErr As String = "") As Integer
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_CompositionDetail_Del"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("CompositionSequenceNo", pSes.CompositionSequenceNo)
                cmd.Parameters.AddWithValue("ChildCompositionSequenceNo", pSes.ChildCompositionSequenceNo)
                cmd.Parameters.AddWithValue("ParentItemCode", pSes.ParentItemCode)
             

                Dim rtn As Integer = cmd.ExecuteNonQuery
                Return rtn
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try
    End Function

End Class

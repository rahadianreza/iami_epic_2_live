﻿Public Class ClsMasterPartVariant
    Private _Type As String
    Private _VariantCode As String
    Private _VariantName As String
    Private _MaterialFUSAP As String
    Private _PartNo As String
    Private _PartName As String
    Private _Qty As Double
    Private _RegisterDate As String
    Private _RegisterUser As String
    Private _UpdateDate As String
    Private _UpdateUser As String
    Private _User As String

    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal value As String)
            _Type = value
        End Set
    End Property

    Public Property VariantCode() As String
        Get
            Return _VariantCode
        End Get
        Set(ByVal value As String)
            _VariantCode = value
        End Set
    End Property

    Public Property VariantName() As String
        Get
            Return _VariantName
        End Get
        Set(ByVal value As String)
            _VariantName = value
        End Set
    End Property

    Public Property MaterialFUSAP() As String
        Get
            Return _MaterialFUSAP
        End Get
        Set(ByVal value As String)
            _MaterialFUSAP = value
        End Set
    End Property

    Public Property PartNo() As String
        Get
            Return _PartNo
        End Get
        Set(ByVal value As String)
            _PartNo = value
        End Set
    End Property

    Public Property PartName() As String
        Get
            Return _PartName
        End Get
        Set(ByVal value As String)
            _PartName = value
        End Set
    End Property

    Public Property Qty() As Double
        Get
            Return _Qty
        End Get
        Set(ByVal value As Double)
            _Qty = value
        End Set
    End Property

    Public Property RegisterDate() As String
        Get
            Return _RegisterDate
        End Get
        Set(ByVal value As String)
            _RegisterDate = value
        End Set
    End Property

    Public Property RegisterUser() As String
        Get
            Return _RegisterUser
        End Get
        Set(ByVal value As String)
            _RegisterUser = value
        End Set
    End Property

    Public Property UpdateDate() As String
        Get
            Return _UpdateDate
        End Get
        Set(ByVal value As String)
            _UpdateDate = value
        End Set
    End Property

    Public Property UpdateUser() As String
        Get
            Return _UpdateUser
        End Get
        Set(ByVal value As String)
            _UpdateUser = value
        End Set
    End Property

    Public Property User() As String
        Get
            Return _User
        End Get
        Set(ByVal value As String)
            _User = value
        End Set
    End Property
End Class

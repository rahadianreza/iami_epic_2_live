﻿Imports System.Data.SqlClient

Public Class clsPasswordHistoryDB
    Public Shared Function GetLastData(ByVal pUserID As String) As clsPasswordHistory
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "select top 1 * from JSOX_PasswordHistory where UserID = @UserID order by UpdateDate desc"

                Dim cmd As New SqlCommand(sql, conn)
                cmd.Parameters.AddWithValue("UserID", pUserID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                If dt.Rows.Count = 0 Then
                    Return Nothing
                Else
                    With dt.Rows(0)
                        Dim History As New clsPasswordHistory
                        History.UserID = pUserID
                        History.SeqNo = .Item("SeqNo")
                        History.UpdateDate = .Item("UpdateDate")
                        History.Password = .Item("Password")
                        Return History
                    End With
                End If
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function Insert(History As clsPasswordHistory) As Integer
        Using Cn As New SqlConnection(Sconn.Stringkoneksi)
            Cn.Open()
            Dim q As String = "Insert into JSOX_PasswordHistory (AppID, UserID, SeqNo, UpdateDate, Password) values " & vbCrLf &
                "('EAT', @UserID, (select isnull(max(SeqNo), 0) + 1 from JSOX_PasswordHistory where UserID = @UserID), " & vbCrLf &
                "GetDate(), @Password)"
            Dim cmd As New SqlCommand(q, Cn)
            cmd.Parameters.AddWithValue("UserID", History.UserID)
            cmd.Parameters.AddWithValue("Password", History.Password)
            Dim i As Integer = cmd.ExecuteNonQuery
            Return i
        End Using
    End Function
End Class

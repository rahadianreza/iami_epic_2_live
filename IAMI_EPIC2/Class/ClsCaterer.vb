﻿Public Class ClsCaterer
    Public Property CatererID As String
    Public Property CatererName As String
    Public Property MenuID As String
    Public Property Address As String
    Public Property Phone1 As String
    Public Property Phone2 As String
    Public Property Fax As String
    Public Property Email As String
    Public Property GSTNo As String
    Public Property ContactPerson As String
    Public Property CompanyRegistrationNo As String
    Public Property ServedLocationID As String
    Public Property ActiveStatus As String
    Public Property CreateDate As String
    Public Property CreateUser As String
    Public Property UpdateDate As String
    Public Property UpdateUser As String
	
End Class

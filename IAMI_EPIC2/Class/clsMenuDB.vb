﻿Imports System.Data
Imports System.Data.SqlClient

Public Class clsMenuDB
    Public Shared Function Insert(Menu As clsMenu) As Integer
        Using Cn As New SqlConnection(Sconn.Stringkoneksi)
            Cn.Open()
            Dim q As String
            q = "Select isnull(max(MenuID), '') from Menu where MenuID like 'M%' and len(MenuID) = 5 "
            Dim cmdno As New SqlCommand(q, Cn)
            Dim LastNo As String = cmdno.ExecuteScalar
            LastNo = Mid(Trim(LastNo), 2, 4)
            LastNo = Val(LastNo) + 1
            Menu.MenuID = "M" & LastNo.PadLeft(4, "0")
            q = "Insert into Menu (" & vbCrLf &
                "CatererID, MenuID, MenuName, Calorie, Price, MenuCategory, TaxCode, Description, " & vbCrLf &
                "MenuType, ActiveStatus, RecommendedStatus, SpicyStatus, CreateDate, CreateUser " & vbCrLf
            If Menu.Picture IsNot Nothing Then
                q = q & ", Picture "
            End If
            q = q &
                ") Values ( " & vbCrLf &
                "@CatererID, @MenuID, @MenuName, @Calorie, @Price, @MenuCategory, @TaxCode, @Description, " & vbCrLf &
                "@MenuType, @ActiveStatus, @RecommendedStatus, @SpicyStatus, GetDate(), UPPER(@CreateUser) "
            If Menu.Picture IsNot Nothing Then
                q = q & ", @Picture " & vbCrLf
            End If
            q = q & ") "
            Menu.MenuName = StrConv(Menu.MenuName, VbStrConv.ProperCase)
            Dim cmd As New SqlCommand(q, Cn)
            cmd.Parameters.AddWithValue("CatererID", Menu.CatererID)
            cmd.Parameters.AddWithValue("MenuID", Menu.MenuID)
            cmd.Parameters.AddWithValue("MenuName", Menu.MenuName)
            cmd.Parameters.AddWithValue("Calorie", Menu.Calorie)
            cmd.Parameters.AddWithValue("Price", Menu.Price)
            cmd.Parameters.AddWithValue("MenuCategory", Menu.MenuCategory)
            cmd.Parameters.AddWithValue("TaxCode", Menu.TaxCode)
            cmd.Parameters.AddWithValue("Description", Menu.Description)
            cmd.Parameters.AddWithValue("MenuType", Menu.MenuType)
            cmd.Parameters.AddWithValue("RecommendedStatus", Menu.RecommendedStatus)
            cmd.Parameters.AddWithValue("SpicyStatus", Menu.SpicyStatus)
            cmd.Parameters.AddWithValue("ActiveStatus", Menu.ActiveStatus)
            cmd.Parameters.AddWithValue("CreateUser", Menu.CreateUser)
            If Menu.Picture IsNot Nothing Then
                cmd.Parameters.AddWithValue("Picture", Menu.Picture)
            End If
            Dim i As Integer = cmd.ExecuteNonQuery
            Return i
        End Using
    End Function

    Public Shared Function Update(Menu As clsMenu) As Integer
        Using Cn As New SqlConnection(Sconn.Stringkoneksi)
            Cn.Open()
            Dim q As String = "Update Menu set " & vbCrLf &
                "MenuName = @MenuName, Calorie = @Calorie, Price = @Price, MenuCategory = @MenuCategory, TaxCode = @TaxCode, Description = @Description, " & vbCrLf &
                "MenuType = @MenuType, ActiveStatus = @ActiveStatus, RecommendedStatus = @RecommendedStatus, SpicyStatus = @SpicyStatus, " & vbCrLf & _
                "UpdateDate = GetDate(), UpdateUser = UPPER(@UpdateUser) "
            If Menu.Picture IsNot Nothing Then
                q = q & ", Picture = @Picture " & vbCrLf
            End If

            q = q & "where CatererID = @CatererID and MenuID = @MenuID " & vbCrLf
            Dim cmd As New SqlCommand(q, Cn)
            Menu.MenuName = StrConv(Menu.MenuName, VbStrConv.ProperCase)
            cmd.Parameters.AddWithValue("CatererID", Menu.CatererID)
            cmd.Parameters.AddWithValue("MenuID", Menu.MenuID)
            cmd.Parameters.AddWithValue("MenuName", Menu.MenuName)
            cmd.Parameters.AddWithValue("Calorie", Menu.Calorie)
            cmd.Parameters.AddWithValue("Price", Menu.Price)
            cmd.Parameters.AddWithValue("MenuCategory", Menu.MenuCategory)
            cmd.Parameters.AddWithValue("TaxCode", Menu.TaxCode)
            cmd.Parameters.AddWithValue("Description", Menu.Description)
            cmd.Parameters.AddWithValue("MenuType", Menu.MenuType)
            cmd.Parameters.AddWithValue("RecommendedStatus", Menu.RecommendedStatus)
            cmd.Parameters.AddWithValue("SpicyStatus", Menu.SpicyStatus)
            cmd.Parameters.AddWithValue("ActiveStatus", Menu.ActiveStatus)
            cmd.Parameters.AddWithValue("UpdateUser", Menu.CreateUser)
            If Menu.Picture IsNot Nothing Then
                cmd.Parameters.AddWithValue("Picture", Menu.Picture)
            End If
            Dim i As Integer = cmd.ExecuteNonQuery
            Return i
        End Using
    End Function

    Public Shared Function Delete(CatererID As String, MenuID As String) As Integer
        Using Cn As New SqlConnection(Sconn.Stringkoneksi)
            Cn.Open()
            Dim q As String = "Delete Menu " & vbCrLf &
                "where CatererID = @CatererID and MenuID = @MenuID " & vbCrLf
            Dim cmd As New SqlCommand(q, Cn)
            cmd.Parameters.AddWithValue("CatererID", CatererID)
            cmd.Parameters.AddWithValue("MenuID", MenuID)
            Dim i As Integer = cmd.ExecuteNonQuery
            Return i
        End Using
    End Function

    Public Shared Function ClearImage(CatererID As String, MenuID As String) As Integer
        Using Cn As New SqlConnection(Sconn.Stringkoneksi)
            Cn.Open()
            Dim q As String = "Update Menu set Picture = NULL " & vbCrLf &
                "where CatererID = @CatererID and MenuID = @MenuID " & vbCrLf
            Dim cmd As New SqlCommand(q, Cn)
            cmd.Parameters.AddWithValue("CatererID", CatererID)
            cmd.Parameters.AddWithValue("MenuID", MenuID)
            Dim i As Integer = cmd.ExecuteNonQuery
            Return i
        End Using
    End Function

    Public Shared Function GetList(CatererID As String, MenuType As String, ActiveStatus As String) As List(Of clsMenu)
        Using Cn As New SqlConnection(Sconn.Stringkoneksi)
            Cn.Open()
            Dim q As String = "Select * from Menu where CatererID = @CatererID " & vbCrLf
            If MenuType <> "" Then
                q = q & "and isnull(MenuType, 0) = @MenuType" & vbCrLf
            End If
            If ActiveStatus = "1" Then
                q = q & "and isnull(ActiveStatus, 0) = 1 " & vbCrLf
            ElseIf ActiveStatus = "2" Then
                q = q & "and isnull(ActiveStatus, 0) = 0 " & vbCrLf
            End If
            Dim cmd As New SqlCommand(q, Cn)
            Dim da As New SqlDataAdapter(cmd)
            cmd.Parameters.AddWithValue("CatererID", CatererID)
            If MenuType <> "" Then
                cmd.Parameters.AddWithValue("MenuType", Val(MenuType))
            End If
            If ActiveStatus <> "" Then
                cmd.Parameters.AddWithValue("ActiveStatus", Val(ActiveStatus))
            End If
            Dim dt As New DataTable
            da.Fill(dt)
            Dim MenuList As New List(Of clsMenu)
            For i = 0 To dt.Rows.Count - 1
                Dim Menu As New clsMenu
                With dt.Rows(i)
                    Menu.CatererID = .Item("CatererID").ToString.Trim
                    Menu.MenuID = .Item("MenuID").ToString.Trim
                    Menu.MenuName = StrConv(.Item("MenuName").ToString.Trim, VbStrConv.ProperCase)
                    Menu.Calorie = .Item("Calorie").ToString.Trim
                    Menu.Price = .Item("Price")
                    Menu.MenuCategory = Val(.Item("MenuCategory") & "")
                    Menu.TaxCode = .Item("TaxCode").ToString.Trim
                    Menu.Description = .Item("Description").ToString.Trim
                    Menu.ActiveStatus = .Item("ActiveStatus") & ""
                    Menu.RecommendedStatus = .Item("RecommendedStatus") & ""
                    Menu.SpicyStatus = .Item("SpicyStatus") & ""
                    Menu.MenuType = Val(.Item("MenuType") & "")
                    Menu.CreateUser = .Item("CreateUser").ToString.Trim
                    Menu.Picture = .Item("Picture")
                End With
                MenuList.Add(Menu)
            Next
            Return MenuList
        End Using
    End Function
End Class

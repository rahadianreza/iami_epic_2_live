﻿Imports System.Data.SqlClient

Public Class ClsCPListDB
    Public Shared Function GetData(ByVal pCPList As ClsCPList, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_CPList_Sel"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("DtFrom", pCPList.QuotationDateFrom)
                cmd.Parameters.AddWithValue("DtTo", pCPList.QuotationDateTo)
                cmd.Parameters.AddWithValue("Supplier", pCPList.Supplier)
                cmd.Parameters.AddWithValue("Counter", pCPList.CounterProposal)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetCPHeaderData(pCPNumber As String, pRevision As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_CP_List_Tender_GetDataHeader '" & pCPNumber & "' ,'" & pRevision & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function resendInvitationMeeting(ByVal CPList As ClsCPList, Optional ByRef pErr As String = "") As Integer
        Try
            pErr = ""
            Using cn As New SqlConnection(Sconn.Stringkoneksi)
                cn.Open()
                Dim sql As String = "sp_CP_InvitationMeeting_Resend @CP_Number,@Rev,@Supplier_Code"
                Dim Cmd As New SqlCommand(sql, cn)
                With Cmd.Parameters
                    .AddWithValue("CP_Number", CPList.CounterProposalCode)
                    .AddWithValue("Rev", CPList.Rev)
                    .AddWithValue("Supplier_Code", CPList.SupplierCode)
                End With
                Return Cmd.ExecuteNonQuery
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try

    End Function

    Public Shared Function InvitationMeeting(ByVal CPList As ClsCPList, Optional ByRef pErr As String = "") As Integer
        Try
            pErr = ""
            Using cn As New SqlConnection(Sconn.Stringkoneksi)
                cn.Open()
                Dim sql As String = "exec sp_CP_InvitationMeetingSubmit @CP_Number,@Rev,@Supplier_Code,@Invitation_Date ,@Invitation_Time,@Location_Meeting "
                Dim Cmd As New SqlCommand(sql, cn)
                With Cmd.Parameters
                    .AddWithValue("CP_Number", CPList.CounterProposalCode)
                    .AddWithValue("Rev", CPList.Rev)
                    .AddWithValue("Supplier_Code", CPList.SupplierCode)
                    .AddWithValue("Invitation_Date", CPList.Invitation_Date)
                    .AddWithValue("Invitation_Time", CPList.Invitation_Time)
                    .AddWithValue("Location_Meeting", CPList.Location_Meeting)
                End With
                Return Cmd.ExecuteNonQuery
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return 0
        End Try

    End Function

    Public Shared Sub CPInsertHeader(ByVal pCPNumber As String, pRev As String, pCENumber As String, pInvitDate As String, _
                                     ByVal pNotes As String, pUser As String, pInvitTime As String, pTenderLoc As String, Optional ByRef pCPNumber_New As String = "", Optional ByRef pErr As String = "")
        Dim sql As String
        Dim ds As New DataSet

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Using sqlTran As SqlTransaction = con.BeginTransaction()
                    sql = "sp_CP_Insert_Header"

                    Dim sqlComm As New SqlCommand(sql, con)
                    sqlComm.CommandType = CommandType.StoredProcedure
                    sqlComm.Parameters.AddWithValue("@CPNumber", pCPNumber)
                    sqlComm.Parameters.AddWithValue("@Rev", pRev)
                    sqlComm.Parameters.AddWithValue("@CENumber", pCENumber)
                    sqlComm.Parameters.AddWithValue("@InvitationDate", pInvitDate)
                    sqlComm.Parameters.AddWithValue("@CPStatus", "0")
                    sqlComm.Parameters.AddWithValue("@Notes", pNotes)
                    sqlComm.Parameters.AddWithValue("@RegUser", pUser)
                    sqlComm.Parameters.AddWithValue("@Invitation_Time", pInvitTime)
                    sqlComm.Parameters.AddWithValue("@Tender_Location", pTenderLoc)


                    Dim outPutParameter As SqlParameter = New SqlParameter()
                    outPutParameter.ParameterName = "@CPNumber_Output"
                    outPutParameter.SqlDbType = System.Data.SqlDbType.VarChar
                    outPutParameter.Direction = System.Data.ParameterDirection.Output
                    outPutParameter.Size = 50
                    sqlComm.Parameters.Add(outPutParameter)
                    sqlComm.ExecuteNonQuery()
                    pCPNumber_New = outPutParameter.Value.ToString()
                    sqlComm.Dispose()

                    sqlTran.Commit()
                End Using

            End Using

        Catch ex As Exception
            pErr = ex.Message
        End Try
    End Sub

    Public Shared Sub CPUpdateHeader(ByVal pCPNumber As String, pRev As String, pCENumber As String, pInvitDate As String, _
                                    ByVal pNotes As String, pUser As String, pInvitTime As String, pTenderLoc As String, Optional ByRef pErr As String = "")
        Dim sql As String
        Dim ds As New DataSet

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                Using sqlTran As SqlTransaction = con.BeginTransaction()
                    sql = "sp_CP_Update_Header"

                    Dim sqlComm As New SqlCommand(sql, con)
                    sqlComm.CommandType = CommandType.StoredProcedure
                    sqlComm.Parameters.AddWithValue("@CPNumber", pCPNumber)
                    sqlComm.Parameters.AddWithValue("@Rev", pRev)
                    sqlComm.Parameters.AddWithValue("@CENumber", pCENumber)
                    sqlComm.Parameters.AddWithValue("@InvitationDate", pInvitDate)
                    sqlComm.Parameters.AddWithValue("@CPStatus", "0")
                    sqlComm.Parameters.AddWithValue("@Notes", pNotes)
                    sqlComm.Parameters.AddWithValue("@RegUser", pUser)
                    sqlComm.Parameters.AddWithValue("@Invitation_Time", pInvitTime)
                    sqlComm.Parameters.AddWithValue("@Tender_Location", pTenderLoc)

                    sqlComm.Dispose()
                    sqlTran.Commit()
                End Using

            End Using

        Catch ex As Exception
            pErr = ex.Message
        End Try
    End Sub
End Class

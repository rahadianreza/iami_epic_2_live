﻿Public Class ClsMasterBudgetInflation
    Private _SupplierCode As String
    Private _SupplierName As String
    Private _Type As String
    Private _VariantCode As String
    Private _VariantName As String
    Private _Material As Double
    Private _Process As Double
    Private _RegisterDate As String
    Private _RegisterUser As String
    Private _UpdateDate As String
    Private _UpdateUser As String
    Private _User As String

    Public Property SupplierCode() As String
        Get
            Return _SupplierCode
        End Get
        Set(ByVal value As String)
            _SupplierCode = value
        End Set
    End Property

    Public Property SupplierName() As String
        Get
            Return _SupplierName
        End Get
        Set(ByVal value As String)
            _SupplierName = value
        End Set
    End Property

    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal value As String)
            _Type = value
        End Set
    End Property

    Public Property VariantCode() As String
        Get
            Return _VariantCode
        End Get
        Set(ByVal value As String)
            _VariantCode = value
        End Set
    End Property

    Public Property VariantName() As String
        Get
            Return _VariantName
        End Get
        Set(ByVal value As String)
            _VariantName = value
        End Set
    End Property

    Public Property Material() As Double
        Get
            Return _Material
        End Get
        Set(ByVal value As Double)
            _Material = value
        End Set
    End Property

    Public Property Process() As Double
        Get
            Return _Process
        End Get
        Set(ByVal value As Double)
            _Process = value
        End Set
    End Property

    Public Property RegisterDate() As String
        Get
            Return _RegisterDate
        End Get
        Set(ByVal value As String)
            _RegisterDate = value
        End Set
    End Property

    Public Property RegisterUser() As String
        Get
            Return _RegisterUser
        End Get
        Set(ByVal value As String)
            _RegisterUser = value
        End Set
    End Property

    Public Property UpdateDate() As String
        Get
            Return _UpdateDate
        End Get
        Set(ByVal value As String)
            _UpdateDate = value
        End Set
    End Property

    Public Property UpdateUser() As String
        Get
            Return _UpdateUser
        End Get
        Set(ByVal value As String)
            _UpdateUser = value
        End Set
    End Property

    Public Property User() As String
        Get
            Return _User
        End Get
        Set(ByVal value As String)
            _User = value
        End Set
    End Property
End Class

﻿Imports System.Data.SqlClient

Public Class ClsEngineeringPartListDB
    Public Shared Function getlist(ByVal Project_ID As String, Optional ByRef perr As String = "") As List(Of ClsEngineeringPartList)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Engineering_Part_List_Sel"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", Project_ID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsEngineeringPartList)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New ClsEngineeringPartList With {
                        .Part_No = Trim(dt.Rows(i).Item("Part_No") & ""),
                        .Part_Name = Trim(dt.Rows(i).Item("Part_Name") & ""),
                        .Upc = Trim(dt.Rows(i).Item("Upc") & ""),
                        .Fna = Trim(dt.Rows(i).Item("Fna") & ""),
                        .Epl_Part_No = Trim(dt.Rows(i).Item("Epl_Part_No") & ""),
                        .Dtl_Upc = Trim(dt.Rows(i).Item("Dtl_Upc") & ""),
                        .Dtl_Fna = Trim(dt.Rows(i).Item("Dtl_Fna") & ""),
                        .Usg_Seq = Trim(dt.Rows(i).Item("Usg_Seq") & ""),
                        .Lvl = Trim(dt.Rows(i).Item("Lvl") & ""),
                        .Qty = Trim(dt.Rows(i).Item("Qty") & ""),
                        .Hand = Trim(dt.Rows(i).Item("Hand") & ""),
                        .D_C = Trim(dt.Rows(i).Item("D_C") & ""),
                        .Dwg_No = Trim(dt.Rows(i).Item("Dwg_No") & ""),
                        .J_Note_Address = Trim(dt.Rows(i).Item("J_Note_Address") & ""),
                        .L_Note_Address = Trim(dt.Rows(i).Item("L_Note_Address") & ""),
                        .Color_Part = Trim(dt.Rows(i).Item("Color_Part") & ""),
                        .CC_Code = Trim(dt.Rows(i).Item("CC_Code") & ""),
                        .Intr_Color = Trim(dt.Rows(i).Item("Intr_Color") & ""),
                        .Body_Color = Trim(dt.Rows(i).Item("Body_Color") & ""),
                        .Extr_Color = Trim(dt.Rows(i).Item("Extr_Color") & ""),
                        .Check_Item = Trim(dt.Rows(i).Item("Check_Item") & ""),
                        .Check_Ru1_Supply_Cntry_Code1 = Trim(dt.Rows(i).Item("Check_Ru1_Supply_Cntry_Code1") & ""),
                        .Check_Ru1_KD1 = Trim(dt.Rows(i).Item("Check_Ru1_KD1") & ""),
                        .Check_Ru1_PID1 = Trim(dt.Rows(i).Item("Check_Ru1_PID1") & ""),
                        .Check_Ru1_Shipping_unit_IKP = Trim(dt.Rows(i).Item("Check_Ru1_Shipping_unit_IKP") & ""),
                        .Supply_Cntr_Code = Trim(dt.Rows(i).Item("Supply_Cntr_Code") & ""),
                        .Partition_Classification = Trim(dt.Rows(i).Item("Partition_Classification") & ""),
                        .Splr_From = Trim(dt.Rows(i).Item("Splr_From") & ""),
                        .Splr_To = Trim(dt.Rows(i).Item("Splr_To") & ""),
                        .Co_New = Trim(dt.Rows(i).Item("Co_New") & ""),
                        .Source_of_Diversion = Trim(dt.Rows(i).Item("Source_of_Diversion") & ""),
                        .Product_Number = Trim(dt.Rows(i).Item("Product_Number") & ""),
                        .Plan_Change_Setting = Trim(dt.Rows(i).Item("Plan_Change_Setting") & ""),
                        .ECR_No = Trim(dt.Rows(i).Item("ECR_No") & ""),
                        .Remarks = Trim(dt.Rows(i).Item("Remarks") & ""),
                        .IAMI_sourcing = Trim(dt.Rows(i).Item("IAMI_sourcing") & ""),
                        .Variant_Supply_Cntry_Code1 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code1") & ""),
                        .Variant_Supply_Cntry_Code2 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code2") & ""),
                        .Variant_Supply_Cntry_Code3 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code3") & ""),
                        .Variant_Supply_Cntry_Code4 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code4") & ""),
                        .Variant_Supply_Cntry_Code5 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code5") & ""),
                        .Variant_Supply_Cntry_Code6 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code6") & ""),
                        .Variant_Supply_Cntry_Code7 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code7") & ""),
                        .Variant_Supply_Cntry_Code8 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code8") & ""),
                        .Variant_Supply_Cntry_Code9 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code9") & ""),
                        .Variant_Supply_Cntry_Code10 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code10") & ""),
                        .Variant_Supply_Cntry_Code11 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code11") & ""),
                        .Variant_Supply_Cntry_Code12 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code12") & ""),
                        .Variant_Supply_Cntry_Code13 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code13") & ""),
                        .Variant_Supply_Cntry_Code14 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code14") & ""),
                        .Variant_Supply_Cntry_Code15 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code15") & ""),
                        .Variant_Supply_Cntry_Code16 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code16") & ""),
                        .Variant_Supply_Cntry_Code17 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code17") & ""),
                        .Variant_Supply_Cntry_Code18 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code18") & ""),
                        .Variant_Supply_Cntry_Code19 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code19") & ""),
                        .Variant_Supply_Cntry_Code20 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code20") & ""),
                        .Variant_Supply_Cntry_Code21 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code21") & ""),
                        .Variant_Supply_Cntry_Code22 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code22") & ""),
                        .Variant_Supply_Cntry_Code23 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code23") & ""),
                        .Variant_Supply_Cntry_Code24 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code24") & ""),
                        .Variant_Supply_Cntry_Code25 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code25") & ""),
                        .Variant_Supply_Cntry_Code26 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code26") & ""),
                        .Variant_Supply_Cntry_Code27 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code27") & ""),
                        .Variant_Supply_Cntry_Code28 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code28") & ""),
                        .Variant_Supply_Cntry_Code29 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code29") & ""),
                        .Variant_Supply_Cntry_Code30 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code30") & ""),
                        .Variant_Supply_Cntry_Code31 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code31") & ""),
                        .Variant_Supply_Cntry_Code32 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code32") & ""),
                        .Variant_Supply_Cntry_Code33 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code33") & ""),
                        .Variant_Supply_Cntry_Code34 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code34") & ""),
                        .Variant_Supply_Cntry_Code35 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code35") & ""),
                        .Variant_Supply_Cntry_Code36 = Trim(dt.Rows(i).Item("Variant_Supply_Cntry_Code36") & ""),
 .Variant_KD1 = Trim(dt.Rows(i).Item("Variant_KD1") & ""),
.Variant_KD2 = Trim(dt.Rows(i).Item("Variant_KD2") & ""),
.Variant_KD3 = Trim(dt.Rows(i).Item("Variant_KD3") & ""),
.Variant_KD4 = Trim(dt.Rows(i).Item("Variant_KD4") & ""),
.Variant_KD5 = Trim(dt.Rows(i).Item("Variant_KD5") & ""),
.Variant_KD6 = Trim(dt.Rows(i).Item("Variant_KD6") & ""),
.Variant_KD7 = Trim(dt.Rows(i).Item("Variant_KD7") & ""),
.Variant_KD8 = Trim(dt.Rows(i).Item("Variant_KD8") & ""),
.Variant_KD9 = Trim(dt.Rows(i).Item("Variant_KD9") & ""),
.Variant_KD10 = Trim(dt.Rows(i).Item("Variant_KD10") & ""),
.Variant_KD11 = Trim(dt.Rows(i).Item("Variant_KD11") & ""),
.Variant_KD12 = Trim(dt.Rows(i).Item("Variant_KD12") & ""),
.Variant_KD13 = Trim(dt.Rows(i).Item("Variant_KD13") & ""),
.Variant_KD14 = Trim(dt.Rows(i).Item("Variant_KD14") & ""),
.Variant_KD15 = Trim(dt.Rows(i).Item("Variant_KD15") & ""),
.Variant_KD16 = Trim(dt.Rows(i).Item("Variant_KD16") & ""),
.Variant_KD17 = Trim(dt.Rows(i).Item("Variant_KD17") & ""),
.Variant_KD18 = Trim(dt.Rows(i).Item("Variant_KD18") & ""),
.Variant_KD19 = Trim(dt.Rows(i).Item("Variant_KD19") & ""),
.Variant_KD20 = Trim(dt.Rows(i).Item("Variant_KD20") & ""),
.Variant_KD21 = Trim(dt.Rows(i).Item("Variant_KD21") & ""),
.Variant_KD22 = Trim(dt.Rows(i).Item("Variant_KD22") & ""),
.Variant_KD23 = Trim(dt.Rows(i).Item("Variant_KD23") & ""),
.Variant_KD24 = Trim(dt.Rows(i).Item("Variant_KD24") & ""),
.Variant_KD25 = Trim(dt.Rows(i).Item("Variant_KD25") & ""),
.Variant_KD26 = Trim(dt.Rows(i).Item("Variant_KD26") & ""),
.Variant_KD27 = Trim(dt.Rows(i).Item("Variant_KD27") & ""),
.Variant_KD28 = Trim(dt.Rows(i).Item("Variant_KD28") & ""),
.Variant_KD29 = Trim(dt.Rows(i).Item("Variant_KD29") & ""),
.Variant_KD30 = Trim(dt.Rows(i).Item("Variant_KD30") & ""),
.Variant_KD31 = Trim(dt.Rows(i).Item("Variant_KD31") & ""),
.Variant_KD32 = Trim(dt.Rows(i).Item("Variant_KD32") & ""),
.Variant_KD33 = Trim(dt.Rows(i).Item("Variant_KD33") & ""),
.Variant_KD34 = Trim(dt.Rows(i).Item("Variant_KD34") & ""),
.Variant_KD35 = Trim(dt.Rows(i).Item("Variant_KD35") & ""),
.Variant_KD36 = Trim(dt.Rows(i).Item("Variant_KD36") & ""),
.Variant_PID1 = Trim(dt.Rows(i).Item("Variant_PID1") & ""),
.Variant_PID2 = Trim(dt.Rows(i).Item("Variant_PID2") & ""),
.Variant_PID3 = Trim(dt.Rows(i).Item("Variant_PID3") & ""),
.Variant_PID4 = Trim(dt.Rows(i).Item("Variant_PID4") & ""),
.Variant_PID5 = Trim(dt.Rows(i).Item("Variant_PID5") & ""),
.Variant_PID6 = Trim(dt.Rows(i).Item("Variant_PID6") & ""),
.Variant_PID7 = Trim(dt.Rows(i).Item("Variant_PID7") & ""),
.Variant_PID8 = Trim(dt.Rows(i).Item("Variant_PID8") & ""),
.Variant_PID9 = Trim(dt.Rows(i).Item("Variant_PID9") & ""),
.Variant_PID10 = Trim(dt.Rows(i).Item("Variant_PID10") & ""),
.Variant_PID11 = Trim(dt.Rows(i).Item("Variant_PID11") & ""),
.Variant_PID12 = Trim(dt.Rows(i).Item("Variant_PID12") & ""),
.Variant_PID13 = Trim(dt.Rows(i).Item("Variant_PID13") & ""),
.Variant_PID14 = Trim(dt.Rows(i).Item("Variant_PID14") & ""),
.Variant_PID15 = Trim(dt.Rows(i).Item("Variant_PID15") & ""),
.Variant_PID16 = Trim(dt.Rows(i).Item("Variant_PID16") & ""),
.Variant_PID17 = Trim(dt.Rows(i).Item("Variant_PID17") & ""),
.Variant_PID18 = Trim(dt.Rows(i).Item("Variant_PID18") & ""),
.Variant_PID19 = Trim(dt.Rows(i).Item("Variant_PID19") & ""),
.Variant_PID20 = Trim(dt.Rows(i).Item("Variant_PID20") & ""),
.Variant_PID21 = Trim(dt.Rows(i).Item("Variant_PID21") & ""),
.Variant_PID22 = Trim(dt.Rows(i).Item("Variant_PID22") & ""),
.Variant_PID23 = Trim(dt.Rows(i).Item("Variant_PID23") & ""),
.Variant_PID24 = Trim(dt.Rows(i).Item("Variant_PID24") & ""),
.Variant_PID25 = Trim(dt.Rows(i).Item("Variant_PID25") & ""),
.Variant_PID26 = Trim(dt.Rows(i).Item("Variant_PID26") & ""),
.Variant_PID27 = Trim(dt.Rows(i).Item("Variant_PID27") & ""),
.Variant_PID28 = Trim(dt.Rows(i).Item("Variant_PID28") & ""),
.Variant_PID29 = Trim(dt.Rows(i).Item("Variant_PID29") & ""),
.Variant_PID30 = Trim(dt.Rows(i).Item("Variant_PID30") & ""),
.Variant_PID31 = Trim(dt.Rows(i).Item("Variant_PID31") & ""),
.Variant_PID32 = Trim(dt.Rows(i).Item("Variant_PID32") & ""),
.Variant_PID33 = Trim(dt.Rows(i).Item("Variant_PID33") & ""),
.Variant_PID34 = Trim(dt.Rows(i).Item("Variant_PID34") & ""),
.Variant_PID35 = Trim(dt.Rows(i).Item("Variant_PID35") & ""),
.Variant_PID36 = Trim(dt.Rows(i).Item("Variant_PID36") & "")
                        }
                    list.Add(ss)

                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function Insert(ByVal pclsEng As ClsEngineeringPartList, Optional ByRef pErr As String = "") As Integer
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                cmd = New SqlCommand("sp_Engineering_Part_List_insert", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Pno", pclsEng.Pno)
                cmd.Parameters.AddWithValue("Project_ID", pclsEng.Project_ID)
                cmd.Parameters.AddWithValue("Part_No", pclsEng.Part_No)
                cmd.Parameters.AddWithValue("Part_Name", pclsEng.Part_Name)
                cmd.Parameters.AddWithValue("Upc", pclsEng.Upc)
                cmd.Parameters.AddWithValue("Fna", pclsEng.Fna)
                cmd.Parameters.AddWithValue("Epl_Part_No", pclsEng.Epl_Part_No)
                cmd.Parameters.AddWithValue("Dtl_Upc", pclsEng.Dtl_Upc)
                cmd.Parameters.AddWithValue("Dtl_Fna", pclsEng.Dtl_Fna)
                cmd.Parameters.AddWithValue("Usg_Seq", pclsEng.Usg_Seq)
                cmd.Parameters.AddWithValue("Lvl", pclsEng.Lvl)
                cmd.Parameters.AddWithValue("Qty", pclsEng.Qty)
                cmd.Parameters.AddWithValue("Hand", pclsEng.Hand)
                cmd.Parameters.AddWithValue("D_C", pclsEng.D_C)
                cmd.Parameters.AddWithValue("Dwg_No", pclsEng.Dwg_No)
                cmd.Parameters.AddWithValue("J_Note_Address", pclsEng.J_Note_Address)
                cmd.Parameters.AddWithValue("L_Note_Address", pclsEng.L_Note_Address)
                cmd.Parameters.AddWithValue("Color_Part", pclsEng.Color_Part)
                cmd.Parameters.AddWithValue("CC_Code", pclsEng.CC_Code)
                cmd.Parameters.AddWithValue("Intr_Color", pclsEng.Intr_Color)
                cmd.Parameters.AddWithValue("Body_Color", pclsEng.Body_Color)
                cmd.Parameters.AddWithValue("Extr_Color", pclsEng.Extr_Color)
                cmd.Parameters.AddWithValue("Check_Item", pclsEng.Check_Item)
                cmd.Parameters.AddWithValue("Check_Ru1_Supply_Cntry_Code1", pclsEng.Check_Ru1_Supply_Cntry_Code1)
                cmd.Parameters.AddWithValue("Check_Ru1_KD1", pclsEng.Check_Ru1_KD1)
                cmd.Parameters.AddWithValue("Check_Ru1_PID1", pclsEng.Check_Ru1_PID1)
                cmd.Parameters.AddWithValue("Check_Ru1_Shipping_unit_IKP", pclsEng.Check_Ru1_Shipping_unit_IKP)
                cmd.Parameters.AddWithValue("Supply_Cntr_Code", pclsEng.Supply_Cntr_Code)
                cmd.Parameters.AddWithValue("Partition_Classification", pclsEng.Partition_Classification)
                cmd.Parameters.AddWithValue("Splr_From", pclsEng.Splr_From)
                cmd.Parameters.AddWithValue("Splr_To", pclsEng.Splr_To)
                cmd.Parameters.AddWithValue("Co_New", pclsEng.Co_New)
                cmd.Parameters.AddWithValue("Source_of_Diversion", pclsEng.Source_of_Diversion)
                cmd.Parameters.AddWithValue("Product_Number", pclsEng.Product_Number)
                cmd.Parameters.AddWithValue("Plan_Change_Setting", pclsEng.Plan_Change_Setting)
                cmd.Parameters.AddWithValue("ECR_No", pclsEng.ECR_No)
                cmd.Parameters.AddWithValue("Remarks", pclsEng.Remarks)
                cmd.Parameters.AddWithValue("IAMI_sourcing", pclsEng.IAMI_sourcing)
                cmd.Parameters.AddWithValue("Register_By", pclsEng.UserID)
                i = cmd.ExecuteNonQuery
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
    Public Shared Function InsertVar(ByVal pclsEng As ClsEngineeringPartList, ByVal pNo As String, Optional ByRef pErr As String = "") As Integer
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                cmd = New SqlCommand("sp_Engineering_Part_List_Update_Var", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", pclsEng.Project_ID)
                cmd.Parameters.AddWithValue("Part_No", pclsEng.Part_No)
                cmd.Parameters.AddWithValue("Variant_Supply_Cntry_Code", pclsEng.Variant_Supply_Cntry_Code)
                cmd.Parameters.AddWithValue("Variant_KD", pclsEng.Variant_KD)
                cmd.Parameters.AddWithValue("Variant_PID", pclsEng.Variant_PID)
                cmd.Parameters.AddWithValue("Pno", pNo)
                i = cmd.ExecuteNonQuery
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
   
    Public Shared Function getCountVariant(ByVal Project_ID As String, Optional ByRef perr As String = "") As List(Of ClsUploadEngineeringPartList)
        Try
            Using conn As New SqlConnection(Sconn.Stringkoneksi)
                conn.Open()
                Dim sql As String = ""
                sql = "sp_Engineering_Part_List_Variant_SelCount"
                Dim cmd As New SqlCommand(sql, conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Project_ID", Project_ID)
                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim list As New List(Of ClsUploadEngineeringPartList)
                For i = 0 To dt.Rows.Count - 1
                    Dim ss As New ClsUploadEngineeringPartList With {
                        .No = dt.Rows(i).Item("no"),
                        .pVariant = Trim(dt.Rows(i).Item("Variant") & "")
                        }
                    list.Add(ss)
                Next
                Return list
            End Using
        Catch ex As Exception
            perr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

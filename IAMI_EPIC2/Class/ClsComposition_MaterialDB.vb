﻿Imports System.Data.SqlClient


Public Class ClsComposition_MaterialDB
    Public Shared Function CompositionMaterial_List(Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Composition_Material_GetList", con)
                cmd.CommandType = CommandType.StoredProcedure

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteData(pMaterialCode As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Composition_Material_Delete", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Parent_MaterialCode", pMaterialCode)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    'form detail

    Public Shared Function getDetailComposition(pMaterialCode As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Composition_Material_GetDataDetail", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Parent_MaterialCode", pMaterialCode)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertData(pParentItem As String, pChildItem As String, pComposition As Decimal, pUOM As String, _
                                             pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Composition_Material_InsertDetail", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Parent_MaterialCode", pParentItem)
                cmd.Parameters.AddWithValue("@Child_MaterialCode", pChildItem)
                cmd.Parameters.AddWithValue("@CompositionPercent", pComposition)
                cmd.Parameters.AddWithValue("@UOM", pUOM)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateData(pParentItem As String, pChildItem As String, pSeqNo As Integer, pComposition As Decimal, pUOM As String, _
                                             pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Composition_Material_UpdateDetail", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Parent_MaterialCode", pParentItem)
                cmd.Parameters.AddWithValue("@Child_MaterialCode", pChildItem)
                cmd.Parameters.AddWithValue("@Sequence_No", pSeqNo)
                cmd.Parameters.AddWithValue("@CompositionPercent", pComposition)
                cmd.Parameters.AddWithValue("@UOM", pUOM)
                cmd.Parameters.AddWithValue("@UserID", pUser)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteData(pParentItem As String, pChildItem As String, pSeqNo As Integer, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Composition_Material_DeleteDetail", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Parent_MaterialCode", pParentItem)
                cmd.Parameters.AddWithValue("@Child_MaterialCode", pChildItem)
                cmd.Parameters.AddWithValue("@Sequence_No", pSeqNo)
                con.Open()
                cmd.ExecuteNonQuery()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataMaterialName(pMaterialCode As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try

            Dim sQuery As String = "SELECT COALESCE(Material_Code,'') Parent_MaterialCode,COALESCE(Material_Name,'') Parent_MaterialName FROM Mst_Material WHERE Material_Code='" & pMaterialCode & "' "
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sQuery, con)
                cmd.CommandType = CommandType.Text

                'cmd.Parameters.AddWithValue("Value", pMaterialCode)
                Dim da As New SqlDataAdapter(cmd)

                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    '*************************'
    'GET DESCRIPTION OF COMBO
    '*************************
    Public Shared Function GetDataCombo(pComboType As String, Optional ByVal pComboValue As String = "ALL", Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try

            Dim sQuery As String = "sp_Mst_CompositionMaterial_GetDataCombo"
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sQuery, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Type", pComboType)
                cmd.Parameters.AddWithValue("ComboValue", pComboValue)
                Dim da As New SqlDataAdapter(cmd)

                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


End Class

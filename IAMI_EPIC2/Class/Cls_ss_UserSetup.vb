﻿Public Class Cls_UserSetup
    Public Property AppID As String
    Public Property UserID As String
    Public Property Email As String
    Public Property UserName As String
    Public Property UserType As String
    Public Property Password As String
    Public Property Description As String
    Public Property SecurityQuestion As String
    Public Property SecurityAnswer As String
    Public Property PasswordHint As String
    Public Property Locked As String
    Public Property AdminStatus As String
    Public Property CatererID As String
    Public Property LastLogin As String
    Public Property RegisterDate As String
    Public Property LastUpdate As String

    Public Property Supplier As String
    Public Property PIC_PO As String
    Public Property Department As String
    Public Property Section As String
    Public Property JobPos As String

    Public Property PicSign As Byte
    Public Property Pic_Sign As Object

    Public Property Approval_Group As String
    Public Property Approval_ID As String
    Public Property Approval_Level As String
    Public Property Approval_Name As String
    
End Class

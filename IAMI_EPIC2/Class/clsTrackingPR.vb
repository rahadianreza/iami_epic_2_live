﻿Public Class clsTrackingPR
    Public Property Department As String
    Public Property Section As String
    Public Property Project As String
    Public Property PRNo As String
    Public Property Description As String
    Public Property Specification As String
    Public Property Qty As Double
    Public Property UOM As String

    Public Property PICUserName As String
    Public Property PICCostControl As String

    Public Property PRReceived As String

    Public Property CEPlanning As String
    Public Property CEActual As String

    Public Property IABudgetReceived As String
    Public Property CostTarget As String
    Public Property Tender As String
    Public Property IAPrice As String
    Public Property Supplier As String

    Public Property PONo As String
    Public Property PODate As String
    Public Property POTotalAmount As Double
    Public Property POCreated As String

    Public Property GRNo As String
    Public Property GRDate As String
    Public Property GRCreated As String

    Public Property Remarks As String
End Class

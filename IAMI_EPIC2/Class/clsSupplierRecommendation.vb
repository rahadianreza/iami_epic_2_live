﻿
Public Class clsSupplierRecommendation
    Public Property SRNumber As String
    Public Property SRDate As Date

    Public Property FromDate As Date
    Public Property ToDate As Date
    Public Property Revision As String
    Public Property CPNumber As String
    Public Property SRStatus As String
    Public Property SupplierRecommend As String
    Public Property Consideration As String
    Public Property Notes As String

    Public Property Information_Code As String
    Public Property Information As String
    Public Property GroupType As String
    Public Property Supplier1 As String
    Public Property Quotation1 As String
    Public Property Final1 As String

    Public Property Supplier2 As String
    Public Property Quotation2 As String
    Public Property Final2 As String

    Public Property Supplier3 As String
    Public Property Quotation3 As String
    Public Property Final3 As String

    Public Property Supplier4 As String
    Public Property Quotation4 As String
    Public Property Final4 As String

    Public Property Supplier5 As String
    Public Property Quotation5 As String
    Public Property Final5 As String
    Public Property Weight As String
    Public Property Point1 As String
    Public Property WeightPoint1 As String
    Public Property Notes1 As String

    Public Property Point2 As String
    Public Property WeightPoint2 As String
    Public Property Notes2 As String

    Public Property Point3 As String
    Public Property WeightPoint3 As String
    Public Property Notes3 As String

    Public Property Point4 As String
    Public Property WeightPoint4 As String
    Public Property Notes4 As String

    Public Property Point5 As String
    Public Property WeightPoint5 As String
    Public Property Notes5 As String
   
End Class

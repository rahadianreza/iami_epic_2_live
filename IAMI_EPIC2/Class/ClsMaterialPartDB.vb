﻿Imports System.Data.SqlClient

Public Class ClsMaterialPartDB
    Public Shared Function getGrid(Category_Material As String, Material_Code As String, pUser As String, _
                                   type As String, PeriodFrom As String, Periodto As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Part_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Category_Material", Category_Material)
                cmd.Parameters.AddWithValue("@Material_Code", Material_Code)
                cmd.Parameters.AddWithValue("@Type", type)
                cmd.Parameters.AddWithValue("@PeriodFrom", PeriodFrom)
                cmd.Parameters.AddWithValue("@PeriodTo", Periodto)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function getDetail(Category_Material As String, Material_Code As String, period As String, type As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Part_Detail_Sel", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Category_Material", Category_Material)
                cmd.Parameters.AddWithValue("@Material_Code", Material_Code)
                cmd.Parameters.AddWithValue("@Period", period)
                cmd.Parameters.AddWithValue("@Type", type)

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function



    'Public Shared Function getMaterialCode(Category_Material As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
    '    Dim ds As New DataSet
    '    Try

    '        Dim sQuery As String = ""
    '        If Category_Material = "00" Then
    '            sQuery = "SELECT Material_Code FROM Mst_Material_Part"
    '        Else
    '            sQuery = "SELECT Material_Code FROM Mst_Material_Part WHERE Category_Material = '" + Category_Material + "'"
    '        End If

    '        Using con As New SqlConnection(Sconn.Stringkoneksi)
    '            Dim cmd As New SqlCommand(sQuery, con)
    '            cmd.CommandType = CommandType.Text
    '            Dim da As New SqlDataAdapter(cmd)
    '            da.Fill(ds)
    '            da.Dispose()
    '            cmd.Dispose()
    '            Return ds
    '        End Using
    '    Catch ex As Exception
    '        pErr = ex.Message
    '        Return Nothing
    '    End Try
    'End Function

    Public Shared Function InsertMaterialCode(Period As String, type As String, Category_Material As String, Material_Name As String, _
                                              Specification As String, UoMUnitPrice As String, UoMUnitConsump As String, UnitPrice As Decimal, _
                                              UnitConsump As Decimal, currency As String, MaterialType As String, GroupItem As String, Category As String, _
                                              materialcode As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Part_Ins", con)
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.AddWithValue("@Period", Period)
                cmd.Parameters.AddWithValue("@Type", type)
                cmd.Parameters.AddWithValue("@Category_Material", Category_Material)
                cmd.Parameters.AddWithValue("@Material_Name", Material_Name)
                cmd.Parameters.AddWithValue("@Specification", Specification)
                cmd.Parameters.AddWithValue("@UoMUnitPrice", UoMUnitPrice)
                cmd.Parameters.AddWithValue("@UnitPrice", UnitPrice)
                cmd.Parameters.AddWithValue("@UoMUnitConsump", UoMUnitConsump)
                cmd.Parameters.AddWithValue("@UnitConsump", UnitConsump)
                cmd.Parameters.AddWithValue("@MaterialType", MaterialType)
                cmd.Parameters.AddWithValue("@GroupItem", GroupItem)
                cmd.Parameters.AddWithValue("@Category", Category)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@Register_By", pUser)
                cmd.Parameters.AddWithValue("@Material_Code", materialcode)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateMaterialCode(Period As String, type As String, Category_Material As String, Material_Name As String, _
                                              Specification As String, UoMUnitPrice As String, UoMUnitConsump As String, UnitPrice As Decimal, _
                                              UnitConsump As Decimal, currency As String, MaterialType As String, GroupItem As String, Category As String, _
                                              materialcode As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Part_Upd", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Period", Period)
                cmd.Parameters.AddWithValue("@Type", type)
                cmd.Parameters.AddWithValue("@Category_Material", Category_Material)
                cmd.Parameters.AddWithValue("@Material_Name", Material_Name)
                cmd.Parameters.AddWithValue("@Specification", Specification)
                cmd.Parameters.AddWithValue("@UoMUnitPrice", UoMUnitPrice)
                cmd.Parameters.AddWithValue("@UnitPrice", UnitPrice)
                cmd.Parameters.AddWithValue("@UoMUnitConsump", UoMUnitConsump)
                cmd.Parameters.AddWithValue("@UnitConsump", UnitConsump)
                cmd.Parameters.AddWithValue("@MaterialType", MaterialType)
                cmd.Parameters.AddWithValue("@GroupItem", GroupItem)
                cmd.Parameters.AddWithValue("@Category", Category)
                cmd.Parameters.AddWithValue("@Currency", currency)
                cmd.Parameters.AddWithValue("@Update_By", pUser)
                cmd.Parameters.AddWithValue("@Material_Code", materialcode)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteMaterialCode(Category_Material As String, Material_Code As String, _
                                             pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Part_Del", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Category_Material", Category_Material)
                cmd.Parameters.AddWithValue("@Material_Code", Material_Code)
                con.Open()
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetMaterialCode(Category_Material As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try

            Dim sQuery As String
            If Category_Material = "ALL" Then
                sQuery = "Select Rtrim(Par_Code) Code From Mst_Parameter Where Par_Group = 'MaterialCode' "
            Else
                sQuery = "Select Rtrim(Par_Code) Code From Mst_Parameter Where Par_Group = 'MaterialCode' AND Par_ParentCode = '" + Category_Material + "'"
            End If

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sQuery, con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function GetDescriptionMatrialCode(MaterialCode As String, pUser As String, Optional ByRef pErr As String = "") As String
        Dim ds As New DataSet
        Try

            Dim sQuery As String = "Select Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialCode' AND Par_Code = '" + MaterialCode + "'"

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand(sQuery, con)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds.Tables(0).Rows(0).Item(0).ToString
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetMaterialChart(Category_Material As String, materialCode As String, type As String, periodfrom As String, periodto As String, _
                                            Currency As String, pUser As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_Material_Part_Chart", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Category_Material", Category_Material)
                cmd.Parameters.AddWithValue("@Material_Code", materialCode)
                cmd.Parameters.AddWithValue("@Type", type)
                cmd.Parameters.AddWithValue("@PeriodFrom", IIf(String.IsNullOrEmpty(periodfrom), "1980-01-01", CDate(periodfrom)))
                cmd.Parameters.AddWithValue("@PeriodTo", IIf(String.IsNullOrEmpty(periodto), "1980-01-01", CDate(periodto)))
                cmd.Parameters.AddWithValue("@Currency", Currency)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
                Return ds
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

End Class

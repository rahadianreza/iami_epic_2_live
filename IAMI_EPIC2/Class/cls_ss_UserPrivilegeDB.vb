﻿Imports System.Data.SqlClient

Public Class Cls_ss_UserPrivilegeDB
    ''' <summary>
    ''' get list of User
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetList(Optional ByRef pErr As String = "") As List(Of cls_ss_UserPrivilege)
        Try
            Using cn As New SqlConnection(Sconn.Stringkoneksi)
                Dim sql As String = "exec sp_UserPrivilage_Sel"
                Dim Cmd As New SqlCommand(sql, cn)
                Dim da As New SqlDataAdapter(Cmd)
                Dim dt As New DataTable
                da.Fill(dt)
                Dim UPs As New List(Of Cls_ss_UserPrivilege)
                For i = 0 To dt.Rows.Count - 1
                    Dim UP As New Cls_ss_UserPrivilege With {.AppID = "P01", .UserID = dt.Rows(i)("UserID"),
                            .MenuID = Trim(dt.Rows(i)("MenuID") & ""), .AllowAccess = dt.Rows(i)("AllowAccess"), .AllowUpdate = dt.Rows(i)("AllowUpdate"), .AllowPrint = dt.Rows(i)("AllowPrint")}
                    UPs.Add(UP)
                Next
                Return UPs
            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try

    End Function

    Public Shared Function GetValidate(ByVal pUserID As String, Optional ByRef pErr As String = "") As DataSet
        Try
            pErr = ""
            Using cn As New SqlConnection(Sconn.Stringkoneksi)
                cn.Open()
                Dim sql As String = ""
                sql = "sp_UserPrivilage_GetValidate"
                Dim cmd As New SqlCommand(sql, cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("UserID", pUserID)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try



    End Function

    ''' <summary>
    ''' Insert data to UserSetup table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Save(ByVal pUserP As Cls_ss_UserPrivilege, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_UserPrivilege_Ins"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("AppID", "P01")
                cmd.Parameters.AddWithValue("UserID", pUserP.UserID)
                cmd.Parameters.AddWithValue("MenuID", pUserP.MenuID)
                cmd.Parameters.AddWithValue("AllowAccess", pUserP.AllowAccess)
                cmd.Parameters.AddWithValue("AllowUpdate", pUserP.AllowUpdate)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Delete data from UserSetup table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Delete(ByVal pUserP As Cls_ss_UserPrivilege, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_UserPrivilege_Del"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("UserID", pUserP.UserID)
               
                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            Return Nothing
        End Try

    End Function
End Class

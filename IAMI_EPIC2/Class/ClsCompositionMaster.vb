﻿
Public Class ClsCompositionMaster
    Public Property CompositionSequenceNo As Integer
    Public Property ParentItemCode As String
    Public Property ParentItemDescs As String
    Public Property ParentItemQty As Integer
    Public Property ParentItemUOM As String
    Public Property ChildCompositionSequenceNo As Integer
    Public Property ChildItemCode As String
    Public Property ChildItemQty As Integer
    Public Property CurrencyCode As String
    Public Property ChildItemUOM As String
    Public Property Price As String
    Public Property CreateDate As String
    Public Property CreateUser As String
    Public Property UpdateDate As String
    Public Property UpdateUser As String
End Class

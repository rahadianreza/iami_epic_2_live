﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TaskList.aspx.vb" Inherits="IAMI_EPIC2.TaskList" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    function GridLoad(s, e) {
        Grid.PerformCallback('load|' + dtQuotationFrom.GetText() + '|' + dtQuotationTo.GetText());
    }

    function downloadValidation(s, e) {
        if (Grid.GetVisibleRowsOnPage() == 0) {
            toastr.info('There is no data to download!', 'Info');
            e.processOnServer = false;
            return;
        }
    }

    function GridCustomButtonClick(s, e) {
        if (e.buttonID == 'det') {
            var rowKey = Grid.GetRowKey(e.visibleIndex);
            
            var jsDate = dtQuotationFrom.GetDate();
            var year = jsDate.getFullYear(); // where getFullYear returns the year (four digits)
            var month = jsDate.getMonth()+1; // where getMonth returns the month (from 0-11)
            var day = jsDate.getDate();   // where getDate returns the day of the month (from 1-31)
            var myDateFrom = year + "-" + month + "-" + day;

            jsDate = dtQuotationTo.GetDate();
            year = jsDate.getFullYear(); // where getFullYear returns the year (four digits)
            month = jsDate.getMonth()+1; // where getMonth returns the month (from 0-11)
            day = jsDate.getDate();   // where getDate returns the day of the month (from 1-31)
            var myDateTo = year + "-" + month + "-" + day;

            var rowPos = Grid.GetVisibleRowsOnPage();

            //<12 Parameters>
            //0 : CPNumber
            //1 : Rev
            //2 : PRNumber
            //3 : RFQNumber
            //4 : QuotationNo
            //5 : SupplierCode
            //6 : QuotationDateFrom <return value for filter area at CPList.aspx>
            //7 : QuotationDateTo <return value for filter area at CPList.aspx>
            //8 : PRNumber <return value for filter area at CPList.aspx>
            //9 : SupplierCode <return value for filter area at CPList.aspx>
            //10: CPNumber <return value for filter area at CPList.aspx>
            //11: Row Position on the grid <return value for filter area at CPList.aspx>
            window.location.href = 'CPListDetail.aspx?ID=' + rowKey + '|' + myDateFrom + '|' + myDateTo + '|' + cboPRNumber.GetText() + '|' + cboSupplierCode.GetText() + '|' + cboCPNumber.GetText() + '|' + rowPos;
        }
    }


    function btnAdd(s, e) {

    }


    function LoadCompleted(s, e) {
        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }
    }
</script>

<style type="text/css">
    .colwidthbutton
    {
        width: 90px;
    }
    
    .rowheight
    {
        height: 35px;
    }
       
    .col1
    {
        width: 10px;
    }
    .colLabel1
    {
        width: 133px;
    }
    .colLabel2
    {
        width: 150px;
    }
    .colInput1
    {
        width: 133px;
    }
    .colInput2
    {
        width: 133px;
    }
    .colSpace
    {
        width: 100px;
    }
        
    .customHeader {
        height: 20px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
    </div>
   <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="KeyField" >
            
            <Columns>
                <dx:GridViewDataTextColumn Caption="Task Group" FieldName="Task_Group"
                     VisibleIndex="0" Width="200px" Name="Task Group">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Transaction Number" FieldName="Trans_Number"
                     VisibleIndex="1" Width="200px" Name="Trans_Number">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Rev" FieldName="Trans_Revision"
                     VisibleIndex="2" Width="50px" Name="Trans_Revision">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Transaction Date" FieldName="Trans_Date" 
                     VisibleIndex="3" Width="180px" Name="Trans_Date">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataHyperLinkColumn Caption="Link" FieldName="Trans_Link" 
                    Name="Trans_Link" VisibleIndex="4" Width="100px">
                    <PropertiesHyperLinkEdit Text="Goto Link">
                    </PropertiesHyperLinkEdit>
                </dx:GridViewDataHyperLinkColumn>
                <dx:GridViewDataTextColumn Caption="KeyField" FieldName="KeyField" 
                    Name="KeyField" Visible="False" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
               
             </Columns>

            <SettingsBehavior AllowFocusedRow="True" AllowSort="False" AllowDragDrop="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
            <Settings ShowFilterRow="false" VerticalScrollableHeight="225" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" ShowStatusBar="Hidden"></Settings>
            
            <ClientSideEvents 
                CustomButtonClick="GridCustomButtonClick"
                EndCallback="LoadCompleted"
            />
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" 
                AllowInsert="False" />

            <Styles Header-Paddings-Padding="2px" EditFormColumnCaption-Paddings-PaddingLeft="0px" EditFormColumnCaption-Paddings-PaddingRight="0px" >
                <Header CssClass="customHeader" HorizontalAlign="Center" Wrap="True">
<Paddings Padding="2px"></Paddings>
                </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="0px" PaddingRight="0px"></Paddings>
</EditFormColumnCaption>
            </Styles>
        </dx:ASPxGridView>
    </div>
    <%--<div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
      
        </table>
    </div>--%>
</div>
</asp:Content>
﻿Imports DevExpress.Web.ASPxGridView
Imports System.IO
Imports DevExpress.XtraPrinting
Imports DevExpress.Web.ASPxEditors

Public Class DrawingItemListSourcing_Grouping
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            up_GridLoad("0")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("I060")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        prj = Request.QueryString("ProjectID")
        grp = Request.QueryString("GroupID")
        comm = Request.QueryString("Commodity")

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            'cboProjectType.SelectedIndex = 0
            'cboStatusDrawing.SelectedIndex = 0

            cboProjectType.SelectedIndex = 0
            If String.IsNullOrEmpty(prj) Then
                cboProject.SelectedIndex = 0
                cboGroup.SelectedIndex = 0
                cboCommodity.SelectedIndex = 0
            Else
                cboProject.Value = prj
                cboGroup.Value = grp
                cboCommodity.Value = comm
            End If
            cboStatusDrawing.SelectedIndex = 0


            up_FillComboProjectType(cboProjectType, statusAdmin, pUser, "ProjectType")
            up_FillComboProject(cboProject, statusAdmin, pUser, cboProjectType.Value)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "1", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboGroup.Value, cboProject.Value, "", "2", statusAdmin, pUser)

        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Dim _itmLstSource As New clsItemListSourcing
        Dim pErr As String = ""

        Dim cnt As String = clsItemListSourcingDB.countGroupCommodity(cboProject.Value, cboGroup.Value, cboCommodity.Value)
        Dim groupid As String = cboCommodity.Value.ToString().Substring(0, 3) & Now.Year.ToString.Substring(2, 2) & Now.Month.ToString.PadLeft(3, "0").Substring(1, 2) & (cnt + 1).ToString().PadLeft(3, "0")

        Try
            With Grid
                If e.UpdateValues.Count > 0 Then
                    For i = 0 To e.UpdateValues.Count - 1
                        _itmLstSource.Status_Drawing = e.UpdateValues(i).NewValues("Status_Group")
                        _itmLstSource.Project_ID = cboProject.Value
                        _itmLstSource.Part_No = e.UpdateValues(i).OldValues("Part_No")
                        _itmLstSource.Group_ID = e.UpdateValues(i).OldValues("Group_ID")
                        _itmLstSource.Commodity = e.UpdateValues(i).OldValues("Commodity")
                        _itmLstSource.Upc = e.UpdateValues(i).OldValues("Upc")
                        _itmLstSource.Fna = e.UpdateValues(i).OldValues("Fna")
                        _itmLstSource.Dtl_Upc = e.UpdateValues(i).OldValues("Dtl_Upc")
                        _itmLstSource.Dtl_Fna = e.UpdateValues(i).OldValues("Dtl_Fna")
                        _itmLstSource.Usg_Seq = e.UpdateValues(i).OldValues("Usg_Seq")
                        _itmLstSource.Lvl = e.UpdateValues(i).OldValues("Lvl")
                        _itmLstSource.Hand = e.UpdateValues(i).OldValues("Hand")
                        _itmLstSource.D_C = e.UpdateValues(i).OldValues("D_C")
                        _itmLstSource.Dwg_No = e.UpdateValues(i).OldValues("Dwg_No")

                      
                        clsItemListSourcingDB.InsertDrawingGrouping(_itmLstSource, groupid, pUser, pErr)
                    Next
                End If
            End With
        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 1)
        End Try
    End Sub

    Private Sub Grid_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If (e.Column.FieldName = "Status_Group") Then
            e.Editor.ReadOnly = False
        Else
            e.Editor.ReadOnly = True
        End If

    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else

            up_GridLoad("1")
        End If
    End Sub

    Private Sub up_FillComboProjectType(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                              pAdminStatus As String, pUserID As String, _
                              Optional ByRef pGroup As String = "", _
                              Optional ByRef pParent As String = "", _
                              Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboProjectTypeGrouping(pUser, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_FillComboGroupCommodityPIC(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _param As String, _param2 As String, _param3 As String, _type As String, _
                            pAdminStatus As String, pUserID As String, _
                            Optional ByRef pGroup As String = "", _
                            Optional ByRef pParent As String = "", _
                            Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsItemListSourcingDB.getGroupCommodityPIC(_param, _param2, _param3, _type, "Y", "3", pUserID)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsItemListSourcingDB.getHeaderGridDrawingGrouping(cboProject.Value, cboGroup.Value, cboCommodity.Value, pUser, cboStatusDrawing.Value, pUser)
        If ErrMsg = "" Then

            Grid.DataSource = Ses
            Grid.DataBind()

        End If
    End Sub
#End Region

#Region "Event Control"
    Private Sub up_Excel()
        up_GridLoad("1")

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ItemListSourcing" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub up_FillComboProject(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                             pAdminStatus As String, pUserID As String, projtype As String, _
                             Optional ByRef pGroup As String = "", _
                             Optional ByRef pParent As String = "", _
                             Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboProject(projtype, pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub cboProject_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboProject.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProjectType As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboProject(cboProject, statusAdmin, pUser, "")

        ElseIf pFunction = "filter" Then
            up_FillComboProject(cboProject, statusAdmin, pUser, pProjectType)
        End If


    End Sub

    Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProject As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroup, pProject, "", "", "1", statusAdmin, pUser, "")
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroup, pProject, "", "", "1", statusAdmin, pUser)
        End If


    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboGroup.Value, cboProject.Value, "", "2", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboGroup.Value, cboProject.Value, "", "2", statusAdmin, pUser)
        End If
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        'If e.VisibleIndex = 0 Then
        'If clsItemListSourcingDB.checkStatusGrouping(cboProject.Value, cboGroup.Value, cboCommodity.Value, Grid.GetRowValues(e.VisibleIndex, "Part_No")) = "1" Then
        '    If e.DataColumn.FieldName = "Status_Group" Then
        '        e.Cell.Text = ""
        '        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        '        e.Cell.Attributes.Add("readonly", "true")
        '    End If
        'End If

        If e.DataColumn.FieldName = "Status_Group" Then
            If e.GetValue("Status_Group") = 1 Then
                e.Cell.Text = ""
                e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
                e.Cell.Attributes.Add("readonly", "true")
            End If
        End If

        'End If
    End Sub

#End Region




End Class
﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Gate_I_Check_Sheet_Approval

    '''<summary>
    '''btnBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBack As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''btnDownload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDownload As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''btnComplete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnComplete As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''SqlDataSource1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSource1 As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''cbupload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbupload As Global.DevExpress.Web.ASPxCallback.ASPxCallback

    '''<summary>
    '''SqlDataSource2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSource2 As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''GridExporter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridExporter As Global.DevExpress.Web.ASPxGridView.Export.ASPxGridViewExporter

    '''<summary>
    '''cbMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbMessage As Global.DevExpress.Web.ASPxCallback.ASPxCallback

    '''<summary>
    '''cbkDrawingComplete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbkDrawingComplete As Global.DevExpress.Web.ASPxCallback.ASPxCallback

    '''<summary>
    '''cbSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbSave As Global.DevExpress.Web.ASPxCallback.ASPxCallback

    '''<summary>
    '''cbSpecInformation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbSpecInformation As Global.DevExpress.Web.ASPxCallback.ASPxCallback

    '''<summary>
    '''cbComplete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbComplete As Global.DevExpress.Web.ASPxCallback.ASPxCallback

    '''<summary>
    '''pageControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pageControl As Global.DevExpress.Web.ASPxTabControl.ASPxPageControl

    '''<summary>
    '''ContentControl0 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ContentControl0 As Global.DevExpress.Web.ASPxClasses.ContentControl

    '''<summary>
    '''Grid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Grid As Global.DevExpress.Web.ASPxGridView.ASPxGridView

    '''<summary>
    '''ContentControl2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ContentControl2 As Global.DevExpress.Web.ASPxClasses.ContentControl

    '''<summary>
    '''lblBaseDrawing control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBaseDrawing As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboBaseDrawing control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboBaseDrawing As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''lblSOR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSOR As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboSOR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSOR As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''lblMaterial control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMaterial As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''txtMaterial control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMaterial As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''lblProcess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProcess As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''txtProcess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProcess As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''lblCheckDevelopmentSchedule control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCheckDevelopmentSchedule As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboDevSchedule control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboDevSchedule As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''lblAdditionalInformation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAdditionalInformation As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''txtAdditionalInformation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAdditionalInformation As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''lblKSHAttendance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblKSHAttendance As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''KHSAttendance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents KHSAttendance As Global.DevExpress.Web.ASPxEditors.ASPxDateEdit

    '''<summary>
    '''ASPxCallback1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxCallback1 As Global.DevExpress.Web.ASPxCallback.ASPxCallback

    '''<summary>
    '''GridCostControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridCostControl As Global.DevExpress.Web.ASPxGridView.ASPxGridView

    '''<summary>
    '''lblCostControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCostControl As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''sdsCC control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents sdsCC As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''sdsDepartment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents sdsDepartment As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''ContentControl3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ContentControl3 As Global.DevExpress.Web.ASPxClasses.ContentControl

    '''<summary>
    '''GridBidderList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridBidderList As Global.DevExpress.Web.ASPxGridView.ASPxGridView

    '''<summary>
    '''sdsSupplier control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents sdsSupplier As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''ContentControl4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ContentControl4 As Global.DevExpress.Web.ASPxClasses.ContentControl

    '''<summary>
    '''GridSourcing control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridSourcing As Global.DevExpress.Web.ASPxGridView.ASPxGridView

    '''<summary>
    '''Master property.
    '''</summary>
    '''<remarks>
    '''Auto-generated property.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As IAMI_EPIC2.Site
        Get
            Return CType(MyBase.Master, IAMI_EPIC2.Site)
        End Get
    End Property
End Class

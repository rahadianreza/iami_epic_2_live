﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data

Public Class Material_RubberDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
    Dim strMaterialType As String = ""
    Dim strSupplier As String = ""
    Dim strPeriod As String = ""
    Dim strMaterialCode As String = ""
    Dim strGroupItem As String = ""
    Dim strCategory As String = ""
#End Region
    Public Sub FillCombo(pMaterialType, pMaterialCode, pGroupItem)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsAll_MaterialDB.GetDataCombo("MaterialType", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboMaterialType.Items.Add(Trim(ds.Tables(0).Rows(i)("MaterialTypeCode") & ""))
            Next
        End If

        Dim dsSupp As DataSet
        dsSupp = clsAll_MaterialDB.GetDataCombo("Supplier", pMaterialType, pErr)
        If pErr = "" Then
             cboSupplier.DataSource = dsSupp
            cboSupplier.DataBind()
        End If


        Dim dsMC As DataSet
        dsMC = clsAll_MaterialDB.GetDataCombo("MaterialCode", pErr)
        If pErr = "" Then
            For i = 0 To dsMC.Tables(0).Rows.Count - 1
                cboMaterialCode.Items.Add(Trim(dsMC.Tables(0).Rows(i)("MaterialCode") & ""))
            Next
        End If

        Dim dsGroup As DataSet
        dsGroup = clsAll_MaterialDB.GetDataCombo("GroupItem", pMaterialCode, pErr)
        If pErr = "" Then
            ' For i = 0 To dsGroup.Tables(0).Rows.Count - 1
            cboGroupItem.DataSource = dsGroup
            cboGroupItem.DataBind()  'Items.Add(Trim(dsGroup.Tables(0).Rows(i)("GroupItem") & ""))
            'Next
        End If

        Dim dsCategory As DataSet
        dsCategory = clsAll_MaterialDB.GetDataCombo("Category", pGroupItem, pErr)
        If pErr = "" Then
            cboCategory.DataSource = dsCategory
            cboCategory.DataBind()
            'For i = 0 To dsCategory.Tables(0).Rows.Count - 1
            '    cboCategory.Items.Add(Trim(dsCategory.Tables(0).Rows(i)("Category") & ""))
            'Next
        End If

        Dim dscurr As DataSet
        dscurr = clsAll_MaterialDB.GetDataCombo("Currency", pErr)
        If pErr = "" Then
            For i = 0 To dscurr.Tables(0).Rows.Count - 1
                cboCurrency.Items.Add(Trim(dscurr.Tables(0).Rows(i)("Currency") & ""))
            Next
        End If



    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A180")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        ' statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        Dim str As String = Request.QueryString("ID")
        Dim script As String = ""
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If str <> "" Then
                btnSubmit.Text = "Update"

                script = "cboMaterialType.SetEnabled(false);" & vbCrLf & _
                         "cboSupplier.SetEnabled(false);" & vbCrLf & _
                         "cboMaterialCode.SetEnabled(false);" & vbCrLf & _
                         "txtPeriod.SetEnabled(false);" & vbCrLf & _
                         "cboGroupItem.SetEnabled(false);" & vbCrLf & _
                         "cboCategory.SetEnabled(false);"

                ScriptManager.RegisterStartupScript(cboMaterialType, cboMaterialType.GetType(), "cboMaterialType", script, True)
                ScriptManager.RegisterStartupScript(btnClear, btnClear.GetType(), "btnClear", "btnClear.SetEnabled(false);", True)

                strMaterialType = str.Split("|")(0)
                strSupplier = str.Split("|")(1)
                strPeriod = str.Split("|")(2)
                strMaterialCode = str.Split("|")(3)
                strGroupItem = str.Split("|")(4)
                strCategory = str.Split("|")(5)

                'untuk fill combobox dari database
                FillCombo(strMaterialType, strMaterialCode, strGroupItem)

                Dim ds As DataSet = clsAll_MaterialDB.getDetailRubber(strMaterialType, strSupplier, strPeriod, strMaterialCode, strGroupItem, strCategory, pUser)
                If ds.Tables(0).Rows.Count > 0 Then

                    cboMaterialType.SelectedIndex = cboMaterialType.Items.IndexOf(cboMaterialType.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("MaterialTypeCode") & "")))
                    cboSupplier.SelectedIndex = cboSupplier.Items.IndexOf(cboSupplier.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("SupplierCode") & "")))
                    txtPeriod.Value = CDate(ds.Tables(0).Rows(0)("Period").ToString())
                    cboMaterialCode.SelectedIndex = cboMaterialCode.Items.IndexOf(cboMaterialCode.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("MaterialCode") & "")))
                    cboGroupItem.SelectedIndex = cboGroupItem.Items.IndexOf(cboGroupItem.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("GroupItem") & "")))
                    cboCategory.SelectedIndex = cboCategory.Items.IndexOf(cboCategory.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Category") & "")))
                    cboCurrency.SelectedIndex = cboCurrency.Items.IndexOf(cboCurrency.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Currency") & "")))
                    txtPrice.Text = ds.Tables(0).Rows(0)("Price").ToString()

                End If

            Else
                script = "cboMaterialType.SetEnabled(true);" & vbCrLf & _
                      "cboSupplier.SetEnabled(true);" & vbCrLf & _
                      "cboMaterialCode.SetEnabled(true);" & vbCrLf & _
                      "txtPeriod.SetEnabled(true);" & vbCrLf & _
                      "cboGroupItem.SetEnabled(true);" & vbCrLf & _
                      "cboCategory.SetEnabled(true);"

                ScriptManager.RegisterStartupScript(cboMaterialType, cboMaterialType.GetType(), "cboMaterialType", script, True)
                txtPeriod.Value = Now

            End If
        End If

    End Sub

    Private Sub up_FillComboSupplier(pMaterialType As String, Optional ByRef pErr As String = "")
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsAll_MaterialDB.GetDataCombo("Supplier", pMaterialType, pmsg)
        If pmsg = "" Then
            cboSupplier.DataSource = ds
            cboSupplier.DataBind()
        End If
    End Sub
    Private Sub cboSupplier_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSupplier.Callback
        Dim pMaterialType As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        up_FillComboSupplier(pMaterialType, errmsg)

    End Sub

    Private Sub cboGroupItem_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupItem.Callback
        Dim pMaterialCode As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsAll_MaterialDB.GetDataCombo("GroupItem", pMaterialCode, pmsg)
        If pmsg = "" Then
            cboGroupItem.DataSource = ds
            cboGroupItem.DataBind()

        End If

    End Sub

    Private Sub cboCategory_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCategory.Callback
        Dim pGroupItem As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsAll_MaterialDB.GetDataCombo("Category", pGroupItem, pmsg)
        If pmsg = "" Then
            cboCategory.DataSource = ds
            cboCategory.DataBind()

        End If

    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Material_Rubber.aspx")
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"

    End Sub

    Protected Sub cbSave_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSave.Callback
        Dim pErr As String = ""
        Dim pFunction As String = Split(e.Parameter, "|")(0)

        Dim vMaterialType As String = cboMaterialType.Value
        Dim vSupplier As String = cboSupplier.Value
        Dim vPeriod As String = Format(txtPeriod.Value, "yyyy-MM") & "-01"
        Dim vMaterialCode As String = cboMaterialCode.Value
        Dim vGroupItem As String = cboGroupItem.Value
        Dim vCategory As String = cboCategory.Value
        Dim vCurrency As String = cboCurrency.Value
        Dim vPrice As Decimal = txtPrice.Text

        If pFunction = "Save" Then
            'If IsNothing(Request.QueryString("ID")) Then

            clsAll_MaterialDB.InsertDataRubber(vMaterialType, vSupplier, vPeriod, vMaterialCode, vGroupItem, vCategory, vCurrency, vPrice, pUser, pErr)
            If pErr = "" Then
                cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
                cbSave.JSProperties("cpButtonText") = "Update"
            Else
                cbSave.JSProperties("cpMessage") = pErr

            End If
        Else
            If IsNothing(Request.QueryString("ID")) Then
                clsAll_MaterialDB.UpdateDataRubber(vMaterialType, vSupplier, vPeriod, vMaterialCode, vGroupItem, vCategory, vCurrency, vPrice, pUser, pErr)
                If pErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                    cbSave.JSProperties("cpButtonText") = "Update"
                End If
            Else
                Dim str As String = Request.QueryString("ID")
                strMaterialType = str.Split("|")(0)
                strSupplier = str.Split("|")(1)
                strPeriod = str.Split("|")(2)
                strMaterialCode = str.Split("|")(3)
                strGroupItem = str.Split("|")(4)

                clsAll_MaterialDB.UpdateDataRubber(vMaterialType, vSupplier, vPeriod, vMaterialCode, vGroupItem, vCategory, vCurrency, vPrice, pUser, pErr)
                If pErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                    cbSave.JSProperties("cpButtonText") = "Update"
                End If


            End If


        End If

    End Sub
End Class
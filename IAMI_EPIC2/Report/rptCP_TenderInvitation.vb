﻿Imports System.Drawing.Printing

Public Class rptCP_TenderInvitation
    Private detailRowCount As Integer = 0

    Private Sub Detail_BeforePrint(sender As Object, e As PrintEventArgs) Handles Detail.BeforePrint
        ' Dim i As Integer = System.Threading.Interlocked.Increment(detailRowCount)
        Dim a As String = GetPreviousColumnValue("Supplier_Name")
        Dim b As String = GetCurrentColumnValue("Supplier_Name")

        If System.Threading.Interlocked.Increment(detailRowCount) = 11 Then
            'If a <> b AndAlso (Detail.Report.CurrentRowIndex Mod 10 = 0) Then
            XrPageBreak1.Visible = True
        Else
            '    'GroupFooter1.Visible = False
            XrPageBreak1.Visible = False

        End If
    End Sub

    ''    For i As Integer = 0 To RecordCount - 1
    ''        If (i >= 4) Then
    ''            If ((i + 1) Mod 5 > 0) Then
    ''                Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.None
    ''            ElseIf ((i + 1) Mod 5 = 0) Then
    ''                If Detail.Report.RowCount > RecordCount And RecordCount = 5 Then
    ''                    Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
    ''                    'If Detail.Report.RowCount - 1 = RecordCount Then
    ''                    RecordCount = 0
    ''                    'End If
    ''                Else
    ''                    Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.None
    ''                End If
    ''            Else

    ''                Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
    ''                RecordCount = 0
    ''            End If

    ''        Else
    ''            Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.None
    ''        End If

    ''    Next
    ''End Sub

    Private Sub GroupHeader1_BeforePrint(sender As Object, e As PrintEventArgs) Handles GroupHeader1.BeforePrint

        If (Detail.Report.CurrentRowIndex = 0 And Detail.Report.CurrentRowIndex Mod 10 = 0) Then
            GroupHeader1.Visible = True
            Exit Sub
        End If

        Dim a As String = GetPreviousColumnValue("Supplier_Name")
        Dim b As String = GetCurrentColumnValue("Supplier_Name")

        ' If (Detail.Report.CurrentRowIndex < Detail.Report.RowCount - 1) Then
        'GroupHeader1.Visible = True
        'Else
        'If Detail.Report.CurrentRowIndex Mod 10 = 0 And detailRowCount <> 0 Then
        'If detailRowCount Mod 10 = 0 Then
        '    GroupHeader1.Visible = True
        'Else
        '    GroupHeader1.Visible = False
        'End If

        detailRowCount = 0
        'End If
    End Sub

    Private Sub GroupFooter1_AfterPrint(sender As System.Object, e As System.EventArgs) Handles GroupFooter1.BeforePrint
        Dim a As String = GetPreviousColumnValue("Supplier_Name")
        Dim b As String = GetCurrentColumnValue("Supplier_Name")
        If (Detail.Report.CurrentRowIndex = 0 And Detail.Report.CurrentRowIndex Mod 10 = 0) Then
            Exit Sub
        End If

        GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
    End Sub

    'Private Sub XrLabel1_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel1.BeforePrint
    '    Dim a As String = GetPreviousColumnValue("Supplier_Name")
    '    Dim b As String = GetCurrentColumnValue("Supplier_Name")

    '    e.Cancel = a <> b Or CurrentRowIndex = 0
    'End Sub
End Class
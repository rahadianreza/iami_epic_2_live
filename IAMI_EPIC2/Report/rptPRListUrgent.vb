﻿Public Class rptPRListUrgent
    Private Sub GroupHeader1_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs) Handles GroupHeader1.BeforePrint
        If (Detail.Report.CurrentRowIndex = 0 And Detail.Report.CurrentRowIndex Mod 10 = 0) Then
            GroupHeader1.Visible = True
            Exit Sub
        End If

        If (Detail.Report.CurrentRowIndex < Detail.Report.RowCount - 1) Then
            GroupHeader1.Visible = True
        Else
            If Detail.Report.RowCount Mod 10 <> 0 Then
                GroupHeader1.Visible = True
            Else
                GroupHeader1.Visible = False
            End If

        End If

    End Sub
End Class
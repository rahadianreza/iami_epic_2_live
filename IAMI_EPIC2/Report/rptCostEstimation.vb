﻿Imports DevExpress.XtraReports.UI

Public Class rptCostEstimation


    Dim RecordCount = 0
    Dim detailCount = 0

    'Private Sub xrPanel1_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrPanel1.BeforePrint
    '    e.Cancel = Convert.ToInt32(Me.GetCurrentColumnValue("Material_No")) = 0
    'End Sub

    Private Sub XrSubreport1_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs) Handles XrSubreport1.BeforePrint
        Dim xrSubReport As XRSubreport = DirectCast(sender, XRSubreport)
        Dim subRep As rptCostEstimation_Resume = TryCast(xrSubReport.ReportSource, rptCostEstimation_Resume)
        subRep.Parameters("CENumber").Value = CStr(GetCurrentColumnValue("CE_Number"))
        subRep.Parameters("Rev").Value = CStr(GetCurrentColumnValue("Revision_No"))
        subRep.ApplyFiltering()
    End Sub

    'Private Sub Detail_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs) Handles Detail.BeforePrint
    '    RecordCount += 1
    '    For i As Integer = 0 To RecordCount - 1
    '        If (i >= 4) Then
    '            If ((i + 1) Mod 5 > 0) Then
    '                Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.None
    '            ElseIf ((i + 1) Mod 5 = 0) Then
    '                If Detail.Report.RowCount > RecordCount And RecordCount = 5 Then
    '                    Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
    '                    'If Detail.Report.RowCount - 1 = RecordCount Then
    '                    RecordCount = 0
    '                    'End If
    '                Else
    '                    Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.None
    '                End If
    '            Else

    '                Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
    '                RecordCount = 0
    '            End If

    '        Else
    '            Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.None
    '        End If

    '    Next
    '    'If Me.CurrentRowIndex Mod 4 = 0 And Me.CurrentRowIndex <> 0 Then

    '    'End If
    'End Sub

    'Private Sub GroupHeaderBand1_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs) Handles GroupHeader1.BeforePrint
    '    GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
    '    GroupHeader1.RepeatEveryPage = True
    '    'RecordCount = 0
    'End Sub

    Private Sub GroupHeader1_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs) Handles GroupHeader1.BeforePrint
        If (Detail.Report.CurrentRowIndex = 0 And Detail.Report.CurrentRowIndex Mod 10 = 0) Then
            GroupHeader1.Visible = True
            Exit Sub
        End If

        If (Detail.Report.CurrentRowIndex < Detail.Report.RowCount - 1) Then
            GroupHeader1.Visible = True
        Else
            If Detail.Report.RowCount Mod 10 <> 0 Then
                GroupHeader1.Visible = True
            Else
                GroupHeader1.Visible = False
            End If

        End If

    End Sub
End Class
﻿Public Class rptIAPriceDetail
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub rptIAPriceDetail_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs) Handles MyBase.BeforePrint
        ' XrLabel2.Text = CENumber.Value

        Dim sqlQuery = SqlDataSource1.Queries(0)
        sqlQuery.Parameters(0).Type = GetType(String)
        sqlQuery.Parameters(0).Value = IANumber.Value
        sqlQuery.Parameters(1).Type = GetType(String)
        sqlQuery.Parameters(1).Value = CENumber.Value
        sqlQuery.Parameters(2).Type = GetType(String)
        sqlQuery.Parameters(2).Value = Rev.Value
        SqlDataSource1.Fill()
    End Sub


    Private Sub GroupHeader1_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs) Handles GroupHeader1.BeforePrint
        If (Detail.Report.CurrentRowIndex = 0 And Detail.Report.CurrentRowIndex Mod 20 = 0) Then
            GroupHeader1.Visible = True
            Exit Sub
        End If

        If (Detail.Report.CurrentRowIndex < Detail.Report.RowCount - 1) Then
            GroupHeader1.Visible = True
        Else
            If Detail.Report.RowCount Mod 20 <> 0 Then
                GroupHeader1.Visible = True
            Else
                GroupHeader1.Visible = False
            End If

        End If

    End Sub

End Class
﻿Imports DevExpress.XtraReports.UI
Public Class rptIAPrice

    Private Sub XrSubreport1_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs) Handles XrSubreport1.BeforePrint
        Dim xrSubReport As XRSubreport = DirectCast(sender, XRSubreport)
        Dim subRep As rptIAPriceDetail = TryCast(xrSubReport.ReportSource, rptIAPriceDetail)
        subRep.Parameters("IANumber").Value = CStr(GetCurrentColumnValue("IA_Number"))
        subRep.Parameters("CENumber").Value = CStr(GetCurrentColumnValue("CE_Number"))
        subRep.Parameters("Rev").Value = CStr(GetCurrentColumnValue("Revision_No"))
        subRep.ApplyFiltering()
    End Sub

    Private Sub GroupHeader1_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs) Handles GroupHeader1.BeforePrint
        If (Detail.Report.CurrentRowIndex = 0 And Detail.Report.CurrentRowIndex Mod 10 = 0) Then
            GroupHeader1.Visible = True
            Exit Sub
        End If

        If (Detail.Report.CurrentRowIndex < Detail.Report.RowCount - 1) Then
            GroupHeader1.Visible = True
        Else
            If Detail.Report.RowCount Mod 10 <> 0 Then
                GroupHeader1.Visible = True
            Else
                GroupHeader1.Visible = False
            End If

        End If

    End Sub

End Class
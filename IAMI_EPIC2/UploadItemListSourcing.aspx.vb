﻿Imports System.IO
Imports DevExpress.Web.ASPxUploadControl
Imports OfficeOpenXml
Imports System.Transactions

Public Class UploadItemListSourcing
    Inherits System.Web.UI.Page
#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""

    Dim FilePath As String = ""
    Dim FileName As String = ""
    Dim FileExt As String = ""
    Dim Ext As String = ""
    Dim aMsg As String = ""
#End Region

#Region "Control Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("I030")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            up_FillComboProjectType(cboProjectType, statusAdmin, pUser, "ProjectType")
        Else
            Ext = Server.MapPath("")
        End If
    End Sub

    Private Sub up_FillComboProjectType(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                           pAdminStatus As String, pUserID As String, _
                           Optional ByRef pGroup As String = "", _
                           Optional ByRef pParent As String = "", _
                           Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboOnlyPT01PT02(pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_FillComboProject(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                           pAdminStatus As String, pUserID As String, projtype As String, _
                           Optional ByRef pGroup As String = "", _
                           Optional ByRef pParent As String = "", _
                           Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboProject(projtype, pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub cboProject_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboProject.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProjectType As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboProject(cboProject, statusAdmin, pUser, "")

        ElseIf pFunction = "filter" Then
            up_FillComboProject(cboProject, statusAdmin, pUser, pProjectType)
        End If

        If cboProject.Items.Count > 0 Then
            cboProject.SelectedIndex = 0
        End If
    End Sub

    Protected Sub Uploader_FileUploadComplete(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)
        Try
            'e.CallbackData = SavePostedFiles(e.UploadedFile)
        Catch ex As Exception
            e.IsValid = False
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 0)
        End Try
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        cbMessage.JSProperties("cp_Message") = ErrMsg
    End Sub

    Private Sub importExcel()
        Dim pErr As String = ""
        Try
            If Uploader.HasFile Then
                FileName = Path.GetFileName(Uploader.PostedFile.FileName)
                FileExt = Path.GetExtension(Uploader.PostedFile.FileName)
                'Ext = Mid(Ext, 1, Len(Ext) - 7)

                FilePath = Ext & "\Import\" & FileName

                Uploader.SaveAs(FilePath)

                'proses import from excel ke database
                uf_ImpotExcel(FilePath)


                Dim ls_Dir As New IO.DirectoryInfo(Ext & "\Import\")
                Dim ls_GetFile As IO.FileInfo() = ls_Dir.GetFiles()
                Dim ls_File As IO.FileInfo
                Dim li_CountDate As Long

                For Each ls_File In ls_GetFile
                    'keep file Import for 30 days
                    li_CountDate = DateDiff(DateInterval.Day, CDate(Format(ls_File.LastWriteTime, "MM/dd/yyyy")), CDate(Format(Now, "MM/dd/yyyy")))
                    If li_CountDate > 30 Then
                        File.Delete(ls_File.FullName)
                    End If
                Next
            End If


        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 1)
        End Try
    End Sub

    Private Function uf_ImpotExcel(ByVal pFileName As String, Optional ByRef perr As String = "") As String
        'Initialize of Excel
        Dim _itemListSourcing As New clsItemListSourcing
        Dim pbar As Integer
        Dim pNoError As Integer
        Dim pno As Integer
        Dim pCountVariant As Integer
        Dim pError As String = ""
        Dim VariantName As String = ""
        Dim closedummyflag As Integer = 0

        Try
            popUp.ShowOnPageLoad = True
            Dim fi As New FileInfo(pFileName)
            Dim dt As New DataTable
            Dim exl As New ExcelPackage(fi)
            Dim ws As ExcelWorksheet
            Dim tbl As New DataTable
            MemoMessage.Text = ""
            MemoMessage.Text = "No  " & "Error Message ||" & vbCrLf

            Dim wss As OfficeOpenXml.ExcelWorksheets
            Try
                wss = exl.Workbook.Worksheets
            Catch ex As Exception
                wss = exl.Workbook.Worksheets
            End Try

            Dim options As New TransactionOptions
            options.IsolationLevel = IsolationLevel.ReadCommitted
            options.Timeout = New TimeSpan(0, 45, 0)

            Using Scope As New TransactionScope(TransactionScopeOption.Required, options)
                ProgressBar.Value = 0
                ws = exl.Workbook.Worksheets(1)

                Dim pcount As Integer = 21
                Dim pdatadifferent As Boolean = False
                Dim xMsg As String = ""

                If pError <> "" Then
                    pNoError = CInt(pNoError + 1)
                    MemoMessage.Text = MemoMessage.Text & pNoError & " " & pError & " ||" & vbCrLf
                End If

                'Get All Variant Name
                For f As Integer = 0 To 36
                    If ws.Cells(5, 18 + f).Value <> "" Then
                        _itemListSourcing.Variant_Name += IIf(String.IsNullOrEmpty(ws.Cells(5, 18 + f).Value.ToString()), "0", ws.Cells(5, 18 + f).Value.ToString()).Replace(System.Environment.NewLine, "").Replace("  ", " ") + ","
                    Else
                        Exit For
                    End If
                Next

                'checking valid template
                If ws.Cells("C4").Value <> "GROUP" AndAlso ws.Cells("D4").Value <> "COMODITY" AndAlso ws.Cells("E4").Value <> "UPC" AndAlso _
                   ws.Cells("F4").Value <> "FNA" AndAlso ws.Cells("G4").Value <> "DTL UPC" AndAlso ws.Cells("H4").Value <> "DTL FNA" AndAlso _
                   ws.Cells("I4").Value <> "USG/SEQ" AndAlso ws.Cells("J4").Value <> "LVL" AndAlso ws.Cells("K4").Value <> "HAND" AndAlso _
                   ws.Cells("L4").Value <> "D/C" AndAlso ws.Cells("M4").Value <> "DWG NO" AndAlso ws.Cells("N4").Value <> "PART NO" Then
                    MemoMessage.Text = "Incorrect Excel Template"
                    Exit Function
                End If

                'proses insert per row
                For y = 6 To ws.Dimension.End.Row

                    Dim GroupID As String = IIf(String.IsNullOrEmpty(ws.Cells(y, 3).Value), "", ws.Cells(y, 3).Value.ToString)
                    Dim Commodity As String = IIf(String.IsNullOrEmpty(ws.Cells(y, 4).Value), "", ws.Cells(y, 4).Value.ToString)
                    Dim UPC As String = IIf(String.IsNullOrEmpty(ws.Cells(y, 5).Value), "", ws.Cells(y, 5).Value.ToString)
                    Dim Fna As String = IIf(String.IsNullOrEmpty(ws.Cells(y, 6).Value), "", ws.Cells(y, 6).Value.ToString)
                    Dim dtl_upc As String = IIf(String.IsNullOrEmpty(ws.Cells(y, 7).Value), "", ws.Cells(y, 7).Value.ToString)
                    Dim dtl_fna As String = IIf(String.IsNullOrEmpty(ws.Cells(y, 8).Value), "", ws.Cells(y, 8).Value.ToString)
                    Dim usg_Seq As String = IIf(String.IsNullOrEmpty(ws.Cells(y, 9).Value), "", ws.Cells(y, 9).Value.ToString)
                    Dim lvl As String = "" 'IIf(String.IsNullOrEmpty(ws.Cells(y, 10).Value), "", ws.Cells(y, 10).Value.ToString)
                    Dim hand As String = "" 'IIf(String.IsNullOrEmpty(ws.Cells(y, 11).Value), "", ws.Cells(y, 11).Value.ToString)
                    Dim D_C As String = "" 'IIf(String.IsNullOrEmpty(ws.Cells(y, 12).Value), "", ws.Cells(y, 12).Value.ToString)
                    Dim dwg_No As String = "" ' IIf(String.IsNullOrEmpty(ws.Cells(y, 13).Value), "", ws.Cells(y, 13).Value.ToString)
                    Dim Qty As String = ""

                    'Lvl
                    Try
                        If Not String.IsNullOrEmpty(ws.Cells(y, 10).Value.ToString) Then
                            lvl = ws.Cells(y, 10).Value.ToString
                        End If
                    Catch ex As Exception
                        lvl = ""
                    End Try

                    'hand
                    Try
                        If Not String.IsNullOrEmpty(ws.Cells(y, 11).Value.ToString) Then
                            hand = ws.Cells(y, 11).Value.ToString
                        End If
                    Catch ex As Exception
                        hand = ""
                    End Try

                    'D_C
                    Try
                        If Not String.IsNullOrEmpty(ws.Cells(y, 12).Value.ToString) Then
                            D_C = ws.Cells(y, 12).Value.ToString
                        End If
                    Catch ex As Exception
                        D_C = ""
                    End Try

                    'DWG No
                    Try
                        If Not String.IsNullOrEmpty(ws.Cells(y, 13).Value.ToString) Then
                            dwg_No = ws.Cells(y, 13).Value.ToString
                        End If
                    Catch ex As Exception
                        dwg_No = ""
                    End Try


                    'Qty
                    Try
                        If Not String.IsNullOrEmpty(ws.Cells(y, 17).Value.ToString) Then
                            Qty = ws.Cells(y, 17).Value.ToString
                        End If
                    Catch ex As Exception
                        Qty = ""
                    End Try



                    If GroupID = "" AndAlso Commodity = "" AndAlso UPC = "" AndAlso Fna = "" AndAlso _
                       dtl_upc = "" AndAlso dtl_fna = "" AndAlso usg_Seq = "" AndAlso lvl = "" AndAlso _
                       hand = "" AndAlso D_C = "" AndAlso dwg_No = "" AndAlso ws.Cells(y, 14).Value = "" Then
                        Exit For
                    End If

                    'If ws.Cells(y, 3).Value.ToString = "" AndAlso ws.Cells(y, 4).Value = "" AndAlso ws.Cells(y, 5).Value.ToString = "" AndAlso ws.Cells(y, 6).Value = "" AndAlso _
                    '   ws.Cells(y, 7).Value = "" AndAlso ws.Cells(y, 8).Value = "" AndAlso ws.Cells(y, 9).Value = "" AndAlso ws.Cells(y, 10).Value = "" AndAlso _
                    '   ws.Cells(y, 11).Value = "" AndAlso ws.Cells(y, 12).Value = "" AndAlso ws.Cells(y, 13).Value = "" AndAlso ws.Cells(y, 14).Value = "" Then
                    '    Exit For
                    'End If


                    _itemListSourcing.Project_ID = cboProject.Value
                    _itemListSourcing.Group_ID = ws.Cells(y, 3).Value.ToString
                    _itemListSourcing.Commodity = ws.Cells(y, 4).Value
                    _itemListSourcing.Upc = ws.Cells(y, 5).Value.ToString
                    _itemListSourcing.Fna = ws.Cells(y, 6).Value
                    _itemListSourcing.Dtl_Upc = ws.Cells(y, 7).Value
                    _itemListSourcing.Dtl_Fna = ws.Cells(y, 8).Value
                    _itemListSourcing.Usg_Seq = ws.Cells(y, 9).Value
                    _itemListSourcing.Lvl = ws.Cells(y, 10).Value
                    _itemListSourcing.Hand = ws.Cells(y, 11).Value
                    _itemListSourcing.D_C = ws.Cells(y, 12).Value
                    _itemListSourcing.Dwg_No = ws.Cells(y, 13).Value
                    _itemListSourcing.Part_No = ws.Cells(y, 14).Value
                    _itemListSourcing.Part_No_9_Digit = ws.Cells(y, 15).Value
                    _itemListSourcing.Part_Name = ws.Cells(y, 16).Value
                    _itemListSourcing.Qty = ws.Cells(y, 17).Value

                    Dim dsnewVariant As DataSet

                    'checking all row valid or not null

                    For ydummy = 6 To ws.Dimension.End.Row
                        'Session("No2") = ws.Cells(ydummy, 2).Value
                        Dim _GroupID As String = IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 3).Value), "", ws.Cells(ydummy, 3).Value.ToString)
                        Dim _Commodity As String = IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 4).Value), "", ws.Cells(ydummy, 4).Value.ToString)
                        Dim _UPC As String = IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 5).Value), "", ws.Cells(ydummy, 5).Value.ToString)
                        Dim _Fna As String = IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 6).Value), "", ws.Cells(ydummy, 6).Value.ToString)
                        Dim _dtl_upc As String = IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 7).Value), "", ws.Cells(ydummy, 7).Value.ToString)
                        Dim _dtl_fna As String = IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 8).Value), "", ws.Cells(ydummy, 8).Value.ToString)
                        Dim _usg_Seq As String = IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 9).Value), "", ws.Cells(ydummy, 9).Value.ToString)
                        Dim _lvl As String = "" 'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 10).Value), "", ws.Cells(ydummy, 10).Value.ToString)
                        Dim _hand As String = "" 'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 11).Value), "", ws.Cells(ydummy, 11).Value.ToString)
                        Dim _D_C As String = "" 'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 12).Value), "", ws.Cells(ydummy, 12).Value.ToString)
                        Dim _dwg_No As String = "" 'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 13).Value), "", ws.Cells(ydummy, 13).Value.ToString)
                        Dim _Qty As String = ""

                        'Lvl
                        Try
                            If Not String.IsNullOrEmpty(ws.Cells(ydummy, 10).Value.ToString) Then
                                _lvl = ws.Cells(ydummy, 10).Value.ToString
                            End If
                        Catch ex As Exception
                            _lvl = ""
                        End Try

                        'hand
                        Try
                            If Not String.IsNullOrEmpty(ws.Cells(ydummy, 11).Value.ToString) Then
                                _hand = ws.Cells(ydummy, 11).Value.ToString
                            End If
                        Catch ex As Exception
                            _hand = ""
                        End Try

                        'D_C
                        Try
                            If Not String.IsNullOrEmpty(ws.Cells(ydummy, 12).Value.ToString) Then
                                _D_C = ws.Cells(ydummy, 12).Value.ToString
                            End If
                        Catch ex As Exception
                            _D_C = ""
                        End Try

                        'DWG No
                        Try
                            If Not String.IsNullOrEmpty(ws.Cells(ydummy, 13).Value.ToString) Then
                                _dwg_No = ws.Cells(ydummy, 13).Value.ToString
                            End If
                        Catch ex As Exception
                            _dwg_No = ""
                        End Try

                        'Qty
                        Try
                            If Not String.IsNullOrEmpty(ws.Cells(ydummy, 17).Value.ToString) Then
                                _Qty = ws.Cells(ydummy, 17).Value.ToString
                            End If
                        Catch ex As Exception
                            _Qty = ""
                        End Try


                        If _GroupID = "" AndAlso _Commodity = "" AndAlso _UPC = "" AndAlso _Fna = "" AndAlso _
                           _dtl_upc = "" AndAlso _dtl_fna = "" AndAlso _usg_Seq = "" AndAlso _lvl = "" AndAlso _
                           _hand = "" AndAlso _D_C = "" AndAlso _dwg_No = "" AndAlso ws.Cells(ydummy, 14).Value = "" Then
                            Exit For
                        End If

                        'If ws.Cells(ydummy, 3).Value.ToString = "" AndAlso ws.Cells(ydummy, 4).Value = "" AndAlso ws.Cells(ydummy, 5).Value.ToString = "" AndAlso ws.Cells(ydummy, 6).Value = "" AndAlso _
                        '   ws.Cells(ydummy, 7).Value = "" AndAlso ws.Cells(ydummy, 8).Value = "" AndAlso ws.Cells(ydummy, 9).Value = "" AndAlso ws.Cells(ydummy, 10).Value = "" AndAlso _
                        '   ws.Cells(ydummy, 11).Value = "" AndAlso ws.Cells(ydummy, 12).Value = "" AndAlso ws.Cells(ydummy, 13).Value = "" AndAlso ws.Cells(ydummy, 14).Value = "" Then
                        '    Exit For
                        'End If


                        For valueNull As Integer = 3 To ws.Dimension.End.Column
                            If ws.Cells(4, valueNull).Value <> "" Then

                                If String.IsNullOrEmpty(ws.Cells(ydummy, valueNull).Value) Then
                                    aMsg += validationOfExcelData(ws.Cells(ydummy, valueNull).Value, ws.Cells(4, valueNull).Value, ydummy)
                                End If

                                If ws.Cells(4, valueNull).Value = "LATEST COST REPORT" Then
                                    If Not String.IsNullOrEmpty(ws.Cells(ydummy, valueNull).Value) Then
                                        Dim value As String = ws.Cells(ydummy, valueNull).Value

                                        For i = 0 To value.Length - 1
                                            If Convert.ToInt32(value.Chars(i)) < 48 Or Convert.ToInt32(value.Chars(i)) > 57 Then
                                                'Return "Row : " + CStr(ydummy) + " - " + Message + " only numeric ||" & vbCrLf
                                                aMsg += "Row : " + CStr(ydummy) + " - " + "LATEST COST REPORT Only numeric ||" & vbCrLf
                                            End If
                                        Next

                                        If value > 10000000 Then
                                            'Return "Row : " + CStr(ydummy) + " - " + Message + "  maximum 10.000.000 ||" & vbCrLf
                                            aMsg += "Row : " + CStr(ydummy) + " - " + "LATEST COST REPORT Maximum Amount 10.000.000 ||" & vbCrLf
                                        End If
                                    End If


                                End If
                            Else
                                If ws.Cells(4, valueNull).Value = "DRAWING RECEIVE" Or ws.Cells(4, valueNull).Value = "DRAWING CHECK" _
                                  Or ws.Cells(4, valueNull).Value = "LATEST COST REPORT" Or ws.Cells(4, valueNull).Value = "QTY" Or ws.Cells(4, valueNull).Value = "PIC" Or ws.Cells(4, valueNull).Value = "CURRENCY" _
                                  Or ws.Cells(4, valueNull).Value = "GROUP" Or ws.Cells(4, valueNull).Value = "COMODITY" Then
                                    aMsg += validationOfExcelData(ws.Cells(ydummy, valueNull).Value, ws.Cells(4, valueNull).Value, ydummy)
                                End If
                            End If

                        Next

                        For h As Integer = 0 To 36
                            If ws.Cells(4, 18 + h).Value = "PIC" Then
                                Exit For
                            Else
                                If String.IsNullOrEmpty(ws.Cells(y, 18 + h).Value) Then
                                    aMsg += validationOfExcelData(IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 18 + h).Value), "0", ws.Cells(ydummy, 18 + h).Value), "Variant " + ws.Cells(5, 18 + h).Value, ydummy)
                                End If

                                'For i = 0 To ws.Cells(y, 18 + h).Value.Length - 1
                                '    If Convert.ToInt32(ws.Cells(y, 18 + h).Value.Chars(i)) < 48 Or Convert.ToInt32(ws.Cells(y, 18 + h).Value.Chars(i)) > 57 Then
                                '        aMsg += "Row : " + CStr(y) + " - " + Message + " only numeric ||" & vbCrLf
                                '        Exit For
                                '    End If
                                'Next

                                For Each c As Char In CStr(IIf(String.IsNullOrEmpty(ws.Cells(y, 18 + h).Value), 0, ws.Cells(y, 18 + h).Value))
                                    If Not Char.IsDigit(c) Then
                                        aMsg += "Row : " + CStr(y) + " - " + "Variant " + ws.Cells(5, 18 + h).Value + " only numeric ||" & vbCrLf
                                        Exit For
                                    End If
                                Next

                            End If

                        Next

                        If closedummyflag = 0 Then
                            aMsg += validationOfSimilarData(cboProject.Value, _
                                                            _GroupID, _Commodity, IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 14).Value), "", ws.Cells(ydummy, 14).Value.ToString), _
                                                            _UPC, _Fna, _dtl_upc, _dtl_fna, _usg_Seq, _Qty, pUser, ydummy)
                            'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 3).Value), "", ws.Cells(ydummy, 3).Value.ToString), _
                            'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 4).Value), "", ws.Cells(ydummy, 4).Value.ToString), _
                            'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 14).Value), "", ws.Cells(ydummy, 14).Value.ToString), _
                            'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 5).Value), "", ws.Cells(ydummy, 5).Value.ToString), _
                            'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 6).Value), "", ws.Cells(ydummy, 6).Value.ToString), _
                            'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 7).Value), "", ws.Cells(ydummy, 7).Value.ToString), _
                            'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 8).Value), "", ws.Cells(ydummy, 8).Value.ToString), _
                            'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 9).Value), "", ws.Cells(ydummy, 9).Value.ToString), _
                            'IIf(String.IsNullOrEmpty(ws.Cells(ydummy, 17).Value), "", ws.Cells(ydummy, 17).Value.ToString), _
                            ' pUser, ydummy)
                        End If

                    Next
                    closedummyflag = 1

                    If aMsg <> "" Then
                        MemoMessage.Text = aMsg
                        popUp.ShowOnPageLoad = False
                        clsItemListSourcingDB.getExistsTempUpload("", "", "", "", "", "", "", "", "", "0", "2", pUser)
                        Scope.Dispose()
                        Exit Function
                    End If

                    dsnewVariant = clsItemListSourcingDB.getOrderVariant(_itemListSourcing.Variant_Name, cboProject.Value, _itemListSourcing)

                    Dim variantFlag As Integer = 0
                    Dim manyColumnVariant As Integer = 0
                    For x As Integer = 0 To 36
                        If variantFlag = 0 AndAlso ws.Cells(5, 18 + x).Value <> "" Then
                            manyColumnVariant = manyColumnVariant + 1

                            'get countorder of variant
                            Dim currentOrder As Integer
                            For n As Integer = 0 To dsnewVariant.Tables(0).Rows.Count - 1
                                If dsnewVariant.Tables(0).Rows(n)(1).ToString = ws.Cells(5, 18 + x).Value Then
                                    currentOrder = dsnewVariant.Tables(0).Rows(n)(0).ToString
                                    Exit For
                                End If
                            Next

                            'insert variant
                            Select Case currentOrder
                                Case 1
                                    _itemListSourcing.Variant1 = ws.Cells(y, 18 + x).Value
                                Case 2
                                    _itemListSourcing.Variant2 = ws.Cells(y, 18 + x).Value
                                Case 3
                                    _itemListSourcing.Variant3 = ws.Cells(y, 18 + x).Value
                                Case 4
                                    _itemListSourcing.Variant4 = ws.Cells(y, 18 + x).Value
                                Case 5
                                    _itemListSourcing.Variant5 = ws.Cells(y, 18 + x).Value
                                Case 6
                                    _itemListSourcing.Variant6 = ws.Cells(y, 18 + x).Value
                                Case 7
                                    _itemListSourcing.Variant7 = ws.Cells(y, 18 + x).Value
                                Case 8
                                    _itemListSourcing.Variant8 = ws.Cells(y, 18 + x).Value
                                Case 9
                                    _itemListSourcing.Variant9 = ws.Cells(y, 18 + x).Value
                                Case 10
                                    _itemListSourcing.Variant10 = ws.Cells(y, 18 + x).Value
                                Case 11
                                    _itemListSourcing.Variant11 = ws.Cells(y, 18 + x).Value
                                Case 12
                                    _itemListSourcing.Variant12 = ws.Cells(y, 18 + x).Value
                                Case 13
                                    _itemListSourcing.Variant13 = ws.Cells(y, 18 + x).Value
                                Case 14
                                    _itemListSourcing.Variant14 = ws.Cells(y, 18 + x).Value
                                Case 15
                                    _itemListSourcing.Variant15 = ws.Cells(y, 18 + x).Value
                                Case 16
                                    _itemListSourcing.Variant16 = ws.Cells(y, 18 + x).Value
                                Case 17
                                    _itemListSourcing.Variant17 = ws.Cells(y, 18 + x).Value
                                Case 18
                                    _itemListSourcing.Variant18 = ws.Cells(y, 18 + x).Value
                                Case 19
                                    _itemListSourcing.Variant19 = ws.Cells(y, 18 + x).Value
                                Case 20
                                    _itemListSourcing.Variant20 = ws.Cells(y, 18 + x).Value
                                Case 21
                                    _itemListSourcing.Variant21 = ws.Cells(y, 18 + x).Value
                                Case 22
                                    _itemListSourcing.Variant22 = ws.Cells(y, 18 + x).Value
                                Case 23
                                    _itemListSourcing.Variant23 = ws.Cells(y, 18 + x).Value
                                Case 24
                                    _itemListSourcing.Variant24 = ws.Cells(y, 18 + x).Value
                                Case 25
                                    _itemListSourcing.Variant25 = ws.Cells(y, 18 + x).Value
                                Case 26
                                    _itemListSourcing.Variant26 = ws.Cells(y, 18 + x).Value
                                Case 27
                                    _itemListSourcing.Variant27 = ws.Cells(y, 18 + x).Value
                                Case 28
                                    _itemListSourcing.Variant28 = ws.Cells(y, 18 + x).Value
                                Case 29
                                    _itemListSourcing.Variant29 = ws.Cells(y, 18 + x).Value
                                Case 30
                                    _itemListSourcing.Variant30 = ws.Cells(y, 18 + x).Value
                                Case 31
                                    _itemListSourcing.Variant31 = ws.Cells(y, 18 + x).Value
                                Case 32
                                    _itemListSourcing.Variant32 = ws.Cells(y, 18 + x).Value
                                Case 33
                                    _itemListSourcing.Variant33 = ws.Cells(y, 18 + x).Value
                                Case 34
                                    _itemListSourcing.Variant34 = ws.Cells(y, 18 + x).Value
                                Case 35
                                    _itemListSourcing.Variant35 = ws.Cells(y, 18 + x).Value
                                Case Else
                                    _itemListSourcing.Variant36 = ws.Cells(y, 18 + x).Value
                            End Select
                        Else
                            variantFlag = 1
                            Exit For
                        End If
                    Next

                    _itemListSourcing.PIC = ws.Cells(y, 17 + manyColumnVariant + 1).Value
                    _itemListSourcing.Guide_Price = ws.Cells(y, 17 + manyColumnVariant + 2).Value
                    _itemListSourcing.Currency = ws.Cells(y, 17 + manyColumnVariant + 3).Value
                    _itemListSourcing.Jpn_Supplier = ws.Cells(y, 17 + manyColumnVariant + 4).Value
                    'If String.IsNullOrEmpty(ws.Cells(y, 17 + manyColumnVariant + 5).Value).ToString = "" Then
                    '    _itemListSourcing.Drawing_Receive = IsDBNull((ws.Cells(y, 17 + manyColumnVariant + 5).Value))
                    'Else
                    '    _itemListSourcing.Drawing_Receive = Format((ws.Cells(y, 17 + manyColumnVariant + 5).Value), "yyyy/MM/dd")
                    'End If
                    _itemListSourcing.Drawing_Receive = IIf(String.IsNullOrEmpty(ws.Cells(y, 17 + manyColumnVariant + 5).Value), ws.Cells(y, 17 + manyColumnVariant + 5).Value, Format((ws.Cells(y, 17 + manyColumnVariant + 5).Value), "yyyy/MM/dd"))
                    _itemListSourcing.Latest_Cost_Report = ws.Cells(y, 17 + manyColumnVariant + 6).Value
                    _itemListSourcing.Part_No_Oes = ws.Cells(y, 17 + manyColumnVariant + 7).Value
                    _itemListSourcing.SentToBuyer = ws.Cells(y, 17 + manyColumnVariant + 8).Value
                    _itemListSourcing.BuyerDrawingCheck = ws.Cells(y, 17 + manyColumnVariant + 9).Value
                    _itemListSourcing.Register_By = pUser

                    pError = ""

                    'Dim ds As DataSet = clsItemListSourcingDB.CheckingEPL(_itemListSourcing.Project_ID, _itemListSourcing.Part_No, _itemListSourcing.Upc, _
                    '                                                                _itemListSourcing.Fna, _itemListSourcing.Dtl_Upc, _itemListSourcing.Dtl_Fna, _
                    '                                                                _itemListSourcing.Usg_Seq, _itemListSourcing.Qty, _itemListSourcing.D_C)

                    'If ds.Tables(0).Rows(0)(0).ToString = "0" Then
                    '    xMsg += "Row : " + CStr(y) + " Data EPL Not Registered at Drawing Item List Sourcing ||" & vbCrLf
                    'End If

                    clsItemListSourcingDB.insertData(_itemListSourcing, pUser, pError)

                    If pError <> "" Then
                        pNoError = CInt(pNoError + 1)
                        MemoMessage.Text = MemoMessage.Text & pNoError & " " & pError & " ||" & vbCrLf
                        Scope.Dispose()
                    End If

                    If pNoError = 0 Then
                        ProgressBar.Value = (y * 100) / ws.Dimension.End.Row
                        Session("Looping1") = y
                    End If
                    ProgressBar.ShowPosition = True

                Next

                If xMsg <> "" Then
                    MemoMessage.Text = xMsg
                    popUp.ShowOnPageLoad = False
                    clsItemListSourcingDB.getExistsTempUpload("", "", "", "", "", "", "", "", "", "0", "2", pUser)
                    Scope.Dispose()
                    Exit Function
                End If

                If pNoError > 0 Then
                    BtnSubmit.ClientEnabled = True
                    popUp.ShowOnPageLoad = False
                    clsItemListSourcingDB.getExistsTempUpload("", "", "", "", "", "", "", "", "", "0", "2", pUser)
                    Scope.Dispose()
                    Exit Function
                End If

                clsItemListSourcingDB.getExistsTempUpload("", "", "", "", "", "", "", "", "", "0", "2", pUser)
                Scope.Complete()
                MemoMessage.Text = "Upload Item List Sourcing Successfully"
                popUp.ShowOnPageLoad = False
            End Using

        Catch ex As Exception
            popUp.ShowOnPageLoad = False
            MemoMessage.Text = ex.Message
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 1)

        End Try
    End Function

    Private Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSubmit.Click
        If validation() = True Then
            Uploader.Enabled = True
            ProgressBar.Value = 0
            importExcel()
            BtnSubmit.Enabled = True
            BtnSaveError.Enabled = True
        End If
    End Sub

    Private Function validation() As Boolean
        validation = True
        If cboProject.Text = "" Then
            show_error(MsgTypeEnum.ErrorMsg, "Please select Project Name", 1)
            cboProject.Focus()
            validation = False
        End If
    End Function

    Private Function validationOfExcelData(value As String, message As String, currentRow As Integer) As String
        If (message = "D/C" Or message = "GUIDE PRICE" Or message = "JPN SUPLIER" Or message = "DRAWING RECEIVE" Or message = "LATEST COST REPORT" Or message = "PART NO OES" Or message = "HAND" Or message = "BUYER DRAWING CHECK") Then
            '  Return ""
        Else

            If String.IsNullOrEmpty(value) Then
                Return "Row : " + CStr(currentRow) + " - " + message + " cannot be empty ||" & vbCrLf
            Else
                Return ""
            End If
        End If

        If (message = "GUIDE PRICE" Or message = "SEND TO BUYER" Or message = "DRAWING CHECK" _
        Or message = "QTY" Or message = "VARIANT") Then
            If String.IsNullOrEmpty(value) Then
                ' Return "Row : " + CStr(currentRow) + " - " + message + " only numeric ||" & vbCrLf
            Else
                For i = 0 To value.Length - 1
                    If Convert.ToInt32(value.Chars(i)) < 48 Or Convert.ToInt32(value.Chars(i)) > 57 Then
                        Return "Row : " + CStr(currentRow) + " - " + message + " only numeric ||" & vbCrLf
                    End If
                Next
            End If
        End If

        If (message = "LATEST COST REPORT") Then
            If String.IsNullOrEmpty(value) Then
                Return ""
            End If
            For i = 0 To value.Length - 1
                If Convert.ToInt32(value.Chars(i)) < 48 Or Convert.ToInt32(value.Chars(i)) > 57 Then
                    Return "Row : " + CStr(currentRow) + " - " + message + " only numeric ||" & vbCrLf
                End If
            Next

            If value > 10000000 Then
                Return "Row : " + CStr(currentRow) + " - " + message + "  maximum 10.000.000 ||" & vbCrLf
            End If

        End If

        If (message = "DRAWING RECEIVE") Then
            If value <> "" Then
                Dim isValidDate As Boolean = IsDate(value)
                If isValidDate = False Then
                    Return "Row : " + CStr(currentRow) + " - " + message + " only date (#Format YYYY/MM/DD) ||" & vbCrLf
                End If
            End If

        End If

        If (message = "CURRENCY") Then
            Dim isValidDate As Boolean = clsItemListSourcingDB.checkCurrency(value)
            If isValidDate = False Then
                Return "Row : " + CStr(currentRow) + " - " + message + " invalid  ||" & vbCrLf
            End If
        End If

        If (message = "PIC") Then
            Dim isValidDate As Boolean = clsItemListSourcingDB.checkUserPIC(value)
            If isValidDate = False Then
                Return "Row : " + CStr(currentRow) + " - " + message + " invalid  ||" & vbCrLf
            End If
        End If

    End Function

    Private Function validationOfSimilarData(_projectId As String, _groupId As String, _commodity As String, _partno As String, _Upc As String, _Fna As String, _
                                             _Dtl_Upc As String, _Dtl_Fna As String, _UsgSeq As String, _Qty As Double, _
                                             _pUser As String, _currentRow As Integer) As String
        Dim ds As DataSet = clsItemListSourcingDB.getExistsTempUpload(_projectId, _groupId, _commodity, _partno, _Upc, _Fna, _Dtl_Upc, _Dtl_Fna, _UsgSeq, _Qty, "1", _pUser)
        If ds.Tables(0).Rows.Count > 0 Then
            Return "Row " + CStr(_currentRow) + " : Part No " + _partno + " already exists on this excel." + vbCrLf
        Else
            Return Nothing
        End If
    End Function

    Protected Sub BtnSaveError_Click(sender As Object, e As EventArgs) Handles BtnSaveError.Click
        up_Excel()
    End Sub

    Private Sub up_Excel()
        Dim NameHeader As String = ""
        If MemoMessage.Text = "" Then
            Exit Sub
        End If
        Using Pck As New ExcelPackage
            Dim ws As ExcelWorksheet = Pck.Workbook.Worksheets.Add("Sheet1")
            With ws
                .Cells(1, 1, 1, 1).Value = Split(MemoMessage.Text, "||")(0)
                For iRow = 1 To Split(MemoMessage.Text, "||").Count - 1
                    Dim r As Integer = iRow + 1
                    .Cells(r, 1, r, 1).Value = Split(MemoMessage.Text, "||")(iRow)
                Next

                Dim rg As OfficeOpenXml.ExcelRange = .Cells(1, 1, Split(MemoMessage.Text, "||").Count, 1)
                rg.Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Font.Name = "Tahoma"
                rg.Style.Font.Size = 10

                Dim rgHeader As OfficeOpenXml.ExcelRange = .Cells(1, 1, 1, 1)
                rgHeader.Style.Fill.PatternType = Style.ExcelFillStyle.Solid
                rgHeader.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue)
            End With

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment; filename=Save_Log_" & Format(Date.Now, "yyyy-MM-dd HH:mm:ss") & ".xlsx")
            'Response.BufferOutput = True
            Dim stream As MemoryStream = New MemoryStream(Pck.GetAsByteArray())

            Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length)
            Response.Flush()
            Response.Close()

        End Using
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        MemoMessage.Text = ""
    End Sub
#End Region

End Class
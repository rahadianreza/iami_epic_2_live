﻿'########################################################################################################################################
'System         : IAMI EPIC2
'Program        : 
'Overview       : This program about             
'                 1. 
'Parameter      :  
'Created By     : Agus
'Created Date   : 18 Oct 2018
'Last Update By :
'Last Update    :
'Modify Update  ([Date],[Editor],[Summary],[Version])
'               ([],[],[],[])
'########################################################################################################################################

Imports System.Data.SqlClient

Public Class clsRFQDB


    Public Shared Function GetStatus(pRFQNo As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select * From RFQ_Header Where RFQ_Number = '" & pRFQNo & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetMaxRFQNo(pMonth As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select RIGHT(Isnull(RIGHT(RTRIM(Max(RFQ_Number)),3),0) + 1001,3) As RFQ_Number " & vbCrLf & _
                      "From RFQ_Header H Inner Join RFQ_Set S On S.RFQ_Set = H.RFQ_Set Where Month(RFQ_Date) = " & pMonth & ""


                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetMaxRFQSetNo(pMonth As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select RIGHT(Isnull(RIGHT(RTRIM(Max(RFQ_Set)),3),0) + 1001,3) As RFQ_SetNumber " & vbCrLf & _
                      "From RFQ_Set Where Month(RFQ_Date) = " & pMonth & ""

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetSupplier(Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Supplier_Code As Code, Supplier_Name As Description From Mst_Supplier "

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataHeader(pRFQSetNo As String, pRev As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select H.RFQ_Title, S.RFQ_Set, H.RFQ_Number, S.Rev, S.PR_Number, S.RFQ_Date, P.Supplier_Name, H.Supplier_Code,  S.RFQ_DueDate, ISNULL(H.RFQ_Status,'0') As RFQ_Status " & vbCrLf & _
                      "From RFQ_Set S  " & vbCrLf & _
                      "Inner Join RFQ_Header H On S.RFQ_Set = H.RFQ_Set And S.Rev = H.Rev  " & vbCrLf & _
                      "Left Join Mst_Supplier P On P.Supplier_Code = H.Supplier_Code  " & vbCrLf & _
                      "Where S.RFQ_Set = @RFQSetNo And S.Rev = @Revision "

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text

                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSetNo)
                cmd.Parameters.AddWithValue("Revision", pRev)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataDetail(pPRNo As String, pRFQSetNo As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "Select	M.Material_No, M.PR_Number, M.Qty, RFQ_Number, " & vbCrLf & _
                '      "I.Description, I.Specification, I.UOM, " & vbCrLf & _
                '      "CASE WHEN D.Material_No IS NULL THEN '0' ELSE '1' END AllowCheck " & vbCrLf & _
                '      "From " & vbCrLf & _
                '      "( " & vbCrLf & _
                '      "Select PR_Number, Material_No, Qty, Remarks  " & vbCrLf & _
                '      "From PR_Detail Where PR_Number = @PRNo " & vbCrLf & _
                '      ") M " & vbCrLf & _
                '      "Left Join " & vbCrLf & _
                '      "(" & vbCrLf & _
                '      "Select	H.RFQ_Number, S.PR_Number, D.Material_No, D.Qty " & vbCrLf & _
                '      "From RFQ_Header H   " & vbCrLf & _
                '      "Inner Join RFQ_Detail D On H.RFQ_Number = D.RFQ_Number " & vbCrLf & _
                '      "Left Join RFQ_Set S On S.RFQ_Set = H.RFQ_Set  " & vbCrLf & _
                '      "Where H.RFQ_Number = @RFQNo " & vbCrLf & _
                '      ") D On M.Material_No = D.Material_No And M.PR_Number = D.PR_Number " & vbCrLf & _
                '      "Left Join ItemMaster I On M.Material_No = I.ItemCode "

                sql = "Select PR_Number, G.Material_No, G.Qty, I.Description, I.Specification, I.UOM, AllowCheck " & vbCrLf & _
                      "From ( " & vbCrLf & _
                      "Select DISTINCT S.PR_Number, Q.Material_No, Qty, '1' AllowCheck From RFQ_Set S  " & vbCrLf & _
                      "Left Join RFQ_Header H On S.RFQ_Set = H.RFQ_Set  " & vbCrLf & _
                      "Inner Join RFQ_Detail Q On Q.RFQ_Number = H.RFQ_Number  " & vbCrLf & _
                      "Where S.PR_Number = @PRNo And H.RFQ_Set = @RFQSetNo  " & vbCrLf & _
                      "Union All  " & vbCrLf & _
                      "Select PR_Number, P.Material_No, P.Qty, CASE WHEN D.Material_No IS NULL THEN '0' ELSE  '1' END AllowCheck   " & vbCrLf & _
                      "From  " & vbCrLf & _
                      "(  " & vbCrLf & _
                      "Select * From PR_Detail Where PR_Number = @PRNo And Rev = (Select Max(Rev) Rev From PR_Detail Where PR_Number = @PRNo)  " & vbCrLf & _
                      ") P  " & vbCrLf & _
                      "Left Join (  " & vbCrLf & _
                      "Select Q.* From RFQ_Set S   " & vbCrLf & _
                      "Left Join RFQ_Header H On S.RFQ_Set = H.RFQ_Set  " & vbCrLf & _
                      "Inner Join RFQ_Detail Q On Q.RFQ_Number = H.RFQ_Number And Q.Rev = H.Rev  " & vbCrLf & _
                      "Where S.PR_Number = @PRNo   " & vbCrLf & _
                      ") D On P.Material_No = D.Material_No   " & vbCrLf & _
                      "Where CASE WHEN D.Material_No IS NULL THEN '0' ELSE  '1' END = '0'  " & vbCrLf & _
                      ") G Left Join Mst_Item I On G.Material_No = I.Material_No Order By G.Material_No"

                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("PRNo", pPRNo)
                cmd.Parameters.AddWithValue("RFQSetNo", pRFQSetNo)

                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetRFQHeader(pRFQSetNo As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select * From RFQ_Set Where RFQ_Set = '" & pRFQSetNo & "' And Rev = (Select MAX(Rev) Rev From RFQ_Set Where RFQ_Set = '" & pRFQSetNo & "') "

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataRFQ(pPRNo As String, pRFQSetNo As String, pRev As Integer, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select PR_Number, G.Material_No, G.Qty, I.Description, I.Specification, I.UOM As UOM_Description, AllowCheck " & vbCrLf & _
                      "From ( " & vbCrLf & _
                      "Select DISTINCT S.PR_Number, Q.Material_No, Qty, '1' AllowCheck From RFQ_Set S  " & vbCrLf & _
                      "Left Join RFQ_Header H On S.RFQ_Set = H.RFQ_Set AND S.Rev = H.Rev " & vbCrLf & _
                      "Inner Join RFQ_Detail Q On Q.RFQ_Number = H.RFQ_Number AND H.Rev = Q.Rev " & vbCrLf & _
                      "Where S.PR_Number = '" & pPRNo & "' And H.RFQ_Set = '" & pRFQSetNo & "' AND S.Rev = " & pRev & " " & vbCrLf & _
                      "Union All  " & vbCrLf & _
                      "Select PR_Number, P.Material_No, P.Qty, CASE WHEN D.Material_No IS NULL THEN '0' ELSE  '1' END AllowCheck   " & vbCrLf & _
                      "From  " & vbCrLf & _
                      "(  " & vbCrLf & _
                      "Select * From PR_Detail Where PR_Number = '" & pPRNo & "' And Rev = (Select Max(Rev) Rev From PR_Detail Where PR_Number = '" & pPRNo & "') " & vbCrLf & _
                      ") P  " & vbCrLf & _
                      "Left Join (  " & vbCrLf & _
                      "Select Q.* From RFQ_Set S   " & vbCrLf & _
                      "Left Join RFQ_Header H On S.RFQ_Set = H.RFQ_Set AND S.Rev = H.Rev " & vbCrLf & _
                      "Inner Join RFQ_Detail Q On Q.RFQ_Number = H.RFQ_Number AND H.Rev = Q.Rev " & vbCrLf & _
                      "Where S.PR_Number = '" & pPRNo & "' AND S.Rev = " & pRev & "  " & vbCrLf & _
                      ") D On P.Material_No = D.Material_No   " & vbCrLf & _
                      "Where CASE WHEN D.Material_No IS NULL THEN '0' ELSE  '1' END = '0'  " & vbCrLf & _
                      ") G Left Join Mst_Item I On G.Material_No = I.Material_No Order By G.Material_No"


                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetDataPR(pPRNo As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                '"Left Join (Select * From RFQ_Detail) D On P.Material_No = D.Material_No " & vbCrLf & _

                sql = "Select	P.PR_Number, P.Material_No, P.Qty, I.Description, I.Specification, U.UOM_Description, " & vbCrLf & _
                        "CASE WHEN D.Material_No IS NULL THEN '0' ELSE  '1' END AllowCheck " & vbCrLf & _
                       "From " & vbCrLf & _
                       "( " & vbCrLf & _
                       "Select * From PR_Detail Where PR_Number = '" & pPRNo & "' And Rev = (Select Max(Rev) Rev From PR_Detail Where PR_Number = '" & pPRNo & "') " & vbCrLf & _
                       ") P " & vbCrLf & _
                       "Left Join ( " & vbCrLf & _
                       "Select Q.* From RFQ_Set S  " & vbCrLf & _
                       "Left Join RFQ_Header H On S.RFQ_Set = H.RFQ_Set And  S.Rev = H.Rev    " & vbCrLf & _
                       "Inner Join RFQ_Detail Q On Q.RFQ_Number = H.RFQ_Number And Q.Rev = H.Rev " & vbCrLf & _
                       "Where S.PR_Number = '" & pPRNo & "'    " & vbCrLf & _
                       ") D On P.Material_No = D.Material_No  " & vbCrLf & _
                       "LEFT JOIN Mst_Item I On P.Material_No = I.Material_No" & vbCrLf & _
                       "LEFT JOIN (Select Par_Code, Par_Description As UOM_Description From Mst_Parameter Where Par_Group = 'UOM') U" & vbCrLf & _
                       "On U.Par_Code = I.UOM" & vbCrLf & _
                       "Where CASE WHEN D.Material_No IS NULL THEN '0' ELSE  '1' END = '0' "

                'sql = "SELECT    PD.PR_Number, PD.Material_No, PD.Qty, PD.Remarks, IM.Description, IM.Specification, IM.GroupItem, IM.CategoryItem, IM.PRType, " & vbCrLf & _
                '      "IM.LastIAPrice, IM.LastSupplier, IM.UOM, UOM_Description,  '0' As AllowCheck " & vbCrLf & _
                '      "FROM            PR_Detail AS PD  " & vbCrLf & _
                '      "LEFT OUTER JOIN ItemMaster AS IM ON PD.Material_No = IM.ItemCode" & vbCrLf & _
                '      "LEFT JOIN (Select Par_Code, Par_Description As UOM_Description From Mst_Parameter Where Par_Group = 'UOM') UO " & vbCrLf & _
                '      "On UO.Par_Code = IM.UOM Where PR_Number = '" & pPRNo & "'"

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetList(pDateFrom As String, pDateTo As String, pRFQSetNo As String, Optional ByRef pErr As String = "") As DataSet
        Try
            Dim sql As String = ""

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "Select Distinct RF.RFQ_Set, RF.Rev, RF.RFQ_Date, RF.RFQ_DueDate, RF.PR_Number, PH.PR_Date,  " & vbCrLf & _
                      "PRTypeDescription, DepartmentName, SectionName, PH.Project, PH.Urgent_Status, TenderName, CASE WHEN RFQ_Status = '1' THEN 'Submit' WHEN RFQ_Status = '2' THEN 'Approve' WHEN RFQ_Status = '4' THEN 'Reject' ELSE 'Draft' END RFQStatus " & vbCrLf & _
                      "From   " & vbCrLf & _
                      "( " & vbCrLf & _
                      "SELECT * FROM RFQ_Set   " & vbCrLf & _
                      ") RF  " & vbCrLf & _
                      "Left Join RFQ_Header RH On RF.RFQ_Set = RH.RFQ_Set AND RF.Rev = RH.Rev " & vbCrLf & _
                      "Left Join PR_Header PH On RF.PR_Number = PH.PR_Number " & vbCrLf & _
                      "Left Join (Select Par_Code, Par_Description As PRTypeDescription From Mst_Parameter Where Par_Group = 'PRType') PT On PH.PRType_Code = PT.Par_Code " & vbCrLf & _
                      "Left Join (Select Par_Code, Par_Description As DepartmentName From Mst_Parameter Where Par_Group = 'Department') DP On PH.Department_Code = DP.Par_Code " & vbCrLf & _
                      "Left Join (Select Par_Code, Par_Description As SectionName From Mst_Parameter Where Par_Group = 'Section') SC On PH.Section_Code = SC.Par_Code " & vbCrLf & _
                      "Left Join (Select Par_Code, Par_Description As TenderName From Mst_Parameter Where Par_Group = 'Tender') TD On PH.Tender_Code = TD.Par_Code " & vbCrLf & _
                      "Where RF.RFQ_Date >= '" & pDateFrom & "' And RF.RFQ_Date <= '" & pDateTo & "' "

                If pRFQSetNo <> "" Then
                    sql = sql + " And RF.RFQ_Set = '" & pRFQSetNo & "'"
                End If

                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                Dim ds As New DataSet
                da.Fill(ds)

                Return ds

            End Using
        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboData(pDateFrom As String, pDateTo As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

         
                sql = "Select 'ALL' As RFQ_Number " & vbCrLf & _
                      "Union All " & vbCrLf & _
                      "Select RFQ_Set From RFQ_Set  " & vbCrLf & _
                      "Where RFQ_Date >= '" & pDateFrom & "' And RFQ_Date <= '" & pDateTo & "'"


                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function GetComboPRNumber(pDepartment As String, pSection As String, pPRType As String, Optional ByRef pErr As String = "") As DataSet
        Dim ds As New DataSet
        Dim sql As String
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                If pDepartment = "null" Then
                    pDepartment = ""
                End If

                If pSection = "null" Then
                    pSection = ""
                End If

                If pPRType = "null" Then
                    pPRType = ""
                End If

                'sql = "Select PR_Number As Code From PR_Header " & vbCrLf & _
                '      "Where Isnull(PRType_Code,'') = CASE WHEN '" & pPRType & "' = '' THEN PRType_Code ELSE '" & pPRType & "' END " & vbCrLf & _
                '      "And Isnull(Department_Code,'') = CASE WHEN '" & pDepartment & "' = '' THEN Department_Code ELSE '" & pDepartment & "' END " & vbCrLf & _
                '      "And Isnull(Section_Code,'') = CASE WHEN '" & pSection & "' = '' THEN Section_Code ELSE '" & pSection & "' END"


                sql = "Select DISTINCT H.PR_Number As Code  " & vbCrLf & _
                      "From " & vbCrLf & _
                      "(" & vbCrLf & _
                      "Select PR_Number  " & vbCrLf & _
                      "From PR_Header " & vbCrLf & _
                      "Where Isnull(PRType_Code,'') = CASE WHEN '" & pPRType & "' = '' THEN PRType_Code ELSE '" & pPRType & "' END " & vbCrLf & _
                      "And Isnull(Department_Code,'') = CASE WHEN '" & pDepartment & "' = '' THEN Department_Code ELSE '" & pDepartment & "' END " & vbCrLf & _
                      "And Isnull(Section_Code,'') = CASE WHEN '" & pSection & "' = '' THEN Section_Code ELSE '" & pSection & "' END " & vbCrLf & _
                      ") H Inner Join PR_Detail D On H.PR_Number = D.PR_Number " & vbCrLf & _
                      "Left Join " & vbCrLf & _
                      "( " & vbCrLf & _
                      "Select Material_No From RFQ_Set S  " & vbCrLf & _
                      "Inner Join RFQ_Header E On E.RFQ_Set = S.RFQ_Set " & vbCrLf & _
                      "Inner Join RFQ_Detail T On T.RFQ_Number = E.RFQ_Number " & vbCrLf & _
                      "Group By Material_No " & vbCrLf & _
                      ") K On K.Material_No = D.Material_No " & vbCrLf & _
                      "Where K.Material_No IS NULL "


                Dim cmd As New SqlCommand(sql, con)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                da.Dispose()
                cmd.Dispose()
            End Using
            Return ds

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertSet(ByVal pItem As clsRFQ, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Ins]"

                sql = "INSERT INTO RFQ_Set (RFQ_Set, PR_Number, Rev, RFQ_Date, RFQ_DueDate, Currency_Code, Register_User, Register_Date) " & vbCrLf & _
                      "VALUES (@RFQSetNumber, @PRNumber, @Rev, @RFQDate, @RFQDueDate, @Currency, @UserId, GETDATE() )"
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("PRNumber", pItem.PRNumber)
                cmd.Parameters.AddWithValue("RFQSetNumber", pItem.RFQSetNumber)
                cmd.Parameters.AddWithValue("RFQDate", pItem.RFQDate)
                cmd.Parameters.AddWithValue("RFQDueDate", pItem.Deadline)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("Rev", pItem.Rev)
                cmd.Parameters.AddWithValue("Currency", pItem.Currency)
                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDetailFromRFQData(ByVal pItem As clsRFQ, pNewRFQNo As String, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Ins]"

                sql = "INSERT INTO RFQ_Detail  " & vbCrLf & _
                        "( RFQ_Set, RFQ_Number, Material_No, Qty, Register_User, Register_Date )  " & vbCrLf & _
                        "Select S.RFQ_Set, '" & pNewRFQNo & "' RFQ_Number, Material_No, Qty, @UserId As Register_User, GETDATE() Register_Date  " & vbCrLf & _
                        "From RFQ_Set S " & vbCrLf & _
                        "Inner Join RFQ_Header H On H.RFQ_Set = S.RFQ_Set " & vbCrLf & _
                        "Inner Join RFQ_Detail D On H.RFQ_Number = D.RFQ_Number " & vbCrLf & _
                        "Where S.RFQ_Set = @RFQSetNumber And H.RFQ_Number = @RFQNumber "

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("RFQNumber", pItem.RFQNumber)
                cmd.Parameters.AddWithValue("RFQSetNumber", pItem.RFQSetNumber)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertHeader(ByVal pItem As clsRFQ, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Ins]"

                sql = "INSERT INTO RFQ_Header (RFQ_Set, RFQ_Number, Rev, Supplier_Code, RFQ_Title, Register_User, Register_Date) " & vbCrLf & _
                      "VALUES (@RFQSetNumber, @RFQNumber, @Rev, @Supplier, @RFQTitle, @UserId, GETDATE())"
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("RFQNumber", pItem.RFQNumber)
                cmd.Parameters.AddWithValue("RFQSetNumber", pItem.RFQSetNumber)
                cmd.Parameters.AddWithValue("Rev", pItem.Rev)
                cmd.Parameters.AddWithValue("RFQDate", pItem.RFQDate)
                cmd.Parameters.AddWithValue("Supplier", pItem.Supplier)
                cmd.Parameters.AddWithValue("RFQTitle", pItem.RFQTitle)

                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function


    Public Shared Function UpdateStatus(pRFQSetNo As String, pUser As String, pRev As Integer, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_RFQCreate_Submit"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNumber", pRFQSetNo)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("Rev", pRev)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateHeader(pRFQNo As String, pSupplier As String, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "UPDATE RFQ_Header Set Supplier_Code = @Supplier, Update_User = '" & pUser & "', Update_Date = GETDATE() WHERE RFQ_Number = @RFQNumber"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("RFQNumber", pRFQNo)
                cmd.Parameters.AddWithValue("Supplier", pSupplier)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function Reject(pRFQSetNo As String, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_RFQReject_Proc"

                cmd = New SqlCommand(sql, con)
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("RFQSetNumber", pRFQSetNo)
                cmd.Parameters.AddWithValue("UserId", pUser)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function UpdateData(pRFQSetNo As String, pUser As String, pRev As Integer, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Ins]"

                'sql = "UPDATE RFQ_Header Set RFQ_Status = '1', Update_User = '" & pUser & "', Update_Date = GETDATE() WHERE RFQ_Set = @RFQSetNumber"
                sql = "sp_RFQCreate_Submit"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSetNumber", pRFQSetNo)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("Rev", pRev)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function DeleteDetail(ByVal pItem As clsRFQ, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Ins]"

                sql = "DELETE FROM RFQ_Detail WHERE RFQ_Set = @RFQSetNumber  AND Material_No = @MaterialNo AND Rev = @Rev"

                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                'cmd.Parameters.AddWithValue("RFQNumber", pItem.RFQNumber)
                cmd.Parameters.AddWithValue("RFQSetNumber", pItem.RFQSetNumber)
                cmd.Parameters.AddWithValue("MaterialNo", pItem.MaterialNo)
                cmd.Parameters.AddWithValue("Rev", pItem.Rev)

                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertDetail(ByVal pItem As clsRFQ, pUser As String, Optional ByRef pErr As String = "") As Integer
        Dim sql As String
        Dim cmd As SqlCommand
        Dim i As Integer

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                'sql = "[sp_ItemMaster_Ins]"

                sql = "INSERT INTO RFQ_Detail (RFQ_Set, RFQ_Number, Material_No, Rev, Qty, Register_User, Register_Date) " & vbCrLf & _
                      "VALUES (@RFQSetNumber, @RFQNumber, @MaterialNo, @Rev, @Qty, @UserId, GETDATE())"
                cmd = New SqlCommand(sql, con)

                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("RFQNumber", pItem.RFQNumber)
                cmd.Parameters.AddWithValue("RFQSetNumber", pItem.RFQSetNumber)
                cmd.Parameters.AddWithValue("MaterialNo", pItem.MaterialNo)
                cmd.Parameters.AddWithValue("Qty", pItem.Qty)
                cmd.Parameters.AddWithValue("UserId", pUser)
                cmd.Parameters.AddWithValue("Rev", pItem.Rev)
                i = cmd.ExecuteNonQuery
            End Using

            Return i

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try
    End Function
End Class

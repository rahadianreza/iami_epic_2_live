﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web

Public Class CEAcceptanceUserDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim ls_VCENumber As String
    Dim txtCEParameter As Object
    Dim pUser As String = ""

    Dim vCENumber As String
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

    Private Sub up_FillComboPRNo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsCostEstimationAcceptanceUserDB.GetComboPRNo(pErr)
        If pErr = "" Then
            cboPRNo.DataSource = ds
            cboPRNo.DataBind()
        End If
    End Sub

    Public Sub FillCombo(ByVal pType As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsCostEstimationAcceptanceUserDB.GetComboBudgetNo(pUser, pErr)
        If pErr = "" Then
            cbIABudget.DataSource = ds
            cbIABudget.DataBind()
        End If
    End Sub

    Private Sub up_FillComboBudgetNo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsCostEstimationAcceptanceUserDB.GetComboBudgetNo(pUser, pErr)
        If pErr = "" Then
            cbIABudget.DataSource = ds
            cbIABudget.DataBind()
        End If
    End Sub

    Private Sub up_FillComboRFQ(pPRNo As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsCostEstimationAcceptanceUserDB.GetComboRFQSetNo(pPRNo, pErr)
        If pErr = "" Then

            cboRFQSetNumber.DataSource = ds
            cboRFQSetNumber.DataBind()
        End If
    End Sub

    Private Sub up_FillComboSupplier(pRFQSetNo As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsCostEstimationAcceptanceUserDB.GetComboSupplier(pRFQSetNo, pErr)
        If pErr = "" Then
            'cboSupplier.DataSource = ds
            'cboSupplier.DataBind()
        End If
    End Sub

    Private Sub up_GridHeader(pRFQSetNo As String, pSupplier As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        For i = 0 To 4
            Grid.Columns.Item(5 + i).Visible = False
        Next

        ds = clsCostEstimationAcceptanceUserDB.GetDataSupplier(pRFQSetNo, pSupplier, pErr)
        If pErr = "" Then

            For i = 0 To ds.Tables(0).Rows.Count - 1
                Grid.Columns.Item(5 + i).Caption = ds.Tables(0).Rows(i)("Description")
                Grid.Columns.Item(5 + i).Visible = True
            Next

        End If
    End Sub

    Private Sub up_GridLoadCE(CENumber As String, Revision As Integer)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim vSupplier As String = ""
        Dim headSupplier As String = ""
        Dim vname As String = "Supplier"
        Dim vfield As String = "S"
        Dim a As Integer

        ds = clsCostEstimationAcceptanceUserDB.GetCEData(CENumber, Revision, ErrMsg)

        If ds.Tables(0).Rows.Count > 0 Then
            dtCEDate.Value = ds.Tables(0).Rows(0)("CE_Date")
            cboPRNo.Text = ds.Tables(0).Rows(0)("PR_Number")
            cboRFQSetNumber.Text = ds.Tables(0).Rows(0)("RFQ_Set")
            txtRev.Text = Revision

            txtParameter.Text = CENumber & "|" & Revision & "|" & ds.Tables(0).Rows(0)("RFQ_Set")

            'If ds.Tables(0).Rows(0)("CE_Status") = "1" Or ds.Tables(0).Rows(0)("CE_Status") = "2" Or ds.Tables(0).Rows(0)("CE_Status") = "4" Then
            '    btnSubmit.Enabled = False
            '    btnDraft.Enabled = False
            'End If
            cbDraft.JSProperties("cpStatus") = ds.Tables(0).Rows(0)("CE_Status")
            Session("cpStatus") = ds.Tables(0).Rows(0)("CE_Status")
            gs_CERevNo = ds.Tables(0).Rows(0)("Revision_No")
            txtNotes.Text = ds.Tables(0).Rows(0)("CE_Notes")
            cbIABudget.Text = ds.Tables(0).Rows(0)("CE_Budget") 'pakai textbox
            'txtIABudget.Text = ds.Tables(0).Rows(0)("CE_Budget")

            If ds.Tables(0).Rows(0)("Skip_Budget") = "1" Then
                chkSkip.Checked = True
            Else
                chkSkip.Checked = False
            End If

			'inkFile.Text = ds.Tables(0).Rows(0)("File_Name")
			'inkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
			'add 27-02-2019
            LinkFile.Text = IIf(ds.Tables(0).Rows(0)("File_Name") = "", "No File", ds.Tables(0).Rows(0)("File_Name"))
            'LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
            Dim extension As String = ""
            extension = System.IO.Path.GetExtension("~/Files/" & ds.Tables(0).Rows(0)("File_Name"))
            If extension <> ".pdf" Then
                If ds.Tables(0).Rows(0)("File_Name") <> "" And ds.Tables(0).Rows(0)("File_Name") <> "No File" Then
                    LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
                End If
            End If

            IALinkFile.Text = IIf(ds.Tables(0).Rows(0)("FileName_Acceptance") = "", "No File", ds.Tables(0).Rows(0)("FileName_Acceptance"))
            'LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
            Dim extension2 As String = ""
                extension2 = System.IO.Path.GetExtension("~/Files/" & ds.Tables(0).Rows(0)("FileName_Acceptance"))
                If extension2 <> ".pdf" Then
                If ds.Tables(0).Rows(0)("FileName_Acceptance") <> "" And ds.Tables(0).Rows(0)("FileName_Acceptance") <> "No File" Then
                    IALinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("FileName_Acceptance")
                End If

                End If


                cbDraft.JSProperties("cpSkip") = ds.Tables(0).Rows(0)("Skip_Budget")

                txtReason.Text = ds.Tables(0).Rows(0)("Skip_Notes")

                dtCEDate.Enabled = False
                cboRFQSetNumber.Enabled = False
                cboPRNo.Enabled = False

                'dtCEDate.ReadOnly = True
                'cboRFQSetNumber.ReadOnly = True
                'cboPRNo.ReadOnly = True

                'dtCEDate.BackColor = Color.Silver

                'cboPRNo.BackColor = Color.Silver

                'cboRFQSetNumber.BackColor = Color.Silver
                txtFlag.Text = 1
            End If

            'begin harusnya sudah gak perlu bind supplier dengan cara seperti ini , get supplier dari sp saja 13-03-2019
            ds = clsCostEstimationAcceptanceUserDB.GetDataSupplier(cboRFQSetNumber.Text, "00", ErrMsg)
            For i = 0 To ds.Tables(0).Rows.Count - 1
                a = a + 1

                vSupplier = vSupplier & "[" & ds.Tables(0).Rows(i)("Code") & "]" & ","

                headSupplier = headSupplier & "ISNULL([" & ds.Tables(0).Rows(i)("Code") & "],0) As " & vname & a & ","
            Next

            If a < 5 Then
                For a = a + 1 To 5
                    vSupplier = vSupplier & "[" & vfield & a & "]" & ","
                    headSupplier = headSupplier & "ISNULL([" & vfield & a & "],0) As " & vname & a & ","
                Next
            End If


            If Len(vSupplier) > 0 Then
                vSupplier = Left(vSupplier, Len(vSupplier) - 1)
                headSupplier = Left(headSupplier, Len(headSupplier) - 1)
            End If

            'Dim ds1 As New DataSet
            'ds = clsCostEstimationAcceptanceUserDB.GetListCEData(CENumber, vSupplier, headSupplier, ErrMsg)
        ds = clsCostEstimationAcceptanceUserDB.GetListCEData(CENumber, Revision, cboRFQSetNumber.Text, ErrMsg)


            If ErrMsg = "" Then
                Grid.DataSource = ds
                Grid.DataBind()
            Else
                show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
            End If

    End Sub

    Private Sub up_GridLoad(RFSet As String, Supplier As String)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        Dim vSupplier As String = ""
        Dim headSupplier As String = ""
        Dim vname As String = "Supplier"
        Dim vfield As String = "S"
        Dim a As Integer
		
		'get supplier dari sp
        If Supplier = "00" Then
            ds = clsCostEstimationAcceptanceUserDB.GetDataSupplier(RFSet, Supplier, ErrMsg)
            For i = 0 To ds.Tables(0).Rows.Count - 1
                a = a + 1

                vSupplier = vSupplier & "[" & ds.Tables(0).Rows(i)("Code") & "]" & ","

                headSupplier = headSupplier & "ISNULL([" & ds.Tables(0).Rows(i)("Code") & "],0) As " & vname & a & ","
            Next

            If a < 5 Then
                For a = a + 1 To 5
                    vSupplier = vSupplier & "[" & vfield & a & "]" & ","
                    headSupplier = headSupplier & "ISNULL([" & vfield & a & "],0) As " & vname & a & ","
                Next
            End If


            If Len(vSupplier) > 0 Then
                vSupplier = Left(vSupplier, Len(vSupplier) - 1)
                headSupplier = Left(headSupplier, Len(headSupplier) - 1)
            End If

        Else
            headSupplier = "[" & Supplier & "] As Supplier1" & ", [S2] As Supplier2" & ", [S3] As Supplier3" & ", [S4] As Supplier4" & ", [S5] As Supplier5"
            vSupplier = "[" & Supplier & "],[S2],[S3],[S4],[S5] "
        End If


        ds = clsCostEstimationAcceptanceUserDB.GetList(RFSet, vSupplier, headSupplier, ErrMsg)

        If ErrMsg = "" Then

            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|D030", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAccept.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAccept, btnAccept.GetType(), "btnAccept", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboRFQSetNumber)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("D030")
        ' Master.SiteTitle = sGlobal.menuName
        Master.SiteTitle = "COST ESTIMATION ACCEPTANCE USER DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "D030")
        Dim CENumber As String = ""
        Dim RFQSet As String = ""
        Dim Revision As Integer
        Dim vDate1 As Date
        Dim vDate2 As Date

        If Request.QueryString("ID") <> "" Then
            CENumber = Split(Request.QueryString("ID"), "|")(0)
            Revision = Split(Request.QueryString("ID"), "|")(1)
            RFQSet = Split(Request.QueryString("ID"), "|")(2)
        End If


        If Not Page.IsPostBack Then

            If CENumber = "" Then
                gs_CENumber = ""
                txtCENumber.Text = "--NEW--"
                gs_CENo = "--NEW--"
                vDate1 = Split(Request.QueryString("Date"), "|")(0)
                vDate2 = Split(Request.QueryString("Date"), "|")(1)
                gs_DateFrom = Format(vDate1, "yyyy-MM-dd")
                gs_DateTo = Format(vDate2, "yyyy-MM-dd")

                up_FillComboPRNo()
                up_FillComboBudgetNo()
                dtCEDate.Value = Now
                up_GridHeader("xx", "xx")
                cbDraft.JSProperties("cpStatus") = ""
                cbDraft.JSProperties("cpSkip") = ""
                cbDraft.JSProperties("cpFile") = ""

                txtFlag.Text = 0
            Else
                gs_CENo = CENumber
                gs_CENumber = CENumber
                txtCENumber.Text = CENumber
                up_FillComboBudgetNo()
                up_GridHeader(RFQSet, "00")
                up_GridLoadCE(CENumber, Revision)
                GridLoadAttachment(RFQSet)
                'Dim dsStatus As DataSet
                'dsStatus = clsCostEstimationAcceptanceUserDB.GetCEStatus(CENumber, Revision)
                'Dim script2 As String = ""
                'If dsStatus.Tables(0).Rows.Count > 0 Then
                '    script2 = "btnAccept.SetEnabled(true);" & vbCrLf & _
                '              " btnSubmit.SetEnabled(false);"
                '    ScriptManager.RegisterStartupScript(btnAccept, btnAccept.GetType(), "btnAccept", script2, True)

                'End If


            End If
        Else
            Dim dsStatus As DataSet
            dsStatus = clsCostEstimationAcceptanceUserDB.GetCEStatus(CENumber, Revision)
            Dim script2 As String = ""
            If dsStatus.Tables(0).Rows.Count > 0 Then
                up_FillComboBudgetNo()

            End If
        End If
        cbFile.JSProperties("cpMessage") = ""
        cbDraft.JSProperties("cpMessage") = ""
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/CEAcceptanceUser.aspx")
    End Sub

    Private Function uf_ConvertMonth(pMonth As Integer) As String
        Dim angka As String = ""

        If pMonth = 1 Then
            angka = "I"
        ElseIf pMonth = 2 Then
            angka = "II"
        ElseIf pMonth = 3 Then
            angka = "III"
        ElseIf pMonth = 4 Then
            angka = "IV"
        ElseIf pMonth = 5 Then
            angka = "V"
        ElseIf pMonth = 6 Then
            angka = "VI"
        ElseIf pMonth = 7 Then
            angka = "VII"
        ElseIf pMonth = 8 Then
            angka = "VIII"
        ElseIf pMonth = 9 Then
            angka = "IX"
        ElseIf pMonth = 10 Then
            angka = "X"
        ElseIf pMonth = 11 Then
            angka = "XI"
        ElseIf pMonth = 12 Then
            angka = "XII"
        End If

        Return angka
    End Function

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Dim pRFQSet As String
        If Split(e.Parameters, "|")(1) <> "" Then
            pRFQSet = Split(e.Parameters, "|")(3)
        End If

        Dim pRev As Integer

        'If txtRev.Text = "" Then pRev = 0
        If Split(e.Parameters, "|")(1) = "" Then
            pRev = 0
        Else
            pRev = Split(e.Parameters, "|")(2)
        End If


        If pfunction = "gridload" Then
            up_GridHeader(pRFQSet, "00")
            up_GridLoad(pRFQSet, "00")
        ElseIf pfunction = "draft" Then
            '    'If gs_CENumber <> "" And gs_CENumber <> "ALL" Then
            up_GridHeader(cboRFQSetNumber.Text, "00")
            up_GridLoadCE(gs_CENumber, pRev)
            'End If
        End If

    End Sub

    Private Sub cbGrid_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbGrid.Callback
        Dim pfunction As String = Split(e.Parameter, "|")(0)
        Dim pRFQSet As String = Split(e.Parameter, "|")(3)
        Dim pRev As Integer

        If Split(e.Parameter, "|")(2) = "" Then pRev = 0

        up_GridLoadCE(gs_CENumber, pRev)

    End Sub

    Private Sub cboRFQSetNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboRFQSetNumber.Callback
        'Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim prno As String = e.Parameter

        up_FillComboRFQ(prno)
    End Sub

    Private Sub cbIABudget_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbIABudget.Callback
        'Dim pFunction As String = Split(e.Parameter, "|")(0)
        up_FillComboBudgetNo()
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
       
            'Non-Editable
            e.Cell.BackColor = Color.LemonChiffon
            e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")

    End Sub

    Private Sub up_Print()
        'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/ViewPRAcceptance.aspx")
        Session("CENumber") = txtCENumber.Text
        Session("Revision") = txtRev.Text
        Session("RFQSet") = cboRFQSetNumber.Text
        Response.Redirect("~/ViewCostEstimationAcceptance.aspx")

    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        up_Print()
    End Sub

    'Protected Sub docUpload_FileUploadComplete(sender As Object, e As ASPxUploadControl.FileUploadCompleteEventArgs) Handles docUpload.FileUploadComplete
    '    If e.IsValid = False Then
    '        docUpload.JSProperties("cpType") = "3"
    '        docUpload.JSProperties("cpMessage") = "Invalid file!"
    '    End If

    '    Dim folderPath As String = Server.MapPath("~/Files/")
    '    Dim SizeFile As Single
    '    Dim fullPath As String
    '    Dim FileName1 As String

    '    If Not Directory.Exists(folderPath) Then
    '        'If Directory (Folder) does not exists. Create it.
    '        Directory.CreateDirectory(folderPath)
    '    End If

    '    FileName1 = e.UploadedFile.FileName + "." + e.UploadedFile.ContentType
    '    fullPath = String.Format("{0}{1}", folderPath, FileName1)
    '    SizeFile = docUpload.UploadedFiles.Length / 1024 / 1024

    '    Session("FileName") = FileName1
    '    Session("FilePath") = fullPath

    '    If SizeFile > 2 Then
    '        cbFile.JSProperties("cpMessage") = "File size must not exceed 2 MB"
    '        'Call DisplayMessage(MsgTypeEnum.ErrorMsg, "File size must not exceed 2 MB", cbExist)
    '        Exit Sub
    '    End If

    '    e.UploadedFile.SaveAs(fullPath)

    '    docUpload.JSProperties("cpType") = "1"
    '    docUpload.JSProperties("cpMessage") = "Quotation " & e.UploadedFile.FileName & " has been uploaded successfully!"
    'End Sub

    'Private Sub cbUpload_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cbUpload.Callback
    '    Select Case e.Parameter
    '        Case "upload"

    '            Dim folderPath As String = Server.MapPath("~/Files/")
    '            Dim SizeFile As Single
    '            Dim fullPath As String
    '            Dim FileName1 As String = ""

    '            If Not Directory.Exists(folderPath) Then
    '                'If Directory (Folder) does not exists. Create it.
    '                Directory.CreateDirectory(folderPath)
    '            End If
    '            FileName1 = docUpload.UploadedFiles(0).FileName
    '            Session("FileName") = FileName1


    '            Call DisplayMessage(MsgTypeEnum.Success, Session("FileName"), cbExist)

    '    End Select



    'End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Dim ls_SQL As String = "", UserLogin As String = Session("user"), ls_ApprovalID As String = "1"
        Dim idx As Integer = 0

        Try

            'Dim dsIAB As New DataSet
            'dsIAB = clsCostEstimationAcceptanceUserDB.CheckIABudget(txtCENumber.Text, txtIABudget.Text, "")
            'If dsIAB.Tables(0).Rows.Count > 0 Then
            '    cbSave.JSProperties("cpMessage") = "IA CAPEX/OPEX is already exists!"
            '    'cbSave.JSProperties("cpStatus") = "1"
            '    Exit Sub
            'End If


            Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                sqlConn.Open()

                Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                    ls_SQL = "EXEC sp_CE_Acceptance_Detail_Accept " & _
                             "'" & txtCENumber.Text & "','" & txtRev.Text & "','" & "" & "'," & _
                             "'3','" & UserLogin & "'" & _
                             ",NULL" & _
                             ",NULL"
                    'sebelumnya pakai cbIABudget
                    Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                    sqlComm.CommandType = CommandType.Text
                    sqlComm.ExecuteNonQuery()
                    sqlComm.Dispose()

                    sqlTran.Commit()
                End Using

            End Using

            cbSave.JSProperties("cpMessage") = "Data accepted successfully!"
            cbDraft.JSProperties("cpStatus") = "3"
            'Call DisplayMessage(MsgTypeEnum.Success, "Data accepted successfully!", cbExist)
            'End If

        Catch ex As Exception
            cbSave.JSProperties("cpMessage") = Err.Description
            'DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
        End Try

    End Sub

    'Private Sub cbExist_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbExist.Callback
    '    Dim ds As New DataSet
    '    Dim ErrMsg As String = ""

    '    Select Case e.Parameter

    '        Case "accept"
    '            Dim ls_SQL As String = "", UserLogin As String = Session("user"), ls_ApprovalID As String = "1"
    '            Dim idx As Integer = 0


    '            Try
    '                cbExist.JSProperties("cpParameter") = "accept"

    '                'Dim folderPath As String = Server.MapPath("~/Files/")
    '                'Dim SizeFile As Single
    '                'Dim fullPath As String
    '                'Dim Extention As String
    '                'If Not Directory.Exists(folderPath) Then
    '                '    'If Directory (Folder) does not exists. Create it.
    '                '    Directory.CreateDirectory(folderPath)
    '                'End If
    '                'Dim PostedFile1 As HttpPostedFile = uploaderFile.PostedFile
    '                'Dim FileName1 As String = ""

    '                'If uploaderFile.FileName <> "" Then
    '                'If Session("uploadedFile") <> "" Then

    '                'uploaderFile.SaveAs(folderPath & Path.GetFileName(uploaderFile.FileName))

    '                'FileName1 = Path.GetFileName(PostedFile1.FileName)
    '                'fullPath = String.Format("{0}{1}", folderPath, FileName1)
    '                'SizeFile = uploaderFile.FileBytes.Length / 1024 / 1024

    '                'FileName1 = Session("Name")
    '                'fullPath = String.Format("{0}{1}", folderPath, FileName1)
    '                'SizeFile = docUpload.UploadedFiles.Length / 1024 / 1024
    '                'Extention = Session("ContentType")

    '                'If SizeFile > 2 Then
    '                '    cbFile.JSProperties("cpMessage") = "File size must not exceed 2 MB"
    '                '    'Call DisplayMessage(MsgTypeEnum.ErrorMsg, "File size must not exceed 2 MB", cbExist)
    '                '    Exit Sub
    '                'End If

    '                'docUpload.UploadedFile.SaveAs(fullPath)

    '                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
    '                    sqlConn.Open()

    '                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
    '                        ls_SQL = "EXEC sp_CE_Acceptance_Detail_Accept " & _
    '                                 "'" & txtCENumber.Text & "','" & txtRev.Text & "','" & cbIABudget.Text & "'," & _
    '                                 "'3','" & UserLogin & "'" & _
    '                                 ",'" & Session("FileName") & "'" & _
    '                                 ",'" & Session("FilePath") & "'"

    '                        Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
    '                        sqlComm.CommandType = CommandType.Text
    '                        sqlComm.ExecuteNonQuery()
    '                        sqlComm.Dispose()

    '                        sqlTran.Commit()
    '                    End Using

    '                End Using

    '                Call DisplayMessage(MsgTypeEnum.Success, "Data accepted successfully!", cbExist)
    '                'End If

    '            Catch ex As Exception
    '                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
    '            End Try

    '    End Select
    'End Sub

    Private Sub up_UploadFile()
        Dim folderPath As String = Server.MapPath("~/Files/")
        Dim SizeFile As Single
        Dim fullPath As String

        If Not Directory.Exists(folderPath) Then
            'If Directory (Folder) does not exists. Create it.
            Directory.CreateDirectory(folderPath)
        End If
        Dim PostedFile1 As HttpPostedFile = uploaderFile.PostedFile
        Dim FileName1 As String = ""

        If uploaderFile.FileName <> "" Then
            uploaderFile.SaveAs(folderPath & Path.GetFileName(uploaderFile.FileName))

            FileName1 = Path.GetFileName(PostedFile1.FileName)
            fullPath = String.Format("{0}{1}", folderPath, FileName1)
            SizeFile = uploaderFile.FileBytes.Length / 1024 / 1024

            If SizeFile > 2 Then
                cbFile.JSProperties("cpMessage") = "File size must not exceed 2 MB"
                Exit Sub
            End If
            'clsCostEstimationDB.UpdateFile(txtCENumber.Text, fullPath, FileName1, pUser)

            'cbFile.JSProperties("cpMessage") = "Upload File Succesfull"
            'cbFile.JSProperties("cpFileName") = FileName1
            'cbFile.JSProperties("cpMessage") = "Upload File Succesfull"

        End If


    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim ls_SQL As String = "", UserLogin As String = Session("user"), ls_ApprovalID As String = "1"
        Dim idx As Integer = 0

        Dim folderPath As String = Server.MapPath("~/Files/")
        Dim SizeFile As Single
        Dim fullPath As String

        If Not Directory.Exists(folderPath) Then
            'If Directory (Folder) does not exists. Create it.
            Directory.CreateDirectory(folderPath)
        End If
        Dim PostedFile1 As HttpPostedFile = uploaderFile.PostedFile
        Dim FileName1 As String = "", RenameFile As String = ""
        Dim CENumber As String = "", extension As String = ""

        Dim dsIAB As New DataSet
        dsIAB = clsCostEstimationAcceptanceUserDB.CheckIABudget(txtCENumber.Text, cbIABudget.Text, "")
        If dsIAB.Tables(0).Rows.Count > 0 Then
            cbSave.JSProperties("cpMessage") = "IA CAPEX/OPEX is already exists!"
            cbSave.JSProperties("cpStatus") = Session("cpStatus")
            Exit Sub
        End If

        'If rfvDoc.IsValid = False Then
        '    Exit Sub
        'End If

        'If uploaderFile.FileName = "" Or uploaderFile.FileName Is Nothing Then
        '    cbSave.JSProperties("cpMessage") = "Document File is required"
        '    cbDraft.JSProperties("cpStatus") = "2"
        '    Exit Sub
        'End If

        If uploaderFile.FileName <> "" Then
            extension = System.IO.Path.GetExtension(uploaderFile.FileName)
            FileName1 = Regex.Replace(Path.GetFileNameWithoutExtension(PostedFile1.FileName), "[\[\]\\\^\$\.\|\?\*\+\(\)\{\}%,;><!@#&\-\+/]", "") + extension
            uploaderFile.SaveAs(folderPath & FileName1)

            'FileName1 = Path.GetFileName(PostedFile1.FileName)
            fullPath = String.Format("{0}{1}", folderPath, FileName1)
            SizeFile = uploaderFile.FileBytes.Length / 1024 / 1024

            If SizeFile > 2 Then
                cbSave.JSProperties("cpMessage") = "File size must not exceed 2 MB"
                'cbSave.JSProperties("cpStatus") = "2"
                'Call DisplayMessage(MsgTypeEnum.ErrorMsg, "File size must not exceed 2 MB", cbExist)
                Exit Sub
            End If
        End If

        Try

            Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                sqlConn.Open()

                Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                    ls_SQL = "EXEC sp_CE_Acceptance_Detail_Accept " & _
                             "'" & txtCENumber.Text & "','" & txtRev.Text & "','" & cbIABudget.Text & "'," & _
                             "'3','" & UserLogin & "'" & _
                             ",'" & FileName1 & "'" & _
                             ",'" & fullPath & "'"

                    'sebelumnya pakai cbIABudget sementara textboxt

                    Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                    sqlComm.CommandType = CommandType.Text
                    sqlComm.ExecuteNonQuery()
                    sqlComm.Dispose()

                    sqlTran.Commit()
                End Using

            End Using

            cbSave.JSProperties("cpMessage") = "Data Submit successfully!"
            cbDraft.JSProperties("cpStatus") = "3"
            'cbDraft.JSProperties("cpFile") = ""
            cbSave.JSProperties("cpFileName") = FileName1
            cbSave.JSProperties("cpIABudget") = cbIABudget.Text
            'Call DisplayMessage(MsgTypeEnum.Success, "Data accepted successfully!", cbExist)
            'End If

        Catch ex As Exception
            'cbSave.JSProperties("cpMessage") = Err.Description
            'DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
        End Try

    End Sub
    'quotation attachment info
    Private Sub GridLoadAttachment(pRFQSetNo)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        Try
            ds = clsCostEstimationDB.GetDocumentQuotation_List(pRFQSetNo, ErrMsg)
            If ErrMsg = "" Then
                Grid_QuotationAtc.DataSource = ds.Tables(0)
                Grid_QuotationAtc.DataBind()

                If ds.Tables(0).Rows.Count = 0 Then
                    DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid_QuotationAtc)
                End If
            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid_QuotationAtc)
            End If

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid_QuotationAtc)
        End Try
    End Sub

    Private Sub Grid_QuotationAtc_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid_QuotationAtc.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_QuotationAtc_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid_QuotationAtc.CustomCallback
        Call GridLoadAttachment(cboRFQSetNumber.Value)
    End Sub

    Private Sub Grid_QuotationAtc_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid_QuotationAtc.HtmlDataCellPrepared
        'If e.DataColumn.FieldName = "FileType" Or e.DataColumn.FieldName = "FileName" Then
        'Editable
        e.Cell.BackColor = Color.LemonChiffon
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")

    End Sub

    Protected Sub keyFieldLink_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim link As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
        Dim container As GridViewDataItemTemplateContainer = TryCast(link.NamingContainer, GridViewDataItemTemplateContainer)

        If container.ItemIndex >= 0 Then
            link.Text = Split(container.KeyValue, "|")(0)
            link.Target = "_blank"
            link.NavigateUrl = Split(container.KeyValue, "|")(1)
        End If
    End Sub
End Class
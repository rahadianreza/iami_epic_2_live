﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CPList_NonTender.aspx.vb" Inherits="IAMI_EPIC2.CPList_NonTender" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function FillComboSupplier() {
        cboSupplierCode.PerformCallback();
    }

    function FillComboPRNumber() {
        cboPRNumber.PerformCallback(dtQuotationFrom.GetText() + '|' + dtQuotationTo.GetText());
    }

    function FillComboCPNumber() {
        cboCPNumber.PerformCallback(dtQuotationFrom.GetText() + '|' + dtQuotationTo.GetText() + '|' + cboPRNumber.GetText());
    }

    function GridLoad(s, e) {
        if (cboPRNumber.GetText() == '') {
            toastr.warning('Please select PR Number first!', 'Warning');
            cboPRNumber.Focus();
            return false;
        }
        if (cboPRNumber.GetSelectedIndex() == -1) {
            toastr.warning('Invalid PR Number!', 'Warning');
            cboPRNumber.Focus();
            return false;
        }

        if (cboCPNumber.GetText() == '') {
            toastr.warning('Please select Counter Proposal Number first!', 'Warning');
            cboCPNumber.Focus();
            return false;
        }

        if (cboCPNumber.GetSelectedIndex() == -1) {
            toastr.warning('Invalid Counter Proposal Number!', 'Warning');
            cboCPNumber.Focus();
            return false;
        }
        
        var dtFrom = dtQuotationFrom.GetDate();
        var YearFrom = dtFrom.getFullYear();
        var MonthFrom = dtFrom.getMonth();
        var DateFrom = dtFrom.getDate();
        var dtQFrom = new Date(YearFrom, MonthFrom, DateFrom)

        var dtTo = dtQuotationTo.GetDate();
        var YearTo = dtTo.getFullYear();
        var MonthTo = dtTo.getMonth();
        var DateTo = dtTo.getDate();
        var dtQTo = new Date(YearTo, MonthTo, DateTo)

        if (dtQFrom > dtQTo) {
            toastr.warning('CP Date From cannot be greater than CP Date To!', 'Warning');
            dtQuotationFrom.Focus();
            return;
        }

        Grid.PerformCallback('load|' + dtQuotationFrom.GetText() + '|' + dtQuotationTo.GetText());
    }

    function downloadValidation(s, e) {
        if (Grid.GetVisibleRowsOnPage() == 0) {
            toastr.info('There is no data to download!', 'Info');
            e.processOnServer = false;
            return;
        }
    }

    function GridCustomButtonClick(s, e) {
        if (e.buttonID == 'det') {
            var rowKey = Grid.GetRowKey(e.visibleIndex);
            
            var jsDate = dtQuotationFrom.GetDate();
            var year = jsDate.getFullYear(); // where getFullYear returns the year (four digits)
            var month = jsDate.getMonth()+1; // where getMonth returns the month (from 0-11)
            var day = jsDate.getDate();   // where getDate returns the day of the month (from 1-31)
            var myDateFrom = year + "-" + month + "-" + day;

            jsDate = dtQuotationTo.GetDate();
            year = jsDate.getFullYear(); // where getFullYear returns the year (four digits)
            month = jsDate.getMonth()+1; // where getMonth returns the month (from 0-11)
            day = jsDate.getDate();   // where getDate returns the day of the month (from 1-31)
            var myDateTo = year + "-" + month + "-" + day;

            var rowPos = Grid.GetVisibleRowsOnPage();

            //<12 Parameters>
            //0 : CPNumber
            //1 : Rev
            //2 : PRNumber
            //3 : RFQNumber
            //4 : QuotationNo
            //5 : SupplierCode
            //6 : QuotationDateFrom <return value for filter area at CPList.aspx>
            //7 : QuotationDateTo <return value for filter area at CPList.aspx>
            //8 : PRNumber <return value for filter area at CPList.aspx>
            //9 : SupplierCode <return value for filter area at CPList.aspx>
            //10: CPNumber <return value for filter area at CPList.aspx>
            //11: Row Position on the grid <return value for filter area at CPList.aspx>
            window.location.href = 'CPListDetail_NonTender.aspx?ID=' + rowKey + '|' + myDateFrom + '|' + myDateTo + '|' + cboPRNumber.GetText() + '|' + cboSupplierCode.GetText() + '|' + cboCPNumber.GetText() + '|' + rowPos;
        }
    }


    function btnAdd(s, e) {

    }


    function LoadCompleted(s, e) {
        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }
    }
</script>

<style type="text/css">
    .colwidthbutton
    {
        width: 90px;
    }
    
    .rowheight
    {
        height: 35px;
    }
       
    .col1
    {
        width: 10px;
    }
    .colLabel1
    {
        width: 133px;
    }
    .colLabel2
    {
        width: 150px;
    }
    .colInput1
    {
        width: 133px;
    }
    .colInput2
    {
        width: 133px;
    }
    .colSpace
    {
        width: 100px;
    }
        
    .customHeader {
        height: 20px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
            <tr class="rowheight">
                <td class="col1">
                    </td>
                <td class="colLabel1">
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="CP Date"></dx1:aspxlabel>
                </td>
                <td class="colInput1">
                    <dx:aspxdateedit ID="dtQuotationFrom" runat="server" Theme="Office2010Black" 
                        Width="120px" ClientInstanceName="dtQuotationFrom"
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        <ClientSideEvents ValueChanged="FillComboPRNumber" />
                    </dx:aspxdateedit>
                </td>
                <td style="width:20px">
                    <dx1:aspxlabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="~"></dx1:aspxlabel>
                </td>
                <td class="colInput1">
                    <dx:aspxdateedit ID="dtQuotationTo" runat="server" Theme="Office2010Black" 
                        Width="120px" AutoPostBack="false" ClientInstanceName="dtQuotationTo"
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        <ClientSideEvents ValueChanged="FillComboPRNumber" />
                    </dx:aspxdateedit> 
                </td>
                <td class="colSpace">
                    </td>
                <td class="colLabel2">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Supplier Name"></dx1:aspxlabel>
                </td>
                <td class="colInput2">
                    <dx1:ASPxComboBox ID="cboSupplierCode" runat="server" ClientInstanceName="cboSupplierCode"
                        Width="210px" Font-Names="Segoe UI"  TextField="Supplier_Name"
                        ValueField="Supplier_Code" TextFormatString="{1}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">
                        <ClientSideEvents Init="FillComboSupplier" Validation="" EndCallback="LoadCompleted" />
                        <Columns>            
                            <dx:ListBoxColumn Caption="Supplier Code" FieldName="Supplier_Code" Width="80px" />
                            <dx:ListBoxColumn Caption="Supplier Name" FieldName="Supplier_Name" Width="235px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx1:ASPxComboBox>
                </td>
                <td>
                    </td>
            </tr>
            <tr class="rowheight">
                <td>
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="PR Number"></dx1:aspxlabel>
                </td>
                <td colspan="3">
                    <dx1:ASPxComboBox ID="cboPRNumber" runat="server" ClientInstanceName="cboPRNumber"
                        Width="200px" Font-Names="Segoe UI"  TextField="PR_Number"
                        ValueField="PR_Number" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">
                        <ClientSideEvents Init="FillComboPRNumber" EndCallback="LoadCompleted" ValueChanged="FillComboCPNumber" />
                        <Columns>            
                            <dx:ListBoxColumn Caption="PR Number" FieldName="PR_Number" Width="100px" />                       
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx1:ASPxComboBox>
                </td>
                <td>
                    </td>
                <td> 
                    <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Counter Proposal Number"></dx1:aspxlabel>
                </td>
                <td>
                    <dx1:ASPxComboBox ID="cboCPNumber" runat="server" ClientInstanceName="cboCPNumber"
                        Width="210px" Font-Names="Segoe UI"  TextField="CP_Number"
                        ValueField="CP_Number" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">
                        <ClientSideEvents Init="FillComboCPNumber" Validation="" EndCallback="LoadCompleted" />
                        <Columns>            
                            <dx:ListBoxColumn Caption="Counter Proposal Number" FieldName="CP_Number" Width="100px" />                       
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx1:ASPxComboBox>
                </td>
                <td>
                    </td>
            </tr>
            <tr style="height:10px">
                <td colspan="9">
                    </td>
               </tr>
            <tr class="rowheight">
                <td></td>
                <td colspan="8">
                    <table>
                        <tr class="rowheight">
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnShow" runat="server" Text="Show Data" UseSubmitBehavior="False"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                                    ClientInstanceName="btnShow" Theme="Default">                        
                                    <ClientSideEvents Click="GridLoad" />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnExcel" Theme="Default">                        
                                    <Paddings Padding="2px" />
                                    <ClientSideEvents
                                        Click="downloadValidation"
                                    />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnAdd" Theme="Default">                        
                                    <Paddings Padding="2px" />                                    
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 10px">
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td> 
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                </td>
            </tr>
        </table>
    </div>
   <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="KeyField" >
            
            <Columns>
                <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" FixedStyle="Left" Width="50px">                    
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="det" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>                    
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Counter Proposal Number" FieldName="CP_Number"
                     VisibleIndex="1" Width="220px" FixedStyle="Left">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Rev" FieldName="Rev"
                     VisibleIndex="2" Width="40px" FixedStyle="Left">
                     <CellStyle HorizontalAlign="Center" Wrap="True"></CellStyle>
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Counter Proposal Date" FieldName="CP_Date" 
                     VisibleIndex="3" Width="100px" FixedStyle="Left">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center" Wrap="True"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="PR Number" FieldName="PR_Number" 
                     VisibleIndex="4" Width="180px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="PR Date" FieldName="PR_Date" 
                     VisibleIndex="5" Width="80px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
               <dx:GridViewDataTextColumn Caption="Due Date" FieldName="Due_Date" 
                     VisibleIndex="7" Width="120px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="RFQ Number" FieldName="RFQ_Number" 
                     VisibleIndex="8" Width="180px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="RFQ Date" FieldName="RFQ_Date" 
                     VisibleIndex="9" Width="90px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Quotation No." FieldName="Quotation_No" visible ="false"
                     VisibleIndex="10" Width="180px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Quotation Date" FieldName="Quotation_Date" visible="false"
                     VisibleIndex="11" Width="80px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Supplier Name" FieldName="Supplier_Name" 
                     VisibleIndex="12" Width="250px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Status" FieldName="CP_Status" 
                     VisibleIndex="13" Width="70px">
                    <Settings AutoFilterCondition="Contains" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewBandColumn Caption="Prepared" Name="Prepared" 
                    VisibleIndex="14">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Name" FieldName="Prepared_Name" 
                            VisibleIndex="0" Width="100px">
                            <CellStyle HorizontalAlign="Center"></CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Date" FieldName="Prepared_Date" 
                            VisibleIndex="1" Width="100px">
                            <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Center"></CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewDataTextColumn Caption="KeyFieldName" FieldName="KeyField" Visible="false"
                     VisibleIndex="17" Width="0px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
             </Columns>

            <SettingsBehavior AllowFocusedRow="True" AllowSort="False" AllowDragDrop="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
            <SettingsPager PageSize="9"></SettingsPager>
            <Settings ShowFilterRow="false" VerticalScrollableHeight="225" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" ShowStatusBar="Hidden"></Settings>
            
            <ClientSideEvents 
                CustomButtonClick="GridCustomButtonClick"
                EndCallback="LoadCompleted"
            />
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" 
                AllowInsert="False" />

            <Styles Header-Paddings-Padding="2px" EditFormColumnCaption-Paddings-PaddingLeft="0px" EditFormColumnCaption-Paddings-PaddingRight="0px" >
                <Header CssClass="customHeader" HorizontalAlign="Center" Wrap="True">
<Paddings Padding="2px"></Paddings>
                </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="0px" PaddingRight="0px"></Paddings>
</EditFormColumnCaption>
            </Styles>
        </dx:ASPxGridView>
    </div>
    <%--<div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
      
        </table>
    </div>--%>
</div>
</asp:Content>
﻿
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.UI
Imports System.IO
Imports System.Drawing
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls.Style
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxUploadControl
Imports System
Imports DevExpress.Utils
Imports System.Collections.Generic
Imports OfficeOpenXml

Public Class RFQCreate
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim pErr As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False


#End Region

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cpMessage") = ErrMsg
        Grid.JSProperties("cpType") = msgType
        Grid.JSProperties("cpVal") = pVal
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|C010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                         "btnDraft.SetEnabled(false);" & vbCrLf & _
                         "btnPrint.SetEnabled(false);"

                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboPRNumber)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        pUser = Session("user")
        'sGlobal.getMenu("C010")
        Master.SiteTitle = "REQUEST FOR QUOTATION CREATE" 'sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "C010 ")

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            dtDRFQDate.Value = Now
            dtDatelineDate.Value = DateAdd(DateInterval.Day, 7, Now)
            dtDateToday.Value = Now
            gs_SetRFQNo = ""
            up_DeleteTempRFQData()

            If Request.QueryString.Count = 0 Then
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                         "btnDraft.SetEnabled(true);" & vbCrLf & _
                         "btnPrint.SetEnabled(false);"

                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
            End If

            gs_RFQNo1 = ""
            gs_RFQNo2 = ""
            gs_RFQNo3 = ""
            gs_RFQNo4 = ""
            gs_RFQNo5 = ""




        End If
        
        gs_Back = True
    End Sub

    Private Sub up_LoadDataHeader()
        Dim ErrMsg As String = ""
        Dim vRFQSetNo As String = ""

        vRFQSetNo = gs_SetRFQNo

        Dim ds As New DataSet
        ds = clsRFQDB.GetRFQHeader(vRFQSetNo, ErrMsg)

        If ds.Tables(0).Rows.Count > 0 Then
            txtRev.Text = ds.Tables(0).Rows(0)("Rev")
            gs_Revision = ds.Tables(0).Rows(0)("Rev")
        End If

        'If ErrMsg = "" Then
        '    Grid.DataSource = ds
        '    Grid.DataBind()
        'Else
        '    show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        'End If
    End Sub

    Private Sub up_GridLoadRFQ(pRFQSetNo As String, pPRNo As String)
        Dim ErrMsg As String = ""
        Dim vPRNumber As String = ""
        Dim vRFQSetNo As String = ""
        Dim vRev As Integer

        Dim ds As New DataSet

        For a = 0 To 4
            vSupplier(a) = ""
        Next

        ds = clsRFQDB.GetRFQNumber(pRFQSetNo)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            vSupplier(i) = ds.Tables(0).Rows(i)("Supplier_Code") & ""
            vRFQNo(i) = ds.Tables(0).Rows(i)("RFQ_Number") & ""
        Next

        vPRNumber = pPRNo
        vRFQSetNo = pRFQSetNo
        vRev = txtRev.Text

        ds = clsRFQDB.GetDataRFQ(vPRNumber, vRFQSetNo, vRev, ErrMsg)

        'If ds.Tables(0).Rows.Count > 0 Then
        '    txtRev.Text = ds.Tables(0).Rows(0)("Rev")
        'End If

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_GridLoad(pPRNo As String)
        Dim ErrMsg As String = ""
        Dim vPRNumber As String = ""

        vPRNumber = pPRNo
        Dim ds As New DataSet
        ds = clsRFQDB.GetDataPR(vPRNumber, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub cboPRNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboPRNumber.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim Department As String = Split(e.Parameter, "|")(1)
        Dim Section As String = Split(e.Parameter, "|")(2)
        Dim PRType As String = Split(e.Parameter, "|")(3)

        If Department = "null" And Section = "null" And PRType = "null" Then
        Else
            up_FillComboPRNumber(Department, Section, PRType)


        End If

    End Sub

    Private Sub up_FillComboPRNumber(pDepartment As String, pSection As String, pPRType As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsRFQDB.GetComboPRNumber(pUser, pDepartment, pSection, pPRType, pErr)

        If pErr = "" Then
            cboPRNumber.DataSource = ds
            cboPRNumber.DataBind()
        End If
    End Sub

    Private Sub Grid_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If (e.Column.FieldName = "Material_No") Then
            e.Editor.ReadOnly = True
        Else
            e.Editor.ReadOnly = False
        End If
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
    End Sub

    Private Sub cbGrid_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbGrid.Callback

        Dim ds As New DataSet
        ds = clsRFQDB.GetInfoPR(cboPRNumber.Text, pErr)
        If ds.Tables(0).Rows.Count > 0 Then
            cbGrid.JSProperties("cpDepartment") = ds.Tables(0).Rows(0)("Department").ToString
            cbGrid.JSProperties("cpPRType") = ds.Tables(0).Rows(0)("PR_Type").ToString
            cbGrid.JSProperties("cpSection") = ds.Tables(0).Rows(0)("Section").ToString
        End If


    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim PRNo As String = Split(e.Parameters, "|")(1)
        Dim RFQSetNo As String = Split(e.Parameters, "|")(2)
       

        If pFunction = "load" Then
           

            up_GridLoad(PRNo)
        ElseIf pFunction = "draft" Then
            'up_LoadDataHeader()
            up_GridLoadRFQ(RFQSetNo, PRNo)

        End If
    End Sub

    Private Function uf_ConvertMonth(pMonth As Integer) As String
        Dim angka As String = ""

        If pMonth = 1 Then
            angka = "I"
        ElseIf pMonth = 2 Then
            angka = "II"
        ElseIf pMonth = 3 Then
            angka = "III"
        ElseIf pMonth = 4 Then
            angka = "IV"
        ElseIf pMonth = 5 Then
            angka = "V"
        ElseIf pMonth = 6 Then
            angka = "VI"
        ElseIf pMonth = 7 Then
            angka = "VII"
        ElseIf pMonth = 8 Then
            angka = "VIII"
        ElseIf pMonth = 9 Then
            angka = "IX"
        ElseIf pMonth = 10 Then
            angka = "X"
        ElseIf pMonth = 11 Then
            angka = "XI"
        ElseIf pMonth = 12 Then
            angka = "XII"
        End If

        Return angka
    End Function

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        If e.UpdateValues.Count = 0 Then
            gs_Message = "Data is not found"
            Exit Sub
        End If
        Dim pErr As String = ""

        Dim ls_PRNo As String = cboPRNumber.Text
        Dim RFQData As New clsRFQ
        ' Dim ls_Bln As String
        'Dim li_Bln As Integer = Format(dtDRFQDate.Value, "MM")

        Dim ls_RFQSetNumber As String = ""
        Dim ds As New DataSet


        If txtRev.Text = "" Then
            RFQData.Rev = 0
        Else
            RFQData.Rev = txtRev.Text
        End If

        If gs_SetRFQNo = "" Then
            'ds = clsRFQDB.GetMaxRFQSetNo(li_Bln, pErr)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    ls_Bln = uf_ConvertMonth(Format(dtDRFQDate.Value, "MM"))

            '    ls_RFQSetNumber = "IAMI/COST/RFQSET/" & ls_Bln & "/" & Format(dtDRFQDate.Value, "yy") & "/" & ds.Tables(0).Rows(0)("RFQ_SetNumber")

            'End If

            Dim cbo As New DevExpress.Web.ASPxEditors.ASPxComboBox
            Dim RFQSetNo As String = ""

            RFQData.PRNumber = cboPRNumber.Text
            RFQData.Deadline = Format(dtDatelineDate.Value, "yyyy-MM-dd")

            RFQData.RFQDate = Format(dtDRFQDate.Value, "yyyy-MM-dd")
            RFQData.Currency = cboCurrency.Text

            clsRFQDB.InsertSet(RFQData, pUser, RFQSetNo, pErr)
            RFQData.RFQSetNumber = RFQSetNo
            'gs_SetRFQNo = RFQSetNo
            ls_RFQSetNumber = RFQSetNo

        Else
            RFQData.RFQSetNumber = gs_SetRFQNo
            ls_RFQSetNumber = gs_SetRFQNo
        End If


        If cboSupplier1.SelectedIndex > -1 Then
            up_SaveDetail(cboSupplier1, RFQData, ls_RFQSetNumber, sender, e, gs_RFQNo1, pErr)
            gs_RFQNo1 = RFQData.RFQNumber
        End If
        If cboSupplier2.SelectedIndex > -1 Then
            up_SaveDetail(cboSupplier2, RFQData, ls_RFQSetNumber, sender, e, gs_RFQNo2, pErr)
            gs_RFQNo2 = RFQData.RFQNumber
        End If
        If cboSupplier3.SelectedIndex > -1 Then
            up_SaveDetail(cboSupplier3, RFQData, ls_RFQSetNumber, sender, e, gs_RFQNo3, pErr)
            gs_RFQNo3 = RFQData.RFQNumber
        End If
        If cboSupplier4.SelectedIndex > -1 Then
            up_SaveDetail(cboSupplier4, RFQData, ls_RFQSetNumber, sender, e, gs_RFQNo4, pErr)
            gs_RFQNo4 = RFQData.RFQNumber
        End If
        If cboSupplier5.SelectedIndex > -1 Then
            up_SaveDetail(cboSupplier5, RFQData, ls_RFQSetNumber, sender, e, gs_RFQNo5, pErr)
            gs_RFQNo5 = RFQData.RFQNumber
        End If


        If pErr = "" Then
            If gs_SetRFQNo = "" Then

                txtRFQNumber.Text = ls_RFQSetNumber

                gs_SetRFQNo = ls_RFQSetNumber
                gs_Message = "Draft data saved successfull"

                'Else
                '    gs_Message = "Data saved successfull"
                'gs_SetRFQNo = ""
            End If
          

        Else
            gs_Message = pErr
            gs_SetRFQNo = ""
        End If

    End Sub

    Private Sub up_SaveDetail(cboSupplier As DevExpress.Web.ASPxEditors.ASPxComboBox, pClsRFQ As clsRFQ, pRFQSet As String, sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs, pRFQNo As String, Optional ByRef pErr As String = "")
        Dim ds As New DataSet
        Dim ls_RFQNumber As String = ""
        Dim ls_Check As String
        Dim ls_MaterialNo As String
        Dim li_Qty As Integer
        Dim ls_Supplier As String

        Dim a As Integer
        a = e.UpdateValues.Count

        pClsRFQ.RFQSetNumber = pRFQSet
        pClsRFQ.RFQTitle = txtRFQTitle.Text

        If txtRev.Text = "" Then
            pClsRFQ.Rev = 0
        Else
            pClsRFQ.Rev = txtRev.Text
        End If

        ls_Supplier = cboSupplier.SelectedItem.GetValue("Code").ToString()
        pClsRFQ.Supplier = ls_Supplier

        'insert rfq header ( bukan tabel RFQSET)
        Dim RFQNoOutput As String = ""
        If pRFQNo = "" Or IsNothing(pRFQNo) Then
            clsRFQDB.InsertHeader(pClsRFQ, pUser, RFQNoOutput, pErr)
            pClsRFQ.RFQNumber = RFQNoOutput
            'pRFQNo = RFQNoOutput
            'insert detail from tmp rfq
            clsRFQDB.InsertDetailFromRFQData(pClsRFQ, RFQNoOutput, pUser, pErr)
        Else
            pClsRFQ.RFQNumber = pRFQNo
        End If


        For iLoop = 0 To a - 1
            ls_Check = (e.UpdateValues(iLoop).NewValues("AllowCheck").ToString())

            If ls_Check = True Then ls_Check = "1" Else ls_Check = "0"
            ls_MaterialNo = Trim(e.UpdateValues(iLoop).NewValues("Material_No").ToString())
            li_Qty = e.UpdateValues(iLoop).NewValues("Qty")

            pClsRFQ.MaterialNo = ls_MaterialNo
            pClsRFQ.Qty = li_Qty

            If ls_Check = "0" Then
                clsRFQDB.DeleteDetail(pClsRFQ, pErr)
            Else
                clsRFQDB.InsertDetail(pClsRFQ, pUser, pErr)
            End If

            If pErr <> "" Then
                Exit For
            End If
        Next iLoop

    End Sub
	
	'21-03-2019 
    Public Sub getSupplierValue(pRFQNo1 As String, pRFQNo2 As String, pRFQNo3 As String, pRFQNo4 As String, pRFQNo5 As String, Optional ByRef pErr As String = "")
        Dim vsup As String
        Dim ds As New DataSet
        Dim ls_Bln As String
        Dim li_Bln As Integer = Format(Now, "MM")
        Dim ls_RFQNumber As String = ""
        Dim RFQCls As New clsRFQ


        For a = 0 To 4
            vSupplier(a) = ""
            vRFQNo(a) = ""
        Next

        ds = clsRFQDB.GetRFQNumber(gs_SetRFQNo)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            vSupplier(i) = ds.Tables(0).Rows(i)("Supplier_Code") & ""
            vRFQNo(i) = ds.Tables(0).Rows(i)("RFQ_Number") & ""
        Next

        'If pDataChange = False Then
        If cboSupplier1.SelectedIndex >= 0 Then
            If vRFQNo(0) <> "" Then
                If vSupplier(0) <> cboSupplier1.SelectedItem.GetValue("Code").ToString() Then
                    vsup = cboSupplier1.SelectedItem.GetValue("Code").ToString()
                    clsRFQDB.UpdateHeader(vRFQNo(0), vsup, pUser, pErr)
                End If
            End If
        End If

        If cboSupplier2.SelectedIndex >= 0 Then
            If vRFQNo(1) <> "" Then
                If vSupplier(1) <> cboSupplier2.SelectedItem.GetValue("Code").ToString() And pRFQNo2 <> "" Then
                    vsup = cboSupplier2.SelectedItem.GetValue("Code").ToString()
                    clsRFQDB.UpdateHeader(vRFQNo(1), vsup, pUser, pErr)
                    'Else
                    '    RFQCls.RFQSetNumber = gs_SetRFQNo
                    '    RFQCls.RFQNumber = vRFQNo(1)
                    '    RFQCls.Supplier = cboSupplier2.SelectedItem.GetValue("Code").ToString()
                    '    RFQCls.RFQTitle = txtRFQTitle.Text
                    '    RFQCls.Rev = txtRev.Text
                    '    clsRFQDB.CopyInsertDetail(RFQCls, ls_RFQNumber, pUser, pErr)
                End If
            Else

                RFQCls.RFQSetNumber = gs_SetRFQNo
                RFQCls.Supplier = cboSupplier2.SelectedItem.GetValue("Code").ToString()
                RFQCls.RFQTitle = txtRFQTitle.Text
                RFQCls.Rev = txtRev.Text

                Dim RFQNoOutput As String = ""

                clsRFQDB.InsertHeader(RFQCls, pUser, RFQNoOutput, pErr)
                RFQCls.RFQNumber = RFQNoOutput
                gs_RFQNo2 = RFQNoOutput
                'ls_RFQNumber = RFQNoOutput

                ' up_GridLoadRFQ(gs_SetRFQNo, PRNo)

                clsRFQDB.InsertDetailFromRFQData(RFQCls, RFQNoOutput, pUser, pErr)

            End If
        End If
        If cboSupplier3.SelectedIndex >= 0 Then
            If vRFQNo(2) <> "" Then
                If vSupplier(2) <> cboSupplier3.SelectedItem.GetValue("Code").ToString() And pRFQNo3 <> "" Then
                    vsup = cboSupplier3.SelectedItem.GetValue("Code").ToString()
                    clsRFQDB.UpdateHeader(vRFQNo(2), vsup, pUser, pErr)
                End If
            Else
                RFQCls.RFQSetNumber = gs_SetRFQNo
                RFQCls.Supplier = cboSupplier3.SelectedItem.GetValue("Code").ToString()
                RFQCls.RFQTitle = txtRFQTitle.Text
                RFQCls.Rev = txtRev.Text

                Dim RFQNoOutput As String = ""

                clsRFQDB.InsertHeader(RFQCls, pUser, RFQNoOutput, pErr)
                RFQCls.RFQNumber = RFQNoOutput
                gs_RFQNo3 = RFQNoOutput
                'ls_RFQNumber = RFQNoOutput

                clsRFQDB.InsertDetailFromRFQData(RFQCls, RFQNoOutput, pUser, pErr)

            End If
        End If
        If cboSupplier4.SelectedIndex >= 0 Then
            If vRFQNo(3) <> "" Then
                If vSupplier(3) <> cboSupplier4.SelectedItem.GetValue("Code").ToString() And pRFQNo4 <> "" Then
                    vsup = cboSupplier4.SelectedItem.GetValue("Code").ToString()
                    clsRFQDB.UpdateHeader(vRFQNo(3), vsup, pUser, pErr)
                End If
            Else
                RFQCls.RFQSetNumber = gs_SetRFQNo
                RFQCls.Supplier = cboSupplier4.SelectedItem.GetValue("Code").ToString()
                RFQCls.RFQTitle = txtRFQTitle.Text
                RFQCls.Rev = txtRev.Text

                Dim RFQNoOutput As String = ""

                clsRFQDB.InsertHeader(RFQCls, pUser, RFQNoOutput, pErr)
                RFQCls.RFQNumber = RFQNoOutput
                gs_RFQNo4 = RFQNoOutput
                'ls_RFQNumber = RFQNoOutput

                clsRFQDB.InsertDetailFromRFQData(RFQCls, RFQNoOutput, pUser, pErr)

            End If
        End If
        If cboSupplier5.SelectedIndex >= 0 Then
            If vRFQNo(4) <> "" Then
                If vSupplier(4) <> cboSupplier5.SelectedItem.GetValue("Code").ToString() And pRFQNo5 <> "" Then
                    vsup = cboSupplier5.SelectedItem.GetValue("Code").ToString()
                    clsRFQDB.UpdateHeader(vRFQNo(4), vsup, pUser, pErr)
                End If
            Else
                RFQCls.RFQSetNumber = gs_SetRFQNo
                RFQCls.Supplier = cboSupplier5.SelectedItem.GetValue("Code").ToString()
                RFQCls.RFQTitle = txtRFQTitle.Text
                RFQCls.Rev = txtRev.Text

                Dim RFQNoOutput As String = ""

                clsRFQDB.InsertHeader(RFQCls, pUser, RFQNoOutput, pErr)
                RFQCls.RFQNumber = RFQNoOutput
                gs_RFQNo5 = RFQNoOutput
                'ls_RFQNumber = RFQNoOutput

                clsRFQDB.InsertDetailFromRFQData(RFQCls, RFQNoOutput, pUser, pErr)

            End If
        End If
    End Sub

    Private Sub up_UpdateDataHeader(pRFQNo1 As String, pRFQNo2 As String, pRFQNo3 As String, pRFQNo4 As String, pRFQNo5 As String, Optional ByRef pMsg As String = "")
		 'supplier value 1-5
        getSupplierValue(pRFQNo1, pRFQNo2, pRFQNo3, pRFQNo4, pRFQNo5, pMsg)

        '21/03/2019 --update rfq set (deadline date , currency)
        Dim ds1 As New DataSet
        ds1 = clsRFQDB.UpdateRFQSet(gs_SetRFQNo, txtRev.Text, Format(dtDatelineDate.Value, "yyyy-MM-dd"), cboCurrency.Text, pErr)

        If pErr = "" Then
            gs_Message = "Draft data saved successfull"
        End If

        'clsRFQDB.UpdateHeader(pClsRFQ, pErr)
        'End If
        'Dim rev As Integer

        'If txtRev.Text = "" Then
        '    rev = 0
        'Else
        '    rev = txtRev.Text
        'End If
        'clsRFQDB.UpdateStatus(gs_SetRFQNo, pUser, rev, pErr)

        'If pErr = "" Then
        '    clsRFQDB.UpdateStatus(gs_SetRFQNo, pErr)
        'Else
        '    pMsg = pErr
        'End If

        pDataChange = False
    End Sub

    Private Sub up_DeleteTempRFQData(Optional ByRef pErr As String = "")
        Try
            clsRFQDB.DeleteTempDataRFQ(pErr)

        Catch ex As Exception
            gs_Message = ex.Message
        End Try
    End Sub

    Private Sub up_UpdateStatus(Optional ByRef pErr As String = "")
        Try
			Dim rev As Integer
            rev = txtRev.Text
            Dim DDate As String
            DDate = Format(dtDatelineDate.Value, "yyyy-MM-dd")
            'clsRFQDB.UpdateData(gs_SetRFQNo, pUser, txtRev.Text, pErr)
            clsRFQDB.UpdateData(gs_SetRFQNo, pUser, rev, DDate, txtRFQTitle.Text, cboCurrency.Text, pErr)
            If pErr = "" Then
                gs_Message = "Data saved successfull"
            Else
                gs_Message = pErr
            End If

        Catch ex As Exception
            gs_Message = ex.Message
        End Try
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "AllowCheck" Then
            'Editable
            e.Cell.BackColor = Color.White
        Else
            'Non-Editable
            e.Cell.BackColor = Color.LemonChiffon
        End If
    End Sub

    Private Sub cbDraft_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDraft.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pRFQSetNo As String = Split(e.Parameter, "|")(1)
        Dim pRFQNo01 As String = Split(e.Parameter, "|")(4)
        Dim pRFQNo02 As String = Split(e.Parameter, "|")(5)
        Dim pRFQNo03 As String = Split(e.Parameter, "|")(6)
        Dim pRFQNo04 As String = Split(e.Parameter, "|")(7)
        Dim pRFQNo05 As String = Split(e.Parameter, "|")(8)

        Dim perr As String = ""

        If pFunction = "draft" Then
            If pRFQSetNo <> "--NEW--" Then
                up_UpdateDataHeader(pRFQNo01, pRFQNo02, pRFQNo03, pRFQNo04, pRFQNo05)
            End If

            up_LoadDataHeader()

            
        Else
			getSupplierValue(pRFQNo01, pRFQNo02, pRFQNo03, pRFQNo04, pRFQNo05, perr)
            up_UpdateStatus(perr)
        End If

		cbDraft.JSProperties("cpRevision") = gs_Revision
        cbDraft.JSProperties("cpMessage") = gs_Message
        cbDraft.JSProperties("cpRFQSetNo") = gs_SetRFQNo
        cbDraft.JSProperties("cpRFQNo1") = gs_RFQNo1
        cbDraft.JSProperties("cpRFQNo2") = gs_RFQNo2
        cbDraft.JSProperties("cpRFQNo3") = gs_RFQNo3
        cbDraft.JSProperties("cpRFQNo4") = gs_RFQNo4
        cbDraft.JSProperties("cpRFQNo5") = gs_RFQNo5
			
        cbDraft.JSProperties("cpMessage") = gs_Message
        gs_Message = ""
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        gs_RFQNo1 = ""
        gs_RFQNo2 = ""
        gs_RFQNo3 = ""
        gs_RFQNo4 = ""
        gs_RFQNo5 = ""
        gs_SetRFQNo = ""
        Response.Redirect("~/RFQList.aspx")
    End Sub

    'Private Sub cbDate_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDate.Callback
    '    dtDatelineDate.Value = DateAdd(DateInterval.Day, 10, dtDRFQDate.Value)
    'End Sub

    'Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
    '    Session("RFQSet") = txtRFQNumber.Text
    '    Session("PRNumber") = cboPRNumber.Text
    '    Response.Redirect("~/ViewRFQ.aspx")
    'End Sub

    Private Sub cbReject_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbReject.Callback

        'cbReject.JSProperties("cpMessage") = gs_Message
    End Sub

    Private Sub cbPrint_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbPrint.Callback
        Dim RFQSetNo As String = Split(e.Parameter, "|")(0)
        Dim PRNo As String = Split(e.Parameter, "|")(1)
        Dim Revision As String = Split(e.Parameter, "|")(2)


        Session("RFQSet") = RFQSetNo
        Session("PRNumber") = PRNo
        Session("Revision") = Revision

        'Response.Redirect("~/ViewRFQ.aspx")


    End Sub

   
  
End Class
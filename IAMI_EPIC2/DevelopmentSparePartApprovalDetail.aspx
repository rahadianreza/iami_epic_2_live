﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="DevelopmentSparePartApprovalDetail.aspx.vb" Inherits="IAMI_EPIC2.DevelopmentSparePartApprovalDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
        
        function GetMessage(s, e) {
            if (s.cpMessage == "Data Approved Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                btnApprove.SetEnabled(false);
                Grid.SetEnabled(false);

                setTimeout(function () {
                    window.location.href = 'DevelopmentSparePartApproval.aspx' ;
                }, 1000);
                
            } else {
                toastr.error(s.cpMessage, 'Error');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
    
        }
</script>
<style type="text/css">
    .td-col-l
    {
        padding:0px 0px 0px 10px;
        width:120px;
    }
    .td-col-m
    {
        width:10px;
    }
    td.col-r
    {
        width:200px;
    }
    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding:5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
             <tr style="height:10px">
                <td class="td-col-l"></td>
                <td class="td-col-m"></td>
                <td class="td-col-r"></td>
                <td ></td>
                <td></td>
            </tr>
              <tr style="height:35px">
                <td colspan="5" style="padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnBack" Theme="Default">                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp; &nbsp;
                     <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload"  Theme="Default">                        
                        <Paddings Padding="2px" />
                        <%--<ClientSideEvents Click="Validation" />--%>
                    </dx:ASPxButton>
                    &nbsp; &nbsp;
                    <dx:ASPxButton ID="btnApprove" runat="server" Text="Approve" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnApprove"  Theme="Default">                        
                        <Paddings Padding="2px" />
                        <ClientSideEvents Click="function (s,e) { cbApprove.PerformCallback('approve');}" />
                    </dx:ASPxButton>
                    </td>
            </tr>
            <tr style="height:10px">
                <td colspan="5">
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                </td>
                
                    
            </tr>
        </table>
        </div>
        <div>
            <dx:ASPxCallback ID="cbApprove" runat="server" ClientInstanceName="cbApprove" >
                <ClientSideEvents EndCallback="GetMessage" />
            </dx:ASPxCallback>
        </div>
        <div style="padding:5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                    EnableTheming="True" KeyFieldName="ProjectYear,ProjectID,PartNo,Model" Theme="Office2010Black" Width="100%" 
                    Font-Names="Segoe UI" Font-Size="9pt"  >
                 
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                HorizontalScrollBarMode="Auto" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="No" FieldName="No" VisibleIndex="1" Width="50px">
                        <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px" PaddingTop="3px" PaddingBottom="3px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ProjectYear" Caption="Project Year" VisibleIndex="2"
                        Width="100px">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px" PaddingTop="3px" PaddingBottom="3px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PartNo" Caption="Part No" VisibleIndex="3"
                        Width="120px">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px" PaddingTop="3px" PaddingBottom="3px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PartName" Width="180px" Caption="Part Name"
                        VisibleIndex="4">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn FieldName="Model" Width="100px" Caption="ModelCd" Visible ="false"
                        VisibleIndex="5">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ModelName" Width="100px" Caption="Model"
                        VisibleIndex="6">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="FIGNo" Width="120px" Caption="FIG No New/Multi"
                        VisibleIndex="7">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="KeyNo" Width="120px" Caption="Key No"
                        VisibleIndex="8">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TargetPrice" Caption="Target Price" VisibleIndex="9" >
                     <PropertiesTextEdit DisplayFormatString= "#,##0">
                     </PropertiesTextEdit>
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="TargetConsumption" Caption="Target Consumption" VisibleIndex="10" Width="150px" >
                     <PropertiesTextEdit DisplayFormatString="#,##0">
                     </PropertiesTextEdit>
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                </SettingsPager>
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                    HorizontalScrollBarMode="Auto" />
                <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>
                    <EditFormColumnCaption>
                        <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
                    </EditFormColumnCaption>
                </Styles>
            </dx:ASPxGridView>
           
        </div>
      
    </div>
    
</asp:Content>
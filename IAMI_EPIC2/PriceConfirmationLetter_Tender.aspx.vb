﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class PriceConfirmationLetter_Tender
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim TransactionNumber As String = ""
      
        TransactionNumber = Request.QueryString("ID")

        up_LoadReport(TransactionNumber)
        ' up_LoadReport_Resume()
    End Sub

    Private Sub up_LoadReport(TransactionNumber As String)
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptPriceConfirmationLetter_Tender

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_PriceConfirmationLetter_Tender '" & TransactionNumber & "'"

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "PriceConfirmationLetter_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report
                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub
End Class
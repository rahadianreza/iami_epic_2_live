﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class RFQApproval
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False

#End Region

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_ExcelGrid()
        Try

            up_GridLoad(gs_DateFrom, gs_DateTo, gs_StatusApproval)

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=RFQApproval" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            gs_Message = ex.Message
        End Try
    End Sub

    Private Sub up_GridLoad(pDate1 As String, pDate2 As String, pStatus As String)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        ' up_GridHeader()
        ds = clsRFQApprovalDB.GetList(pDate1, pDate2, pStatus, pUser, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()

            If ds.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cp_message") = "Grid Load Data Success"
            Else
                Grid.JSProperties("cp_message") = ""
            End If

            'show_error(MsgTypeEnum.Success, ErrMsg, 1)
        Else
            Grid.JSProperties("cp_message") = ""
            'show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_GridHeader()
        Dim ErrMsg As String = ""
        Dim pDate1 As String = ""
        Dim pDate2 As String = ""

        pDate1 = Format(dtDateFrom.Value, "yyyy-MM-dd")
        pDate2 = Format(dtDateTo.Value, "yyyy-MM-dd")


        For i = 0 To 1
            Grid.Columns.Item(15 + i).Visible = False
        Next

        Dim ds As New DataSet
        ds = clsRFQApprovalDB.GetHeaderApproval(pDate1, pDate2, pUser, ErrMsg)

        If ErrMsg = "" Then

            For i = 0 To ds.Tables(0).Rows.Count - 1
                Grid.Columns.Item(15 + i).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                Grid.Columns.Item(15 + i).Visible = True
            Next

        End If
    End Sub
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("C020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "C020")

        If (Not Page.IsPostBack) Then
            Dim dfrom As Date
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dtDateFrom.Value = dfrom
            dtDateTo.Value = Now
            cboStatus.SelectedIndex = 1
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        up_GridLoad(gs_DateFrom, gs_DateTo, gs_StatusApproval)
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pDateFrom As Date = Split(e.Parameters, "|")(1)
        Dim pDateTo As Date = Split(e.Parameters, "|")(2)

        Dim pStatusApproval As String = Split(e.Parameters, "|")(3)

        Dim vDate1, vDate2, vApp As String

        vDate1 = Format(pDateFrom, "yyyy-MM-dd")
        vDate2 = Format(pDateTo, "yyyy-MM-dd")

        'If pApproval = "YES" Then
        '    vApp = "1"
        'Else
        '    vApp = "0"
        'End If


        gs_DateFrom = vDate1
        gs_DateTo = vDate2
        gs_StatusApproval = pStatusApproval
        'gs_Approval = vApp
        If pFunction = "gridload" Then
            up_GridLoad(vDate1, vDate2, gs_StatusApproval)
        End If
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        Dim ErrMsg As String = ""
        up_ExcelGrid()
    End Sub
End Class
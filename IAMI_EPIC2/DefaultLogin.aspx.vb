﻿Imports IAMIEngine

Public Class DefaultLogin
    Inherits System.Web.UI.Page
    Dim clsDESEncryption As New clsDESEncryption("TOS")

    '2, 5, 7, 9, 10

#Region "Declaration"
    Dim UserUnlocked As Boolean
#End Region

#Region "Control Events"
    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLogin.Click
        Try
            If validation() Then
                Session("user") = txtusername.Text
                Response.Redirect("Main.aspx")
            End If
        Catch ex As Exception
            lblInfo.Text = ex.Message
        End Try
    End Sub
    Private Sub DefaultLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UsernameLogin As String = ""
        Dim PasswordLogin As String = ""


        If Request.QueryString("ID") <> "" Then
            UsernameLogin = Request.QueryString("ID")
        End If
        txtusername.Text = UsernameLogin

        btnVersion.Text = "Version: " & System.Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString()
    End Sub
#End Region

#Region "Function"
    Private Function validation() As Boolean
        Dim cUserSetup As New clsUserSetup

        Try
            If txtusername.Text.Trim = "" Then
                lblInfo.Visible = True
                lblInfo.Text = "Please input user ID!"
                txtusername.Focus()
                Return False
            End If

            If txtpassword.Text.Trim = "" Then
                lblInfo.Visible = True
                lblInfo.Text = "Please input password!"
                txtpassword.Focus()
                Return False
            End If

            Dim User As clsUserSetup = clsUserSetupDB.GetData(txtusername.Text, cUserSetup.ConnectionString)
            If User Is Nothing Then
                lblInfo.Visible = True
                lblInfo.Text = "Invalid User Name!"
                txtusername.Focus()
                Return False
            End If

            'Dim cJSOXSetup = New clsJSOXSetup
            'cJSOXSetup.RuleID = "10"
            'Dim jsox As clsJSOXSetup = clsJSOXSetupDB.GetRule(cJSOXSetup)
            'If jsox.Enable Then
            '    Dim iUpd As Integer = clsUserSetupDB.UnlockUser(User.UserID, jsox.ParamValue, cUserSetup.ConnectionString)
            '    If iUpd > 0 Then
            '        User.Locked = 0
            '    End If
            'End If

            Dim pwd As String = clsDESEncryption.DecryptData(User.Password)
            If pwd <> txtpassword.Text Then


                cUserSetup = New clsUserSetup
                cUserSetup.UserID = txtusername.Text
                clsUserSetupDB.AddFailedLogin(cUserSetup)

                cUserSetup.UserID = txtusername.Text
                UserUnlocked = clsUserSetupDB.GetUserLock(cUserSetup, "")

                lblInfo.Visible = True
                If UserUnlocked = False Then
                    'linkForgot.ClientVisible = False
                    lblInfo.Text = "User is locked after 3 failed logins." & vbCrLf & "Please contact your administrator!"
                    txtusername.Focus()
                    Return False
                Else
                    'linkForgot.Text = "Forgot password?"
                    ' linkForgot.ClientVisible = True
                    lblInfo.Text = "Invalid Password!"
                    'If User.PasswordHint <> "" Then
                    '    lblInfo.Text = lblInfo.Text & vbCrLf & "Password hint: " & User.PasswordHint
                    'End If
                    txtpassword.Focus()
                    txtusername.Focus()
                    Return False
                End If
            End If

            If User.Locked = "1" And User.AdminStatus <> "1" Then
                '  linkForgot.ClientVisible = False
                lblInfo.Visible = True
                lblInfo.Text = "User is locked. Please contact your administrator!"
                txtusername.Focus()
                Return False
            End If

            Session("user") = txtusername.Text
            Session("AdminStatus") = User.AdminStatus
            Session("UserName") = User.UserName

            'Dim cPasswordHistory As New clsPasswordHistory            
            'cPasswordHistory.UserID = txtusername.Text
            'Dim His As clsPasswordHistory = clsPasswordHistoryDB.GetLastData(cPasswordHistory)
            'If His IsNot Nothing Then
            '    cJSOXSetup = New clsJSOXSetup
            '    cJSOXSetup.RuleID = "3"
            '    jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
            '    Dim ServerDate As Date = clsPasswordHistoryDB.GetServerDate(cJSOXSetup.ConnectionString)
            '    Dim dif As Integer = DateDiff(DateInterval.Day, His.UpdateDate, ServerDate)
            '    Dim expireDay As Integer = jsox.ParamValue
            '    If dif > expireDay Then
            '        cJSOXSetup.RuleID = "2"
            '        jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
            '        Dim agingDay As Integer = jsox.ParamValue
            '        If jsox.Enable Then
            '            If dif > expireDay + agingDay Then
            '                lblInfo.Text = "Password expired! Contact your Administrator."
            '                txtusername.Focus()
            '                Return False
            '            ElseIf dif > expireDay Then
            '                lblInfo.Text = "Password expired! Please change your password."
            '                linkForgot.Text = "Change Password"
            '                linkForgot.ClientSideEvents.Click = "function (s,e) {{location.href = 'ChangePassword.aspx';}}"
            '                linkForgot.ClientVisible = True
            '                txtusername.Focus()
            '                Return False
            '            End If
            '        End If
            '    End If
            'End If

            cUserSetup.UserID = txtusername.Text
            cUserSetup.Locked = "0"
            clsUserSetupDB.UpdateLock(cUserSetup)

            cUserSetup.UserID = txtusername.Text
            clsUserSetupDB.ResetFailedLogin(cUserSetup)

            Return True
        Catch ex As Exception
            Response.Write("Validation Exception :<br>" & ex.ToString)
            Return False
        End Try
    End Function

    Sub Clear()
        Me.txtusername.Text = ""
        Me.txtpassword.Text = ""
        Me.txtusername.Focus()
    End Sub
#End Region

End Class
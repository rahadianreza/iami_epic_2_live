﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Material_Price.aspx.vb" 
Inherits="IAMI_EPIC2.Material_Price" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
   
    //GET MESSAGE
        function GetMessage(s, e) {
            if (s.cp_disabled == "N") {
                //alert(s.cp_disabled);
                btnDownload.SetEnabled(true);
            } else if (s.cp_disabled == "Y") {

                btnDownload.SetEnabled(false);
            }
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        
    }

    function ItemCodeValidation(s, e) {
        if (ItemCode.GetValue() == null) {            
              e.isValid = false;           
        }
      }

   function downloadValidation(s, e) {
       if (Grid.GetVisibleRowsOnPage() == 0) {
           toastr.info('There is no data to download!', 'Info');
           e.processOnServer = false;
           return;
       }
   }

   function showDataValidation(s, e) {
       if (Grid.GetVisibleRowsOnPage() == 0) {
           toastr.info('There is no data to show!', 'Info');
           e.processOnServer = false;
           return;
       }
   }

   //popup calender month year
   function formatDateISO(date) {
       var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

       if (month.length < 2) month = '0' + month;
       if (day.length < 2) day = '0' + day;

       return [year, month, day].join('-');
   }

   function Period_From_OnInit(s, e) {
       var calendar = s.GetCalendar();
       calendar.owner = s;
       //calendar.SetVisible(false);
       calendar.GetMainElement().style.opacity = '0';

       var d = new Date();
       var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
       Period_From.SetDate(myDate);
   }

   function Period_From_OnDropDown(s, e) {
       var calendar = s.GetCalendar();
       var fastNav = calendar.fastNavigation;
       fastNav.activeView = calendar.GetView(0, 0);
       fastNav.Prepare();
       fastNav.GetPopup().popupVerticalAlign = "Below";
       fastNav.GetPopup().ShowAtElement(s.GetMainElement())

       fastNav.OnOkClick = function () {
           var parentDateEdit = this.calendar.owner;
           var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
           parentDateEdit.SetDate(currentDate);
           parentDateEdit.HideDropDown();

           var startDate = formatDateISO(Period_From.GetDate());
           var endDate = formatDateISO(Period_To.GetDate());

           if (startDate > endDate) {
               Message(2, 'Period from must be lower than period to!');
               Period_From.Focus();
               return false;
           }
       }

       fastNav.OnCancelClick = function () {
           var parentDateEdit = this.calendar.owner;
           parentDateEdit.HideDropDown();
       }
   }

   //period to
   function Period_To_OnInit(s, e) {
       var calendar = s.GetCalendar();
       calendar.owner = s;
       //calendar.SetVisible(false);
       calendar.GetMainElement().style.opacity = '0';

       var d = new Date();
       var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
       Period_To.SetDate(myDate);
   }

   function Period_To_OnDropDown(s, e) {
       var calendar = s.GetCalendar();
       var fastNav = calendar.fastNavigation;
       fastNav.activeView = calendar.GetView(0, 0);
       fastNav.Prepare();
       fastNav.GetPopup().popupVerticalAlign = "Below";
       fastNav.GetPopup().ShowAtElement(s.GetMainElement())

       fastNav.OnOkClick = function () {
           var parentDateEdit = this.calendar.owner;
           var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
           parentDateEdit.SetDate(currentDate);
           parentDateEdit.HideDropDown();

           var startDate = formatDateISO(Period_From.GetDate());
           var endDate = formatDateISO(Period_To.GetDate());

           if (startDate > endDate) {
               Message(2, 'Period from must be lower than period to!');
               Period_From.Focus();
               return false;
           }
       }

       fastNav.OnCancelClick = function () {
           var parentDateEdit = this.calendar.owner;
           parentDateEdit.HideDropDown();
       }
   }
   //end popup calender


</script>

    <style type="text/css">
        .style1
        {
            width: 82px;
        }
        .style5
        {
            height: 2px;
        }
    </style>
    <style type="text/css">
        .td-col-l
        {
            padding:0px 0px 0px 10px;
            width:80px;
            
        }
        .td-col-m
        {
            width:10px;
        }
        .td-col-r
        {
            width:100px;
        }
        .td-col-f
        {
            width:50px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">

<%--         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%"
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="ItemCode" DataSourceID="SqlDataSource1">--%>

<table style="width: 100%; border: 1px solid black">  
    <tr style="height:10px">
        <td class="td-col-r" colspan="9"></td>
   </tr>
    <tr style="height:35px">
        <td style="padding:0px 0px 0px 10px; width:80px">
            <dx1:ASPxLabel ID="lblPeriodFrom" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                           Text="Period From" ClientInstanceName="lblPeriodFrom">
            </dx1:ASPxLabel>
        </td>
        <td class="td-col-m"></td>
        
        <td class="td-col-r">
            <dx:ASPxDateEdit ID="Period_From" runat="server" ClientInstanceName="Period_From" EnableTheming="True"
                ShowShadow="false" Font-Names="Segoe UI" Theme="Office2010Black" Font-Size="9pt"
                Height="22px" DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy" Width="120px">
                <ClientSideEvents DropDown="Period_From_OnDropDown" Init="Period_From_OnInit" />
            </dx:ASPxDateEdit>
        </td>
        <td class="td-col-f"></td>

        <td style="width:80px">
            <dx1:ASPxLabel ID="lblPeriodTo" runat="server"  Font-Names="Segoe UI" Font-Size="9pt" 
                           Text="Period To" ClientInstanceName="lblPeriodTo">
            </dx1:ASPxLabel>
        </td>
        <td class="td-col-m"></td>
        <td class="td-col-r" >
            <dx:ASPxDateEdit ID="Period_To" runat="server" ClientInstanceName="Period_To" EnableTheming="True"
                ShowShadow="false" Font-Names="Segoe UI" Theme="Office2010Black" Font-Size="9pt"
                Height="22px" DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy" Width="120px">
                <ClientSideEvents DropDown="Period_To_OnDropDown" Init="Period_To_OnInit" />
            </dx:ASPxDateEdit>
        </td>
        <td></td>
        <td></td>
        
    </tr>
    <tr style="height:35px">
        <td style="padding:0px 0px 0px 10px;">
             <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Material Type" ClientInstanceName="lblType">
            </dx1:ASPxLabel>
        </td>
        <td class="td-col-m"></td>
        <td class="td-col-r">
            <dx:ASPxComboBox ID="cboMaterialType" runat="server" ClientInstanceName="cboMaterialType"
                Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code" 
                DataSourceID="SqlDataSource2" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                Height="22px">
                
                <Columns>
                    <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                    <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                </Columns>
                <ItemStyle Height="10px" Paddings-Padding="4px">
                    <Paddings Padding="4px"></Paddings>
                </ItemStyle>
                <ButtonStyle Width="5px" Paddings-Padding="4px">
                    <Paddings Padding="4px"></Paddings>
                </ButtonStyle>
            </dx:ASPxComboBox>
             <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="select 'ALL' Code , 'ALL' Description UNION ALL select Par_Code,Par_Description from Mst_Parameter where Par_Group='MaterialType'">
            </asp:SqlDataSource>
        </td>
        <td colspan="5"></td>
    </tr>
    <tr style="height:35px">
        <td style="padding:0px 0px 0px 10px; width:80px">
             <dx1:ASPxLabel ID="lblType" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Supplier" ClientInstanceName="lblType">
            </dx1:ASPxLabel>
        </td>
        <td class="td-col-m"></td>
        <td class="td-col-r">
            <dx:ASPxComboBox ID="cboSupplier" runat="server" ClientInstanceName="cboSupplier"
                Width="170px" Font-Names="Segoe UI" TextField="SupplierName" ValueField="SupplierCode"
                DataSourceID="SqlDataSource1" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                Height="22px">
                
                <Columns>
                    <dx:ListBoxColumn Caption="Code" FieldName="SupplierCode" Width="100px" />
                    <dx:ListBoxColumn Caption="Description" FieldName="SupplierName" Width="250px" />
                </Columns>
                <ItemStyle Height="10px" Paddings-Padding="4px">
                    <Paddings Padding="4px"></Paddings>
                </ItemStyle>
                <ButtonStyle Width="5px" Paddings-Padding="4px">
                    <Paddings Padding="4px"></Paddings>
                </ButtonStyle>
            </dx:ASPxComboBox>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT 'ALL' SupplierCode,'ALL' SupplierName UNION ALL SELECT SupplierCode,SupplierName FROM VW_Mst_Material_getSupplier">
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr style="height:10px">
        <td class="td-col-r" colspan="9"></td>
    </tr>
    <tr style="height: 35px">
        <td style=" padding:0px 0px 0px 10px" colspan="9">            
            <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                ClientInstanceName="btnRefresh" Theme="Default">                        
                <ClientSideEvents Click="function(s, e) {
                    if ( Period_From.GetValue() &gt; Period_To.GetValue()){
						    toastr.warning('Period To must greater than Period From', 'Warning');
                            toastr.options.closeButton = false;
                            toastr.options.debug = false;
                            toastr.options.newestOnTop = false;
                            toastr.options.progressBar = false;
                            toastr.options.preventDuplicates = true;
                            toastr.options.onclick = null;
                            e.processOnServer = false;        
					}
                    else if (cboMaterialType.GetValue() == '' ){
                         toastr.warning('Please input Material Type', 'Warning');
                         toastr.options.closeButton = false;
                         toastr.options.debug = false;
                         toastr.options.newestOnTop = false;
                         toastr.options.progressBar = false;
                         toastr.options.preventDuplicates = true;
                         toastr.options.onclick = null;
                         e.processOnServer = false;        
                    }
                    else if (cboSupplier.GetValue() ==''){
                         toastr.warning('Please input Supplier', 'Warning');
                         toastr.options.closeButton = false;
                         toastr.options.debug = false;
                         toastr.options.newestOnTop = false;
                         toastr.options.progressBar = false;
                         toastr.options.preventDuplicates = true;
                         toastr.options.onclick = null;
                         e.processOnServer = false;        
                    }

                    else{
                        Grid.PerformCallback('gridload|' + cboMaterialType.GetValue() + '|'+  cboSupplier.GetValue());   
	                }
                }" />
                <Paddings Padding="2px" />
            </dx:ASPxButton>
        &nbsp;<dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                ClientInstanceName="btnDownload" Theme="Default">                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>
            &nbsp;
            <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="false"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnAdd" Theme="Default" >                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>&nbsp;
            <dx:ASPxButton ID="btnChart" runat="server" Text="Chart" UseSubmitBehavior="false"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnChart" Theme="Default" >                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>
        </td>
    </tr>   
    <tr style="height: 0px">
        <td style=" padding:0px 0px 0px 10px" class="style1" colspan="9">            
            &nbsp;</td>
    </tr>   
</table>
      <div style="padding:5px 5px 5px 5px">
          <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
              <ClientSideEvents EndCallback="GetMessage" />
          </dx:ASPxCallback>
          <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
          </dx:ASPxGridViewExporter>
          <dx:ASPxCallback ID="cbExcel" runat="server" ClientInstanceName="cbExcel">
              <ClientSideEvents EndCallback="GetMessage" />
          </dx:ASPxCallback>
      </div>
      </div>
<div style="padding: 5px 5px 5px 5px">
    <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
        EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Size="9pt" Font-Names="Segoe UI"
        KeyFieldName="Material_Code;Price_Type;Supplier_Code;Period">
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
            HorizontalScrollBarMode="Auto" />
        <ClientSideEvents EndCallback="GetMessage" CustomButtonClick="function(s, e) {
	                  if(e.buttonID == &#39;Edit&#39;){ 
                            var rowKey = Grid.GetRowKey(e.visibleIndex);
                            window.location.href= &#39;Material_PriceDetail.aspx?ID=&#39; + rowKey;
                      }
                      else if (e.buttonID == &#39;Delete&#39;){
                           if (confirm('Are you sure want to delete ?')) {
                               var rowKey = Grid.GetRowKey(e.visibleIndex);
                               Grid.PerformCallback('delete |' + rowKey);
                           } else {
                     
                           }
                     }
                  }"></ClientSideEvents>
        <Columns>
            <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" Width="120px">
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Detail">
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete">
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="Material_Code" Caption="Material Code" VisibleIndex="1" Width="120px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Material_Name" Caption="Material Name" VisibleIndex="2" Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MaterialTypeDesc" Caption="Material Type" VisibleIndex="3" Width="120px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Group_Item" Caption="Group Item" VisibleIndex="4" Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Price_Type" Caption="Price Type"  VisibleIndex="5" Visible="false"
                Settings-AutoFilterCondition="Contains">
                <PropertiesTextEdit MaxLength="50" Width="90px" ClientInstanceName="PriceTypeCode">
                    <%--<ClientSideEvents Validation="ItemCodeValidation" />--%>
                    <Style HorizontalAlign="Left"></Style>
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains"></Settings>
                <EditFormSettings VisibleIndex="1" />
                <EditFormSettings VisibleIndex="1"></EditFormSettings>
                <FilterCellStyle Paddings-PaddingRight="4px">
                    <Paddings PaddingRight="4px"></Paddings>
                </FilterCellStyle>
                <HeaderStyle Paddings-PaddingLeft="8px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="8px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="PriceTypeDesc" Caption="Price Type" VisibleIndex="6" Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier_Code" VisibleIndex="7" Visible="false"
                Settings-AutoFilterCondition="Contains">
                <PropertiesTextEdit MaxLength="50" Width="90px" ClientInstanceName="SupplierCode">
                    <%--<ClientSideEvents Validation="ItemCodeValidation" />--%>
                    <Style HorizontalAlign="Left"></Style>
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains"></Settings>
                <EditFormSettings VisibleIndex="2" />
                <EditFormSettings VisibleIndex="2"></EditFormSettings>
                <FilterCellStyle Paddings-PaddingRight="4px">
                    <Paddings PaddingRight="4px"></Paddings>
                </FilterCellStyle>
                <HeaderStyle Paddings-PaddingLeft="8px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="8px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SupplierName" Caption="Supplier Name" VisibleIndex="8" Width="200px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Period" Caption="Period" VisibleIndex="9" Width="100px">
                <Settings AutoFilterCondition="Contains"  />
                  <PropertiesTextEdit DisplayFormatString="MMM yyyy">
                    </PropertiesTextEdit>
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Price" VisibleIndex="7" FieldName="Price">
                  <PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="CurrencyDesc" Caption="Currency" VisibleIndex="10" Width="100px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="11" FieldName="Register_User">
                    <Settings AllowAutoFilter="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="12" FieldName="Register_Date">
                <Settings AllowAutoFilter="False" />
                <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="13" FieldName="Update_User">
                <Settings AllowAutoFilter="False" />
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="14" FieldName="Update_Date">
                <Settings AllowAutoFilter="False" />
                <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
        <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
        </SettingsPager>
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
            HorizontalScrollBarMode="Auto" />
        <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
            <Header>
                <Paddings Padding="2px"></Paddings>
            </Header>
            <EditFormColumnCaption>
                <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
            </EditFormColumnCaption>
        </Styles>
    </dx:ASPxGridView>
   
    </div>
</div>


</asp:Content>

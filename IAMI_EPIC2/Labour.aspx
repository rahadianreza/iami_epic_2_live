﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Labour.aspx.vb" Inherits="IAMI_EPIC2.Labour" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
//    function GetMessage(s, e) {
//        if (s.cp_message == "There is no data to show!") {
//            //alert(s.cpRowCount);
//            toastr.info(s.cpMessage, 'Information');
//            toastr.options.closeButton = false;
//            toastr.options.debug = false;
//            toastr.options.newestOnTop = false;
//            toastr.options.progressBar = false;
//            toastr.options.preventDuplicates = true;
//            toastr.options.onclick = null;
//            btnDownload.SetEnabled(false);

//        } else if (s.cp_message == "Download Excel Successfully!") {
//            toastr.info(s.cpMessage, 'Success');
//            toastr.options.closeButton = false;
//            toastr.options.debug = false;
//            toastr.options.newestOnTop = false;
//            toastr.options.progressBar = false;
//            toastr.options.preventDuplicates = true;
//            toastr.options.onclick = null;
//            btnDownload.SetEnabled(true);
//        } else if (s.cp_message == "Success") {
//            btnDownload.SetEnabled(true);
//        }

//    }
    
    //GET MESSAGE
    function GetMessage(s, e) {
        if(s.cp_disabled == "N") {
            //alert(s.cp_disabled);
            btnDownload.SetEnabled(true);
        } else if (s.cp_disabled == "Y") {
            btnDownload.SetEnabled(false);
           
            toastr.info("There's no data to show!", 'Info');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return;

            
        }
        if (s.cp_message != "" && s.cp_val == 1) {
            if (s.cp_type == "Success" && s.cp_val == 1) {
                toastr.success(s.cp_message, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                s.cp_val = 0;
                s.cp_message = "";
            }
            else if (s.cp_type == "Warning" && s.cp_val == 1) {
                toastr.warning(s.cp_message, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                ss.cp_val = 0;
                s.cp_message = "";
            }
            else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                toastr.error(s.cp_message, 'Error');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                s.cp_val = 0;
                s.cp_message = "";
            }
        }
        else if (s.cp_message == "" && s.cp_val == 0) {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        
    }

    function ItemCodeValidation(s, e) {
        if (ItemCode.GetValue() == null) {            
              e.isValid = false;           
        }
      }

   function downloadValidation(s, e) {
       if (Grid.GetVisibleRowsOnPage() == 0) {
           toastr.info('There is no data to download!', 'Info');
           e.processOnServer = false;
           return;
       }
   }

   function showDataValidation(s, e) {
       if (Grid.GetVisibleRowsOnPage() == 0) {
           toastr.info('There is no data to show!', 'Info');
           e.processOnServer = false;
           return;
       }
   }

   //filter combobox regioncity berdasarkan province yang dipilih
   function FilterRegion() {
       cboRegion.PerformCallback('filter|' + cboProvince.GetValue());
   }      
   
//    function GridHeader(s, e) {
//        Grid.PerformCallback('gridload|' + ASPxSpinEditPeriodFrom.GetNumber() + '|' + ASPxSpinEditPeriodTo.GetNumber());
//    }


</script>

    <style type="text/css">
        .style1
        {
            width: 82px;
        }
        .style5
        {
            height: 2px;
        }
    </style>
    <style type="text/css">
        .td-col-l
        {
            padding:0px 0px 0px 10px;
            width:80px;
            
        }
        .td-col-m
        {
            width:10px;
        }
        .td-col-r
        {
            width:100px;
        }
        .td-col-f
        {
            width:50px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">

<%--         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%"
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="ItemCode" DataSourceID="SqlDataSource1">--%>

<table style="width: 100%; border: 1px solid black">  
    <tr style="height:10px">
        <td class="td-col-r" colspan="9"></td>
   </tr>
    <tr style="height:35px">
        <td style="padding:0px 0px 0px 10px; width:80px">
            <dx1:ASPxLabel ID="lblPeriodFrom" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                           Text="Period From" ClientInstanceName="lblPeriodFrom">
            </dx1:ASPxLabel>
        </td>
        <td class="td-col-m"></td>
        <td class="td-col-r">
             <dx:ASPxSpinEdit ID="ASPxSpinEditPeriodFrom" ClientInstanceName="ASPxSpinEditPeriodFrom" runat="server"  NumberType="Integer" 
                              Width="100px" Height="25px" />
        </td>
        <td class="td-col-f"></td>

        <td style="width:80px">
            <dx1:ASPxLabel ID="lblPeriodTo" runat="server"  Font-Names="Segoe UI" Font-Size="9pt" 
                           Text="Period To" ClientInstanceName="lblPeriodTo">
            </dx1:ASPxLabel>
        </td>
        <td class="td-col-m"></td>
        <td class="td-col-r" >
            <dx:ASPxSpinEdit ID="ASPxSpinEditPeriodTo" ClientInstanceName="ASPxSpinEditPeriodTo" runat="server"  NumberType="Integer" 
              Width="100px" Height="25px"  />
        </td>
        <td></td>
        <td></td>
        
    </tr>
    <tr style="height:35px">
        <td style="padding:0px 0px 0px 10px; width:80px">
             <dx1:ASPxLabel ID="lblProvince" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Province" ClientInstanceName="lblProvince">
            </dx1:ASPxLabel>
        </td>
        <td class="td-col-m"></td>
        <td class="td-col-r">
            <dx:ASPxComboBox ID="cboProvince" runat="server" ClientInstanceName="cboProvince"
                Width="170px" Font-Names="Segoe UI" TextField="ProvinceDesc" ValueField="Province"
                DataSourceID="SqlDataSource1" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                Height="22px">
                <ClientSideEvents SelectedIndexChanged="FilterRegion" />
                <Columns>
                    <dx:ListBoxColumn Caption="Code" FieldName="Province" Width="100px" />
                    <dx:ListBoxColumn Caption="Description" FieldName="ProvinceDesc" Width="250px" />
                </Columns>
                <ItemStyle Height="10px" Paddings-Padding="4px">
                    <Paddings Padding="4px"></Paddings>
                </ItemStyle>
                <ButtonStyle Width="5px" Paddings-Padding="4px">
                    <Paddings Padding="4px"></Paddings>
                </ButtonStyle>
            </dx:ASPxComboBox>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="Select 'ALL' Province,'ALL' ProvinceDesc UNION ALL Select Rtrim(Par_Code) Province, Rtrim(Par_Description) ProvinceDesc  From Mst_Parameter Where Par_Group = 'Provinsi'">
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr class="tr-height">
        <td style="padding:0px 0px 0px 10px; width:80px">
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                Text="City / Region">
            </dx:ASPxLabel>
        </td>
        <td style="width: 20px">
            &nbsp;
        </td>
        <td class="td-col-r">
            <dx:ASPxComboBox ID="cboRegion" runat="server" ClientInstanceName="cboRegion" Width="170px"
                Font-Names="Segoe UI" TextField="RegionDesc" ValueField="Region" TextFormatString="{1}"
                Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                EnableIncrementalFiltering="True" Height="25px">
               <%-- <ClientSideEvents ValueChanged="function(s, e) {
	txtRegion.SetText(cboRegion.GetValue());
}" />--%>
                <ClientSideEvents Init="function(s, e) {
	cboRegion.PerformCallback('ALL|');
}" />
                <Columns>
                    <dx:ListBoxColumn Caption="Code" FieldName="Region" Width="100px" />
                    <dx:ListBoxColumn Caption="Description" FieldName="RegionDesc" Width="120px" />
                </Columns>
                <ItemStyle Height="10px" Paddings-Padding="4px">
                    <Paddings Padding="4px"></Paddings>
                </ItemStyle>
                <ButtonStyle Width="5px" Paddings-Padding="4px">
                    <Paddings Padding="4px"></Paddings>
                </ButtonStyle>
            </dx:ASPxComboBox>
        </td>
        <td>
            <dx:ASPxTextBox ID="txtRegion" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                Width="80px" ClientInstanceName="txtRegion" MaxLength="50" ClientVisible="False">
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr style="height:10px">
        <td class="td-col-r" colspan="9"></td>
    </tr>
    <tr style="height: 35px">
        <td style=" padding:0px 0px 0px 10px" colspan="9">            
            <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                ClientInstanceName="btnRefresh" Theme="Default">                        
                <ClientSideEvents Click="function(s, e) {
                    if ( ASPxSpinEditPeriodFrom.GetNumber() &gt; ASPxSpinEditPeriodTo.GetNumber()){
						    toastr.warning('Period To must greater than Period From', 'Warning');
                            toastr.options.closeButton = false;
                            toastr.options.debug = false;
                            toastr.options.newestOnTop = false;
                            toastr.options.progressBar = false;
                            toastr.options.preventDuplicates = true;
                            toastr.options.onclick = null;
                            e.processOnServer = false;        
					}
                    else{
                        Grid.PerformCallback('gridload|' + ASPxSpinEditPeriodFrom.GetNumber() + '|' + ASPxSpinEditPeriodTo.GetNumber() + '|' + cboProvince.GetValue() + '|' + cboRegion.GetValue() );   
	                }
                }" />
                <Paddings Padding="2px" />
            </dx:ASPxButton>
        &nbsp;<dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                ClientInstanceName="btnDownload" Theme="Default">                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>
            &nbsp;
            <dx:ASPxButton ID="btnAdd" runat="server" Text="Add Labour" UseSubmitBehavior="false"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnAdd" Theme="Default" >                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>&nbsp;
            <dx:ASPxButton ID="btnChart" runat="server" Text="Chart" UseSubmitBehavior="false"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnChart" Theme="Default" >                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>
        </td>
    </tr>   
    <tr style="height: 0px">
        <td style=" padding:0px 0px 0px 10px" class="style1" colspan="9">            
            &nbsp;</td>
    </tr>   
</table>
      <div style="padding:5px 5px 5px 5px">
          <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
              <ClientSideEvents EndCallback="GetMessage" />
          </dx:ASPxCallback>
          <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
          </dx:ASPxGridViewExporter>
          <dx:ASPxCallback ID="cbExcel" runat="server" ClientInstanceName="cbExcel">
              <ClientSideEvents EndCallback="GetMessage" />
          </dx:ASPxCallback>
      </div>
      </div>
<div style="padding: 5px 5px 5px 5px">
    <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
        EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Size="9pt" Font-Names="Segoe UI"
        KeyFieldName="Province;Region;Period">
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
            HorizontalScrollBarMode="Auto" />
        <ClientSideEvents EndCallback="GetMessage" CustomButtonClick="function(s, e) {
	                  if(e.buttonID == &#39;Edit&#39;){ 
                            var rowKey = Grid.GetRowKey(e.visibleIndex);
                            window.location.href= &#39;LabourDetail.aspx?ID=&#39; + rowKey;
                      }
                      else if (e.buttonID == &#39;Delete&#39;){
                           if (confirm('Are you sure want to delete ?')) {
                               var rowKey = Grid.GetRowKey(e.visibleIndex);
                               Grid.PerformCallback('delete |' + rowKey);
                           } else {
                     
                           }
                     }
                  }"></ClientSideEvents>
        <Columns>
            <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" Width="150px">
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit">
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete">
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="Province" FieldName="Province" VisibleIndex="1" Visible="false"
                Settings-AutoFilterCondition="Contains">
                <PropertiesTextEdit MaxLength="50" Width="90px" ClientInstanceName="ItemCode">
                    <%--<ClientSideEvents Validation="ItemCodeValidation" />--%>
                    <Style HorizontalAlign="Left"></Style>
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains"></Settings>
                <EditFormSettings VisibleIndex="1" />
                <EditFormSettings VisibleIndex="1"></EditFormSettings>
                <FilterCellStyle Paddings-PaddingRight="4px">
                    <Paddings PaddingRight="4px"></Paddings>
                </FilterCellStyle>
                <HeaderStyle Paddings-PaddingLeft="8px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="8px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Region" Caption="Region" VisibleIndex="2" Visible="false"
                Width="250px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ProvinceDesc" Caption="Province" VisibleIndex="3" Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="RegionDesc" Caption="City/Region" VisibleIndex="4" Width="200px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn FieldName="Period" Caption="Period" VisibleIndex="5" Width="100px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="UMR" VisibleIndex="6" FieldName="UMR">
                  <PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn FieldName="Sectoral" Caption="Sectoral" VisibleIndex="7"
                Width="250px">
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="UMSP" VisibleIndex="8" FieldName="UMSP">
                  <PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
            </dx:GridViewDataTextColumn>
              <dx:GridViewDataTextColumn Caption="Estimated UMSP" VisibleIndex="9" FieldName="EstimatedUMSP">
                  <PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="10" FieldName="RegisterUser">
                    <Settings AllowAutoFilter="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="11" FieldName="RegisterDate">
                <Settings AllowAutoFilter="False" />
                <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="12" FieldName="UpdateUser">
                <Settings AllowAutoFilter="False" />
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="13" FieldName="UpdateDate">
                <Settings AllowAutoFilter="False" />
                <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
        <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
        </SettingsPager>
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
            HorizontalScrollBarMode="Auto" />
        <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
            <Header>
                <Paddings Padding="2px"></Paddings>
            </Header>
            <EditFormColumnCaption>
                <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
            </EditFormColumnCaption>
        </Styles>
    </dx:ASPxGridView>
   
    </div>
</div>
<%--    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="SELECT ItemCode, ItemName FROM ItemMaster"></asp:SqlDataSource>--%>

  

</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="AddMaterialPart.aspx.vb" Inherits="IAMI_EPIC2.AddMaterialPart" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function MessageError(s, e) {
            if (s.cpMessage == "Data Saved Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                cboPeriod.SetText('');
                cboType.SetText('');
                cboMaterialCode.SetText('');
                txtMaterialName.SetText('');
                txtSpecification.SetText('');
                cboUoMUnitPrice.SetText('');
                txtUnitPrice.SetText('');
                cboUoMUnitConsump.SetText('');
                txtUnitConsump.SetText('');
                cboCategoryMaterial.SetText('');
                cboGroupItem.SetText('');
                cboCurrency.SetText('');
                cboMaterialCategory.SetText('');
            }
            else if (s.cpMessage == "Data Updated Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

            }
            else if (s.cpMessage == "Data Clear Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                cboPeriod.SetText('');
                cboType.SetText('');
                cboMaterialCode.SetText('');
                txtMaterialName.SetText('');
                txtSpecification.SetText('');
                cboUoMUnitPrice.SetText('');
                txtUnitPrice.SetText('');
                cboUoMUnitConsump.SetText('');
                txtUnitConsump.SetText('');
                cboCategoryMaterial.SetText('');
                cboGroupItem.SetText('');
                cboCurrency.SetText('');
                cboMaterialCategory.SetText('');
            }
            else if (s.cpMessage == "Data Cancel Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }

        }
        function GridLoad() {
            Grid.PerformCallback('refresh');
            tmpProjectType.SetText(cboProjectType.GetValue());
        };
        function fn_AllowonlyNumeric(s, e) {
            var theEvent = e.htmlEvent || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;

            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault)
                    theEvent.preventDefault();
            }
        };
        function MessageDelete(s, e) {

            if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                //window.location.href = "/ItemList.aspx";
                millisecondsToWait = 2000;
                setTimeout(function () {
                    var pathArray = window.location.pathname.split('/');
                    var url = window.location.origin + '/' + pathArray[1] + '/PRJList.aspx'
                    //window.location.href = url;
                    //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                    //alert(window.location.host);
                    if (pathArray[1] == "AddPRJList.aspx") {
                        window.location.href = window.location.origin + '/PRJList.aspx';
                    }
                    else {
                        window.location.href = url;
                    }
                }, millisecondsToWait);

            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function FilterGroup() {
            cboGroupItem.PerformCallback('filter|' + cboPRType.GetValue());
        }

        function FilterCategory() {
            cboCategory.PerformCallback('filter|' + cboGroupItem.GetValue());
        }
        function SetCode(s, e) {
            tmpProjectType.SetText(cboProjectType.GetValue());
        }

        document.getElementById('form').addEventListener('keypress', function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 70px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period">
                </dx:ASPxLabel>
            </td>
            <td style="width: 10px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <%--<dx:ASPxDateEdit ID="cboPeriod" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="cboPeriod" EditFormatString="MMM-yyyy" DisplayFormatString="MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <ClientSideEvents ValueChanged="GridLoad" />
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>--%>
                <dx:ASPxTimeEdit ID="cboPeriod" Width="100px" Theme="Office2010Black" Font-Names="Segoe UI"
                    Font-Size="9pt" EditFormat="Custom" ClientInstanceName="cboPeriod" runat="server"
                    DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy">
                    <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                        <Paddings Padding="3px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxTimeEdit>
            </td>
            <td colspan="5">
            </td>
            <td style="width:180px"></td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 70px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Type">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboType" runat="server" ClientInstanceName="cboType" Width="170px"
                    Font-Names="Segoe UI" TextField="Description" ValueField="Code" DataSourceID="SqlDataSource2"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="170px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                     <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialType'">
                </asp:SqlDataSource>
            </td>
            <td colspan="5">
            </td>
            <td style="width:180px"></td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 70px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Material Type">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboCategoryMaterial" runat="server" ClientInstanceName="cboCategoryMaterial"
                    Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    DataSourceID="SqlDataSource5" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                         <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                      cboMaterialCode.PerformCallback('filter|' + cboCategoryMaterial.GetSelectedItem().GetColumnText(0));
                      }"/>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialCategory'">
                </asp:SqlDataSource>
               
            </td>
            <td colspan="6">
            </td>
            <td style="width:180px"></td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 70px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Material No">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                 <dx:ASPxComboBox ID="cboMaterialCode" runat="server" ClientInstanceName="cboMaterialCode"
                    Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
      
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                     <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                      cbDescriptions.PerformCallback('filter|' + cboMaterialCode.GetSelectedItem().GetColumnText(0));
                      }"/>
                </dx:ASPxComboBox>
                 
               <%-- <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MeterialCode'">
                </asp:SqlDataSource>--%>
            </td>
           <td colspan="5">
            </td>
            <td style="width:180px"></td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 70px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Description">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtMaterialName" runat="server" Font-Names="Segoe UI" Font-Size="9pt" ClientEnabled  ="false" 
                    Width="170px" ClientInstanceName="txtMaterialName" MaxLength="100">
                </dx:ASPxTextBox>
            </td>
            <td colspan="5">
            </td>
            <td style="width:180px"></td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 70px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Specification">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtSpecification" runat="server" Font-Names="Segoe UI" Font-Size="9pt" AutoCompleteType="Disabled"
                    Width="170px" ClientInstanceName="txtSpecification" MaxLength="40">
                </dx:ASPxTextBox>
            </td>
            <td colspan="5">
            </td>
            <td style="width:180px"></td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 70px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="UoM Unit Price">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboUoMUnitPrice" runat="server" ClientInstanceName="cboUoMUnitPrice"
                    Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    DataSourceID="SqlDataSource1" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <Columns>
                       <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="170px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'UOM'">
                </asp:SqlDataSource>
            </td>
            <td style="width: 100px; padding-top: 10px; padding-left: 10px;">
                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Unit Price">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 200px; padding-top: 10px; ">
                <dx:ASPxTextBox ID="txtUnitPrice" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtUnitPrice" MaxLength="11" HorizontalAlign="Right">
                       <MaskSettings Mask="<0..100000000g>.<00..99>" />
                </dx:ASPxTextBox>
            </td>
            <td colspan="2" style="width:180px"></td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 70px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="UoM Unit Consump">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboUoMUnitConsump" runat="server" ClientInstanceName="cboUoMUnitConsump"
                    Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    DataSourceID="SqlDataSource3" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="170px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'UOM'">
                </asp:SqlDataSource>
            </td>
            <td style="width: 120px; padding-top: 10px; padding-left: 10px;">
                <dx:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Unit Consump">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td colspan="3" style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtUnitConsump" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtUnitConsump" MaxLength="11" HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>.<00..99>" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 70px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Currency">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboCurrency" runat="server" ClientInstanceName="cboCurrency"
                    Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    DataSourceID="SqlDataSource7" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="170px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <Paddings Padding="4px"></Paddings>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Currency'">
                </asp:SqlDataSource>
            </td>
            <td colspan="7">
            </td>
           
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 70px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Group Item">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboGroupItem" runat="server" ClientInstanceName="cboGroupItem"
                    Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    DataSourceID="SqlDataSource4" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialGroupItem'">
                </asp:SqlDataSource>
            </td>
            <td colspan="7">
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 70px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel12" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Category">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboMaterialCategory" runat="server" ClientInstanceName="cboMaterialCategory"
                    Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    DataSourceID="SqlDataSource6" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialCategory'">
                </asp:SqlDataSource>
            </td>
            <td colspan="7">
            </td>
            
        </tr>
        <tr style="display:none">
            <td>
               &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <dx:ASPxTextBox ID="txtMaterialNameTemp" runat="server" Font-Names="Segoe UI" Font-Size="9pt"  
                    Width="170px" ClientInstanceName="txtMaterialNameTemp" MaxLength="100">
                </dx:ASPxTextBox>
            </td>
            <td colspan="7"></td>
           
        </tr>
        <tr style="height:10px">
            <td colspan="10">&nbsp;</td> 
        </tr>
       
        <tr style="height: 25px;">
            <td style="width: 50px;">
                &nbsp;
            </td>
            <td style="width: 10px">
               
            </td>
            <td style="width: 500px" colspan="7">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack"
                    Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI" UseSubmitBehavior="false"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">

                    
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnUpdate" runat="server" Text="Update" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnUpdate" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
           
            <td style="width: 20px">
                &nbsp;
            </td>
        </tr>
    </table>
    <div>
          <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
                    <ClientSideEvents CallbackComplete="SetCode" />
                </dx:ASPxCallback>
                <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                    <ClientSideEvents Init="MessageError" />
                </dx:ASPxCallback>
                <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
                    <ClientSideEvents Init="MessageError" />
                </dx:ASPxCallback>
                <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
                    <ClientSideEvents CallbackComplete="MessageDelete" />
                </dx:ASPxCallback>
    </div>
       <dx:ASPxCallback ID="cbDescriptions" ClientInstanceName="cbDescriptions" runat="server">
         <ClientSideEvents EndCallback="function(s,e) {
                txtMaterialNameTemp.SetText(s.cp_Descs);
                txtMaterialName.SetText(s.cp_Descs);
          }
          " />
    </dx:ASPxCallback>
   
</asp:Content>

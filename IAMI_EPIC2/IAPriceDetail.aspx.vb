﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors

Public Class IAPriceDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    ' Dim ls_VIANumber As String
    Dim vIANumber As String
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
    Dim pCENumber As String
    Dim ls_IANumber As String
#End Region

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cpMessage") = ErrMsg
        Grid.JSProperties("cpType") = msgType
        Grid.JSProperties("cpVal") = pVal
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|G010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                          "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboCENumber)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("G010")
        Master.SiteTitle = "IA PRICE DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "G010")
       

        Dim dtfrom As String = ""
        Dim dtto As String = ""
        Dim IANumber As String = ""
        Dim Revision As Integer
        Dim CENumber As String = ""

        Dim Data As String = ""

        gs_Message = ""
        If Request.QueryString("Date") <> "" Then
            dtfrom = Split(Request.QueryString("Date"), "|")(0)
            dtto = Split(Request.QueryString("Date"), "|")(1)
        End If
        If Request.QueryString("ID") <> "" Then
            IANumber = Split(Request.QueryString("ID"), "|")(0)
            Revision = Split(Request.QueryString("ID"), "|")(1)
            CENumber = Split(Request.QueryString("ID"), "|")(2)

            Session("IANumber") = Split(Request.QueryString("ID"), "|")(0)
            Session("Revision") = Split(Request.QueryString("ID"), "|")(1)
            Session("CENumber") = Split(Request.QueryString("ID"), "|")(2)

        End If

        If Not Page.IsPostBack Then

            If IANumber = "" Then
                Session("IANumber") = ""
                Session("IARev") = "0"
                gs_IANumber = ""
                txtIAPriceNo.Text = "--NEW--"
                gs_IANo = "--NEW--"
                up_FillComboCENumber(dtfrom, dtto)
                cbDraf.JSProperties("cpStatus") = ""
                gs_Status = ""
                dtIADate.Value = Now
                dtIAValidDate.Value = Now
                txtFlag.Text = 0
            Else
                gs_IANo = IANumber
                gs_IANumber = IANumber
                txtIAPriceNo.Text = IANumber
                up_GridLoadIANumber(IANumber, Revision)
                cbDraf.JSProperties("cpView") = ""
                Session("IANumber") = CENumber
                Session("IARev") = Revision
            End If
            'up_GridLoad(CENumber)
        End If

        cbDraf.JSProperties("cpMessage") = ""
        cbDraf.JSProperties("cpIA") = IANumber


      
    End Sub

    Private Sub up_GridLoad(pCENo As String)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet

        ds = GetDataSource(CmdType.StoreProcedure, "sp_IA_Detail_List", "CENumber|Rev", cboCENumber.Text & "|")

        If ErrMsg = "" Then

            Grid.DataSource = ds
            Grid.DataBind()


        End If
    End Sub

    Private Sub up_GridLoadIANumber(IANumber As String, Revision As Integer)
        Dim ds As New DataSet
        Dim ds_Delivery As New DataSet
        Dim ds_Quality As New DataSet
        Dim ErrMsg As String = ""
        Dim a As Integer

        ds = clsIAPriceDB.GetIAData(IANumber, Revision, ErrMsg)

        If ds.Tables(0).Rows.Count > 0 Then
            dtIADate.Value = ds.Tables(0).Rows(0)("IA_Date")
            dtIAValidDate.Value = ds.Tables(0).Rows(0)("Valid_Date")
            cboCENumber.Text = ds.Tables(0).Rows(0)("CE_Number")
            txtDescs.Text = ds.Tables(0).Rows(0)("Description")
            memoNote.Text = ds.Tables(0).Rows(0)("Note")
            txtRev.Text = Revision
            txtIAPriceNo.Text = IANumber
            txtParameter.Text = IANumber & "|" & Revision & "|" & ds.Tables(0).Rows(0)("CE_Number")

            If ds.Tables(0).Rows(0)("IAStatus") = "1" Then
                cbDraf.JSProperties("cpStatus") = "1"
                gs_IAStatus = "1"
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                          "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
                Dim script2 As String = ""
                script = "dtIAValidDate.SetEnabled(false);" & vbCrLf & _
                          "txtDescs.SetEnabled(false);" & vbCrLf & _
                          "memoNote.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(dtIAValidDate, dtIAValidDate.GetType(), "dtIAValidDate", script2, True)


            ElseIf ds.Tables(0).Rows(0)("IAStatus") = "2" Then
                cbDraf.JSProperties("cpStatus") = "2"
                gs_IAStatus = "2"
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                          "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
                Dim script2 As String = ""
                script = "dtIAValidDate.SetEnabled(false);" & vbCrLf & _
                          "txtDescs.SetEnabled(false);" & vbCrLf & _
                          "memoNote.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(dtIAValidDate, dtIAValidDate.GetType(), "dtIAValidDate", script2, True)

            ElseIf ds.Tables(0).Rows(0)("IAStatus") = "3" Then
                cbDraf.JSProperties("cpStatus") = "3"
                gs_IAStatus = "3"
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                          "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
                Dim script2 As String = ""
                script = "dtIAValidDate.SetEnabled(false);" & vbCrLf & _
                          "txtDescs.SetEnabled(false);" & vbCrLf & _
                          "memoNote.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(dtIAValidDate, dtIAValidDate.GetType(), "dtIAValidDate", script2, True)

            ElseIf ds.Tables(0).Rows(0)("IAStatus") = "4" Then
                cbDraf.JSProperties("cpStatus") = "4"
                gs_IAStatus = "4"
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                          "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
                Dim script2 As String = ""
                script = "dtIAValidDate.SetEnabled(false);" & vbCrLf & _
                          "txtDescs.SetEnabled(false);" & vbCrLf & _
                          "memoNote.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(dtIAValidDate, dtIAValidDate.GetType(), "dtIAValidDate", script2, True)

            Else
                cbDraf.JSProperties("cpStatus") = "0"
                btnSubmit.Enabled = True
                btnDraft.Enabled = True
                gs_IAStatus = "0"


            End If

            cbDraf.JSProperties("cpStatus") = ds.Tables(0).Rows(0)("IAStatus")
            'If cbDraf.JSProperties("cpStatus") = "1" Or cbDraf.JSProperties("cpStatus") = "2" Then
            '    Grid.Enabled = False
            'Else
            '    Grid.Enabled = True
            'End If
            gs_IARevNo = ds.Tables(0).Rows(0)("Revision_No")


            dtIADate.Enabled = False
            cboCENumber.Enabled = False

            txtFlag.Text = 1
        End If
        'Dim ds1 As New DataSet
        ds = GetDataSource(CmdType.StoreProcedure, "sp_IA_Detail_ListIA", "CENumber|IANumber|Rev", cboCENumber.Text & "|" & IANumber & "|" & Revision)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()


        End If

    End Sub
  
    Private Sub up_FillComboCENumber(pDate1 As Date, pDate2 As Date)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim dtfrom As String = Format(pDate1, "yyyy-MM-dd")
        Dim dtto As String = Format(pDate2, "yyyy-MM-dd")

        ds = clsIAPriceDB.GetComboCEData(dtfrom, dtto, pUser, pErr)
        If pErr = "" Then

            cboCENumber.DataSource = ds
            cboCENumber.DataBind()
        End If
    End Sub

    Private Function GetAllSupplierForGridHeaderCaption()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "SELECT Sup = dbo.MergeSupplierCE('" & cboCENumber.Text & "')", "", "", ErrMsg)

        If ErrMsg = "" Then
            retVal = ds.Tables(0).Rows(0).Item("Sup").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboCENumber)
        End If

        Return retVal
    End Function

    Private Sub up_SaveHeader(Optional ByRef pErr As String = "")
        Dim Errmsg As String = ""
        Dim ls_IANumber As String = ""
        Dim IACls As New clsIAPrice
        Dim ds As New DataSet
        'Dim ls_Bln As String
        'Dim li_Bln As Integer = Format(Now, "MM")
        'Dim li_Year As Integer = Format(Now, "yyyy")

        'If txtIAPriceNo.Text = "--NEW--" Then
        '    'ds = clsIAPriceDB.GetMaxIANo(li_Bln, li_Year, Errmsg)
        '    'If ds.Tables(0).Rows.Count > 0 Then
        '    '    ls_Bln = uf_ConvertMonth(Format(Now, "MM"))

        '    '    ls_IANumber = "IAMI/COST/IA/" & ls_Bln & "/" & Format(Now, "yy") & "/" & ds.Tables(0).Rows(0)("IA_Number")

        '    'End If

        '    IACls.Revision = "0"

        'End If
        'Dim i As Integer

        ' If ls_IANumber <> "" Then
        'IACls.IAPriceNo = ls_IANumber
        IACls.IAPriceDate = Format(dtIADate.Value, "yyyy-MM-dd")
        IACls.IAStatus = "0"
        IACls.Description = txtDescs.Text
        IACls.Note = memoNote.Text
        IACls.CENumber = cboCENumber.Text
        IACls.IAValidDate = Format(dtIAValidDate.Value, "yyyy-MM-dd")

        Dim IANumberOutput As String = ""
        If txtIAPriceNo.Text = "--NEW--" Then
            IACls.Revision = "0"
            clsIAPriceDB.InsertHeader(IACls, pUser, IANumberOutput, pErr)
            gs_IANo = IANumberOutput
            gs_IANumber = IANumberOutput
            IACls.CENumber = IANumberOutput
            ls_IANumber = gs_IANumber
            vIANumber = gs_IANumber
        Else
            IACls.IAPriceNo = txtIAPriceNo.Text
            IACls.Revision = txtRev.Text
            clsIAPriceDB.UpdateHeader(IACls, pUser, pErr)
        End If

        If pErr = "" Then
            txtIAPriceNo.Text = ls_IANumber
        End If
        ' End If

    End Sub

    Private Sub up_UpdateHeader(pIANo As String, pIAStatus As String, Optional ByRef pErr As String = "")
        Dim Errmsg As String = ""
        Dim IACls As New clsIAPrice

        IACls.IAPriceNo = pIANo
        IACls.Revision = gs_IARevNo
        IACls.CENumber = cboCENumber.Text
        IACls.IAStatus = pIAStatus
        IACls.IAPriceDate = Format(dtIADate.Value, "yyyy-MM-dd")
        IACls.Description = txtDescs.Text
        IACls.Note = memoNote.Text
        IACls.IAValidDate = Format(dtIAValidDate.Value, "yyyy-MM-dd")
       
        'ls_VIANumber = pIANo

        'clsCostEstimationDB.UpdateHeader(CECls, pUser, pErr)
        Dim i As Integer

        i = clsIAPriceDB.UpdateHeader(IACls, pUser, pErr)

        'If i = 0 Then
        'clsCostEstimationDB.InsertHeader(CECls, pUser, pErr)
        'End If

    End Sub

    Private Sub up_SaveDetail(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs, Optional ByRef pErr As String = "")
        Dim a As Integer
        Dim ls_MaterialNo As String
        Dim ls_Qty As String
        Dim ls_Selected_Supplier As String
        Dim ls_Reference_Supplier As String
        Dim ls_IANumber As String
        Dim IACls As New clsIAPrice

        a = e.UpdateValues.Count

        If vIANumber <> "" Then
            ls_IANumber = vIANumber
            IACls.Revision = "0"
            Dim i As Integer
            Dim aa As Integer
            For iLoop = 0 To a - 1
                ls_MaterialNo = Trim(e.UpdateValues(iLoop).NewValues("Material_No").ToString())
                ls_Qty = Trim(e.UpdateValues(iLoop).NewValues("Qty").ToString())
                ls_Selected_Supplier = e.UpdateValues(iLoop).NewValues("Selected_Supplier").ToString()
                ls_Reference_Supplier = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Reference_Supplier")), "", e.UpdateValues(iLoop).NewValues("Reference_Supplier"))

                IACls.IAPriceNo = ls_IANumber

                If txtRev.Text = "" Then
                    IACls.Revision = 0
                Else
                    IACls.Revision = txtRev.Text
                End If

                IACls.MaterialNo = ls_MaterialNo
                IACls.Qty = ls_Qty
                IACls.SelectedSupplier = ls_Selected_Supplier
                IACls.ReferenceSupplier = ls_Reference_Supplier


                i = clsIAPriceDB.UpdateDetail(IACls, pUser, pErr)

                If i = 0 Then
                    clsIAPriceDB.InsertDetail(IACls, pUser, pErr)
                End If
            Next
        End If
    End Sub

    Private Sub up_Submit()
        Dim Errmsg As String = ""
        Dim IACls As New clsIAPrice
        IACls.CENumber = cboCENumber.Text
        IACls.Revision = Session("IARev")
        IACls.IAPriceNo = Session("IANumber")

        clsIAPriceDB.SubmitData(IACls, pUser, Errmsg)
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        If e.UpdateValues.Count = 0 Then
            gs_Message = "Data is not found"
            Exit Sub
        End If
        Dim errmsg As String = ""
        Dim ls_SQL As String = ""
        Dim IACls As New clsIAPrice
        IACls.IAPriceDate = Format(dtIADate.Value, "yyyy-MM-dd")
        IACls.IAStatus = "0"
        IACls.Description = txtDescs.Text
        IACls.Note = memoNote.Text
        IACls.CENumber = cboCENumber.Text
        IACls.IAValidDate = Format(dtIAValidDate.Value, "yyyy-MM-dd")
        If txtRev.Text = "" Then
            IACls.Revision = 0
        Else
            IACls.Revision = txtRev.Text
        End If

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                If txtIAPriceNo.Text = "--NEW--" Then
                    ls_SQL = "sp_IA_Insert_Header"  'insert header 
                Else
                    ls_SQL = "sp_IA_Update_Header"
                End If

                Dim cmd As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("Rev", IACls.Revision)
                cmd.Parameters.AddWithValue("CENumber", IACls.CENumber)
                cmd.Parameters.AddWithValue("IADate", IACls.IAPriceDate)
                cmd.Parameters.AddWithValue("IAStatus", IACls.IAStatus)
                cmd.Parameters.AddWithValue("Descs", IACls.Description)
                cmd.Parameters.AddWithValue("Notes", IACls.Note)
                cmd.Parameters.AddWithValue("RegUser", pUser)
                cmd.Parameters.AddWithValue("IAValidDate", IACls.IAValidDate)

                Dim outPutParameter As SqlParameter = New SqlParameter()
                If txtIAPriceNo.Text = "--NEW--" Then
                    cmd.Parameters.AddWithValue("IA_Number", "")
                    outPutParameter.ParameterName = "IA_Number_Output"
                    outPutParameter.SqlDbType = System.Data.SqlDbType.VarChar
                    outPutParameter.Direction = System.Data.ParameterDirection.Output
                    outPutParameter.Size = 50
                    cmd.Parameters.Add(outPutParameter)
                Else
                    cmd.Parameters.AddWithValue("IA_Number", txtIAPriceNo.Text) 'for update
                End If
                cmd.ExecuteNonQuery()
                If txtIAPriceNo.Text = "--NEW--" Then
                    ls_IANumber = outPutParameter.Value.ToString()
                    gs_IANumber = outPutParameter.Value.ToString()
                    Session("IANumber") = outPutParameter.Value.ToString()
                    Session("IARev") = "0"
                    txtIAPriceNo.Text = ls_IANumber
                Else
                    ls_IANumber = txtIAPriceNo.Text
                End If

                cmd.Dispose()

                '#DETAIL 
                If ls_IANumber <> "" Or ls_IANumber <> "--NEW--" Then
                    For idx = 0 To e.UpdateValues.Count - 1
                        Dim ls_SQLSP As String
                        ls_SQLSP = "sp_IA_Detail_InsertUpdate"

                        IACls.MaterialNo = Trim(e.UpdateValues(idx).NewValues("Material_No").ToString()) ' ls_MaterialNo
                        IACls.IAPriceNo = ls_IANumber
                        IACls.Qty = Trim(e.UpdateValues(idx).NewValues("Qty").ToString())
                        IACls.SelectedSupplier = e.UpdateValues(idx).NewValues("Selected_Supplier").ToString()
                        IACls.ReferenceSupplier = IIf(String.IsNullOrEmpty(e.UpdateValues(idx).NewValues("Reference_Supplier")), "", e.UpdateValues(idx).NewValues("Reference_Supplier"))

                       

                        Dim sqlComm = New SqlCommand(ls_SQLSP, sqlConn, sqlTran)
                        sqlComm.CommandType = CommandType.StoredProcedure
                        sqlComm.Parameters.AddWithValue("IA_Number", IACls.IAPriceNo)
                        sqlComm.Parameters.AddWithValue("Rev", IACls.Revision)
                        sqlComm.Parameters.AddWithValue("Material_No", IACls.MaterialNo)
                        sqlComm.Parameters.AddWithValue("Qty", IACls.Qty)
                        sqlComm.Parameters.AddWithValue("Selected_Supplier", IACls.SelectedSupplier)
                        sqlComm.Parameters.AddWithValue("Reference_Supplier", IACls.ReferenceSupplier)
                        sqlComm.Parameters.AddWithValue("RegUser", pUser)

                        sqlComm.ExecuteNonQuery()
                        sqlComm.Dispose()
                    Next idx
                    sqlTran.Commit()
                End If
            End Using
        End Using


        'If gs_IANumber = "" Then
        '    up_SaveHeader(errmsg)
        'Else
        '    vIANumber = gs_IANumber
        'End If

        'If errmsg = "" Then
        '    up_SaveDetail(sender, e, errmsg)
        'End If

        'If errmsg = "" Then
        '    gs_Message = ""
        'Else
        '    gs_Message = errmsg
        'End If
        'Grid.EndUpdate()



    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback


        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Dim pCENo As String
        If Split(e.Parameters, "|")(1) <> "" Then
            pCENo = Split(e.Parameters, "|")(3)
        End If

        Dim pRev As Integer

        'If txtRev.Text = "" Then pRev = 0
        If Split(e.Parameters, "|")(1) = "" Then
            pRev = 0
        Else
            pRev = Split(e.Parameters, "|")(2)
        End If


        If pfunction = "gridload" Then

            Dim headerCaption As String = ""
            Grid.JSProperties("cpType") = ""
            Grid.JSProperties("cpMessage") = ""

            Try
                up_GridLoad(pCENo)

                'SET HEADER CAPTION (SUPPLIER)
                '#Initialize
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("Supplier1").Visible = True
                Grid.Columns("Supplier2").Visible = True
                Grid.Columns("Supplier3").Visible = True
                Grid.Columns("Supplier4").Visible = True
                Grid.Columns("Supplier5").Visible = True

                '#Set by condition
                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid.Columns("Supplier1").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid.Columns("Supplier2").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid.Columns("Supplier3").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid.Columns("Supplier4").Visible = False
                End Try

                Try
                    headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                    If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True
                Catch ex As Exception
                    Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid.Columns("Supplier5").Visible = False
                End Try
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
            End Try


        ElseIf pfunction = "draft" Then
            up_GridLoadIANumber(gs_IANumber, pRev)
        ElseIf pfunction = "save" Then
            Dim a As Integer
            Dim ls_MaterialNo As String
            Dim ls_Qty As String
            Dim ls_Selected_Supplier As String
            Dim ls_Reference_Supplier As String
            Dim ls_IANumber As String
            Dim IACls As New clsIAPrice

            'save header
            Dim errmsg As String = ""
            'If Session("IANumber") = "" Or IsNothing(Session("IANumber")) Then
            '    up_SaveHeader(errmsg)
            'Else
            '    vIANumber = Session("IANumber")
            'End If


            'Dim errmsg As String = ""
            Dim ls_SQL As String = ""
            'Dim IACls As New clsIAPrice
            IACls.Revision = "0"
            IACls.IAPriceDate = Format(dtIADate.Value, "yyyy-MM-dd")
            IACls.IAStatus = "0"
            IACls.Description = txtDescs.Text
            IACls.Note = memoNote.Text
            IACls.CENumber = cboCENumber.Text
            IACls.IAValidDate = Format(dtIAValidDate.Value, "yyyy-MM-dd")
            If txtRev.Text = "" Then
                IACls.Revision = 0
            Else
                IACls.Revision = txtRev.Text
            End If


            Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                sqlConn.Open()

                Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                    If Session("IANumber") = "" Or Session("IANumber") = Nothing Then
                        ls_SQL = "sp_IA_Insert_Header"  'insert header 
                    Else
                        ls_SQL = "sp_IA_Update_Header"
                    End If

                    Dim cmd As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("Rev", IACls.Revision)
                    cmd.Parameters.AddWithValue("CENumber", IACls.CENumber)
                    cmd.Parameters.AddWithValue("IADate", IACls.IAPriceDate)
                    cmd.Parameters.AddWithValue("IAStatus", IACls.IAStatus)
                    cmd.Parameters.AddWithValue("Descs", IACls.Description)
                    cmd.Parameters.AddWithValue("Notes", IACls.Note)
                    cmd.Parameters.AddWithValue("RegUser", pUser)
                    cmd.Parameters.AddWithValue("IAValidDate", IACls.IAValidDate)

                    Dim outPutParameter As SqlParameter = New SqlParameter()
                    If Session("IANumber") = "" Or Session("IANumber") = Nothing Then
                        cmd.Parameters.AddWithValue("IA_Number", "")
                        outPutParameter.ParameterName = "IA_Number_Output"
                        outPutParameter.SqlDbType = System.Data.SqlDbType.VarChar
                        outPutParameter.Direction = System.Data.ParameterDirection.Output
                        outPutParameter.Size = 50
                        cmd.Parameters.Add(outPutParameter)
                    Else
                        cmd.Parameters.AddWithValue("IA_Number", txtIAPriceNo.Text) 'for update
                    End If
                    cmd.ExecuteNonQuery()
                    If Session("IANumber") = "" Or Session("IANumber") = Nothing Then
                        ls_IANumber = outPutParameter.Value.ToString()
                        gs_IANumber = outPutParameter.Value.ToString()
                        Session("IANumber") = outPutParameter.Value.ToString()
                        Session("IARev") = "0"
                        txtIAPriceNo.Text = ls_IANumber
                    Else
                        ls_IANumber = txtIAPriceNo.Text
                    End If

                    cmd.Dispose()


                    If Session("IANumber") <> "" And errmsg = "" Then
                        ls_IANumber = Session("IANumber")
                        IACls.Revision = "0"
                        Dim i As Integer
                        Dim aa As Integer
                        Dim ds As New DataSet

                        IACls.IAPriceNo = Session("IANumber")

                        Dim Err As String = ""
                        up_UpdateHeader(Session("IANumber"), "0", Err)


                        ds = GetDataSource(CmdType.StoreProcedure, "sp_IA_Detail_ListIA", "CENumber|IANumber|Rev", cboCENumber.Text & "|" & Session("IANumber") & "|" & IACls.Revision)
                        For iLoop = 0 To ds.Tables(0).Rows.Count - 1
                            'ls_MaterialNo = Trim(ds.Tables(0).Rows(iLoop).Item("Material_No"))
                            'ls_Qty = ds.Tables(0).Rows(iLoop).Item("Qty")
                            'ls_Selected_Supplier = Trim(ds.Tables(0).Rows(iLoop).Item("Selected_Supplier"))
                            'ls_Reference_Supplier = Trim(ds.Tables(0).Rows(iLoop).Item("Reference_Supplier"))

                            'IACls.MaterialNo = ls_MaterialNo
                            'IACls.Qty = ls_Qty
                            'IACls.SelectedSupplier = ls_Selected_Supplier
                            'IACls.ReferenceSupplier = ls_Reference_Supplier

                            'Dim pErr As String = ""
                            'i = clsIAPriceDB.UpdateDetail(IACls, pUser, pErr)

                            'If i = 0 Then
                            '    clsIAPriceDB.InsertDetail(IACls, pUser, pErr)
                            'End If


                            Dim ls_SQLSP As String
                            ls_SQLSP = "sp_IA_Detail_InsertUpdate"

                            IACls.MaterialNo = Trim(ds.Tables(0).Rows(iLoop).Item("Material_No")) ' ls_MaterialNo
                            IACls.IAPriceNo = Session("IANumber")
                            IACls.Qty = ds.Tables(0).Rows(iLoop).Item("Qty")
                            IACls.SelectedSupplier = Trim(ds.Tables(0).Rows(iLoop).Item("Selected_Supplier"))
                            IACls.ReferenceSupplier = Trim(ds.Tables(0).Rows(iLoop).Item("Reference_Supplier"))
                            IACls.Revision = Session("IARev")

                            Dim sqlComm = New SqlCommand(ls_SQLSP, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.StoredProcedure
                            sqlComm.Parameters.AddWithValue("IA_Number", IACls.IAPriceNo)
                            sqlComm.Parameters.AddWithValue("Rev", IACls.Revision)
                            sqlComm.Parameters.AddWithValue("Material_No", IACls.MaterialNo)
                            sqlComm.Parameters.AddWithValue("Qty", IACls.Qty)
                            sqlComm.Parameters.AddWithValue("Selected_Supplier", IACls.SelectedSupplier)
                            sqlComm.Parameters.AddWithValue("Reference_Supplier", IACls.ReferenceSupplier)
                            sqlComm.Parameters.AddWithValue("RegUser", pUser)

                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()

                        Next
                        sqlTran.Commit()
                    End If
                End Using
            End Using



        ElseIf pfunction = "view" Then
                Try
                    Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                    Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                    Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                    Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                    Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                    Grid.Columns("Supplier1").Visible = True
                    Grid.Columns("Supplier2").Visible = True
                    Grid.Columns("Supplier3").Visible = True
                    Grid.Columns("Supplier4").Visible = True
                    Grid.Columns("Supplier5").Visible = True

                    Dim headerCaption As String = ""
                    Try
                        headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                        If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True
                    Catch ex As Exception
                        Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                        Grid.Columns("Supplier1").Visible = False
                    End Try

                    Try
                        headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                        If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True
                    Catch ex As Exception
                        Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                        Grid.Columns("Supplier2").Visible = False
                    End Try

                    Try
                        headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                        If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True
                    Catch ex As Exception
                        Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                        Grid.Columns("Supplier3").Visible = False
                    End Try

                    Try
                        headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                        If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True
                    Catch ex As Exception
                        Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                        Grid.Columns("Supplier4").Visible = False
                    End Try

                    Try
                        headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                        If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True
                    Catch ex As Exception
                        Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                        Grid.Columns("Supplier5").Visible = False
                    End Try





                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
                End Try
        End If


    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "Selected_Supplier" Or e.DataColumn.FieldName = "Reference_Supplier" Then
            'Editable
            Dim ds As New DataSet
            Dim ErrMsg As String = ""

            ds = clsIAPriceDB.DisabledForm(txtIAPriceNo.Text, txtRev.Text, ErrMsg)
            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    e.Cell.BackColor = Color.LemonChiffon
                Else
                    e.Cell.BackColor = Color.White
                End If

            End If

        Else
            'Non-Editable
            e.Cell.BackColor = Color.LemonChiffon
            e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        End If
    End Sub

    Private Sub cbGrid_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbGrid.Callback
        Dim pfunction As String = Split(e.Parameter, "|")(0)
        Dim pIANo As String = Split(e.Parameter, "|")(3)
        Dim pRev As Integer

        If Split(e.Parameter, "|")(2) = "" Then pRev = 0

        up_GridLoadIANumber(gs_IANumber, pRev)

    End Sub

    Private Sub cbDraf_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDraf.Callback
        'up_UploadFile()
        Session("CENumber") = cboCENumber.Text
        If gs_Message = "" Then

            cbDraf.JSProperties("cpMessage") = "Draft data saved successfull"
            ' If gs_IANumber = "" Then
            'cbDraf.JSProperties("cpIANo") = vIANumber
            'Else
            vIANumber = gs_IANumber
            cbDraf.JSProperties("cpIANo") = Session("IANumber") 'vIANumber
            cbDraf.JSProperties("cpRevision") = Session("IARev")
            'End If

            'If IsNothing(gs_IARevNo) Then
            '    cbDraf.JSProperties("cpRevision") = 0
            'Else
            '    cbDraf.JSProperties("cpRevision") = Session("IANumber")
            'End If

            If gs_IAStatus = "" Then
                cbDraf.JSProperties("cpStatus") = 0
            Else
                cbDraf.JSProperties("cpStatus") = gs_IAStatus
            End If
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim errmsg As String = ""

        up_UpdateHeader(gs_IANumber, "1", errmsg)

        If errmsg = "" Then
            cbDraf.JSProperties("cpMessage") = "Data saved successfull"
        End If

    End Sub

    Private Sub up_Print()
        'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/ViewPRAcceptance.aspx")
        Session("IANumber") = txtIAPriceNo.Text
        Session("Revision") = txtRev.Text
        Session("CENumber") = IIf(Session("CENumber") = Nothing, cboCENumber.Text, Session("CENumber"))
        Session("Action") = "1"
        Response.Redirect("ViewIAPrice.aspx")

    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        up_Print()
    End Sub

    Private Sub cbSubmit_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSubmit.Callback
        up_Submit()
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/IAPrice.aspx")
    End Sub

    Protected Sub Grid_CustomJSProperties(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewClientJSPropertiesEventArgs) Handles Grid.CustomJSProperties
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        ds = clsIAPriceDB.DisabledForm(txtIAPriceNo.Text, txtRev.Text, ErrMsg)
        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cpExist") = "1"
            Else
                Grid.JSProperties("cpExist") = "0"
            End If
        End If
    End Sub

#Region "Row grid updating"

    'Private Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles Grid.RowDeleting
    '    e.Cancel = True
    'End Sub

    Private Sub Grid_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles Grid.RowInserting
        e.Cancel = True
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
    End Sub
#End Region

End Class
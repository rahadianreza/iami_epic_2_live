﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="EngineeringPartList.aspx.vb" Inherits="IAMI_EPIC2.EngineeringPartList" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

         function Validation() {
            if (cboProject.GetText() == '') {
                toastr.warning('Please Select Project Name');                
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
           <tr style="height: 20px">
                <td>
                </td>
                <td style="width:10px">&nbsp;</td>
                <td></td>
                <td style="width:10px">&nbsp;</td>
                <td style="width:10px">&nbsp;</td>
                <td align="left">
                </td>
                <td style="width:10px">&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td style="width:600px">&nbsp;</td>
            </tr>     
           <tr style="height: 10px">
                <td style=" padding:0px 0px 0px 10px" class="style1">            
                    <dx:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Project Name">
                    </dx:aspxlabel>
                </td>
                <td style=" width:20px">&nbsp;</td>
                <td colspan="3">
           
        <dx:aspxcombobox ID="cboProject" runat="server" ClientInstanceName="cboProject"
                            Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Project_Name"
                            ValueField="Project_ID" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                                         
                            <Columns>            
                            <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                            <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:aspxcombobox>
           
                </td>
                <td align="left">
                    
                </td>
                <td style=" width:10px">
           
          
           
                </td>
                <td>
                 
                              
                </td>
                <td>
                    </td>
                <td>
                       
                    </td>
            </tr>          
            

            <tr style="height: 50px">
                <td style=" width:100px; padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|' + cboProject.GetValue());
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td style=" width:100px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">                        
                        <Paddings Padding="2px" />
                        <ClientSideEvents Click="Validation" />
                    </dx:ASPxButton>
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td style=" width:10px">
                    <dx:ASPxButton ID="btnUpload" runat="server" Text="Upload" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnUpload" 
                        Theme="Default">                        
                        <Paddings Padding="2px" />
                        <ClientSideEvents Click="function(s, e) {
	                        cbupload.PerformCallback('');
                        }" />
                    </dx:ASPxButton>
                </td>
                <td align="left">
                    
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td>
         
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select Project_ID, Project_Name From Proj_Header Where Project_Type='PT01'">
                    </asp:SqlDataSource>
                    &nbsp;<dx:ASPxCallback ID="cbupload" runat="server" ClientInstanceName="cbupload">
                    </dx:ASPxCallback>
&nbsp;<asp:SqlDataSource ID="SqlDataSource2" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Language'">
                    </asp:SqlDataSource>
                 
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                </dx:ASPxGridViewExporter>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div>
    <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" KeyFieldName="Project_ID; Part_No" Theme="Office2010Black" Width="100%" 
                Font-Names="Segoe UI" Font-Size="9pt" >
                <ClientSideEvents EndCallback="OnEndCallback" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Part No" FieldName="Part_No" 
                        VisibleIndex="1" Width="100px">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="6px" />
                        </HeaderStyle>     
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn FieldName="Part_Name" 
                      Width="250px" Caption="Part Name" VisibleIndex="2">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="Upc" 
                      Width="70px" Caption="Upc" VisibleIndex="3">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="Fna" 
                      Width="70px" Caption="Fna" VisibleIndex="4">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="Epl_Part_No" 
                      Width="100px" Caption="Epl Part No" VisibleIndex="5">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="Dtl_Upc" 
                      Width="70px" Caption="Dtl Upc" VisibleIndex="6">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="Dtl_Fna" 
                      Width="70px" Caption="Dtl Fna" VisibleIndex="7">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="Usg_Seq" 
                      Width="70px" Caption="Usg Seq" VisibleIndex="8">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="Lvl" 
                      Width="70px" Caption="Lvl" VisibleIndex="9">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="Qty" 
                      Width="70px" Caption="Qty" VisibleIndex="10">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="Hand" 
                      Width="70px" Caption="Hand" VisibleIndex="11">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="D_C" 
                      Width="70px" Caption="D_C" VisibleIndex="12">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                     <dx:GridViewDataTextColumn FieldName="Dwg_No" 
                      Width="100px" Caption="Dwg No" VisibleIndex="13">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="J_Note_Address" 
                      Width="100px" Caption="J Note Address" VisibleIndex="14">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="L_Note_Address" 
                      Width="100px" Caption="L Note Address" VisibleIndex="15">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="Color_Part" 
                      Width="70px" Caption="Color Part" VisibleIndex="16">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="CC_Code" 
                        Width="100px" Caption="CC Code" VisibleIndex="17">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="Intr_Color" 
                      Width="70px" Caption="Intr Color" VisibleIndex="18">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="Body_Color" 
                        Width="100px" Caption="Body Color" VisibleIndex="19">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn FieldName="Extr_Color" 
                      Width="70px" Caption="Extr Color" VisibleIndex="20">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                
                    <dx:GridViewBandColumn Caption="Variant 1" VisibleIndex="21" Name="Variant1" Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code1" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="22" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD1" 
                                    Width="90px" Caption="KD" VisibleIndex="23" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID1" 
                                  Width="90px" Caption="PID" VisibleIndex="24" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>

                     <dx:GridViewBandColumn Caption="Variant 2" VisibleIndex="25" Name="Variant2"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code2" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="26" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD2" 
                                    Width="90px" Caption="KD" VisibleIndex="27" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID2" 
                                  Width="90px" Caption="PID" VisibleIndex="28" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>

                      <dx:GridViewBandColumn Caption="Variant 3" VisibleIndex="29" Name="Variant3" Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code3" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="30" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD3" 
                                    Width="90px" Caption="KD" VisibleIndex="31" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID3" 
                                  Width="90px" Caption="PID" VisibleIndex="32" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>

                      <dx:GridViewBandColumn Caption="Variant 4" VisibleIndex="33" Name="Variant4"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code4" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="34" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD4" 
                                    Width="90px" Caption="KD" VisibleIndex="35" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID4" 
                                  Width="90px" Caption="PID" VisibleIndex="36" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>

                      <dx:GridViewBandColumn Caption="Variant 5" VisibleIndex="37" Name="Variant5"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code5" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="38" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD5" 
                                    Width="90px" Caption="KD" VisibleIndex="39" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID5" 
                                  Width="90px" Caption="PID" VisibleIndex="40" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>

                    <dx:GridViewBandColumn Caption="Variant 6" VisibleIndex="41" Name="Variant6"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code6" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="42" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD6" 
                                    Width="90px" Caption="KD" VisibleIndex="43" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID6" 
                                  Width="90px" Caption="PID" VisibleIndex="44" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 7" VisibleIndex="45" Name="Variant7"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code7" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="46" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD7" 
                                    Width="90px" Caption="KD" VisibleIndex="47" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID7" 
                                  Width="90px" Caption="PID" VisibleIndex="48" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 8" VisibleIndex="49" Name="Variant8"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code8" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="50" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD8" 
                                    Width="90px" Caption="KD" VisibleIndex="51" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID8" 
                                  Width="90px" Caption="PID" VisibleIndex="52" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 9" VisibleIndex="53" Name="Variant9"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code9" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="54" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD9" 
                                    Width="90px" Caption="KD" VisibleIndex="55" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID9" 
                                  Width="90px" Caption="PID" VisibleIndex="56" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 10" VisibleIndex="57" Name="Variant10"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code10" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="58" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD10" 
                                    Width="90px" Caption="KD" VisibleIndex="59" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID10" 
                                  Width="90px" Caption="PID" VisibleIndex="60" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>

                    <dx:GridViewBandColumn Caption="Variant 11" VisibleIndex="61" Name="Variant11"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code11" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="62" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD11" 
                                    Width="90px" Caption="KD" VisibleIndex="63" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID11" 
                                  Width="90px" Caption="PID" VisibleIndex="64" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 12" VisibleIndex="65" Name="Variant12"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code12" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="66" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD12" 
                                    Width="90px" Caption="KD" VisibleIndex="67" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID12" 
                                  Width="90px" Caption="PID" VisibleIndex="68" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 13" VisibleIndex="69" Name="Variant13"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code13" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="70" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD13" 
                                    Width="90px" Caption="KD" VisibleIndex="71" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID13" 
                                  Width="90px" Caption="PID" VisibleIndex="72" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 14" VisibleIndex="73" Name="Variant14"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code14" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="74" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD14" 
                                    Width="90px" Caption="KD" VisibleIndex="75" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID14" 
                                  Width="90px" Caption="PID" VisibleIndex="76" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 15" VisibleIndex="77" Name="Variant15"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code15" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="78" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD15" 
                                    Width="90px" Caption="KD" VisibleIndex="79" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID15" 
                                  Width="90px" Caption="PID" VisibleIndex="80" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
                     
                    <dx:GridViewBandColumn Caption="Variant 16" VisibleIndex="81" Name="Variant16"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code16" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="82" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD16" 
                                    Width="90px" Caption="KD" VisibleIndex="83" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID16" 
                                  Width="90px" Caption="PID" VisibleIndex="84" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 17" VisibleIndex="85" Name="Variant17"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code17" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="86" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD17" 
                                    Width="90px" Caption="KD" VisibleIndex="87" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID17" 
                                  Width="90px" Caption="PID" VisibleIndex="88" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 18" VisibleIndex="89" Name="Variant18"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code18" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="90" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD18" 
                                    Width="90px" Caption="KD" VisibleIndex="91" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID18" 
                                  Width="90px" Caption="PID" VisibleIndex="92" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 19" VisibleIndex="93" Name="Variant19"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code19" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="94" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD19" 
                                    Width="90px" Caption="KD" VisibleIndex="95" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID19" 
                                  Width="90px" Caption="PID" VisibleIndex="96" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 20" VisibleIndex="97" Name="Variant20"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code20" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="98" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD20" 
                                    Width="90px" Caption="KD" VisibleIndex="99" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID20" 
                                  Width="90px" Caption="PID" VisibleIndex="100" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
                    
                    <dx:GridViewBandColumn Caption="Variant 21" VisibleIndex="101" Name="Variant21"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code21" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="102" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD21" 
                                    Width="90px" Caption="KD" VisibleIndex="103" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID21" 
                                  Width="90px" Caption="PID" VisibleIndex="104" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 22" VisibleIndex="105" Name="Variant22"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code22" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="106" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD22" 
                                    Width="90px" Caption="KD" VisibleIndex="107" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID22" 
                                  Width="90px" Caption="PID" VisibleIndex="108" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 23" VisibleIndex="109" Name="Variant23"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code23" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="110" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD23" 
                                    Width="90px" Caption="KD" VisibleIndex="111" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID23" 
                                  Width="90px" Caption="PID" VisibleIndex="112" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 24" VisibleIndex="113" Name="Variant24"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code24" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="114" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD24" 
                                    Width="90px" Caption="KD" VisibleIndex="115" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID24" 
                                  Width="90px" Caption="PID" VisibleIndex="116" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 25" VisibleIndex="117" Name="Variant25"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code25" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="118" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD25" 
                                    Width="90px" Caption="KD" VisibleIndex="119" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID25" 
                                  Width="90px" Caption="PID" VisibleIndex="120" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
                    
                    <dx:GridViewBandColumn Caption="Variant 26" VisibleIndex="121" Name="Variant26"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code26" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="122" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD26" 
                                    Width="90px" Caption="KD" VisibleIndex="123" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID26" 
                                  Width="90px" Caption="PID" VisibleIndex="124" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 27" VisibleIndex="125" Name="Variant27"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code27" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="126" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD27" 
                                    Width="90px" Caption="KD" VisibleIndex="127" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID27" 
                                  Width="90px" Caption="PID" VisibleIndex="128" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 28" VisibleIndex="129" Name="Variant28"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code28" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="130" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD28" 
                                    Width="90px" Caption="KD" VisibleIndex="131" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID28" 
                                  Width="90px" Caption="PID" VisibleIndex="132" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 29" VisibleIndex="133" Name="Variant29"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code29" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="134" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD29" 
                                    Width="90px" Caption="KD" VisibleIndex="135" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID29" 
                                  Width="90px" Caption="PID" VisibleIndex="136" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 30" VisibleIndex="137" Name="Variant30"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code30" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="138" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD30" 
                                    Width="90px" Caption="KD" VisibleIndex="139" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID30" 
                                  Width="90px" Caption="PID" VisibleIndex="140" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
                   
                   	<dx:GridViewBandColumn Caption="Variant 31" VisibleIndex="141" Name="Variant31"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code31" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="142" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD31" 
                                    Width="90px" Caption="KD" VisibleIndex="143" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID31" 
                                  Width="90px" Caption="PID" VisibleIndex="144" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 32" VisibleIndex="145" Name="Variant32"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code32" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="146" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD32" 
                                    Width="90px" Caption="KD" VisibleIndex="147" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID32" 
                                  Width="90px" Caption="PID" VisibleIndex="148" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 33" VisibleIndex="149" Name="Variant33"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code33" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="150" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD33" 
                                    Width="90px" Caption="KD" VisibleIndex="151" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID33" 
                                  Width="90px" Caption="PID" VisibleIndex="152" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 34" VisibleIndex="153" Name="Variant34"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code34" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="154" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD34" 
                                    Width="90px" Caption="KD" VisibleIndex="155" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID34" 
                                  Width="90px" Caption="PID" VisibleIndex="156" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 35" VisibleIndex="157" Name="Variant35"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code35" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="158" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD35" 
                                    Width="90px" Caption="KD" VisibleIndex="159" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID35" 
                                  Width="90px" Caption="PID" VisibleIndex="160" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>
					
					<dx:GridViewBandColumn Caption="Variant 36" VisibleIndex="161" Name="Variant36"  Visible="false">
                                <Columns>
                                 <dx:GridViewDataTextColumn FieldName="Variant_Supply_Cntry_Code36" 
                                  Width="90px" Caption="Supply Cntry Code" VisibleIndex="162" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="Variant_KD36" 
                                    Width="90px" Caption="KD" VisibleIndex="163" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Variant_PID36" 
                                  Width="90px" Caption="PID" VisibleIndex="164" Visible="false">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>

                      <dx:GridViewDataTextColumn FieldName="Check_Item" 
                                  Width="150px" Caption="Check Item" VisibleIndex="165">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                <Paddings PaddingLeft="5px"></Paddings>
                                </HeaderStyle>
                     </dx:GridViewDataTextColumn>

                     <dx:GridViewBandColumn Caption="○RU1ﾁｪｯｸ" VisibleIndex="166" Name="○RU1">
                                <Columns>
                                <dx:GridViewDataTextColumn FieldName="Check_Ru1_Supply_Cntry_Code1" 
                                              Width="90px" Caption="Supply Cntry Code" VisibleIndex="167">
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                                            <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                 </dx:GridViewDataTextColumn>

                                 <dx:GridViewDataTextColumn FieldName="Check_Ru1_KD1" 
                                              Width="90px" Caption="KD" VisibleIndex="168">
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                 </dx:GridViewDataTextColumn>

                                  <dx:GridViewDataTextColumn FieldName="Check_Ru1_PID1" 
                                              Width="90px" Caption="PID" VisibleIndex="169">
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                 </dx:GridViewDataTextColumn>

                                 <dx:GridViewDataTextColumn FieldName="Check_Ru1_Shipping_unit_IKP" 
                                              Width="90px" Caption="IKP納入単位" VisibleIndex="170">
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                 </dx:GridViewDataTextColumn>
                                </Columns>
                    </dx:GridViewBandColumn>

                    <dx:GridViewDataTextColumn FieldName="Supply_Cntr_Code" 
                                Width="90px" Caption="Supply Cntry Code" VisibleIndex="171">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>  

                    <dx:GridViewDataTextColumn FieldName="Partition_Classification" 
                                Width="150px" Caption="部品の区分" VisibleIndex="172">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="Splr_From" 
                                Width="150px" Caption="Splr From" VisibleIndex="173">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="Splr_To" 
                                Width="150px" Caption="Splr To" VisibleIndex="174">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="Co_New" 
                                Width="90px" Caption="Co New" VisibleIndex="175">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>
               
                    <dx:GridViewDataTextColumn FieldName="Source_of_Diversion" 
                                Width="150px" Caption="流用元" VisibleIndex="176">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="Product_Number" 
                                Width="150px" Caption="流用元品番 (IDN LOCAL)" VisibleIndex="177">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="Plan_Change_Setting" 
                                Width="150px" Caption="設変予定" VisibleIndex="178">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="ECR_No" 
                                Width="90px" Caption="ECR No" VisibleIndex="179">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn FieldName="Remarks" 
                                Width="150px" Caption="ｺﾒﾝﾄ" VisibleIndex="180">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="IAMI_sourcing" 
                                Width="150px" Caption="IAMI sourcing" VisibleIndex="181">
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                                
                </Columns>

                <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                <SettingsPopup>
                    <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                </SettingsPopup>

                <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />

                <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                    <Header HorizontalAlign="Center">
                        <Paddings Padding="2px"></Paddings>
                    </Header>

                    <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                        <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                    </EditFormColumnCaption>
                </Styles>
       
             </dx:ASPxGridView>
    </div> 
</asp:Content>

﻿Imports System
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors

Public Class PO_Notification
    Inherits System.Web.UI.Page
    Dim pUser As String = ""
    Dim pPRNo As String = ""
    Private Sub GridLoad(pUser As String, pPRNo As String)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        ds = GetDataSource(CmdType.StoreProcedure, "sp_PO_Notification", "UserID|PR_Number", pUser & "|" & pPRNo, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "PO Notification"

        If Request.QueryString("ID") <> "" Then
            pUser = Split(Request.QueryString("ID"), "|")(0)
            pPRNo = Split(Request.QueryString("ID"), "|")(1)

            Call GridLoad(pUser, pPRNo)
        End If

    End Sub


    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Call GridLoad(pUser, pPRNo)

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try
    End Sub

    Private Sub Grid_CustomButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonEventArgs) Handles Grid.CustomButtonInitialize
        If (e.Column.Name = "PR") Then
            If (e.CellType = DevExpress.Web.ASPxGridView.GridViewTableCommandCellType.Data) Then
                Dim grid As ASPxGridView = CType(sender, ASPxGridView)
                e.Text = grid.GetRowValues(e.VisibleIndex, "PR_Number").ToString
            End If

        End If

        If (e.Column.Name = "CE") Then
            If (e.CellType = DevExpress.Web.ASPxGridView.GridViewTableCommandCellType.Data) Then
                Dim grid As ASPxGridView = CType(sender, ASPxGridView)
                e.Text = grid.GetRowValues(e.VisibleIndex, "CE_Number").ToString
            End If

        End If

        If (e.Column.Name = "IAB") Then
            If (e.CellType = DevExpress.Web.ASPxGridView.GridViewTableCommandCellType.Data) Then
                Dim grid As ASPxGridView = CType(sender, ASPxGridView)
                e.Text = grid.GetRowValues(e.VisibleIndex, "IA_Budget_Number").ToString
            End If

        End If

        If (e.Column.Name = "IAN") Then
            If (e.CellType = DevExpress.Web.ASPxGridView.GridViewTableCommandCellType.Data) Then
                Dim grid As ASPxGridView = CType(sender, ASPxGridView)
                e.Text = grid.GetRowValues(e.VisibleIndex, "IA_Number").ToString
            End If

        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Call GridLoad(pUser, pPRNo)

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try
    End Sub

    Protected Sub keyFieldLink_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim link As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
        Dim container As GridViewDataItemTemplateContainer = TryCast(link.NamingContainer, GridViewDataItemTemplateContainer)

        If container.ItemIndex >= 0 Then
            link.Text = Split(container.KeyValue, "|")(5)
            link.Target = "_blank"
            link.NavigateUrl = Split(container.KeyValue, "|")(9)
        End If
    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Main.aspx.vb" Inherits="IAMI_EPIC2.Main" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="header" ContentPlaceHolderID="HeadContent" runat="server">
<!-- Bootstrap 3.3.7 -->
   <link rel="stylesheet" type="text/css" media="screen" href="dashboard_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
   <link rel="stylesheet" type="text/css" media="screen" href="dashboard_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
   <link rel="stylesheet" type="text/css" media="screen" href="dashboard_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
   <link rel="stylesheet" type="text/css" media="screen" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" type="text/css" media="screen" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
   <link rel="stylesheet" type="text/css" media="screen" href="dashboard_components/morris.js/morris.css">
  <!-- jvectormap -->
   <link rel="stylesheet" type="text/css" media="screen" href="dashboard_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
   <link rel="stylesheet" type="text/css" media="screen" href="dashboard_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
   <link rel="stylesheet" type="text/css" media="screen" href="dashboard_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <%-- <link rel="stylesheet" type="text/css" media="screen" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">--%>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <link rel="stylesheet" type="text/css" media="screen" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style type="text/css">
        .style2
        {
            height: 45px;
        }
        .style3
        {
            width: 40px;
            height: 45px;
        }
        .hidden-div
        {
            display: none;
        }
        
         .show-div
        {
           
        }
    </style>
</asp:Content>
<asp:Content ID="Main" ContentPlaceHolderID="content" runat="server">
<asp:Repeater ID="rpMain" runat="server">
<ItemTemplate>
 <div class='<%# Eval("css")%>' >
 </ItemTemplate>
</asp:Repeater>
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h4 mb-0 text-gray-800">Task List</h1>
           </div>

             <!-- Main content -->
     <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-2 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><asp:Label ID="lblTaskList1" runat="server" Text="lblTaskList"></asp:Label></h3>

              <p>Purchase Request</p>
            </div>
            <div class="icon">
              <i class="fa fa-lg fa-fw fa-folder-o"></i>
            </div>
            <a href="TaskList.aspx" class="small-box-footer">Task List Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lblTaskList2" runat="server" Text="lblTaskList"></asp:Label></h3>

              <p>RFQ</p>
            </div>
            <div class="icon">
              <i class="fa fa-lg fa-fw fa-barcode"></i>
            </div>
            <a href="TaskList.aspx" class="small-box-footer">Task List Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><asp:Label ID="lblTaskList3" runat="server" Text="lblTaskList"></asp:Label></h3>

              <p>Cost Estimation</p>
            </div>
            <div class="icon">
              <i class="fa fa-lg fa-fw fa-desktop"></i>
            </div>
            <a href="TaskList.aspx" class="small-box-footer">Task List Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><asp:Label ID="lblTaskList4" runat="server" Text="lblTaskList"></asp:Label></h3>

              <p>CP Tender</p>
            </div>
            <div class="icon">
              <i class="fa fa-lg fa-fw fa-edit"></i>
            </div>
            <a href="TaskList.aspx" class="small-box-footer">Task List Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
          <!-- ./col -->
        <div class="col-lg-2 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3><asp:Label ID="lblTaskList5" runat="server" Text="lblTaskList"></asp:Label></h3>

              <p>CP Non Tender</p>
            </div>
            <div class="icon">
              <i class="fa fa-lg fa-fw fa-book"></i>
            </div>
            <a href="TaskList.aspx" class="small-box-footer">Task List Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

          <!-- ./col -->
        <div class="col-lg-2 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><asp:Label ID="lblTaskList6" runat="server" Text="lblTaskList"></asp:Label></h3>

              <p>IA Price</p>
            </div>
            <div class="icon">
              <i class="fa fa-lg fa-fw fa-money"></i>
            </div>
            <a href="TaskList.aspx" class="small-box-footer">Task List Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
           <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h2 class="h4 mb-0 text-gray-800">&nbsp;&nbsp;&nbsp;Purchase Request Monitoring Status</h2>
           </div>
      </div>
      <!-- /.row (main row) -->
        <!-- /.box -->

          <div class="box"> 

          </div>
          

<div style="width: 100%; height: 300px; overflow: scroll">
    <asp:GridView ID="GridViewReport" runat="server" Width="100%" ShowHeaderWhenEmpty="true"  CssClass="table table-responsive table-striped"
          AutoGenerateColumns=False ShowHeader="true" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
        <asp:BoundField />
       <asp:BoundField ItemStyle-Width="20%" HeaderStyle-Height=30px DataField = "PR_Number" 
            HeaderText = "PR Number" >
<HeaderStyle Height="30px"></HeaderStyle>

<ItemStyle Width="15%"></ItemStyle>
        </asp:BoundField>
       <asp:BoundField ItemStyle-Width="30%" DataField = "Current_Status" 
            HeaderText = "Current Status" >
<ItemStyle Width="30%"></ItemStyle>
        </asp:BoundField>
       <asp:BoundField ItemStyle-Width="45%" DataField = "Progress" 
            HeaderText = "Progress" HtmlEncode="False" >
<ItemStyle Width="45%" HorizontalAlign="Left" VerticalAlign="Bottom"></ItemStyle>
        </asp:BoundField>
       <asp:BoundField ItemStyle-Width="5%" DataField = "Progress_Percentage" 
            HtmlEncode="False"  >
<ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
        </asp:BoundField>
   </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
  </asp:GridView></div>
          <!-- /.box -->
</div>

</asp:Content>
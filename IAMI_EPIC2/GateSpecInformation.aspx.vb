﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors

Public Class GateSpecInformation
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String

    Dim prjid As String
    Dim grpid As String
    Dim comm As String
    Dim pErr As String = ""
    Dim pg As String = ""
    Dim py As String = ""
    Dim byr As String = ""
    Dim grpcomm As String = ""
    Dim pagetype As String = ""
    'notes
    'CC  	costcontrol
    'PP	    product planing
    'QA	    qualityassurance
    'PC	    prodcostplan
    'PED	engdesing
    'PPE	PPE
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        'sGlobal.getMenu("J010")
        Master.SiteTitle = "Gate Spec Information"
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        prjid = Request.QueryString("Project_Id")
        grpid = Request.QueryString("Group_Id")
        comm = Request.QueryString("comm")
        pg = Request.QueryString("page")
        py = Request.QueryString("project_type")
        byr = Request.QueryString("buyer")
        grpcomm = Request.QueryString("groupcommodity")
        pagetype = Request.QueryString("pagetype")


        sGlobal.getMenu(pg)
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            getExistsData()
            getGridDepartment(GridCostControl, "CC")

            If pg = "J010" Then
                If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Accepted" Then
                    btnSave.Enabled = False
                End If
            ElseIf pg = "J020" Then
                If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Completed" Then
                    btnSave.Enabled = False
                End If
            ElseIf pg = "J030" Then
                If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Confirmed" Then
                    btnSave.Enabled = False
                End If
            End If



            'If pagetype = "submit" Then
            '    KHSAttendance.ClientEnabled = False
            '    GridCostControl.Enabled = False
            'Else
            '    KHSAttendance.ClientEnabled = True
            '    GridCostControl.Enabled = True
            'End If

        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
       
    End Sub

    Private Sub getExistsData()
        Dim ds As DataSet
        Dim clsgatespec As New clsGateSpecInformation
        ds = clsGateICheckSheetDB.GetDateSpecInformation(prjid, grpid, comm, byr, pUser, grpcomm, statusAdmin, clsgatespec)

        cboBaseDrawing.Value = clsgatespec.BaseDrawing
        cboSOR.Value = clsgatespec.SOR
        txtMaterial.Text = clsgatespec.Material
        txtProcess.Text = clsgatespec.Process
        cboDevSchedule.Value = clsgatespec.CheckDevelopmentSchedule
        txtAdditionalInformation.Text = clsgatespec.AdditionalInformation
        KHSAttendance.Value = CDate(IIf(String.IsNullOrEmpty(clsgatespec.KSHAttendance), Now.Date, clsgatespec.KSHAttendance))
    End Sub

    Private Sub getGridDepartment(_gridView As ASPxGridView, _department As String)
        Dim ds As DataSet = clsGateICheckSheetDB.GetGridDepartment(prjid, grpid, comm, byr, _department, pUser, grpcomm, statusAdmin)
        _gridView.DataSource = ds
        _gridView.DataBind()
    End Sub

    Private Sub GridCostControl_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridCostControl.AfterPerformCallback
        getGridDepartment(GridCostControl, "CC")
    End Sub

    Private Sub GridCostControl_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles GridCostControl.CommandButtonInitialize
   
        If pg = "J010" Then
            If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Accepted" Then
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                    e.Visible = False
                End If

                If e.ButtonType = ColumnCommandButtonType.New Then
                    e.Visible = False
                End If
            End If
        ElseIf pg = "J020" Then
            If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Completed" Then
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                    e.Visible = False
                End If
                If e.ButtonType = ColumnCommandButtonType.New Then
                    e.Visible = False
                End If
            End If
        ElseIf pg = "J030" Then
            If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Confirmed" Then
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                    e.Visible = False
                End If
                If e.ButtonType = ColumnCommandButtonType.New Then
                    e.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub GridCostControl_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridCostControl.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            getGridDepartment(GridCostControl, "CC")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            GridCostControl.DataSource = ds
            GridCostControl.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else
            getGridDepartment(GridCostControl, "CC")
        End If
    End Sub

    Private Sub GridCostControl_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles GridCostControl.RowDeleting
        e.Cancel = True
        clsGateICheckSheetDB.DeleteDepartment(prjid, grpid, comm, e.Values("Department_Code"), byr, e.Values("UserID"), pUser, grpcomm, statusAdmin, pErr)
        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCostControl)
        Else
            GridCostControl.CancelEdit()
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridCostControl)
            getGridDepartment(GridCostControl, "CC")
        End If
    End Sub

    Public Function checking(value As String) As Boolean
        If String.IsNullOrEmpty(value) Or value = "0" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub GridCostControl_RowValidating(sender As Object, e As DevExpress.Web.Data.ASPxDataValidationEventArgs) Handles GridCostControl.RowValidating
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridCostControl.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "Department_Code" Then
                If IsNothing(e.NewValues("Department_Code")) Then
                    GridCostControl.JSProperties("cp_message") = 1
                    e.Errors(dataColumn) = "Deartment cannot empty"
                    show_error(MsgTypeEnum.Warning, "Department cannot empty", 1, GridCostControl)
                End If
            End If

            If dataColumn.FieldName = "UserID" Then
                If IsNothing(e.NewValues("UserID")) Then
                    GridCostControl.JSProperties("cp_message") = 1
                    e.Errors(dataColumn) = "UserID cannot empty"
                    show_error(MsgTypeEnum.Warning, "User ID cannot empty", 1, GridCostControl)
                End If
            End If

        Next
       
    End Sub

    Private Sub GridCostControl_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles GridCostControl.RowInserting
        e.Cancel = True

        'cek data spec info sebelum input spec info attention user ini
        Dim ds2 As New DataSet
        ds2 = clsGateICheckSheetDB.CheckExistsData(prjid, grpid, comm, byr, grpcomm, cboBaseDrawing.Value, cboSOR.Value)

        If ds2.Tables(0).Rows(0)("RetVal").ToString.Trim = "0" Then
            show_error(MsgTypeEnum.Warning, "Please insert Specification Info first!", 1, GridCostControl)
            Exit Sub
        End If

        clsGateICheckSheetDB.InsertDepartment(prjid, grpid, comm, e.NewValues("Department_Code"), grpcomm, byr, e.NewValues("UserID"), pUser, statusAdmin, pErr)
        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCostControl)
        Else
            GridCostControl.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridCostControl)
            getGridDepartment(GridCostControl, "CC")
        End If
    End Sub

    Private Sub GridCostControl_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridCostControl.RowUpdating
        e.Cancel = True

        clsGateICheckSheetDB.UpdateDepartment(prjid, grpid, comm, e.OldValues("Department_Code"), grpcomm, byr, e.OldValues("UserID"), pUser, statusAdmin, e.NewValues("Department_Code"), e.NewValues("UserID"), pErr)
        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCostControl)
        Else
            GridCostControl.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridCostControl)
            getGridDepartment(GridCostControl, "CC")
        End If
    End Sub
#End Region

#Region "Event Control"
    Private Sub up_FillComboUserId(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _department As String, _
                            pAdminStatus As String, pUserID As String, _
                            Optional ByRef pGroup As String = "", _
                            Optional ByRef pParent As String = "", _
                            Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsGateICheckSheetDB.GetDepartment(_department)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        If pg = "J010" Then
            If pagetype <> "submit" Then
                Response.Redirect("GetCheckSheetView.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
            Else
                Response.Redirect("Get_I_Check_Sheet.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
            End If

        ElseIf pg = "J020" Then
            Response.Redirect("GetCheckSheetConfirm.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
        ElseIf pg = "J030" Then
            Response.Redirect("GetCheckSheetApproval.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
        Else
            'nothing
        End If
    End Sub

    Private Sub cbSave_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSave.Callback
        Try
            clsGateICheckSheetDB.InsertGateSpecInformation(prjid, grpid, comm, cboBaseDrawing.Value, cboSOR.Value, txtMaterial.Text, txtProcess.Text, _
                                                            cboDevSchedule.Value, txtAdditionalInformation.Text, KHSAttendance.Value, byr, grpcomm, pUser, statusAdmin)
            cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"

        Catch ex As Exception
            cbSave.JSProperties("cpMessage") = ex.Message
        End Try
    End Sub

    Private Sub cmbCombo_OnCallback(ByVal source As Object, ByVal e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase)
        FillCombo(TryCast(source, ASPxComboBox), e.Parameter, sdsCC)
    End Sub

    Protected Sub InitializeCombo(ByVal e As ASPxGridViewEditorEventArgs, ByVal parentComboName As String, ByVal source As SqlDataSource, ByVal callBackHandler As DevExpress.Web.ASPxClasses.CallbackEventHandlerBase)

        Dim id As String = String.Empty
        If (Not GridCostControl.IsNewRowEditing) Then
            Dim val As Object = GridCostControl.GetRowValuesByKeyValue(e.KeyValue, parentComboName)
            If (val Is Nothing OrElse val Is DBNull.Value) Then
                id = Nothing
            Else
                id = val.ToString()
            End If
        End If
        Dim combo As ASPxComboBox = TryCast(e.Editor, ASPxComboBox)
        If combo IsNot Nothing Then
            ' unbind combo
            combo.DataSourceID = Nothing
            FillCombo(combo, id, source)
            AddHandler combo.Callback, callBackHandler
        End If
        Return
    End Sub

    Protected Sub FillCombo(ByVal cmb As ASPxComboBox, ByVal id As String, ByVal source As SqlDataSource)
        cmb.Items.Clear()
        ' trap null selection
        If String.IsNullOrEmpty(id) Then
            Return
        End If

        ' get the values
        source.SelectParameters(0).DefaultValue = id
        ' Dim view As DataView = CType(source.Select(DataSourceSelectArguments.Empty), DataView)
        Dim dt As DataTable = CType(source.Select(DataSourceSelectArguments.Empty), DataView).Table
        'Dim col As ListBoxColumn = New ListBoxColumn("UserID")
        'col.Name = "UserID"
        'col.FieldName = "UserID"
        'cmb.Columns.Add(col)

        'col = New ListBoxColumn("UserName")
        'col.Name = "UserName"
        'col.FieldName = "UserName"

        'cmb.Columns.Add(col)
        'cmb.TextFormatString = "{1}"
        cmb.ValueField = "UserID"
        cmb.TextField = "UserName"
        cmb.DataSource = dt
        cmb.DataBind()

        'For Each row As DataRowView In view
        '    cmb.Items.Add(row(1).ToString(), row(0))
        '    cmb.ValueField = "UserID"
        '    cmb.TextField = "UserName"
        '    'cmb.Items.Add(row(0).ToString(), row(1))
        'Next row


    End Sub

    Protected Sub GridCostControl_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridCostControl.CellEditorInitialize
        Select Case e.Column.FieldName
            Case "UserID"
                InitializeCombo(e, "Department_Code", sdsCC, AddressOf cmbCombo_OnCallback)
            Case "UserName"
                'If e.Value <> "" Then
                'e.Editor.Enabled = False
                'End If

        End Select
    End Sub
#End Region




   
End Class
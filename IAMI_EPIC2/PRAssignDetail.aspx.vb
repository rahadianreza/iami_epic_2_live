﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data

Public Class PRAssignDetail
    Inherits System.Web.UI.Page

    Dim pUser As String

    Private Sub up_FillCombo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsPRAcceptanceDB.GetComboData("PRBudget", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPRBudget.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("PRType", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPRType.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("Department", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboDepartment.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("Section", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboSection.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("CostCenter", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboCostCenter.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("PIC", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPIC.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        'ds = clsPRAcceptanceDB.GetComboData("Tender", pErr)
        'If pErr = "" Then
        '    For i = 0 To ds.Tables(0).Rows.Count - 1
        '        cboTender.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
        '    Next
        'End If

    End Sub

    Private Sub up_Print()
        'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/ViewPRAcceptance.aspx")
        Session("PRNumber") = txtPRNo.Text

        If chkUrgent.Checked Then
            Response.Redirect("~/ViewPRAssignDetailUrgent.aspx")
        Else
            Response.Redirect("~/ViewPRAssignDetail.aspx")
        End If

        '  Response.Redirect("~/ViewPRAssign.aspx")

    End Sub


    Private Sub up_LoadData(pPRNo As String, pRev As Integer)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim cPRAcceptance As New clsPRAcceptance

        Try
            cPRAcceptance.PRNumber = pPRNo
            cPRAcceptance.Revision = pRev

            ds = clsPRAssignDB.GetDataPR(cPRAcceptance, pErr)
            If pErr = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    dtDate.Value = ds.Tables(0).Rows(0)("PR_Date")
                    cboPRBudget.SelectedIndex = cboPRBudget.Items.IndexOf(cboPRBudget.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Budget_Code") & "")))
                    cboPRType.SelectedIndex = cboPRType.Items.IndexOf(cboPRType.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("PRType_Code") & "")))
                    cboDepartment.SelectedIndex = cboDepartment.Items.IndexOf(cboDepartment.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Department_Code") & "")))
                    cboSection.SelectedIndex = cboSection.Items.IndexOf(cboSection.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Section_Code") & "")))
                    cboCostCenter.SelectedIndex = cboCostCenter.Items.IndexOf(cboCostCenter.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("CostCenter") & "")))
                    txtProject.Text = ds.Tables(0).Rows(0)("Project")
                    txtRev.Text = ds.Tables(0).Rows(0)("Rev")
                    Dim PRStatus As String

                    If IsDBNull(ds.Tables(0).Rows(0)("Urgent_Status")) Then
                        chkUrgent.Checked = False
                        cbAssign.JSProperties("cpUrgent") = "0"
                    ElseIf ds.Tables(0).Rows(0)("Urgent_Status").ToString.ToUpper = "YES" Then
                        chkUrgent.Checked = True
                        cbAssign.JSProperties("cpUrgent") = "1"
                    ElseIf ds.Tables(0).Rows(0)("Urgent_Status").ToString.ToUpper = "NO" Then
                        chkUrgent.Checked = False
                        cbAssign.JSProperties("cpUrgent") = "0"
                    End If

                    If Not IsDBNull(ds.Tables(0).Rows(0)("Req_POIssueDate")) Then
                        dtReqPOIssue.Value = ds.Tables(0).Rows(0)("Req_POIssueDate")
                    End If

                    If IsDBNull(ds.Tables(0).Rows(0)("Urgent_Note")) Then
                        txtUrgentNote.Text = ""
                    Else
                        txtUrgentNote.Text = ds.Tables(0).Rows(0)("Urgent_Note")
                    End If


                    'If IsDBNull(ds.Tables(0).Rows(0)("Approval_Note")) Then
                    '    txtApprovalNote.Text = ""
                    'Else
                    '    txtApprovalNote.Text = ds.Tables(0).Rows(0)("Approval_Note")
                    'End If

                    If IsDBNull(ds.Tables(0).Rows(0)("PR_Status")) Then
                        PRStatus = "0"
                    Else
                        PRStatus = ds.Tables(0).Rows(0)("PR_Status")
                    End If


                    If Not IsDBNull(ds.Tables(0).Rows(0)("PIC_Assign")) Then
                        cboPIC.Text = ds.Tables(0).Rows(0)("PIC_Assign")
                    End If

                    If Not IsDBNull(ds.Tables(0).Rows(0)("PIC_PO")) Then
                        cboPICPO.Text = ds.Tables(0).Rows(0)("PIC_PO")
                    End If

                    If Not IsDBNull(ds.Tables(0).Rows(0)("PIC_PO_2")) Then
                        cboPICPO2.Text = ds.Tables(0).Rows(0)("PIC_PO_2")
                    End If

                    If Not IsDBNull(ds.Tables(0).Rows(0)("PIC_PO_3")) Then
                        cboPICPO3.Text = ds.Tables(0).Rows(0)("PIC_PO_3")
                    End If

                    If Not IsDBNull(ds.Tables(0).Rows(0)("Tender_Status")) Then
                        cboTender.Text = ds.Tables(0).Rows(0)("Tender_Status")
                    End If

                    'If cboPIC.Text <> "" Or cboPICPO.Text <> "" Then
                    '    btnAssign.Enabled = False
                    'End If

                    If PRStatus = "5" Then
                        Dim script As String = ""
                        script = "btnAssign.SetEnabled(false);" & vbCrLf & _
                                 "cboPIC.SetEnabled(false);" & vbCrLf & _
                                 "cboPICPO.SetEnabled(false);" & vbCrLf & _
                                 "cboPICPO2.SetEnabled(false);" & vbCrLf & _
                                 "cboPICPO3.SetEnabled(false);" & vbCrLf & _
                                 "cboTender.SetEnabled(false);"

                        ScriptManager.RegisterStartupScript(btnAssign, btnAssign.GetType(), "btnAssign", script, True)
                    End If
                    'If PRStatus = "1" Then
                    '    btnApprove.Enabled = False
                    '    btnReject.Enabled = True
                    '    lblStatus.Text = "UDAH DIAPPROVED"
                    '    lblStatus.ForeColor = Color.Blue
                    'ElseIf PRStatus = "2" Then
                    '    btnReject.Enabled = False
                    '    btnApprove.Enabled = True
                    '    lblStatus.Text = "UDAH DIREJECT"
                    '    lblStatus.ForeColor = Color.Red
                    'Else
                    '    lblStatus.Text = ""
                    'End If


                    Grid.DataSource = ds
                    Grid.DataBind()
                End If
            End If
        Catch ex As Exception
            cbAssign.JSProperties("cpMessage") = ex.Message
        End Try
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|B040", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAssign.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAssign, btnAssign.GetType(), "btnAssign", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboDepartment)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim prno As String
        Dim rev As Integer

        pUser = Session("user")

        If Request.QueryString("ID") <> "" Then
            prno = Split(Request.QueryString("ID"), "|")(0)
            rev = Split(Request.QueryString("ID"), "|")(1)
        End If


        Master.SiteTitle = "PURCHASE REQUEST ASSIGN DETAIL"

        cbAssign.JSProperties("cpMessage") = ""

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            txtPRNo.Text = prno
            up_FillCombo()
            up_LoadData(prno, rev)
        End If
    End Sub

    Private Sub up_Assign()
        Dim PRAssign As New clsPRAssign
        Dim MsgErr As String = ""
        Dim i As Integer

        PRAssign.PRNumber = txtPRNo.Text
        PRAssign.PIC = cboPIC.SelectedItem.GetValue("Code").ToString.Trim
        PRAssign.PICPO = cboPICPO.SelectedItem.GetValue("Code").ToString.Trim

        If IsNothing(cboPICPO2.Text) Or cboPICPO2.Text = "" Or cboPICPO2.SelectedIndex = -1 Then
            PRAssign.PICPO2 = ""
        Else
            PRAssign.PICPO2 = cboPICPO2.SelectedItem.GetValue("Code").ToString
        End If

        If IsNothing(cboPICPO3.Text) Or cboPICPO3.Text = "" Or cboPICPO3.SelectedIndex = -1 Then
            PRAssign.PICPO3 = ""
        Else
            PRAssign.PICPO3 = cboPICPO3.SelectedItem.GetValue("Code").ToString
        End If
        'PRAssign.PICPO2 = IIf(String.IsNullOrEmpty(cboPICPO2.SelectedItem.GetValue("Code")), "", cboPICPO2.SelectedItem.GetValue("Code"))
        'PRAssign.PICPO3 = IIf(String.IsNullOrEmpty(cboPICPO3.SelectedItem.GetValue("Code")), "", cboPICPO3.SelectedItem.GetValue("Code"))
        PRAssign.TenderStatus = cboTender.Text
        PRAssign.Revision = txtRev.Text
        i = clsPRAssignDB.Assign(PRAssign, pUser, MsgErr)
        If MsgErr = "" Then
            cbAssign.JSProperties("cpMessage") = "Data Has Been Assign Successfully"
        Else
            cbAssign.JSProperties("cpMessage") = MsgErr
        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        gs_Back = True
        Response.Redirect("~/PRAssign.aspx")
    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        up_Print()
    End Sub

    'Protected Sub btnAssign_Click(sender As Object, e As EventArgs) Handles btnAssign.Click
    '    up_Assign()
    'End Sub

    Private Sub cbApprove_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbAssign.Callback
        Dim pPIC As String = Split(e.Parameter, "|")(0)
        Dim pTender As String = Split(e.Parameter, "|")(1)
        up_Assign()
    End Sub


    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

End Class
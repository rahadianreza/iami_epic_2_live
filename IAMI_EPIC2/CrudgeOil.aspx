﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CrudgeOil.aspx.vb" Inherits="IAMI_EPIC2.CrudgeOil" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <script type="text/javascript">
      function OnEndCallback(s, e) {
          if (s.cp_disabled == "N") {
              //alert(s.cp_disabled);
              btnDownload.SetEnabled(true);
          } else if (s.cp_disabled == "Y") {
              //alert(s.cp_disabled);
              btnDownload.SetEnabled(false);

              toastr.info("There's no data to show!", 'Info');
              toastr.options.closeButton = false;
              toastr.options.debug = false;
              toastr.options.newestOnTop = false;
              toastr.options.progressBar = false;
              toastr.options.preventDuplicates = true;
              toastr.options.onclick = null;
              e.processOnServer = false;
              return;
          }
          if (s.cp_message != "" && s.cp_val == 1) {
              if (s.cp_type == "Success" && s.cp_val == 1) {
                  toastr.success(s.cp_message, 'Success');
                  toastr.options.closeButton = false;
                  toastr.options.debug = false;
                  toastr.options.newestOnTop = false;
                  toastr.options.progressBar = false;
                  toastr.options.preventDuplicates = true;
                  toastr.options.onclick = null;
                  s.cp_val = 0;
                  s.cp_message = "";
              }
              else if (s.cp_type == "Warning" && s.cp_val == 1) {
                  toastr.warning(s.cp_message, 'Warning');
                  toastr.options.closeButton = false;
                  toastr.options.debug = false;
                  toastr.options.newestOnTop = false;
                  toastr.options.progressBar = false;
                  toastr.options.preventDuplicates = true;
                  toastr.options.onclick = null;
                  ss.cp_val = 0;
                  s.cp_message = "";
              }
              else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                  toastr.error(s.cp_message, 'Error');
                  toastr.options.closeButton = false;
                  toastr.options.debug = false;
                  toastr.options.newestOnTop = false;
                  toastr.options.progressBar = false;
                  toastr.options.preventDuplicates = true;
                  toastr.options.onclick = null;
                  s.cp_val = 0;
                  s.cp_message = "";
              }
          }
          else if (s.cp_message == "" && s.cp_val == 0) {
              toastr.options.closeButton = false;
              toastr.options.debug = false;
              toastr.options.newestOnTop = false;
              toastr.options.progressBar = false;
              toastr.options.preventDuplicates = true;
              toastr.options.onclick = null;
          }
      };
      function MessageBox(s, e) {
          //alert(s.cpmessage);
          if (s.cbMessage == "Download Excel Successfully") {
              toastr.success(s.cbMessage, 'Success');
              toastr.options.closeButton = false;
              toastr.options.debug = false;
              toastr.options.newestOnTop = false;
              toastr.options.progressBar = false;
              toastr.options.preventDuplicates = true;
              toastr.options.onclick = null;
          }
          else if (s.cbMessage == "Request Date To must be higher than Request Date From") {
              toastr.success(s.cbMessage, 'Warning');
              toastr.options.closeButton = false;
              toastr.options.debug = false;
              toastr.options.newestOnTop = false;
              toastr.options.progressBar = false;
              toastr.options.preventDuplicates = true;
              toastr.options.onclick = null;
              e.processOnServer = false;
              return;
          }
          else {
              toastr.warning(s.cbMessage, 'Warning');
              toastr.options.closeButton = false;
              toastr.options.debug = false;
              toastr.options.newestOnTop = false;
              toastr.options.progressBar = false;
              toastr.options.preventDuplicates = true;
              toastr.options.onclick = null;
          }
      }

      function ItemCodeValidation(s, e) {
          if (ItemCode.GetValue() == null) {
              e.isValid = false;
          }
      }

      function GridLoad() {
          Grid.PerformCallback('refresh');
      }
    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        .style1
        {
            height: 18px;
        }
        .style2
        {
            height: 19px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
 <table style="width: 100%; border: 1px solid black; height: 100px;">  
                <tr style="height: 2px">
                    <td>&nbsp;</td>
                    <td></td>
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                </tr>      
           
                <tr style="height: 20px">
                    <td style=" padding:0px 0px 0px 10px; width:10%">
                        <dx:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="Period">
                        </dx:aspxlabel>                 
                    </td>
                    <td style= "width:1%">&nbsp;</td>
                    <td style="width:10%">
                          <dx:ASPxDateEdit ID="dtPeriodFrom" runat="server" Theme="Office2010Black" 
                                        Width="120px" AutoPostBack="false" ClientInstanceName="dtPeriodFrom"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" >
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>  
                    
                    </td>
                   <%-- <td style="width:10%">
                        <dx:ASPxTimeEdit ID="TmPeriod_From" EditFormat="Custom"  
                            EditFormatString="MMM yyyy" DisplayFormatString="MMM yyyy"  
                            Theme="Office2010Black" runat="server">
                        </dx:ASPxTimeEdit>

                    </td>--%>
                    <td style="width:1%; padding:0px 0px 0px 15px">    
                        <dx:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="~&nbsp; ">
                        </dx:aspxlabel>   
                                                     
                    </td>
                    <td style="width:180px">
                        <dx:ASPxDateEdit ID="dtPeriodTo" runat="server" Theme="Office2010Black" Width="120px"
                            AutoPostBack="false" ClientInstanceName="dtPeriodTo" EditFormatString="dd-MMM-yyyy"
                            DisplayFormatString="dd-MMM-yyyy" Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                            <CalendarProperties>
                                <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                </WeekNumberStyle>
                                <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                </ButtonStyle>
                            </CalendarProperties>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                        </dx:ASPxDateEdit>
                        <%--   <dx:ASPxTimeEdit ID="TmPeriod_To" EditFormat="Custom" 
                            EditFormatString="MMM yyyy" DisplayFormatString="MMM yyyy"  
                            Theme="Office2010Black" runat="server" 
                            >
                        </dx:ASPxTimeEdit>--%>

                    </td>
                </tr>    
           
                <tr>

                    <td style=" padding:0px 0px 0px 10px" class="style2">
                        </td>
                    <td class="style2"></td>
                    <td class="style2">           
                        </td>
                    <td class="style2"></td>
                    <td class="style2"></td>
                </tr>  
                        
                <tr>

                    <td style=" padding:0px 0px 0px 10px" class="style1" colspan="5">
                <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">
                        <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('refresh');
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
   
     &nbsp;<dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnDownload" Theme="Default">                        
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    &nbsp;<dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnAdd" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp;
                        <dx:ASPxButton ID="btnChart" runat="server" Text="Chart" UseSubmitBehavior="false"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnChart"
                            Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                     </td>

                </tr>  
                        
                <tr>

                    <td style=" padding:0px 0px 0px 10px" class="style3">
                        &nbsp;</td>
                    <td class="style3">&nbsp;</td>
                    <td class="style3">&nbsp;</td>
                    <td class="style3">&nbsp;</td>
                    <td class="style3">                
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                </dx:ASPxGridViewExporter>
                    </td>
                </tr>  
                        
                 </table>

 <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%"
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="OilType;Period">
            <ClientSideEvents EndCallback="OnEndCallback" CustomButtonClick="function(s, e) {
	                  if(e.buttonID == &#39;Edit&#39;){ 
                            var rowKey = Grid.GetRowKey(e.visibleIndex);
                            window.location.href= &#39;AddCrudgeOil.aspx?ID=&#39; + rowKey;
                      }
                      else if (e.buttonID == &#39;Delete&#39;){
                           if (confirm('Are you sure want to delete ?')) {
                        var rowKey = Grid.GetRowKey(e.visibleIndex);
                          Grid.PerformCallback('delete |' + rowKey);
                    } else {
                     
                    }
                      }
                  }"></ClientSideEvents>
            <Columns>
                <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" Width="150px">
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Oil Type" VisibleIndex="1" FieldName="OilType"
                    Width="150px">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="UOM" FieldName="UOM" VisibleIndex="2"
                    Width="150px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Price" VisibleIndex="3" FieldName="Price">
                     <PropertiesTextEdit DisplayFormatString="#,##0.00">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Period" VisibleIndex="4" FieldName="Period">
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
               
                <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="5" FieldName="Register_By">
                    <Settings AllowAutoFilter="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="6" FieldName="Register_Date">
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="7" FieldName="Update_By">
                    <Settings AllowAutoFilter="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="8" FieldName="Update_Date">
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollableHeight="300" HorizontalScrollBarMode="Auto"
                VerticalScrollBarMode="Auto"></Settings>
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header HorizontalAlign="Center">
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption>
                    <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
                </EditFormColumnCaption>
            </Styles>
             <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="Select Code, Description  From VW_MonthCode">
        </asp:SqlDataSource>
</asp:Content>

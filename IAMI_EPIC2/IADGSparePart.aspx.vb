﻿Imports DevExpress.XtraPrinting
Imports System.IO
Imports OfficeOpenXml


Public Class IADGSparePart
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""

    Dim pHeader As Boolean
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
    Dim fRefresh As Boolean = False
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(pProjectName As String)
        Dim ErrMsg As String = ""
        'Dim Pro As List(Of clsLabor)
        Dim ds As New DataSet
        ds = ClsIADGSparePartDB.getListIADDGSparePart(pProjectName, pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
            Dim Script
            If ds.Tables(0).Rows.Count > 0 Then

                Grid.JSProperties("cpMessage") = "Success"
                Script = "btnDownload.SetEnabled(true);"
                ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)
            Else
                Grid.JSProperties("cpMessage") = "There is no data to show!"
                Script = "btnDownload.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)
            End If
        Else
            Grid.JSProperties("cpMessage") = ErrMsg
        End If
    End Sub

    Private Sub up_ExcelGridAuto(Optional ByRef pErr As String = "")
        Try
            up_GridLoad(cboProject.Value)

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=IADGSparePartList" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using
            ps.Dispose()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            up_GridLoad("")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("I070")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "I070")
    End Sub
#End Region

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        up_GridLoad(cboProject.Value)
    End Sub
    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback

        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pProjectName As String
        pProjectName = Split(e.Parameters, "|")(1)

        If pFunction = "load" Then
            up_GridLoad(pProjectName)
        End If
    End Sub

    Protected Sub BtnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        'cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        Dim ErrMsg As String = ""
        'up_Excel(ErrMsg)
        up_ExcelGridAuto(ErrMsg)

        'If ErrMsg <> "" Then
        '    cbExcel.JSProperties("cpmessage") = ErrMsg
        'Else
        '    cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        'End If

    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Response.Redirect("UploadIADGSparePart.aspx")
    End Sub

    'unused
    Public Sub TemplateIADG_Excel()
        'Dim rowstart As Integer = 18
        'Dim dsEPL As DataSet
        'Dim dsDataEPL As DataSet
        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        Using exl As ExcelPackage = New ExcelPackage()
            Dim ws As ExcelWorksheet = exl.Workbook.Worksheets.Add("IADG Spare Part")
            ws.Cells.Style.Font.Size = 11

            ws.Cells("A1").Value = "No"
            ws.Cells("B1").Value = "Project Year"
            ws.Cells("C1").Value = "IADG Part No"
            ws.Cells("D1").Value = "Part Name"
            ws.Cells("E1").Value = "Model"
            ws.Cells("F1").Value = "New / Multi"
            ws.Cells("G1").Value = "Sample IADG Compare"
            ws.Cells("H1").Value = "Sample OEM"
            ws.Cells("I1").Value = "Volume / Month"
            ws.Cells("J1").Value = "Cost Target"
            ws.Cells("K1").Value = "Priority"

            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=IADGSparePartList" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using
            ps.Dispose()
            'Response.Clear()
            'Response.Buffer = False
            'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            'Response.AddHeader("content-disposition", "attachment; filename=IADGSparePartList" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd") + ".xlsx")

            'Dim stream As MemoryStream = New MemoryStream(exl.GetAsByteArray())
            ''Response.BinaryWrite(exl.GetAsByteArray())
            'Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length)
            'Response.Flush()
            'Response.Close()
        End Using


    End Sub
  

    Protected Sub btnTemplate_Click(sender As Object, e As EventArgs) Handles btnTemplate.Click
        ' Dim filepath = Server.MapPath("\Import\Template_IADGSparePart.xlsx")
        Dim folderPath As String = Server.MapPath("~/Export/Template_IADGSparePart.xlsx")
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + IO.Path.GetFileName(folderPath))
        Response.TransmitFile(folderPath)
        Response.End()
    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="MasterToolingDepreciation.aspx.vb" Inherits="IAMI_EPIC2.MasterToolingDepreciation" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };

         function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtItemCode.Focus();
            cboGroupItem.SetText('');
            cboCategory.SetText('');
            cboPRType.SetText('');
            txtSAPNumber.SetText('');
        }
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/ItemList.aspx";

            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/ItemList.aspx';
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                if (pathArray[1] == "AddItemMaster.aspx") {
                    window.location.href = window.location.origin + '/ItemList.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtSAPNumber.SetText('');
            txtItemCode.Focus();

        }
        else if (s.cpMessage == "Data Completed!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtItemCode.Focus();
            cboGroupItem.SetText('');
            cboCategory.SetText('');
            cboPRType.SetText('');
            txtSAPNumber.SetText('');
        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    };

         function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "Part_No" || currentColumnName == "Commodity" || currentColumnName == "Upc") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        };  

        function SaveData(s, e) {
//            cbkValid.PerformCallback('save');
            Grid.UpdateEdit()
        }

        function disableenablebuttonsubmit(_val) {
        alert('adsds');
            if (_val = '1') {
                btnSave.SetEnabled(false)
            } else {
                btnSave.SetEnabled(true)
             }
        }

          function OnUpdateCheckedChanged(s, e) {
            if (s.GetValue() == -1) s.SetValue(1);
            Grid.SetFocusedRowIndex(-1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "Status_Drawing", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "Status_Drawing", s.GetValue());
                }                
            }
        }

         function OnTabChanging(s, e) {
            var tabName = (pageControl.GetActiveTab()).name;
            e.cancel = !ASPxClientEdit.ValidateGroup('group' + tabName);
        }

         function gv_OnCustomButtonClick(s, e) {
             if(e.buttonID == 'btnDetail'){ 
                var key =  Grid.GetRowKey(e.visibleIndex);
                var keysplit =  key.split("|");

                window.location.href='AddMasterToolingDepreciation.aspx?partno=' +  keysplit[0]
             }
         }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="border: 1px solid black">
            <table>
                <tr>
                    <td style="padding: 10px 0px 0px 10px; width">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Part Name">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px; width">
                        <dx:ASPxComboBox ID="cboPartName" runat="server" ClientInstanceName="cboPartName" Width="120px"
                            Font-Names="Segoe UI" TextField="Part_No" ValueField="Part_No" TextFormatString="{0}" DataSourceID="sdsPartName"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Part No" FieldName="Part_No" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>

                         <asp:SqlDataSource ID="sdsPartName" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                                SelectCommand="Select A.Part_No, B.Part_Name From Mst_Tooling_Depreciation A INNER JOIN Mst_Tooling B ON A.Part_No = B.Part_No"></asp:SqlDataSource>
                    </td>
                </tr>
            </table>
            <table>
                <tr style="height: 50px">
                    <td style="width: 80px; padding: 10px 0px 0px 10px">
                        <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShowData" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
                                Grid.PerformCallback('load|');
                            }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 5px">
                        &nbsp;
                    </td>
                    <td style="width: 80px; padding: 10px 0px 0px 10px">
                        <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                   
                </tr>
            </table>
        </div>

        <br />

        <div>
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
            Font-Size="9pt" KeyFieldName="Part_No">
            <ClientSideEvents EndCallback="OnEndCallback" CustomButtonClick="gv_OnCustomButtonClick"></ClientSideEvents>
            <Columns>
               <%-- <dx:GridViewCommandColumn ShowEditButton="true">
                </dx:GridViewCommandColumn>--%>
                 <dx:GridViewCommandColumn Caption=" " VisibleIndex="0">
                       <CustomButtons>
                           <dx:GridViewCommandColumnCustomButton ID="btnDetail" Text="Detail">
                           </dx:GridViewCommandColumnCustomButton>
                       </CustomButtons>
                   </dx:GridViewCommandColumn>
                                  
                <dx:GridViewDataTextColumn Caption="Part No" VisibleIndex="1" FieldName="Part_No" Width="150px">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Part Name" FieldName="Part_Name" VisibleIndex="2"
                    Width="150px">
                    <EditFormSettings Visible="false" />
                      <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="5" Width="130px" FieldName="Register_By">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="5" Width="130px" FieldName="Register_Date">
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Updated By" VisibleIndex="5" Width="130px" FieldName="Update_By">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Updated Date" VisibleIndex="5" Width="130px" FieldName="Update_Date">
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

            </Columns>
            <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
            <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="380" />
            </SettingsPopup>
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header>
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
                <CommandColumnItem ForeColor="Orange">
                </CommandColumnItem>
            </Styles>
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
            <Templates>
                <EditForm>
                    <div style="padding: 15px 15px 15px 15px">
                        <dx:ContentControl ID="ContentControl1" runat="server">
                            <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                runat="server"></dx:ASPxGridViewTemplateReplacement>
                        </dx:ContentControl>
                    </div>
                    <div style="text-align: left; padding: 5px 5px 5px 15px">
                        <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                        <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                    </div>
                </EditForm>
            </Templates>
        </dx:ASPxGridView>
        </div>

        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
        </dx:ASPxGridViewExporter>
                
  </div>   
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PurchasedPart.aspx.vb" Inherits="IAMI_EPIC2.PurchasedPart" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }


	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
            <tr style="height: 50px">
                <td style=" width:100px; padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td style=" width:100px">                             
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td align="left">
    
                    <dx:ASPxGridViewExporter ID="GridExporter7" runat="server" 
                        GridViewID="Grid">
                    </dx:ASPxGridViewExporter>   
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                        SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'UOM'">
                    </asp:SqlDataSource>
                               
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td>
      
            
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <tr style="height: 50px">
        <td>
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                                    EnableTheming="True" KeyFieldName="Purchased_Part_Code" Theme="Office2010Black" 
                                    OnRowValidating="Grid_RowValidating" OnStartRowEditing="Grid_StartRowEditing"
                                    OnRowInserting="Grid_RowInserting" OnRowDeleting="Grid_RowDeleting" 
                                    OnAfterPerformCallback="Grid_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Purchased Part Code" FieldName="Purchased_Part_Code" 
                                            VisibleIndex="1" Width="150px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="Purchased_Part_Name" 
                                                            Width="200px" Caption="Purchased Part Name" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Purchased_Part_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataTextColumn FieldName="Specification" 
                                                            Width="200px" Caption="Specification" VisibleIndex="3">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Specification">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn FieldName="UoM" 
                                                            Width="200px" Caption="UoM" VisibleIndex="4">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="UoM">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>
                                        <EditFormSettings Visible="False" />
                                        
                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>  

                                         <dx:GridViewDataComboBoxColumn FieldName="UoM" VisibleIndex="5" Width="0px" Caption="UoM">  
                                            <PropertiesComboBox DataSourceID="SqlDataSource1" TextField="Description" 
                                                ValueField="Description" Width="100px" TextFormatString="{0}" ClientInstanceName="UoM"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                     <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="100px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>   
                                                                                   
                                        </dx:GridViewDataComboBoxColumn>
                                         <dx:GridViewDataTextColumn Caption="Price" VisibleIndex="6" FieldName="Price"
                                            EditFormSettings-VisibleIndex="5"  >
                                            <PropertiesTextEdit Width="100px" MaxLength="12"  DisplayFormatString="#,###">
                                                <MaskSettings Mask="<0..1000000000g>"  IncludeLiterals="DecimalSymbol" />
                                                <Style HorizontalAlign="Right">
                                                </Style>
                                            </PropertiesTextEdit>

                                            <EditFormSettings VisibleIndex="6"></EditFormSettings>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                            <%--<Settings AllowAutoFilter="True" />--%>
                                        <CellStyle HorizontalAlign="Right" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="7" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="8" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="9" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="10" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        

                                    </Columns>
                                           <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                      
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                          
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
        </dx:ASPxGridView>
                                    <%--<SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>--%>
        </td>                 

        </tr>
    </div>
</asp:Content>

﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UploadIADGSparePart.aspx.vb" MasterPageFile="~/Site.Master"
Inherits="IAMI_EPIC2.UploadIADGSparePart" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cpMessage == "Please Select File Excel") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }


        //------------Upload ----------------------------
        function OnBtnUploadClick(s, e) {
            ProgressBar.SetPosition(0);
        }

        function Uploader_OnUploadStart() {
            ProgressBar.SetPosition(0);
        }

        function Uploader_OnFilesUploadComplete(args) {
            UpdateUploadButton();
            ProgressBar.SetPosition(100);
            //alert('3');
        }
        function OnUploadProgressChanged(s, e) {
            ProgressBar.SetPosition(e.progress);
            //alert('4');
        }
        function UpdateUploadButton() {
            BtnSubmit.SetEnabled(Uploader.GetText(0) != "");
            var a = Uploader.GetText();
            ProgressBar.SetPosition(0);
        }
        function OnUploadProgressChanged(s, e) {
            ProgressBar.SetPosition(e.progress);
            //alert('6');
        }
        function Validation() {
            if (Uploader.GetText() == '') {
                toastr.warning('Please Select Excel File','Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            if (cboProject.GetText() == '') {
                toastr.warning('Please Select Project Name');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else {
                popUp.Show();
            }
        }
    </script>
    <style type="text/css">
.test {
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none;
}
.spinner {
  margin: 100px auto;
  width: 200px;
  height: 80px;
  text-align: center;
  font-size: 20px;
}

.spinner > div {
  background-color: #fff;
  height: 100%;
  width: 6px;
  display: inline-block;
  
  -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
  animation: sk-stretchdelay 1.2s infinite ease-in-out;
}

.spinner .rect2 {
  -webkit-animation-delay: -1.1s;
  animation-delay: -1.1s;
}

.spinner .rect3 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

.spinner .rect4 {
  -webkit-animation-delay: -0.9s;
  animation-delay: -0.9s;
}

.spinner .rect5 {
  -webkit-animation-delay: -0.8s;
  animation-delay: -0.8s;
}

@-webkit-keyframes sk-stretchdelay {
  0%, 40%, 100% { -webkit-transform: scaleY(0.4) }  
  20% { -webkit-transform: scaleY(1.0) }
}

@keyframes sk-stretchdelay {
  0%, 40%, 100% { 
    transform: scaleY(0.4);
    -webkit-transform: scaleY(0.4);
  }  20% { 
    transform: scaleY(1.0);
    -webkit-transform: scaleY(1.0);
  }
}
</style>
<style type="text/css">
    .td-col-l
    {
        padding:0px 0px 0px 10px;
        width:100px;
    }
    .td-col-m
    {
        width:10px;
    }
    .td-col-r
    {
        width:200px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
            <tr style="height:10px">
                <td class="td-col-l"></td>
                <td class="td-col-m"></td>
                <td class="td-col-r"></td>
                <td ></td>
                <td></td>
            </tr>
            <tr style="height:25px">
                <td style="padding:0px 0px 0px 10px" class="td-col-l">
                    <dx:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Project Name">
                    </dx:aspxlabel>
                </td>
                <td class="td-col-m"></td>
                <td class="td-col-r">
                    <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                        Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Project_Name"
                        ValueField="Project_ID" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                        DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                        Height="25px">
                        <Columns>
                            <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                            <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td colspan="2">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select Project_ID, Project_Name From VW_UploadIADGSparePart_S">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr style="height:20px">
                <td colspan="5"></td>
            </tr>
            
        </table>
        <table style="width: 100%; border: thin ridge #9598A1">
            <tr>
                <td width="100px" align="left">
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="*FILE" Font-Names="Tahoma" Font-Size="8pt"
                        Width="100">
                    </dx:ASPxLabel>
                </td>
                <td width="800px" align="left">
                    <dx:ASPxUploadControl ID="Uploader" runat="server" Width="100%" Font-Names="Verdana"
                        Font-Size="8pt" ClientInstanceName="Uploader" ShowClearFileSelectionButton="False"
                        NullText="Click here to browse files...">
                        <ClientSideEvents UploadingProgressChanged="OnUploadProgressChanged" FilesUploadComplete="function(s, e) { Uploader_OnFilesUploadComplete(e); }"
                            FileUploadComplete="function(s, e) { Uploader_OnFileUploadComplete(e); }" FileUploadStart="function(s, e) { Uploader_OnUploadStart(); }"
                            TextChanged="function(s, e) { UpdateUploadButton(); }" />
                        <ValidationSettings AllowedFileExtensions=".xls,.xlsx" />
                        <BrowseButton Text="...">
                        </BrowseButton>
                        <BrowseButtonStyle Paddings-Padding="3px">
                        </BrowseButtonStyle>
                    </dx:ASPxUploadControl>
                </td>
                <td align="right" width="100px">
                    <dx:ASPxButton ID="BtnSubmit" runat="server" Text="Upload" Font-Names="Tahoma" Width="85px"
                        Font-Size="8pt" TabIndex="2" AutoPostBack="False" UseSubmitBehavior="False" ClientInstanceName="BtnSubmit">
                        <ClientSideEvents Click="Validation" />
                    </dx:ASPxButton>
                </td>
                <td align="center" width="90">
                    <dx:ASPxButton ID="btnClear" runat="server" Text="Clear Log" Font-Names="Tahoma"
                        Width="85px" Font-Size="8pt" TabIndex="3" AutoPostBack="False" UseSubmitBehavior="False"
                        ClientInstanceName="btnClear">
                    </dx:ASPxButton>
                </td>
                <td align="left" width="100">
                    <dx:ASPxButton ID="BtnSaveError" runat="server" Text="Save Log " Font-Names="Tahoma"
                        Width="85px" Font-Size="8pt" TabIndex="4" AutoPostBack="False" UseSubmitBehavior="False"
                        ClientInstanceName="BtnSaveError">
                    </dx:ASPxButton>
                </td>
                <td align="left">
                    &nbsp;
                </td>
            </tr>
        </table>
        <table style="width: 100%; border: thin ridge #9598A1">
            <tr>
                <td class="style1" bgcolor="#99CCFF" align="left">
                    <dx:ASPxProgressBar ID="ProgressBar" runat="server" Width="100%" ClientInstanceName="ProgressBar"
                        Font-Names="verdana" Theme="Office2010Silver">
                    </dx:ASPxProgressBar>
                </td>
            </tr>
        </table>
        <table style="width: 100%; border: thin ridge #9598A1">
            <tr>
                <td class="style1" bgcolor="#99CCFF" align="left">
                    <dx:ASPxMemo ID="MemoMessage" runat="server" Height="320px" Width="100%">
                    </dx:ASPxMemo>
                </td>
            </tr>
        </table>
        <table style="width: 100%; border: thin ridge #9598A1">
            <tr>
                <td valign="top" align="left">
                    &nbsp;
                </td>
                <td class="style38" align="right">
                    &nbsp;
                </td>
                <td width="85px" align="right">
                    &nbsp;
                </td>
                <td width="85px" align="right">
                    &nbsp;
                </td>
                <td width="85px" align="right">
                    &nbsp;
                </td>
                <td align="right" width="85px">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <div style="padding :0px 0px 0px 10px">
          <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
                        <%--<ClientSideEvents    alert('1'); EndCallback="MessageBox"  alert('2'); />--%>
                        <ClientSideEvents EndCallback="OnEndCallback" />
                    </dx:ASPxCallback>

    </div>
    <dx:ASPxPopupControl ID="popUp" runat="server"   Modal="True" CssClass="test"
      ClientInstanceName="popUp"  PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
     ModalBackgroundStyle-BackColor="Black"  AllowDragging="false" ShowHeader="false"
        PopupAnimationType="Fade" EnableViewState="False" Width="200px" BackColor="Transparent" >
       
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                <dx:ASPxPanel ID="Aspxpanel2" runat="server" DefaultButton="btOK" Width="100%">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent" runat="server" Width="100%">
                           <div class="spinner">
                              <div class="rect1"></div>
                              <div class="rect2"></div>
                              <div class="rect3"></div>
                              <div class="rect4"></div>
                              <div class="rect5"></div>
                            </div>
                            <br /><br />
                            <div style="color:White; text-align:center; font-size:17px; font-weight:700;">
                                LOADING
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <Border BorderColor="Transparent"></Border>
    </dx:ASPxPopupControl>



</asp:Content>
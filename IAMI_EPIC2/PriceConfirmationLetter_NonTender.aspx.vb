﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class PriceConfirmationLetter_NonTender
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim TransactionNumber As String = ""
        Dim Supplier As String = ""

        TransactionNumber = Split(Request.QueryString("ID"), "|")(0)
        Supplier = Split(Request.QueryString("ID"), "|")(1)

        up_LoadReport(TransactionNumber, Supplier)
        ' up_LoadReport_Resume()
    End Sub

    Private Sub up_LoadReport(TransactionNumber As String, Supplier As String)
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptPriceConfirmationLetter_NonTender

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_PriceConfirmationLetter_NonTender '" & TransactionNumber & "' , '" & Supplier & "'"

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "PriceConfirmationLetterNonTender_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report
                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub
End Class
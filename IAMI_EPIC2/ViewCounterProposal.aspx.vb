﻿Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewCounterProposal
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"
    Private Sub LoadReport()
        Dim ls_SQL As String = ""
        Dim CPNumber As String = "", CE_Number As String = ""
        Dim ds As New DataSet
        Dim Report As New rptCP_Tender

        Try
            ASPxDocumentViewer1.Report = Nothing

            Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                sqlConn.Open()

                If Session("CP_calledFrom") = "E010" Then
                    'E010_paramViewCP sort index : 0 (CPNo), 1 (CENo) , 2 (revNo)
                    ls_SQL = "exec sp_CP_CounterProposalReport '" & Split(Session("E010_paramViewCP"), "|")(0) & "','" & Split(Session("E010_paramViewCP"), "|")(3) & "','" & Split(Session("E010_paramViewCP"), "|")(1) & "'" & vbCrLf
                ElseIf Session("CP_calledFrom") = "E020" Then
                    ls_SQL = "exec sp_CP_CounterProposalReport '" & Split(Session("E020_paramViewCP"), "|")(0) & "','" & Split(Session("E020_paramViewCP"), "|")(1) & "','" & Split(Session("E020_paramViewCP"), "|")(2) & "'" & vbCrLf
                ElseIf Session("CP_calledFrom") = "E040" Then
                    ls_SQL = "exec sp_CP_CounterProposalReport '" & Split(Session("E040_paramViewCP"), "|")(0) & "','" & Split(Session("E040_paramViewCP"), "|")(1) & "','" & Split(Session("E040_paramViewCP"), "|")(2) & "'" & vbCrLf
                End If

                Dim da As New SqlDataAdapter(ls_SQL, sqlConn)
                da.Fill(ds)
                Report.DataSource = ds.Tables(0)

                Report.Name = "CounterProposalTender_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report
                ASPxDocumentViewer1.DataBind()
            End Using

        Catch ex As Exception
            ASPxDocumentViewer1.Report = Nothing
            ASPxDocumentViewer1.DataBind()
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Master.SiteTitle = "COUNTER PROPOSAL LIST (TENDER) REPORT"
            pUser = Session("user")
            If Session("CP_calledFrom") = "E010" Then
                AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E010")
            ElseIf Session("CP_calledFrom") = "E020" Then
                AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E020")
            ElseIf Session("CP_calledFrom") = "E040" Then
                AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E040")
            End If

            Call LoadReport()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Select Case Session("CP_calledFrom")
            Case "E010"
                Session.Remove("CP_calledFrom")
                If IsNothing(Session("E010_QueryString")) = True Then
                    If IsNothing(Session("E010_paramViewCP")) = True Then
                        Response.Redirect("CPListDetail.aspx")
                    Else
                        Response.Redirect("CPListDetail.aspx?" & Session("E010_paramViewCP"))
                    End If

                Else
                    Response.Redirect("CPListDetail.aspx?" & Session("E010_QueryString"))
                End If

            Case "E020"
                Session.Remove("CP_calledFrom")
                If IsNothing(Session("E020_QueryString")) = True Then
                    Response.Redirect("CPApprovalDetail.aspx")
                Else
                    Response.Redirect("CPApprovalDetail.aspx?" & Session("E020_QueryString"))
                End If

            Case "E040"
                Session.Remove("CP_calledFrom")
                If IsNothing(Session("E040_QueryString")) = True Then
                    Response.Redirect("CPAcceptanceDetail.aspx")
                Else
                    Response.Redirect("CPAcceptanceDetail.aspx?" & Session("E040_QueryString"))
                End If
        End Select

        Session.Remove("CP_calledFrom")
    End Sub
#End Region

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SupplierRecomendList.aspx.vb" Inherits="IAMI_EPIC2.SupplierRecomendList_aspx" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">

    function GetMessage(s,e) {
        if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
          //  btnExcel.SetEnabled(false);
        }
        else if (s.cpMessage == "Grid Load Data Success") {
            btnExcel.SetEnabled(true);
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
    }

    function FillComboSupplier() {
        cboSupplier.PerformCallback();
    }

    function LoadComboSupplier() {
        cboSupplier.PerformCallback();
    }

   

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    

    <div style="padding: 5px 5px 5px 5px">
  <table style="width: 100%; border: 1px solid black; height: 100px;">  
         <tr style="height: 20px">
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="SR Date Range">
                    </dx1:aspxlabel>                 
                </td>
                <td style="width:30px">&nbsp;</td>
                <td style="width:120px">
                <dx:aspxdateedit ID="dtDateFrom" runat="server" Theme="Office2010Black" 
                                        Width="100px" AutoPostBack="false" ClientInstanceName="dtDateFrom"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:aspxdateedit>                       </td>
                <td style="width:30px">   
                    
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="To">
                    </dx1:aspxlabel>   
                                                     
                </td>
                <td style="width:100px">
                <dx:aspxdateedit ID="dtDateTo" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateTo"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                    </dx:aspxdateedit>   
                    
                </td>
                             <td></td>
                <td></td>
                <td></td>
            </tr>      
         <tr style="height: 40px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Supplier">
                    </dx1:ASPxLabel>                 
                </td>
                <td></td>
                <td colspan="6">           
  <dx1:ASPxComboBox ID="cboSupplier" runat="server" ClientInstanceName="cboSupplier"
                            Width="200px" Font-Names="Segoe UI"  TextField="Code"
                            ValueField="Code" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" DataSourceID="SqlDataSource2">                            
                            <ClientSideEvents Init="LoadComboSupplier" SelectedIndexChanged="function(s, e) {
	                            Grid.PerformCallback('gridload|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|' + 'xxx');
                                
                            }" />
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                             <dx:ListBoxColumn Caption="Supplier Name" FieldName="Name" Width="100px" />
                       
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
            </tr>        
      <tr style="height: 10px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
      <tr style="height: 30px">
                <td colspan="8" style=" padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnRefresh" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {

                            var startdate = dtDateFrom.GetDate(); // get value dateline date
                            var enddate = dtDateTo.GetDate(); // get value dateline date
                          
                            if (startdate == null)
                            {
                                toastr.warning('Please input valid Start Date', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;                                
                            }
                            else if (enddate == null)
                            {
                                toastr.warning('Please input valid End Date', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;                                
                            }


                           

                                                            
	                            Grid.PerformCallback('gridload|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|' + cboSupplier.GetValue());
                                //cbEnabled.PerformCallback('active|');
                            }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                     &nbsp;
                    <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnExcel" Theme="Default">                        
                        <ClientSideEvents Init="function(s, e) {
	btnExcel.SetEnabled(true);
}" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 &nbsp;
                    <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnAdd" Theme="Default">                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 </td>
            </tr>                                              
      <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select RFQ_Number From RFQ_Master">
                    </asp:SqlDataSource>

                    <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
                        SelectCommand="select 'ALL' Code,'ALL' [Name]  UNION ALL Select Rtrim(Supplier_Code) Code, Rtrim(Supplier_Name) [Name]  From Mst_Supplier">
                    </asp:SqlDataSource>
                         <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                </td>
                <td>&nbsp;</td>
            </tr>                                              
        </table>
    </div>


    <div style="padding: 5px 5px 5px 5px">
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="SR_Number;Rev;CP_Number" >
             <ClientSideEvents CustomButtonClick="function(s, e) {
	                            if(e.buttonID == 'edit'){                     
                                        var rowKey = Grid.GetRowKey(e.visibleIndex);  
                                                            
	                                    window.location.href= 'SupplierRecomendListDetail.aspx?ID=' + rowKey;
                                    }
                                }" EndCallback="GetMessage"  />
             <Columns>
                 <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" FixedStyle="Left">
                     <CustomButtons>
                         <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                         </dx:GridViewCommandColumnCustomButton>
                     </CustomButtons>
                 </dx:GridViewCommandColumn>
                 <dx:GridViewDataTextColumn Caption="Supplier Recommendation Number" FieldName="SR_Number" FixedStyle="Left"
                     VisibleIndex="1" Width="230px">
                     <Settings AutoFilterCondition="Contains" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Revision" FieldName="Rev" VisibleIndex="2" FixedStyle="Left"
                 Width="120px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Supplier Recommendation Date" FieldName="SR_Date" FixedStyle="Left"
                     VisibleIndex="3" Width="230px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CP Number" FieldName="CP_Number" 
                     VisibleIndex="5" Width="270px">
                     <Settings AutoFilterCondition="Contains" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Supplier Name" FieldName="Supplier_Recomm" 
                     VisibleIndex="5"  Width="270px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Status" FieldName="SR_Status" 
                     VisibleIndex="5"  Width="270px">
                 </dx:GridViewDataTextColumn>
                      <dx:GridViewBandColumn VisibleIndex="6">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="User_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="User_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="User_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>
                      <dx:GridViewBandColumn VisibleIndex="7">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="CostControl_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="CostControl_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="CostControl_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>

                 <dx:GridViewBandColumn VisibleIndex="8">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="AccountDept_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="AccountDept_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="AccountDept_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>
                 <dx:GridViewBandColumn VisibleIndex="9">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="GA_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="GA_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="GA_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>

             </Columns>
                    <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
             
                    <Settings ShowFilterRow="True" VerticalScrollableHeight="300" 
                HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>

         
                 <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
</EditFormColumnCaption>
                 </Styles>
        
        </dx:ASPxGridView>


        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 0px; height: 100px;">  
                  <tr style="height: 20px">
                    <td style=" padding:0px 0px 0px 10px; width:120px">
                    
                        &nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>  
                  <tr style="height: 20px">
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>  
                  <tr style="height: 20px">
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>  
                  <tr style="height: 20px">
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>  
            </table>
        </div>

    </div>


</div>
</asp:Content>

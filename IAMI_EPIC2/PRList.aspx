﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PRList.aspx.vb" Inherits="IAMI_EPIC2.PRList" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function MessageBox(s, e) {
            //alert(s.cpmessage);
            if (s.cbMessage == "Download Excel Successfully") {
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cbMessage == "Request Date To must be higher than Request Date From") {
                toastr.success(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function ItemCodeValidation(s, e) {
            if (ItemCode.GetValue() == null) {
                e.isValid = false;
            }
        }

        function GridLoad() {
            Grid.PerformCallback('refresh');
            tmpDepartment.SetText(cboDepartment.GetValue());
            tmpSection.SetText(cboSection.GetValue());                
        }

        function FilterSection() {
            cboSection.PerformCallback('filter|' + cboDepartment.GetValue());
        }
        function SetCode(s, e) {
            tmpDepartment.SetText(cboDepartment.GetValue());
            tmpSection.SetText(cboSection.GetValue());
        }

        function OnComboBoxKeyDown(s, e) {
            // 'Delete' button key code = 46

            if (e.htmlEvent.keyCode == 46 || e.htmlEvent.keyCode == 8)
                s.SetSelectedIndex(0);
            //alert(s.GetValue());
        }

        function OnComboBoxKeyDownPR(s, e) {
            // 'Delete' button key code = 46

            if (e.htmlEvent.keyCode == 46 || e.htmlEvent.keyCode == 8)
                s.SetSelectedIndex(0);
            //alert(s.GetValue());
        }

    </script> 
    <style type="text/css">        
        .hidden-div
        {
            display:none;
        }
        .style1
        {
            height: 30px;
        }
        .style2
        {
            height: 50px;
        }
        .style3
        {
            height: 20px;
        }
    </style>       
    <style type="text/css">
        .tr-all-height
        {
              height:10px;
        }
        
        .td-col-left 
        {
            width:100px;
            padding:0px 0px 0px 10px;
            
        }
        
        .td-col-left2 
        {
            width:140px;
        }
        
        
        .td-col-mid
        {
            width:10px;
            
        }
        .td-col-right
        {
            width:200px;
          
        }
        .td-col-free
        {
            width:20px;
            
        }
        
        .div-pad-fil
        {
            padding: 5px 5px 5px 5px;
        }
        
        .td-margin
        {
            padding:10px;
        }
        
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="div-pad-fil">
        <div class="div-pad-fil">
            <table style="width:100%; border: 1px solid black; height:100px">
                <tr style="height:20px">
                    <td class="td-col-left"></td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-left2" colspan="3">
                       
                    </td>
                    
                    <td class="td-col-free"></td>
                    <td class="td-col-left"></td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-right"></td>

                     <td class="td-col-free"></td>
                    <td class="td-col-left"></td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-right"></td>
                </tr>
                <tr class="tr-all-height; ">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="PR Date">
                        </dx1:ASPxLabel>  
                    </td>
                    <td class="td-col-mid">&nbsp;</td>
                    <td class="td-col-left2" >
                        <dx:ASPxDateEdit ID="ReqDate_From" runat="server" Theme="Office2010Black" 
                            Width="120px" ClientInstanceName="ReqDate_From"
                            EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                            Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                            <CalendarProperties>
                                <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
    <Paddings Padding="5px"></Paddings>
                                </HeaderStyle>
                                <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
    <Paddings Padding="5px"></Paddings>
                                </DayStyle>
                                <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
    <Paddings Padding="5px"></Paddings>
                                </WeekNumberStyle>
                                <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
    <Paddings Padding="10px"></Paddings>
                                </FooterStyle>
                                <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
    <Paddings Padding="10px"></Paddings>
                                </ButtonStyle>
                            </CalendarProperties>
                            <ClientSideEvents ValueChanged="GridLoad" />
                            <ButtonStyle Width="5px" Paddings-Padding="4px" >
    <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxDateEdit>
                         
                    </td>
                    <td class="td-col-mid">
                       <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="~">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-right">
                        <dx:ASPxDateEdit ID="ReqDate_To" runat="server" Theme="Office2010Black" 
                            Width="120px" ClientInstanceName="ReqDate_To"
                            EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                            Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                            <CalendarProperties>
                                <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
    <Paddings Padding="5px"></Paddings>
                                </HeaderStyle>
                                <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
    <Paddings Padding="5px"></Paddings>
                                </DayStyle>
                                <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
    <Paddings Padding="5px"></Paddings>
                                </WeekNumberStyle>
                                <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
    <Paddings Padding="10px"></Paddings>
                                </FooterStyle>
                                <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
    <Paddings Padding="10px"></Paddings>
                                </ButtonStyle>
                            </CalendarProperties>
                            <ClientSideEvents ValueChanged="GridLoad" />
                            <ButtonStyle Width="5px" Paddings-Padding="4px" >
    <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxDateEdit>
                    </td>

                    <td class="td-col-free"></td>
                    
                    <td class="td-col-left">
                       <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="Department">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">&nbsp;</td>
                    <td class="td-col-right">
                          <dx1:ASPxComboBox ID="cboDepartment" runat="server" ClientInstanceName="cboDepartment"
                        Width="153px" Font-Names="Segoe UI" TextField="Description"
                        ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDownList" 
                        IncrementalFilteringMode="Contains"
                        Height="22px">                            
                        <ClientSideEvents SelectedIndexChanged="GridLoad
tmpDepartment.SetText(cboDepartment.GetValue());" 
                            ValueChanged="FilterSection" KeyDown="OnComboBoxKeyDown" />
<ClientSideEvents SelectedIndexChanged="GridLoad" ValueChanged="FilterSection" init="function(s, e) {
	cbTmp.PerformCallback('Code');
}"></ClientSideEvents>
                        <Columns>            
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                        </Columns>
                                <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx1:ASPxComboBox>
           
                    </td>
                    
                    <td class="td-col-free"></td>
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="Status PR">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">&nbsp;</td>
                    <td class="td-col-right">
                         <dx1:ASPxComboBox ID="cboStatusPR" runat="server" ClientInstanceName="cboStatusPR"
                        Width="120px" Font-Names="Segoe UI" TextFormatString="{1}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDownList" 
                        IncrementalFilteringMode="Contains"  
                        Height="25px">                                                    
                        <ClientSideEvents SelectedIndexChanged="GridLoad" KeyDown="OnComboBoxKeyDown"/>
                        <Items>
                            <dx:ListEditItem Text="ALL" Value="6" />
                            <dx:ListEditItem Text="Draft" Value="0" />
                            <dx:ListEditItem Text="Submit" Value="1" />
                            <dx:ListEditItem Text="Approved" Value="2" />
                            <dx:ListEditItem Text="Accept" Value="3" />
                            <dx:ListEditItem Text="Reject" Value="4" />
                            <dx:ListEditItem Text="Assignment" Value="5" />
                        </Items>  
                        <ItemStyle Height="10px" Paddings-Padding="4px" >

<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>                              
                        </dx1:ASPxComboBox>
                    </td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left; td-margin">
                         <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="PR Type">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">&nbsp;</td>
                    <td class="td-col-left2">
                        <dx1:ASPxComboBox ID="cboPRType" runat="server" ClientInstanceName="cboPRType"
                            Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDownList" 
                            IncrementalFilteringMode="Contains" Height="25px">                            
                            <ClientSideEvents SelectedIndexChanged="GridLoad" KeyDown="OnComboBoxKeyDownPR" />
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                    <ItemStyle Height="10px" Paddings-Padding="4px" >

<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>

                    <td class="td-col-free"></td>
                    <td class="td-col-left2"></td>
                    <td class="td-col-mid"></td>
                   
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="Section">
                        </dx1:ASPxLabel>                 
                    </td>
                    <td class="td-col-mid">&nbsp;</td>
                    <td class="td-col-right">
                        <dx1:ASPxComboBox ID="cboSection" runat="server" ClientInstanceName="cboSection"
                        Width="153px" Font-Names="Segoe UI" TextField="Description"
                        ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDownList" 
                        IncrementalFilteringMode="Contains" Height="22px">                            
                        <ClientSideEvents SelectedIndexChanged="GridLoad" KeyDown="OnComboBoxKeyDown"/>
                        <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="180px" />
                        </Columns>
                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                        <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                        <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td class="td-col-free"></td>
                    <td class="td-col-left"></td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-right"></td>
                   
                    
                </tr>
                <tr>
                    <td  colspan="13" style="height:30px; padding: 0px 0px 10px 10px; ">
                        <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShowData" Theme="Default">                        
                            <ClientSideEvents Click="function(s, e) {
								var startdate = ReqDate_From.GetDate(); // get value dateline date
var enddate = ReqDate_To.GetDate(); // get value dateline date

if (startdate == null)
{
	toastr.warning('Please input valid PR Start Date', 'Warning');
	toastr.options.closeButton = false;
	toastr.options.debug = false;
	toastr.options.newestOnTop = false;
	toastr.options.progressBar = false;
	toastr.options.preventDuplicates = true;
	toastr.options.onclick = null;
	e.processOnServer = false;
	return;                                
}
else if (enddate == null)
{
	toastr.warning('Please input valid PR End Date', 'Warning');
	toastr.options.closeButton = false;
	toastr.options.debug = false;
	toastr.options.newestOnTop = false;
	toastr.options.progressBar = false;
	toastr.options.preventDuplicates = true;
	toastr.options.onclick = null;
	e.processOnServer = false;
	return;                                
}
                                
                                var yearstartdate = startdate.getFullYear(); // where getFullYear returns the year (four digits)
                                var monthstartdate = startdate.getMonth(); // where getMonth returns the month (from 0-11)
                                var daystartdate = startdate.getDate();   // where getDate returns the day of the month (from 1-31)

                                if (daystartdate &lt; 10) {
                                    daystartdate = '0'+ daystartdate  
                                }

                                if (monthstartdate &lt; 10) {
                                    monthstartdate = '0'+ monthstartdate  
                                }

                                var vStartDate = yearstartdate + '-' + monthstartdate + '-' + daystartdate;

                                
                                var yearenddate = enddate.getFullYear(); // where getFullYear returns the year (four digits)
                                var monthenddate = enddate.getMonth(); // where getMonth returns the month (from 0-11)
                                var dayenddate = enddate.getDate();   // where getDate returns the day of the month (from 1-31)

                                if (dayenddate &lt; 10) {
                                    dayenddate = '0'+ dayenddate  
                                }

                                if (monthenddate &lt; 10) {
                                    monthenddate = '0'+ monthenddate  
                                }

                                var vEndDate = yearenddate + '-' + monthenddate + '-' + dayenddate;

                                if (vEndDate &lt; vStartDate) {
                                    toastr.warning('PR End date must be bigger than PR Start Date', 'Warning');
                                    toastr.options.closeButton = false;
                                    toastr.options.debug = false;
                                    toastr.options.newestOnTop = false;
                                    toastr.options.progressBar = false;
                                    toastr.options.preventDuplicates = true;
                                    toastr.options.onclick = null;
                                    e.processOnServer = false;
                                    return;
                                }
	
	                            Grid.PerformCallback('showdata');
                            }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                     &nbsp;


                        <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnDownload" Theme="Default">                        
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    &nbsp;


                        <dx:ASPxButton ID="btnAdd" runat="server" Text="New PR" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnAdd" Theme="Default">                        
                            <ClientSideEvents Click="function(s, e) {
	cbMessage.PerformCallback('');
}" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                   
                </tr>
                <tr style="display:none">
                    <td colspan="7" style="height:30px">
                        <dx:ASPxTextBox ID="tmpDepartment" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                            Width="170px" ClientInstanceName="tmpDepartment" MaxLength="15" 
                            Theme="Office2010Silver">
                        </dx:ASPxTextBox>
                      
                    </td>
                    <td colspan="6">
                        <dx:ASPxTextBox ID="tmpSection" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                            Width="170px" ClientInstanceName="tmpSection" MaxLength="15" 
                            Theme="Office2010Silver">
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                </table>    
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="select '00' Code,'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRType'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"                         
                        SelectCommand="select '00' Code,'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Department'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="select '00' Code,'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Section'">
                    </asp:SqlDataSource>
                    <dx:ASPxCallback ID="cbMessage" runat="server" 
                        ClientInstanceName="cbMessage">
                    <ClientSideEvents CallbackComplete="MessageBox" />                                           
                </dx:ASPxCallback> 
                <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
                    <ClientSideEvents CallbackComplete="SetCode" />
                </dx:ASPxCallback>                
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                </dx:ASPxGridViewExporter>
                  <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" 
                 Width="100%" OnAfterPerformCallback="Grid_AfterPerformCallback"
                Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="PR_Number;PR_Status;Rev" >
                      <ClientSideEvents CustomButtonClick="function(s, e) {
	              if(e.buttonID == 'edit'){
                     
                                    var rowKey = Grid.GetRowKey(e.visibleIndex);
                             
	                                window.location.href= 'PRListDetail.aspx?ID=' + rowKey;
                                }
    }" />
    <ClientSideEvents CustomButtonClick="function(s, e) {
	              if(e.buttonID == &#39;edit&#39;){
                     
                                    var rowKey = Grid.GetRowKey(e.visibleIndex);
                             
	                                window.location.href= &#39;PRListDetail.aspx?ID=&#39; + rowKey;
                                }
    }"></ClientSideEvents>
                      <Columns>
						  <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" FixedStyle="Left">
                              <CustomButtons>
                                  <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                                  </dx:GridViewCommandColumnCustomButton>
                              </CustomButtons>
                          </dx:GridViewCommandColumn>						 
                          <dx:GridViewDataTextColumn Caption="PR Number" VisibleIndex="1" FixedStyle="Left" 
                              FieldName="PR_Number" Width="170px">
                              <Settings AutoFilterCondition="Contains" />
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                          </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn Caption="Rev" FieldName="Rev" VisibleIndex="2"  FixedStyle="Left"
                              Width="50px">
                              <Settings AutoFilterCondition="Contains" />
                          </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn Caption="PR Date" VisibleIndex="3"  FixedStyle="Left"
                              FieldName="PR_Date" Width="90px">
                              <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                              <Settings AllowAutoFilter="False" />
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                          </dx:GridViewDataTextColumn>
						  <dx:GridViewDataTextColumn Caption="PR Register" VisibleIndex="4" width="150px"
                              FieldName="PR_Register">
                              <Settings AutoFilterCondition="Contains" />
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                          </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn Caption="PR Budget" VisibleIndex="5" 
                              FieldName="PRBudget">
                              <Settings AutoFilterCondition="Contains" />
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                          </dx:GridViewDataTextColumn>
                           
                          <dx:GridViewDataTextColumn Caption="PR Type" VisibleIndex="6" Width="130px"
                              FieldName="PRType">
                              <Settings AutoFilterCondition="Contains" />
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                          </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn Caption="Department" VisibleIndex="7" 
                              FieldName="Department">
                              <Settings AutoFilterCondition="Contains" />
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                          </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn Caption="Section" VisibleIndex="8" Width="150px"
                              FieldName="Section"> 
                              <Settings AutoFilterCondition="Contains" />
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                          </dx:GridViewDataTextColumn>
						   <dx:GridViewDataTextColumn Caption="Cost Center" FieldName="CostCenter" 
                              VisibleIndex="9">
                              <Settings AutoFilterCondition="Contains" />
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>                              
                          </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn Caption="Project" VisibleIndex="10" 
                              FieldName="Project">
                              <Settings AutoFilterCondition="Contains" />
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                          </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn Caption="Urgent" VisibleIndex="11" 
                              FieldName="Urgent_Status">
                                <Settings AutoFilterCondition="Contains" />
                                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                          </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn Caption="Req. PO Issue Date" FieldName="Req_POIssueDate" 
                              VisibleIndex="12">
                              <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>                              
                          </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn Caption="Tender" FieldName="Tender_Code" 
                              VisibleIndex="13">
                              <Settings AutoFilterCondition="Contains" />
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>                              
                          </dx:GridViewDataTextColumn>
                          <dx:GridViewDataTextColumn Caption="Status" FieldName="PR_Status" 
                              VisibleIndex="14">
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>                              
                          </dx:GridViewDataTextColumn>

                          <dx:GridViewBandColumn VisibleIndex="15">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Prepared By" VisibleIndex="0" 
                                      FieldName="Dept_PreparedBy">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Prepared Date" VisibleIndex="3" 
                                      FieldName="Dept_PreparedDate">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                                        </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="Dept_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="Dept_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Notes" FieldName="Dept_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn>
                          <dx:GridViewBandColumn VisibleIndex="16">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" VisibleIndex="0" 
                                      FieldName="GM_ApprovedBy">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" VisibleIndex="2" 
                                      FieldName="GM_ApprovedDate">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Notes" FieldName="GM_ApprovedNote" 
                                      VisibleIndex="3">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn>
                          <dx:GridViewBandColumn VisibleIndex="17">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" VisibleIndex="0" 
                                      FieldName="Director_ApprovedBy">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="ApprovedDate" VisibleIndex="2" 
                                      FieldName="Director_ApprovedDate">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="Director_ApprovedNote" 
                                      VisibleIndex="3">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn>
                       <%--   <dx:GridViewBandColumn VisibleIndex="18">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Received By" FieldName="CostControl_ApprovedBy" 
                                      VisibleIndex="0">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Received Date" FieldName="CostControl_ApprovedDate" 
                                      VisibleIndex="1">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="CostControl_ApprovedNote" 
                                      VisibleIndex="2">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn> --%>
                          <dx:GridViewBandColumn Caption="Acceptance" VisibleIndex="19">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="Name" FieldName="Acceptance_ApprovedBy" 
                                          VisibleIndex="0">
                                          <HeaderStyle HorizontalAlign="Center" />
                                          <Settings AllowAutoFilter="False" />
                                      </dx:GridViewDataTextColumn>
                                      <dx:GridViewDataTextColumn Caption="Date" FieldName="Acceptance_ApprovedDate" 
                                          VisibleIndex="1">
                                          <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                                            </PropertiesTextEdit>
                                          <HeaderStyle HorizontalAlign="Center" />
                                          <Settings AllowAutoFilter="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Notes" FieldName="Acceptance_Note" 
                                      VisibleIndex="2">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                </Columns>
                          </dx:GridViewBandColumn>   
                          <dx:GridViewDataTextColumn Caption="PIC" FieldName="PIC_Assign" 
                              VisibleIndex="20">
                              <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>                              
                          </dx:GridViewDataTextColumn>                      
                      </Columns>
                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                    <Settings ShowFilterRow="True" VerticalScrollableHeight="300" 
                    HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>

                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                        <Header HorizontalAlign="Center">
                            <Paddings Padding="2px"></Paddings>
                        </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
</EditFormColumnCaption>
                    </Styles>
                </dx:ASPxGridView>
        </div>
    
    </div>
</asp:Content>

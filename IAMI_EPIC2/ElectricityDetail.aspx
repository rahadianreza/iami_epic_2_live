﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ElectricityDetail.aspx.vb" Inherits="IAMI_EPIC2.ElectricityDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function MessageError(s, e) {
            if (s.cpMessage == "Data Saved Successfully!") {
                
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                cboRateClass.SetEnabled(false);
                cboVAKind.SetEnabled(false);
                Period.SetEnabled(false);
            }
            else if (s.cpMessage == "Data Updated Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                cboRateClass.SetEnabled(false);
                cboVAKind.SetEnabled(false);
                Period.SetEnabled(false);
//                millisecondsToWait = 2000;
//                setTimeout(function () {
//                    var pathArray = window.location.pathname.split('/');
//                    var url = window.location.origin + '/' + pathArray[1] + '/PRJList.aspx';
//                }, millisecondsToWait);
            }
            else if (s.cpMessage == "Data Clear Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Cancel Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function MessageBox(s, e) {
            //alert(s.cpmessage);
            if (s.cbMessage == "Download Excel Successfully") {
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        //popup calender 
        function Period_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            Period.SetDate(myDate);
        }

        function Period_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();
              
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }
        }

    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        .style1
        {
            width: 20px;
        }
        .style2
        {
            width: 78px;
        }
        .style3
        {
            width: 79px;
        }
        .style4
        {
            width: 68px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr style="height: 25px">
            <td style="width:50px">&nbsp;</td>
            <td style="width: 180px; padding-left: 40px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Rate Class">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="padding-top: 15px;" class="style2" colspan="8">

                        <dx:ASPxComboBox ID="cboRateClass" runat="server" ClientInstanceName="cboRateClass" Width="120px"
                            Font-Names="Segoe UI" TextField="Description" ValueField="Code" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3" DataSourceID="SqlDataSource1">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboVAKind.PerformCallback(cboRateClass.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Rate Class" FieldName="Code" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                 
            </td>
          <td></td>
        </tr>
        <tr style="height: 25px">
            <td>&nbsp;</td>
            <td style="width: 180px; padding-left: 40px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="VA Kind">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="padding-top: 10px;" class="style2" colspan="8">
                        <dx:ASPxComboBox ID="cboVAKind" runat="server" ClientInstanceName="cboVAKind" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Par_Description" ValueField="Par_Code"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="VAKind" FieldName="Par_Description" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
          <td></td>
        </tr>
        <tr style="height: 25px">
             <td>&nbsp;</td>
            <td style="width: 120px; padding-left: 40px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="padding-top: 10px;" class="style2" colspan="8">
                <dx:ASPxDateEdit ID="Period" runat="server" ClientInstanceName="Period" EnableTheming="True"
                    ShowShadow="false" Font-Names="Segoe UI" Theme="Office2010Black" Font-Size="9pt"
                    Height="22px" DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy" Width="120px">
                    <ClientSideEvents DropDown="Period_OnDropDown" Init="Period_OnInit" />
                </dx:ASPxDateEdit>
            </td>
           <td></td>
        </tr>
        <tr style="height: 25px">
            <td>&nbsp;</td>
            <td style="width: 120px; padding-left: 40px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Reguler Post Paid">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="padding-top: 10px;" class="style2">
                <dx:ASPxTextBox ID="txtRegulerPostPaid" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtRegulerPostPaid" MaxLength="15" HorizontalAlign="Right" >
                    <MaskSettings Mask="<0..100000000g>.<00000..99>" />
                    <ValidationSettings Display="Dynamic"></ValidationSettings>
                </dx:ASPxTextBox>
                 
            </td>
           
            <td class="style1" style="padding-top: 10px ; padding-left:10px">
               <dx:ASPxLabel ID="lblKva" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="kVA">
                </dx:ASPxLabel>
            </td>
            <td class="style1">&nbsp;</td>
            <td style="padding-top: 10px;" class="style3">
                <dx:ASPxTextBox ID="txtRegulerPostPaidWBP" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtRegulerPostPaidWBP" MaxLength="15" HorizontalAlign="Right" >
                    <MaskSettings Mask="<0..100000000g>.<00000..99>" />
                    <ValidationSettings Display="Dynamic"></ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            
            <td  style="width:20px; padding-top: 10px ; padding-left:10px">
                <dx:ASPxLabel ID="lblWBP" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="WBP">
                </dx:ASPxLabel>
            </td>
            <td class="style1">&nbsp;</td>
            <td style="padding-top: 10px;" class="style4">
                <dx:ASPxTextBox ID="txtRegulerPostPaidLWBP" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtRegulerPostPaidLWBP" MaxLength="15" HorizontalAlign="Right" >
                    <MaskSettings Mask="<0..100000000g>.<00000..99>" />
                    <ValidationSettings Display="Dynamic"></ValidationSettings>
                </dx:ASPxTextBox>
            </td>
            <td style="width:20px; padding-top: 10px ; padding-left:10px">
                 <dx:ASPxLabel ID="lblLWBP" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="LWBP">
                </dx:ASPxLabel>
            </td>
            <td></td>
        </tr>
        <tr style="height: 25px">
            <td>&nbsp;</td>
            <td style="width: 120px; padding-left: 40px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Pre Post Paid">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="padding-top: 10px;" class="style2" colspan="8">
                <dx:ASPxTextBox ID="txtPrePostPaid" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtPrePostPaid" MaxLength="15" 
                    HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>.<00000..99>" />
                </dx:ASPxTextBox>
            </td>
            <td></td>
            
        </tr>
        <tr style="height: 25px">
            <td>&nbsp;</td>
            <td style="width: 120px; padding-left: 40px; padding-top: 10px;">
                &nbsp;</td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="padding-top: 10px;" colspan="9"> 
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack"
                    Theme="Default">
                    <Paddings Padding="2px" /> 
                </dx:ASPxButton> 
                &nbsp; &nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
                            cboRateClass.SetEnabled(true);
                            cboVAKind.SetEnabled(true);
                            Period.SetEnabled(true);
                            txtRegulerPostPaid.SetText(0.00);
                            txtRegulerPostPaidWBP.SetText(0.00);
                            txtRegulerPostPaidLWBP.SetText(0.00);
                            txtPrePostPaid.SetText(0.00);
                            cboRateClass.SetText('');
                            cboVAKind.ClearItems();
                        }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                  &nbsp;&nbsp;
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
           
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="height: 30px;" colspan="11">
                &nbsp;
            </td>
        </tr>
        <tr style="height: 25px;">
            <td colspan="12"></td>
        </tr>
    </table>
    <div>
         <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description, Nom = 1 From Mst_Parameter Where Par_Group = 'RateClass'">
        </asp:SqlDataSource>
                <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
                    <%--<ClientSideEvents CallbackComplete="SetCode" />--%>
                </dx:ASPxCallback>
                <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                    <ClientSideEvents Init="MessageError" />
                </dx:ASPxCallback>
                   <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
                    <ClientSideEvents Init="MessageError" />                                           
                </dx:ASPxCallback>
                   <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
                                                             
                </dx:ASPxCallback>
    </div>
</asp:Content>

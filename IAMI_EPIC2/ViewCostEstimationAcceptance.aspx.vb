﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewCostEstimationAcceptance
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        up_LoadReport()
    End Sub

    Private Sub up_LoadReport()
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptCostEstimation
        Dim pCENumber As String
        Dim pRev As Integer

        pCENumber = Session("CENumber")
        pRev = Session("Revision")

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_CE_CostEstimationReport '" & pCENumber & "' , " & pRev & ""

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "CostEstimationAcceptance_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report

                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim pCENumber As String
        Dim Rev As Integer
        Dim RFQSet As String

        pCENumber = Session("CENumber")
        Rev = Session("Revision")
        RFQSet = Session("RFQSet")
        Response.Redirect("~/CEAcceptanceUserDetail.aspx?ID=" & pCENumber & "|" & Rev & "|" & RFQSet)
    End Sub

End Class
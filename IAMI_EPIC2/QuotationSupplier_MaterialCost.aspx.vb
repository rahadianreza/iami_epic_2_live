﻿Imports DevExpress.XtraPrinting
Imports DevExpress.Web.ASPxGridView
Imports System.IO
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxClasses
Imports System.Drawing

Public Class QuotationSupplier_MaterialCost
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim pErr As String = ""
    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim py As String = ""
    Dim pty As String = ""
    Dim sDS As String = ""
    Dim pg As String = ""
    Dim supp As String = ""
    Dim grpCommodity As String = ""
    Dim pic As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("K010")
        Master.SiteTitle = "Quotation Supplier Material Cost"
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        prj = Request.QueryString("projectid")
        grp = Request.QueryString("groupid")
        comm = Request.QueryString("commodity")
        pty = Request.QueryString("projecttype")
        pg = Request.QueryString("page")
        supp = Request.QueryString("supplier").ToString()
        grpCommodity = Request.QueryString("groupcommodity")
        pic = Request.QueryString("pic")

        'If pty = "PT01" Then
        sdsCategoryMaterial.SelectCommand = "SELECT COALESCE(Par_Code,'') AS CategoryCode,COALESCE(Par_Description,'') Description FROM dbo.Mst_Parameter WHERE Par_Group='MaterialGroupItem' "
        'Else
        '    sdsCategoryMaterial.SelectCommand = "SELECT Par_Code as Category_Material, Par_Description as Category_Name FROM Mst_Parameter WHERE Par_Group = 'MaterialCategory' AND Par_Code <> 'PT01'"
        'End If

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then

            If Not String.IsNullOrEmpty(prj) Then
                Dim ds As DataSet = clsPRJListDB.getDetailGrid(prj, pUser, "")

                lblUSDRate.Text = Format(CDec(ds.Tables(0).Rows(0)("Rate_USD_IDR").ToString()), "#,##0.00")
                lblJPYRate.Text = Format(CDec(ds.Tables(0).Rows(0)("Rate_YEN_IDR").ToString()), "#,##0.00")
                lblTHBRate.Text = Format(CDec(ds.Tables(0).Rows(0)("Rate_BATH_IDR").ToString()), "#,##0.00")
            End If

            up_FillComboPartNo(cboPartNo, prj, grp, comm)
        End If

    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_Excel()
        up_GridLoad("1")

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})
        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ItemListSourcing" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub up_FillComboPartNo(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _projid As String, _grpid As String, _comm As String,
                           Optional ByRef pGroup As String = "", _
                           Optional ByRef pParent As String = "", _
                           Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsQuotationSupplierDB.fillComboPartNo(_projid, _grpid, _comm, grpCommodity, supp, pmsg)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsQuotationSupplierDB.getHeaderMaterialCost(prj, grp, comm, cboPartNo.Value, supp, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub
#End Region

#Region "Event Control"
    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles Grid.BeforeGetCallbackResult
        If Grid.IsNewRowEditing Then
            Grid.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            fRefresh = True
        Else
            up_GridLoad("1")
        End If
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles Grid.RowDeleting
        e.Cancel = True
      
        clsQuotationSupplierDB.DeleteQuotationSupplierMaterialCost(prj, grp, comm, cboPartNo.Value, e.Keys("Category_Material"), e.Keys("Material_Code"), supp, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
        End If
    End Sub

    Public Function checking(value As String) As Boolean
        If String.IsNullOrEmpty(value) Or value = "0" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub Grid_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles Grid.RowInserting
        e.Cancel = True

        Dim aFlag As String = 0

        If e.NewValues("AdjustmentType") = "F" Then
            If checking(prj) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
            ElseIf checking(grp) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
            ElseIf checking(comm) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Commodity cannot empty", 1)
            ElseIf checking(cboPartNo.Value) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Part No cannot empty", 1)
            ElseIf checking(e.NewValues("Category_Material")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Category Material cannot empty", 1)
            ElseIf checking(e.NewValues("Material_Code")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Material Code cannot empty", 1)
            ElseIf checking(e.NewValues("Local_Import")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Local Import cannot empty", 1)
            ElseIf checking(e.NewValues("Country")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Country cannot empty", 1)
            ElseIf checking(e.NewValues("Dimension_Thickness")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Thickness cannot empty", 1)
            ElseIf checking(e.NewValues("Dimension_Length")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Length cannot empty", 1)
            ElseIf checking(e.NewValues("Dimension_Width")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Width cannot empty", 1)
            ElseIf checking(e.NewValues("Dimension_Diameter")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Diameter cannot empty", 1)
            ElseIf checking(e.NewValues("Dimension_Density")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Density cannot empty", 1)

            ElseIf checking(e.NewValues("Dimension_FactorUnit")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Factor Unit cannot empty", 1)

            ElseIf checking(e.NewValues("Currency")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Currency cannot empty", 1)
            ElseIf checking(e.NewValues("Scrap_Weight")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Scrap Weight cannot empty", 1)
            ElseIf checking(e.NewValues("Scrap_Unit_Price")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Scrap Unit Price cannot empty", 1)
            Else
                aFlag = 1
            End If
        Else
            If checking(prj) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
            ElseIf checking(grp) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
            ElseIf checking(comm) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Commodity cannot empty", 1)
            ElseIf checking(cboPartNo.Value) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Part No cannot empty", 1)
            ElseIf checking(e.NewValues("Category_Material")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Category Material cannot empty", 1)
            ElseIf checking(e.NewValues("Material_Code")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Material Code cannot empty", 1)
            ElseIf checking(e.NewValues("Local_Import")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Local Import cannot empty", 1)
            ElseIf checking(e.NewValues("Country")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Country cannot empty", 1)
            ElseIf checking(e.NewValues("Amount")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Material Cost cannot empty", 1)
            Else
                aFlag = 1
            End If
        End If

        If aFlag = 1 Then
            clsQuotationSupplierDB.InsertQuotationSupplierMaterialCost(prj, grp, comm, cboPartNo.Value, supp, e.NewValues("Category_Material"), e.NewValues("Local_Import"), _
                                                                      e.NewValues("Country"), e.NewValues("Material_Code"), e.NewValues("Dimension_Thickness"), e.NewValues("Dimension_Length"), _
                                                                      e.NewValues("Dimension_Width"), e.NewValues("Dimension_Diameter"), e.NewValues("Dimension_Density"), e.NewValues("Dimension_FactorUnit"), e.NewValues("Weight"), e.NewValues("Qty_Usage"), e.NewValues("Currency"), e.NewValues("Rate"), _
                                                                      e.NewValues("Unit"), e.NewValues("Unit_Cost"), e.NewValues("Amount"), e.NewValues("Scrap_Weight"), e.NewValues("Scrap_Unit_Price"), grpCommodity, e.NewValues("AdjustmentType"), supp, pErr)
            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Insert data successfully!", 1)
            End If
        Else
            Grid.JSProperties("cp_AdjustmentType") = e.NewValues("AdjustmentType")
        End If
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        Dim aFlag As String = 0

        If e.NewValues("AdjustmentType") = "F" Then
            If checking(prj) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
            ElseIf checking(grp) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
            ElseIf checking(comm) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Commodity cannot empty", 1)
            ElseIf checking(cboPartNo.Value) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Part No cannot empty", 1)
            ElseIf checking(e.NewValues("Category_Material")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Category Material cannot empty", 1)
            ElseIf checking(e.NewValues("Material_Code")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Material Code cannot empty", 1)
            ElseIf checking(e.NewValues("Local_Import")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Local Import cannot empty", 1)
            ElseIf checking(e.NewValues("Country")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Country cannot empty", 1)
            ElseIf checking(e.NewValues("Dimension_Thickness")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Thickness cannot empty", 1)
            ElseIf checking(e.NewValues("Dimension_Length")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Length cannot empty", 1)
            ElseIf checking(e.NewValues("Dimension_Width")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Width cannot empty", 1)
            ElseIf checking(e.NewValues("Dimension_Diameter")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Diameter cannot empty", 1)
            ElseIf checking(e.NewValues("Dimension_Density")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Density cannot empty", 1)

            ElseIf checking(e.NewValues("Dimension_FactorUnit")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Dimension Factor Unit cannot empty", 1)

            ElseIf checking(e.NewValues("Currency")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Currency cannot empty", 1)
            ElseIf checking(e.NewValues("Scrap_Weight")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Scrap Weight cannot empty", 1)
            ElseIf checking(e.NewValues("Scrap_Unit_Price")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Scrap Unit Price cannot empty", 1)
            Else
                aFlag = 1
            End If
        Else
            If checking(prj) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
            ElseIf checking(grp) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
            ElseIf checking(comm) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Commodity cannot empty", 1)
            ElseIf checking(cboPartNo.Value) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Part No cannot empty", 1)
            ElseIf checking(e.NewValues("Category_Material")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Category Material cannot empty", 1)
            ElseIf checking(e.NewValues("Material_Code")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Material Code cannot empty", 1)
            ElseIf checking(e.NewValues("Local_Import")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Local Import cannot empty", 1)
            ElseIf checking(e.NewValues("Country")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Country cannot empty", 1)
            ElseIf checking(e.NewValues("Amount")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Material Cost cannot empty", 1)
            Else
                aFlag = 1
            End If
        End If

        If aFlag = 1 Then
            clsQuotationSupplierDB.UpdateQuotationSupplierMaterialCost(prj, grp, comm, cboPartNo.Value, supp, e.NewValues("Category_Material"), e.NewValues("Local_Import"), _
                                                              e.NewValues("Country"), e.NewValues("Material_Code"), e.NewValues("Dimension_Thickness"), e.NewValues("Dimension_Length"), _
                                                              e.NewValues("Dimension_Width"), e.NewValues("Dimension_Diameter"), e.NewValues("Dimension_Density"), e.NewValues("Dimension_FactorUnit"), e.NewValues("Weight"), e.NewValues("Qty_Usage"), e.NewValues("Currency"), e.NewValues("Rate"), _
                                                              e.NewValues("Unit"), e.NewValues("Unit_Cost"), e.NewValues("Amount"), e.NewValues("Scrap_Weight"), e.NewValues("Scrap_Unit_Price"), grpCommodity, e.NewValues("AdjustmentType"), supp, pErr)
            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
            End If
        Else
            Grid.JSProperties("cp_AdjustmentType") = e.NewValues("AdjustmentType")
        End If

    End Sub

    Private Sub Grid_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If Not Grid.IsNewRowEditing Then
            If e.Column.FieldName = "Material_Code" Or e.Column.FieldName = "Category_Material" Or e.Column.FieldName = "AdjustmentType" Then
                e.Editor.ClientEnabled = False
                e.Editor.ForeColor = Color.Silver
            End If
        End If

        Dim getAdjustmentType As String = Grid.GetRowValues(e.VisibleIndex, "AdjustmentType")

        If getAdjustmentType = "M" Then
            If e.Column.FieldName = "Dimension_Thickness" Or e.Column.FieldName = "Dimension_Length" Or e.Column.FieldName = "Dimension_Width" Or _
               e.Column.FieldName = "Dimension_Diameter" Or e.Column.FieldName = "Qty_Usage" Or e.Column.FieldName = "Rate" Or e.Column.FieldName = "Unit" Or _
               e.Column.FieldName = "Unit_Cost" Or e.Column.FieldName = "Scrap_Weight" Or e.Column.FieldName = "Scrap_Unit_Price" Then
                e.Editor.Visible = False
                e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                e.Column.EditFormSettings.Caption = ""
                e.Column.EditCellStyle.CssClass = "Hidden"
                e.Column.EditFormCaptionStyle.CssClass = "Hidden"

            End If
            If e.Column.FieldName = "Amount" Then
                e.Editor.Visible = True
                e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True
                e.Column.EditCellStyle.CssClass = "AmountVisible"
                e.Column.EditFormCaptionStyle.CssClass = "AmountVisible"
            End If
        End If

        Dim a As String = Grid.GetRowValues(e.VisibleIndex, "Category_Material")

        If (Not Grid.IsEditing) OrElse e.Column.FieldName <> "Material_Code" Then
            Return
        End If

        Dim val As Object = Grid.GetRowValuesByKeyValue(e.KeyValue, "Category_Material")
        If val Is DBNull.Value Then
            Return
        End If
        Dim category As String = CStr(val)

        Dim combo As ASPxComboBox = TryCast(e.Editor, ASPxComboBox)
        FillMaterialCode(combo, category)

        AddHandler combo.Callback, AddressOf cmbCode_OnCallback
    End Sub

    Protected Sub FillMaterialCode(ByVal cmb As ASPxComboBox, ByVal category As String)
        If String.IsNullOrEmpty(category) Then
            Return
        End If

        Dim cities As List(Of String) = GetCode(category)
        cmb.Items.Clear()

        Dim a As DataSet = clsQuotationSupplierDB.getMaterialCodeCombo(category, supp)
        cmb.DataSource = a
        cmb.DataBind()
    End Sub

    Private Function GetCode(ByVal category As String) As List(Of String)
        Dim a As DataSet = clsQuotationSupplierDB.getMaterialCodeCombo(category, supp)
        Return a.Tables(0).AsEnumerable().[Select](Function(r) r.Field(Of String)(0)).ToList()
    End Function

    Private Sub cmbCode_OnCallback(ByVal source As Object, ByVal e As CallbackEventArgsBase)
        FillMaterialCode(TryCast(source, ASPxComboBox), e.Parameter)
    End Sub

#End Region
    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        prj = Request.QueryString("projectid")
        grp = Request.QueryString("groupid")
        comm = Request.QueryString("commodity")
        pty = Request.QueryString("projecttype")
        pg = Request.QueryString("page")
        supp = Request.QueryString("supplier").ToString()
        grpCommodity = Request.QueryString("groupcommodity")
        pic = Request.QueryString("pic")
        pUser = Session("user")

        If (clsQuotationSupplierDB.checkingStatus(prj, grp, comm, cboPartNo.Value, supp, grpCommodity) <> "2" And clsQuotationSupplierDB.checkingStatusComplete(prj, grp, comm, cboPartNo.Value, supp, grpCommodity) = "1") Or pg = "acc" Then
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                e.Visible = False
            End If
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Delete Then
                e.Visible = False
            End If
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.New Then
                e.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        If pg = "acc" Then
            Response.Redirect("QuotationSupplier_Acceptance.aspx?projectid=" + prj + "&groupid=" + grp + "&commodity=" + comm + "&projecttype=" + pty + "&supplier=" + supp + "&groupcomodity=" + grpCommodity + "&pic=" + pic)
        Else
            Response.Redirect("QuotationSupplier.aspx?projectid=" + prj + "&groupid=" + grp + "&commodity=" + comm + "&projecttype=" + pty + "&supplier=" + pUser + "&groupcomodity=" + grpCommodity + "&pic=" + pic)
        End If
    End Sub

    Private Sub Grid_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs) Handles Grid.StartRowEditing
        If (Not Grid.IsNewRowEditing) Then
            Grid.DoRowValidation()
        End If
    End Sub
End Class
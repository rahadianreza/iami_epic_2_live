﻿Public Class AddMasterToolingDepreciation
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
    Dim CategoryMaterial As String = ""
    Dim MaterialCode As String = ""
    Dim period As String = ""
    Dim type As String = ""
    Dim partno As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A110")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        partno = Request.QueryString("partno")

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            GetExistData()
        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("MasterToolingDepreciation.aspx")
    End Sub

    Sub GetExistData()
        Dim ses As DataSet
        ses = cls_MasterToolingDB.getGridDepreciation(partno)
        If Not ses Is Nothing Then
            If ses.Tables(0).Rows.Count > 0 Then

                txtPartNo.Text = ses.Tables(0).Rows(0)("Part_No")
                txtPartName.Text = ses.Tables(0).Rows(0)("Part_Name")
                txtProjectName.Text = ses.Tables(0).Rows(0)("Project_Name")
                txtSupplierName.Text = ses.Tables(0).Rows(0)("UserName")
                txtToolingPrice.Text = ses.Tables(0).Rows(0)("Tooling_Price")
                txtInterestYear.Text = ses.Tables(0).Rows(0)("Interest_Year")
                txtDepreciationPeriodYear.Text = ses.Tables(0).Rows(0)("Depreciation_Period")
                txtTotalInterest.Text = ses.Tables(0).Rows(0)("Interest_Year") / ses.Tables(0).Rows(0)("Depreciation_Period")
                txtQtyPerUnit.Text = ses.Tables(0).Rows(0)("Qty_PerUnit")
                txtProjectProdUnitPermonth.Text = ses.Tables(0).Rows(0)("Project_Prod_unit_Month")
                txtDepreciationPeriod.Text = ses.Tables(0).Rows(0)("Qty_PerUnit") * (ses.Tables(0).Rows(0)("Project_Prod_unit_Month") * 12)
                txtTotalAmountInterest.Text = ses.Tables(0).Rows(0)("Tooling_Price") * (ses.Tables(0).Rows(0)("Interest_Year") / ses.Tables(0).Rows(0)("Depreciation_Period"))
                txtTotalToolingCost.Text = ses.Tables(0).Rows(0)("Tooling_Price") + (ses.Tables(0).Rows(0)("Tooling_Price") * (ses.Tables(0).Rows(0)("Interest_Year") / ses.Tables(0).Rows(0)("Depreciation_Period")))
                txtDepreciationCost.Text = (ses.Tables(0).Rows(0)("Tooling_Price") + (ses.Tables(0).Rows(0)("Tooling_Price") * (ses.Tables(0).Rows(0)("Interest_Year") / ses.Tables(0).Rows(0)("Depreciation_Period")))) / (ses.Tables(0).Rows(0)("Qty_PerUnit") * (ses.Tables(0).Rows(0)("Project_Prod_unit_Month") * 12))
            End If
        End If


    End Sub
End Class
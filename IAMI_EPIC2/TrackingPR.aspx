﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TrackingPR.aspx.vb" Inherits="IAMI_EPIC2.TrackingPR" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //Function for Message
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;

                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;

                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;

                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function BatchEditStartEditing(s, e) {
            if (e.focusedColumn.fieldName == "CEPlanning") {
                e.cancel = false;
            } else {
                e.cancel = true;
            }
        }  

	</script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    <table style="width: 100%;">
        <tr>
            <td>
                
            </td>
        </tr>
        <tr>
            <td></td>
            <td style=" padding:0px 0px 0px 0px; width:100px">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="From Date">
                    </dx1:aspxlabel>                       
            </td>
            <td width="8px"></td>
            <td colspan="1" style="width:120px">
                <dx:aspxdateedit ID="dtDateFrom" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateFrom"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:aspxdateedit>                       </td>
                <td style="width:30px">   
                    
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="To">
                    </dx1:aspxlabel>   
                                                     
                </td>
                <td>
                <dx:aspxdateedit ID="dtDateTo" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateTo"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                    </dx:aspxdateedit>   
                    
            </td>
            <td colspan="6"></td>
        </tr>
        <tr>
            <td colspan="7" style="height:10px"></td>
        </tr>
        <tr>
            <td width="5px">
                &nbsp;
            </td>
            <td width="100px">
                <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnRefresh" Theme="Default">   
                    <ClientSideEvents Click="function(s, e) {
                        Grid.PerformCallback('load|');
                        s.cp_val = 0;
                        s.cp_message = '';
                    }" />                     
                </dx:ASPxButton>
                &nbsp;
            </td>
            <td width="5px">
                &nbsp;
            </td>
            <td width="100px">
                <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnExcel" Theme="Default">    
                                   
                </dx:ASPxButton>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="7" style="height:10px"></td>
        </tr>
        <tr>
            <td colspan="7" style="height:10px">
                <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
            </td>
        </tr>
        </table>
</div>
<div style="padding: 5px 5px 5px 5px">
    <dx:ASPxGridView ID="Grid" runat="server" ClientInstanceName="Grid" Width="100%" 
        AutoGenerateColumns="false" EnableTheming="True" Theme="Office2010Black"  
        Font-Names="Segoe UI" Font-Size="8pt" KeyFieldName="Department;Section;Project;PRNo;Material_No">
        <ClientSideEvents EndCallback="OnEndCallback" BatchEditStartEditing="BatchEditStartEditing" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="DEPARTMENT (USER)" FieldName="Department" VisibleIndex="1" Width="250px" >
                <Settings AutoFilterCondition="Contains" />  
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="SECTION" FieldName="Section" VisibleIndex="2" Width="150px" >
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="PROJECT" FieldName="Project" VisibleIndex="3" Width="150px" >
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="PURCHASE REQUEST NO" FieldName="PRNo" VisibleIndex="4" Width="150px" >
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="PR TYPE" FieldName="PRType" VisibleIndex="5" Width="150px" >
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="MATERIAL NO" FieldName="Material_No" VisibleIndex="6" Width="150px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ITEM DESCRIPTION" FieldName="Description" VisibleIndex="6" Width="250px" >
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="SPESIFICATION" FieldName="Specification" VisibleIndex="7" Width="200px" >
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="QTY" FieldName="Qty" VisibleIndex="8" Width="50px" >
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="UOM" FieldName="UOM" VisibleIndex="9" Width="50px" >
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="PIC USER" FieldName="PICUserName" VisibleIndex="10" Width="100px" >
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="PIC COST CONTROL" FieldName="PICCostControl" VisibleIndex="11" Width="100px" >
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <%---PURCHASE REQUEST ---%>
            <dx:GridViewDataTextColumn Caption="PR CREATE" FieldName="PRReceived" VisibleIndex="12" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="PR APPROVAL 1" FieldName="PRApproval1" VisibleIndex="13" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="PR APPROVAL 2" FieldName="PRApproval2" VisibleIndex="14" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="PR APPROVAL 3" FieldName="PRApproval3" VisibleIndex="15" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <%---REQUEST FOR QUOTATION ---%>
            <dx:GridViewDataTextColumn Caption="RFQ CREATE" FieldName="RFQDate" VisibleIndex="16" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="RFQ APPROVAL" FieldName="RFQApproval" VisibleIndex="17" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="QUOTATION ACCEPTANCE" FieldName="QuotationAcceptance" VisibleIndex="18" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>

            <%---COST ESTIMATION ---%>
            <dx:GridViewDataTextColumn Caption="CE CREATE" FieldName="CEActual" VisibleIndex="19" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <%--<dx:GridViewDataTextColumn Caption="CE CREATE" FieldName="CEActual" VisibleIndex="20" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> --%>
            <dx:GridViewDataTextColumn Caption="CE APPROVAL 1" FieldName="CEApproval1" VisibleIndex="21" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="CE APPROVAL 2" FieldName="CEApproval2" VisibleIndex="22" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="CE APPROVAL 3" FieldName="CEApproval3" VisibleIndex="23" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="CE APPROVAL 4" FieldName="CEApproval4" VisibleIndex="24" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="CE ACCEPTANCE" FieldName="CEAcceptance" VisibleIndex="25" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="IA BUDGET" FieldName="IABudget" VisibleIndex="26" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            
            <%---COUNTER PROPOSAL ---%>
            <dx:GridViewDataTextColumn Caption="CP CREATE" FieldName="CounterProposal" VisibleIndex="27" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="CP APPROVAL 1" FieldName="CPApproval1" VisibleIndex="28" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="CP APPROVAL 2" FieldName="CPApproval2" VisibleIndex="28" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="CP APPROVAL 3" FieldName="CPApproval3" VisibleIndex="29" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 

            <%---SUPPLIER RECOMMENDATION ---%>
            <dx:GridViewDataTextColumn Caption="SR CREATE" FieldName="SuppRecommendation" VisibleIndex="30" Width="150px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="SR APPROVAL 1" FieldName="SRApproval1" VisibleIndex="31" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="SR APPROVAL 2" FieldName="SRApproval2" VisibleIndex="32" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="SR APPROVAL 3" FieldName="SRApproval3" VisibleIndex="33" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="SR APPROVAL 4" FieldName="SRApproval4" VisibleIndex="34" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 

             <%---CP ACCEPTANCE ---%>
             <dx:GridViewDataTextColumn Caption="CP ACCEPTANCE" FieldName="CPAcceptance" VisibleIndex="35" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            
            <%--IA PRICE--%>
            <dx:GridViewDataTextColumn Caption="IA PRICE" FieldName="IAPrice" VisibleIndex="36" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="IA PRICE APPROVAL 1" FieldName="IAApproval1" VisibleIndex="37" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="IA PRICE APPROVAL 2" FieldName="IAApproval2" VisibleIndex="38" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="IA PRICE APPROVAL 3" FieldName="IAApproval3" VisibleIndex="39" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="IA PRICE APPROVAL 4" FieldName="IAApproval4" VisibleIndex="40" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 


            <dx:GridViewDataTextColumn Caption="PO" FieldName="PODate" VisibleIndex="41" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
             <dx:GridViewDataTextColumn Caption="GOOD RECEIPT" FieldName="GRDate" VisibleIndex="42" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
<%--            <dx:GridViewDataTextColumn Caption="SUPPLIER" FieldName="Supplier" VisibleIndex="18" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
--%>        
<%--            <dx:GridViewDataTextColumn Caption="PO DATE" FieldName="PODate" VisibleIndex="20" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> --%>
<%--            <dx:GridViewDataTextColumn Caption="TOTAL AMOUNT PO" FieldName="POTotalAmount" VisibleIndex="21" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> --%>
            <dx:GridViewDataTextColumn Caption="PO CREATED BY" FieldName="POCreated" VisibleIndex="43" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="PO NO" FieldName="PONo" VisibleIndex="44" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="GR CREATED BY" FieldName="GRCreated" VisibleIndex="45" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="GR NO" FieldName="GRNo" VisibleIndex="46" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn> 
<%--            <dx:GridViewDataTextColumn Caption="GR DATE" FieldName="GRDate" VisibleIndex="24" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
--%>
<%--            <dx:GridViewDataTextColumn Caption="REMARKS" FieldName="Remarks" VisibleIndex="26" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn> 
--%>        
        </Columns>
        <SettingsBehavior ColumnResizeMode="Control" AllowSort="false" />
        <SettingsPager Mode="ShowAllRecords" AlwaysShowPager="true"></SettingsPager>
        <Settings ShowFilterRow="true" VerticalScrollBarMode="Auto" VerticalScrollableHeight="320" HorizontalScrollBarMode="Auto" ShowStatusBar="Hidden" />
      <%--  <SettingsEditing Mode="Batch"></SettingsEditing>--%>
    </dx:ASPxGridView>
</div>
</asp:Content>

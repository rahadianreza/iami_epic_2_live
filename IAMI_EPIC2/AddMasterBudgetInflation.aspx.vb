﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Public Class AddMasterBudgetInflation
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim outError As String = ""
    Dim msgtype As String = ""
    Dim message As String = ""
    Dim dt As DataTable
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Functions"
    Private Sub show_JS(ByVal MsgType As String, ByVal ErrMsg As String)
        cbSave.JSProperties("cp_type") = MsgType
        cbSave.JSProperties("cp_message") = ErrMsg
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A190")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A190")
    End Sub

    Protected Sub Grid_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In Grid.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "Qty" Then
                If IsNothing(e.NewValues("Qty")) OrElse e.NewValues("Qty").ToString.Trim = "" OrElse e.NewValues("Qty").ToString.Trim = 0 Then
                    e.Errors(dataColumn) = "Please Input Qty !"
                    Grid.JSProperties("cp_disabled") = ""
                    Grid.JSProperties("cp_type") = "2"
                    Grid.JSProperties("cp_message") = "Please input Qty !"
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Dim cls As New ClsMasterBudgetInflation
        If e.UpdateValues.Count = 0 Then
            Session("msgtype") = "2"
            Session("message") = "Please Select Data Minimum 1 !"
            Exit Sub
        End If
        Dim ls_Chk As String = ""
        For iLoop = 0 To e.UpdateValues.Count - 1
            ls_Chk = (e.UpdateValues(iLoop).NewValues("Cek").ToString())

            If ls_Chk = True Then
                cls.SupplierCode = cboSupplierCode.Text
                cls.SupplierName = txtSupplierName.Text
                cls.Type = cboType.Text
                cls.VariantCode = (e.UpdateValues(iLoop).NewValues("VariantCode").ToString())
                cls.VariantName = (e.UpdateValues(iLoop).NewValues("VariantName").ToString())
                cls.Material = CDbl((e.UpdateValues(iLoop).NewValues("Material").ToString()))
                cls.Process = CDbl((e.UpdateValues(iLoop).NewValues("Process").ToString()))
                cls.User = pUser

                'cekData
                dt = ClsMasterBudgetInflationDB.getData(cls, , , outError)

                If dt.Rows.Count > 0 Then
                    Grid.JSProperties("cp_type") = "2"
                    Grid.JSProperties("cp_message") = "Data Already Exist !"
                    Exit Sub
                End If

                ClsMasterBudgetInflationDB.insert(cls, , , outError)

                If outError <> "" Then
                    show_JS("2", outError)
                    Exit For
                End If
            End If
            show_JS("1", "Data Saved Success")
            Grid.EndUpdate()
        Next iLoop
        'clear()
        Grid.CancelEdit()
    End Sub
    Private Sub Grid_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If e.Column.FieldName = "VariantCode" Or e.Column.FieldName = "VariantName" Then
            e.Editor.ReadOnly = True
        Else
            e.Editor.ReadOnly = False
        End If
    End Sub

    Protected Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim cMasterBudgetInflation As New ClsMasterBudgetInflation
        Dim pAction As String = ""
        Dim outError As String = ""

        Dim dt As System.Data.DataTable

        pAction = Split(e.Parameters, "|")(0)

        If pAction = "load" Then
            cMasterBudgetInflation.SupplierCode = Split(e.Parameters, "|")(1)
            cMasterBudgetInflation.Type = Split(e.Parameters, "|")(2)

            dt = ClsMasterBudgetInflationDB.getDataDetail(cMasterBudgetInflation, , )

            Grid.DataSource = dt
            Grid.DataBind()
        End If

    End Sub

    Private Sub cbSave_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSave.Callback
        Dim pAction = ""
        pAction = Split(e.Parameter, "|")(0)
        If pAction = "loadMessage" Then
            Dim cls As New ClsMasterBudgetInflation
            cls.SupplierCode = cboSupplierCode.Text
            cls.Type = cboType.Text
            dt = ClsMasterBudgetInflationDB.CheckData(cls, , , outError)
            If dt.Rows.Count = 0 Then
                show_JS("2", "Data Not Saved !")
            Else
                show_JS("1", "Data Saved Success")
            End If

        End If
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
    End Sub

#End Region
End Class

'Public Class AddMasterBudgetInflation
'    Inherits System.Web.UI.Page

'    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

'    End Sub


'    Protected Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
'        Dim cMasterBudgetInflation As New ClsMasterBudgetInflation
'        Dim pAction As String = ""
'        Dim outError As String = ""

'        Dim dt As System.Data.DataTable

'        pAction = Split(e.Parameters, "|")(0)

'        If pAction = "load" Then
'            cMasterBudgetInflation.SupplierCode = Split(e.Parameters, "|")(1)
'            cMasterBudgetInflation.Type = Split(e.Parameters, "|")(2)

'            dt = ClsMasterBudgetInflationDB.getDataDetail(cMasterBudgetInflation, , )

'            Grid.DataSource = dt
'            Grid.DataBind()
'        End If

'    End Sub

'End Class
﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO
Imports System.Transactions
Imports System.Web.UI

Public Class CPUpdateDetail
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
    Dim ls_SupplierCode As String
#End Region

#Region "PROCEDURE"
    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
        ls_SupplierCode = GetSupplierCodeByUserLogin()

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_List", "SupplierCode|CPNumber|CENumber", ls_SupplierCode & "|" & txtCPNumber.Text & "|" & txtCENumber.Text, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub SetInformation(ByVal pObj As Object, Optional pCalledFromCPList As Boolean = False)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Supplier_Code As String = GetSupplierCodeByUserLogin()
        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_SetHeader", "CPNumber|CENumber|Rev|Supplier_Code", txtCPNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text & "|" & Supplier_Code, ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_SetHeader", "CPNumber|CENumber|Rev|Supplier_Code", Split(Request.QueryString(0), "|")(0) & "|" & Split(Request.QueryString(0), "|")(2) & "|" & Split(Request.QueryString(0), "|")(1) & "|" & Supplier_Code, ErrMsg)
        End If


        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                pObj.JSProperties("cpCPNumber") = ds.Tables(0).Rows(0).Item("CP_Number").ToString()
                pObj.JSProperties("cpRev") = ds.Tables(0).Rows(0).Item("Rev").ToString()
                pObj.JSProperties("cpCENumber") = ds.Tables(0).Rows(0).Item("CE_Number")
                pObj.JSProperties("cpNotes") = ds.Tables(0).Rows(0).Item("Notes").ToString()
                pObj.JSProperties("cpSupplierNotes") = ds.Tables(0).Rows(0).Item("Supplier_Notes").ToString()
                'memoNote.Text = ds.Tables(0).Rows(0).Item("Notes").ToString()
            Else
                pObj.JSProperties("cpCPNumber") = ""
                pObj.JSProperties("cpRev") = ""
                pObj.JSProperties("cpCENumber") = ""
                pObj.JSProperties("cpNotes") = ""
                pObj.JSProperties("cpSupplierNotes") = ""
            End If

            If pCalledFromCPList = True Then
                'add this parameter to prevent reset value of Notes when callback
                pObj.JSProperties("cpCalledFromCPList") = "1"
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, pObj)
        End If

        'load grid
        Dim ds2 As New DataSet
        Dim ErrMsg2 As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
        ls_SupplierCode = GetSupplierCodeByUserLogin()

        ds2 = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_List", "SupplierCode|CPNumber|CENumber", ls_SupplierCode & "|" & txtCPNumber.Text & "|" & txtCENumber.Text, ErrMsg2)

        If ErrMsg2 = "" Then
            Grid.DataSource = ds2.Tables(0)
            Grid.DataBind()

            If ds2.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg2, Grid)
        End If
    End Sub

    Private Sub up_UpdateSupplierNotes()
        Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
        Dim idx As Integer = 0

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                Dim sqlComm As New SqlCommand()
                Dim Supplier_Code As String = GetSupplierCodeByUserLogin()
                ls_SQL = "sp_CP_Update_SupplierNotes "
                sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("CPNumber", txtCPNumber.Text)
                sqlComm.Parameters.AddWithValue("Rev", txtRev.Text)
                sqlComm.Parameters.AddWithValue("CENumber", txtCENumber.Text)
                sqlComm.Parameters.AddWithValue("SupplierCode", Supplier_Code)
                sqlComm.Parameters.AddWithValue("SupplierNotes", suppNote.Text)
                sqlComm.Parameters.AddWithValue("RegUser", pUser)
                sqlComm.ExecuteNonQuery()
                sqlComm.Dispose()


                sqlTran.Commit()
            End Using
        End Using
    End Sub
#End Region

#Region "FUNCTIONS"
    Private Function GetSupplierCodeByUserLogin() As String
        Dim retVal As String = ""
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "SELECT Supplier_Code = ISNULL(Supplier_Code,'') FROM UserSetup WHERE UserID = '" & Session("user") & "'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = ds.Tables(0).Rows(0).Item("Supplier_Code")
                Else
                    'NOT EXISTS
                    retVal = ""
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataExist() As String
        Dim retVal As String = ""
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Exist", "CPNumber", txtCPNumber.Text, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = ds.Tables(0).Rows(0).Item("Ret")
                Else
                    'NOT EXISTS
                    retVal = "N"
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataSubmitted() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "exec sp_CP_Update_IsDataSubmitted '" & txtCPNumber.Text & "','" & txtCENumber.Text & "', '" & txtRev.Text & "','" & pUser & "'", "", "", ErrMsg)

            'ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '6'", "", "", ErrMsg)


            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function ConvertToFormatDate_yyyyMMdd(ByVal pDate As String) As String
        Dim retVal As String = "1900-01-01"
        Dim pYear As String = Split(pDate, "-")(0)
        Dim pMonth As String = Strings.Right("0" & Split(pDate, "-")(1), 2)
        Dim pDay As String = Strings.Right("0" & Split(pDate, "-")(2), 2)

        retVal = pYear & "-" & pMonth & "-" & pDay

        Return retVal
    End Function
#End Region

#Region "EVENTS"
    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|E030", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                          "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
       
        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, txtCENumber)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "COUNTER PROPOSAL UPDATE (TENDER) DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E030")
        Dim Supplier_Code As String = GetSupplierCodeByUserLogin()
        gs_Message = ""

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then

            If Request.QueryString.Count > 0 Then
                Try
                    'SetInformation(memoNote)
                    txtCPNumber.Text = Split(Request.QueryString(0), "|")(0)
                    txtRev.Text = Split(Request.QueryString(0), "|")(1)
                    txtCENumber.Value = Split(Request.QueryString(0), "|")(2)

                    Dim ds As New DataSet
                    Dim ErrMsg As String = ""

                    If Request.QueryString.Count <= 0 Then
                        'Using Add Button
                        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_SetHeader", "CPNumber|CENumber|Rev|Supplier_Code", txtCPNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text & "|" & Supplier_Code, ErrMsg)
                    Else
                        'Using Detail Link from Grid
                        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_SetHeader", "CPNumber|CENumber|Rev|Supplier_Code", Split(Request.QueryString(0), "|")(0) & "|" & Split(Request.QueryString(0), "|")(2) & "|" & Split(Request.QueryString(0), "|")(1) & "|" & Supplier_Code, ErrMsg)
                    End If

                    If ds.Tables(0).Rows.Count > 0 Then
                        memoNote.Text = ds.Tables(0).Rows(0).Item("Notes").ToString
                        suppNote.Text = ds.Tables(0).Rows(0).Item("Supplier_Notes").ToString
                    End If

                    Dim script As String = ""
                    If IsDataSubmitted() = True Then
                        btnDraft.Enabled = False
                        btnSubmit.Enabled = False

                        script = "suppNote.SetEnabled(false);"
                        ScriptManager.RegisterStartupScript(suppNote, suppNote.GetType(), "suppNote", script, True)
                      
                    Else
                        btnDraft.Enabled = True
                        btnSubmit.Enabled = True
                        script = "suppNote.SetEnabled(true);"
                        ScriptManager.RegisterStartupScript(suppNote, suppNote.GetType(), "suppNote", script, True)

                    End If

                Catch ex As Exception
                    Response.Redirect("CPUpdateDetail.aspx")
                End Try


                'Remove session after back from ViewCounterProposal.aspx
                If IsNothing(Session("E030_QueryString")) = False Then
                    Session.Remove("E030_QueryString")
                End If

            Else

            End If
        End If
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "BestPricePcs" Then
            'Editable
            If IsDataSubmitted() = True Then
                'Non-Editable
                e.Cell.BackColor = Color.LemonChiffon
            Else
                'Editable
                e.Cell.BackColor = Color.White
            End If
        Else
            'Non-Editable
            e.Cell.BackColor = Color.LemonChiffon
        End If
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Try
            If e.UpdateValues.Count > 0 Then
                Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
                Dim idx As Integer = 0

                Dim lb_IsDataExist As String = IsDataExist()

                If lb_IsDataExist <> "Y" Then
                    Session("E030_ErrMsg") = "CP Number " & txtCPNumber.Text & " is invalid!"
                    Exit Sub
                End If

                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()

                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                        Dim sqlComm As New SqlCommand()

                        '#DETAIL
                        For idx = 0 To e.UpdateValues.Count - 1

                            ls_SQL = "EXEC sp_CP_Update_Detail_BestPrice "
                            ls_SQL = ls_SQL + _
                                          "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Material_No").ToString() & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("BestPricePcs").ToString() & "'," & _
                                          "'" & UserLogin & "'"

                            sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()
                        Next idx

                        sqlTran.Commit()
                    End Using
                End Using
                up_UpdateSupplierNotes()
            Else
                Exit Sub
            End If


        Catch ex As Exception
            Session("E030_ErrMsg") = Err.Description
        End Try
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim headerCaption As String = ""
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""
        Dim pfunction As String = Split(e.Parameters, "|")(0)

        If pfunction = "gridload" Then
            Call GridLoad()
        ElseIf pfunction = "save" Then
            Dim ds As New DataSet
            Dim ErrMsg As String = ""
            Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
            Dim ls_SQL As String = ""

            ls_SupplierCode = GetSupplierCodeByUserLogin()
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_List", "SupplierCode|CPNumber|CENumber", ls_SupplierCode & "|" & txtCPNumber.Text & "|" & txtCENumber.Text, ErrMsg)

            Grid.JSProperties("cpSaveData") = "1"

            Try
                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()

                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                        Dim sqlComm As New SqlCommand()

                        '#DETAIL
                        For iLoop = 0 To ds.Tables(0).Rows.Count - 1

                            ls_SQL = "EXEC sp_CP_Update_Detail_BestPrice "
                            ls_SQL = ls_SQL + _
                                          "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                          "'" & ds.Tables(0).Rows(iLoop)("Material_No").ToString() & "'," & _
                                          "'" & ds.Tables(0).Rows(iLoop)("BestPricePcs") & "'," & _
                                          "'" & pUser & "'"

                            sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()
                        Next iLoop

                        sqlTran.Commit()
                    End Using
                End Using
                up_UpdateSupplierNotes()


                If IsNothing(Session("F020_ErrMsg")) = True Then
                    Call SetInformation(Grid)
                    Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", Grid)
                Else
                    If Session("F020_ErrMsg") = "Please fill all of rows on the grid before save!" Then
                        Call DisplayMessage(MsgTypeEnum.Warning, Session("F020_ErrMsg"), Grid)
                        Session.Remove("F020_ErrMsg")
                        Exit Sub
                    End If
                End If
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
                Return
            End Try
        ElseIf pfunction = "draft" Then
            SetInformation(Grid)
        End If
        'Try
        '    'After Save Data
        '    If e.Parameters = "save" Then
        '        Grid.JSProperties("cpSaveData") = "1"

        '        If IsNothing(Session("E030_ErrMsg")) = True Then
        '            Call SetInformation(Grid)
        '            Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", Grid)
        '        Else
        '            If Session("E030_ErrMsg") = "Please fill all of rows on the grid before save!" Then
        '                Call DisplayMessage(MsgTypeEnum.Warning, Session("E030_ErrMsg"), Grid)
        '                Session.Remove("E030_ErrMsg")
        '                Exit Sub
        '            End If
        '        End If
        '    End If

        '    If e.Parameters = "draft" Then
        '        Grid.JSProperties("cpSaveData") = "1"

        '        If IsNothing(Session("E030_ErrMsg")) = True Then
        '            Call SetInformation(Grid)
        '            Call up_UpdateSupplierNotes()
        '            Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", Grid)
        '        Else
        '            If Session("E030_ErrMsg") = "Please fill all of rows on the grid before save!" Then
        '                Call DisplayMessage(MsgTypeEnum.Warning, Session("E030_ErrMsg"), Grid)
        '                Session.Remove("E030_ErrMsg")
        '                Exit Sub
        '            End If
        '        End If
        '    End If

        '    Call GridLoad()

        'Catch ex As Exception
        '    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        'End Try
    End Sub

    Private Sub cbExist_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbExist.Callback
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        Select Case e.Parameter
            Case "exist"
                Dim ret As String = "N"
                cbExist.JSProperties("cpParameter") = "exist"

                ret = IsDataExist()
                cbExist.JSProperties("cpResult") = ret

            Case "IsSubmitted"
                cbExist.JSProperties("cpParameter") = "IsSubmitted"
                If IsDataSubmitted() = True Then
                    cbExist.JSProperties("cpResult") = "1"
                Else
                    cbExist.JSProperties("cpResult") = "0"
                End If
            Case "Draft"
                'cbExist.JSProperties("cpMessage") = "Data updated successfully!"
                Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", cbExist)
            Case "submit"
                Dim ls_SQL As String = "", UserLogin As String = Session("user")
                Dim idx As Integer = 0

                up_UpdateSupplierNotes()

                Try
                    cbExist.JSProperties("cpParameter") = "submit"

                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()

                        Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                            ls_SQL = "exec sp_CP_Update_Submit '" & txtCPNumber.Text & "','" & txtCENumber.Text & "', '" & txtRev.Text & "','" & UserLogin & "'"

                            Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()

                            sqlTran.Commit()
                        End Using
                    End Using

                    Call SetInformation(cbExist)
                    cbExist.JSProperties("cpSubmit") = "Y"
                    Call DisplayMessage(MsgTypeEnum.Success, "Data submitted successfully!", cbExist)
                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                End Try
        End Select
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Try
            If Request.QueryString.Count > 0 Then
                Dim pDateFrom As String = ConvertToFormatDate_yyyyMMdd(Split(Request.QueryString(0), "|")(6)), _
                    pDateTo As String = ConvertToFormatDate_yyyyMMdd(Split(Request.QueryString(0), "|")(7)), _
                    pPRNumber As String = Split(Request.QueryString(0), "|")(8), _
                    pSupplierCode As String = Split(Request.QueryString(0), "|")(9), _
                    pCPNumber As String = Split(Request.QueryString(0), "|")(10), _
                    pRowPos As String = Split(Request.QueryString(0), "|")(11)
                Session("btnBack_CPUpdateDetail") = pDateFrom & "|" & pDateTo & "|" & pPRNumber & "|" & pSupplierCode & "|" & pCPNumber & "|" & pRowPos
            End If

        Catch ex As Exception

        End Try

        Response.Redirect("CPUpdate.aspx")
    End Sub
#End Region


#Region "Row grid updating"

    'Private Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles Grid.RowDeleting
    '    e.Cancel = True
    'End Sub

    Private Sub Grid_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles Grid.RowInserting
        e.Cancel = True
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
    End Sub
#End Region

End Class
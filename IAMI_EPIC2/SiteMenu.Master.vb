﻿Option Strict On
Imports System.Data.SqlClient

Public Class SiteMenu
    Inherits System.Web.UI.MasterPage

#Region "DECLARATION"
    Dim sqlstring As String
    Dim dtView As DataTable
    Dim constr As New SqlClient.SqlConnection(Sconn.Stringkoneksi)
    Public SiteTitle As String
    Public MenuIndex As Integer
    Public IdMenu As String
    Dim script As String
#End Region

#Region "CONTROL EVENTS"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        MenuIndex = sGlobal.indexMenu
        IdMenu = sGlobal.idMenu.Trim()
        'Label1.Text = SiteTitle
        If IsNothing(Session("user")) Then
            If Page.IsCallback Then
                DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/Default.aspx")
            Else
                Response.Redirect("~/Default.aspx")
            End If
            Exit Sub
        End If

        'If checkPrivilege() = False Then
        '    If Page.IsCallback Then
        '        DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/need_authorization.aspx")
        '    Else
        '        Response.Redirect("~/need_authorization.aspx")
        '    End If
        '    Exit Sub
        'End If

        With Me
            .BindMenu()
            lblUser.Text = Session("user").ToString.ToUpper
            'lblAdmin.Text = Session("AdminStatus").ToString
        End With

        lblTaskList.Text = "Task List (" & GetTaskListCount() & ")"

        Session.Timeout = 600 'in minutes
    End Sub

    Private Sub rptMenu_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMenu.ItemDataBound
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim rptSubMenu As Repeater = TryCast(e.Item.FindControl("rptChildMenu"), Repeater)
            Dim test As String = DirectCast(e.Item.DataItem, System.Data.DataRowView).Row(0).ToString
            'Dim q As String = "SELECT GroupID,RTRIM(MenuDesc) MenuDesc, MenuName, '' HelpName, MenuIndex, " & vbCrLf &
            '    "CASE WHEN USM.MenuID = '" & IdMenu & "' THEN 'active' ELSE '' END StatusActiveChild," & vbCrLf &
            '    "CASE WHEN MenuIndex = '" & MenuIndex & "' THEN 'active' ELSE '' END StatusActiveParent FROM dbo.UserMenu USM " & vbCrLf &
            '    "LEFT JOIN dbo.UserPrivilege UP ON USM.AppID = UP.AppID AND USM.MenuID = UP.MenuID WHERE GroupID ='" & test & "' " & vbCrLf &
            '    "AND AllowRead='1' and MenuName <> '' AND UserID='" & Session("user").ToString & "' " & vbCrLf &
            '    "order by GroupIndex, MenuIndex "
            Dim q As String
            If Session("AdminStatus").ToString = "1" Then
                q = "SELECT GroupID,RTRIM(MenuDesc) MenuDesc, MenuName, '' HelpName, MenuIndex, " & vbCrLf &
                "CASE WHEN USM.MenuID = '" & IdMenu & "' THEN 'active' ELSE '' END StatusActiveChild," & vbCrLf &
                "CASE WHEN MenuIndex = '" & MenuIndex & "' THEN 'active' ELSE '' END StatusActiveParent " & vbCrLf &
                "FROM dbo.UserMenu USM " & vbCrLf &
                "WHERE GroupID = '" & test & "' " &
                "and ActiveStatus = '1' " &
                "order by GroupIndex, MenuIndex "
            Else
                q = "SELECT GroupID,RTRIM(MenuDesc) MenuDesc, MenuName, '' HelpName, MenuIndex, " & vbCrLf &
                "CASE WHEN USM.MenuID = '" & IdMenu & "' THEN 'active' ELSE '' END StatusActiveChild," & vbCrLf &
                "CASE WHEN MenuIndex = '" & MenuIndex & "' THEN 'active' ELSE '' END StatusActiveParent " & vbCrLf &
                "FROM dbo.UserMenu USM LEFT JOIN dbo.UserPrivilege UP ON USM.AppID = UP.AppID AND USM.MenuID = UP.MenuID  " & vbCrLf &
                "WHERE UP.AllowAccess = '1' AND UP.UserID='" & Session("User").ToString & "' " & vbCrLf &
                "and GroupID = '" & test & "' " &
                "and ActiveStatus = '1' " &
                "order by GroupIndex, MenuIndex "
            End If
            rptSubMenu.DataSource = GetData(q)
            rptSubMenu.DataBind()
        End If
    End Sub

    Private Function GetTaskListCount() As String
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim strCount As String = "0"

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Task_List", "UserID", Session("user").ToString, ErrMsg)

        If ErrMsg = "" Then
            strCount = ds.Tables(0).Rows.Count.ToString
        End If

        Return strCount
    End Function
#End Region

#Region "FUNCTION"

    Private Function checkPrivilege() As Boolean
        Dim page As String = Server.HtmlEncode(Request.Path)

        Dim resultSplit() As String = page.Split(CChar("/"))
        Dim sqlstring As String = " SELECT * FROM dbo.UserPrivilege UP " &
                                  " JOIN dbo.UserMenu UM ON UP.MenuID = UM.MenuID " &
                                  " WHERE UP.AllowRead = 1 AND UserID='" & Session("User").ToString & "' "
        Dim dt As DataTable = sGlobal.GetData(sqlstring)
        If dt.Rows.Count > 0 Then
            Return True
        End If

        Return False
    End Function

    Private Sub BindMenu()
        Dim q As String
        If Session("AdminStatus").ToString = "1" Then
            q = "select USM.GroupID, USM.GroupIndex, USM.GroupID MenuDesc, USM.GroupID MenuName, " & vbCrLf &
            "max(GroupIcon) HelpName, 0 MenuIndex,'' StatusActiveChild, '' StatusActiveParent  " & vbCrLf &
            "FROM dbo.UserMenu USM " & vbCrLf &
            "where ActiveStatus = '1' " & vbCrLf &
            "group by USM.GroupID, USM.GroupIndex, USM.GroupID, USM.GroupID " & vbCrLf &
            "order by USM.GroupIndex "
        Else
            q = "select USM.GroupID, USM.GroupIndex, USM.GroupID MenuDesc, USM.GroupID MenuName, " & vbCrLf &
                "max(GroupIcon) HelpName, 0 MenuIndex,'' StatusActiveChild, '' StatusActiveParent  " & vbCrLf &
                "FROM dbo.UserMenu USM INNER JOIN dbo.UserPrivilege UP ON USM.AppID = UP.AppID AND USM.MenuID = UP.MenuID  " & vbCrLf &
                "WHERE UP.AllowAccess = '1' " & vbCrLf &
                "AND UP.UserID='" & Session("User").ToString & "' " & vbCrLf &
                "AND USM.MenuID <> 'Z010 ' " & vbCrLf &
                "and ActiveStatus = '1' " & vbCrLf &
                "group by USM.GroupID, USM.GroupIndex, USM.GroupID, USM.GroupID " & vbCrLf &
                "order by USM.GroupIndex "
        End If
        Me.rptMenu.DataSource = GetData(q)
        Me.rptMenu.DataBind()
    End Sub

#End Region
End Class
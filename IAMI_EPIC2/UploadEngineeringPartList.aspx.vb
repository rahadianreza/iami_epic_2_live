﻿Imports System.Web.UI
Imports System.IO
Imports System.Drawing
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls.Style
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxUploadControl
Imports OfficeOpenXml
Imports System.Transactions


Public Class UploadEngineeringPartList
    Inherits System.Web.UI.Page
    Dim pUser As String = ""

    Dim FilePath As String = ""
    Dim FileName As String = ""
    Dim FileExt As String = ""
    Dim Ext As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Master.SiteTitle = "UPLOAD ENGINEERING PART LIST"
            pUser = Session("user")
            If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            Else
                Ext = Server.MapPath("")
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Function SavePostedFiles(ByVal uploadedFile As UploadedFile) As String
        If (Not uploadedFile.IsValid) Then
            Return String.Empty
        End If
        Ext = Path.Combine(MapPath(""))
        FileName = Path.GetFileNameWithoutExtension(Uploader.PostedFile.FileName)
        FileExt = Path.GetExtension(Uploader.PostedFile.FileName)
        Ext = Mid(Ext, 1, Len(Ext) - 7)
        FilePath = Ext & "\Import\" & FileName & "_" & Format(CDate(Now), "yyyyMMdd_hhmmss")
        uploadedFile.SaveAs(FilePath)

        Return FilePath
    End Function
    Protected Sub Uploader_FileUploadComplete(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)
        Try
            e.CallbackData = SavePostedFiles(e.UploadedFile)
        Catch ex As Exception
            e.IsValid = False
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 0)
        End Try
    End Sub
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        cbsubmit.JSProperties("cp_Message") = ErrMsg
    End Sub

    Private Sub importExcel()
        Dim pErr As String = ""
        Try

            BtnSubmit.ClientEnabled = False
            If Uploader.HasFile Then
                'FileName = Path.GetFileName(Uploader.PostedFile.FileName)
                FileName = Path.GetFileNameWithoutExtension(Uploader.PostedFile.FileName)
                FileExt = Path.GetExtension(Uploader.PostedFile.FileName)
                'Ext = Mid(Ext, 1, Len(Ext) - 7)
                FilePath = Ext & "\Import\" & FileName & "_" & Format(CDate(Now), "yyyyMMdd_hhmmss") & FileExt
                Uploader.SaveAs(FilePath)

                'proses import from excel ke database
                uf_ImpotExcel(FilePath)


                Dim ls_Dir As New IO.DirectoryInfo(Ext & "\Import\")
                Dim ls_GetFile As IO.FileInfo() = ls_Dir.GetFiles()
                Dim ls_File As IO.FileInfo
                Dim li_CountDate As Long

                For Each ls_File In ls_GetFile
                    'keep file Import for 30 days
                    li_CountDate = DateDiff(DateInterval.Day, CDate(Format(ls_File.LastWriteTime, "MM/dd/yyyy")), CDate(Format(Now, "MM/dd/yyyy")))
                    If li_CountDate > 30 Then
                        File.Delete(ls_File.FullName)
                    End If
                Next
            End If


        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 1)
        End Try
    End Sub
    Private Function uf_ImpotExcel(ByVal pFileName As String, Optional ByRef perr As String = "") As String
        Session("idx") = Nothing
        Dim listenggineering As New ClsEngineeringPartList
        Dim pbar As Integer
        Dim pNoError As Integer
        Dim pno As Integer
        Dim pCountVariant As Integer
        Dim pError As String = ""
        Try
            popUp.ShowOnPageLoad = True
            Dim fi As New FileInfo(pFileName)
            Dim dt As New DataTable
            Dim exl As New ExcelPackage(fi)
            Dim ws As ExcelWorksheet
            Dim tbl As New DataTable
            MemoMessage.Text = ""
            MemoMessage.Text = "No  " & "Error Message ||" & vbCrLf

            Dim wss As OfficeOpenXml.ExcelWorksheets
            Try
                wss = exl.Workbook.Worksheets
            Catch ex As Exception
                wss = exl.Workbook.Worksheets
            End Try


            Dim options As New TransactionOptions
            options.IsolationLevel = IsolationLevel.ReadCommitted
            options.Timeout = New TimeSpan(0, 30, 0)

            Using Scope As New TransactionScope(TransactionScopeOption.Required, options) ' TimeSpan.FromMinutes(10)
                ProgressBar.Position = 0
                ProgressBar.Value = 0
                ws = exl.Workbook.Worksheets(1)
                'Check header
                If ws.Cells(2, 1).Value <> "PART NO" Or ws.Cells(2, 2).Value <> "PART NAME" Or ws.Cells(2, 3).Value <> "UPC" Or ws.Cells(2, 4).Value <> "FNA" Or ws.Cells(2, 5).Value <> "EPL PART NO" Or ws.Cells(2, 6).Value <> "DTL UPC" _
                    Or ws.Cells(2, 7).Value <> "DTL FNA" Or ws.Cells(2, 8).Value <> "USG/SEQ" Or ws.Cells(2, 9).Value <> "LVL" Or ws.Cells(2, 10).Value <> "QTY" Or ws.Cells(2, 11).Value <> "HAND" Or ws.Cells(2, 12).Value <> "D/C" _
                    Or ws.Cells(2, 13).Value <> "DWG NO" Or ws.Cells(2, 14).Value <> "J NOTE ADDRESS" Or ws.Cells(2, 15).Value <> "L NOTE ADDRESS" Or ws.Cells(2, 16).Value <> "COLOR PART" Or ws.Cells(2, 17).Value <> "CC CODE" _
                    Or ws.Cells(2, 18).Value <> "INTR COLOR" Or ws.Cells(2, 19).Value <> "BODY COLOR" Or ws.Cells(2, 20).Value <> "EXTR COLOR" Then
                    pNoError = CInt(pNoError + 1)
                    MemoMessage.Text = MemoMessage.Text & pNoError & " Wrong Template ||" & vbCrLf
                End If

                'Check variant
                Dim pcount As Integer = 21
                Dim pdatadifferent As Boolean = False
                'If ClsUploadEngineeringPartListDB.SelCount(cboProject.Value, pError) = True Then
                '    'Compare Variant in db with in excel
                '    For xy = 1 To 36
                '        If ws.Cells(2, pcount).Value = "SUPPLY CNTRY CODE" Then
                '            pError = ""
                '            pno = pno + 1
                '            If ClsUploadEngineeringPartListDB.SelCompare(pno, cboProject.Value, ws.Cells(1, pcount).Value, pError) = False Then
                '                pNoError = CInt(pNoError + 1)
                '                MemoMessage.Text = MemoMessage.Text & pNoError & " for Variant" & pno & " " & ws.Cells(1, pcount).Value & " different with variant name in database " & vbCrLf
                '                pdatadifferent = True
                '            End If
                '            pcount = pcount + 3
                '            If pError <> "" Then
                '                pNoError = CInt(pNoError + 1)
                '                MemoMessage.Text = MemoMessage.Text & pNoError & pError & vbCrLf
                '            End If
                '        End If
                '    Next


                'End If

                'Delete list Variant And Delete Enngeneering part list
                ClsUploadEngineeringPartListDB.DeleteVariantAndEnggeneer(cboProject.Value, pError)
                If pError <> "" Then
                    pNoError = CInt(pNoError + 1)
                    MemoMessage.Text = MemoMessage.Text & pNoError & " " & pError & " ||" & vbCrLf
                End If


                'If pdatadifferent = False Then
                'Insert Engineering_Part_List_Variant
                pcount = 21
                pno = 0
                For xy = 1 To 36
                    If ws.Cells(2, pcount).Value = "SUPPLY CNTRY CODE" Then
                        pError = ""
                        pno = pno + 1
                        'If ClsUploadEngineeringPartListDB.SelCompare(pno, cboProject.Value, ws.Cells(1, pcount).Value, pError) = False Then
                        ClsUploadEngineeringPartListDB.Insert(pno, cboProject.Value, ws.Cells(1, pcount).Value, pUser, pError)
                        'End If
                        pcount = pcount + 3
                        If pError <> "" Then
                            pNoError = CInt(pNoError + 1)
                            MemoMessage.Text = MemoMessage.Text & pNoError & " " & pError & " ||" & vbCrLf
                        End If
                    End If
                Next
                'End If


                'proses insert perrow
                For y = 3 To ws.Dimension.End.Row
                    listenggineering.Pno = y - 2
                    listenggineering.Project_ID = cboProject.Value
                    listenggineering.UserID = pUser.ToString.Trim
                    listenggineering.Part_No = ws.Cells(y, 1).Value
                    listenggineering.Part_Name = ws.Cells(y, 2).Value
                    listenggineering.Upc = ws.Cells(y, 3).Value
                    listenggineering.Fna = ws.Cells(y, 4).Value
                    listenggineering.Epl_Part_No = ws.Cells(y, 5).Value
                    listenggineering.Dtl_Upc = ws.Cells(y, 6).Value
                    listenggineering.Dtl_Fna = ws.Cells(y, 7).Value
                    listenggineering.Usg_Seq = ws.Cells(y, 8).Value
                    listenggineering.Lvl = ws.Cells(y, 9).Value
                    listenggineering.Qty = ws.Cells(y, 10).Value
                    listenggineering.Hand = ws.Cells(y, 11).Value
                    listenggineering.D_C = IIf(ws.Cells(y, 12).Value = Nothing, "", ws.Cells(y, 12).Value)
                    listenggineering.Dwg_No = ws.Cells(y, 13).Value
                    listenggineering.J_Note_Address = ws.Cells(y, 14).Value
                    listenggineering.L_Note_Address = ws.Cells(y, 15).Value
                    listenggineering.Color_Part = ws.Cells(y, 16).Value
                    listenggineering.CC_Code = ws.Cells(y, 17).Value
                    listenggineering.Intr_Color = ws.Cells(y, 18).Value
                    listenggineering.Body_Color = ws.Cells(y, 19).Value
                    listenggineering.Extr_Color = ws.Cells(y, 10).Value
                    listenggineering.Check_Item = IIf(ws.Cells(y, pcount).Value = Nothing, "", ws.Cells(y, pcount).Value)
                    listenggineering.Check_Ru1_Supply_Cntry_Code1 = IIf(ws.Cells(y, pcount + 2).Value = Nothing, "", ws.Cells(y, pcount + 2).Value)
                    listenggineering.Check_Ru1_KD1 = IIf(ws.Cells(y, pcount + 3).Value = Nothing, "", ws.Cells(y, pcount + 3).Value)
                    listenggineering.Check_Ru1_PID1 = IIf(ws.Cells(y, pcount + 4).Value = Nothing, "", ws.Cells(y, pcount + 4).Value)
                    listenggineering.Check_Ru1_Shipping_unit_IKP = IIf(ws.Cells(y, pcount + 5).Value = Nothing, "", ws.Cells(y, pcount + 5).Value)
                    listenggineering.Supply_Cntr_Code = IIf(ws.Cells(y, pcount + 6).Value = Nothing, "", ws.Cells(y, pcount + 6).Value)
                    listenggineering.Partition_Classification = IIf(ws.Cells(y, pcount + 7).Value = Nothing, "", ws.Cells(y, pcount + 7).Value)
                    'listenggineering.Splr_From = IIf(ws.Cells(y, pcount + 8).Value = Nothing, "", ws.Cells(y, pcount + 8).Value)

                    'supplier from if value nothing
                    Try
                        If Not String.IsNullOrEmpty(ws.Cells(y, pcount + 8).Value) Then
                            listenggineering.Splr_From = ws.Cells(y, pcount + 8).Value
                        End If
                    Catch ex As Exception
                        listenggineering.Splr_From = ""
                    End Try

                    'supplier from if value #N/A
                    Try
                        If ws.Cells(y, pcount + 8).Value.ToString.Trim <> "#N/A" Then
                            listenggineering.Splr_From = ws.Cells(y, pcount + 8).Value
                        End If
                    Catch ex As Exception
                        listenggineering.Splr_From = ""
                    End Try


                    'supplier to if value nothing
                    Try
                        If Not String.IsNullOrEmpty(ws.Cells(y, pcount + 9).Value) Then
                            listenggineering.Splr_To = ws.Cells(y, pcount + 9).Value
                        End If
                    Catch ex As Exception
                        listenggineering.Splr_To = ""
                    End Try

                    'supplier to if value #N/A
                    Try
                        If ws.Cells(y, pcount + 9).Value.ToString.Trim <> "#N/A" Then
                            listenggineering.Splr_To = ws.Cells(y, pcount + 9).Value
                        End If
                    Catch ex As Exception
                        listenggineering.Splr_To = ""
                    End Try


                    'If ws.Cells(y, pcount + 8).Value.ToString.Trim = "#N/A" Or String.IsNullOrEmpty(ws.Cells(y, pcount + 8).Value) Then
                    '    listenggineering.Splr_From = ""
                    'Else
                    '    listenggineering.Splr_From = ws.Cells(y, pcount + 8).Value
                    'End If


                    'If ws.Cells(y, pcount + 9).Value.ToString.Trim = "#N/A" Then
                    '    listenggineering.Splr_To = ""
                    'Else
                    '    listenggineering.Splr_To = ws.Cells(y, pcount + 9).Value
                    'End If

                    'listenggineering.Splr_To = IIf(ws.Cells(y, pcount + 9).Value = Nothing, "", ws.Cells(y, pcount + 9).Value)
                    listenggineering.Co_New = IIf(ws.Cells(y, pcount + 10).Value = Nothing, "", ws.Cells(y, pcount + 10).Value)
                    listenggineering.Source_of_Diversion = IIf(ws.Cells(y, pcount + 11).Value = Nothing, "", ws.Cells(y, pcount + 11).Value)
                    listenggineering.Product_Number = IIf(ws.Cells(y, pcount + 12).Value = Nothing, "", ws.Cells(y, pcount + 12).Value)
                    listenggineering.Plan_Change_Setting = IIf(ws.Cells(y, pcount + 13).Value = Nothing, "", ws.Cells(y, pcount + 13).Value)
                    listenggineering.ECR_No = IIf(ws.Cells(y, pcount + 14).Value = Nothing, "", ws.Cells(y, pcount + 14).Value)
                    listenggineering.Remarks = IIf(ws.Cells(y, pcount + 15).Value = Nothing, "", ws.Cells(y, pcount + 15).Value)
                    listenggineering.IAMI_sourcing = IIf(ws.Cells(y, pcount + 17).Value = Nothing, "", ws.Cells(y, pcount + 17).Value)

                    pError = ""
                    ClsEngineeringPartListDB.Insert(listenggineering, pError)
                    If pError <> "" Then
                        pNoError = CInt(pNoError + 1)
                        MemoMessage.Text = MemoMessage.Text & pNoError & " " & pError & " ||" & vbCrLf
                    End If


                    Dim dcount As Integer = 20
                    For px = 1 To pno
                        listenggineering.Variant_Supply_Cntry_Code = IIf(ws.Cells(y, dcount + 1).Value = Nothing, "", ws.Cells(y, dcount + 1).Value)
                        listenggineering.Variant_KD = IIf(ws.Cells(y, dcount + 2).Value = Nothing, "", ws.Cells(y, dcount + 2).Value)
                        listenggineering.Variant_PID = IIf(ws.Cells(y, dcount + 3).Value = Nothing, "", ws.Cells(y, dcount + 3).Value)
                        dcount = dcount + 3

                        pError = ""
                        ClsEngineeringPartListDB.InsertVar(listenggineering, px, pError)
                        If pError <> "" Then
                            pNoError = CInt(pNoError + 1)
                            MemoMessage.Text = MemoMessage.Text & pNoError & " " & pError & " ||" & vbCrLf
                        End If
                    Next

                    If pNoError = 0 Then
                        ProgressBar.Position = (y * 100) / ws.Dimension.End.Row
                        ProgressBar.Value = (y * 100) / ws.Dimension.End.Row
                    End If
                    ProgressBar.ShowPosition = True

                    Session("idx") = y

                Next
                If pNoError > 0 Then
                    BtnSubmit.ClientEnabled = True
                    popUp.ShowOnPageLoad = False
                    show_error(MsgTypeEnum.ErrorMsg, pNoError, 1)
                    Exit Function
                End If
                Scope.Complete()
                MemoMessage.Text = "Upload Engineering Part List Successfully"
                popUp.ShowOnPageLoad = False
            End Using


        Catch ex As Exception
            popUp.ShowOnPageLoad = False
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 1)
            'MemoMessage.Text = MemoMessage.Text & Session("idx") + 1 & " " & IIf(ex.Message.Contains("ExcelErrorValue"), "Please Change Value : #N/A ", ex.Message)
        End Try
    End Function
    'Private Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSubmit.Click
    '    If validation() = True Then
    '        Uploader.Enabled = True
    '        ProgressBar.Value = 0
    '        importExcel()
    '        BtnSubmit.Enabled = True
    '        BtnSaveError.Enabled = True
    '    End If
    'End Sub
    Protected Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If validation() = True Then
            Uploader.Enabled = True
            ProgressBar.Value = 0
            importExcel()
            BtnSubmit.Enabled = True
            BtnSaveError.Enabled = True
        End If
    End Sub
    
    Private Function validation() As Boolean
        validation = True
        If cboProject.Text = "" Then
            show_error(MsgTypeEnum.ErrorMsg, "Please select Project Name", 1)
            cboProject.Focus()
            validation = False
        End If
    End Function

    Private Sub BtnSaveError_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSaveError.Click
        up_Excel()
    End Sub
    Private Sub up_Excel()
        Dim NameHeader As String = ""
        If MemoMessage.Text = "" Then
            Exit Sub
        End If
        Using Pck As New ExcelPackage
            Dim ws As ExcelWorksheet = Pck.Workbook.Worksheets.Add("Sheet1")
            With ws
                .Cells(1, 1, 1, 1).Value = Split(MemoMessage.Text, "||")(0)
                For iRow = 1 To Split(MemoMessage.Text, "||").Count - 1
                    Dim r As Integer = iRow + 1
                    .Cells(r, 1, r, 1).Value = Split(MemoMessage.Text, "||")(iRow)
                Next

                Dim rg As OfficeOpenXml.ExcelRange = .Cells(1, 1, Split(MemoMessage.Text, "||").Count, 1)
                rg.Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Font.Name = "Tahoma"
                rg.Style.Font.Size = 10

                Dim rgHeader As OfficeOpenXml.ExcelRange = .Cells(1, 1, 1, 1)
                rgHeader.Style.Fill.PatternType = Style.ExcelFillStyle.Solid
                rgHeader.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue)
            End With

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment; filename=Save_Log_" & Format(Date.Now, "yyyy-MM-dd HH:mm:ss") & ".xlsx")
            'Response.BufferOutput = True
            Dim stream As MemoryStream = New MemoryStream(Pck.GetAsByteArray())

            Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length)
            Response.Flush()
            Response.Close()

        End Using
    End Sub

    Private Sub BtnClearLog_Click(sender As Object, e As System.EventArgs) Handles BtnClearLog.Click
        MemoMessage.Text = ""
    End Sub

 
 

End Class
﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewCostEstimation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        up_LoadReport()
        ' up_LoadReport_Resume()
    End Sub

    Private Sub up_LoadReport()
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptCostEstimation
        Dim pCENumber As String
        Dim pAction As String
        Dim pRev As Integer
        pAction = Session("Action")
        pCENumber = Session("CENumber")
        pRev = Session("Revision")

        Try
            ASPxDocumentViewer1.Report = Nothing

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_CE_CostEstimationReport '" & pCENumber & "' , " & pRev & ""

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "CostEstimation_" & Format(CDate(Now), "yyyyMMdd_HHmmss")

                'Dim sql2 As String
                'Dim ds2 As New DataSet
                'sql2 = "exec sp_CostEstimationResume_Report '" & pCENumber & "' , " & pRev & ""

                'Dim da2 As New SqlDataAdapter(sql2, con)
                'da2.Fill(ds2)
                'Report.XrSubreport1.Report.DataSource = ds2
                'Report.XrSubreport1.Report.ApplyFiltering()
                ASPxDocumentViewer1.Report = Report
                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception
            ASPxDocumentViewer1.Report = Nothing
            ASPxDocumentViewer1.DataBind()
        End Try
    End Sub

    Private Sub up_LoadReport_Resume()
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptCostEstimation_Resume
        Dim pCENumber As String
        Dim pRev As Integer

        pCENumber = Session("CENumber")
        pRev = Session("Revision")

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_CostEstimationResume_Report '" & pCENumber & "' , " & pRev & ""

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "CostEstimationResume_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                Report.Parameters("CENumber").Value = pCENumber
                Report.Parameters("Rev").Value = pRev
                ' ASPxDocumentViewer1.Report = Report
                'ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim pCENumber As String
        Dim Rev As Integer
        Dim RFQSet As String
        Dim pAction As String
        pAction = Session("Action")
        pCENumber = Session("CENumber")
        Rev = Session("Revision")
        RFQSet = Session("RFQSet")

        If pAction = "1" Then
            Response.Redirect("~/CEDetail.aspx?ID=" & pCENumber & "|" & Rev & "|" & RFQSet)
        Else
            Response.Redirect("~/CEApprovalDetail.aspx?ID=" & pCENumber & "|" & Rev & "|" & RFQSet)

        End If

    End Sub

End Class
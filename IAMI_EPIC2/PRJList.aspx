﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PRJList.aspx.vb"
    Inherits="IAMI_EPIC2.PRJList" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };
        function MessageBox(s, e) {
            //alert(s.cpmessage);
            if (s.cbMessage == "Download Excel Successfully") {
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cbMessage == "Request Date To must be higher than Request Date From") {
                toastr.success(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function ItemCodeValidation(s, e) {
            if (ItemCode.GetValue() == null) {
                e.isValid = false;
            }
        }

        function GridLoad() {
            Grid.PerformCallback('refresh');
            tmpProjectType.SetText(cboProjectType.GetValue());
        }

        function SetCode(s, e) {
            tmpProjectType.SetText(cboProjectType.GetValue());
        }

    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        .style1
        {
            height: 30px;
        }
        .style2
        {
            height: 50px;
        }
        .style3
        {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%; border: 1px solid black; height: 100px;">
        <tr style="height: 2px">
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td style="width: 70px">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 20px">
            <td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Date">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="width: 110px">
                <dx:ASPxDateEdit ID="ProjectDate_From" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="ProjectDate_From" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <ClientSideEvents ValueChanged="GridLoad" />
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>
            </td>
            <td style="padding: 0px 0px 0px 2px; width: 30px; text-align: center;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="~">
                </dx:ASPxLabel>
            </td>
           
            <td style="width: 110px">
                <dx:ASPxDateEdit ID="ProjectDate_To" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="ProjectDate_To" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <ClientSideEvents ValueChanged="GridLoad" />
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>
            </td>
        </tr>
        <tr style="height: 50px;">
            <td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Type">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="width: 110px" colspan="5">
                <dx:ASPxComboBox ID="cboProjectType" runat="server" ClientInstanceName="cboProjectType"
                    Width="240px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDownList" 
                    IncrementalFilteringMode="Contains" Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0px 0px 10px" colspan="6" class="style1">
                <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnShowData" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
                        var startdate = ProjectDate_From.GetDate(); // get value dateline date
                        var enddate = ProjectDate_To.GetDate(); // get value dateline date
                       
                        if (startdate == null)
                        {
	                        toastr.warning('Please input valid PR Start Date', 'Warning');
	                        toastr.options.closeButton = false;
	                        toastr.options.debug = false;
	                        toastr.options.newestOnTop = false;
	                        toastr.options.progressBar = false;
	                        toastr.options.preventDuplicates = true;
	                        toastr.options.onclick = null;
	                        e.processOnServer = false;
	                        return;                                
                        }
                        else if (enddate == null)
                        {
	                        toastr.warning('Please input valid PR End Date', 'Warning');
	                        toastr.options.closeButton = false;
	                        toastr.options.debug = false;
	                        toastr.options.newestOnTop = false;
	                        toastr.options.progressBar = false;
	                        toastr.options.preventDuplicates = true;
	                        toastr.options.onclick = null;
	                        e.processOnServer = false;
	                        return;                                
                        }
                                
                        var yearstartdate = startdate.getFullYear(); // where getFullYear returns the year (four digits)
                        var monthstartdate = startdate.getMonth(); // where getMonth returns the month (from 0-11)
                        var daystartdate = startdate.getDate();   // where getDate returns the day of the month (from 1-31)

                        if (daystartdate &lt; 10) {
                            daystartdate = '0'+ daystartdate  
                        }

                        if (monthstartdate &lt; 10) {
                            monthstartdate = '0'+ monthstartdate  
                        }

                        var vStartDate = yearstartdate + '-' + monthstartdate + '-' + daystartdate;

                                
                        var yearenddate = enddate.getFullYear(); // where getFullYear returns the year (four digits)
                        var monthenddate = enddate.getMonth(); // where getMonth returns the month (from 0-11)
                        var dayenddate = enddate.getDate();   // where getDate returns the day of the month (from 1-31)

                        if (dayenddate &lt; 10) {
                            dayenddate = '0'+ dayenddate  
                        }

                        if (monthenddate &lt; 10) {
                            monthenddate = '0'+ monthenddate  
                        }

                        var vEndDate = yearenddate + '-' + monthenddate + '-' + dayenddate;

                        if (vEndDate &lt; vStartDate) {
                            toastr.warning('End date must be bigger than Start Date', 'Warning');
                            toastr.options.closeButton = false;
                            toastr.options.debug = false;
                            toastr.options.newestOnTop = false;
                            toastr.options.progressBar = false;
                            toastr.options.preventDuplicates = true;
                            toastr.options.onclick = null;
                            e.processOnServer = false;
                            return;
                        }
	                    Grid.PerformCallback('showdata');
                            }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;
                <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnDownload" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;
                <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False" ClientInstanceName="btnAdd"
                    Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 2px">
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td class="hidden-div">
                <dx:ASPxTextBox ID="tmpProjectType" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                    Width="170px" ClientInstanceName="tmpProjectType" MaxLength="15" Theme="Office2010Silver">
                </dx:ASPxTextBox>
            </td>
            <td style="width: 70px">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div style="padding: 5px 0px 5px 0px">
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="select '00' Code,'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'ProjectType'">
        </asp:SqlDataSource>
        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
            <ClientSideEvents CallbackComplete="SetCode" />
        </dx:ASPxCallback>
        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
        </dx:ASPxGridViewExporter>
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%"
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Project_ID">
            <ClientSideEvents EndCallback="OnEndCallback" CustomButtonClick="function(s, e) {
	              if(e.buttonID == &#39;Detail&#39;){ 
                        var rowKey = Grid.GetRowKey(e.visibleIndex);
                        window.location.href= &#39;AddPRJList.aspx?ID=&#39; + rowKey;
                  }
                  else if (e.buttonID == &#39;detailList&#39;){
                        var rowKey = Grid.GetRowKey(e.visibleIndex);
                        window.location.href= &#39;PRJListDetail.aspx?ID=&#39; + rowKey;
                  }
                  else if (e.buttonID == &#39;btnDeleteGrid&#39;){
                   if (confirm('Are you sure want to delete ?')) {
                        var rowKey = Grid.GetRowKey(e.visibleIndex);
                          Grid.PerformCallback('delete |' + rowKey);
                    } else {
                     
                    }
                  }
                 
                  }"></ClientSideEvents>
            <Columns>
                <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" Width="150px">
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="Detail" Text="Edit">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="btnDeleteGrid" Text="Delete">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="detailList" Text="Detail List">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Project Id" VisibleIndex="1" FieldName="Project_ID"
                    Width="150px">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Project Name" FieldName="Project_Name" VisibleIndex="2"
                    Width="150px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Project Date" VisibleIndex="3" FieldName="Project_Date"
                    Width="120px">
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="true" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Project Type" VisibleIndex="4" FieldName="Project_Type">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Project Status" VisibleIndex="5" Width="130px"
                    FieldName="Project_Status">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Start Period" VisibleIndex="6" FieldName="Start_Period">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="End Period" VisibleIndex="7" FieldName="End_Period">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="USD/IDR" VisibleIndex="8" FieldName="Rate_USD_IDR">
                 <PropertiesTextEdit DisplayFormatString="#,###.00">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="YEN/IDR" VisibleIndex="9" FieldName="Rate_YEN_IDR">
                    <PropertiesTextEdit DisplayFormatString="#,###.00">
                      </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="BTH/IDR" VisibleIndex="10" FieldName="Rate_BATH_IDR">
                    <PropertiesTextEdit DisplayFormatString="#,###.00">
                      </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="OTS Sample" VisibleIndex="11" FieldName="Shec_OTS_Sample">
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="SOP Timing" VisibleIndex="12" FieldName="Shec_SOP_Timing">
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="13" FieldName="Register_By">
                    <Settings AllowAutoFilter="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="14" FieldName="Register_Date">
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="15" FieldName="Update_By">
                    <Settings AllowAutoFilter="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="16" FieldName="Update_Date">
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollableHeight="300" HorizontalScrollBarMode="Auto"
                VerticalScrollBarMode="Auto"></Settings>
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header HorizontalAlign="Center">
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption>
                    <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
                </EditFormColumnCaption>
            </Styles>
             <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
        </dx:ASPxGridView>

    </div>
</asp:Content>

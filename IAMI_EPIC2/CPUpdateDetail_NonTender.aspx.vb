﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO
Imports System.Transactions
Imports System.Web.UI
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxGridView

Public Class CPUpdateDetail_NonTender
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
    Dim ls_SupplierCode As String
#End Region

#Region "PROCEDURE"
    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
        ls_SupplierCode = GetSupplierCodeByUserLogin()

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_List", "SupplierCode|CPNumber|CENumber", ls_SupplierCode & "|" & txtCPNumber.Text & "|" & txtCENumber.Text, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub SetInformation(ByVal pObj As Object, Optional pCalledFromCPList As Boolean = False)
        Dim ds As New DataSet
        Dim ds1 As New DataSet
        Dim ErrMsg As String = ""
        Dim Supplier_Code As String = GetSupplierCodeByUserLogin()

        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_SetHeader", "CPNumber|CENumber|Rev|Supplier_Code", txtCPNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text & "|" & Supplier_Code, ErrMsg)
            ds1 = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_GetSupplierNotes", "CPNumber|CENumber|Rev|SupplierCode", txtCPNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text & "|" & Supplier_Code, ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_SetHeader", "CPNumber|CENumber|Rev|Supplier_Code", Split(Request.QueryString(0), "|")(0) & "|" & Split(Request.QueryString(0), "|")(2) & "|" & Split(Request.QueryString(0), "|")(1) & "|" & Supplier_Code, ErrMsg)
            ds1 = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_GetSupplierNotes", "CPNumber|CENumber|Rev|SupplierCode", Split(Request.QueryString(0), "|")(0) & "|" & Split(Request.QueryString(0), "|")(2) & "|" & Split(Request.QueryString(0), "|")(1) & "|" & Supplier_Code, ErrMsg)
        End If


        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                pObj.JSProperties("cpCPNumber") = ds.Tables(0).Rows(0).Item("CP_Number").ToString()
                pObj.JSProperties("cpRev") = ds.Tables(0).Rows(0).Item("Rev").ToString()
                pObj.JSProperties("cpCENumber") = ds.Tables(0).Rows(0).Item("CE_Number")
                pObj.JSProperties("cpNotes") = ds.Tables(0).Rows(0).Item("Notes").ToString()
                pObj.JSProperties("cpSupplierNotes") = ds1.Tables(0).Rows(0).Item("Supplier_Notes").ToString()
            Else
                pObj.JSProperties("cpCPNumber") = ""
                pObj.JSProperties("cpRev") = ""
                pObj.JSProperties("cpCENumber") = ""
                pObj.JSProperties("cpNotes") = ""
                pObj.JSProperties("cpSupplierNotes") = ""
            End If

            If pCalledFromCPList = True Then
                'add this parameter to prevent reset value of Notes when callback
                pObj.JSProperties("cpCalledFromCPList") = "1"
            End If

            Dim ds2 As New DataSet
            Dim ErrMsg2 As String = ""
            Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
            ls_SupplierCode = GetSupplierCodeByUserLogin()

            ds2 = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_List", "SupplierCode|CPNumber|CENumber", ls_SupplierCode & "|" & txtCPNumber.Text & "|" & txtCENumber.Text, ErrMsg)

            If ErrMsg2 = "" Then
                Grid.DataSource = ds2.Tables(0)
                Grid.DataBind()

                If ds2.Tables(0).Rows.Count = 0 Then
                    DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
                End If
            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg2, Grid)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, pObj)
        End If
    End Sub

    Private Sub up_UpdateSupplierNotes()
        Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
        Dim idx As Integer = 0

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                Dim sqlComm As New SqlCommand()
                Dim Supplier_Code As String = GetSupplierCodeByUserLogin()
                ls_SQL = "sp_CP_Update_SupplierNotes "
                sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("CPNumber", txtCPNumber.Text)
                sqlComm.Parameters.AddWithValue("Rev", txtRev.Text)
                sqlComm.Parameters.AddWithValue("CENumber", txtCENumber.Text)
                sqlComm.Parameters.AddWithValue("SupplierCode", Supplier_Code)
                sqlComm.Parameters.AddWithValue("SupplierNotes", suppNote.Text)
                sqlComm.Parameters.AddWithValue("RegUser", pUser)
                sqlComm.ExecuteNonQuery()
                sqlComm.Dispose()


                sqlTran.Commit()
            End Using
        End Using
    End Sub

#End Region

#Region "FUNCTIONS"
    Private Function GetSupplierCodeByUserLogin() As String
        Dim retVal As String = ""
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "SELECT Supplier_Code = ISNULL(Supplier_Code,'') FROM UserSetup WHERE UserID = '" & Session("user") & "'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = ds.Tables(0).Rows(0).Item("Supplier_Code")
                Else
                    'NOT EXISTS
                    retVal = ""
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataExist() As String
        Dim retVal As String = ""
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Exist", "CPNumber", txtCPNumber.Text, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = ds.Tables(0).Rows(0).Item("Ret")
                Else
                    'NOT EXISTS
                    retVal = "N"
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataSubmitted() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "exec sp_CP_Update_IsDataSubmitted '" & txtCPNumber.Text & "','" & txtCENumber.Text & "', '" & txtRev.Text & "','" & pUser & "'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    'for getdate format
    Private Function ConvertToFormatDate_yyyyMMdd(ByVal pDate As String) As String
        Dim retVal As String = "1900-01-01"
        Dim pYear As String = Split(pDate, "-")(0)
        Dim pMonth As String = Strings.Right("0" & Split(pDate, "-")(1), 2)
        Dim pDay As String = Strings.Right("0" & Split(pDate, "-")(2), 2)

        retVal = pYear & "-" & pMonth & "-" & pDay

        Return retVal
    End Function
#End Region

#Region "EVENTS"
    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|F020", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                          "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, txtCENumber)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "COUNTER PROPOSAL UPDATE (NON TENDER) DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "F020")
        Dim Supplier_Code = GetSupplierCodeByUserLogin()
        gs_Message = ""
        If Page.IsCallback Then
            ucAtc.Enabled = False
        End If

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If Request.QueryString.Count > 0 Then
                Try
                    Session("CP_Number") = Split(Request.QueryString(0), "|")(0)
                    gs_CPNumber = Split(Request.QueryString(0), "|")(0)
                    txtCPNumber.Text = Split(Request.QueryString(0), "|")(0)
                    txtRev.Text = Split(Request.QueryString(0), "|")(1)
                    txtCENumber.Value = Split(Request.QueryString(0), "|")(2)

                    Dim ds As New DataSet
                    Dim ds1 As New DataSet
                    Dim ErrMsg As String = ""

                    If Request.QueryString.Count <= 0 Then
                        'Using Add Button
                        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_SetHeader", "CPNumber|CENumber|Rev|Supplier_Code", txtCPNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text & "|" & Supplier_Code, ErrMsg)
                        ds1 = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_GetSupplierNotes", "CPNumber|CENumber|Rev|SupplierCode", txtCPNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text & "|" & Supplier_Code, ErrMsg)
                    Else
                        'Using Detail Link from Grid
                        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_SetHeader", "CPNumber|CENumber|Rev|Supplier_Code", Split(Request.QueryString(0), "|")(0) & "|" & Split(Request.QueryString(0), "|")(2) & "|" & Split(Request.QueryString(0), "|")(1) & "|" & Supplier_Code, ErrMsg)
                        ds1 = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_GetSupplierNotes", "CPNumber|CENumber|Rev|SupplierCode", Split(Request.QueryString(0), "|")(0) & "|" & Split(Request.QueryString(0), "|")(2) & "|" & Split(Request.QueryString(0), "|")(1) & "|" & Supplier_Code, ErrMsg)
                    End If

                    If ds1.Tables(0).Rows.Count > 0 Then
                        memoNote.Text = ds1.Tables(0).Rows(0).Item("Notes").ToString
                        suppNote.Text = ds1.Tables(0).Rows(0).Item("Supplier_Notes").ToString
                    End If

                    Dim script As String = ""
                    If IsDataSubmitted() = True Then
                        btnDraft.Enabled = False
                        btnSubmit.Enabled = False

                        script = "suppNote.SetEnabled(false);"
                        ScriptManager.RegisterStartupScript(suppNote, suppNote.GetType(), "suppNote", script, True)
                        ScriptManager.RegisterStartupScript(btnDeleteAtc, btnDeleteAtc.GetType(), "btnDeleteAtc", "btnDeleteAtc.SetEnabled(false);", True)
                        ScriptManager.RegisterStartupScript(btnAddFile, btnAddFile.GetType(), "btnAddFile", "btnAddFile.SetEnabled(false);", True)
                        'GridAtc.Enabled = False
                        ucAtc.Enabled = False
                    Else
                        btnDraft.Enabled = True
                        btnSubmit.Enabled = True
                        script = "suppNote.SetEnabled(true);"
                        ScriptManager.RegisterStartupScript(suppNote, suppNote.GetType(), "suppNote", script, True)
                        ScriptManager.RegisterStartupScript(btnDeleteAtc, btnDeleteAtc.GetType(), "btnDeleteAtc", "btnDeleteAtc.SetEnabled(true);", True)
                        ScriptManager.RegisterStartupScript(btnAddFile, btnAddFile.GetType(), "btnAddFile", "btnAddFile.SetEnabled(true);", True)
                        'GridAtc.Enabled = False

                        ucAtc.Enabled = True

                    End If

                Catch ex As Exception
                    Response.Redirect("CPUpdateDetail_NonTender.aspx")
                End Try


                'Remove session after back from ViewCounterProposal.aspx
                If IsNothing(Session("F020_QueryString")) = False Then
                    Session.Remove("F020_QueryString")
                End If

            Else

            End If
        End If
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "BestPricePcs" Then
            'Editable
            If IsDataSubmitted() = True Then
                'Non-Editable
                e.Cell.BackColor = Color.LemonChiffon
            Else
                'Editable
                e.Cell.BackColor = Color.White
            End If
        Else
            'Non-Editable
            e.Cell.BackColor = Color.LemonChiffon
        End If
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Try
            If e.UpdateValues.Count > 0 Then
                Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user"), pErr As String = ""
                Dim idx As Integer = 0

                Dim lb_IsDataExist As String = IsDataExist()

                If lb_IsDataExist <> "Y" Then
                    Session("F020_ErrMsg") = "CP Number " & txtCPNumber.Text & " is invalid!"
                    Exit Sub
                End If


                Try
                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()

                        Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                            Dim sqlComm As New SqlCommand()

                            '#DETAIL
                            For idx = 0 To e.UpdateValues.Count - 1

                                ls_SQL = "EXEC sp_CP_Update_Detail_BestPrice "
                                ls_SQL = ls_SQL + _
                                              "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                              "'" & e.UpdateValues(idx).NewValues("Material_No").ToString() & "'," & _
                                              "'" & e.UpdateValues(idx).NewValues("BestPricePcs").ToString() & "'," & _
                                              "'" & UserLogin & "'"

                                sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                                sqlComm.CommandType = CommandType.Text
                                sqlComm.ExecuteNonQuery()
                                sqlComm.Dispose()
                            Next idx

                            sqlTran.Commit()
                        End Using
                    End Using
                    up_UpdateSupplierNotes()
                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, pErr, Grid)
                    Return
                End Try
            Else
                Exit Sub
            End If

        Catch ex As Exception
            Session("F020_ErrMsg") = Err.Description
        End Try
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim headerCaption As String = ""
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""
        Dim pfunction As String = Split(e.Parameters, "|")(0)


        If pfunction = "gridload" Then
            Call GridLoad()
        ElseIf pfunction = "save" Then
            Dim ds As New DataSet
            Dim ErrMsg As String = ""
            Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
            Dim ls_SQL As String = ""

            ls_SupplierCode = GetSupplierCodeByUserLogin()
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_List", "SupplierCode|CPNumber|CENumber", ls_SupplierCode & "|" & txtCPNumber.Text & "|" & txtCENumber.Text, ErrMsg)

            Grid.JSProperties("cpSaveData") = "1"

            Try
                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()

                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                        Dim sqlComm As New SqlCommand()

                        '#DETAIL
                        For iLoop = 0 To ds.Tables(0).Rows.Count - 1

                            ls_SQL = "EXEC sp_CP_Update_Detail_BestPrice "
                            ls_SQL = ls_SQL + _
                                          "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                          "'" & ds.Tables(0).Rows(iLoop)("Material_No").ToString() & "'," & _
                                          "'" & ds.Tables(0).Rows(iLoop)("BestPricePcs") & "'," & _
                                          "'" & pUser & "'"

                            sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()
                        Next iLoop

                        sqlTran.Commit()
                    End Using
                End Using
                up_UpdateSupplierNotes()
           

                If IsNothing(Session("F020_ErrMsg")) = True Then
                    Call SetInformation(Grid)
                    Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", Grid)
                Else
                    If Session("F020_ErrMsg") = "Please fill all of rows on the grid before save!" Then
                        Call DisplayMessage(MsgTypeEnum.Warning, Session("F020_ErrMsg"), Grid)
                        Session.Remove("F020_ErrMsg")
                        Exit Sub
                    End If
                End If
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
                Return
            End Try

        ElseIf pfunction = "draft" Then
            SetInformation(Grid)
        End If
      
    End Sub

    Private Sub cbExist_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbExist.Callback
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        Select Case e.Parameter
            Case "exist"
                Dim ret As String = "N"
                cbExist.JSProperties("cpParameter") = "exist"

                ret = IsDataExist()
                cbExist.JSProperties("cpResult") = ret

            Case "IsSubmitted"
                cbExist.JSProperties("cpParameter") = "IsSubmitted"
                If IsDataSubmitted() = True Then
                    cbExist.JSProperties("cpResult") = "1"
                Else
                    cbExist.JSProperties("cpResult") = "0"
                End If
            Case "Draft"
                'cbExist.JSProperties("cpMessage") = "Data updated successfully!"
                Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", cbExist)
            Case "submit"
                Dim ls_SQL As String = "", UserLogin As String = Session("user")
                Dim idx As Integer = 0
                ls_SupplierCode = GetSupplierCodeByUserLogin()
                up_UpdateSupplierNotes()
                Try
                    cbExist.JSProperties("cpParameter") = "submit"
                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()
                        Dim sqlComm
                        Dim sql = "sp_CP_Update_CheckExistAttachment_NonTender"
                        sqlComm = New SqlCommand(sql, sqlConn)
                        sqlComm.CommandType = CommandType.StoredProcedure
                        sqlComm.Parameters.AddWithValue("CP_Number", txtCPNumber.Text)
                        sqlComm.Parameters.AddWithValue("Rev", txtRev.Text)
                        sqlComm.Parameters.AddWithValue("SupplierCode", ls_SupplierCode)

                        Dim da1 As New SqlDataAdapter(sqlComm)
                        Dim ds1 As New DataSet
                        da1.Fill(ds1)

                        If ds1.Tables(0).Rows.Count < 1 Then
                            cbExist.JSProperties("cpResult") = "Atc"
                            Call DisplayMessage(MsgTypeEnum.Warning, "Please Upload Document Attachment First", cbExist)
                            Exit Sub
                        End If
                    End Using

                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()

                        Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                            ls_SQL = "exec sp_CP_Update_Submit '" & txtCPNumber.Text & "','" & txtCENumber.Text & "', '" & txtRev.Text & "','" & UserLogin & "'"

                            Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()

                            sqlTran.Commit()
                        End Using
                    End Using

                    Call SetInformation(cbExist)
                    cbExist.JSProperties("cpSubmit") = "Y"
                    Call DisplayMessage(MsgTypeEnum.Success, "Data submitted successfully!", cbExist)

                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                End Try
        End Select
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Try
            If Request.QueryString.Count > 0 Then
                Dim pDateFrom As String = ConvertToFormatDate_yyyyMMdd(Split(Request.QueryString(0), "|")(6)), _
                    pDateTo As String = ConvertToFormatDate_yyyyMMdd(Split(Request.QueryString(0), "|")(7)), _
                    pPRNumber As String = Split(Request.QueryString(0), "|")(8), _
                    pSupplierCode As String = Split(Request.QueryString(0), "|")(9), _
                    pCPNumber As String = Split(Request.QueryString(0), "|")(10), _
                    pRowPos As String = Split(Request.QueryString(0), "|")(11)
                Session("btnBack_CPUpdateDetail_NonTender") = pDateFrom & "|" & pDateTo & "|" & pPRNumber & "|" & pSupplierCode & "|" & pCPNumber & "|" & pRowPos
            End If

        Catch ex As Exception

        End Try

        Response.Redirect("CPUpdate_NonTender.aspx")
    End Sub

    'for attachment
#Region "Attachment"
    Private Sub GridLoadAttachment()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", RevNo As String = ""

        Try
            If txtRev.Text = "" Then
                RevNo = hf.Get("RevisionNo")
            Else
                RevNo = txtRev.Text
            End If

            ls_SupplierCode = GetSupplierCodeByUserLogin()
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_Detail_Attachment_NonTender", "CPNumber|Rev|SupplierCode", txtCPNumber.Text & "|" & RevNo & "|" & ls_SupplierCode, ErrMsg)

            If ErrMsg = "" Then
                GridAtc.DataSource = ds.Tables(0)
                GridAtc.DataBind()

                If ds.Tables(0).Rows.Count = 0 Then
                    'DisplayMessage(MsgTypeEnum.Info, "There is no attachment to show!", GridAtc)
                End If
            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridAtc)
            End If

        Catch ex As Exception
            ' DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridAtc)
        End Try
    End Sub

    'create sp insert/delete attachmnt update
    Private Sub SaveAttachment(ByVal pCPNumber As String, ByVal pRev As String, ByVal pSupplierCode As String, ByVal pFileName As String, ByVal pFilePath As String, pFileSize As Double)
        Dim ls_SQL As String = "", UserLogin As String = Session("user")

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                ls_SQL = "EXEC sp_CP_Update_Insert_Attachment_NonTender " & _
                         "'" & pCPNumber & "','" & pRev & "'," & _
                         "'" & pSupplierCode & "', " & _
                         "'" & pFileName & "','" & pFilePath & "','" & UserLogin & "'," & _
                         "'" & pFileSize & "'"
                Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                sqlComm.CommandType = CommandType.Text
                sqlComm.ExecuteNonQuery()
                sqlComm.Dispose()

                sqlTran.Commit()
            End Using
        End Using
    End Sub

    Private Sub DeleteAttachment(ByVal pDeleteOption As String, Optional pCPNumber As String = "", _
                                 Optional pRev As String = "", Optional pSupplierCode As String = "", _
                                 Optional pFileName As String = "", Optional pSeqNo As Integer = 1)
        Select Case pDeleteOption
            Case "database"
                Dim ls_SQL As String = ""

                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()

                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                        ls_SQL = "EXEC sp_CP_Update_DeleteAttachment_NonTender" & _
                                 "'" & pCPNumber & "','" & pRev & "','" & pSupplierCode & "','" & pFileName & "'," & pSeqNo & ""

                        Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                        sqlComm.CommandType = CommandType.Text
                        sqlComm.ExecuteNonQuery()
                        sqlComm.Dispose()

                        sqlTran.Commit()
                    End Using
                End Using

            Case "file"
                Dim sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                sqlConn.Open()

                Dim folderPath As String = Server.MapPath("~/AttachmentsCP/")
                Dim ds As New DataSet
                ds = GetDataSource(CmdType.SQLScript, "SELECT REPLACE(FIlePath,LEFT(FIlePath,16),'') FileName FROM CP_Attachment_NonTender WHERE CP_Number='" & pCPNumber & "' AND Rev='" & pRev & "' AND Supplier_Code='" & pSupplierCode & "' AND FileName='" & pFileName & "' AND Seq_No = '" & pSeqNo & "'")
                If ds.Tables(0).Rows.Count > 0 Then
                    If System.IO.File.Exists(folderPath & ds.Tables(0).Rows(0)("FileName").ToString().Trim()) = True Then
                        System.IO.File.Delete(folderPath & ds.Tables(0).Rows(0)("FileName").ToString().Trim())
                    End If
                End If

                ds.Dispose()
                sqlConn.Close()
        End Select
    End Sub

    Private Sub GridAtc_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles GridAtc.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub GridAtc_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridAtc.CustomCallback
        Call GridLoadAttachment()
    End Sub

    Private Sub GridAtc_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridAtc.HtmlDataCellPrepared
        'If e.DataColumn.FieldName = "FileType" Or e.DataColumn.FieldName = "FileName" Then
        'Editable
        e.Cell.BackColor = Color.LemonChiffon
        'Else
        'Non-Editable
        'e.Cell.BackColor = Color.White
        'End If
    End Sub

    Private Sub ucAtc_FileUploadComplete(sender As Object, e As DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs) Handles ucAtc.FileUploadComplete
        If IsNothing(e.UploadedFile.FileName) Then
            ucAtc.JSProperties("cpType") = "2"
            ucAtc.JSProperties("cpMessage") = "Upload file is required"
            Exit Sub
        End If
        If e.IsValid = False Then
            ucAtc.JSProperties("cpType") = "3"
            ucAtc.JSProperties("cpMessage") = "Invalid file!"
        Else
            Dim CPNumber As String = hf.Get("CPNumber")
            CPNumber = Replace(CPNumber, "/", "")
            CPNumber = Replace(CPNumber, "~", "")
            CPNumber = Replace(CPNumber, "\", "")
            CPNumber = Replace(CPNumber, ".", "")

            Dim Rev As String = hf.Get("Rev")
            ls_SupplierCode = GetSupplierCodeByUserLogin()

            Dim serverPath As String = Server.MapPath("~/AttachmentsCP/")
            Dim fileName As String = CPNumber & "_" & Format(CDate(Now), "yyyyMMdd_hhmmss") & ".pdf" 'e.UploadedFile.FileName
            Dim fullPath As String = String.Format("{0}{1}", serverPath, fileName)
            Dim SizeFile As Double = ((e.UploadedFile.ContentLength / 1024) / 1024)
            Dim FileName1 As String = ""

            Try
                Using conn As New SqlConnection(Sconn.Stringkoneksi)
                    conn.Open()
                    Dim Sql = "Select CASE WHEN ROUND(SUM(FileSize),2)=2 THEN ROUND(SUM(filesize),2) ELSE SUM(FileSize) END AS FileSize from CP_Attachment_NonTender where CP_Number='" & hf.Get("CPNumber") & "' AND Rev = '" & Rev & "' AND Supplier_Code = '" & ls_SupplierCode & "'  Group By CP_Number"
                    Dim cmd As New SqlCommand(Sql, conn)
                    cmd.CommandType = CommandType.Text
                    Dim da As New SqlDataAdapter(cmd)
                    Dim ds As New DataSet
                    da.Fill(ds)
                    If SizeFile > 2 Then
                        ucAtc.JSProperties("cpType") = "2"
                        ucAtc.JSProperties("cpMessage") = "File size must not exceed 2 MB"
                        Exit Sub
                    End If

                    If ds.Tables(0).Rows.Count > 0 Then
                        '    FileSizeDB = 10
                        '    If SizeFile > FileSizeDB Then
                        '        ucAtc.JSProperties("cpType") = "3"
                        '        ucAtc.JSProperties("cpMessage") = "File size limit max 10 MB"
                        '        Exit Sub
                        '    End If
                        'Else
                        Dim FileSizeDB = ds.Tables(0).Rows(0)("FileSize")
                        If (SizeFile + FileSizeDB) > 2 Then
                            ucAtc.JSProperties("cpType") = "2"
                            ucAtc.JSProperties("cpMessage") = "File size limit max 2 MB, remain size (" & IIf(Math.Ceiling((2 - FileSizeDB) * 1024) = 49, 48, Math.Ceiling((2 - FileSizeDB) * 1024)) & ") KB"
                            Exit Sub
                        End If
                    End If
                End Using
            Catch ex As Exception
                'ucAtc.JSProperties("cpType") = "3"
                'ucAtc.JSProperties("cpMessage") = ex.Message
            End Try
            Try
                'CHECK EXISTING FOLDER (Attachment)
                If (Not System.IO.Directory.Exists(serverPath)) Then
                    System.IO.Directory.CreateDirectory(serverPath)
                End If


                'SAVE ATTACHMENT
                e.UploadedFile.SaveAs(fullPath)

                ucAtc.JSProperties("cpType") = "1"
                ucAtc.JSProperties("cpMessage") = "Counter Proposal " & e.UploadedFile.FileName & " has been uploaded successfully!"

                Call SaveAttachment(hf.Get("CPNumber"), Rev, ls_SupplierCode, e.UploadedFile.FileName, "~/AttachmentsCP/" & fileName, SizeFile)

            Catch ex As Exception
                ucAtc.JSProperties("cpType") = "3"
                ucAtc.JSProperties("cpMessage") = Err.Description
            End Try
        End If
    End Sub

    Protected Sub keyFieldLink_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim link As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
        Dim container As GridViewDataItemTemplateContainer = TryCast(link.NamingContainer, GridViewDataItemTemplateContainer)

        If container.ItemIndex >= 0 Then
            link.Text = Split(container.KeyValue, "|")(0)
            link.Target = "_blank"
            link.NavigateUrl = Split(container.KeyValue, "|")(1)
        End If
    End Sub

    Private Sub cb_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cb.Callback
        Select Case e.Parameter
            Case "DeleteAttachment"
                Dim iData As Integer = 0
                Dim iDataSeqNo As Integer = 0
                Dim param As String = hf.Get("DeleteAttachment")
                Dim paramSeqNo As String = hf.Get("SeqNo")
                ls_SupplierCode = GetSupplierCodeByUserLogin()
                'If param <> "" Then param = Strings.Left(param, Len(param) - 1)

                'IDENTIFY THE NUMBER OF DATA FILE NAME
                For idx As Integer = 1 To Len(param)
                    If Strings.Mid(param, idx, 1) = "|" Then
                        iData = iData + 1
                    End If
                Next idx

                For idx2 As Integer = 1 To Len(paramSeqNo)
                    If Strings.Mid(paramSeqNo, idx2, 1) = "|" Then
                        iDataSeqNo = iDataSeqNo + 1
                    End If
                Next idx2
                'DELETE ATTACHMENT
                For idx As Integer = 0 To iData - 1
                    For idx2 As Integer = 0 To iDataSeqNo - 1
                        'file
                        Call DeleteAttachment("file", txtCPNumber.Text, txtRev.Text, ls_SupplierCode, Split(param, "|")(idx), Split(paramSeqNo, "|")(idx2))

                        'database
                        Call DeleteAttachment("database", txtCPNumber.Text, txtRev.Text, ls_SupplierCode, Split(param, "|")(idx), Split(paramSeqNo, "|")(idx2))
                    Next idx2

                Next idx

                cb.JSProperties("cpActionAfter") = "DeleteAtc"
                DisplayMessage(MsgTypeEnum.Success, "Data deleted successfully!", cb)

            Case Else
                Call SetInformation(cb)
        End Select
    End Sub

#End Region

#Region "Row grid updating"

    'Private Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles Grid.RowDeleting
    '    e.Cancel = True
    'End Sub

    Private Sub Grid_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles Grid.RowInserting
        e.Cancel = True
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
    End Sub
#End Region
#End Region


End Class
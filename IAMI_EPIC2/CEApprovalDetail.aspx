﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CEApprovalDetail.aspx.vb" Inherits="IAMI_EPIC2.CEApprovalDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

    function GetMessage(s, e) {
        if (s.cpMessage == "Data Has Been Approved Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            btnApprove.SetEnabled(false);
            btnReject.SetEnabled(false);
            txtApproveNote.SetEnabled(false);

			//comment 
            //millisecondsToWait = 1000;
            //setTimeout(function () {
            //    var pathArray = window.location.pathname.split('/');
            //    var url = window.location.origin + '/' + pathArray[1] + '/CEApproval.aspx';
            //    if (pathArray[1] == "CEApprovalDetail.aspx") {
            //        window.location.href = window.location.origin + '/CEApproval.aspx';
            //    }
            //    else {
            //        window.location.href = url;
            //   }
            //}, millisecondsToWait);
        }
        else if (s.cpMessage == "Data Has Been Reject Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            btnApprove.SetEnabled(false);
            btnReject.SetEnabled(false);
            txtApproveNote.SetEnabled(false);
            //comment 4/4/2019
			//millisecondsToWait = 1000;
            //setTimeout(function () {
            //    var pathArray = window.location.pathname.split('/');
            //    var url = window.location.origin + '/' + pathArray[1] + '/CEApproval.aspx';
            //    if (pathArray[1] == "CEApprovalDetail.aspx") {
            //        window.location.href = window.location.origin + '/CEApproval.aspx';
            //    }
            //   else {
            //        window.location.href = url;
            //    }
            //}, millisecondsToWait);
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }

    function ApproveProcess() {
        var msg = confirm('Are you sure want to approve this data ?');
        if (msg == false) {
            e.processOnServer = false;
            return;
        }

        cbApprove.PerformCallback();
                
    }

    function RejectProcess() {
        if (txtApproveNote.GetText() == '') {
            toastr.warning('Please input Notes for Reject!', 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            return;
        }

        var msg = confirm('Are you sure want to reject this data ?');
        if (msg == false) {
            e.processOnServer = false;
            return;
        }



        cbReject.PerformCallback();
    }

    function LoadCompleted(s, e) {
        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }

        window.setTimeout(function () {
            delete s.cpType;
        }, 10);
    }


    function OnTabChanging(s, e) {
        var tabName = (pageControl.GetActiveTab()).name;
        e.cancel = !ASPxClientEdit.ValidateGroup('group' + tabName);
    }
</script>

<style type="text/css">
        .style2
        {
            height: 20px;
        }
</style>
<style type="text/css">
        .tr-all-height
        {
            height: 30px;
        }
        
        .td-col-left
        {
            width: 100px;
            padding: 0px 0px 0px 10px;
        }
        
        .td-col-left2
        {
            width: 140px;
        }
        
        
        .td-col-mid
        {
            width: 10px;
        }
        .td-col-right
        {
            width: 200px;
        }
        .td-col-free
        {
            width: 20px;
        }
        
        .div-pad-fil
        {
            padding: 5px 5px 5px 5px;
        }
        
        .td-margin
        {
            padding: 10px;
        }
		.customHeaderAtc {
			height: 32px;
		}				  
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <div style="padding: 5px 5px 5px 5px">

    <div style="padding: 5px 5px 5px 5px">
         <table style="width: 100%; border: 1px solid black; height: 100px;">  
            <tr style="height: 10px">
                    <td colspan="5"> </td>
                    
            </tr>
            <tr class="tr-all-height">
                <td class="td-col-left">
                     <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" 
                         Font-Size="9pt" Text="CE Number">
                    </dx1:aspxlabel>                   
                </td>
                <td class="td-col-mid"></td>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <dx1:ASPxTextBox ID="txtCENumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                            Width="170px" ClientInstanceName="txtCENumber" MaxLength="15" 
                                            Height="25px" ReadOnly="True" BackColor="Silver" Font-Bold="True" >
                                </dx1:ASPxTextBox>
                            </td>
                            <td class="td-col-mid"></td>
                            <td class="td-col-right">
                                <dx1:ASPxTextBox ID="txtRev" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                                 Width="45px" ClientInstanceName="txtRev" MaxLength="20" 
                                                 Height="25px" BackColor="Silver" ReadOnly="True" Font-Bold="True" 
                                                 ForeColor="Black" HorizontalAlign="Center" >
     
                                </dx1:ASPxTextBox>   
                            </td>
                        </tr>
                    </table>
                </td>
           
                <td></td>
              
            </tr>    
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                     <dx1:aspxlabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" 
                         Font-Size="9pt" Text="CE Date">
                    </dx1:aspxlabel>                   
                </td>
                <td class="style2">&nbsp;</td>
                <td class="td-col-right" colspan="2">
           
                <dx:ASPxDateEdit ID="dtCEDate" runat="server" Theme="Office2010Black" 
                                        Width="100px" AutoPostBack="false" ClientInstanceName="dtCEDate"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" ReadOnly="True" >
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                </td>
                <td></td>
                
            </tr>    
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                     <dx1:aspxlabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" 
                         Font-Size="9pt" Text="PR No">
                    </dx1:aspxlabel>                   
                </td>
                <td class="style2">&nbsp;</td>
                <td class="td-col-right" colspan="2">
           
        <dx1:ASPxComboBox ID="cboPRNo" runat="server" ClientInstanceName="cboPRNo"
                            Width="220px" Font-Names="Segoe UI"  TextField="Code"
                            ValueField="Code" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" ReadOnly="True" 
                            Height="25px">                            
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	cboRFQSetNumber.PerformCallback(cboPRNo.GetText());
}" />
                            <Columns>            
                            <dx:ListBoxColumn Caption="RFQ Number" FieldName="Code" Width="100px" />
                        
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" ><Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td >

                     &nbsp;</td>
                
            </tr>    
            <tr class="tr-all-height">
                <td class="td-col-left">
                     <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="RFQ Set Number">
                    </dx1:aspxlabel>                   
                </td>
                <td class="td-col-mid"></td>
                <td colspan="2" class="td-col-right">
           
        <dx1:ASPxComboBox ID="cboRFQSetNumber" runat="server" ClientInstanceName="cboRFQSetNumber"
                            Width="220px" Font-Names="Segoe UI"  TextField="Code"
                            ValueField="Code" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" ReadOnly="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="RFQ Number" FieldName="Code" Width="100px" />
                        
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" ><Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
							<ClientSideEvents SelectedIndexChanged="function(s, e) { alert('a');
	Grid_QuotationAtc.PerformCallback();
}" />																	
                        </dx1:ASPxComboBox>
           
                </td>
                <td></td>
                
            </tr>    
            <tr style="height: 10px">
                    <td colspan="5"> </td>
                    
            </tr>
         </table>
    </div>
    <div>
         <dx:ASPxCallback ID="cbApprove" runat="server" ClientInstanceName="cbApprove">
                         <ClientSideEvents CallbackComplete="GetMessage" />
                                                        
                </dx:ASPxCallback>

                     <dx:ASPxCallback ID="cbReject" runat="server" ClientInstanceName="cbReject">
                         <ClientSideEvents CallbackComplete="GetMessage" />
                                                        
                </dx:ASPxCallback>
    
    </div>

    <div style="padding: 5px 5px 5px 5px">
    <dx:ASPxPageControl ID="pageControl" runat="server" ClientInstanceName="pageControl"
                ActiveTabIndex="1" EnableHierarchyRecreation="True" Width="100%">
                <ClientSideEvents ActiveTabChanging="OnTabChanging" />
                <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                </ActiveTabStyle>
                <TabPages>
                    <dx:TabPage Name="CostEstimation" Text="Cost Estimation">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControlTab1" runat="server">																		  
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black"  Width="100%" Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Item_Code" >
            <Columns>
                <dx:GridViewDataTextColumn Name="Qty" Caption="Quantity" VisibleIndex="3" FieldName="Qty"
                    Width="100px">
                    <PropertiesTextEdit MaxLength="6" DisplayFormatString="#,###.00">
                      <MaskSettings AllowMouseWheel="False" Mask="<0..99999g>.<00..99>" IncludeLiterals="DecimalSymbol"></MaskSettings>
                       
                        <Style HorizontalAlign="Right"></Style>
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataComboBoxColumn Name="ItemCode" Caption="Material No" FieldName="Item_Code" 
                    VisibleIndex="0" Width="150px" FixedStyle="Left">
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataTextColumn Name="Description" Caption="Description" 
                    VisibleIndex="1" Width="250px" FieldName="Description">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Name="Specification" Caption="Specification" VisibleIndex="2" 
                    Width="350px" FieldName="Specification">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Name="UOM" Caption="UoM" VisibleIndex="4" 
                    FieldName="UOM">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="5" Width="150px" 
                    FieldName="Supplier1">
                     <PropertiesTextEdit DisplayFormatString="#,###.00">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="6" Width="150px" 
                    FieldName="Supplier2">
                     <PropertiesTextEdit DisplayFormatString="#,###.00">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="7" Width="150px" 
                    FieldName="Supplier3">
                     <PropertiesTextEdit DisplayFormatString="#,###.00">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="8" Width="150px" 
                    FieldName="Supplier4">
                     <PropertiesTextEdit DisplayFormatString="#,###.00">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn VisibleIndex="9" Width="150px" 
                    FieldName="Supplier5">
                     <PropertiesTextEdit DisplayFormatString="#,###.00">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Name="PriceEst" Caption="Price Estimation" 
                    VisibleIndex="10" FieldName="PriceEst" Width="135px">
                    <PropertiesTextEdit MaxLength="6" DisplayFormatString="#,###.00">
                      <MaskSettings AllowMouseWheel="False" Mask="<0..99999g>.<00..99>" IncludeLiterals="DecimalSymbol"></MaskSettings>
                       
                        <Style HorizontalAlign="Right"></Style>
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Name="Amount" Caption="Amount" VisibleIndex="11" FieldName="Amount" 
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatString="#,###.00">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Right">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Condition Of Price" VisibleIndex="12" 
                    FieldName="Remarks" Width="250px" Name="Remarks" >
                        <PropertiesTextEdit MaxLength="100" ClientInstanceName="Remarks">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>                   
                </dx:GridViewDataTextColumn>
                
            </Columns>

                 <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" 
                EnableRowHotTrack="True" AllowSelectByRowClick="True" />
                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
             
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="200" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>
 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                     </dx:TabPage>
                    <dx:TabPage Name="Quotation" Text="Quotation">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControlTab2" runat="server">
                                <dx:ASPxGridView ID="Grid_QuotationAtc" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid_QuotationAtc"
                                    EnableTheming="True" Theme="Office2010Black" Width="100%" 
                                    Settings-VerticalScrollBarMode="Visible" Font-Names="Segoe UI" Font-Size="9pt"
                                    KeyFieldName="FileName;FilePath">
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Caption=" "
                                            Width="30px" Visible="false">
                                        </dx:GridViewCommandColumn>
                                        <%--<dx:GridViewDataCheckColumn Caption=" " Name="chk" VisibleIndex="0" Width="30px">
                                <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueType="System.Int32" >
                                </PropertiesCheckEdit>
                            </dx:GridViewDataCheckColumn>--%>
                                        <dx:GridViewDataTextColumn Caption="No" FieldName="No" Visible="true" VisibleIndex="1"
                                            Width="50px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Quotation No" FieldName="Quotation_No" VisibleIndex="2"
                                            Width="150px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="RFQ Number" FieldName="RFQ_Number" VisibleIndex="3"
                                            Width="150px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Supplier Name" FieldName="Supplier_Name" VisibleIndex="4"
                                            Width="200px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="File Name" FieldName="FileName" VisibleIndex="5"
                                            Width="500px">
                                            <DataItemTemplate>
                                                <dx:ASPxHyperLink ID="linkFileName" runat="server" OnInit="keyFieldLink_Init">
                                                </dx:ASPxHyperLink>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataTextColumn Caption="File Path" FieldName="FilePath" Visible="false"
                                            VisibleIndex="6" Width="0px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="File Size" FieldName="FileSize" Visible="false"
                                            VisibleIndex="7" Width="80px">
                                        </dx:GridViewDataTextColumn>
                                       
                                    </Columns>
                                    <SettingsEditing Mode="Batch">
                                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                                    </SettingsEditing>
                                    <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSort="False"
                                        EnableRowHotTrack="True" />
                                    <Settings HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" VerticalScrollableHeight="200" ShowStatusBar="Hidden" />
           
                                    <Styles>
                                        <Header CssClass="customHeaderAtc" HorizontalAlign="Center">
                                        </Header>
                                    </Styles>
                                    <ClientSideEvents CallbackError="function(s, e) {e.handled = true;}"  EndCallback="LoadCompleted" />
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
        </dx:ASPxPageControl>
    </div>


    <div style="padding: 5px 5px 5px 5px">
         <table style="width: 100%; border: 0px; height: 100px;">  
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:180px">
                        <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="CE Notes">
                    </dx1:aspxlabel>                   
                </td>
                <td></td>
                <td>
                    <dx1:ASPxMemo ID="txtNotes" runat="server" Height="50px" Width="500px"
                        ClientInstanceName="txtNotes" Font-Names="Segoe UI" Font-Size="9pt"
                        Theme="Metropolis" MaxLength="300" Enabled="false">
                    </dx1:ASPxMemo>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>  
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        <dx1:aspxlabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="IA CAPEX/OPEX">
                    </dx1:aspxlabel>                   
                </td>
                <td></td>
                <td> 
           
                        <dx:ASPxComboBox ID="cbIABudget" runat="server" ClientInstanceName="cbIABudget"
                            Width="173px" Font-Names="Segoe UI"  TextField="IA_BudgetNumber"
                            ValueField="IA_BudgetNumber" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" Enabled="false">
                            <ItemStyle Height="10px" Paddings-Padding="4px" ><Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>                        
                        </dx:ASPxComboBox>
                    
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>    
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>       
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">         
                <dx:ASPxCheckBox ID="chkSkip" runat="server" ClientInstanceName="chkSkip" 
                    Text="Skip Budget" ValueChecked="1" ValueType="System.Int32" ValueUnchecked="0" 
                        Font-Names="Segoe UI" Font-Size="9pt" CheckState="Unchecked" Enabled="false">
                    <ClientSideEvents CheckedChanged="CheckBoxSkip" />
                    
                    </dx:ASPxCheckBox></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        <dx1:aspxlabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="Notes/Reason">
                    </dx1:aspxlabel>                   
                </td>
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxMemo ID="txtReason" runat="server" Height="50px" Width="500px"
                        ClientInstanceName="txtReason" Font-Names="Segoe UI" Font-Size="9pt"
                        Theme="Metropolis" MaxLength="300" Enabled="false">
                        
                    </dx1:ASPxMemo>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        <dx1:aspxlabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="Document Upload">
                    </dx1:aspxlabel>                   
                </td>
                <td>&nbsp;</td>
                <td>
					 <dx:ASPxHyperLink ID="LinkFile"  ClientInstanceName="LinkFile" runat="server" Text="No File" >
                        <ClientSideEvents Click="function(s, e) {
                            //alert(LinkFile.GetText());

                            if (LinkFile.GetText() != 'No File' || LinkFile.GetText() != '') {
                                var fileName, fileExtension;
                                fileName = LinkFile.GetText();
                                fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));

                                if (fileExtension =='pdf'){
                                    var pathArray = window.location.pathname.split('/');
                                    var url = window.location.origin + '/' + pathArray[1] + '/Files/' + LinkFile.GetText();
                                  
                                    window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
                                }
                                var pathArray = window.location.pathname.split('/');

                                //alert(window.location.origin + '/Files/' + LinkFile.GetText());
                               
                                //var url = window.location.origin + '/' + pathArray[1] + '/ItemList.aspx';                                
                                //window.location.href = window.location.origin + '/ItemList.aspx';

                            }
	                       
                        }" />
                    </dx:ASPxHyperLink>
				    <asp:FileUpload ID="uploaderFile" runat="server" Font-Names="Segoe UI" 
						Font-Size="9pt" Height="20px" Width="250px" Visible="false"/>
           
                </td>
                 <td> 
                        <dx:ASPxCallback ID="cbFile" runat="server" ClientInstanceName="cbFile">
                            <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
                                                        
                        </dx:ASPxCallback>

                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       
            <tr>
                <td style=" padding:0px 0px 0px 10px; width:120px">

                    <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Approval Notes">
                    </dx1:ASPxLabel>                 

                </td>
                <td></td>
                <td colspan="2" rowspan="2">
                    <dx1:ASPxMemo ID="txtApproveNote" runat="server" Height="50px" Width="500px"
                        ClientInstanceName="txtApproveNote" Font-Names="Segoe UI" Font-Size="9pt"
                        Theme="Metropolis" MaxLength="200" Enabled="true">
                        
                    </dx1:ASPxMemo>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; " colspan="8">

                    <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>
                    &nbsp;
                    <dx1:ASPxButton ID="btnApprove" runat="server" Text="Approve" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnApprove" Theme="Default" >                        
                        <ClientSideEvents Click="ApproveProcess" />
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>
                    &nbsp;

                    <dx1:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnReject" Theme="Default" >                        
                     
                        
                     
                        <ClientSideEvents Click="RejectProcess" />
                     
                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>
                   &nbsp;
                        <dx1:ASPxButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="False" Font-Names="Segoe UI"
                            Font-Size="9pt" Width="80px" Height="25px" UseSubmitBehavior="False" ClientInstanceName="btnPrint"
                            Theme="Default">
                            <Paddings Padding="2px" />
                        </dx1:ASPxButton>

                    
                    </td>
            </tr>       
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>                                       
         </table>
    </div>

</div>

</asp:Content>

﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class DevelopmentListSupplierDetail

    '''<summary>
    '''btnBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBack As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''btnDownload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDownload As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''btnSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSubmit As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''GridExporter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridExporter As Global.DevExpress.Web.ASPxGridView.Export.ASPxGridViewExporter

    '''<summary>
    '''cbUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbUpdate As Global.DevExpress.Web.ASPxCallback.ASPxCallback

    '''<summary>
    '''cbSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbSubmit As Global.DevExpress.Web.ASPxCallback.ASPxCallback

    '''<summary>
    '''SqlDataSource4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSource4 As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''Grid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Grid As Global.DevExpress.Web.ASPxGridView.ASPxGridView

    '''<summary>
    '''btnDetails control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDetails As Global.DevExpress.Web.ASPxGridView.GridViewCommandColumnCustomButton

    '''<summary>
    '''popup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents popup As Global.DevExpress.Web.ASPxPopupControl.ASPxPopupControl

    '''<summary>
    '''PopupControlContentControl1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PopupControlContentControl1 As Global.DevExpress.Web.ASPxPopupControl.PopupControlContentControl

    '''<summary>
    '''lblProject control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProject As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''txtProjectID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProjectID As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''lblPartNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPartNo As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''txtPartNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPartNo As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''lblSupplier1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSupplier1 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboSupplier1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplier1 As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''lblSupplier2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSupplier2 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboSupplier2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplier2 As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''lblSupplier3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSupplier3 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboSupplier3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplier3 As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''lblSupplier4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSupplier4 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboSupplier4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplier4 As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''ASPxLabel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ASPxLabel1 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboSupplier5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplier5 As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''lblSupplier6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSupplier6 As Global.DevExpress.Web.ASPxEditors.ASPxLabel

    '''<summary>
    '''cboSupplier6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplier6 As Global.DevExpress.Web.ASPxEditors.ASPxComboBox

    '''<summary>
    '''btnUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpdate As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.DevExpress.Web.ASPxEditors.ASPxButton

    '''<summary>
    '''Master property.
    '''</summary>
    '''<remarks>
    '''Auto-generated property.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As IAMI_EPIC2.Site
        Get
            Return CType(MyBase.Master, IAMI_EPIC2.Site)
        End Get
    End Property
End Class

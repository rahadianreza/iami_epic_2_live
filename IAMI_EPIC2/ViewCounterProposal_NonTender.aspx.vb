﻿Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewCounterProposal_NonTender1
    Inherits System.Web.UI.Page


#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"
    Private Sub LoadReport()
        Dim ls_SQL As String = ""
        Dim CPNumber As String = "", CE_Number As String = ""
        Dim ds As New DataSet
        Dim Report As New rptCP_NonTender

        Try
            ASPxDocumentViewer1.Report = Nothing

            Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                sqlConn.Open()

                If Session("CP_calledFrom") = "F010" Then
                    'F010_paramViewCP sort index : 0 (CPNo), 1 (CENo) , 2 (revNo)
                    ls_SQL = "exec sp_CP_CounterProposal_NonTender_Report '" & Split(Session("F010_paramViewCP"), "|")(0) & "','" & Split(Session("F010_paramViewCP"), "|")(3) & "','" & Split(Session("F010_paramViewCP"), "|")(1) & "'" & vbCrLf

                    ' ls_SQL = "exec sp_CP_CounterProposal_NonTender_Report '" & Split(Session("F010_paramViewCP"), "|")(0) & "','" & Split(Session("F010_paramViewCP"), "|")(1) & "','" & Split(Session("F010_paramViewCP"), "|")(2) & "'" & vbCrLf
                ElseIf Session("CP_calledFrom") = "F020" Then
                    ls_SQL = "exec sp_CP_CounterProposal_NonTender_Report '" & Split(Session("F020_paramViewCP"), "|")(0) & "','" & Split(Session("F020_paramViewCP"), "|")(1) & "','" & Split(Session("F020_paramViewCP"), "|")(2) & "'" & vbCrLf
                ElseIf Session("CP_calledFrom") = "F030" Then
                    ls_SQL = "exec sp_CP_CounterProposal_NonTender_Report '" & Split(Session("F030_paramViewCP"), "|")(0) & "','" & Split(Session("F030_paramViewCP"), "|")(1) & "','" & Split(Session("F030_paramViewCP"), "|")(2) & "'" & vbCrLf
                End If

                Dim da As New SqlDataAdapter(ls_SQL, sqlConn)
                da.Fill(ds)
                Report.DataSource = ds.Tables(0)

                Report.Name = "CounterProposalNonTender_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report
                ASPxDocumentViewer1.DataBind()
            End Using

        Catch ex As Exception
            ASPxDocumentViewer1.Report = Nothing
            ASPxDocumentViewer1.DataBind()
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Master.SiteTitle = "COUNTER PROPOSAL LIST (NON TENDER) REPORT"
            pUser = Session("user")
            If Session("CP_calledFrom") = "F010" Then
                AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "F010")
            ElseIf Session("CP_calledFrom") = "F020" Then
                AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "F020")
            ElseIf Session("CP_calledFrom") = "F030" Then
                AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "F030")
            End If

            Call LoadReport()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Select Case Session("CP_calledFrom")
            Case "F010"
                'Session.Remove("CP_calledFrom")
                'If IsNothing(Session("F010_QueryString")) = True Then
                '    Response.Redirect("CPListDetail_NonTender.aspx")
                'Else
                '    Response.Redirect("CPListDetail_NonTender.aspx?" & Session("F010_QueryString"))
                'End If

                Session.Remove("CP_calledFrom")
                If IsNothing(Session("F010_QueryString")) = True Then
                    If IsNothing(Session("F010_paramViewCP")) = True Then
                        Response.Redirect("CPListDetail_NonTender.aspx")
                    Else
                        Response.Redirect("CPListDetail_NonTender.aspx?" & Session("F010_paramViewCP"))
                    End If

                Else
                    Response.Redirect("CPListDetail_NonTender.aspx?" & Session("F010_QueryString"))
                End If

            Case "F020"
                Session.Remove("CP_calledFrom")
                If IsNothing(Session("F020_QueryString")) = True Then
                    Response.Redirect("CPApprovalDetail_NonTender.aspx")
                Else
                    Response.Redirect("CPApprovalDetail_NonTender.aspx?" & Session("F020_QueryString"))
                End If

            Case "F030"
                Session.Remove("CP_calledFrom")
                If IsNothing(Session("F030_QueryString")) = True Then
                    Response.Redirect("CPAcceptanceDetail_NonTender.aspx")
                Else
                    Response.Redirect("CPAcceptanceDetail_NonTender.aspx?" & Session("F030_QueryString"))
                End If
        End Select

        Session.Remove("CP_calledFrom")
    End Sub
#End Region

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="IADGSparePart.aspx.vb" Inherits="IAMI_EPIC2.IADGSparePart" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    function GetMessage(s, e) {
        if (s.cpMessage == "There is no data to show!") {
            //alert(s.cpRowCount);
            toastr.info(s.cpMessage, 'Information');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            btnDownload.SetEnabled(false);

        } else if (s.cpMessage == "Download Excel Successfully!") {
            toastr.info(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            btnDownload.SetEnabled(true);
        } else if (s.cpMessage == "Success") {
            btnDownload.SetEnabled(true);
        } else {
            toastr.error(s.cpMessage, 'Error');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }
    
   
    function Validation() {
        if (cboProject.GetText() == '') {
            toastr.warning('Please Select Project !', 'Warning');
            cboProject.Focus();
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return;
        }
    }
</script>
<style type="text/css">
    .td-col-l
    {
        padding:0px 0px 0px 10px;
        width:100px;
    }
    .td-col-m
    {
        width:10px;
    }
    td.col-r
    {
        width:200px;
    }
    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
            <tr style="height:10px">
                <td class="td-col-l"></td>
                <td class="td-col-m"></td>
                <td class="td-col-r"></td>
                <td ></td>
                <td></td>
            </tr>
            <tr style="height:25px">
                <td style="padding:0px 0px 0px 10px" class="td-col-l">
                    <dx:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Project Name">
                    </dx:aspxlabel>
                </td>
                <td class="td-col-m"></td>
                <td class="td-col-r">
                    <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                        Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Project_Name"
                        ValueField="Project_ID" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                        DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                        Height="25px">
                        <Columns>
                            <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                            <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td colspan="2">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select Project_ID, Project_Name From Proj_Header Where Project_Type='PT03'">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr style="height:20px">
                <td colspan="5"></td>
            </tr>
            <tr style="height:35px">
                <td colspan="5" style="padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {
                            if (cboProject.GetText()=='')
                            {
                                toastr.warning('Please Select Project !', 'Warning');
                                cboProject.Focus();
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;
                            }
                            Grid.PerformCallback('load|' + cboProject.GetValue());
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp; &nbsp;
                     <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload"  Theme="Default">                        
                        <Paddings Padding="2px" />
                        <ClientSideEvents Click="Validation" />
                    </dx:ASPxButton>
                    &nbsp; &nbsp;
                     <dx:ASPxButton ID="btnUpload" runat="server" Text="Upload" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnUpload" Theme="Default">
                                                
                      <%--   <ClientSideEvents Click="function(s, e) {
	if (cboProject.GetText()==''){
		toastr.warning('Please Select Project !', 'Warning');
        cboProject.Focus();
        toastr.options.closeButton = false;
        toastr.options.debug = false;
        toastr.options.newestOnTop = false;
        toastr.options.progressBar = false;
        toastr.options.preventDuplicates = true;
        toastr.options.onclick = null;
        e.processOnServer = false;
        return;
	}

}" />--%>
                                                
                        <Paddings Padding="2px" />
                        
                    </dx:ASPxButton>&nbsp; &nbsp;
                    <dx:ASPxButton ID="btnTemplate" runat="server" Text="Template" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnTemplate" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr style="height:10px">
                <td colspan="5">
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                </td>
            </tr>
        </table>
    </div>
    <div style="padding:5px 5px 5px 5px">
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" KeyFieldName="ProjectID; ProjectYear; IADGPartNo" Theme="Office2010Black" Width="100%" 
                Font-Names="Segoe UI" Font-Size="9pt" >
                <ClientSideEvents EndCallback="GetMessage" />
             <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                    HorizontalScrollBarMode="Auto" />
             <Columns>
                 <dx:GridViewDataTextColumn Caption="No" FieldName="No" VisibleIndex="0" 
                     Width="50px">
                     <Settings AutoFilterCondition="Contains" AllowAutoFilter="False"/>
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" >
                         <Paddings PaddingLeft="5px" />
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="ProjectYear" Caption="Project Year" VisibleIndex="1"
                     Width="100px">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px" />
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="IADGPartNo" Width="120px" Caption="IADG PartNo"
                     VisibleIndex="2">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="PartName" Width="250px" Caption="Part Name"
                     VisibleIndex="3">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="ModelName" Width="100px" Caption="Model"
                     VisibleIndex="4">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="NewMulti" Width="100px" Caption="New / Multi"
                     VisibleIndex="5">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="SampleIADGCompare" Width="120px" Caption="Sample IADG Compare"
                     VisibleIndex="6">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="SampleOEM" Width="120px" Caption="Sample OEM"
                     VisibleIndex="7">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="VolumePerMonth" Caption="Volume / Month" VisibleIndex="8" Width="120px">
                     <PropertiesTextEdit DisplayFormatString="#,##0">
                     </PropertiesTextEdit>
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="CostTarget" Caption="Cost Target" VisibleIndex="9" >
                     <PropertiesTextEdit DisplayFormatString="#,##0">
                     </PropertiesTextEdit>
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="Priority" Caption="Priority" VisibleIndex="10" >
                     <PropertiesTextEdit DisplayFormatString="#,##0">
                     </PropertiesTextEdit>
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
             </Columns>
              <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
        </SettingsPager>
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
            HorizontalScrollBarMode="Auto" />
        <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
            <Header>
                <Paddings Padding="2px"></Paddings>
            </Header>
            <EditFormColumnCaption>
                <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
            </EditFormColumnCaption>
        </Styles>


         </dx:ASPxGridView>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Material_PlasticDetail.aspx.vb" 
Inherits="IAMI_EPIC2.Material_PlasticDetail" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;


            //alert(s.cpButtonText);
            btnSubmit.SetText(s.cpButtonText);
            cboMaterialType.SetEnabled(false);
            cboSupplier.SetEnabled(false);
            txtPeriod.SetEnabled(false);
            cboMaterialCode.SetEnabled(false);
            cboGroupItem.SetEnabled(false);
        }
       
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/Labor.aspx";
            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/Material_Plastic.aspx';
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                if (pathArray[1] == "Material_PlasticDetail.aspx") {
                    window.location.href = window.location.origin + '/Material_Plastic.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            cboMaterialType.SetEnabled(true);
            cboSupplier.SetEnabled(true);
            txtPeriod.SetEnabled(true);
            cboMaterialCode.SetEnabled(true);
            cboGroupItem.SetEnabled(true);

            cboMaterialType.SetText('');
            cboSupplier.SetText('');
            cboMaterialCode.SetText('');

            var today = new Date().toLocaleDateString();
            //var year = day.getFullYear();
            txtPeriod.SetText(today);

            cboGroupItem.SetText('');
            //cboCategory.SetText('');
            cboCurrency.SetText('');
            txtPrice.SetText('0');
            
        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }

    function ValueChange() {
        txtMaterialType.SetText(cboMaterialType.GetValue());
        txtSupplier.SetText(cboSupplier.GetValue());
        txtMaterialCode.SetText(cboMaterialCode.GetValue());
        txtGroupItem.SetText(cboGroupItem.GetValue());
        //txtCategory.SetText(cboCategory.GetValue());
        txtCurrency.SetText(cboCurrency.GetValue());
    }


    //filter combobox Supplier berdasarkan material type yang dipilih
    function FilterSupplier() {
        cboSupplier.PerformCallback('filter|' + cboMaterialType.GetValue());
    }

    function FilterGroupItem() {
        cboGroupItem.PerformCallback('filter|' + cboMaterialCode.GetValue());
    }

    function SubmitProcess(s, e) {
        if (s.GetText() == 'Update') {
            if (cboGroupItem.GetText() == '') {
                toastr.warning('Please Select Group Item !', 'Warning');
                cboGroupItem.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
//            if (cboCategory.GetText() == '') {
//                toastr.warning('Please Select Category !', 'Warning');
//                cboCategory.Focus();
//                toastr.options.closeButton = false;
//                toastr.options.debug = false;
//                toastr.options.newestOnTop = false;
//                toastr.options.progressBar = false;
//                toastr.options.preventDuplicates = true;
//                toastr.options.onclick = null;
//                e.processOnServer = false;
//                return;
//            }

            if (cboCurrency.GetText() == '') {
                toastr.warning('Please Select Currency !', 'Warning');
                cboCurrency.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (txtPrice.GetText() == '' || txtPrice.GetText() == 0) {
                toastr.warning('Price must greater than zero', 'Warning');
                txtPrice.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
           
        }
        else {
            if (cboMaterialType.GetText() == '') {
                toastr.warning('Please Select Material Type Code !', 'Warning');
                cboMaterialType.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            if (cboSupplier.GetText() == '') {
                toastr.warning('Please Select Supplier Code !', 'Warning');
                cboSupplier.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            if (cboMaterialCode.GetText() == '') {
                toastr.warning('Please Select Material Code !', 'Warning');
                cboMaterialCode.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (cboGroupItem.GetText() == '') {
                toastr.warning('Please Select Group Item !', 'Warning');
                cboGroupItem.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
//            if (cboCategory.GetText() == '') {
//                toastr.warning('Please Select Category !', 'Warning');
//                cboCategory.Focus();
//                toastr.options.closeButton = false;
//                toastr.options.debug = false;
//                toastr.options.newestOnTop = false;
//                toastr.options.progressBar = false;
//                toastr.options.preventDuplicates = true;
//                toastr.options.onclick = null;
//                e.processOnServer = false;
//                return;
//            }

            if (cboCurrency.GetText() == '') {
                toastr.warning('Please Select Currency !', 'Warning');
                cboCurrency.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (txtPrice.GetText() == '' || txtPrice.GetText() == 0) {
                toastr.warning('Price must greater than zero', 'Warning');
                txtPrice.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

        if (btnSubmit.GetText() != 'Update') {
            cbSave.PerformCallback('Save');
        } else {
            cbSave.PerformCallback('Update');

        }
        
    }

    </script>
    <style type="text/css">
        .td-col-l 
        {
            padding:0px 0px 0px 10px;
            width:120px;
        }
        .td-col-m
        {
            width:20px;
        }
        .td-col-r
        {
            width:200px;
        }
        
        .tr-height
        {
            height: 35px;
        }
            
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr style="height:10px">
            <td colspan="5"></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material Type Code">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m">
                &nbsp;
            </td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboMaterialType" runat="server" ClientInstanceName="cboMaterialType"
                    Width="170px" Font-Names="Segoe UI" TextField="MaterialTypeDesc" ValueField="MaterialTypeCode"
                    DataSourceID="SqlDataSource1" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <ClientSideEvents ValueChanged="function(s, e) {
	txtMaterialType.SetText(cboMaterialType.GetValue());
}"  SelectedIndexChanged="FilterSupplier" />
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="MaterialTypeCode" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="MaterialTypeDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="select Par_Code AS MaterialTypeCode,Par_Description AS MaterialTypeDesc from Mst_Parameter where Par_Group='MaterialType'">
                </asp:SqlDataSource>
            </td>
            <td></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Supplier Code">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboSupplier" runat="server" ClientInstanceName="cboSupplier" 
                    Width="170px" Font-Names="Segoe UI" TextField="SupplierName" ValueField="SupplierCode"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                    <ClientSideEvents ValueChanged="function(s, e) {
	txtSupplier.SetText(cboSupplier.GetValue());
}"  />
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="SupplierCode" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="SupplierName" Width="120px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
               <%-- <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT SupplierCode,SupplierName FROM VW_Mst_Material_getSupplier">
                </asp:SqlDataSource>--%>
            </td>
            <td>
               
            </td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="lblPeriod" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTimeEdit ID="txtPeriod" runat="server" ClientInstanceName="txtPeriod" 
                    DisplayFormatString="MMM yyyy" EditFormat="Custom" EditFormatString="MMM yyyy" 
                    EnableTheming="true" Font-Size="9pt" Theme="Office2010Black" Width="120px" Height="9px">
                    <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                        <Paddings Padding="4px" />
                    </ButtonStyle>
                </dx:ASPxTimeEdit>
            </td>
            <td></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material Code">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboMaterialCode" runat="server" ClientInstanceName="cboMaterialCode" DataSourceID="SqlDataSource4"
                    Width="170px" Font-Names="Segoe UI" TextField="MaterialDesc" ValueField="MaterialCode"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                    <ClientSideEvents ValueChanged="function(s, e) {
	txtMaterialCode.SetText(cboMaterialCode.GetValue());
}"  SelectedIndexChanged="FilterGroupItem"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="MaterialCode" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="MaterialDesc" Width="120px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) MaterialCode,RTRIM(Par_Description) MaterialDesc FROM Mst_Parameter WHERE Par_Group = 'MaterialCode' AND Par_ParentCode='MC04'">
                </asp:SqlDataSource>
           
            </td>
            <td></td>
        </tr>
      
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Group Item">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboGroupItem" runat="server" ClientInstanceName="cboGroupItem"
                    Width="170px" Font-Names="Segoe UI" TextField="GroupItemDesc" ValueField="GroupItem" 
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                     <ClientSideEvents ValueChanged="function(s, e) {
	txtGroupItem.SetText(cboGroupItem.GetValue());
}"  />
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="GroupItem" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="GroupItemDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) GroupItem,RTRIM(Par_Description) GroupItemDesc FROM Mst_Parameter WHERE Par_Group = 'MaterialGroupItem'">
                </asp:SqlDataSource>
            </td>
            <td></td>
        </tr>
 <%--        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Category">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboCategory" runat="server" ClientInstanceName="cboCategory"
                    Width="170px" Font-Names="Segoe UI" TextField="CategoryDesc" ValueField="Category" DataSourceID="SqlDataSource5"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <ClientSideEvents ValueChanged="function(s, e) {
	txtCategory.SetText(cboCategory.GetValue());
}"  />
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Category" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="CategoryDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) Category,RTRIM(Par_Description) CategoryDesc FROM Mst_Parameter WHERE Par_Group = 'MaterialCategory'">
                </asp:SqlDataSource>
            </td>
            <td></td>
        </tr>--%>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Currency">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboCurrency" runat="server" ClientInstanceName="cboCurrency"
                    Width="170px" Font-Names="Segoe UI" TextField="CurrencyDesc" ValueField="Currency" DataSourceID="SqlDataSource6"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <ClientSideEvents ValueChanged="function(s, e) {
	txtCurrency.SetText(cboCurrency.GetValue());
}" />
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Currency" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="CurrencyDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) Currency,RTRIM(Par_Description) CurrencyDesc FROM Mst_Parameter WHERE Par_Group = 'Currency'">
                </asp:SqlDataSource>
            </td>
          
            <td></td>
        </tr>
          <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Price">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtPrice" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtPrice"  HorizontalAlign="Right">
                     <MaskSettings Mask="<0..100000000g>.<00..99>" />
                </dx:ASPxTextBox>
            </td>
            <td></td>
        </tr>
       <tr >
           <td class="td-col-l"></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>

       </tr>
        <tr style="height: 25px;">
            <td class="td-col-l"></td>
            <td class="td-col-l"></td>
            <td></td>
            <td style="width: 500px">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack"
                    Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>

                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                   
                    <ClientSideEvents Click="function(s, e) {
	var msg = confirm('Are you sure want to submit this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }
	SubmitProcess(s,e);
    
   
}" />
                   
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
              <%--  <dx:ASPxButton ID="btnUpdate" runat="server" Text="Update" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnUpdate" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>--%>

            </td>
            <td></td>
           
        </tr>
        <tr style="display:none">
            <td colspan="9">
               
            </td>
        </tr>
    </table>
    <div style="display:none">
          <dx:ASPxTextBox ID="txtSupplier" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="80px" ClientInstanceName="txtSupplier" MaxLength="100" >
                                  
                </dx:ASPxTextBox>
                <dx:ASPxTextBox ID="txtMaterialType" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="80px" ClientInstanceName="txtMaterialType" MaxLength="100" >
                                  
                </dx:ASPxTextBox>
                 <dx:ASPxTextBox ID="txtMaterialCode" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="80px" ClientInstanceName="txtMaterialCode" MaxLength="100" >
                                  
                </dx:ASPxTextBox>
                <dx:ASPxTextBox ID="txtGroupItem" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="80px" ClientInstanceName="txtGroupItem" MaxLength="100" >
                                  
                </dx:ASPxTextBox>
                <%--<dx:ASPxTextBox ID="txtCategory" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="80px" ClientInstanceName="txtCategory" MaxLength="100" >
                                  
                </dx:ASPxTextBox>--%>
                <dx:ASPxTextBox ID="txtCurrency" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="80px" ClientInstanceName="txtCurrency" MaxLength="100" >
                                  
                </dx:ASPxTextBox>
    </div>

    <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
           <%-- <ClientSideEvents CallbackComplete="SetCode" />--%>
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
            <ClientSideEvents EndCallback="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
            <ClientSideEvents Init="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
           <%-- <ClientSideEvents CallbackComplete="MessageDelete" />--%>
        </dx:ASPxCallback>
    </div>
</asp:Content>

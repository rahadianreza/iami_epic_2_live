﻿<%@ Page Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeBehind="ExchangeRateChart.aspx.vb" Inherits="IAMI_EPIC2.ExchangeRateChart" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register assembly="DevExpress.XtraCharts.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
     <style type="text/css">
        .table-col-title01
        {
            width: 160px;                     
        }
        .table-col-control01
        {
            width: 110px;        
        }   
        .table-col-control02
        {
            width: 250px; 
        }   
        .table-col-title02
        {
            width: 180px;
        }   
        .table-col-control03
        {
            width: 120px;
        }   
        .table-height
        {
            height: 35px      
        }    
        .td-col-l 
        {
            padding:0px 0px 0px 10px;
            width:70px;
        }
        .td-col-m
        {
            width:20px;
        }
        .td-col-r
        {
            width:200px;
        }
        .td-col-f
        {
            width:50px;
        }
        .tr-height
        {
            height: 35px;
        }
        
        .widthPanelChart
        {
            width : 100%;
            height : 400px;
        }    
        
    </style>
    <script type="text/javascript">
        function LoadChart() {
            ASPxCallbackPanel1.PerformCallback('show');
        }
        function formatDateISO(date) {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
        function dtDateFrom_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            dtDateFrom.SetDate(myDate);
        }

        function dtDateFrom_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();

                var startDate = formatDateISO(dtDateFrom.GetDate());
                var endDate = formatDateISO(dtDateTo.GetDate());

                if (startDate > endDate) {
                    Message(2, 'Period from must be lower than period to!');
                    dtDateFrom.Focus();
                    return false;
                }
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }
        }

        function dtDateTo_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            dtDateTo.SetDate(myDate);
        }

        function dtDateTo_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();

                var startDate = formatDateISO(dtDateFrom.GetDate());
                var endDate = formatDateISO(dtDateTo.GetDate());

                if (startDate > endDate) {
                    Message(2, 'Exchange rate date from must be lower than exchange rate date to!');
                    dtDateFrom.Focus();
                    return false;
                }
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }


        }

        function OnEndCallback(s, e) {
           // alert(s.cpMessage);
            if ((s.cpMessage == "There's no data to show!") && s.cpMessage !== undefined) {
               // alert(s.cpMessage);
                toastr.info(s.cpMessage, 'Info');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
              
            } else {
                delete s.cpMessage;
            }   
           
        }
       
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
 
   <%-- <div style="padding : 5px 5px 5px 5px">
       <dxchartsui:WebChartControl ID="WebChartControl1" runat="server">
        </dxchartsui:WebChartControl>
        
    </div>--%>
    <div style="padding: 5px 5px 5px 5px"> 
        <div style="padding: 5px 5px 5px 5px"> 
            <table runat="server" style="border: 1px solid; width: 100%">
                <tr style="height: 10px">
                    <td style="padding: 0px 0px 0px 10px; width: 180px">
                    </td>
                    <td class="td-col-m"></td>
                    <td class="td-col-f">
                    </td>
                    <td class="td-col-l">
                    </td>
                    <td style="width:20px">
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-r">
                    </td>
                    <td colspan="2">
                    </td>
                </tr>
                <tr class="table-height">
                    <td class="table-col-title01">
                        <dx:ASPxLabel ID="lblDateFrom" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Exchange Rate Date From" Style="padding-left: 10px" />
                    </td>
                    <td class="table-col-control01">
                        <dx:ASPxDateEdit ID="dtDateFrom" runat="server" Theme="Office2010Black" Width="100px"
                            AutoPostBack="false" ClientInstanceName="dtDateFrom" EditFormatString="MMM-yyyy"
                            DisplayFormatString="MMM-yyyy" EnableTheming="True" ShowShadow="false" Font-Names="Segoe UI"
                            Font-Size="9pt" Height="25px">
                            <CalendarProperties >
                                <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                </WeekNumberStyle>
                                <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                </ButtonStyle>
                            </CalendarProperties>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                            <ClientSideEvents DropDown="dtDateFrom_OnDropDown" Init="dtDateFrom_OnInit" />
                        </dx:ASPxDateEdit>
                    </td>
                    
                    <td class="table-col-title02">
                        <dx:ASPxLabel ID="lblDateTo" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Exchange Rate Date To" Style="padding-left: 10px" />
                    </td>
                    <td class="table-col-control03">
                        <dx:ASPxDateEdit ID="dtDateTo" runat="server" Theme="Office2010Black" Width="100px"
                            AutoPostBack="false" ClientInstanceName="dtDateTo" EditFormatString="MMM-yyyy"
                            DisplayFormatString="MMM-yyyy" EnableTheming="True" ShowShadow="false" Font-Names="Segoe UI"
                            Font-Size="9pt" Height="25px">
                            <CalendarProperties>
                                <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                </WeekNumberStyle>
                                <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                </ButtonStyle>
                            </CalendarProperties>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                            <ClientSideEvents DropDown="dtDateTo_OnDropDown" Init="dtDateTo_OnInit" />
                        </dx:ASPxDateEdit>
                    </td>
                    <td style="width:20px"></td>
                    <td>
                        <asp:SqlDataSource ID="ds_Bank" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="select rtrim(Par_Code) as Code, rtrim(Par_Description) as Description from Mst_Parameter where Par_Group = 'Bank' ">
                        </asp:SqlDataSource>
                    </td>
                    
                    <td class="td-col-r">
                    </td>
                    <td colspan="2">
                    </td>
                </tr>
                <tr class="table-height">
                    <td class="table-col-title01">
                        <dx:ASPxLabel ID="lblBankCode" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Bank Code" Style="padding-left: 10px" />
                    </td>
                    <td class="table-col-control01">
                        <dx:ASPxComboBox ID="cboBankCode" runat="server" ClientInstanceName="cboBankCode"
                            Width="100px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            DataSourceID="ds_Bank" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                            Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                           
                        </dx:ASPxComboBox>
                    </td>
                    
                    <td class="table-col-title01">
                        <dx:ASPxLabel ID="lblCurrencyCode" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Currency Code" Style="padding-left: 10px" />
                    </td>
                    <td class="table-col-control03">
                        <dx:ASPxComboBox ID="cboCurrencyCode" runat="server" ClientInstanceName="cboCurrencyCode"
                            Width="100px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            DataSourceID="ds_Currency" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                            Height="25px">
                            
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
 	if (s.GetSelectedItem().value=='USD' || s.GetSelectedItem().value=='EUR'){
        txtLimitFrom.SetText('10000');
        txtLimitTo.SetText('20000');
    }else if (s.GetSelectedItem().value=='JPN' || s.GetSelectedItem().value=='JPY'){
        txtLimitFrom.SetText('100');
        txtLimitTo.SetText('180');
    }else if (s.GetSelectedItem().value=='THB') {
   		txtLimitFrom.SetText('200');
		txtLimitTo.SetText('600');
	}else{
        txtLimitFrom.SetText('');
		txtLimitTo.SetText('');
    }
}" />
                            
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td style="width:20px"></td>
                    <td class="table-height">
                        <asp:SqlDataSource ID="ds_Currency" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="select rtrim(Par_Code) as Code, rtrim(Par_Description) as Description from Mst_Parameter where Par_Group = 'Currency' ">
                        </asp:SqlDataSource>
                    </td>
                     <td class="td-col-r">
                    </td>
                    <td colspan="2">
                    </td>
                </tr>
                <tr class="table-height">
                    <td class="table-col-title01">
                        
                    </td>
                    <td class="table-col-control01">
                       
                    </td>
                    
                    <td class="table-col-title01">
                       <dx:ASPxLabel ID="lblFromLimit" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="From Limit" Style="padding-left: 10px" />
                    </td>
                    <td class="table-col-control03">
                        <dx:ASPxTextBox ID="txtLimitFrom" runat="server" Width="100px" Height="25px" ClientInstanceName="txtLimitFrom"
                            MaxLength="50" AutoCompleteType="Disabled" HorizontalAlign="Right">
                            <MaskSettings Mask="<0..100000000g>" />
                            <ValidationSettings Display="Dynamic"></ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                    <td class="table-height">
                       <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text=" To " Style="padding-left: 10px" />
                    </td>
                     <td class="td-col-r" style="padding-left:10px">
                        <dx:ASPxTextBox ID="txtLimitTo" runat="server" Width="100px" Height="25px" ClientInstanceName="txtLimitTo"
                            MaxLength="50" AutoCompleteType="Disabled" HorizontalAlign="Right">
                            <MaskSettings Mask="<0..100000000g>" />
                            <ValidationSettings Display="Dynamic"></ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                    <td colspan="2">
                    </td>
                </tr>
                <tr class="tr-height">
                    <td style="padding: 0px 0px 0px 10px" colspan="6">
                        <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnBack" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;&nbsp;
                        <dx:ASPxButton ID="btnShow" runat="server" Text="Show" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShow" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
                                 if (cboBankCode.GetText()==''){
                                     
									 toastr.warning('Please select Bank Code', 'Warning');
                                     cboBankCode.Focus();
                                     toastr.options.closeButton = false;
                                     toastr.options.debug = false;
                                     toastr.options.newestOnTop = false;
                                     toastr.options.progressBar = false;
                                     toastr.options.preventDuplicates = true;
                                     toastr.options.onclick = null;
                                     e.processOnServer = false;
                                     return;
                                 }     
                                 
                                  if (cboCurrencyCode.GetText()==''){
                                    
									 toastr.warning('Please select Currency', 'Warning');
                                     cboCurrencyCode.Focus();
                                     toastr.options.closeButton = false;
                                     toastr.options.debug = false;
                                     toastr.options.newestOnTop = false;
                                     toastr.options.progressBar = false;
                                     toastr.options.preventDuplicates = true;
                                     toastr.options.onclick = null;
                                     e.processOnServer = false;
                                     return;
                                 }

                                 if (cboCurrencyCode.GetText()=='USD' || cboCurrencyCode.GetText()=='JPN' || cboCurrencyCode.GetText()=='JPY' ){
                                     if (txtLimitFrom.GetText()==''){
                                         toastr.warning('Please Input Limit From', 'Warning');
                                         txtLimitFrom.Focus();
                                         toastr.options.closeButton = false;
                                         toastr.options.debug = false;
                                         toastr.options.newestOnTop = false;
                                         toastr.options.progressBar = false;
                                         toastr.options.preventDuplicates = true;
                                         toastr.options.onclick = null;
                                         e.processOnServer = false;
                                         return;
                                     }

                                     if (txtLimitTo.GetText()=='' || txtLimitTo.GetText()=='0' ){
                                         toastr.warning('Please Input Limit To', 'Warning');
                                         txtLimitTo.Focus();
                                         toastr.options.closeButton = false;
                                         toastr.options.debug = false;
                                         toastr.options.newestOnTop = false;
                                         toastr.options.progressBar = false;
                                         toastr.options.preventDuplicates = true;
                                         toastr.options.onclick = null;
                                         e.processOnServer = false;
                                         return;
                                     }

								 
                                     if (parseFloat(txtLimitTo.GetText()) &lt; parseFloat(txtLimitFrom.GetText()) )  {
                                         toastr.warning('Limit To must greater than Limit From', 'Warning');
                                         txtLimitTo.Focus();
                                         toastr.options.closeButton = false;
                                         toastr.options.debug = false;
                                         toastr.options.newestOnTop = false;
                                         toastr.options.progressBar = false;
                                         toastr.options.preventDuplicates = true;
                                         toastr.options.onclick = null;
                                         e.processOnServer = false;
                                         return;
                                     }

                                    
                                }
 									
									var date1 = dtDateFrom.GetDate();  
                                    var date2 = dtDateTo.GetDate(); // new Date('12/04/2019');

                                    var diff = date2 - date1;
                                    var oneDay = 1000 * 60 * 60 * 24;
                                    var day = Math.floor(diff / oneDay) / 365.2425;

                                    if (day.toFixed(1) &gt; 6.0) {
									   toastr.warning('Period out of Range ..! Maximum Period is 6 Years', 'Warning');
                                       toastr.options.closeButton = false;
                                       toastr.options.debug = false;
                                       toastr.options.newestOnTop = false;
                                       toastr.options.progressBar = false;
                                       toastr.options.preventDuplicates = true;
                                       toastr.options.onclick = null;
                                       e.processOnServer = false;
                                       return;
       

                                    }        
                                   
                                 ASPxCallbackPanel1.PerformCallback('show|'+cboBankCode.GetValue()+'|'+cboCurrencyCode.GetValue());
                            }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td colspan="3">
                    </td>
                </tr>
                <tr style="height: 10px">
                    <td colspan="9">
                    </td>
                </tr>
            </table>
          </div>
    </div>
    <div style="padding: 5px 5px 5px 5px"> 
        <div style="padding: 5px 5px 5px 5px"> 
            <table runat="server" style="width:100%; border : 1px solid">
                <tr>
                    <td style="padding:5px 5px 5px 5px">
                        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" ClientInstanceName="ASPxCallbackPanel1" Width="960px"  Height="400px"
                            runat="server" CssClass="widthPanelChart" >
                                
                            <ClientSideEvents EndCallback="OnEndCallback"  />
                            <PanelCollection>
                               
                                <dx:PanelContent runat="server">
                                     <dxchartsui:WebChartControl ID="wbc" runat="server" Width="960px"  Height="400px" Visible="false">
                                     <%--    <diagramserializable>
                                             <cc1:XYDiagram>
                                                 <axisx visibleinpanesserializable="-1">
                                                 </axisx>
                                                 <axisy visibleinpanesserializable="-1">
                                                     <range scrollingrange-maxvalueserializable="18000" 
                                                         scrollingrange-minvalueserializable="10000" 
                                                         scrollingrange-sidemarginsenabled="False" />
                                                     <visualrange autosidemargins="False" sidemarginsvalue="800" />
                                                     <wholerange alwaysshowzerolevel="False" auto="False" autosidemargins="False" 
                                                         maxvalueserializable="18000" minvalueserializable="10000" 
                                                         sidemarginsvalue="800" />
                                                     <numericscaleoptions autogrid="False" gridalignment="Thousands" 
                                                         gridspacing="2" />
                                                 </axisy>
                                             </cc1:XYDiagram>
                                         </diagramserializable>
                                         <seriesserializable>
                                             <cc1:Series Name="Series 1">
                                                 <viewserializable>
                                                     <cc1:LineSeriesView>
                                                     </cc1:LineSeriesView>
                                                 </viewserializable>
                                             </cc1:Series>
                                             <cc1:Series Name="Series 2">
                                                 <viewserializable>
                                                     <cc1:LineSeriesView>
                                                     </cc1:LineSeriesView>
                                                 </viewserializable>
                                             </cc1:Series>
                                         </seriesserializable>
                                         <seriestemplate>
                                             <viewserializable>
                                                 <cc1:LineSeriesView>
                                                 </cc1:LineSeriesView>
                                             </viewserializable>
                                         </seriestemplate>--%>
                                      
                                         </dxchartsui:WebChartControl>
                                </dx:PanelContent>
                            </PanelCollection>
                    
                        </dx:ASPxCallbackPanel>
                     </td>
                </tr>
            </table>
        </div>
    </div>
    <div>
       
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Province, Rtrim(Par_Description) ProvinceDesc  From Mst_Parameter Where Par_Group = 'Provinsi'">
        </asp:SqlDataSource>
        <dx:ASPxCallback ID="ASPxCallback1" runat="server">
        </dx:ASPxCallback>
    </div>
    
</asp:Content>


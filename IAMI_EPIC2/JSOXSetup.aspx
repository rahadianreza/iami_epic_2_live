﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="JSOXSetup.aspx.vb" Inherits="IAMI_EPIC2.JSOXSetup" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGlobalEvents" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cp_message != "") {
                if (s.cp_type == "Success") {
                    toastr.success(s.cp_message, 'Success');
                }
                else if (s.cp_type == "Warning") {
                    toastr.warning(s.cp_message, 'Warning');
                }
                else if (s.cp_type == "Info") {
                    toastr.info(s.cp_message, 'Info');
                }
                else {
                    toastr.error(s.cp_message, 'Error');
                }
                toastr.options.closeButton = true;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }
    </script>
    <style type="text/css">
        body 
        {
            font-family: "Segoe UI";
        }
        tr
        {
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width:530px; font-family:'Segoe UI'">
        <tr>
            <td style="width:500px">
                <dx:ASPxCheckBox ID="chk1" runat="server" Text="1. Minimum Password Length" 
                    Font-Names="Segoe UI" Font-Size="10pt" ClientInstanceName="chk1"></dx:ASPxCheckBox>
            </td>
            <td>
            </td>
            <td style="width: 50px" valign="middle">
                <dx:ASPxTextBox ID="txt1" runat="server" Width="40px" ClientInstanceName="txt1"></dx:ASPxTextBox>                
            </td>
            <td>
                Chars
            </td>
        </tr>
            <tr>
            <td>
                <dx:ASPxCheckBox ID="chk2" runat="server" Text="2. Advance Aging Control" 
                    Font-Names="Segoe UI" Font-Size="10pt" ClientInstanceName="chk2"></dx:ASPxCheckBox>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txt2" runat="server" Width="40px" ClientInstanceName="txt2"></dx:ASPxTextBox>                
            </td>
            <td>
                Days
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxCheckBox ID="chk3" runat="server" Text="3. Password Expire Every" 
                    Font-Names="Segoe UI" Font-Size="10pt" ClientInstanceName="chk3"></dx:ASPxCheckBox>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txt3" runat="server" Width="40px" ClientInstanceName="txt3"></dx:ASPxTextBox>                
            </td>
            <td>Days</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxCheckBox ID="chk4" runat="server" 
                    Text="4. Maximum failed login attempt (User ID locked once exeeded)" 
                    Font-Names="Segoe UI" Font-Size="10pt" ClientInstanceName="chk4"></dx:ASPxCheckBox>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txt4" runat="server" Width="40px" ClientInstanceName="txt4"></dx:ASPxTextBox>                
            </td>
            <td>Times</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxCheckBox ID="chk5" runat="server" 
                    Text="5. Password history retain (not allow repeat same password)" 
                    Font-Names="Segoe UI" Font-Size="10pt" ClientInstanceName="chk5"></dx:ASPxCheckBox>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txt5" runat="server" Width="40px" ClientInstanceName="txt5"></dx:ASPxTextBox>                
            </td>
            <td>Times</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxCheckBox ID="chk6" runat="server" 
                    Text="6. Password is case sensitive (capital or lowercase)" 
                    Font-Names="Segoe UI" Font-Size="10pt" ClientInstanceName="chk6"></dx:ASPxCheckBox>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <dx:ASPxCheckBox ID="chk7" runat="server" 
                    Text="7. Allow password to contain User Name?" Font-Names="Segoe UI" 
                    Font-Size="10pt" ClientInstanceName="chk7"></dx:ASPxCheckBox>
            </td>
            <td></td>
            <td></td><td></td>
        </tr>
        <tr>
            <td>
                <dx:ASPxCheckBox ID="chk8" runat="server" Text="8. Minimum number of digits" 
                    Font-Names="Segoe UI" Font-Size="10pt" ClientInstanceName="chk8"></dx:ASPxCheckBox>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txt8" runat="server" Width="40px" ClientInstanceName="txt8"></dx:ASPxTextBox>                
            </td>
            <td>Digits</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxCheckBox ID="chk9" runat="server" 
                    Text="9. Allow password to contain repeating characters/digits?" 
                    Font-Names="Segoe UI" Font-Size="10pt" ClientInstanceName="chk9"></dx:ASPxCheckBox>
            </td>
            <td></td>
            <td></td><td></td>
        </tr>
        <tr>
            <td>
                <dx:ASPxCheckBox ID="chk10" runat="server" Text="10. Automatic unlock login ID" 
                    Font-Names="Segoe UI" Font-Size="10pt" ClientInstanceName="chk10"></dx:ASPxCheckBox>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txt10" runat="server" Width="40px" 
                    ClientInstanceName="txt10"></dx:ASPxTextBox>                
            </td>
            <td>Minutes</td>
        </tr>
    </table>
    <div style="height:20px">
                <dx:ASPxCallback ID="cbProgress" runat="server" ClientInstanceName="cbProgress">
                <ClientSideEvents CallbackComplete="OnEndCallback" />                                           
                </dx:ASPxCallback>
            </div>
    <table>
        <tr>
            <td align="right">
                            <dx:ASPxButton ID="btnSubmit" runat="server" AutoPostBack="false" ClientInstanceName="btnSubmit"
                                Font-Names="Verdana" Font-Size="8pt" Text="Submit" Theme="Office2010Silver" Width="80px">
                                <ClientSideEvents Click="function(s, e) {
		                            cbProgress.PerformCallback();	               
                                }" />                                
                            </dx:ASPxButton>
                        </td>
        </tr>
    </table>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="GetCheckSheetConfirm.aspx.vb" Inherits="IAMI_EPIC2.GetCheckSheetConfirm" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";

                    if (s.cp_message =="Data Has Been Reject Successfully"){
                        btnReject.SetEnabled(false);
                    }
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };

        function MessageError(s, e) {
            if (s.cpMessage == "Data Saved Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                txtItemCode.SetText('');
                txtDescription.SetText('');
                txtSpecification.SetText('');
                cboUOM.SetText('');
                txtLastIAPrice.SetText('');
                txtLastSupplier.SetText('');
                txtItemCode.Focus();
                cboGroupItem.SetText('');
                cboCategory.SetText('');
                cboPRType.SetText('');
                txtSAPNumber.SetText('');
            }
            else if (s.cpMessage == "Data Updated Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                //window.location.href = "/ItemList.aspx";

                millisecondsToWait = 2000;
                setTimeout(function () {
                    var pathArray = window.location.pathname.split('/');
                    var url = window.location.origin + '/' + pathArray[1] + '/ItemList.aspx';
                    //window.location.href = url;
                    //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                    if (pathArray[1] == "AddItemMaster.aspx") {
                        window.location.href = window.location.origin + '/ItemList.aspx';
                    }
                    else {
                        window.location.href = url;
                    }
                }, millisecondsToWait);

            }
            else if (s.cpMessage == "Data Clear Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                txtItemCode.SetText('');
                txtDescription.SetText('');
                txtSpecification.SetText('');
                cboUOM.SetText('');
                txtLastIAPrice.SetText('');
                txtLastSupplier.SetText('');
                txtSAPNumber.SetText('');
                txtItemCode.Focus();

            }
            else if (s.cpMessage == "Data Completed!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                txtItemCode.SetText('');
                txtDescription.SetText('');
                txtSpecification.SetText('');
                cboUOM.SetText('');
                txtLastIAPrice.SetText('');
                txtLastSupplier.SetText('');
                txtItemCode.Focus();
                cboGroupItem.SetText('');
                cboCategory.SetText('');
                cboPRType.SetText('');
                txtSAPNumber.SetText('');
            }
            else if (s.cpMessage =="Data Has Been Reject Successfully"){
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                //alert('a');
                btnReject.SetEnabled(false);
            }
            else if (s.cpMessage == "Data Cancel Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }

        };

         function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "Part_No" || currentColumnName == "Commodity" || currentColumnName == "Upc") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        };  

        function SaveData(s, e) {
//            cbkValid.PerformCallback('save');
            Grid.UpdateEdit()
        }

        function disableenablebuttonsubmit(_val) {
        alert('adsds');
            if (_val = '1') {
                btnSave.SetEnabled(false)
            } else {
                btnSave.SetEnabled(true)
             }
        }

          function OnUpdateCheckedChanged(s, e) {
            if (s.GetValue() == -1) s.SetValue(1);
            Grid.SetFocusedRowIndex(-1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "Status_Drawing", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "Status_Drawing", s.GetValue());
                }                
            }
        }

          function OnTabChanging(s, e) {
            var tabName = (pageControl.GetActiveTab()).name;
            e.cancel = !ASPxClientEdit.ValidateGroup('group' + tabName);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="border: 1px solid black; padding-bottom:5px;">
            <table>
                <tr>
                   <td style="width: 80px; padding: 10px 10px 10px 10px">
                        <dx:ASPxButton ID="btnBacktoList" runat="server" Text="Back to List" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnBacktoList" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>

                    <td style="width: 80px; padding: 10px 10px 10px 10px">
                        <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td style="width: 80px; padding: 10px 10px 10px 10px"> 
                        <dx:ASPxButton ID="btnReject" runat="server" Text="Reject" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False" 
                            ClientInstanceName="btnReject" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
	if (txtRejectNotes.GetText()==''){
		toastr.warning('Please Input Reject Notes', 'Warning');
        toastr.options.closeButton = false;
        toastr.options.debug = false;
        toastr.options.newestOnTop = false;
        toastr.options.progressBar = false;
        toastr.options.preventDuplicates = true;
        toastr.options.onclick = null;
        return false;
	}

    var msg = confirm('Are you sure want to reject this data ?');                
    if (msg == false) {
            e.processOnServer = false;
            return;
    }

    cbReject.PerformCallback();

}" />
                            <Paddings Padding="2px" />
                           
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td style="width: 80px; padding: 10px 10px 10px 10px">
                        <dx:ASPxButton ID="btnSpecInformation" runat="server" Text="Spec Information" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnSpecInformation" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px;">
                        &nbsp;
                    </td>
                    <td style="width: 80px; padding: 10px 10px 10px 10px">
                        <dx:ASPxButton ID="btnBiddersList" runat="server" Text="Bidders List" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnBiddersList" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px;">
                        &nbsp;
                    </td>
                    <td style="width: 80px; padding: 10px 10px 10px 10px">
                        <dx:ASPxButton ID="btnSourcingSchedule" runat="server" Text="Sourcing Schedule" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnSourcingSchedule" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px;">
                        &nbsp;
                    </td>
                    <td style="width: 80px; padding: 10px 10px 10px 10px">
                        <dx:ASPxButton ID="btnComplete" runat="server" Text="Release" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False" Visible="false"
                            ClientInstanceName="btnComplete" Theme="Default">
                            <Paddings Padding="2px" />
                           
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px;">
                        &nbsp;
                    </td>
                    <td style="width: 80px; padding: 10px 10px 10px 10px">
                        
                    </td>
                    <td>
                    </td>
                </tr>
                
            </table>
        </div>
        <div style="border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black; ">
            <table>
                <tr>
                    <td colspan="5">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width:20px"></td>
                    <td>
                        <dx1:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Reject Notes">
                        </dx1:ASPxLabel>   
                    </td>
                    <td style="width:20px">&nbsp;</td>
                    <td>
                        <dx1:ASPxMemo ID="txtRejectNotes" runat="server" Height="50px" Width="300px"
                                      ClientInstanceName="txtRejectNotes" Font-Names="Segoe UI" Font-Size="9pt"
                                      Theme="Metropolis" MaxLength="300">                        
                        </dx1:ASPxMemo>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">&nbsp;</td>
                </tr>
            </table>
        </div>
        <br />
    </div>
    <div>
        
        <div>
            <dx:ASPxPageControl ID="pageControl" runat="server" ClientInstanceName="pageControl"
                ActiveTabIndex="0" EnableHierarchyRecreation="True" Width="100%">
                <ClientSideEvents ActiveTabChanging="OnTabChanging" />
                <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                </ActiveTabStyle>
                <TabPages>
                    <dx:TabPage Name="baseInformation" Text="Base Information">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl0" runat="server">
                                <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                                    EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
                                    Font-Size="9pt" KeyFieldName="Part_No">
                                    <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowEditButton="true">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Part No" VisibleIndex="1" FieldName="Part_No"
                                            Width="150px">
                                            <EditFormSettings Visible="true" />
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Drawing ISZ or SPLR" FieldName="Drawing_ISZ_SPLR"
                                            VisibleIndex="2" Width="150px">
                                            <EditFormSettings Visible="default" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Part Name" VisibleIndex="3" FieldName="Part_Name"
                                            Width="120px">
                                            <EditFormSettings Visible="False" />
                                            <Settings AllowAutoFilter="true" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Qty" VisibleIndex="4" FieldName="Qty">
                                            <EditFormSettings Visible="False" />
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Vol x Qty" VisibleIndex="5" Width="130px" FieldName="VolxQty">
                                            <EditFormSettings Visible="False" />
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="New Guide Price JPN=Rp117 (RP)" VisibleIndex="6"
                                            FieldName="New_Guide_Price_Jpn">
                                            <PropertiesTextEdit DisplayFormatString="#,###">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Current JPN Supplier" VisibleIndex="6" FieldName="Current_JPN_Supplier">
                                            <EditFormSettings Visible="False" />
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="IAMI Buyer" VisibleIndex="6" FieldName="IAMI_Buyer">
                                            <EditFormSettings Visible="False" />
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="6" FieldName="Register_By">
                                            <EditFormSettings Visible="False" />
                                            <Settings AllowAutoFilter="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="6" FieldName="Register_Date">
                                            <EditFormSettings Visible="False" />
                                            <Settings AllowAutoFilter="False" />
                                            <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="6" FieldName="Update_By">
                                            <EditFormSettings Visible="False" />
                                            <Settings AllowAutoFilter="False" />
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="6" FieldName="Update_Date">
                                            <EditFormSettings Visible="False" />
                                            <Settings AllowAutoFilter="False" />
                                            <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                                            Width="320" />
                                    </SettingsPopup>
                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                                        EditFormColumnCaption-Paddings-PaddingRight="10px">
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>
                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                                            </Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange">
                                        </CommandColumnItem>
                                    </Styles>
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server"></dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="SpecInformation" Text="Spec Information">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl2" runat="server" >
                                <table style="width: 100%;">
                                    <%--<tr style="height: 25px">
                                        <td style="width: 120px; padding-left: 100px; padding-top: 15px;">
                                            <dx:ASPxLabel ID="lblTitle" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                Text="Title">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td style="width: 20px;">
                                            &nbsp;
                                        </td>
                                        <td style="width: 50px; padding-top: 15px;">
                                            <dx:ASPxTextBox ID="txtTitle" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                Width="170px" ClientInstanceName="txtTitle" MaxLength="20">
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td style="width: 400px;">
                                            &nbsp;
                                        </td>
                                    </tr>--%>
                                    <tr style="height: 25px">
                                        <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                                            <dx:ASPxLabel ID="lblBaseDrawing" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                Text="Base Drawing" >
                                            </dx:ASPxLabel>
                                        </td>
                                        <td style="width: 20px">
                                            &nbsp;
                                        </td>
                                        <td style="width: 50px; padding-top: 10px;">
                                            <dx:ASPxComboBox ID="cboBaseDrawing" runat="server" Value='<%# Bind("ACTIVE") %>'
                                                ValueType="System.String" Height="25px" Font-Size="9pt" Theme="Office2010Black"
                                                Width="120px" TabIndex="1" ReadOnly="true" BackColor="LightGray">
                                                <Items>
                                                    <dx:ListEditItem Text="Isuzu" Value="ISZ" />
                                                    <dx:ListEditItem Text="Supplier" Value="SPL" />
                                                </Items>
                                                <ItemStyle Height="10px" Paddings-Padding="4px">
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                                <ButtonStyle Width="5px" Paddings-Padding="4px">
                                                    <Paddings Padding="4px"></Paddings>
                                                </ButtonStyle>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td style="width: 400px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                                            <dx:ASPxLabel ID="lblSOR" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="SOR">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td style="width: 20px">
                                            &nbsp;
                                        </td>
                                        <td style="width: 50px; padding-top: 10px;">
                                            <dx:ASPxComboBox ID="cboSOR" runat="server" Value='<%# Bind("ACTIVE") %>' ValueType="System.String"
                                                Height="25px" Font-Size="9pt" Theme="Office2010Black" Width="120px" TabIndex="1" ReadOnly="true" BackColor="LightGray">
                                                <Items>
                                                    <dx:ListEditItem Text="Need" Value="1" />
                                                    <dx:ListEditItem Text="No Need" Value="0" />
                                                </Items>
                                                <ItemStyle Height="10px" Paddings-Padding="4px">
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                                <ButtonStyle Width="5px" Paddings-Padding="4px">
                                                    <Paddings Padding="4px"></Paddings>
                                                </ButtonStyle>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td style="width: 400px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="width: 120px; padding-left: 100px; padding-top: 15px;">
                                            <dx:ASPxLabel ID="lblMaterial" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                Text="Material">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td style="width: 20px;">
                                            &nbsp;
                                        </td>
                                        <td style="width: 50px; padding-top: 10px;">
                                            <dx:ASPxTextBox ID="txtMaterial" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                Width="170px" ClientInstanceName="txtMaterial" MaxLength="30" ReadOnly="true" BackColor="LightGray">
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td style="width: 400px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                                            <dx:ASPxLabel ID="lblProcess" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                Text="Process">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td style="width: 20px">
                                            &nbsp;
                                        </td>
                                        <td style="width: 50px; padding-top: 10px;">
                                            <dx:ASPxTextBox ID="txtProcess" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                Width="170px" ClientInstanceName="txtProcess" MaxLength="30" ReadOnly="true" BackColor="LightGray">
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td style="width: 400px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                                            <dx:ASPxLabel ID="lblCheckDevelopmentSchedule" runat="server" Font-Names="Segoe UI"
                                                Font-Size="9pt" Text="Check Development Schedule">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td style="width: 20px">
                                            &nbsp;
                                        </td>
                                        <td style="width: 50px; padding-top: 10px;">
                                            <dx:ASPxComboBox ID="cboDevSchedule" runat="server" Value='<%# Bind("ACTIVE") %>'
                                                ValueType="System.String" Height="25px" Font-Size="9pt" Theme="Office2010Black"
                                                Width="120px" TabIndex="1" ReadOnly="true" BackColor="LightGray">
                                                <Items>
                                                    <dx:ListEditItem Text="Yes" Value="1" />
                                                    <dx:ListEditItem Text="No" Value="0" />
                                                </Items>
                                                <ItemStyle Height="10px" Paddings-Padding="4px">
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                                <ButtonStyle Width="5px" Paddings-Padding="4px">
                                                    <Paddings Padding="4px"></Paddings>
                                                </ButtonStyle>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td style="width: 400px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                                            <dx:ASPxLabel ID="lblAdditionalInformation" runat="server" Font-Names="Segoe UI"
                                                Font-Size="9pt" Text="Additional Information">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td style="width: 20px">
                                            &nbsp;
                                        </td>
                                        <td style="width: 50px; padding-top: 10px;">
                                            <dx:ASPxTextBox ID="txtAdditionalInformation" runat="server" Font-Names="Segoe UI"
                                                Font-Size="9pt" Width="170px" ClientInstanceName="txtAdditionalInformation" MaxLength="30" ReadOnly="true" BackColor="LightGray">
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td style="width: 400px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr style="height: 25px">
                                        <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                                            <dx:ASPxLabel ID="lblKSHAttendance" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                                Text="KSH Attendance">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td style="width: 20px">
                                            &nbsp;
                                        </td>
                                        <td style="width: 50px; padding-top: 10px;">
                                            <dx:ASPxDateEdit ID="KHSAttendance" runat="server" Theme="Office2010Black" Width="120px"
                                                ClientInstanceName="KHSAttendance" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                                Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom" ReadOnly="true" BackColor="LightGray">
                                                <CalendarProperties>
                                                    <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                                                        <Paddings Padding="5px"></Paddings>
                                                    </HeaderStyle>
                                                    <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                                                        <Paddings Padding="5px"></Paddings>
                                                    </DayStyle>
                                                    <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                                        <Paddings Padding="5px"></Paddings>
                                                    </WeekNumberStyle>
                                                    <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                                                        <Paddings Padding="10px"></Paddings>
                                                    </FooterStyle>
                                                    <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                                        <Paddings Padding="10px"></Paddings>
                                                    </ButtonStyle>
                                                </CalendarProperties>
                                                <ButtonStyle Width="5px" Paddings-Padding="4px">
                                                    <Paddings Padding="4px"></Paddings>
                                                </ButtonStyle>
                                            </dx:ASPxDateEdit>
                                        </td>
                                        <td style="width: 400px;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td colspan="2" style="padding: 20px 0 0 0">
                                        </td>
                                    </tr>
                                </table>
                                <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="cbSave">
                                    <ClientSideEvents Init="MessageError" />
                                </dx:ASPxCallback>
                                <br />
                                <br />
                                <div>
                                  <%--  Dummy Header for download excel--%>
                              <dx:ASPxGridView ID="GridHeaderSpecInformation" runat="server" AutoGenerateColumns="False"
                                        ClientInstanceName="GridHeaderSpecInformation" EnableTheming="True" KeyFieldName="UserID"
                                        Theme="Office2010Black" Width="100%" Font-Names="Segoe UI" Font-Size="9pt" Enabled="false" Visible="false">
                                        <ClientSideEvents EndCallback="OnEndCallback" />
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="TitleofValues" Width="150px"
                                                VisibleIndex="1">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="resultValues" Width="150px"
                                                VisibleIndex="1">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                        <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
                                        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                                        </SettingsPager>
                                        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
                                        <SettingsPopup>
                                            <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                                                Width="320" />
                                        </SettingsPopup>
                                        <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                                            EditFormColumnCaption-Paddings-PaddingRight="10px">
                                            <Header>
                                                <Paddings Padding="2px"></Paddings>
                                            </Header>
                                            <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                                <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                                                </Paddings>
                                            </EditFormColumnCaption>
                                            <CommandColumnItem ForeColor="Orange">
                                            </CommandColumnItem>
                                        </Styles>
                                        <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
                                        <Templates>
                                            <EditForm>
                                                <div style="padding: 15px 15px 15px 15px">
                                                    <dx:ContentControl ID="ContentControl1" runat="server">
                                                        <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                    </dx:ContentControl>
                                                </div>
                                                <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                    <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                    <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                    </dx:ASPxGridView>
                                    <%--End--%>
                                    <dx:ASPxGridView ID="GridCostControl" runat="server" AutoGenerateColumns="False"
                                        ClientInstanceName="GridCostControl" EnableTheming="True" KeyFieldName="UserID"
                                        Theme="Office2010Black" Width="100%" Font-Names="Segoe UI" Font-Size="9pt" >
                                        <ClientSideEvents EndCallback="OnEndCallback" />
                                        <Columns>
                                            <%--<dx:GridViewCommandColumn VisibleIndex="0" Width="100px" ShowClearFilterButton="true">
                                            </dx:GridViewCommandColumn>--%>
                                            <dx:GridViewDataComboBoxColumn Caption="Department" FieldName="Department" VisibleIndex="1"
                                                Width="350px">
                                                <PropertiesComboBox DataSourceID="sdsDepartment" TextField="Par_Description" ValueField="Par_Code"
                                                    Width="145px" TextFormatString="{1}" ClientInstanceName="Department" DropDownStyle="DropDownList"
                                                    IncrementalFilteringMode="StartsWith">
                                                    <Columns>
                                                        <dx:ListBoxColumn FieldName="Par_Code" Caption="Code" Width="65px" />
                                                        <dx:ListBoxColumn FieldName="Par_Description" Caption="Description" Width="200px" />
                                                    </Columns>
                                                    <ItemStyle Height="10px" Paddings-Padding="4px">
                                                        <Paddings Padding="4px"></Paddings>
                                                    </ItemStyle>
                                                    <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                        <Paddings Padding="2px"></Paddings>
                                                    </ButtonStyle>
                                                </PropertiesComboBox>
                                                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                    Wrap="True">
                                                    <Paddings PaddingLeft="5px"></Paddings>
                                                </HeaderStyle>
                                                <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                            </dx:GridViewDataComboBoxColumn>
                                             <dx:GridViewDataTextColumn FieldName="User" Width="150px" Caption="User ID"
                                                VisibleIndex="1">
                                                <EditFormSettings Visible="False" />
                                                <Settings AutoFilterCondition="Contains" AllowAutoFilter ="False" /> 
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataComboBoxColumn Caption="UserID" FieldName="UserID" VisibleIndex="2"
                                                Width="0px">
                                                <PropertiesComboBox DataSourceID="sdsCC" TextField="UserName" ValueField="UserID"
                                                    Width="145px" TextFormatString="{1}" ClientInstanceName="UserID" DropDownStyle="DropDownList"
                                                    IncrementalFilteringMode="StartsWith">
                                                    <Columns>
                                                        <dx:ListBoxColumn FieldName="UserID" Caption="ID" Width="65px" />
                                                        <dx:ListBoxColumn FieldName="UserName" Caption="Name" Width="200px" />
                                                    </Columns>
                                                    <ItemStyle Height="10px" Paddings-Padding="4px">
                                                        <Paddings Padding="4px"></Paddings>
                                                    </ItemStyle>
                                                    <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                        <Paddings Padding="2px"></Paddings>
                                                    </ButtonStyle>
                                                </PropertiesComboBox>
                                                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                    Wrap="True">
                                                    <Paddings PaddingLeft="5px"></Paddings>
                                                </HeaderStyle>
                                                <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataTextColumn FieldName="UserName" Width="150px" Caption="User Name"
                                                VisibleIndex="3">
                                                <EditFormSettings Visible="False" />
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                        <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
                                        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                                        </SettingsPager>
                                        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
                                        <SettingsPopup>
                                            <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                                                Width="320" />
                                        </SettingsPopup>
                                        <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                                            EditFormColumnCaption-Paddings-PaddingRight="10px">
                                            <Header>
                                                <Paddings Padding="2px"></Paddings>
                                            </Header>
                                            <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                                <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                                                </Paddings>
                                            </EditFormColumnCaption>
                                            <CommandColumnItem ForeColor="Orange">
                                            </CommandColumnItem>
                                        </Styles>
                                        <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
                                        <Templates>
                                            <EditForm>
                                                <div style="padding: 15px 15px 15px 15px">
                                                    <dx:ContentControl ID="ContentControl1" runat="server">
                                                        <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                    </dx:ContentControl>
                                                </div>
                                                <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                    <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                    <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                    </dx:ASPxGridView>
                                    <dx1:ASPxLabel ID="lblCostControl" runat="server" Text="CC" Visible="false">
                                    </dx1:ASPxLabel>
                                    <asp:SqlDataSource ID="sdsCC" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                                        SelectCommand="sp_SpecInformation_GetDepartment" SelectCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lblCostControl" PropertyName="Text" Name="Department_Code"
                                                Type="String" DefaultValue="CC" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                    <asp:SqlDataSource ID="sdsDepartment" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                                        SelectCommand="SELECT Par_Code, Par_Description FROM Mst_Parameter WHERE Par_Group = 'Department'"
                                        SelectCommandType="Text"></asp:SqlDataSource>
                                </div>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="BidderList" Text="Bidder List">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl3" runat="server" >
                                <dx:ASPxGridView ID="GridBidderList" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                                    EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
                                    Font-Size="9pt" KeyFieldName="SeqNo">
                                    <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
                                    <Columns>
                                      <%--  <dx:GridViewCommandColumn ShowEditButton="true" ShowDeleteButton="true">
                                        </dx:GridViewCommandColumn>--%>
                                        <dx:GridViewDataTextColumn Caption="No" VisibleIndex="1" FieldName="SeqNo" Width="150px">
                                            <PropertiesTextEdit Style-HorizontalAlign="Center">
                                                <Style HorizontalAlign="Center"></Style>
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="false" />
                                            <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataComboBoxColumn Caption="SPLR Name" FieldName="Supplier_Code" VisibleIndex="2"
                                            Width="350px">
                                            <PropertiesComboBox DataSourceID="sdsSupplier" TextField="Supplier_Name" ValueField="Supplier_Code"
                                                Width="145px" TextFormatString="{1}" ClientInstanceName="Supplier_Code" DropDownStyle="DropDownList"
                                                IncrementalFilteringMode="StartsWith">
                                                <Columns>
                                                    <dx:ListBoxColumn FieldName="Supplier_Code" Caption="Code" Width="65px" />
                                                    <dx:ListBoxColumn FieldName="Supplier_Name" Caption="Name" Width="200px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px">
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                                <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn Caption="Main Parts" FieldName="Main_Parts" VisibleIndex="5"
                                            Width="150px">
                                            <PropertiesTextEdit Width="100px">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="default" />
                                            <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Main Customer" VisibleIndex="6" FieldName="Main_Customer"
                                            Width="120px">
                                            <PropertiesTextEdit Width="100px">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="default" />
                                            <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Explanation Meetings" FieldName="Explanation_meetings"
                                            VisibleIndex="7" Width="150px">
                                            
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Remarks" VisibleIndex="7" Width="130px" FieldName="Remarks">
                                            <PropertiesTextEdit Width="100px">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="default" />
                                            <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                                            Width="320" />
                                    </SettingsPopup>
                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                                        EditFormColumnCaption-Paddings-PaddingRight="10px">
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>
                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                                            </Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange">
                                        </CommandColumnItem>
                                    </Styles>
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server"></dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                                </dx:ASPxGridView>
                                <asp:SqlDataSource ID="sdsSupplier" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                                    SelectCommand="select Supplier_Code, Supplier_Name from Mst_Supplier" SelectCommandType="Text">
                                </asp:SqlDataSource>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="SourcingSchedule" Text="Sourcing Schedule">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl4" runat="server" >
                                <dx:ASPxGridView ID="GridSourcing" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                                    EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
                                    Font-Size="9pt" KeyFieldName="Code_Activity">
                                    <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
                                    <Columns>
                                      <%--  <dx:GridViewCommandColumn ShowEditButton="true" ShowDeleteButton="true">
                                        </dx:GridViewCommandColumn>--%>
                                        <dx:GridViewDataTextColumn Caption="Activity" VisibleIndex="1" FieldName="Code_Activity"
                                            Width="150px" Visible="false">
                                            <PropertiesTextEdit Style-HorizontalAlign="Center">
                                                <Style HorizontalAlign="Center"></Style>
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="false" />
                                            <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Activity" VisibleIndex="1" FieldName="Activity"
                                            Width="150px">
                                            <PropertiesTextEdit Style-HorizontalAlign="Center">
                                                <Style HorizontalAlign="Center"></Style>
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="false" />
                                            <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Due Date Plan" FieldName="Due_Date_Plan" VisibleIndex="2"
                                            Width="150px">
                                            <PropertiesDateEdit Width="100px" DisplayFormatString="dd MMM yyyy" EditFormat="Custom"
                                                EditFormatString="dd/MMM/yyyy">
                                                <CalendarProperties>
                                                    <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                                                        <Paddings Padding="5px"></Paddings>
                                                    </HeaderStyle>
                                                    <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                                                        <Paddings Padding="5px"></Paddings>
                                                    </DayStyle>
                                                    <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                                        <Paddings Padding="5px"></Paddings>
                                                    </WeekNumberStyle>
                                                    <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                                                        <Paddings Padding="10px"></Paddings>
                                                    </FooterStyle>
                                                    <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                                        <Paddings Padding="10px"></Paddings>
                                                    </ButtonStyle>
                                                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                                                        <Paddings Padding="4px"></Paddings>
                                                    </ButtonStyle>
                                                </CalendarProperties>
                                            </PropertiesDateEdit>
                                            <Settings AllowAutoFilter="False" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn Caption="Date Actual" FieldName="Date_Actual" VisibleIndex="3"
                                            Width="150px">
                                            <PropertiesDateEdit Width="100px" DisplayFormatString="dd MMM yyyy" EditFormat="Custom"
                                                EditFormatString="dd/MMM/yyyy">
                                                <CalendarProperties>
                                                    <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                                                        <Paddings Padding="5px"></Paddings>
                                                    </HeaderStyle>
                                                    <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                                                        <Paddings Padding="5px"></Paddings>
                                                    </DayStyle>
                                                    <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                                        <Paddings Padding="5px"></Paddings>
                                                    </WeekNumberStyle>
                                                    <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                                                        <Paddings Padding="10px"></Paddings>
                                                    </FooterStyle>
                                                    <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                                        <Paddings Padding="10px"></Paddings>
                                                    </ButtonStyle>
                                                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                                                        <Paddings Padding="4px"></Paddings>
                                                    </ButtonStyle>
                                                </CalendarProperties>
                                            </PropertiesDateEdit>
                                            <Settings AllowAutoFilter="False" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Comment" FieldName="Comment" VisibleIndex="4"
                                            Width="150px">
                                            <PropertiesTextEdit Width="100px">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="default" />
                                            <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                                            Width="320" />
                                    </SettingsPopup>
                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                                        EditFormColumnCaption-Paddings-PaddingRight="10px">
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>
                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                                            </Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange">
                                        </CommandColumnItem>
                                    </Styles>
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server"></dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server"></dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
            </dx:ASPxPageControl>
        </div>
        <div>
            <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <%--<ClientSideEvents CallbackComplete="MessageBox" />--%>
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbkDrawingComplete" runat="server" ClientInstanceName="cbkDrawingComplete">
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                <ClientSideEvents Init="MessageError" />
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbSpecInformation" runat="server" ClientInstanceName="cbSpecInformation">
                <ClientSideEvents EndCallback="function(s,e) {
              if (s.cp_SpecInformation == 'true')
              {
                    btnSpecInformation.SetEnabled(s.cp_SpecInformation);
              } else {
     
              }
          }
           " />
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbComplete" runat="server" ClientInstanceName="cbComplete">
                <ClientSideEvents EndCallback="function(s,e) {
            if (s.cp_Complete == 'true')
              {
                btnComplete.SetEnabled(true);
                }
                else
                {
     
                 btnComplete.SetEnabled(false);
                }  
          }
           " />
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbReject" runat="server" ClientInstanceName="cbReject">
                <ClientSideEvents CallbackComplete="MessageError" />                                           
            </dx:ASPxCallback>
              <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="Select Project_ID, Project_Name From Proj_Header"></asp:SqlDataSource>
                        &nbsp;<dx:ASPxCallback ID="cbupload" runat="server" ClientInstanceName="cbupload">
                        </dx:ASPxCallback>
                        &nbsp;<asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Language'">
                        </asp:SqlDataSource>
                        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                        <dx:ASPxGridViewExporter ID="GridExporter2" runat="server" GridViewID="GridCostControl">
                        </dx:ASPxGridViewExporter>
                        <dx:ASPxGridViewExporter ID="GridExporter3" runat="server" GridViewID="GridBidderList">
                        </dx:ASPxGridViewExporter>
                        <dx:ASPxGridViewExporter ID="GridExporter4" runat="server" GridViewID="GridSourcing">
                        </dx:ASPxGridViewExporter>
                        <dx:ASPxGridViewExporter ID="GridExporter5" runat="server" GridViewID="GridHeaderSpecInformation">
                        </dx:ASPxGridViewExporter>
            <br />
        </div>
    </div>
    
</asp:Content>

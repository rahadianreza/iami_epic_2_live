﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web

Module mdlMain
    Public gs_PRType As Integer
    Public gs_Department As Integer
    Public gs_Section As Integer
    Public gs_PRTypeCode As String
    Public gs_DepartmentCode As String
    Public gs_SectionCode As String
    Public gs_Back As Boolean = False
    Public gs_dtFromBack, gs_dtToBack As DateTime
    Public gs_NewPR As String
    Public gs_Modify As Boolean = False
    Public gs_Search As Boolean = False

    Public gs_GeneratePRNo As String
    Public gs_MaterialNo_Edit As String
    Public gs_Rev As Integer

    'Variabel Agus
    '###########################################################################
    Public gs_Message As String = ""
    Public gs_RFQNo1, gs_RFQNo2, gs_RFQNo3, gs_RFQNo4, gs_RFQNo5 As String
    Public gs_RFQNo As String
    Public gs_RFQNoFilter As String
    Public gs_RFQTitle As String
    Public gs_SetRFQNo As String = ""
    Public gs_RFQDate As String

    Public gs_RFQNoIndex As Integer

    Public vSupplier(4) As String
    Public vStatus(4) As String
    Public vRFQNo(4) As String

    Public pDataChange As Boolean = False

    Public gs_Approval As Integer
    Public gs_ApprovalCode As String
    Public gs_DateFrom As String
    Public gs_DateTo As String
    Public gs_FilterGroupIndex As Integer
    Public gs_FilterCategoryIndex As Integer
    Public gs_FilterPRTypeIndex As Integer
    Public gs_FilterSupplierIndex As Integer
    Public gs_FilterVariantIndex As Integer

    Public gs_FilterGroup As String
    Public gs_FilterCategory As String
    Public gs_FilterPRType As String
    Public gs_FilterSupplier As String
    Public gs_FilterVariant As String

    Public gs_CENumber As String
    Public gs_CENumberIndex As Integer
    Public gs_CENo As String
    Public gs_CERevNo As String
    Public gs_Revision As Integer

    Public gs_Supplier As String
    Public gs_SRNumber As String
    Public gs_SRNumberIndex As Integer
    Public gs_SRNo As String
    Public gs_SRRevNo As String
    Public gs_SRRevision As Integer
    Public gs_Status As String

    Public gs_IANumber As String
    Public gs_IANumberIndex As Integer
    Public gs_IANo As String
    Public gs_IARevNo As String
    Public gs_IARevision As Integer
    Public gs_IAStatus As String
    Public gs_StatusApproval As String
    Public gs_StatusAssign As String

    Public gs_CPNumber As String
    Public gs_CPRev As String
    '###########################################################################

    Enum CmdType
        StoreProcedure = 0
        SQLScript = 1
    End Enum
    Public Function uf_ConvertMonth(pMonth As Integer) As String
        Dim angka As String = ""

        If pMonth = 1 Then
            angka = "I"
        ElseIf pMonth = 2 Then
            angka = "II"
        ElseIf pMonth = 3 Then
            angka = "III"
        ElseIf pMonth = 4 Then
            angka = "IV"
        ElseIf pMonth = 5 Then
            angka = "V"
        ElseIf pMonth = 6 Then
            angka = "VI"
        ElseIf pMonth = 7 Then
            angka = "VII"
        ElseIf pMonth = 8 Then
            angka = "VIII"
        ElseIf pMonth = 9 Then
            angka = "IX"
        ElseIf pMonth = 10 Then
            angka = "X"
        ElseIf pMonth = 11 Then
            angka = "XI"
        ElseIf pMonth = 12 Then
            angka = "XII"
        End If

        Return angka
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="CmdType"></param>
    ''' <param name="CmdText">For StoreProcedure: [sp name], e.g sp_QuotationList_FillCombo. For SQLScript: SQL Command, e.g SELECT Quotation_Number FROM Quotation_Header.</param>
    ''' <param name="CmdParameter">"Parameter1|Parameter2|Parameter3|...|ParameterN"</param>
    ''' <param name="CmdValueParameter">"ValParameter1|ValParameter2|ValParameter3|...|ValParameterN"</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetDataSource(ByVal CmdType As CmdType, ByVal CmdText As String, Optional CmdParameter As String = "", Optional CmdValueParameter As String = "", Optional ByRef ErrMsg As String = "") As DataSet
        Dim sqlConn As New SqlConnection(Sconn.Stringkoneksi)
        Dim sqlCmd As New SqlCommand
        Dim sqlDA As New SqlDataAdapter()
        Dim ds As New DataSet
        Dim toIdx As Integer = 0

        Try
            sqlConn.Open()

            Select Case CmdType
                Case CmdType.StoreProcedure
                    sqlCmd = New SqlCommand(CmdText, sqlConn)
                    sqlCmd.CommandType = CommandType.StoredProcedure

                    If Trim(CmdParameter) <> "" Then
                        toIdx = CountCharacter(CmdParameter, "|")
                        Try
                            For idx As Integer = 0 To toIdx
                                sqlCmd.Parameters.AddWithValue(Split(CmdParameter, "|")(idx), Split(CmdValueParameter, "|")(idx))
                            Next idx

                        Catch ex As Exception
                            'ignore the error
                        End Try
                    End If

                Case CmdType.SQLScript
                    sqlCmd = New SqlCommand(CmdText, sqlConn)
                    sqlCmd.CommandType = CommandType.Text

                    If Trim(CmdParameter) <> "" Then
                        toIdx = CountCharacter(CmdParameter, "|")
                        Try
                            For idx As Integer = 0 To toIdx - 1
                                sqlCmd.Parameters.AddWithValue(Split(CmdParameter, "|")(idx), Split(CmdValueParameter, "|")(idx))
                            Next idx

                        Catch ex As Exception
                            'ignore the error
                        End Try
                    End If

                    sqlDA = New SqlDataAdapter(CmdText, sqlConn)
                    sqlDA.Fill(ds)
            End Select

            ds = New DataSet
            sqlDA = New SqlDataAdapter(sqlCmd)
            sqlDA.Fill(ds)

            sqlConn.Close()
            Return ds

        Catch ex As Exception
            ErrMsg = Err.Description
            Return Nothing
        End Try
    End Function

    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt
    End Function

    Public Sub DisplayMessage(ByVal pMsgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pObject As Object)
        Dim msgType As String = ""
        Select Case pMsgType
            Case MsgTypeEnum.Info
                msgType = "0"
            Case MsgTypeEnum.Success
                msgType = "1"
            Case MsgTypeEnum.Warning
                msgType = "2"
            Case MsgTypeEnum.ErrorMsg
                msgType = "3"
        End Select

        pObject.JSProperties("cpMessage") = ErrMsg
        pObject.JSProperties("cpType") = msgType
    End Sub
End Module

﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApprovalSet.aspx.vb" Inherits="IAMI_EPIC2.ApprovalSet" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="img/computer-41.ico" rel="SHORTCUT ICON" type="image/icon" />
    <script type="text/javascript">
        function OnInit(s, e) {
            AdjustSizeGrid();
        }

        function OnAccessCheckedChanged(s, e) {
            Grid.SetFocusedRowIndex(-1);
            if (s.GetValue() == -1) s.SetValue(1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "isApproval", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "isApproval", s.GetValue());
                }
            }
        }

        function AdjustSizeGrid() {

            var myWidth = 0, myHeight = 0;
            if (typeof (window.innerWidth) == 'number') {
                //Non-IE
                myWidth = window.innerWidth;
                myHeight = window.innerHeight;
            } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                //IE 6+ in 'standards compliant mode'
                myWidth = document.documentElement.clientWidth;
                myHeight = document.documentElement.clientHeight;
            } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                //IE 4 compatible
                myWidth = document.body.clientWidth;
                myHeight = document.body.clientHeight;
            }

            var height = Math.max(0, myHeight);
            height = height - (height * 70 / 100)
            Grid.SetHeight(height);

            
        }

        function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "Approval_Group" || currentColumnName == "Approval_ID" || currentColumnName == "Approval_Level" || currentColumnName == "Approval_Name") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        }

        function SavePrivilege(s, e) {
           
                Grid.UpdateEdit();
                millisecondsToWait = 1000;
                setTimeout(function () {
                    Grid.PerformCallback('loadPrivilege|00');
                    alert('Data save successfully!');
                }, millisecondsToWait);
           
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                    Font-Names="Segoe UI" Font-Size="8pt" KeyFieldName="Approval_Group;Approval_ID" Theme="Office2010Black"
                    Width="100%">
                                        <ClientSideEvents 
                                        BatchEditStartEditing="OnBatchEditStartEditing" 
                                        EndCallback="function(s, e) { 
	                                            Grid.CancelEdit();
                                            }" CallbackError="function(s, e) {
	                                        e.Cancel=True;
                                        }" />                    
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Approval Group" FieldName="Approval_Group" Name="Approval_Group"
                            ReadOnly="True" VisibleIndex="0" Width="220px">
                            <CellStyle Font-Names="Segoe UI" Font-Size="8pt">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn Caption="Approval ID" FieldName="Approval_ID" Name="Approval_ID"
                            ReadOnly="True" VisibleIndex="1" Width="100px">
                            <CellStyle Font-Names="Segoe UI" Font-Size="8pt">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn Caption="Approval Level" FieldName="Approval_Level" Name="Approval_Level"
                            ReadOnly="True" VisibleIndex="2" Width="100px">
                            <CellStyle Font-Names="Segoe UI" Font-Size="8pt">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Approval Name" FieldName="Approval_Name" 
                            Name="Approval_Name" VisibleIndex="3" Width="300px">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn Caption="" FieldName="isApproval" Name="isApproval"
                            VisibleIndex="4" Width="120px">
                            <PropertiesCheckEdit ValueChecked="1" ValueType="System.String" 
                                                    ValueUnchecked="0" AllowGrayedByClick="false">
                                                </PropertiesCheckEdit>
                            <HeaderCaptionTemplate>
                            <dx:ASPxCheckBox ID="chkAccess" runat="server" ClientInstanceName="chkAccess" ClientSideEvents-CheckedChanged="OnAccessCheckedChanged" ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="is Approval" Font-Names="Segoe UI" Font-Size="8pt" ForeColor="White"> 
                            </dx:ASPxCheckBox>
                            </HeaderCaptionTemplate> 
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataCheckColumn>
                    </Columns>
                    <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                    </SettingsEditing>
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="300" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>
                </dx:ASPxGridView>
            
        </div>
          <table width="100%">
                    <tr>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td align="left">
                            <dx:ASPxTextBox ID="txtUserIDTemp" runat="server" BackColor="White" ClientInstanceName="txtUserIDTemp"
                                ForeColor="White" Width="170px">
                                <Border BorderColor="White" />
                            </dx:ASPxTextBox>
                        </td>
                        <td align="center" style="width:85px">
                            &nbsp;</td>
                        <td align="center" style="width:85px">
                            &nbsp;</td>
                        <td align="center" style="width:85px">
                            &nbsp;</td>
                        <td align="center" style="width:85px">
                            &nbsp;</td>
                        <td align="center" style="width:85px">
                            <dx:ASPxButton ID="btnSubmit" runat="server" AutoPostBack="False" ClientInstanceName="btnSubmit"
                                Font-Names="Segoe UI" Font-Size="8pt" Text="Save" Theme="Office2010Silver" 
                                Width="80px">
                                <ClientSideEvents Click="SavePrivilege" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
    </form>
</body>
</html>

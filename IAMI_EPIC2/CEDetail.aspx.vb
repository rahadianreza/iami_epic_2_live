﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors

Public Class CEDetail
    Inherits System.Web.UI.Page
#Region "Declaration"
    'Dim ls_VCENumber As String

    Dim pUser As String = ""

    Dim vCENumber As String
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region
    Dim txtCEParameter As Object
    Dim ls_CENumber As String

    Private Sub up_FillComboPRNo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsCostEstimationDB.GetComboPRNo(pUser, pErr)
        If pErr = "" Then
            cboPRNo.DataSource = ds
            cboPRNo.DataBind()
        End If


    End Sub

    Private Sub up_FillComboRFQ(pPRNo As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsCostEstimationDB.GetComboRFQSetNo(pPRNo, pErr)
        If pErr = "" Then

            cboRFQSetNumber.DataSource = ds
            cboRFQSetNumber.DataBind()
        End If
    End Sub

    Private Sub up_FillComboSupplier(pRFQSetNo As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsCostEstimationDB.GetComboSupplier(pRFQSetNo, pErr)
        If pErr = "" Then
            'cboSupplier.DataSource = ds
            'cboSupplier.DataBind()
        End If
    End Sub

    Private Sub up_GridHeader(pRFQSetNo As String, pSupplier As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        For i = 0 To 4
            Grid.Columns.Item(6 + i).Visible = False
        Next

        ds = clsCostEstimationDB.GetDataSupplier(pRFQSetNo, pSupplier, pErr)
        If pErr = "" Then

            For i = 0 To ds.Tables(0).Rows.Count - 1
                Grid.Columns.Item(6 + i).Caption = ds.Tables(0).Rows(i)("Description")
                Grid.Columns.Item(6 + i).Visible = True
            Next

        End If
    End Sub

  


    Private Sub up_GridLoadCE(CENumber As String, Revision As Integer)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim vSupplier As String = ""
        Dim headSupplier As String = ""
        Dim vname As String = "Supplier"
        Dim vfield As String = "S"
        Dim a As Integer

        ds = clsCostEstimationDB.GetCEData(CENumber, Revision, ErrMsg)

        If ds.Tables(0).Rows.Count > 0 Then
            txtCENumber.Text = CENumber
            dtCEDate.Value = ds.Tables(0).Rows(0)("CE_Date")
            cboPRNo.Text = ds.Tables(0).Rows(0)("PR_Number")
            cboRFQSetNumber.Text = ds.Tables(0).Rows(0)("RFQ_Set")
            txtRev.Text = Revision

            txtParameter.Text = CENumber & "|" & Revision & "|" & ds.Tables(0).Rows(0)("RFQ_Set")

            'If ds.Tables(0).Rows(0)("CE_Status") = "1" Or ds.Tables(0).Rows(0)("CE_Status") = "2" Or ds.Tables(0).Rows(0)("CE_Status") = "4" Then
            '    btnSubmit.Enabled = False
            '    btnDraft.Enabled = False
            'End If
            uploaderFile.Enabled = True
            cbDraft.JSProperties("cpStatus") = ds.Tables(0).Rows(0)("CE_Status")
            gs_CERevNo = ds.Tables(0).Rows(0)("Revision_No")
            txtNotes.Text = ds.Tables(0).Rows(0)("CE_Notes")
            'txtIABudgetNumber.Text = ds.Tables(0).Rows(0)("IA_BudgetNumber")

            'IALinkFile.Text = ds.Tables(0).Rows(0)("IA_BudgetNumber") '("CE_Budget")
            'IABudgetNo.Set("hflink", ds.Tables(0).Rows(0)("FileName_Acceptance"))
          
            If ds.Tables(0).Rows(0)("Skip_Budget") = "1" Then
                chkSkip.Checked = True
            Else
                chkSkip.Checked = False
            End If
            lblIABudgetNo.Text = ds.Tables(0).Rows(0)("IA_BudgetNumber")
            IALinkFile.Text = IIf(String.IsNullOrEmpty(ds.Tables(0).Rows(0)("FileName_Acceptance")), "No File", ds.Tables(0).Rows(0)("FileName_Acceptance"))
            'LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
            Dim extensionIA As String = ""
            extensionIA = System.IO.Path.GetExtension("~/Files/" & ds.Tables(0).Rows(0)("FileName_Acceptance"))
            If extensionIA <> ".pdf" Then
                If ds.Tables(0).Rows(0)("FileName_Acceptance") <> "" Then
                    IALinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("FileName_Acceptance")
                End If
            End If



            'LinkFile.Text = ds.Tables(0).Rows(0)("File_Name")
            'LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
            'add 27-02-2019
            LinkFile.Text = IIf(String.IsNullOrEmpty(ds.Tables(0).Rows(0)("File_Name")), "No File", ds.Tables(0).Rows(0)("File_Name"))
            'LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
            Dim extension As String = ""
            extension = System.IO.Path.GetExtension("~/Files/" & ds.Tables(0).Rows(0)("File_Name"))
            If extension <> ".pdf" Then
                If ds.Tables(0).Rows(0)("File_Name") <> "" Then
                    LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
                End If
            End If


            cbDraft.JSProperties("cpSkip") = ds.Tables(0).Rows(0)("Skip_Budget")

            txtReason.Text = ds.Tables(0).Rows(0)("Skip_Notes")

            dtCEDate.Enabled = False

            'dtCEDate.ReadOnly = True
            'cboRFQSetNumber.ReadOnly = True
            'cboPRNo.ReadOnly = True

            'dtCEDate.BackColor = Color.Silver

            'cboPRNo.BackColor = Color.Silver

            'cboRFQSetNumber.BackColor = Color.Silver
            txtFlag.Text = 1
        End If

        'begin harusnya sudah gak perlu bind supplier dengan cara seperti ini , get supplier dari sp saja 13-03-2019
        ds = clsCostEstimationDB.GetDataSupplier(cboRFQSetNumber.Text, "00", ErrMsg)
        For i = 0 To ds.Tables(0).Rows.Count - 1
            a = a + 1

            vSupplier = vSupplier & "[" & ds.Tables(0).Rows(i)("Code") & "]" & ","

            headSupplier = headSupplier & "ISNULL([" & ds.Tables(0).Rows(i)("Code") & "],0) As " & vname & a & ","
        Next

        If a < 5 Then
            For a = a + 1 To 5
                vSupplier = vSupplier & "[" & vfield & a & "]" & ","
                headSupplier = headSupplier & "ISNULL([" & vfield & a & "],0) As " & vname & a & ","
            Next
        End If


        If Len(vSupplier) > 0 Then
            vSupplier = Left(vSupplier, Len(vSupplier) - 1)
            headSupplier = Left(headSupplier, Len(headSupplier) - 1)
        End If

        'Dim ds1 As New DataSet
        'parameter vSupplier dan headSupplier gak perlu lagi karena sudah dari sp , add parameter RFQ_Set saja (value ambil cboRFQSetNumber.Text)
        ds = clsCostEstimationDB.GetListCEData(CENumber, Revision, cboRFQSetNumber.Text, ErrMsg)
        'ds = clsCostEstimationDB.GetListCEData(CENumber, vSupplier, headSupplier, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If

    End Sub


    Private Sub up_GridLoad(RFSet As String, Supplier As String)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        Dim vSupplier As String = ""
        Dim headSupplier As String = ""
        Dim vname As String = "Supplier"
        Dim vfield As String = "S"
        Dim a As Integer

        '13-03-2019 get supllier dari sp								
        If Supplier = "00" Then
            ds = clsCostEstimationDB.GetDataSupplier(RFSet, Supplier, ErrMsg)
            For i = 0 To ds.Tables(0).Rows.Count - 1
                a = a + 1

                vSupplier = vSupplier & "[" & ds.Tables(0).Rows(i)("Code") & "]" & ","

                headSupplier = headSupplier & "ISNULL([" & ds.Tables(0).Rows(i)("Code") & "],0) As " & vname & a & ","
            Next

            If a < 5 Then
                For a = a + 1 To 5
                    vSupplier = vSupplier & "[" & vfield & a & "]" & ","
                    headSupplier = headSupplier & "ISNULL([" & vfield & a & "],0) As " & vname & a & ","
                Next
            End If


            If Len(vSupplier) > 0 Then
                vSupplier = Left(vSupplier, Len(vSupplier) - 1)
                headSupplier = Left(headSupplier, Len(headSupplier) - 1)
            End If

        Else
            headSupplier = "[" & Supplier & "] As Supplier1" & ", [S2] As Supplier2" & ", [S3] As Supplier3" & ", [S4] As Supplier4" & ", [S5] As Supplier5"
            vSupplier = "[" & Supplier & "],[S2],[S3],[S4],[S5] "
        End If


        'ds = clsCostEstimationDB.GetList(RFSet, vSupplier, headSupplier, ErrMsg)
        ds = clsCostEstimationDB.GetList(RFSet, ErrMsg)

        If ErrMsg = "" Then

            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_UploadFile()
        Dim folderPath As String = Server.MapPath("~/Files/")
        Dim SizeFile As Single
        Dim fullPath As String

        If Not Directory.Exists(folderPath) Then
            'If Directory (Folder) does not exists. Create it.
            Directory.CreateDirectory(folderPath)
        End If
        Dim PostedFile1 As HttpPostedFile = uploaderFile.PostedFile
        Dim FileName1 As String = "", RenameFile As String = ""
        Dim CENumber As String = "", extension As String = ""
        CENumber = txtCENumber.Text
        CENumber = Replace(CENumber, "/", "")
        CENumber = Replace(CENumber, "~", "")
        CENumber = Replace(CENumber, "\", "")

        If uploaderFile.FileName <> "" Then
            extension = System.IO.Path.GetExtension(uploaderFile.FileName)
            FileName1 = Regex.Replace(Path.GetFileNameWithoutExtension(PostedFile1.FileName), "[\[\]\\\^\$\.\|\?\*\+\(\)\{\}%,;><!@#&\-\+/]", "") + extension
            uploaderFile.SaveAs(folderPath & FileName1)


            RenameFile = CENumber & "_" & Format(CDate(Now), "yyyyMMdd_hhmmss") & ".pdf"
            fullPath = String.Format("{0}{1}", folderPath, FileName1)
            SizeFile = uploaderFile.FileBytes.Length / 1024 / 1024

            If SizeFile > 2 Then
                cbFile.JSProperties("cpMessage") = "File size must not exceed 2 MB"
                Exit Sub
            End If
            clsCostEstimationDB.UpdateFile(txtCENumber.Text, fullPath, FileName1, pUser)
            cbFile.JSProperties("cpMessage") = "Upload File Succesfull"
            cbFile.JSProperties("cpFileName") = FileName1
            'cbFile.JSProperties("cpMessage") = "Upload File Succesfull"

        End If
        'ScriptManager.RegisterStartupScript(cbIABudget, cbIABudget.GetType(), "cbIABudget", "cbIABudget.SetEnabled(false);", True)


    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|D010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnSubmit.SetEnabled(false);" & vbCrLf & _
                         "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboRFQSetNumber)
            End Try
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("D010")
        ' Master.SiteTitle = sGlobal.menuName
        Master.SiteTitle = "COST ESTIMATION DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "D010")
        Dim CENumber As String = ""
        Dim RFQSet As String = ""
        Dim Revision As Integer
        Dim vDate1 As Date
        Dim vDate2 As Date

        If Request.QueryString("ID") <> "" Then
            CENumber = Split(Request.QueryString("ID"), "|")(0)
            Revision = Split(Request.QueryString("ID"), "|")(1)
            RFQSet = Split(Request.QueryString("ID"), "|")(2)
        End If

        If Not Page.IsPostBack Or Not Page.IsCallback Then

            If CENumber = "" Then
                Session("CENumber") = ""
                Session("CERev") = "0"
                gs_CENumber = ""
                txtCENumber.Text = "--NEW--"
                gs_CENo = "--NEW--"
                vDate1 = Split(Request.QueryString("Date"), "|")(0)
                vDate2 = Split(Request.QueryString("Date"), "|")(1)
                gs_DateFrom = Format(vDate1, "yyyy-MM-dd")
                gs_DateTo = Format(vDate2, "yyyy-MM-dd")
                txtRev.Text = IIf(txtRev.Text = "", 0, txtRev.Text)

                up_FillComboPRNo()
                'up_FillComboBudgetNo()
                dtCEDate.Value = Now
                up_GridHeader("xx", "xx")
                cbDraft.JSProperties("cpStatus") = ""
                cbDraft.JSProperties("cpSkip") = ""

                txtFlag.Text = 0
            Else
                gs_CENo = CENumber
                gs_CENumber = CENumber
                txtCENumber.Text = CENumber
                up_GridHeader(RFQSet, "00")
                up_GridLoadCE(CENumber, Revision)
                Session("CENumber") = CENumber
                Session("CERev") = Revision
                'up_FillComboBudgetNo()
            End If
            'up_GridLoad(CENumber)

            'Else
            '  gs_CENumber = IIf(txtCENumber.Text = "--NEW--", CENumber, txtCENumber.Text)
        End If

        'If IsCallback Then
        '    up_GridLoadCE(txtCENumber.Text, txtRev.Text)
        'End If

        cbFile.JSProperties("cpMessage") = ""
        cbDraft.JSProperties("cpMessage") = ""
    End Sub

    Private Sub Grid_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If e.Column.FieldName = "Remarks" Then
            TryCast(e.Editor, ASPxTextBox).AutoCompleteType = Web.UI.WebControls.AutoCompleteType.Disabled
        End If
    End Sub

    'Private Sub Grid_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.BeforeGetCallbackResult
    '    If Grid.IsNewRowEditing Then
    '        Grid.SettingsCommandButton.UpdateButton.Text = "Save"
    '    End If
    'End Sub

    'Protected Sub Grid_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles Grid.RowInserting
    '    e.Cancel = True
    '    Dim pErr As String = ""
    '    Dim term As New clsCostEstimation With {.MaterialNo = e.NewValues("Item_Code"),
    '                                     .Qty = e.NewValues("Qty"),
    '                                     .Remarks = e.NewValues("Remarks")}
    '    clsCostEstimationDB.Insert(term, pErr)

    '    If pErr = "" Then
    '        Grid.CancelEdit()
    '        up_GridLoad()
    '    End If

    'End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/CEList.aspx")
    End Sub

    'Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
    '    If e.CallbackName <> "CANCELEDIT" And e.CallbackName <> "CUSTOMCALLBACK" Then
    '        up_GridLoadCE(txtCENumber.Text, txtRev.Text)
    '    End If
    'End Sub

    'Private Sub cboSupplier_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSupplier.Callback
    '    Dim pRFQSetNo As String = e.Parameter
    '    up_FillComboSupplier(pRFQSetNo)
    'End Sub

    Private Function IsDataExist() As String
        Dim retVal As String = ""
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CostEstimation_Exist", "CENumber", txtCENumber.Text, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = ds.Tables(0).Rows(0).Item("Ret")
                Else
                    'NOT EXISTS
                    retVal = "N"
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbDraft)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        If e.UpdateValues.Count = 0 Then
            gs_Message = "Data is not found"
            Exit Sub
        End If
        Dim errmsg As String = ""
        Dim ls_SQL As String = ""
        'Dim lb_IsDataExist As String = IsDataExist()

        Dim CECls As New clsCostEstimation
        CECls.CEDate = Format(dtCEDate.Value, "yyyy-MM-dd")
        CECls.CEStatus = "0"
        If chkSkip.Checked = False Then
            CECls.SkipBudget = "0"
        Else
            CECls.SkipBudget = "1"
        End If

        CECls.SkipNotes = txtReason.Text
        CECls.RFQSetNumber = cboRFQSetNumber.Text
        CECls.CENotes = txtNotes.Text
        If txtRev.Text = "" Then
            CECls.Revision = 0
        Else
            CECls.Revision = txtRev.Text
        End If

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                If txtCENumber.Text = "--NEW--" Then
                    ls_SQL = "sp_CostEstimationHeader_Ins"  'insert header 
                Else
                    ls_SQL = "sp_CostEstimationHeader_Upd"
                End If

                Dim cmd As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("RFQSet", CECls.RFQSetNumber)
                cmd.Parameters.AddWithValue("Revision", CECls.Revision)
                'cmd.Parameters.AddWithValue("CENumber", pCost.CENumber)
                cmd.Parameters.AddWithValue("CEDate", CECls.CEDate)
                cmd.Parameters.AddWithValue("CEStatus", CECls.CEStatus)
                cmd.Parameters.AddWithValue("SkipNotes", CECls.SkipNotes)
                cmd.Parameters.AddWithValue("SkipBudget", CECls.SkipBudget)
                cmd.Parameters.AddWithValue("CENotes", CECls.CENotes)
                'cmd.Parameters.AddWithValue("CEBudget", IIf(pCost.CEBudget = Nothing, "", pCost.CEBudget))
                cmd.Parameters.AddWithValue("UserId", pUser)

                Dim outPutParameter As SqlParameter = New SqlParameter()
                If txtCENumber.Text = "--NEW--" Then
                    cmd.Parameters.AddWithValue("CENumber", "")
                    outPutParameter.ParameterName = "CENumberOutput"
                    outPutParameter.SqlDbType = System.Data.SqlDbType.VarChar
                    outPutParameter.Direction = System.Data.ParameterDirection.Output
                    outPutParameter.Size = 50
                    cmd.Parameters.Add(outPutParameter)
                Else
                    cmd.Parameters.AddWithValue("CENumber", txtCENumber.Text) 'for update
                End If
                cmd.ExecuteNonQuery()
                If txtCENumber.Text = "--NEW--" Then
                    ls_CENumber = outPutParameter.Value.ToString()
                    gs_CENumber = outPutParameter.Value.ToString()
                    Session("CENumber") = outPutParameter.Value.ToString()
                    Session("CERev") = "0"
                    txtCENumber.Text = ls_CENumber
                Else
                    ls_CENumber = txtCENumber.Text
                End If

                cmd.Dispose()

                '#DETAIL 
                If ls_CENumber <> "" Or ls_CENumber <> "--NEW--" Then
                    For idx = 0 To e.UpdateValues.Count - 1
                        Dim ls_SQLSP As String
                        ls_SQLSP = "sp_CostEstimation_Detail_InsertUpdate"

                        CECls.MaterialNo = Trim(e.UpdateValues(idx).NewValues("Item_Code").ToString()) ' ls_MaterialNo
                        CECls.CENumber = ls_CENumber
                        CECls.Qty = e.UpdateValues(idx).NewValues("Qty") ' li_Qty
                        CECls.Price = e.UpdateValues(idx).NewValues("PriceEst") ' li_Price


                        If IsNothing(e.UpdateValues(idx).NewValues("Remarks")) Then
                            CECls.Remarks = ""
                        Else
                            CECls.Remarks = e.UpdateValues(idx).NewValues("Remarks")
                        End If

                        Dim sqlComm = New SqlCommand(ls_SQLSP, sqlConn, sqlTran)
                        sqlComm.CommandType = CommandType.StoredProcedure
                        sqlComm.Parameters.AddWithValue("Revision", CECls.Revision)
                        sqlComm.Parameters.AddWithValue("CENumber", CECls.CENumber)
                        sqlComm.Parameters.AddWithValue("MaterialNo", CECls.MaterialNo)
                        sqlComm.Parameters.AddWithValue("Qty", CECls.Qty)
                        sqlComm.Parameters.AddWithValue("Price", CECls.Price)
                        sqlComm.Parameters.AddWithValue("Remarks", CECls.Remarks)
                        sqlComm.Parameters.AddWithValue("UserID", pUser)

                        sqlComm.ExecuteNonQuery()
                        sqlComm.Dispose()
                    Next idx
                    sqlTran.Commit()
                End If
            End Using
        End Using


        'If gs_CENumber = "" Then
        '    up_SaveHeader(errmsg)
        'Else
        '    vCENumber = gs_CENumber
        'End If

        'If errmsg = "" Then
        '    up_SaveDetail(sender, e, errmsg)
        'End If

        'If errmsg = "" Then
        '    gs_Message = ""
        'Else
        '    gs_Message = errmsg
        'End If
        'Grid.EndUpdate()
    End Sub

    Private Sub up_SaveDetail(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs, Optional ByRef pErr As String = "")
        Dim a As Integer
        Dim ls_MaterialNo As String
        Dim li_Qty As Double
        Dim li_Price As Double
        Dim ls_CENumber As String
        Dim ls_Remarks As String = ""
        Dim CECls As New clsCostEstimation

        a = e.UpdateValues.Count

        If vCENumber <> "" Then
            ls_CENumber = vCENumber
            CECls.Revision = "0"
            Dim i As Integer
            For iLoop = 0 To a - 1

                ls_MaterialNo = Trim(e.UpdateValues(iLoop).NewValues("Item_Code").ToString())
                li_Qty = e.UpdateValues(iLoop).NewValues("Qty")
                li_Price = e.UpdateValues(iLoop).NewValues("PriceEst")
                ls_Remarks = e.UpdateValues(iLoop).NewValues("Remarks")

                CECls.MaterialNo = Trim(e.UpdateValues(iLoop).NewValues("Item_Code").ToString()) ' ls_MaterialNo
                CECls.CENumber = ls_CENumber
                CECls.Qty = e.UpdateValues(iLoop).NewValues("Qty") ' li_Qty
                CECls.Price = e.UpdateValues(iLoop).NewValues("PriceEst") ' li_Price
                If txtRev.Text = "" Then
                    CECls.Revision = 0
                Else
                    CECls.Revision = txtRev.Text
                End If

                If IsNothing(ls_Remarks) Then
                    CECls.Remarks = ""
                Else
                    CECls.Remarks = e.UpdateValues(iLoop).NewValues("Remarks")
                End If

                i = clsCostEstimationDB.UpdateDetail(CECls, pUser, pErr)

                If i = 0 Then
                    clsCostEstimationDB.InsertDetail(CECls, pUser, pErr)
                End If
            Next
        End If

    End Sub

    Private Function uf_ConvertMonth(pMonth As Integer) As String
        Dim angka As String = ""

        If pMonth = 1 Then
            angka = "I"
        ElseIf pMonth = 2 Then
            angka = "II"
        ElseIf pMonth = 3 Then
            angka = "III"
        ElseIf pMonth = 4 Then
            angka = "IV"
        ElseIf pMonth = 5 Then
            angka = "V"
        ElseIf pMonth = 6 Then
            angka = "VI"
        ElseIf pMonth = 7 Then
            angka = "VII"
        ElseIf pMonth = 8 Then
            angka = "VIII"
        ElseIf pMonth = 9 Then
            angka = "IX"
        ElseIf pMonth = 10 Then
            angka = "X"
        ElseIf pMonth = 11 Then
            angka = "XI"
        ElseIf pMonth = 12 Then
            angka = "XII"
        End If

        Return angka
    End Function

    Private Sub up_SaveHeader(Optional ByRef pErr As String = "")
        Dim Errmsg As String = ""
        'Dim ls_CENumber As String = ""
        Dim CECls As New clsCostEstimation
        Dim ds As New DataSet
        'Dim ls_Bln As String
        'Dim li_Bln As Integer = Format(Now, "MM")
        'Dim li_Year As Integer = Format(Now, "yyyy")

        '    If txtCENumber.Text = "--NEW--" Then
        '        ds = clsCostEstimationDB.GetMaxCENo(li_Bln, li_Year, Errmsg)
        '        If ds.Tables(0).Rows.Count > 0 Then
        '            ls_Bln = uf_ConvertMonth(Format(Now, "MM"))

        '            ls_CENumber = "IAMI/COST/CE/" & ls_Bln & "/" & Format(Now, "yy") & "/" & ds.Tables(0).Rows(0)("CE_Number")
        'gs_CENumber = ls_CENumber
        '        End If

        '        CECls.Revision = "0"

        '    End If
        '    Dim i As Integer

        'If ls_CENumber <> "" Then
        'ls_VCENumber = ls_CENumber

    
        CECls.CEDate = Format(dtCEDate.Value, "yyyy-MM-dd")
        CECls.CEStatus = "0"
        If chkSkip.Checked = False Then
            CECls.SkipBudget = "0"
        Else
            CECls.SkipBudget = "1"
        End If

        CECls.SkipNotes = txtReason.Text
        CECls.RFQSetNumber = cboRFQSetNumber.Text
        CECls.CENotes = txtNotes.Text

        'CECls.CEBudget = txtIABudgetNumber.Text
        'CECls.CEBudget = cbIABudget.Text

        'i = clsCostEstimationDB.UpdateHeader(CECls, pUser, pErr)

        'If i = 0 Then

        Dim CENumberOutput As String = ""
        If txtCENumber.Text = "--NEW--" Then
            clsCostEstimationDB.InsertHeader(CECls, pUser, CENumberOutput, pErr)
            gs_CENumber = CENumberOutput
            CECls.CENumber = CENumberOutput
            vCENumber = gs_CENumber
        Else
            CECls.CENumber = txtCENumber.Text
            clsCostEstimationDB.UpdateHeader(CECls, pUser, pErr)
        End If

        'End If

        If pErr = "" Then
            txtCENumber.Text = gs_CENumber
           
        End If
        ' End If

    End Sub

    Private Sub up_Submit()
        Dim Errmsg As String = ""
        Dim CECls As New clsCostEstimation
        CECls.CENumber = Session("CENumber")
        CECls.Revision = Session("CERev")

        clsCostEstimationDB.SubmitData(CECls, pUser, Errmsg)

        cbSubmit.JSProperties("cpStatus") = "1"
    End Sub
    ' Private Sub up_FillComboBudgetNo()
        ' Dim ds As New DataSet
        ' Dim pErr As String = ""

        ' ds = clsCostEstimationAcceptanceUserDB.GetComboBudgetNo(pErr)
        ' If pErr = "" Then
            ' cbIABudget.DataSource = ds
            ' cbIABudget.DataBind()
        ' End If
    ' End Sub
    Private Sub up_UpdateHeader(pCENo As String, pCEStatus As String, Optional ByRef pErr As String = "")
        Dim Errmsg As String = ""
        Dim CECls As New clsCostEstimation

        CECls.CENumber = pCENo
        CECls.CEStatus = pCEStatus
        If chkSkip.Checked = False Then
            CECls.SkipBudget = "0"
        Else
            CECls.SkipBudget = "1"
        End If
        CECls.RFQSetNumber = cboRFQSetNumber.Text
        CECls.Revision = gs_CERevNo
        CECls.CENotes = txtNotes.Text
        'CECls.CEBudget = cbIABudget.Text
        'CECls.CEBudget = txtIABudgetNumber.Text
        CECls.SkipNotes = txtReason.Text
        CECls.CEDate = Format(dtCEDate.Value, "yyyy-MM-dd")
        'ls_VCENumber = pCENo

        'clsCostEstimationDB.UpdateHeader(CECls, pUser, pErr)
        Dim i As Integer

        i = clsCostEstimationDB.UpdateHeader(CECls, pUser, pErr)
		Dim folderPath As String = Server.MapPath("~/Files/")
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        ds = clsCostEstimationDB.GetCEData(gs_CENumber, gs_CERevNo, Errmsg)
        If ds.Tables(0).Rows.Count > 0 Then
            If chkSkip.Checked = False Then
                If (System.IO.File.Exists(folderPath & ds.Tables(0).Rows(0)("File_Name"))) Then
                    System.IO.File.Delete(folderPath & ds.Tables(0).Rows(0)("File_Name"))
                    clsCostEstimationDB.DeleteFile(gs_CENumber, gs_CERevNo, pUser, Errmsg)
                End If
                
            End If
        End If	  
        'If i = 0 Then
        'clsCostEstimationDB.InsertHeader(CECls, pUser, pErr)
        'End If

    End Sub


    Protected Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        'Grid.CancelEdit()
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Dim pRFQSet As String
        If Split(e.Parameters, "|")(1) <> "" Then
            pRFQSet = Split(e.Parameters, "|")(3)
        End If

        Dim pRev As Integer

        'If txtRev.Text = "" Then pRev = 0
        If Split(e.Parameters, "|")(1) = "" Then
            pRev = 0
        Else
            pRev = Split(e.Parameters, "|")(2)
        End If


        If pfunction = "gridload" Then
            up_GridHeader(pRFQSet, "00")
            up_GridLoad(pRFQSet, "00")
        ElseIf pfunction = "draft" Then
            '    'If gs_CENumber <> "" And gs_CENumber <> "ALL" Then
            up_GridHeader(cboRFQSetNumber.Text, "00")
            up_GridLoadCE(gs_CENumber, pRev)
            'End If
        End If

    End Sub

    Private Sub cbGrid_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbGrid.Callback
        Dim pfunction As String = Split(e.Parameter, "|")(0)
        Dim pRFQSet As String = Split(e.Parameter, "|")(3)
        Dim pRev As Integer

        If Split(e.Parameter, "|")(2) = "" Then pRev = 0

        up_GridLoadCE(gs_CENumber, pRev)

    End Sub

    Private Sub cbDraft_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDraft.Callback
        'up_UploadFile()

        If txtCENumber.Text <> "--NEW--" Then
            up_UpdateHeader(gs_CENumber, "0", "")
        End If
        If gs_Message = "" Then
            cbDraft.JSProperties("cpMessage") = "Draft data saved successfull"
            cbDraft.JSProperties("cpCENo") = Session("CENumber")
            cbDraft.JSProperties("cpStatus") = 0
            'If IsNothing(gs_CERevNo) Then
            cbDraft.JSProperties("cpRevision") = Session("CERev")

            'cbDraft.JSProperties("cpRFQSetNumber") = cboRFQSetNumber.Value
        End If
    End Sub

    'Protected Sub btnDraft_Click(sender As Object, e As EventArgs) Handles btnDraft.Click
    'Dim errmsg As String = ""
    ''up_UploadFile()

    ''Jika draft makan statusnya = 0
    'If gs_CENo <> "--NEW--" Then
    '    up_UpdateHeader(gs_CENumber, "0", errmsg)
    'End If

    'If gs_Message = "" And gs_CENumber <> "" Then
    '    cbDraft.JSProperties("cpMessage") = "Draft data saved successfull"
    '    cbDraft.JSProperties("cpCENo") = gs_CENumber
    '    If IsNothing(gs_CERevNo) Then
    '        cbDraft.JSProperties("cpRevision") = 0
    '    Else
    '        cbDraft.JSProperties("cpRevision") = gs_CERevNo
    '    End If

    '    gs_CENo = gs_CENumber
    '    'up_GridLoadCE(gs_CENumber)
    'Else
    '    cbDraft.JSProperties("cpMessage") = gs_Message
    'End If

    'End Sub

    Private Sub cboRFQSetNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboRFQSetNumber.Callback
        'Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim prno As String = e.Parameter

        up_FillComboRFQ(prno)
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
		'cek exist file document (mandatory upload) 28/02/2019
        Dim ds As New DataSet
        Dim errmsg As String = ""
        ' Dim query As String = "select file_name from ce_header where ce_number ='" & gs_CENumber & "'"
        'Dim cmd As New SqlCommand(
        ds = clsCostEstimationDB.GetCEData(gs_CENumber, gs_CERevNo, errmsg)
        If ds.Tables(0).Rows.Count > 0 Then
            If chkSkip.Checked = True Then
                If ds.Tables(0).Rows(0)("File_Name").ToString = "" Then
                    'cbSubmit.JSProperties("cpMessage") = gs_CENumber & "-" & gs_CERevNo
                    cbSubmit.JSProperties("cpMessage") = "Document Upload is required"
                    Exit Sub
                End If
            End If

        End If
        up_UpdateHeader(gs_CENumber, "1", errmsg)
        up_Submit()
        If errmsg = "" Then
            cbSubmit.JSProperties("cpMessage") = "Data saved successfull"
        End If

    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "PriceEst" Or e.DataColumn.FieldName = "Remarks" Then
            'Editable
            e.Cell.BackColor = Color.White

        Else
            'Non-Editable
            e.Cell.BackColor = Color.LemonChiffon
            e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        End If
    End Sub

    Private Sub up_Print()
        'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/ViewPRAcceptance.aspx")
        Session("CENumber") = gs_CENumber 'txtCENumber.Text
        Session("Revision") = txtRev.Text
        Session("RFQSet") = cboRFQSetNumber.Text
        Session("Action") = "1"
        Response.Redirect("~/ViewCostEstimation.aspx")

    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        up_Print()
    End Sub
	
	Protected Sub btnAddFile_Click(sender As Object, e As EventArgs) Handles btnAddFile.Click
        up_UploadFile()
    End Sub

    'Protected Sub keyFieldLink_Init(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim link As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
    '    'Dim container As GridViewDataItemTemplateContainer = TryCast(link.NamingContainer, GridViewDataItemTemplateContainer)

    '    'If container.ItemIndex >= 0 Then
    '    Dim ds As New DataSet
    '    ds = clsCostEstimationDB.GetCEData(txtCENumber.Text, txtRev.Text)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        link.Text = ds.Tables(0).Rows(0)("File_Name").ToString
    '        link.Target = "_blank"
    '        Dim extension As String = ""
    '        extension = System.IO.Path.GetExtension("~/Files/" & ds.Tables(0).Rows(0)("File_Name"))
    '        If extension <> ".pdf" Then
    '            If ds.Tables(0).Rows(0)("File_Name") <> "" Then
    '                LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
    '            End If
    '        End If
    '    End If

    '    'End If
    'End Sub

	'unused   
    Private Sub ucAtc_FileUploadComplete(sender As Object, e As DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs) Handles ucAtc.FileUploadComplete

        Dim serverPath As String = Server.MapPath("~/Attachments/")
        Dim fileName As String = "CostEstimation" & "_" & Format(CDate(Now), "yyyyMMdd_hhmmss") & ".pdf" 'e.UploadedFile.FileName
        Dim fullPath As String = String.Format("{0}{1}", serverPath, fileName)


        'CHECK EXISTING FOLDER (Attachment)
        If (Not System.IO.Directory.Exists(serverPath)) Then
            System.IO.Directory.CreateDirectory(serverPath)
        End If


        'SAVE ATTACHMENT
        e.UploadedFile.SaveAs(fullPath)

        'ASPxUploadControl1.JSProperties("cpType") = "1"
        'ASPxUploadControl1.JSProperties("cpMessage") = "Cost Estimation " & e.UploadedFile.FileName & " has been uploaded successfully!"

        'Call SaveAttachment(hf.Get("QuotationNo"), hf.Get("RevisionNo"), hf.Get("RFQNumber"), e.UploadedFile.FileName, "~/Attachments/" & fileName)

    End Sub

    'Private Sub cbFile_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbFile.Callback
    'LinkFile.Text = "google.com"
    'End Sub


End Class
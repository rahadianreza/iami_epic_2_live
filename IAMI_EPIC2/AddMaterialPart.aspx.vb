﻿Public Class AddMaterialPart
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
    Dim CategoryMaterial As String = ""
    Dim MaterialCode As String = ""
    Dim period As String = ""
    Dim type As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A040")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        Dim s As String = Request.QueryString("ID")
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            cboPeriod.Value = CDate(Now.ToShortDateString)

            Dim script As String = ""
            If s <> "" Then
                btnSubmit.Visible = False
                btnUpdate.Visible = True
                btnClear.Visible = False
                cboCategoryMaterial.Enabled = False
                'txtMaterialName.Enabled = False
                cboPeriod.Enabled = False
                cboType.Enabled = False
                cboMaterialCode.Enabled = False

                script = "txtMaterialName.SetEnabled(false);" & vbCrLf & _
                         "txtMaterialName.SetEnabled(false);"

                ScriptManager.RegisterStartupScript(txtMaterialName, txtMaterialName.GetType(), "txtMaterialName", script, True)

                CategoryMaterial = s.Split("|")(0)
                MaterialCode = s.Split("|")(1)
                period = s.Split("|")(2)
                type = s.Split("|")(3)

                Dim ds As DataSet = ClsMaterialPartDB.getDetail(CategoryMaterial, MaterialCode, period, type, pUser)
                cboPeriod.Value = ds.Tables(0).Rows(0)("Period")
                cboType.Value = ds.Tables(0).Rows(0)("Type").ToString()
                cboMaterialCode.Value = ds.Tables(0).Rows(0)("Material_Code").ToString()
                txtMaterialName.Text = ds.Tables(0).Rows(0)("Material_Name").ToString()
                txtMaterialNameTemp.Text = ds.Tables(0).Rows(0)("Material_Name").ToString()

                txtSpecification.Text = ds.Tables(0).Rows(0)("Specification").ToString()
                cboUoMUnitPrice.Value = ds.Tables(0).Rows(0)("UoMUnitPrice").ToString()
                txtUnitPrice.Text = ds.Tables(0).Rows(0)("UnitPrice").ToString()
                cboUoMUnitConsump.Value = ds.Tables(0).Rows(0)("UoMUnitConsump").ToString()
                txtUnitConsump.Text = ds.Tables(0).Rows(0)("UnitConsump").ToString()
                cboCategoryMaterial.Value = ds.Tables(0).Rows(0)("Category_Material").ToString()
                cboGroupItem.Value = ds.Tables(0).Rows(0)("GroupItem").ToString()
                cboCurrency.Value = ds.Tables(0).Rows(0)("Currency").ToString()
                cboMaterialCategory.Value = ds.Tables(0).Rows(0)("Category_Material").ToString()
            Else
                btnSubmit.Visible = True
                btnUpdate.Visible = False
                cboCategoryMaterial.Enabled = True
                txtMaterialName.Enabled = True
                'script = "txtMaterialName.SetEnabled(true);" & vbCrLf & _
                '        "txtMaterialName.SetEnabled(true);"

                'ScriptManager.RegisterStartupScript(txtMaterialName, txtMaterialName.GetType(), "txtMaterialName", script, True)

            End If
        End If

    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("MaterialPart.aspx")
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim pErr As String = ""

        If String.IsNullOrEmpty(cboPeriod.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Period"
        ElseIf String.IsNullOrEmpty(cboType.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Type"
        ElseIf String.IsNullOrEmpty(cboCategoryMaterial.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Category Material"
        ElseIf String.IsNullOrEmpty(cboMaterialCode.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Material Code"
        ElseIf String.IsNullOrEmpty(txtSpecification.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Specification"
        ElseIf String.IsNullOrEmpty(cboUoMUnitPrice.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input UoM Unit Price"
        ElseIf String.IsNullOrEmpty(txtUnitPrice.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Unit Price"
        ElseIf String.IsNullOrEmpty(cboUoMUnitConsump.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input UoM Unit Consump"
        ElseIf String.IsNullOrEmpty(txtUnitConsump.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Unit Consump"
        ElseIf String.IsNullOrEmpty(cboCategoryMaterial.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Material Type"
        ElseIf String.IsNullOrEmpty(cboCurrency.Value) Then
            cbSave.JSProperties("cpMessage") = "Currency"
        ElseIf String.IsNullOrEmpty(cboGroupItem.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Group Item"
        ElseIf String.IsNullOrEmpty(cboCategoryMaterial.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Category Material"
        Else
            Dim Period As String = Format(cboPeriod.Value, "yyyy-MM-dd")
            ClsMaterialPartDB.InsertMaterialCode(Period, cboType.Value, cboCategoryMaterial.Value, txtMaterialNameTemp.Text, txtSpecification.Text, _
                                                 cboUoMUnitPrice.Value, cboUoMUnitConsump.Text, txtUnitPrice.Text, txtUnitConsump.Text, cboCurrency.Value, cboCategoryMaterial.Value, _
                                                 cboGroupItem.Value, cboCategoryMaterial.Value, cboMaterialCode.Value, pUser, pErr)
            If pErr = "" Then
                cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
            Else
                cbSave.JSProperties("cpMessage") = pErr
            End If

        End If
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"

    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim pErr As String = ""

        If String.IsNullOrEmpty(cboPeriod.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Period"
        ElseIf String.IsNullOrEmpty(cboType.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Type"
        ElseIf String.IsNullOrEmpty(cboCategoryMaterial.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Category Material"
        ElseIf String.IsNullOrEmpty(cboMaterialCode.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Material Code"
        ElseIf String.IsNullOrEmpty(txtMaterialName.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Material Name"
        ElseIf String.IsNullOrEmpty(txtSpecification.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Specification"
        ElseIf String.IsNullOrEmpty(cboUoMUnitPrice.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input UoM Unit Price"
        ElseIf String.IsNullOrEmpty(txtUnitPrice.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Unit Price"
        ElseIf String.IsNullOrEmpty(cboUoMUnitConsump.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input UoM Unit Consump"
        ElseIf String.IsNullOrEmpty(txtUnitConsump.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Unit Consump"
        ElseIf String.IsNullOrEmpty(cboCategoryMaterial.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Material Type"
        ElseIf String.IsNullOrEmpty(cboCurrency.Value) Then
            cbSave.JSProperties("cpMessage") = "Currency"
        ElseIf String.IsNullOrEmpty(cboGroupItem.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Group Item"
        ElseIf String.IsNullOrEmpty(cboCategoryMaterial.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Category Material"
        Else
            Dim Period As String = Format(cboPeriod.Value, "yyyy-MM-dd")

            ClsMaterialPartDB.UpdateMaterialCode(Period, cboType.Value, cboCategoryMaterial.Value, txtMaterialNameTemp.Text, txtSpecification.Text, _
                                                 cboUoMUnitPrice.Value, cboUoMUnitConsump.Text, txtUnitPrice.Text, txtUnitConsump.Text, cboCurrency.Value, cboCategoryMaterial.Value, _
                                                  cboGroupItem.Value, cboCategoryMaterial.Value, cboMaterialCode.Value, pUser, pErr)
            If pErr = "" Then
                cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
            Else
                cbSave.JSProperties("cpMessage") = pErr
            End If

        End If
    End Sub

    Private Sub cboMaterialCode_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboMaterialCode.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(1)

        cboMaterialCode.DataSource = ClsMaterialPartDB.GetMaterialCode(pFunction, pUser)
        cboMaterialCode.DataBind()
    End Sub

    Private Sub cbDescriptions_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDescriptions.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(1)

        Dim aDescs As String = ClsMaterialPartDB.GetDescriptionMatrialCode(pFunction, pUser)
        cbDescriptions.JSProperties("cp_Descs") = aDescs
    End Sub
End Class
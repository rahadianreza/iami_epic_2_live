﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RFQUpdateDetail.aspx.vb" Inherits="IAMI_EPIC2.RFQUpdateDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

    function OnBatchEditStartEditing(s, e) {
        currentColumnName = e.focusedColumn.fieldName;

        if (currentColumnName != "AllowCheck") {
            e.cancel = true;
        }

        currentEditableVisibleIndex = e.visibleIndex;
    }

    function GetMessage(s, e) {
        //alert(s.cpMessage);

        if (s.cpMessage == 'Draft data saved successfull') {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            ASPxLabelRFQNo1.SetText(s.cpRFQNo1);
            ASPxLabelRFQNo2.SetText(s.cpRFQNo2);
            ASPxLabelRFQNo3.SetText(s.cpRFQNo3);
            ASPxLabelRFQNo4.SetText(s.cpRFQNo4);
            ASPxLabelRFQNo5.SetText(s.cpRFQNo5);
            //alert(s.cpRFQNo1);
            //alert(s.cpRFQNo2);

        }
        else if (s.cpMessage == 'Data saved successfull') {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

			//01-04-2019
            ASPxLabelRFQNo1.SetText(s.cpRFQNo1);
            ASPxLabelRFQNo2.SetText(s.cpRFQNo2);
            ASPxLabelRFQNo3.SetText(s.cpRFQNo3);
            ASPxLabelRFQNo4.SetText(s.cpRFQNo4);
            ASPxLabelRFQNo5.SetText(s.cpRFQNo5);

            btnDraft.SetEnabled(false);
           
            txtRFQTitle.SetEnabled(false);
            dtDateline.SetEnabled(false);
            cboCurrency.SetEnabled(false);
           
            Grid.SetEnabled(false);
			
            //millisecondsToWait = 1500;
            //setTimeout(function () {
            //    var pathArray = window.location.pathname.split('/');
            //    var url = window.location.origin + '/' + pathArray[1] + '/RFQList.aspx';
            //    if (pathArray[1] == "RFQDetail.aspx") {
            //        window.location.href = window.location.origin + '/RFQList.aspx';
            //    }
            //    else {
            //        window.location.href = url;
            //    }
            //}, millisecondsToWait);


//            millisecondsToWait = 2000;
//            setTimeout(function () {
//                window.location.href = "/RFQList.aspx";
//            }, millisecondsToWait);
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
    }

	//21-02-2019
    function validation() {
//        if (txtRFQTitle.GetText() == '') {
//            toastr.warning('Please Input RFQ Title!', 'Warning');
//            txtRFQTitle.Focus();
//            toastr.options.closeButton = false;
//            toastr.options.debug = false;
//            toastr.options.newestOnTop = false;
//            toastr.options.progressBar = false;
//            toastr.options.preventDuplicates = true;
//            toastr.options.onclick = null;
//            e.processOnServer = false;
//            return;
//        }
        if (cboSupplier1.GetSelectedIndex() == -1 && cboSupplier2.GetSelectedIndex() == -1 && cboSupplier3.GetSelectedIndex() == -1 && cboSupplier4.GetSelectedIndex() == -1 && cboSupplier5.GetSelectedIndex() == -1) {
            toastr.warning('Please Select Supplier !', 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return;
        }
        else if (cboSupplier1.GetSelectedIndex() == -1) {
            toastr.warning('Please Select Supplier 1!', 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return;
        }
        else if (cboSupplier5.GetText() != "") {
            if (cboSupplier5.GetSelectedIndex() == -1) {
                toastr.warning('Please Input Valid Supplier 5!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier4.GetSelectedIndex() == -1) {
                toastr.warning('Please Input Valid Supplier 4!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier3.GetSelectedIndex() == -1) {
                toastr.warning('Please Input Valid Supplier 3!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier2.GetSelectedIndex() == -1) {
                toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier5.GetText() == cboSupplier1.GetText() || cboSupplier5.GetText() == cboSupplier2.GetText() || cboSupplier5.GetText() == cboSupplier3.GetText() || cboSupplier5.GetText() == cboSupplier4.GetText()) {
                toastr.warning('Duplicate Supplier!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier4.GetText() == cboSupplier1.GetText() || cboSupplier4.GetText() == cboSupplier2.GetText() || cboSupplier4.GetText() == cboSupplier3.GetText()) {
                toastr.warning('Duplicate Supplier!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier3.GetText() == cboSupplier1.GetText() || cboSupplier3.GetText() == cboSupplier2.GetText()) {
                toastr.warning('Duplicate Supplier!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                toastr.warning('Duplicate Supplier!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }
        else if (cboSupplier4.GetText() != "") {
            if (cboSupplier4.GetSelectedIndex() == -1) {
                toastr.warning('Please Input Valid Supplier 4!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier3.GetSelectedIndex() == -1) {
                toastr.warning('Please Input Valid Supplier 3!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier2.GetSelectedIndex() == -1) {
                toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier4.GetText() == cboSupplier1.GetText() || cboSupplier4.GetText() == cboSupplier2.GetText() || cboSupplier4.GetText() == cboSupplier3.GetText()) {
                toastr.warning('Duplicate Supplier!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier3.GetText() == cboSupplier1.GetText() || cboSupplier3.GetText() == cboSupplier2.GetText()) {
                toastr.warning('Duplicate Supplier!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                toastr.warning('Duplicate Supplier!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }
        else if (cboSupplier3.GetText() != "") {
            if (cboSupplier3.GetSelectedIndex() == -1) {
                toastr.warning('Please Input Valid Supplier 3!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier2.GetSelectedIndex() == -1) {
                toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier3.GetText() == cboSupplier1.GetText() || cboSupplier3.GetText() == cboSupplier2.GetText()) {
                toastr.warning('Duplicate Supplier!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                toastr.warning('Duplicate Supplier!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

        else if (cboSupplier2.GetText() != "") {
            if (cboSupplier2.GetSelectedIndex() == -1) {
                toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                toastr.warning('Duplicate Supplier!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }
               
        if (cboCurrency.GetSelectedIndex() == -1) {
            toastr.warning('Please Select Currency!', 'Warning');
            cboCurrency.Focus();
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return;
        }
        if (ASPxLabelRFQNo5.GetText() != "") {
            if (cboSupplier5.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Supplier 5!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

        if (ASPxLabelRFQNo4.GetText() != "") {
            if (cboSupplier4.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Supplier 4!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

        if (ASPxLabelRFQNo3.GetText() != "") {
            if (cboSupplier3.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Supplier 3!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

        if (ASPxLabelRFQNo2.GetText() != "") {
            if (cboSupplier2.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Supplier 2!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

        if (ASPxLabelRFQNo1.GetText() != "") {
            if (cboSupplier1.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Supplier 1!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

    }

	//21-02-2019
    function DraftProcess() {
		//alert(cboSupplier5.GetSelectedIndex());
        //alert(ASPxLabelRFQNo5.GetText());
        validation();

        startIndex = 0;

        var check = "";
        var x = false;

        for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
            check = parseFloat(Grid.batchEditApi.GetCellValue(i, "AllowCheck"));

            if (check == "1") {
                x = true;
            }
        }

        if (x == true) {
			var msg = confirm('Are you sure want to update this data ?');
          if (msg == false) {
              e.processOnServer = false;
              return;
          }
            cbDraft.PerformCallback();
            millisecondsToWait = 1500;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/RFQUpdate.aspx';
                if (pathArray[1] == "RFQUpdateDetail.aspx") {
                    window.location.href = window.location.origin + '/RFQUpdate.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);
        }
        else {
            toastr.warning('Please select material number', 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
        }
    }

	
    function CheckedChanged(s, e) {

        //Grid.SetFocusedRowIndex(-1);
        if (s.GetValue() == -1) s.SetValue(1);

        for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
            if (Grid.batchEditApi.GetCellValue(i, "AllowCheck", false) != s.GetValue()) {
                Grid.batchEditApi.SetCellValue(i, "AllowCheck", s.GetValue());
            }
        }
    }




</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
            <tr style="height: 20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
            <tr style="height: 40px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="RFQ Set Number">
                    </dx1:aspxlabel>                 
                </td>
                <td></td>
                <td style="width:200px">
           
            <dx1:ASPxTextBox ID="txtRFQSetNumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="200px" ClientInstanceName="txtRFQSetNumber" MaxLength="20" 
                        Height="30px" BackColor="#CCCCCC" ReadOnly="True" Font-Bold="True" >
            </dx1:ASPxTextBox>
           
                    </td>
                <td>
                    <dx1:ASPxTextBox ID="txtRev" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="50px" ClientInstanceName="txtRev" MaxLength="20" 
                                Height="25px" BackColor="Silver" ReadOnly="True" Font-Bold="True" 
                                ForeColor="Black" HorizontalAlign="Center" >
     
                    </dx1:ASPxTextBox>           
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Supplier 1">
                    </dx1:aspxlabel>                 
                    </td>
                <td class="style2">
           
        <dx1:ASPxComboBox ID="cboSupplier1" runat="server" ClientInstanceName="cboSupplier1"
                            Width="200px" Font-Names="Segoe UI"  TextField="Description" DataSourceID="SqlDataSource1"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="200px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                    </td>
                <td class="style2">
                    <dx1:aspxlabel ID="ASPxLabelRFQNo1" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Font-Bold="True" ClientInstanceName="ASPxLabelRFQNo1">
                    </dx1:aspxlabel>                 
                    </td>
                <td class="style2">
           
            <dx1:ASPxTextBox ID="txtRFQNo1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="100px" ClientInstanceName="txtRFQNo1" MaxLength="20" 
                        Height="30px" BackColor="#CCCCCC" Visible="False" Font-Bold="True" >
            </dx1:ASPxTextBox>
           
                    </td>
            </tr>  
            <tr style="height: 25px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="RFQ Date">
                    </dx1:aspxlabel>                 
                </td>
                <td></td>
                <td>
                <dx:ASPxDateEdit ID="dtDRFQDate" runat="server" Theme="Office2010Black" 
                                        Width="120px" AutoPostBack="false" ClientInstanceName="dtDRFQDate"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        BackColor="#CCCCCC" ReadOnly="True">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                </td>
                <td></td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Supplier 2">
                    </dx1:aspxlabel>                 
                    </td>
                <td>
           
        <dx1:ASPxComboBox ID="cboSupplier2" runat="server" ClientInstanceName="cboSupplier2"
                            Width="200px" Font-Names="Segoe UI"  TextField="Description" DataSourceID="SqlDataSource1"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="200px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabelRFQNo2" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Font-Bold="True" ClientInstanceName="ASPxLabelRFQNo2">
                    </dx1:aspxlabel>                 
                    </td>
                <td>
           
            <dx1:ASPxTextBox ID="txtRFQNo2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="100px" ClientInstanceName="txtRFQNo2" MaxLength="20" 
                        Height="30px" BackColor="#CCCCCC" Visible="False" Font-Bold="True" >
            </dx1:ASPxTextBox>
           
                    </td>
            </tr>  
            <tr style="height: 40px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Number">
                    </dx1:aspxlabel>                 
                </td>
                <td></td>
                <td>
           
            <dx1:ASPxTextBox ID="txtPRNumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="170px" ClientInstanceName="txtPRNumber" MaxLength="30" 
                        Height="28px" BackColor="#CCCCCC" ReadOnly="True" >
            </dx1:ASPxTextBox>
           
                    </td>
                <td></td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Supplier 3">
                    </dx1:aspxlabel>                 
                    </td>
                <td>
           
        <dx1:ASPxComboBox ID="cboSupplier3" runat="server" ClientInstanceName="cboSupplier3"
                            Width="200px" Font-Names="Segoe UI"  TextField="Description"  DataSourceID="SqlDataSource1"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="200px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabelRFQNo3" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Font-Bold="True" ClientInstanceName="ASPxLabelRFQNo3">
                    </dx1:aspxlabel>                 
                    </td>
                <td>
           
            <dx1:ASPxTextBox ID="txtRFQNo3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="100px" ClientInstanceName="txtRFQNo3" MaxLength="20" 
                        Height="30px" BackColor="#CCCCCC" Visible="False" Font-Bold="True" >
            </dx1:ASPxTextBox>
           
                    </td>
            </tr>  
            <tr style="height: 25px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Deadline Date">
                    </dx1:aspxlabel>                 
                </td>
                <td></td>
                <td>
           
                <dx:ASPxDateEdit ID="dtDateline" runat="server" Theme="Office2010Black" 
                                        Width="120px" AutoPostBack="false" ClientInstanceName="dtDateline"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                    </td>
                <td></td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Supplier 4">
                    </dx1:aspxlabel>                 
                    </td>
                <td>
           
        <dx1:ASPxComboBox ID="cboSupplier4" runat="server" ClientInstanceName="cboSupplier4"
                            Width="200px" Font-Names="Segoe UI"  TextField="Description" DataSourceID="SqlDataSource1"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="200px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabelRFQNo4" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Font-Bold="True" ClientInstanceName="ASPxLabelRFQNo4">
                    </dx1:aspxlabel>                 
                    </td>
                <td>
           
            <dx1:ASPxTextBox ID="txtRFQNo4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="100px" ClientInstanceName="txtRFQNo4" MaxLength="20" 
                        Height="30px" BackColor="#CCCCCC" Visible="False" Font-Bold="True" >
            </dx1:ASPxTextBox>
           
                    </td>
            </tr>  
            <tr style="height: 35px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel12" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="RFQ Title">
                    </dx1:aspxlabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
            <dx1:ASPxTextBox ID="txtRFQTitle" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="250px" ClientInstanceName="txtRFQTitle" MaxLength="30" 
                        Height="25px" >
            </dx1:ASPxTextBox>
           
                    </td>
                <td>&nbsp;</td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Supplier 5">
                    </dx1:aspxlabel>                 
                    </td>
                <td>
           
        <dx1:ASPxComboBox ID="cboSupplier5" runat="server" ClientInstanceName="cboSupplier5"
                            Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource1"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="200px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabelRFQNo5" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Font-Bold="True" ClientInstanceName="ASPxLabelRFQNo5">
                    </dx1:aspxlabel>                 
                    </td>
                <td>
           
            <dx1:ASPxTextBox ID="txtRFQNo5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="100px" ClientInstanceName="txtRFQNo5" MaxLength="20" 
                        Height="30px" BackColor="#CCCCCC" Visible="False" Font-Bold="True" >
            </dx1:ASPxTextBox>
           
                    </td>
            </tr>  
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Currency">
                    </dx1:aspxlabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
        <dx1:ASPxComboBox ID="cboCurrency" runat="server" ClientInstanceName="cboCurrency"
                            Width="80px" Font-Names="Segoe UI"  TextField="Description" DataSourceID="dsCurrency"
                            ValueField="Code" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            

                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="80px" />
       
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
           
                    </td>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
           
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
           
                    &nbsp;</td>
            </tr>  
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px"></td>
                <td></td>
                <td>
                    <dx:ASPxCallback ID="cbSimpan" runat="server" ClientInstanceName="cbSimpan">
                        <ClientSideEvents EndCallback="GetMessage" />
                    </dx:ASPxCallback>
                </td>
                <td>              
                     &nbsp;</td>
                <td></td>
                <td>
           
            <dx1:ASPxTextBox ID="txtStatus" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="100px" ClientInstanceName="txtStatus" MaxLength="20" 
                        Height="30px" BackColor="#CCCCCC" Visible="False" Font-Bold="True" 
                        Text="1" EnableClientSideAPI="True" >
            </dx1:ASPxTextBox>
           
                    </td>
                <td>              
                     <dx:ASPxCallback ID="cbDraft" runat="server" ClientInstanceName="cbDraft">
                         <ClientSideEvents EndCallback="GetMessage" />
                                                        
                </dx:ASPxCallback>
                    </td>
                <td>              
                     &nbsp;</td>
            </tr>  
        </table>
    </div>
   <div style="padding: 5px 5px 5px 5px">
          <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Material_No" >
              <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing" />
              <Columns>
                  <dx:GridViewDataTextColumn Caption="Material No" VisibleIndex="1" Width="120px" 
                      FieldName="Material_No">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="2" Width="300px" 
                      FieldName="Description">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="Specification" VisibleIndex="3" 
                      Width="350px" FieldName="Specification">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="Qty" VisibleIndex="4" Width="80px" 
                      FieldName="Qty">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="UOM" VisibleIndex="5" Width="70px" 
                      FieldName="UOM">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataCheckColumn Caption=" " VisibleIndex="0" Width="35px" 
                      FieldName="AllowCheck" Name="AllowCheck">

                       <PropertiesCheckEdit ValueChecked="1" ValueType="System.String" AllowGrayedByClick="false" ValueUnchecked="0" >
                       </PropertiesCheckEdit>

                        <HeaderCaptionTemplate>
                        <dx:ASPxCheckBox ID="checkBox" runat="server" ClientInstanceName="checkBox" ClientSideEvents-CheckedChanged="CheckedChanged"  ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="" Font-Names="Segoe UI" Font-Size="8pt" ForeColor="White" > 
                        </dx:ASPxCheckBox>
                        </HeaderCaptionTemplate> 

                  </dx:GridViewDataCheckColumn>
              </Columns>
                    <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                    </SettingsEditing>
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="160" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>
             </dx:ASPxGridView>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 0px; height: 100px;">  
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; " colspan="8">
                    <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnBack" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {
	window.history.back();
}" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>   
                    
                    &nbsp;<dx:ASPxButton ID="btnDraft" runat="server" Text="Update RFQ" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDraft" Theme="Default">                        
                        <ClientSideEvents Click="DraftProcess" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>                  
                    &nbsp;&nbsp;</td>
            </tr>  
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select Supplier_Code As Code, Supplier_Name As Description From Mst_Supplier">
                    </asp:SqlDataSource>
                    </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>  
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <asp:SqlDataSource ID="dsCurrency" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
                        SelectCommand="SELECT Code = RTRIM(Par_Code) FROM Mst_Parameter WHERE Par_Group = 'Currency'">
                    </asp:SqlDataSource>
                    </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>  
        </table>
    </div>
</div>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="QuotationSupplier_SupplierFinalSubmit.aspx.vb" Inherits="IAMI_EPIC2.QuotationSupplier_SupplierFinalSubmit" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };

         function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtItemCode.Focus();
            cboGroupItem.SetText('');
            cboCategory.SetText('');
            cboPRType.SetText('');
            txtSAPNumber.SetText('');
        }
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/ItemList.aspx";

            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/ItemList.aspx';
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                if (pathArray[1] == "AddItemMaster.aspx") {
                    window.location.href = window.location.origin + '/ItemList.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtSAPNumber.SetText('');
            txtItemCode.Focus();

        }
        else if (s.cpMessage == "Data Completed!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtItemCode.Focus();
            cboGroupItem.SetText('');
            cboCategory.SetText('');
            cboPRType.SetText('');
            txtSAPNumber.SetText('');
        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    };

         function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "Part_No" || currentColumnName == "Commodity" || currentColumnName == "Upc") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        };  

        function SaveData(s, e) {
//            cbkValid.PerformCallback('save');
            Grid.UpdateEdit()
        }

        function disableenablebuttonsubmit(_val) {
        alert('adsds');
            if (_val = '1') {
                btnSave.SetEnabled(false)
            } else {
                btnSave.SetEnabled(true)
             }
        }

          function OnUpdateCheckedChanged(s, e) {
            if (s.GetValue() == -1) s.SetValue(1);
            Grid.SetFocusedRowIndex(-1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "Status_Drawing", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "Status_Drawing", s.GetValue());
                }                
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="border: 1px solid black">
        <table>
            <tr>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Project ID">
                    </dx:ASPxLabel>
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                        Font-Names="Segoe UI" TextField="Project_Name" ValueField="Project_ID" TextFormatString="{1}"
                        Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                        EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                cboGroup.PerformCallback('filter|' + cboProject.GetSelectedItem().GetColumnText(0));
                }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                            <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td style="width: 50px">
                </td>
                <td style="padding: 10px 0px 0px 30px; width">
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Commodity">
                    </dx:ASPxLabel>
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxComboBox ID="cboCommodity" runat="server" ClientInstanceName="cboCommodity"
                        Font-Names="Segoe UI" TextField="Commodity" ValueField="Commodity" TextFormatString="{0}"
                        Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                        EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                        cboGroupCommodity.PerformCallback('filter|' + cboCommodity.GetSelectedItem().GetColumnText(0));
	                        
                        }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="Commodity" FieldName="Commodity" Width="100px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Group ID">
                    </dx:ASPxLabel>
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxComboBox ID="cboGroup" runat="server" ClientInstanceName="cboGroup" Width="120px"
                        TabIndex="4" Font-Names="Segoe UI" TextField="Group_ID" ValueField="Group_ID"
                        TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                cboCommodity.PerformCallback('filter|' + cboGroup.GetSelectedItem().GetColumnText(0));
                    }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="Group ID" FieldName="Group_ID" Width="100px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td>
                </td>
                <td style="padding: 10px 0px 0px 10px" class="style1">
                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Sourcing Group">
                    </dx:ASPxLabel>
                </td>
                <td style="width: 20px">
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px">
                    <%--Lima--%>
                    <dx:ASPxComboBox ID="cboGroupCommodity" runat="server" ClientInstanceName="cboGroupCommodity"
                        Font-Names="Segoe UI" TextField="Group_Comodity" ValueField="Group_Comodity"
                        TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px"
                        TabIndex="5">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                    cbComplete.PerformCallback('');
                            cbStatus.PerformCallback('');
                            }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="Sourcing Group" FieldName="Group_Comodity" Width="100px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table style="padding: 0 10px 0 5px;">
            <tr>
                <td style="padding: 0 20px 0 10px; font-size: 9pt; font-family: Segoe UI;">
                    Trans No
                </td>
                <td style="font-size: 9pt; font-family: Segoe UI;">
                    :
                </td>
                <td style="font-size: 9pt; font-family: Segoe UI; padding: 0 20px 0 10px;">
                    <dx:ASPxLabel ID="lblTransNo" ClientInstanceName="lblTransNo" runat="server" Text="">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr style="height: 50px">
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">
                        <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnComplete" runat="server" Text="Complete" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnComplete" OnClick="btnComplete_Click" Theme="Default">
                        <Paddings Padding="2px" />
                        <ClientSideEvents Click="SaveData" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
            Font-Size="9pt" KeyFieldName="Part_No;">
            <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Part No" VisibleIndex="1" FieldName="Part_No"
                    Width="150px">
                    <EditFormSettings Visible="false" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Part Name" FieldName="Part_Name" VisibleIndex="2"
                    Width="150px">
                    <EditFormSettings Visible="false" />
                    <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                <%--<dx:GridViewBandColumn Caption="Quotation" HeaderStyle-HorizontalAlign="Center">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Part Cost" VisibleIndex="3" FieldName="PartCost"
                            Width="120px">
                            <PropertiesTextEdit DisplayFormatString="#,###">
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="False" />
                            <Settings AllowAutoFilter="true" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Tooling Cost" VisibleIndex="3" FieldName="ToolingCost"
                            Width="120px">
                            <PropertiesTextEdit DisplayFormatString="#,###">
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="False" />
                            <Settings AllowAutoFilter="true" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>--%>
                <dx:GridViewBandColumn Caption="Cost Target" HeaderStyle-HorizontalAlign="Center">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Part Cost" VisibleIndex="3" FieldName="TargetCostPart"
                            Width="120px">
                            <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="false" />
                            <Settings AllowAutoFilter="true" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Tooling Cost" VisibleIndex="3" FieldName="TargetToolingCost"
                            Width="120px">
                            <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="false" />
                            <Settings AllowAutoFilter="true" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="Final Price" HeaderStyle-HorizontalAlign="Center">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Part Cost" VisibleIndex="3" FieldName="FinalCostPricePart"
                            Width="120px">
                            <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="true" />
                            <Settings AllowAutoFilter="true" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Tooling Cost" VisibleIndex="3" FieldName="FinalToolingCost"
                            Width="120px">
                            <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="true" />
                            <Settings AllowAutoFilter="true" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="6" FieldName="Register_By">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="6" FieldName="Register_Date">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="6" FieldName="Update_By">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="6" FieldName="Update_Date">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
            <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="320" />
            </SettingsPopup>
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header>
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
                <CommandColumnItem ForeColor="Orange">
                </CommandColumnItem>
            </Styles>
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
            <Templates>
                <EditForm>
                    <div style="padding: 15px 15px 15px 15px">
                        <dx:ContentControl ID="ContentControl1" runat="server">
                            <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                runat="server"></dx:ASPxGridViewTemplateReplacement>
                        </dx:ContentControl>
                    </div>
                    <div style="text-align: left; padding: 5px 5px 5px 15px">
                        <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                        <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                    </div>
                </EditForm>
            </Templates>
        </dx:ASPxGridView>
        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbkDrawingComplete" runat="server" ClientInstanceName="cbkDrawingComplete">
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbComplete" runat="server" ClientInstanceName="cbComplete">
            <ClientSideEvents EndCallback="function(s,e) {
        if (s.cp_statusComplete == 'true')
        {
            btnComplete.SetEnabled(true);
        }
        else
        {
            btnComplete.SetEnabled(false);
        }
           
        lblTransNo.SetText(s.cp_TransNo);
      }
       " />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
            <ClientSideEvents Init="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbStatus" runat="server" ClientInstanceName="cbStatus">
            <ClientSideEvents EndCallback="function(s,e) {
       
             lblRemarksStatus.SetValue(s.cp_RemarksData)
      }
       " />
        </dx:ASPxCallback>
    </div>
    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
    </dx:ASPxGridViewExporter>
</asp:Content>

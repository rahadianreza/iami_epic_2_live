﻿Imports System
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO

Public Class CPUpdate
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""
    Dim SupplierCode As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"
    Private Sub FillComboCPNumber()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
        dtFrom = Format(dtCPFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtCPTo.Value, "yyyy-MM-dd")

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_FillCombo", "Combo|StartDate|EndDate", "CP|" & dtFrom & "|" & dtTo, ErrMsg)

        If ErrMsg = "" Then
            cboCPNumber.DataSource = ds.Tables(0)
            cboCPNumber.DataBind()

            If cboCPNumber.Items.Count > 0 Then
                cboCPNumber.SelectedIndex = 0
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboCPNumber)
        End If
    End Sub

    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01", ls_CPNumber As String = "", ls_SupplierCode As String = ""
        dtFrom = Format(dtCPFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtCPTo.Value, "yyyy-MM-dd")
        ls_CPNumber = cboCPNumber.Text

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Update_List", "StartDate|EndDate|CPNumber|SupplierCode", dtFrom & "|" & dtTo & "|" & ls_CPNumber & "|" & SupplierCode, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub ExcelGrid()
        Try
            Call GridLoad()

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=CounterProposalUpdateTender_" & Format(CDate(Now), "yyyyMMdd_hhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            gs_Message = ex.Message
        End Try
    End Sub
#End Region

#Region "FUNCTION"
    Private Function GetSupplierCodeByUserLogin() As String
        Dim retVal As String = ""
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "SELECT Supplier_Code = ISNULL(Supplier_Code,'') FROM UserSetup WHERE UserID = '" & Session("user") & "'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = ds.Tables(0).Rows(0).Item("Supplier_Code")
                Else
                    'NOT EXISTS
                    retVal = ""
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
                Return retVal
            End If
        End Using

        Return retVal
    End Function
#End Region

#Region "EVENTS"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        Dim dfrom As Date
        dfrom = Format(CDate(Now), "yyyy-MM-dd") 'Year(Now) & "-" & Month(Now) & "-" & Day(Now)
        dtCPFrom.Value = dfrom
        dtCPTo.Value = Now
        SupplierCode = GetSupplierCodeByUserLogin()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("E030")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E030")
        Dim dfrom As Date

        gs_Message = ""

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dtCPFrom.Value = dfrom
            dtCPTo.Value = Now
            SupplierCode = GetSupplierCodeByUserLogin()

            If SupplierCode = "" Then
                btnShow.Enabled = False
                btnExcel.Enabled = False
            Else
                btnShow.Enabled = True
                btnExcel.Enabled = True
            End If

            'Set last value because trigger from btnBack in CPUpdateDetail.aspx
            If IsNothing(Session("btnBack_CPUpdateDetail")) = False Then
                dtCPFrom.Value = CDate(Split(Session("btnBack_CPUpdateDetail"), "|")(0))
                dtCPTo.Value = CDate(Split(Session("btnBack_CPUpdateDetail"), "|")(1))
                cboCPNumber.Text = Split(Session("btnBack_CPUpdateDetail"), "|")(2)

                Call GridLoad()

                'Grid.MakeRowVisible(Split(Session("btnBack_CPListDetail"), "|")(5))
            End If
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Call GridLoad()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try
    End Sub

    Private Sub Grid_CustomButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonEventArgs) Handles Grid.CustomButtonInitialize
        If e.Column.Grid.GetRowValues(e.VisibleIndex, "CP_Number") = "" Then
            e.Visible = DevExpress.Utils.DefaultBoolean.False
        Else
            e.Visible = DevExpress.Utils.DefaultBoolean.True
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Call GridLoad()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        'Non-Editable
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Private Sub Grid_HtmlRowPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles Grid.HtmlRowPrepared
        If IsNothing(e.GetValue("CP_Number")) = False Then
            If e.GetValue("CP_Number") = "" Then
                e.Row.BackColor = Color.LemonChiffon
            End If
        End If
    End Sub

    Private Sub cboCPNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCPNumber.Callback
        Try
            Call FillComboCPNumber()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboCPNumber)
        End Try
    End Sub

    Private Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Dim ErrMsg As String = ""
        Call ExcelGrid()
    End Sub
#End Region

End Class
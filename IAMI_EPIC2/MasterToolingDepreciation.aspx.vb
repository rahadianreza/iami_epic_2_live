﻿Imports DevExpress.Web.ASPxGridView
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Data.SqlClient

Public Class MasterToolingDepreciation
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim py As String = ""
    Dim grpCommodity As String = ""
    Dim byr As String = ""
    Dim pagetype As String = ""
#End Region

#Region "Initilization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A110")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub up_Excel()
        up_GridLoad("0")
        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})
        compositeLink.PrintingSystem.ExportOptions.Xlsx.SheetName = "a,b"
        compositeLink.CreateDocument()

        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Transfer-Encoding", "binary")
            Response.AppendHeader("Content-Disposition", "attachment; filename=MasterToolingDepreciation" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            fRefresh = True
        Else
            up_GridLoad("1")
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = cls_MasterToolingDB.getGridDepreciation(cboPartName.Value)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub

    Protected Sub Grid_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True

    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
    End Sub
#End Region

#Region "Event Control"

    Private Sub Grid_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If Not Grid.IsNewRowEditing Then
            If e.Column.FieldName = "Part_No" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

#End Region
End Class
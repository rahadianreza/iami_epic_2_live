﻿
Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors

Public Class CEApprovalDetail
    Inherits System.Web.UI.Page

    Dim pUser As String = ""

    Dim vCENumber As String
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|D020", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnApprove.SetEnabled(false);" & vbCrLf & _
                         "btnReject.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboRFQSetNumber)
            End Try
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("D020")
        ' Master.SiteTitle = sGlobal.menuName
        Master.SiteTitle = "COST ESTIMATION APPROVAL DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "D020")
        Dim CENumber As String = ""
        Dim Revision As Integer
        Dim RFQSet As String = ""
        Dim ds As New DataSet

        If Request.QueryString("ID") <> "" Then
            CENumber = Split(Request.QueryString("ID"), "|")(0)
            Revision = Split(Request.QueryString("ID"), "|")(1)
            RFQSet = Split(Request.QueryString("ID"), "|")(2)
        End If



        If Not Page.IsPostBack Then
            Dim ErrMsg As String = ""
            ds = clsCostEstimationDB.CheckCEApproval(CENumber, Revision, pUser, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim script As String = ""
                    Dim script2 As String = ""
                    script = "btnApprove.SetEnabled(false);" & vbCrLf & _
                             "btnReject.SetEnabled(false);"
                    ScriptManager.RegisterStartupScript(txtApproveNote, txtApproveNote.GetType(), "txtApproveNote", "txtApproveNote.SetEnabled(false);", True)
                    ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", script, True)
                End If
            End If

            If CENumber <> "" Then
                gs_CENumber = CENumber
                txtCENumber.Text = CENumber
                up_GridHeader(RFQSet, "00")
                up_GridLoadCE(CENumber, Revision)
				GridLoadAttachment(RFQSet)
                up_FillComboBudgetNo()
                cbIABudget.SelectedIndex = -1
            End If
            'up_GridLoad(CENumber)
        End If

        'check if user approval position job : user(02) or kadept(08)
        Dim ds2 As New DataSet
        Dim pErr As String = ""
        ds2 = clsCostEstimationDB.GetJobPosition(CENumber, Revision, pUser, pErr)
        If pErr = "" Then
            If ds2.Tables(0).Rows.Count > 0 Then
                Dim script3 As String = ""
                script3 = "btnReject.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnReject, btnReject.GetType(), "btnReject", script3, True)
            End If
        End If


        'cbDraft.JSProperties("cpMessage") = ""
    End Sub

    Private Sub up_GridHeader(pRFQSetNo As String, pSupplier As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        For i = 0 To 4
            Grid.Columns.Item(5 + i).Visible = False
        Next

        ds = clsCostEstimationDB.GetDataSupplier(pRFQSetNo, pSupplier, pErr)
        If pErr = "" Then

            For i = 0 To ds.Tables(0).Rows.Count - 1
                Grid.Columns.Item(5 + i).Caption = ds.Tables(0).Rows(i)("Description")
                Grid.Columns.Item(5 + i).Visible = True
            Next

        End If
    End Sub
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_Print()
        'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/ViewPRAcceptance.aspx")
        Session("CENumber") = gs_CENumber 'txtCENumber.Text
        Session("Revision") = txtRev.Text
        Session("RFQSet") = cboRFQSetNumber.Text
        Session("Action") = "2"
        Response.Redirect("~/ViewCostEstimation.aspx")

    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        up_Print()
    End Sub
    Private Sub up_FillComboBudgetNo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsCostEstimationAcceptanceUserDB.GetComboBudgetNo(pErr)
        If pErr = "" Then
            cbIABudget.DataSource = ds
            cbIABudget.DataBind()
        End If
    End Sub

    Private Sub up_GridLoadCE(CENumber As String, Revision As Integer)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim vSupplier As String = ""
        Dim headSupplier As String = ""
        Dim vname As String = "Supplier"
        Dim vfield As String = "S"
        Dim a As Integer

        ds = clsCostEstimationDB.GetCEApprovalData(CENumber, Revision, pUser, ErrMsg)

        If ds.Tables(0).Rows.Count > 0 Then
            dtCEDate.Value = ds.Tables(0).Rows(0)("CE_Date")
            cboPRNo.Text = ds.Tables(0).Rows(0)("PR_Number")
            cboRFQSetNumber.Text = ds.Tables(0).Rows(0)("RFQ_Set")
            txtRev.Text = Revision

            'If ds.Tables(0).Rows(0)("CE_Status") = "2" Or ds.Tables(0).Rows(0)("CE_Status") = "4" Then
            '    btnApprove.Enabled = False
            '    btnReject.Enabled = False
            'End If

            cbApprove.JSProperties("cpStatus") = ds.Tables(0).Rows(0)("CE_Status")
            gs_CERevNo = ds.Tables(0).Rows(0)("Revision_No")
            txtNotes.Text = ds.Tables(0).Rows(0)("CE_Notes")
            cbIABudget.Text = ds.Tables(0).Rows(0)("CE_Budget")

            If ds.Tables(0).Rows(0)("Skip_Budget") = "1" Then
                chkSkip.Checked = True
            Else
                chkSkip.Checked = False
            End If

            cbApprove.JSProperties("cpSkip") = ds.Tables(0).Rows(0)("Skip_Budget")

            txtReason.Text = ds.Tables(0).Rows(0)("Skip_Notes")

            dtCEDate.ReadOnly = True
            dtCEDate.BackColor = Color.Silver
            cboPRNo.ReadOnly = True
            cboPRNo.BackColor = Color.Silver
            cboRFQSetNumber.ReadOnly = True
            cboRFQSetNumber.BackColor = Color.Silver
			'add 27-02-2019
            LinkFile.Text = IIf(ds.Tables(0).Rows(0)("File_Name") = "", "No File", ds.Tables(0).Rows(0)("File_Name"))
            'LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")
            Dim extension As String = ""
            extension = System.IO.Path.GetExtension("~/Files/" & ds.Tables(0).Rows(0)("File_Name")).ToString
            If extension <> ".pdf" Then
                If ds.Tables(0).Rows(0)("File_Name") <> "" Then
                    LinkFile.NavigateUrl = "~/Files/" & ds.Tables(0).Rows(0)("File_Name")

                End If
            End If

            '06/04/2019
            txtApproveNote.Text = ds.Tables(0).Rows(0)("Approval_Notes")

            'begin harusnya sudah gak perlu bind supplier dengan cara seperti ini , get supplier dari sp saja 13-03-2019
            ds = clsCostEstimationDB.GetDataSupplier(cboRFQSetNumber.Text, "00", ErrMsg)
            For i = 0 To ds.Tables(0).Rows.Count - 1
                a = a + 1

                vSupplier = vSupplier & "[" & ds.Tables(0).Rows(i)("Code") & "]" & ","

                headSupplier = headSupplier & "ISNULL([" & ds.Tables(0).Rows(i)("Code") & "],0) As " & vname & a & ","
            Next

            If a < 5 Then
                For a = a + 1 To 5
                    vSupplier = vSupplier & "[" & vfield & a & "]" & ","
                    headSupplier = headSupplier & "ISNULL([" & vfield & a & "],0) As " & vname & a & ","
                Next
            End If


            If Len(vSupplier) > 0 Then
                vSupplier = Left(vSupplier, Len(vSupplier) - 1)
                headSupplier = Left(headSupplier, Len(headSupplier) - 1)
            End If
            'end begin		  

            '13-03-2019		   
            Dim ds1 As New DataSet
            'ds1 = clsCostEstimationDB.GetListCEApprovalData(CENumber, vSupplier, headSupplier, ErrMsg)
            ds1 = clsCostEstimationDB.GetListCEApprovalData(CENumber, Revision, cboRFQSetNumber.Text, ErrMsg)


            If ErrMsg = "" Then
                Grid.DataSource = ds1
                Grid.DataBind()
            Else
                show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
            End If
        End If


    End Sub

    Private Sub up_Approve()
        Dim Errmsg As String = ""
        Dim CECls As New clsCostEstimation
        CECls.CENumber = txtCENumber.Text
        CECls.Revision = gs_CERevNo
        CECls.ApprovalNotes = txtApproveNote.Text


        clsCostEstimationDB.Approve(CECls, pUser, Errmsg)

        If Errmsg = "" Then
            cbApprove.JSProperties("cpMessage") = "Data Has Been Approved Successfully"
        End If
    End Sub

    Private Sub up_Reject()
        Dim Errmsg As String = ""
        Dim CECls As New clsCostEstimation
        CECls.CENumber = txtCENumber.Text
        CECls.Revision = gs_CERevNo
        CECls.ApprovalNotes = txtApproveNote.Text

        clsCostEstimationDB.Reject(CECls, pUser, Errmsg)

        If Errmsg = "" Then
            cbReject.JSProperties("cpMessage") = "Data Has Been Reject Successfully"
        End If

    End Sub

    Private Sub cbApprove_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbApprove.Callback
        up_Approve()
    End Sub

    Private Sub cbReject_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbReject.Callback
        up_Reject()
    End Sub
	Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
		Response.Redirect("~/CEApproval.aspx")
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
		e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Private Sub GridLoadAttachment(pRFQSetNo)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        Try
            ds = clsCostEstimationDB.GetDocumentQuotation_List(pRFQSetNo, ErrMsg)
            If ErrMsg = "" Then
                Grid_QuotationAtc.DataSource = ds.Tables(0)
                Grid_QuotationAtc.DataBind()

                If ds.Tables(0).Rows.Count = 0 Then
                    DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid_QuotationAtc)
                End If
            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid_QuotationAtc)
            End If

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid_QuotationAtc)
        End Try
    End Sub

    Private Sub Grid_QuotationAtc_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid_QuotationAtc.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_QuotationAtc_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid_QuotationAtc.CustomCallback
        Call GridLoadAttachment(cboRFQSetNumber.Value)
    End Sub

    Private Sub Grid_QuotationAtc_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid_QuotationAtc.HtmlDataCellPrepared
        'If e.DataColumn.FieldName = "FileType" Or e.DataColumn.FieldName = "FileName" Then
        'Editable
        e.Cell.BackColor = Color.LemonChiffon
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")

    End Sub

    Protected Sub keyFieldLink_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim link As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
        Dim container As GridViewDataItemTemplateContainer = TryCast(link.NamingContainer, GridViewDataItemTemplateContainer)

        If container.ItemIndex >= 0 Then
            link.Text = Split(container.KeyValue, "|")(0)
            link.Target = "_blank"
            link.NavigateUrl = Split(container.KeyValue, "|")(1)
        End If
    End Sub
End Class
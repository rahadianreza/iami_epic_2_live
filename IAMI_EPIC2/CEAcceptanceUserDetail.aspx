﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CEAcceptanceUserDetail.aspx.vb" Inherits="IAMI_EPIC2.CEAcceptanceUserDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function Upload() {
             cbSave.PerformCallback();
          
        }
        function GetMessage(s, e) {

            var status = s.cpStatus;

            if (s.cpMessage == "IA CAPEX/OPEX is already exists!") {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }

            //accept
            if (status == "2" ) {
                txtReason.SetEnabled(false);
                txtNotes.SetEnabled(false);
                chkSkip.SetEnabled(false);
                txtReason.SetEnabled(false);
                cbIABudget.SetEnabled(false);
                btnSubmit.SetEnabled(false);
               // cbIABudget.PerformCallback();
            }
            //sudah accept tapi belum submit (harus upload file)
            else if (status == "3" && (IALinkFile.GetText() == "No File")) {
                txtReason.SetEnabled(false);
                txtNotes.SetEnabled(false);
                chkSkip.SetEnabled(false);
                btnAccept.SetEnabled(false);
                cbIABudget.SetEnabled(true);
                //cbIABudget.PerformCallback();

            }
            //sudah submit & upload file
            else if (status == "3" && ( IALinkFile.GetText() != "No File")  ) {
                txtReason.SetEnabled(false);
                txtNotes.SetEnabled(false);
                chkSkip.SetEnabled(false);
               //cbIABudget.SetEnabled(false);
                document.getElementById("<%= uploaderFile.ClientID %>").disabled = true;
                btnAccept.SetEnabled(false);
                btnSubmit.SetEnabled(false);
                cbIABudget.SetEnabled(false);
            }
            else if (s.cpMessage == "IA CAPEX/OPEX is already exists!") {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }else if (s.cpMessage == "Data accepted successfully!"  ) {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                
//                millisecondsToWait = 10000;
//                setTimeout(function () {
//                      window.location.href = "/CEAcceptanceUser.aspx";
//                }, millisecondsToWait);
            }
            else if (s.cpMessage == "Data Submit successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                btnAccept.SetEnabled(false);
                btnSubmit.SetEnabled(false);
                //alert(IALinkFile.GetText());
                IALinkFile.SetText(s.cpFileName);
                document.getElementById("<%= uploaderFile.ClientID %>").disabled = true;
                cbIABudget.SetText(s.cpIABudget);
                cbIABudget.SetEnabled(false);
            }
            else if ((s.cpMessage == "File size must not exceed 2 MB") || (s.cpMessage == "Document File is required")){
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

            }
            
            
        }
		
		function Accept(s, e) {
           									   

            var C = confirm("Are you sure want to accept the data?");
			if (C == false) {
                e.processOnServer = false;
                return;
            }
        }

		
        function OnBatchEditStartEditing(s, e) {
        
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName != "Qty" && currentColumnName != "PriceEst" && currentColumnName != "Remarks") {
                e.cancel = true;
            
            }
            currentEditableVisibleIndex = e.visibleIndex;
        }

        function GridLoad() {

            Grid.PerformCallback('gridload|xx|0|' + cboRFQSetNumber.GetText());
        }

        function UpdateTotalAmount(s,e) {
            startIndex = 0;
            var qty = 0;
            var price = 0;
            var amount = 0;

            qty = parseFloat(qty);
            price = parseFloat(price);
            amount = parseFloat(amount);

            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                qty =  parseFloat(Grid.batchEditApi.GetCellValue(i, "Qty"));
                price =  parseFloat(Grid.batchEditApi.GetCellValue(i, "PriceEst"));
                amount = qty * price;

                Grid.batchEditApi.SetCellValue(i, "Amount", amount);
            }

            //alert(qty);
            //alert(price);
        }


//        function onFilesUploadStart(s, e) {
//            //Upload = s.GetText();
//            //cbcallback.performcalback(Upload, docUpload)
//            btnAccept.SetEnabled(false);
//            alert("aa");
//        }
//        function onFileUploadComplete(s, e) {
//            btnAccept.SetEnabled(true);
//            alert("bb");
//            if (s.cpType == "0") {
//                //INFO
//                toastr.info(s.cpMessage, 'Information');

//            } else if (s.cpType == "1") {
//                //SUCCESS
//                toastr.success(s.cpMessage, 'Success');

//            } else if (s.cpType == "2") {
//                //WARNING
//                toastr.warning(s.cpMessage, 'Warning');

//            } else if (s.cpType == "3") {
//                //ERROR
//                toastr.error(s.cpMessage, 'Error');

//            }

//            window.setTimeout(function () {
//                delete s.cpType;
//            }, 10);
        function LoadCompleted(s, e) {
            if (s.cpType == "0") {
                //INFO
                toastr.info(s.cpMessage, 'Information');

            } else if (s.cpType == "1") {
                //SUCCESS
                toastr.success(s.cpMessage, 'Success');

            } else if (s.cpType == "2") {
                //WARNING
                toastr.warning(s.cpMessage, 'Warning');

            } else if (s.cpType == "3") {
                //ERROR
                toastr.error(s.cpMessage, 'Error');

            }

            window.setTimeout(function () {
                delete s.cpType;
            }, 10);
        }


        function OnTabChanging(s, e) {
            var tabName = (pageControl.GetActiveTab()).name;
            e.cancel = !ASPxClientEdit.ValidateGroup('group' + tabName);
        }

     
    </script>

    <style type="text/css">
        .style2
        {
            height: 20px;
        }
    .hidden-div
        {
            display:none;
        }        
    </style>
<style type="text/css">
        .tr-all-height
        {
            height: 30px;
        }
        
        .td-col-left
        {
            width: 100px;
            padding: 0px 0px 0px 10px;
        }
        
        .td-col-left2
        {
            width: 140px;
        }
        
        
        .td-col-mid
        {
            width: 10px;
        }
        .td-col-right
        {
            width: 200px;
        }
        .td-col-free
        {
            width: 20px;
        }
        
        .div-pad-fil
        {
            padding: 5px 5px 5px 5px;
        }
        
        .td-margin
        {
            padding: 10px;
        }
		.customHeaderAtc {
			height: 32px;
		}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    <div style="padding: 5px 5px 5px 5px">

        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr style="height: 10px">
                    <td colspan="5">
                    </td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="CE Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    <dx1:ASPxTextBox ID="txtCENumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                        Width="170px" ClientInstanceName="txtCENumber" MaxLength="15" Height="25px" ReadOnly="True"
                                        Font-Bold="True" BackColor="Silver">
                                    </dx1:ASPxTextBox>
                                </td>
                                <td class="td-col-mid">
                                </td>
                                <td class="td-col-right">
                                    <dx1:ASPxTextBox ID="txtRev" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                        Width="45px" ClientInstanceName="txtRev" MaxLength="20" Height="25px" ReadOnly="True"
                                        Font-Bold="True" ForeColor="Black" HorizontalAlign="Center" BackColor="Silver">
                                    </dx1:ASPxTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="CE Date">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                        &nbsp;
                    </td>
                    <td class="td-col-right" colspan="2">
                        <dx:ASPxDateEdit ID="dtCEDate" runat="server" Theme="Office2010Black" Width="100px"
                            AutoPostBack="false" ClientInstanceName="dtCEDate" EditFormatString="dd-MMM-yyyy"
                            DisplayFormatString="dd-MMM-yyyy" Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                            <CalendarProperties>
                                <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                </WeekNumberStyle>
                                <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                </ButtonStyle>
                            </CalendarProperties>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                        </dx:ASPxDateEdit>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="PR No">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                        &nbsp;
                    </td>
                    <td class="td-col-right" colspan="2">
                        <dx1:ASPxComboBox ID="cboPRNo" runat="server" ClientInstanceName="cboPRNo" Width="220px"
                            Font-Names="Segoe UI" TextField="Code" ValueField="Code" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	cboRFQSetNumber.PerformCallback(cboPRNo.GetText());
    
}" />
                            <Columns>
                                <dx:ListBoxColumn Caption="PR Number" FieldName="Code" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td >
                       
                    </td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="RFQ Set Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right" colspan="2">
                        <dx1:ASPxComboBox ID="cboRFQSetNumber" runat="server" ClientInstanceName="cboRFQSetNumber"
                            Width="220px" Font-Names="Segoe UI" TextField="Code" ValueField="Code" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="GridLoad" />
                            <Columns>
                                <dx:ListBoxColumn Caption="RFQ Number" FieldName="Code" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td >
                        
                    </td>
                </tr>
                <tr style="height: 10px; display:none;">
                    <td></td>
                    <td></td>
                    <td>
                         <dx1:ASPxTextBox ID="txtParameter" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="200px" ClientInstanceName="txtParameter" MaxLength="20" Height="25px"
                            ReadOnly="True" Font-Bold="True" ForeColor="Black" HorizontalAlign="Center">
                        </dx1:ASPxTextBox>
                        <dx1:ASPxTextBox ID="txtFlag" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="50px" ClientInstanceName="txtFlag" MaxLength="20" Height="25px" ReadOnly="True"
                            Font-Bold="True" ForeColor="Black" HorizontalAlign="Center">
                        </dx1:ASPxTextBox>
                        
                         <dx1:ASPxTextBox ID="txtIABudget" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Theme ="Metropolis"
                            Width="200px" ClientInstanceName="txtIABudget" MaxLength="40" Height="25px" AutoCompleteType="Disabled">
                            
                   </dx1:ASPxTextBox>
                    </td>
                </tr>
                <tr style="height: 10px">
                    <td colspan="5">
                    </td>
                </tr>
            </table>
        </div>
    <div>
        <dx:ASPxCallback ID="cbDraft" runat="server" ClientInstanceName="cbDraft">
            <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
            <ClientSideEvents Init="GetMessage" />
        </dx:ASPxCallback>
    </div>

    <div style="padding: 5px 5px 5px 5px">
		<dx:ASPxPageControl ID="pageControl" runat="server" ClientInstanceName="pageControl"
                ActiveTabIndex="1" EnableHierarchyRecreation="True" Width="100%">
                <ClientSideEvents ActiveTabChanging="OnTabChanging" />
                <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                </ActiveTabStyle>
                <TabPages>
                    <dx:TabPage Name="CostEstimation" Text="Cost Estimation">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControlTab1" runat="server">
                                <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                                    EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
                                    Font-Size="9pt" KeyFieldName="Item_Code">
                                    <ClientSideEvents  BatchEditStartEditing="OnBatchEditStartEditing" />
                                    <Columns>
                                        <dx:GridViewDataTextColumn Name="Remarks" Caption="Condition Of Price" VisibleIndex="14"
                                            FieldName="Remarks" Width="200px">
                                            <PropertiesTextEdit MaxLength="100" ClientInstanceName="Remarks">
                                                <Style HorizontalAlign="Left"></Style>
                                            </PropertiesTextEdit>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataComboBoxColumn Name="ItemCode" Caption="Material No" FieldName="Item_Code"
                                            VisibleIndex="0" Width="120px" FixedStyle="Left">
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn Name="Description" Caption="Description" VisibleIndex="1"
                                            Width="200px" FieldName="Description">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Name="Specification" Caption="Specification" VisibleIndex="2"
                                            Width="250px" FieldName="Specification">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Name="UOM" Caption="UoM" VisibleIndex="5" FieldName="UOM"
                                            Width="70px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="6" Width="120px" FieldName="Supplier1">
                                            <PropertiesTextEdit DisplayFormatString="#,###">
                                            </PropertiesTextEdit>
                                            <HeaderStyle Wrap="True" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="7" Width="120px" FieldName="Supplier2">
                                            <PropertiesTextEdit DisplayFormatString="#,###">
                                            </PropertiesTextEdit>
                                            <HeaderStyle Wrap="True" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="8" Width="120px" FieldName="Supplier3">
                                            <PropertiesTextEdit DisplayFormatString="#,###">
                                            </PropertiesTextEdit>
                                            <HeaderStyle Wrap="True" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="9" Width="120px" FieldName="Supplier4">
                                            <PropertiesTextEdit DisplayFormatString="#,###">
                                            </PropertiesTextEdit>
                                            <HeaderStyle Wrap="True" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="10" Width="120px" FieldName="Supplier5">
                                            <PropertiesTextEdit DisplayFormatString="#,###">
                                            </PropertiesTextEdit>
                                            <HeaderStyle Wrap="True" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Name="Amount" Caption="Amount" VisibleIndex="13" FieldName="Amount"
                                            Width="120px">
                                            <PropertiesTextEdit DisplayFormatString="#,###">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Right">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Price Estimation" FieldName="PriceEst" Name="PriceEst"
                                            VisibleIndex="12" Width="100px">
                                            <PropertiesTextEdit DisplayFormatString="#,###" MaxLength="11">
                                                <MaskSettings IncludeLiterals="DecimalSymbol" />
                                                <ValidationSettings ErrorDisplayMode="None">
                                                </ValidationSettings>
                                                <ClientSideEvents TextChanged="UpdateTotalAmount" />
                                                <Style HorizontalAlign="Right"></Style>
                                            </PropertiesTextEdit>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Quantity" FieldName="Qty" Name="Qty" VisibleIndex="4"
                                            Width="70px">
                                            <PropertiesTextEdit DisplayFormatString="#,###" MaxLength="11">
                                                <MaskSettings IncludeLiterals="DecimalSymbol" />
                                                <ValidationSettings ErrorDisplayMode="None">
                                                </ValidationSettings>
                                                <ClientSideEvents TextChanged="UpdateTotalAmount" />
                                                <Style HorizontalAlign="Right"></Style>
                                            </PropertiesTextEdit>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True"
                                        AllowSelectByRowClick="True" />
                                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                                    </SettingsPager>
                                    <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                                    </SettingsEditing>
                                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                                        VerticalScrollableHeight="200" VerticalScrollBarMode="Visible" />
                                    <Styles>
                                        <Header HorizontalAlign="Center">
                                            <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                        </Header>
                                    </Styles>
                                    <StylesEditors ButtonEditCellSpacing="0">
                                        <ProgressBar Height="21px">
                                        </ProgressBar>
                                    </StylesEditors>
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Quotation" Text="Quotation">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControlTab2" runat="server">
                                <dx:ASPxGridView ID="Grid_QuotationAtc" runat="server" AutoGenerateColumns="False"
                                    ClientInstanceName="Grid_QuotationAtc" EnableTheming="True" Theme="Office2010Black"
                                    Width="100%" Settings-VerticalScrollBarMode="Visible" Font-Names="Segoe UI" Font-Size="9pt"
                                    KeyFieldName="FileName;FilePath">
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Caption=" "
                                            Width="30px" Visible="false">
                                        </dx:GridViewCommandColumn>
                                        <%--<dx:GridViewDataCheckColumn Caption=" " Name="chk" VisibleIndex="0" Width="30px">
                                <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueType="System.Int32" >
                                </PropertiesCheckEdit>
                            </dx:GridViewDataCheckColumn>--%>
                                        <dx:GridViewDataTextColumn Caption="No" FieldName="No" Visible="true" VisibleIndex="1"
                                            Width="50px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Quotation No" FieldName="Quotation_No" VisibleIndex="2"
                                            Width="150px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="RFQ Number" FieldName="RFQ_Number" VisibleIndex="3"
                                            Width="150px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Supplier Name" FieldName="Supplier_Name" VisibleIndex="4"
                                            Width="200px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="File Name" FieldName="FileName" VisibleIndex="5"
                                            Width="500px">
                                            <DataItemTemplate>
                                                <dx:ASPxHyperLink ID="linkFileName" runat="server" OnInit="keyFieldLink_Init">
                                                </dx:ASPxHyperLink>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="File Path" FieldName="FilePath" Visible="false"
                                            VisibleIndex="6" Width="0px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="File Size" FieldName="FileSize" Visible="false"
                                            VisibleIndex="7" Width="80px">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsEditing Mode="Batch">
                                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                                    </SettingsEditing>
                                    <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSort="False"
                                        EnableRowHotTrack="True" />
                                    <Settings HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" VerticalScrollableHeight="200"
                                        ShowStatusBar="Hidden" />
                                    <Styles>
                                        <Header CssClass="customHeaderAtc" HorizontalAlign="Center">
                                        </Header>
                                    </Styles>
                                    <ClientSideEvents CallbackError="function(s, e) {e.handled = true;}" EndCallback="LoadCompleted" />
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
            </dx:ASPxPageControl>					 
    </div>


    <div style="padding: 5px 5px 5px 5px">
         <table style="width: 100%; border: 0px; height: 100px;">  
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:180px">
                        <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="Notes">
                    </dx1:aspxlabel>                   
                </td>
                <td></td>
                <td>
                    <dx1:ASPxMemo ID="txtNotes" runat="server" Height="50px" Width="500px"
                        ClientInstanceName="txtNotes" Font-Names="Segoe UI" Font-Size="9pt"
                        Theme="Metropolis" MaxLength="300" >
           
                    </dx1:ASPxMemo>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>  
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        <dx1:aspxlabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="IA CAPEX/OPEX">
                    </dx1:aspxlabel>                   
                </td>
                <td></td>
                <td> 
                    <dx:ASPxComboBox ID="cbIABudget" runat="server" ClientInstanceName="cbIABudget"
                            Width="240px" Font-Names="Segoe UI"  TextField="IA_BudgetNumber" Visible ="true"
                            ValueField="IA_BudgetNumber" TextFormatString="{0}" Font-Size="9pt"
                            Theme="Office2010Black" Enabled="true" Height="25px" IncrementalFilteringMode="None" DropDownStyle="DropDown" >
						    <Columns>            
                            <dx:ListBoxColumn Caption="IA Budget Number" FieldName="IA_BudgetNumber" Width="100px" />
                        
                            </Columns>
						    <ItemStyle Height="10px" Paddings-Padding="4px" ><Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>   
                                                 
                        </dx:ASPxComboBox>
                    
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>    
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="Document File">
                    </dx1:aspxlabel>                   
                </td>
                <td>&nbsp;</td>
                <td >
                  <%-- <dx:ASPxUploadControl ID="docUpload" ClientInstanceName="docUpload" runat="server"
                        Width="280px" OnFileUploadComplete="docUpload_FileUploadComplete" AutoStartUpload="true">
                        <ClientSideEvents FileUploadComplete="onFileUploadComplete" FileUploadStart="onFilesUploadStart(docUpload.filename)" />
                        <ValidationSettings AllowedFileExtensions=".pdf,.docx,.xlsx" />
                    </dx:ASPxUploadControl>--%>
                    
                    <asp:FileUpload ID="uploaderFile" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Height="20px" Width="250px" Accept=".pdf" /> 
                </td>
            </tr>
             <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        <dx1:aspxlabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="IA Document File">
                    </dx1:aspxlabel>                   
                </td>
                <td>&nbsp;</td>
                <td>
           
                    <dx:ASPxHyperLink ID="IALinkFile"  ClientInstanceName="IALinkFile" runat="server" Text="No File"  
                        >
                        <ClientSideEvents Click="function(s, e) {
                            //alert(IALinkFile.GetText());
							if (IALinkFile.GetText() != 'No File') {
                               
                                var fileName, fileExtension;
                                fileName = IALinkFile.GetText();
                                fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
                                
                                if (fileExtension =='pdf'){
                                      var pathArray = window.location.pathname.split('/');
                                      var url = window.location.origin + '/Files/' + IALinkFile.GetText();
                                      window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
                                }
                                var pathArray = window.location.pathname.split('/');
                              }
	                        
                        }" />
                    </dx:ASPxHyperLink>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       

            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px"></td>
                <td></td>
                <td>
                   <%-- <asp:RequiredFieldValidator ID="rfvDoc" runat="server" Display="Dynamic" ErrorMessage="* Document File is required" ControlToValidate="uploaderFile"
                                                ForeColor="Red" Font-Names="Segoe UI" Font-Size="9pt" ></asp:RequiredFieldValidator>--%>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>       
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">         
                <dx:ASPxCheckBox ID="chkSkip" runat="server" ClientInstanceName="chkSkip" 
                    Text="Skip Budget" ValueChecked="1" ValueType="System.Int32" ValueUnchecked="0" 
                        Font-Names="Segoe UI" Font-Size="9pt" CheckState="Unchecked" >
                    
                    </dx:ASPxCheckBox></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        <dx1:aspxlabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="Notes/Reason">
                    </dx1:aspxlabel>                   
                </td>
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxMemo ID="txtReason" runat="server" Height="50px" Width="500px"
                        ClientInstanceName="txtReason" Font-Names="Segoe UI" Font-Size="9pt"
                        Theme="Metropolis" MaxLength="300" >
                  
                    </dx1:ASPxMemo>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        <dx1:aspxlabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="CE Document File">
                    </dx1:aspxlabel>                   
                </td>
                <td>&nbsp;</td>
                <td>
           
                    <dx:ASPxHyperLink ID="LinkFile"  ClientInstanceName="LinkFile" runat="server" Text="No File" 
                        >
                        <ClientSideEvents Click="function(s, e) {
                            //alert(LinkFile.GetText());
							if (LinkFile.GetText() != 'No File') {
                                
                                var fileName, fileExtension;
                                fileName = LinkFile.GetText();
                                fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));

                                if (fileExtension =='pdf'){
                                    var pathArray = window.location.pathname.split('/');
                                    var url = window.location.origin + '/' + pathArray[1] + '/Files/' + LinkFile.GetText();
                                    window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
                                    //var url =window.location.origin + '/Files/' + LinkFile.GetText();
                                    //window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
                                }
                                var pathArray = window.location.pathname.split('/');

                                
                               
                                //var url = window.location.origin + '/' + pathArray[1] + '/ItemList.aspx';                                
                                //window.location.href = window.location.origin + '/ItemList.aspx';

                            }
	                        
                        }" />
                    </dx:ASPxHyperLink>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        &nbsp;</td>
                <td>&nbsp;</td>
                <td>
           
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        &nbsp;</td>
                <td>&nbsp;</td>
                <td>
           
                        &nbsp;</td>
                <td>

                     <dx:ASPxCallback ID="cbFile" runat="server" ClientInstanceName="cbFile">
                         <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
                                                        
                </dx:ASPxCallback>

                     </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                        &nbsp;</td>
                <td>&nbsp;</td>
                <td>
           
                    &nbsp;</td>
                <td>&nbsp;
                   <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                        <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
                    </dx:ASPxCallback>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>       
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; " colspan="8">

                    <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" UseSubmitBehavior="False"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>
                    &nbsp;
                                <dx:ASPxButton ID="btnAccept" runat="server" Text="Accept" 
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" 
                        Height="25px" AutoPostBack="False"
                                    ClientInstanceName="btnAccept" Theme="Default">                        
                                   <ClientSideEvents Click="Accept" />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            &nbsp;
                            <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" 
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" 
                        Height="25px" AutoPostBack="False"
                                    ClientInstanceName="btnSubmit" Theme="Default">                        
                                    
                                    <ClientSideEvents Click="function(s, e) {
	 
   
     //fill IABudgetNo cek skip budget yes/no
     if (chkSkip.GetChecked() == false) {
            if (cbIABudget.GetText() == '') {
                cbIABudget.Focus();
                toastr.warning('Please select IA CAPEX/OPEX Number!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
			    //$('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }
      }

      
      if (document.getElementById('Content_uploaderFile').value==''){
        
            toastr.warning('Document upload is required', 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
		    //$('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
      }

    var msg = confirm('Are you sure want to submit this data ?');
    if (msg == false) {
        e.processOnServer = false;
        return;       
    }   
							

}" />
                                    
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            &nbsp;
                    
                    <dx1:ASPxButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" UseSubmitBehavior="False"
                        ClientInstanceName="btnPrint" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>


                    </td>
            </tr>       
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px"></td>
                <td></td>
                <td>
           
                    &nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>                                       
         </table>
    </div>

</div>

</asp:Content>

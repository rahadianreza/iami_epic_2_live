﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data

Public Class CompositionListDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String
    Dim MatNo As String
    Dim status As String
    Dim rev As Integer
    Dim statusAdmin As String
    'Dim tempPRNo As String
#End Region

#Region "Initialization"

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|B010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

          

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
     
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SeqNo As String

        Master.SiteTitle = "COMPOSITION LIST DETAIL"
        pUser = Session("user")
        SeqNo = Split(Request.QueryString("ID"), "|")(0)
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            txtPRNo.Text = SeqNo
            up_FillCombo()
            up_GridLoad(SeqNo)
            If SeqNo <> "" Then
                up_LoadData(SeqNo)
                up_GridLoad(SeqNo)
            End If
        End If

    End Sub
#End Region

#Region "Procedure"

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub
    Private Sub up_FillCombo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = ClsCompositionMasterDB.GetDataItemParent(pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboParentItem.Items.Add(Trim(ds.Tables(0).Rows(i)("MaterialCategory") & ""))
            Next
        End If

        ds = ClsCompositionMasterDB.GetDataUOM(pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboParentItemUOM.Items.Add(Trim(ds.Tables(0).Rows(i)("UOM") & ""))
            Next
        End If

    End Sub
    Private Sub up_LoadData(pSequenceNo As Integer)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim cCompositionMaster As New ClsCompositionMaster

        'Try
        cCompositionMaster.CompositionSequenceNo = pSequenceNo
        ds = ClsCompositionMasterDB.GetDataItem(cCompositionMaster, pErr)

        If pErr = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                txtPRNo.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("CompositionSequenceNo")), 0, ds.Tables(0).Rows(0)("CompositionSequenceNo"))
                cboParentItem.SelectedIndex = cboParentItem.Items.IndexOf(cboParentItem.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("ParentItemCode") & "")))
                txtParentItemQty.Text = Trim(ds.Tables(0).Rows(0)("ParentItemQty") & "")
                txtParent.Text = Trim(ds.Tables(0).Rows(0)("ParentItemCode") & "")
                cboParentItemUOM.SelectedIndex = cboParentItemUOM.Items.IndexOf(cboParentItemUOM.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("ParentItemUOM") & "")))


            End If
        Else
            cboParentItem.Text = ""
            txtParentItemQty.Text = ""
            cboParentItemUOM.Text = ""


        End If
    End Sub


    Private Sub up_GridLoad(ByVal SeqNo As String)
        Dim ErrMsg As String = ""
        Dim Ses As New List(Of ClsCompositionMaster)
        Ses = ClsCompositionMasterDB.Detail_Getlist(SeqNo, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()

        End If
    End Sub


#End Region

#Region "Control Event"

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        gs_Back = True
        Response.Redirect("~/CompositionList.aspx")


    End Sub

    Protected Sub Grid_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        'If e.CallbackName <> "CANCELEDIT" Then
        '    up_GridLoad("1", GridJobPos, "JobPosition")
        'End If
        up_GridLoad(txtPRNo.Text)
    End Sub

    Protected Sub Grid_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsCompositionMaster With {.CompositionSequenceNo = txtPRNo.Text,
                                                  .ParentItemCode = txtParent.Text,
                                         .ChildItemCode = e.NewValues("ChildItemCode"),
                                         .ChildItemQty = e.NewValues("ChildItemQty"),
                                         .ChildItemUOM = e.NewValues("ChildItemUOM"),
                                         .CurrencyCode = e.NewValues("CurrencyCode"),
                                         .Price = e.NewValues("Price"),
                                         .CreateUser = pUser}

        ClsCompositionMasterDB.InsertDetail(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, Grid)
            up_GridLoad(txtPRNo.Text)
        End If
    End Sub


    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsCompositionMaster With {.CompositionSequenceNo = txtPRNo.Text,
                                                 .ParentItemCode = txtParent.Text,
                                          .ChildCompositionSequenceNo = e.Keys("ChildCompositionSequenceNo"),
                                        .ChildItemCode = e.NewValues("ChildItemCode"),
                                        .ChildItemQty = e.NewValues("ChildItemQty"),
                                        .ChildItemUOM = e.NewValues("ChildItemUOM"),
                                        .CurrencyCode = e.NewValues("CurrencyCode"),
                                        .Price = e.NewValues("Price"),
                                        .CreateUser = pUser}

        ClsCompositionMasterDB.UpdateDetail(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, Grid)
            up_GridLoad(txtPRNo.Text)
        End If
    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsCompositionMaster With {.CompositionSequenceNo = txtPRNo.Text,
                                                .ParentItemCode = txtParent.Text,
                                         .ChildCompositionSequenceNo = e.Keys("ChildCompositionSequenceNo")}
        ClsCompositionMasterDB.DeleteDetail(Ses, pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, Grid)
            up_GridLoad(txtPRNo.Text)
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
        End If
    End Sub

    Private Sub Grid_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.BeforeGetCallbackResult
        If Grid.IsNewRowEditing Then
            Grid.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub


    Private Sub Grid_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        'If Not Grid.IsNewRowEditing Then
        '    If e.Column.FieldName = "General_Code" Then
        '        e.Editor.ReadOnly = True
        '        e.Editor.ForeColor = Color.Silver
        '    End If
        'End If
    End Sub

    Protected Sub Grid_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not Grid.IsNewRowEditing) Then
            Grid.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, Grid)
    End Sub

    Protected Sub Grid_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In Grid.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "ChildItemCode" Then
                If IsNothing(e.NewValues("ChildItemCode")) OrElse e.NewValues("ChildItemCode").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Child Item Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "ChildItemQty" Then
                If IsNothing(e.NewValues("ChildItemQty")) OrElse e.NewValues("ChildItemQty").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Child Item Qty!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "ChildItemUOM" Then
                If IsNothing(e.NewValues("ChildItemUOM")) OrElse e.NewValues("ChildItemUOM").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Child Item UOM!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "CurrencyCode" Then
                If IsNothing(e.NewValues("CurrencyCode")) OrElse e.NewValues("CurrencyCode").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Currency Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If


            If dataColumn.FieldName = "Price" Then
                If IsNothing(e.NewValues("Price")) OrElse e.NewValues("Price").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Price!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, Grid)
                    Exit Sub
                End If
            End If


        Next column
    End Sub

#End Region




End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Comparison.aspx.vb" Inherits="IAMI_EPIC2.Comparison" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };
        function MessageBox(s, e) {
            //alert(s.cpmessage);
            if (s.cbMessage == "Download Excel Successfully") {
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cbMessage == "Request Date To must be higher than Request Date From") {
                toastr.success(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function OnTabChanging(s, e) {
            var tabName = (pageControl.GetActiveTab()).name;
            e.cancel = !ASPxClientEdit.ValidateGroup('group' + tabName);
        }
    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        .style4
        {
            width: 24px;
        }
        .style8
        {
            height: 38px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<table style="width: 100%; border: 1px solid black; height: 100px;">
<tr>
<td></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Name">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " colspan="3">


                        <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                            Font-Names="Segoe UI"  TextField="Project_Name" ValueField="Project_ID" TextFormatString="{1}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                  cboGroup.PerformCallback(cboProject.GetSelectedItem().GetColumnText(0) );
                                  cboCommodity.ClearItems();
                                  cboCommodityGroup.ClearItems();
                                  cboPartNo.ClearItems();
                               
                }" />  
                            <Columns>
                                <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
<td class="style4">&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Commodity">
                        </dx:ASPxLabel>
                    </td>

<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxComboBox ID="cboCommodity" runat="server" ClientInstanceName="cboCommodity"
                            Font-Names="Segoe UI" TextField="Commodity" ValueField="Commodity" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                  cboCommodityGroup.PerformCallback(cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '|' + cboCommodity.GetSelectedItem().GetColumnText(0));
                                  cboPartNo.ClearItems();
                    }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Commodity" FieldName="Commodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td class="style4">&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Part No">
                        </dx:ASPxLabel>
                    </td>
<td style="padding: 0px 0px 0px 10px; width: 200px">
                        <dx:ASPxComboBox ID="cboPartNo" runat="server" ClientInstanceName="cboPartNo"
                            Font-Names="Segoe UI" TextField="Part No" ValueField="Part_No" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                            <Columns>
                                <dx:ListBoxColumn Caption="PartNo" FieldName="Part_No" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td>&nbsp;</td>
</tr>
<tr style="height: 2px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td class="style4" align = "center">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Group">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 200px" colspan="3" >


                        <dx:ASPxComboBox ID="cboGroup" runat="server" ClientInstanceName="cboGroup" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Group_ID" ValueField="Group_ID"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                 cboCommodity.PerformCallback(cboProject.GetSelectedItem().GetColumnText(0)  + '|' + cboGroup.GetSelectedItem().GetColumnText(0) );
                                 cboCommodityGroup.ClearItems();
                                 cboPartNo.ClearItems();
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Group ID" FieldName="Group_ID" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Sourcing Group">
                        </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxComboBox ID="cboCommodityGroup" runat="server" ClientInstanceName="cboCommodityGroup"
                            Font-Names="Segoe UI" TextField="Group_Comodity" 
                            ValueField="Group_Comodity" TextFormatString="{0}"
                                                Font-Size="9pt" Theme="Office2010Black" 
                            DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                EnableIncrementalFiltering="True" Height="25px" 
                            TabIndex="5">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                          cboPartNo.PerformCallback(cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '|' + cboCommodity.GetSelectedItem().GetColumnText(0) + '|' + cboCommodityGroup.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Sourcing Group" FieldName="Group_Comodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 200px">
                        &nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr>
<td style="padding: 20px 0px 0px 10px" class="style8" colspan="4">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnBack" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnShowData" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
                 
                            Grid.PerformCallback('load|' + cboProject.GetValue() + '|' + cboGroup.GetValue() + '|' + cboCommodity.GetValue() + '|' + cboCommodityGroup.GetValue() + '|' + cboPartNo.GetValue() );
                            GridTooling.PerformCallback('load|' + cboProject.GetValue() + '|' + cboGroup.GetValue() + '|' + cboCommodity.GetValue() + '|' + cboCommodityGroup.GetValue() + '|' + cboPartNo.GetValue() );
                        
                        }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnDownload" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
         
            </td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>

    <div style="padding: 5px 0px 5px 0px">
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="SELECT DISTINCT A.Project_ID, 
				B.Project_Name 
			FROM Quotation_Supplier_Project A
			INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID
			WHERE A.Complete = 1 ">
        </asp:SqlDataSource>
        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
            <ClientSideEvents CallbackComplete="SetCode" />
        </dx:ASPxCallback>
        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
        </dx:ASPxGridViewExporter>

        
        <dx:ASPxGridViewExporter ID="GridExporterTooling" runat="server" GridViewID="GridTooling">
        </dx:ASPxGridViewExporter>

        
    </div>
    <div style="padding: 5px 0px 5px 0px">
        <table style="width: 100%;">
            <tr>
                <td>
                    <dx:aspxpagecontrol id="pageControl" runat="server" clientinstancename="pageControl" EnableTabScrolling="true"
                    activetabindex="0" enablehierarchyrecreation="True"
                    width="100%">
                        <ClientSideEvents ActiveTabChanging="OnTabChanging" />
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <TabPages>
                            <dx:TabPage Name="Parts" Text="Parts">
                                <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                                </ActiveTabStyle>
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl2" runat="server">
                                        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                                            EnableTheming="True" Theme="Office2010Black" Width="100%"
                                            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Description" 
                                            OnAfterPerformCallback="Grid_AfterPerformCallback">
                                            <ClientSideEvents EndCallback="OnEndCallback" ></ClientSideEvents>
                                            <Columns>                           
                                     
                                                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" VisibleIndex="0"
                                                        Width="250px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                                        <Settings AutoFilterCondition="Contains" />

                                                    <EditFormCaptionStyle>
                                                    <Paddings Padding="5px"></Paddings>
                                                    </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>

                                                       <dx:GridViewDataTextColumn Caption="GUIDE PRICE" FieldName="GuidePrice" VisibleIndex="1"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                       
                                                    </dx:GridViewDataTextColumn>

                                                    <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier1" VisibleIndex="2"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                       
                                                    </dx:GridViewDataTextColumn>
                                                      <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier2" VisibleIndex="3"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                        <EditFormCaptionStyle>
                                                        <Paddings Padding="5px"></Paddings>
                                                        </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                      <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier3" VisibleIndex="4"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                        <EditFormCaptionStyle>
                                                        <Paddings Padding="5px"></Paddings>
                                                        </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                      <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier4" VisibleIndex="5"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                        <EditFormCaptionStyle>
                                                        <Paddings Padding="5px"></Paddings>
                                                        </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                      <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier5" VisibleIndex="6"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                        <EditFormCaptionStyle>
                                                        <Paddings Padding="5px"></Paddings>
                                                        </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                   <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier6" VisibleIndex="7"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                            <EditFormCaptionStyle>
                                                            <Paddings Padding="5px"></Paddings>
                                                            </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>

                                            </Columns>
                                                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm"/>
                                                                    <SettingsPager Mode="ShowPager" PageSize="16" AlwaysShowPager="true"></SettingsPager>
                                                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="390" HorizontalScrollBarMode="Auto" />
                                                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                                                    <SettingsPopup>
                                                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                                                    </SettingsPopup>

                                                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                                                        <Header>
                                                                            <Paddings Padding="2px"></Paddings>
                                                                        </Header>

                                                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                                                        </EditFormColumnCaption>
                                                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                                                    </Styles>
                    
                                                                    <Templates>
                                                                        <EditForm>
                                                                            <div style="padding: 15px 15px 15px 15px">
                                                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                                                        runat="server">
                                                                                    </dx:ASPxGridViewTemplateReplacement>
                                                                                </dx:ContentControl>
                                                                            </div>
                                                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                                                    runat="server">
                                                                                </dx:ASPxGridViewTemplateReplacement>
                                                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                                                    runat="server">
                                                                                </dx:ASPxGridViewTemplateReplacement>
                                                                            </div>
                                                                        </EditForm>
                                                                    </Templates>
                                        </dx:ASPxGridView>

                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                            <dx:TabPage Name="Tooling" Text="Tooling">
                                <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                                </ActiveTabStyle>
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl3" runat="server">
                                        <dx:ASPxGridView ID="GridTooling" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridTooling"
                                            EnableTheming="True" Theme="Office2010Black" Width="100%"
                                            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Description" 
                                            OnAfterPerformCallback="GridTooling_AfterPerformCallback">
                                            <ClientSideEvents EndCallback="OnEndCallback" ></ClientSideEvents>
                                            <Columns>                           

                                                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" VisibleIndex="0"
                                                        Width="250px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                                                        <Settings AutoFilterCondition="Contains" />

                                                    <EditFormCaptionStyle>
                                                    <Paddings Padding="5px"></Paddings>
                                                    </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                      <dx:GridViewDataTextColumn Caption="GUIDE PRICE" FieldName="GuidePrice" VisibleIndex="1"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                       
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier1" VisibleIndex="2"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                       
                                                    </dx:GridViewDataTextColumn>
                                                      <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier2" VisibleIndex="3"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                        <EditFormCaptionStyle>
                                                        <Paddings Padding="5px"></Paddings>
                                                        </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                      <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier3" VisibleIndex="4"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                        <EditFormCaptionStyle>
                                                        <Paddings Padding="5px"></Paddings>
                                                        </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                      <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier4" VisibleIndex="5"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                        <EditFormCaptionStyle>
                                                        <Paddings Padding="5px"></Paddings>
                                                        </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                      <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier5" VisibleIndex="6"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                        <EditFormCaptionStyle>
                                                        <Paddings Padding="5px"></Paddings>
                                                        </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                   <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier6" VisibleIndex="7"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                            <EditFormCaptionStyle>
                                                            <Paddings Padding="5px"></Paddings>
                                                            </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>

                                            </Columns>
                                                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm"/>
                                                                    <SettingsPager Mode="ShowPager" PageSize="16" AlwaysShowPager="true"></SettingsPager>
                                                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="390" HorizontalScrollBarMode="Auto" />
                                                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                                                    <SettingsPopup>
                                                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                                                    </SettingsPopup>

                                                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                                                        <Header>
                                                                            <Paddings Padding="2px"></Paddings>
                                                                        </Header>

                                                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                                                        </EditFormColumnCaption>
                                                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                                                    </Styles>
                    
                                                                    <Templates>
                                                                        <EditForm>
                                                                            <div style="padding: 15px 15px 15px 15px">
                                                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                                                        runat="server">
                                                                                    </dx:ASPxGridViewTemplateReplacement>
                                                                                </dx:ContentControl>
                                                                            </div>
                                                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                                                    runat="server">
                                                                                </dx:ASPxGridViewTemplateReplacement>
                                                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                                                    runat="server">
                                                                                </dx:ASPxGridViewTemplateReplacement>
                                                                            </div>
                                                                        </EditForm>
                                                                    </Templates>
                                        </dx:ASPxGridView>

                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                        </TabPages>
                    </dx:aspxpagecontrol>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

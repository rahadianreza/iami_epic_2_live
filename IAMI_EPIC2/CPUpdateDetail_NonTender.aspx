﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CPUpdateDetail_NonTender.aspx.vb" Inherits="IAMI_EPIC2.CPUpdateDetail_NonTender" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    //function for force chrome allow click button browse file when scrollbar
    window.onload = function () {
        var GetDocumentScrollTop = function () {
            var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollTop > 0;
            if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                if (ASPx.Browser.MacOSMobilePlatform)
                    return window.pageYOffset;
                else if (ASPx.Browser.WebKitFamily)
                    return document.documentElement.scrollTop || document.body.scrollTop;
                return document.body.scrollTop;
            }
            else
                return document.documentElement.scrollTop;
        };
        var _aspxGetDocumentScrollTop = function () {
            if (__aspxWebKitFamily) {
                if (__aspxMacOSMobilePlatform)
                    return window.pageYOffset;
                else
                    return document.documentElement.scrollTop || document.body.scrollTop;
            }
            else
                return document.documentElement.scrollTop;
        }
        if (window._aspxGetDocumentScrollTop) {
            window._aspxGetDocumentScrollTop = _aspxGetDocumentScrollTop;
            window.ASPxClientUtils.GetDocumentScrollTop = _aspxGetDocumentScrollTop;
        } else {
            window.ASPx.GetDocumentScrollTop = GetDocumentScrollTop;
            window.ASPxClientUtils.GetDocumentScrollTop = GetDocumentScrollTop;
        }
        /* Begin -> Correct ScrollLeft  */
        var GetDocumentScrollLeft = function () {
            var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollLeft > 0;
            if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                if (ASPx.Browser.MacOSMobilePlatform)
                    return window.pageXOffset;
                else if (ASPx.Browser.WebKitFamily)
                    return document.documentElement.scrollLeft || document.body.scrollLeft;
                return document.body.scrollLeft;
            }
            else
                return document.documentElement.scrollLeft;
        };
        var _aspxGetDocumentScrollLeft = function () {
            if (__aspxWebKitFamily) {
                if (__aspxMacOSMobilePlatform)
                    return window.pageXOffset;
                else
                    return document.documentElement.scrollLeft || document.body.scrollLeft;
            }
            else
                return document.documentElement.scrollLeft;
        }
        if (window._aspxGetDocumentScrollLeft) {
            window._aspxGetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
            window.ASPxClientUtils.GetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
        } else {
            window.ASPx.GetDocumentScrollLeft = GetDocumentScrollLeft;
            window.ASPxClientUtils.GetDocumentScrollLeft = GetDocumentScrollLeft;
        }
        /* End -> Correct ScrollLeft  */
    };
    //end function for force chrome allow
    
    var currentColumnName;
    var dataExist;
    var dataSubmitted;

    function CountingLength(s, e) {
        //Counting lenght
        var memo = memoNote.GetText();
        var lenMemo = memo.length;
        lenMemo = lenMemo;
        if (lenMemo >= 300) {
            lenMemo = 300;
        }

        //Set Text
        lblLenght.SetText(lenMemo + "/300");

        //counting length supplier note
        var supNote = suppNote.GetText();
        var lenNote = supNote.length;
        lenNote = lenNote;
        if (lenNote >= 300) {
            lenMemo = 300;
        }
        //set text supplier note
        lblLenght2.SetText(lenNote + "/300");
    }

    
    function OnBatchEditStartEditing(s, e) {
        currentColumnName = e.focusedColumn.fieldName;

        if ((currentColumnName == 'Material_No') || (currentColumnName == 'Description') || (currentColumnName == 'Specification') || (currentColumnName == 'Qty') || (currentColumnName == 'UOM') || (currentColumnName == 'Remarks') || (currentColumnName == 'Curr_Code')) {
            e.cancel = true;
            Grid.batchEditApi.EndEdit();

        } else if ((currentColumnName == 'QuotationPricePcs') || (currentColumnName == 'TotalQuotationPrice') || (currentColumnName == 'ProposalPricePcs') || (currentColumnName == 'TotalProposalPrice') || (currentColumnName == 'TotalBestPrice')) {
            e.cancel = true;
            Grid.batchEditApi.EndEdit();

        } else if (currentColumnName == 'BestPricePcs') {
            window.setTimeout(function () {
                cbExist.PerformCallback('IsSubmitted');
            }, 10);

            window.setTimeout(function () {
                if (dataSubmitted == "1") {
                    e.cancel = true;
                    Grid.CancelEdit();
                    Grid.batchEditApi.EndEdit();
                }
            }, 10);

        }
    }

    function OnBatchEditEndEditing(s, e) {
        window.setTimeout(function () {
            var price = s.batchEditApi.GetCellValue(e.visibleIndex, "BestPricePcs");
            var qty = s.batchEditApi.GetCellValue(e.visibleIndex, "Qty");

            if ((currentColumnName == "BestPricePcs") || (currentColumnName == "TotalBestPrice")) {
                s.batchEditApi.SetCellValue(e.visibleIndex, "TotalBestPrice", price * qty);
            }
        }, 10);

        window.setTimeout(function () {
            cbExist.PerformCallback('IsSubmitted');
        }, 10);


        window.setTimeout(function () {
            if (dataSubmitted == "1") {
                e.cancel = true;
                Grid.CancelEdit();
                Grid.batchEditApi.EndEdit();
                window.setTimeout(function () { toastr.warning('Cannot update IAMI Best Price/pc! The data already submitted.', 'Warning'); }, 10);
            } 
        }, 100);
    }

    function ShowConfirmOnLosingChanges() {
        var C = confirm("The data you have entered may not be saved. Are you sure want to leave?");
        if (C == true) {
            return true;
        } else {
            return false;
        }
    }

    function LoadDataGrid(s, e) {
        Grid.PerformCallback('gridload');
    }

    function GridLoadAtc(s, e) {
        //Grid.PerformCallback();
        window.setTimeout(function () {
                GridAtc.PerformCallback();
        }, 100);
    }

    //message toastr
    function GetMessage(s, e) {
        if (s.cpParameter == "exist") {
            dataExist = s.cpResult;

        } else if (s.cpParameter == "IsSubmitted") {
            dataSubmitted = s.cpResult;
//        } else if (s.cpParameter == "draft" || s.cpParameter == "submit") {
//            if (s.cpSubmit == "Y") {
//                
//                btnDeleteAtc.SetEnabled(false);
//                btnSubmit.SetEnabled(false);
//                ucAtc.SetEnabled(false);
//            }
        } else if (s.cpMessage == 'Data saved successfully!') {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

        }
        else if (s.cpMessage == 'Data submitted successfully!') {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            btnDeleteAtc.SetEnabled(false);
            btnDraft.SetEnableld(false);
            btnSubmit.SetEnabled(false);
            ucAtc.SetEnabled(false);
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            if (s.cpView == "") {
                Grid.PerformCallback('gridload');

            }
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

        window.setTimeout(function () {
            delete s.cpParameter;
        }, 10);
    }


    function Back(s, e) {
        if (Grid.batchEditApi.HasChanges() == true) {
            //there is an update data
            if (ShowConfirmOnLosingChanges() == false) {
                e.processOnServer = false;
                return false;
            }
        }

        if (hf.Get("hfNotes") != memoNote.GetText()) {
            if (ShowConfirmOnLosingChanges() == false) {
                e.processOnServer = false;
                return false;
            }
        }
    }

    function Draft(s, e) {

        startIndex = 0;
        var BestPricePcs = 0;
        BestPricePcs = parseFloat(BestPricePcs);

        for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
            BestPricePcs = parseFloat(Grid.batchEditApi.GetCellValue(i, "BestPricePcs"));

            if (BestPricePcs <= 0 || isNaN(BestPricePcs) || BestPricePcs == "" ) {
                toastr.warning('Best Price must greater than zero !', 'Warning');
                Grid.batchEditApi.SetCellValue(i, "TotalBestPrice", 0);
                return false;
            }
        }

        window.setTimeout(function () {
            //CONFIRMATION Before update data
            var C = confirm("Are you sure want to save the data?");
            if (C == true) {
                Grid.UpdateEdit();
                millisecondsToWait = 1000;
                setTimeout(function () {
                    Grid.PerformCallback('save');
                }
                , millisecondsToWait);

                setTimeout(function () {
                    cbExist.PerformCallback('Draft');
                }, millisecondsToWait);

                setTimeout(function () {
                    Grid.PerformCallback('draft');
                }, millisecondsToWait);

//                //toastr.info('Data save successfully!', 'Information');

//                if (Grid.batchEditApi.HasChanges() == true) {
//                    Grid.UpdateEdit();
//                    window.setTimeout(Grid.PerformCallback('save'), 200);
//                    //Grid.CancelEdit();

//                } else {
//                    window.setTimeout(Grid.PerformCallback('draft'), 200);
//                }

                hf.Set("hfNotes", memoNote.GetText());
                //$('html, body').animate({ scrollTop: 0 }, 'slow');

            } else {
                return false;
            }
        }, 500);
    }

    function Submit(s, e) {
        if (Grid.batchEditApi.HasChanges() == true) {
            //there is an update data
            toastr.warning('Please draft the data first before submit!', 'Warning');
            btnDraft.Focus();
            return;
        
        } else {
            startIndex = 0;
            var BestPricePcs = 0;
            BestPricePcs = parseFloat(BestPricePcs);

            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                BestPricePcs = parseFloat(Grid.batchEditApi.GetCellValue(i, "BestPricePcs"));

                if (BestPricePcs <= 0 || isNaN(BestPricePcs)) {
                    toastr.warning('Please draft the data first before submit!', 'Warning');
                    btnDraft.Focus();
                    return; 
                }
            }   
        }

        window.setTimeout(function () {
            var C = confirm("Are you sure want to submit the data?");

            if (C == true) {
                cbExist.PerformCallback("submit");

                window.setTimeout(function () {
                    //btnDraft.SetEnabled(false);
                    //btnSubmit.SetEnabled(false);

                    window.setTimeout(function () {
                        Grid.PerformCallback('gridload');
                    }, 50);

                    //$('html, body').animate({ scrollTop: 0 }, 'slow');
                }, 10);
            }
        }, 300);
    }

    function GridLoadCompleted(s, e) {
        if (s.cpSaveData != "1") {
            //Called from CPUpdate.aspx by clicked on the detail grid
            return false;

        } else {
            //Uncomplete fill on the grid.
            if (s.cpMessage == 'Please fill all of rows on the grid before save!') {

            } else {
                Grid.CancelEdit();
            }
        }


//        if (s.cpType == "0") {
//            //INFO
//            toastr.info(s.cpMessage, 'Information');

//        } else if (s.cpType == "1") {
//            //SUCCESS
//            toastr.success(s.cpMessage, 'Success');

//        } else if (s.cpType == "2") {
//            //WARNING
//            toastr.warning(s.cpMessage, 'Warning');

//        } else if (s.cpType == "3") {
//            //ERROR
//            toastr.error(s.cpMessage, 'Error');
//        }

        window.setTimeout(function () {
            delete s.cpSaveData;
        }, 10);
    }


    //for attachment state
     function LoadCompleted(s, e) {
        if (s.cpActionAfter == "DeleteAtc") {
            GridAtc.PerformCallback();
        }
        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');
        }
     
    }


    function OnFileUploadStart(s, e) {
        btnAddFile.SetEnabled(false);
    }

    function OnTextChanged(s, e) {
        btnAddFile.SetEnabled(s.GetText() != "");
    }

    function OnFileUploadComplete(s, e) {
        btnAddFile.SetEnabled(s.GetText() != "");

        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }

        window.setTimeout(function () {
            delete s.cpType;
        }, 10);
    }


    function OnGetSelectedFieldValues(selectedValues) {
        //VALIDATE WHEN THERE IS NO SELECTED DATA
        if (selectedValues.length == 0) {
            toastr.warning('There is no selected data to delete!', 'Warning');
            return;
        }

        //GET FILE NAME
        var sValues;
        var sValuesSeqNo;
        var resultSeqNo = "";
        var result;
        result = "";

        for (i = 0; i < selectedValues.length; i++) {
            sValues = "";
            sValuesSeqNo = "";
            for (j = 0; j < selectedValues[i].length; j++) {
                sValues = sValues + selectedValues[i][j];
                sValuesSeqNo = sValuesSeqNo + selectedValues[i][j];
            }
            result = result + sValues.split("|", 2).slice(-1)[0] + "|";
            resultSeqNo = resultSeqNo + sValuesSeqNo.split("|", 1)[0] + "|";
        }

        //CONFIRMATION
        var C = confirm("Are you sure want to delete selected attachment (" + result + ") ?");
        if (C == false) {
            return;
        }

        //KEEP FILE NAME INTO HIDDENFIELD
        hf.Set("DeleteAttachment", result);
        hf.Set("SeqNo", resultSeqNo);


        //EXECUTE TO DATABASE AFTER DELAY 300ms
        window.setTimeout(function () {
            cb.PerformCallback("DeleteAttachment");
        }, 300);
    }

    function AddFile() {
        if (Grid.batchEditApi.HasChanges() == true) {
            //there is an update data
            toastr.warning('Please draft the data first before submit!', 'Warning');
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            btnDraft.Focus();
            return;
        }
        cbExist.PerformCallback('IsSubmitted');
        window.setTimeout(function () {
            if (dataSubmitted == "Y") {
                toastr.warning('Cannot add attachment! The data already submitted.', 'Warning');
                return false;

            } else {

                if (btnSubmit.GetEnabled() == false) {
                    toastr.warning('Cannot add attachment! The data already submitted.', 'Warning');
                    return false;
                }

                hf.Set("CPNumber", txtCPNumber.GetText());
                ///alert(txtCPNumber.GetText());
                hf.Set("Rev", txtRev.GetText());

                cbExist.PerformCallback('exist');

                window.setTimeout(function () {
                    if (dataExist == "Y") {
                        ucAtc.Upload();
                        window.setTimeout(function () { GridAtc.PerformCallback(); }, 100);
                    } else {
                        toastr.warning('CP Number does not exist! Please save the data first before add attachment.', 'Warning');
                    }
                }, 1000);

            }
        }, 500);

    }

    function DeleteAttachment() {
        cbExist.PerformCallback('IsSubmitted');
        window.setTimeout(function () {
            if (dataSubmitted == "Y") {
                toastr.warning('Cannot delete attachment! The data already accepted.', 'Warning');
                return;

            } else {

                if (btnSubmit.GetEnabled() == false) {
                    toastr.warning('Cannot delete attachment! The data already accepted.', 'Warning');
                    return;
                }

                //INITIALIZE PARAMETER
                hf.Set("DeleteAttachment", "");
                hf.Set("SeqNo", "");

                GridAtc.GetSelectedFieldValues("Seq_No;Separator;FileName", OnGetSelectedFieldValues);
            }
        }, 100);
    }

    //end attachment


</script>

<style type="text/css">
    .colwidthbutton
    {
        width: 90px;
    }
    
    .rowheight
    {
        height: 35px;
    }
       
    .col1
    {
        width: 10px;
    }
    .colLabel1
    {
        width: 150px;
    }
    .colLabel2
    {
        width: 113px;
    }
    .colInput1
    {
        width: 220px;
    }
    .colInput2
    {
        width: 133px;
    }
    .colSpace
    {
        width: 50px;
    }
        
    .customHeader {
        height: 15px;
    }
     .customHeaderAtc {
        height: 32px;
    } 
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
            <tr class="rowheight">
                <td class="col1">
                    </td>
                <td class="colLabel1">
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Counter Proposal Number"></dx1:aspxlabel>
                </td>
                <td class="colInput1">
                    <dx:ASPxTextBox ID="txtCPNumber" runat="server" width="200px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtCPNumber">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                    </dx:ASPxTextBox>
                </td> 
                <td style="width:35px">
                    <dx:ASPxTextBox ID="txtRev" runat="server" width="30px" Height="25px" Font-Size="9pt" 
                        ClientInstanceName="txtRev" BackColor="#CCCCCC" ReadOnly="true" 
                        HorizontalAlign="Center" TabIndex="-1">
                    </dx:ASPxTextBox>
                </td>
                <td class="colSpace">
                    </td>
                <td class="colLabel2">
                    
                </td>
                <td class="colInput2">
                    
                </td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <tr class="rowheight">
                <td>
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="CE Number"></dx1:aspxlabel>
                </td>
                <td colspan="2">
                    <dx:ASPxTextBox ID="txtCENumber" runat="server" width="200px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtCENumber">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                    </dx:ASPxTextBox>
                </td>
                <td>
                    </td>
                <td> 
                    
                </td>
                <td>
                    
                </td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <%--<tr>
                <td></td>
                <td colspan="8">
                    
                </td>
            </tr>--%>
            <tr>
                <td style="height: 10px">
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td> 
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                    &nbsp;
                    <dx:ASPxCallback ID="cbExist" runat="server" ClientInstanceName="cbExist">
                        <ClientSideEvents 
                            EndCallback="GetMessage" />
                            <%--CallbackComplete="LoadCompleted"--%>
                        
                    </dx:ASPxCallback>
                    &nbsp;
                    <%--callback (cb) for attachment--%>
                    <dx:ASPxCallback ID="cb" runat="server" ClientInstanceName="cb">
                        <ClientSideEvents 
                            Init="GridLoadAtc"
                            EndCallback="LoadCompleted"
                        />
                    </dx:ASPxCallback>
                    
                    &nbsp;
                    <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf">
                    </dx:ASPxHiddenField>
                </td>
            </tr>
        </table>
    </div>
   <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Material_No">            
            <Columns>
                <dx:GridViewDataTextColumn Caption="Material No." FieldName="Material_No"
                     VisibleIndex="0" Width="120px" FixedStyle="Left">
                     <HeaderStyle CssClass="customHeader" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" 
                     VisibleIndex="1" Width="300px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Specification" FieldName="Specification" 
                     VisibleIndex="2" Width="300px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Qty" FieldName="Qty" 
                     VisibleIndex="3" Width="60px">
                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Right"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="UOM" FieldName="UOM" 
                     VisibleIndex="4" Width="50px">
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Remarks" FieldName="Remarks" 
                     VisibleIndex="5" Width="120px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Currency" FieldName="Curr_Code" 
                     VisibleIndex="6" Width="70px">
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewBandColumn Caption="Quotation" Name="Quotation" 
                    VisibleIndex="7">
                    <Columns>
                    <dx:GridViewDataTextColumn Caption="Price/Pcs" FieldName="QuotationPricePcs" 
                     VisibleIndex="0" Width="100px">
                     <PropertiesTextEdit DisplayFormatString="###,##0" Width="95px">
                         <Style HorizontalAlign="Right">
                         </Style>
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Right">
                     </CellStyle>
                </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalQuotationPrice" 
                                VisibleIndex="1" Width="100px">
                                <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                <CellStyle HorizontalAlign="Right"></CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="IAMI Proposal" Name="IAMIProposal" 
                    VisibleIndex="8">
                    <Columns>
                        <dx:GridViewDataSpinEditColumn Caption="Price/Pc" FieldName="ProposalPricePcs" VisibleIndex="0"
                            Width="100px">
                            <PropertiesSpinEdit DisplayFormatString="###,###" Width="95px" MaxLength="18">
                                <Style HorizontalAlign="Right"></Style>
                            </PropertiesSpinEdit>
                        </dx:GridViewDataSpinEditColumn>
                        <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalProposalPrice" VisibleIndex="1"
                            Width="100px">
                            <PropertiesTextEdit DisplayFormatString="###,###">
                            </PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="Best Price" Name="BestPrice" VisibleIndex="9">
                    <Columns>
                        <dx:GridViewDataSpinEditColumn Caption="Price/Pc" FieldName="BestPricePcs" VisibleIndex="0"
                            Width="100px">
                            <PropertiesSpinEdit DisplayFormatString="###,##0" Width="95px" MaxLength="18">
                                <Style HorizontalAlign="Right"></Style>
                            </PropertiesSpinEdit>
                        </dx:GridViewDataSpinEditColumn>
                        <%--<dx:GridViewDataTextColumn Caption="Price/Pcs" FieldName="BestPricePcs" 
                     VisibleIndex="0" Width="100px">
                     <PropertiesTextEdit DisplayFormatString="###,##0" Width="95px">
                         <Style HorizontalAlign="Right">
                         </Style>
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Right">
                     </CellStyle>
                </dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice" VisibleIndex="1"
                            Width="110px">
                            <PropertiesTextEdit DisplayFormatString="###,###">
                            </PropertiesTextEdit>
                            <CellStyle HorizontalAlign="Right">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
            </Columns>

            <Settings HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" VerticalScrollableHeight="135" ShowStatusBar="Hidden" />
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsBehavior AllowSelectByRowClick="True" AllowSort="False" AllowDragDrop="false" ColumnResizeMode="Control" />
            <SettingsEditing Mode="Batch">
                <BatchEditSettings StartEditAction="Click" ShowConfirmOnLosingChanges="False" />
            </SettingsEditing>

            <ClientSideEvents 
                Init="LoadDataGrid"
                BatchEditStartEditing="OnBatchEditStartEditing"
                BatchEditEndEditing="OnBatchEditEndEditing" 
            />

            <Styles Header-Paddings-Padding="1px" EditFormColumnCaption-Paddings-PaddingLeft="0px" EditFormColumnCaption-Paddings-PaddingRight="0px" >
                <Header CssClass="customHeader" HorizontalAlign="Center"></Header>
            </Styles>
        </dx:ASPxGridView>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
            <tr style="height:20px">
                <td></td>
                <td><dx1:aspxlabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Attachment :"></dx1:aspxlabel></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <dx:ASPxGridView ID="GridAtc" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridAtc"
                        EnableTheming="True" Theme="Office2010Black" Width="100%" Settings-VerticalScrollableHeight="80" Settings-VerticalScrollBarMode="Visible"
                        Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="FileName;FilePath" >
            
                        <Columns>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Caption=" " Width="30px"></dx:GridViewCommandColumn>
                            <%--<dx:GridViewDataCheckColumn Caption=" " Name="chk" VisibleIndex="0" Width="30px">
                                <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueType="System.Int32" >
                                </PropertiesCheckEdit>
                            </dx:GridViewDataCheckColumn>--%>
                            <dx:GridViewDataTextColumn Caption="Sequence No" FieldName="Seq_No" Visible ="false"
                                 VisibleIndex="1" Width="80px">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="File Type" FieldName="FileType"
                                 VisibleIndex="2" Width="80px">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="File Name" FieldName="FileName" 
                                 VisibleIndex="3" Width="500px">
                                 <DataItemTemplate>
                                   <dx:ASPxHyperLink ID="linkFileName" runat="server" OnInit="keyFieldLink_Init">                                    
                                    </dx:ASPxHyperLink>
                                </DataItemTemplate>  					
                            </dx:GridViewDataTextColumn>                            
							 <dx:GridViewDataTextColumn Caption="File Size" FieldName="FileSize" Visible="true"
                                 VisibleIndex="4" Width="80px">
                            </dx:GridViewDataTextColumn>       
                            <dx:GridViewDataTextColumn Caption="Separator" FieldName="Separator" Visible ="false"
                                 VisibleIndex="5" Width="80px">
                            </dx:GridViewDataTextColumn>  																					 
                         </Columns>
                         <SettingsEditing Mode="Batch">
                            <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                        </SettingsEditing>

                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSort="False" EnableRowHotTrack="True" />
                         
                        <Settings VerticalScrollableHeight="60" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" ShowStatusBar="Hidden"></Settings>
                        <Styles>
                            <Header CssClass="customHeaderAtc" HorizontalAlign="Center">
                            </Header>
                        </Styles>
                        <ClientSideEvents 
                            CallbackError="function(s, e) {e.handled = true;}"
                            EndCallback="LoadCompleted" 
                            BatchEditStartEditing="OnBatchEditStartEditing"
                        />
                    </dx:ASPxGridView>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="rowheight">
                    <dx:ASPxButton ID="btnDeleteAtc" runat="server" Text="Delete Selected Attachment" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="150px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDeleteAtc" Theme="Default">                        
                        <ClientSideEvents Click="DeleteAttachment" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <table width="100%">
                        <tr>
                            <td>
                                <dx:ASPxUploadControl ID="ucAtc" runat="server" ClientInstanceName="ucAtc"
                                    Width="50%" Height="30px" ShowProgressPanel="false">
                                    <ValidationSettings AllowedFileExtensions=".pdf" 
                                        NotAllowedFileExtensionErrorText="The uploaded file format isn't allowed!" >
                                    </ValidationSettings>
                                    <BrowseButton Text="Browse..."></BrowseButton>
                                    <ClientSideEvents
                                        FileUploadStart="OnFileUploadStart"
                                        FilesUploadComplete="OnFileUploadComplete"
                                        TextChanged="OnTextChanged"
                                    />
                                </dx:ASPxUploadControl>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                    Text="*Available file format : pdf" Font-Italic="True">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                    Text="Maximum Size : Total 2 MB" Font-Italic="True">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnAddFile" runat="server" Text="Add File" 
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnAddFile" Theme="Default">                        
                                    <ClientSideEvents 
                                        Click="AddFile" 
                                    />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="col1"></td>
                <td colspan="8">
                    <dx1:aspxlabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Note :"></dx1:aspxlabel>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <dx:ASPxMemo ID="memoNote" runat="server" Height="45px" Width="100%" ClientInstanceName="memoNote" MaxLength="300" ReadOnly="true" BackColor="#CCCCCC">
                        <ClientSideEvents KeyDown="CountingLength" LostFocus="CountingLength" />
                    </dx:ASPxMemo>
                </td>
                <td class="col1"></td>
            </tr>
             <tr>
                <td></td>
                <td colspan="7" align="right" style="display:none" >
                    <dx1:aspxlabel ID="lblLenght" runat="server" Font-Names="Segoe UI" Font-Size="9pt" ClientInstanceName="lblLenght" Text="0/300" Visible="true"></dx1:aspxlabel>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="col1"></td>
                <td colspan="8">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Supplier Note :"></dx1:aspxlabel>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <dx:ASPxMemo ID="suppNote" runat="server" Height="45px" Width="100%" ClientInstanceName="suppNote" MaxLength="300"  >
                        <ClientSideEvents KeyDown="CountingLength" LostFocus="CountingLength"/>
                    </dx:ASPxMemo>
                </td>
                <td class="col1"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7" align="right" style="display:none">
                    <dx1:aspxlabel ID="lblLenght2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" ClientInstanceName="lblLenght2" Text="0/300" Visible="true"></dx1:aspxlabel>
                </td>
                <td></td>
            </tr>
            
            <tr style="height:20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            
            <tr>
                <td></td>
                <td colspan="7">
                    <table>
                        <tr class="rowheight">
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                                    ClientInstanceName="btnBack" Theme="Default">
                                    <Paddings Padding="2px" />
                                    <ClientSideEvents Click="Back" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnDraft" runat="server" Text="Draft" 
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnDraft" Theme="Default">                        
                                    <ClientSideEvents Click="Draft" />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnSubmit" Theme="Default">
                                    <ClientSideEvents Click="Submit" />                        
                                    <Paddings Padding="2px" />                                    
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
        </table>
    </div>
</div>
</asp:Content>
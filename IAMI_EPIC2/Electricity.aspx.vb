﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Drawing

Public Class Electricity
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A080")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            Period_From.Value = Now
            Period_To.Value = Now
            cboRateClass.SelectedIndex = 0
            FillCombo("ALL")
            cboVAKind.SelectedIndex = 0
            'up_GridLoad(Format(Period_From.Value, "MMyyyy"), Format(Period_To.Value, "MMyyyy"), cboRateClass.Value, cboVAKind.Value)

            Dim Script As String
            Script = "btnDownload.SetEnabled(false);"
            ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)

        End If
    End Sub
#End Region

#Region "Control Event"

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad(Format(Period_From.Value, "yyyyMM"), Format(Period_To.Value, "yyyyMM"), cboRateClass.Value, cboRateClass.Value)
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "load"
                Dim pFrom As String = Split(e.Parameters, "|")(1)
                Dim pTo As String = Split(e.Parameters, "|")(2)
                Dim RateClass As String = Split(e.Parameters, "|")(3)
                Dim VAKind As String = Split(e.Parameters, "|")(4)
                If Format(Period_From.Value, "yyyyMM") > Format(Period_To.Value, "yyyyMM") Then
                    show_error(MsgTypeEnum.Warning, "Period From(" & Format(Period_From.Value, "MMM yyyy") & ") Cannot more than Period To " & Format(Period_To.Value, "MMM yyyy"), 1, Grid)
                Else
                    up_GridLoad(Format(Period_From.Value, "yyyyMM"), Format(Period_To.Value, "yyyyMM"), RateClass, VAKind)
                End If

                If Grid.VisibleRowCount > 0 Then
                    'If ds.Tables(0).Rows.Count > 0 Then
                    Grid.JSProperties("cp_disabled") = "N"
                    'Script = "btnDownload.SetEnabled(true);"
                Else
                    Grid.JSProperties("cp_disabled") = "Y"
                    'show_error(MsgTypeEnum.Info, "There is no data to show!", 1)
                    show_error(MsgTypeEnum.Info, "There is no data to show!", 1, Grid)

                    'Grid.JSProperties("cp_Message") = "There is no data to show!"
                End If
                fRefresh = True
            Case "delete "
                Dim pErr As String = ""
                ClsElectricityDB.Delete(Split(e.Parameters, "|")(1), Split(e.Parameters, "|")(2), Format(CDate(Split(e.Parameters, "|")(3)), "yyyyMM"), pUser, pErr)
                If pErr = "" Then
                    show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, Grid)
                    up_GridLoad(Format(Period_From.Value, "yyyyMM"), Format(Period_To.Value, "yyyyMM"), Split(e.Parameters, "|")(1), Split(e.Parameters, "|")(2))
                Else
                    show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
                End If
        End Select
    End Sub

    Private Sub up_Excel()
        up_GridLoad(Format(Period_From.Value, "yyyyMM"), Format(Period_To.Value, "yyyyMM"), cboRateClass.Value, cboVAKind.Value)

        If Grid.VisibleRowCount < 1 Then
            Exit Sub
        End If

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=Electricity" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub
    Protected Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        ClsElectricityDB.Delete(e.Keys("RateClass"), e.Keys("VAKind"), e.Keys("Period"), pUser, pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, Grid)
            up_GridLoad(Format(Period_From.Value, "yyyyMM"), Format(Period_To.Value, "yyyyMM"), e.Keys("RateClass"), e.Keys("VAKind"))
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
        End If
    End Sub

    Private Sub cboVAKind_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboVAKind.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim RateClass As String = Split(e.Parameter, "|")(0)

        ds = ClsElectricityDB.getVAKind(RateClass, pmsg)
        If pmsg = "" Then
            cboVAKind.DataSource = ds
            cboVAKind.DataBind()
            cboVAKind.SelectedIndex = 0
        End If
    End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("ElectricityDetail.aspx")
    End Sub
#End Region

#Region "Procedure"

    Private Sub FillCombo(ByVal RateClass As String)
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsElectricityDB.getVAKind(RateClass, pmsg)
        If pmsg = "" Then
            cboVAKind.DataSource = ds
            cboVAKind.DataBind()
        End If

    End Sub
    Private Sub up_GridLoad(ByVal pFrom As String, ByVal pTo As String, ByVal RateClass As String, ByVal VaKind As String)
        Dim ErrMsg As String = ""
        Dim Ses As New DataSet
        Ses = ClsElectricityDB.getlistds(pFrom, pTo, RateClass, VaKind, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub

    Protected Sub btnChart_Click(sender As Object, e As EventArgs) Handles btnChart.Click
        Response.Redirect("ElectricityChart.aspx")
    End Sub
#End Region


 
End Class
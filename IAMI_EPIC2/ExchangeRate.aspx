﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ExchangeRate.aspx.vb" Inherits="IAMI_EPIC2.ExchangeRate" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function Message(paramType, paramMessage) {
            
            if (paramType == 0) { //Info
                toastr.info(paramMessage, 'Info');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 1) { //Success
                toastr.success(paramMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 2) { //Warning
                toastr.warning(paramMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 3) { //Error
                toastr.error(paramMessage, 'Error');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }        
        }

        function formatDateISO(date) {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        // AddDay function (format yyyy-MM-dd)
        function AddDay(strDate, intNum) {
            sdate = new Date(strDate);
            sdate.setDate(sdate.getDate() + intNum);
            return formatDateISO(sdate);
        }

        // AddMonth function (format yyyy-MM-dd)
        function AddMonth(strDate, intNum) {
            sdate = new Date(strDate);
            sdate.setMonth(sdate.getMonth() + intNum);
            return formatDateISO(sdate);
        }

        // Addyear function (format yyyy-MM-dd)
        function AddYear(strDate, intNum) {
            sdate = new Date(strDate);
            sdate.setFullYear(sdate.getFullYear() + intNum);
            return formatDateISO(sdate);
        }

        function dtDateFrom_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            dtDateFrom.SetDate(myDate);
        }

        function dtDateFrom_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();

                var startDate = formatDateISO(dtDateFrom.GetDate());
                var endDate = formatDateISO(dtDateTo.GetDate());

                if (startDate > endDate) {
                    Message(2, 'Exchange rate date from must be lower than exchange rate date to!');
                    dtDateFrom.Focus();
                    return false;
                }
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }
        }
    
        function dtDateTo_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            dtDateTo.SetDate(myDate);
        }

        function dtDateTo_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();

                var startDate = formatDateISO(dtDateFrom.GetDate());
                var endDate = formatDateISO(dtDateTo.GetDate());

                if (startDate > endDate) {
                    Message(2, 'Exchange rate date from must be lower than exchange rate date to!');
                    dtDateFrom.Focus();
                    return false;
                }
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }
        }

        function cboBankCode_Init(s, e) {
            cboBankCode.SetValue('ALL');
            txtBankDesc.SetText('ALL');
            txtBankDesc.SetEnabled(false);
        }

        function cboBankCode_SelectedIndexChanged(s, e) {
            var desc = s.GetSelectedItem().GetColumnText('Description');
            txtBankDesc.SetText(desc);
        }

        function cboBankCode_LostFocus(s, e) {
            var id = s.GetValue();
            if (id == null) {
                txtBankDesc.SetText('');
            }
        }

        function cboCurrencyCode_Init(s, e) {
            cboCurrencyCode.SetValue('ALL');
        }

        function btnRefresh_Click(s, e) {
            var startDate = formatDateISO(dtDateFrom.GetDate());
            var endDate = formatDateISO(dtDateTo.GetDate());

            if (startDate > endDate) {
                Message(2, 'Exchange rate date from must be lower than exchange rate date to!');
                dtDateFrom.Focus();
                e.processOnServer = false;
                return;
            } else if (cboBankCode.GetSelectedIndex() < 0) {
                Message(2, 'Please select Bank Code!');
                cboBankCode.Focus();
                e.processOnServer = false;
                return;
            } else if (cboCurrencyCode.GetSelectedIndex() < 0) {
                Message(2, 'Please select Currency Code!');
                cboCurrencyCode.Focus();
                e.processOnServer = false;
                return;
            }

            var ExchangeDateFrom = formatDateISO(dtDateFrom.GetDate());
            var ExchangeDateTo = AddDay(AddMonth(dtDateTo.GetDate(), 1), -1);

            Grid.PerformCallback('load|' + ExchangeDateFrom + '|' + ExchangeDateTo + '|' + cboBankCode.GetSelectedItem().GetColumnText(0) + '|' + cboCurrencyCode.GetSelectedItem().GetColumnText(0));
	    }

	    function Grid_CustomButtonClick(s, e) {
	        if (e.buttonID == 'edit') {
	            var rowKey = Grid.GetRowKey(e.visibleIndex);
	            var splitString = rowKey.split("|");
	            var bankCode = splitString[0].trim();
	            var currencyCode = splitString[1].trim();
	            var date = splitString[2].trim();
	            window.location.href = 'AddExchangeRate.aspx?action=edit&bankcode=' + bankCode + '&currencycode=' + currencyCode + '&date=' + date;
	        } else if (e.buttonID == 'delete') {
	            var msg = confirm('Are you sure want to delete this data ?');
	            if (msg == false) {
	                e.processOnServer = false;
	                return;
	            }

	            var rowKey = Grid.GetRowKey(e.visibleIndex);
	            var splitString = rowKey.split("|");
	            var bankCode = splitString[0].trim();
	            var currencyCode = splitString[1].trim();
	            var date = splitString[2].trim();
	            cbAction.PerformCallback('delete|' + bankCode + '|' + currencyCode + '|' + date);
            }
	    }

	    function Grid_EndCallback(s, e) {
	        if (s.cp_disabled == "N") {
	            //alert(s.cp_disabled);
	            btnDownload.SetEnabled(true);
	        } else if (s.cp_disabled == "Y") {
	            //alert(s.cp_disabled);
	            btnDownload.SetEnabled(false);

	            toastr.info("There's no data to show!", 'Info');
	            toastr.options.closeButton = false;
	            toastr.options.debug = false;
	            toastr.options.newestOnTop = false;
	            toastr.options.progressBar = false;
	            toastr.options.preventDuplicates = true;
	            toastr.options.onclick = null;
	            e.processOnServer = false;
	            return;
	        }


	        if (s.cp_message != '') {
	            Message(s.cp_type, s.cp_message);
	            s.cp_message = '';
            }
	    }

	    function cbAction_CallbackComplete(s, e) {
	        var result = e.result;
	        if (result == 'delete') {
	            Message(s.cp_type, s.cp_message);
	            if (s.cp_type == 1) {
	                var ExchangeDateFrom = formatDateISO(dtDateFrom.GetDate());
	                var ExchangeDateTo = AddDay(AddMonth(dtDateTo.GetDate(), 1), -1);

	                Grid.PerformCallback("load|" + ExchangeDateFrom + "|" + ExchangeDateTo + "|" + cboBankCode.GetSelectedItem().GetColumnText(0) + "|" + cboCurrencyCode.GetSelectedItem().GetColumnText(0));
	            }
	        }
	    }

	    
</script>

<style type="text/css">        
    .table-col-title01
    {
        width: 160px;                     
    }
    .table-col-control01
    {
        width: 110px;        
    }   
    .table-col-control02
    {
        width: 250px; 
    }   
    .table-col-title02
    {
        width: 180px;
    }   
    .table-col-control03
    {
        width: 180px;
    }   
    .table-height
    {
        height: 35px      
    }    
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

<div style="padding: 5px 5px 5px 5px">    
    <table style="width: 100%;border: 1px solid black">  
        <tr style="height:15px">
            <td class="table-col-title01">&nbsp;</td>       <%-- Title 1 --%>
            <td class="table-col-control01">&nbsp;</td>     <%-- Control 1 --%>
            <td class="table-col-control02">&nbsp;</td>     <%-- Control 2 --%>
            <td class="table-col-title02">&nbsp;</td>       <%-- Title 2 --%>
            <td class="table-col-control03">
                <dx:ASPxCallback ID="cbAction" runat="server" ClientInstanceName="cbAction">
                    <ClientSideEvents CallbackComplete="cbAction_CallbackComplete" />
                </dx:ASPxCallback>
            </td>     <%-- Control 1 --%>
            <td><dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid" /></td>
        </tr>                 
        <tr class="table-height">
            <td class="table-col-title01">            
                <dx:ASPxLabel ID="lblDateFrom" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Exchange Rate Date From" style="padding-left:10px" />
            </td>
            <td class="table-col-control01">      
                <dx:ASPxDateEdit ID="dtDateFrom" runat="server" Theme="Office2010Black" 
                    Width="100px" AutoPostBack="false" ClientInstanceName="dtDateFrom"
                    EditFormatString="MMM-yyyy" DisplayFormatString="MMM-yyyy" EnableTheming="True" ShowShadow="false"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                    </CalendarProperties>
                    <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>

                    <ClientSideEvents DropDown="dtDateFrom_OnDropDown" Init="dtDateFrom_OnInit" />
                </dx:ASPxDateEdit>                      
            </td>
            <td class="table-col-control02">&nbsp;</td>
            <td class="table-col-title02">                    
                <dx:ASPxLabel ID="lblDateTo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Exchange Rate Date To" style="padding-left:10px" />                    
            </td>
            <td class="table-col-control03">           
                <dx:ASPxDateEdit ID="dtDateTo" runat="server" Theme="Office2010Black" 
                    Width="100px" AutoPostBack="false" ClientInstanceName="dtDateTo"
                    EditFormatString="MMM-yyyy" DisplayFormatString="MMM-yyyy" EnableTheming="True" ShowShadow="false"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                    </CalendarProperties>
                    <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>

                    <ClientSideEvents DropDown="dtDateTo_OnDropDown" Init="dtDateTo_OnInit" />
                </dx:ASPxDateEdit>          
            </td>
            <td>
                <asp:SqlDataSource ID="ds_Bank" runat="server"
                    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"                         
                    SelectCommand=" select 'ALL' As Code, 'ALL' As Description union all select rtrim(Par_Code) as Code, rtrim(Par_Description) as Description from Mst_Parameter where Par_Group = 'Bank' ">
                </asp:SqlDataSource>
            </td>                
        </tr>          
        <tr class="table-height">
            <td class="table-col-title01">            
                <dx:ASPxLabel ID="lblBankCode" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Bank Code" style="padding-left:10px" />
            </td>
            <td class="table-col-control01">
                <dx:ASPxComboBox ID="cboBankCode" runat="server" ClientInstanceName="cboBankCode"
                        Width="100px" Font-Names="Segoe UI" TextField="Description" 
                        ValueField="Code" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" DataSourceID="ds_Bank"
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">
                           
                        <Columns>
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />                             
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>

                        <ClientSideEvents Init="cboBankCode_Init" SelectedIndexChanged="cboBankCode_SelectedIndexChanged" LostFocus="cboBankCode_LostFocus" />
                </dx:ASPxComboBox>
            </td>
            <td class="table-col-control02">
                <dx:ASPxTextBox runat="server" ID="txtBankDesc" 
                    ClientInstanceName="txtBankDesc" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="230px" style="height:25px" />
            </td>
            <td class="table-col-title01">
                <dx:ASPxLabel ID="lblCurrencyCode" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Currency Code" style="padding-left:10px" />
            </td>
            <td class="table-col-control03">
                <dx:ASPxComboBox ID="cboCurrencyCode" runat="server" ClientInstanceName="cboCurrencyCode"
                    Width="100px" Font-Names="Segoe UI" TextField="Description"
                    ValueField="Code" TextFormatString="{0}" Font-Size="9pt" 
                    Theme="Office2010Black" DropDownStyle="DropDown" DataSourceID="ds_Currency"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                    Height="25px">
                    <ClientSideEvents Init="cboCurrencyCode_Init" />
                    <Columns>
                    <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                    <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />                                                      
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px" >
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px" >
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>                                        
            </td>                
            <td class="table-height">
                <asp:SqlDataSource ID="ds_Currency" runat="server"
                    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"                         
                    SelectCommand=" select 'ALL' As Code, 'ALL' Description union all select rtrim(Par_Code) as Code, rtrim(Par_Description) as Description from Mst_Parameter where Par_Group = 'Currency' ">
                </asp:SqlDataSource>
            </td>                
        </tr>               
        
        <tr style="height:60px">
            <td style=" padding:0px 0px 0px 10px" colspan="6">            
                <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnRefresh" Theme="Default">                        
                    <ClientSideEvents Click="btnRefresh_Click" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>&nbsp;
                <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnDownload" Theme="Default">                                         
                    <Paddings Padding="2px" />
                </dx:ASPxButton>&nbsp;
                <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="false"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                    ClientInstanceName="btnAdd" Theme="Default" >                        
                    <Paddings Padding="2px" />
                </dx:ASPxButton>&nbsp;
                <dx:ASPxButton ID="btnChart" runat="server" Text="Chart" UseSubmitBehavior="false"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnChart"
                            Theme="Default">
                            <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
        </tr>                 
    </table>
</div>
<div style="padding: 5px 5px 5px 5px">
    <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid" Width="100%"
        EnableTheming="True" KeyFieldName="BankCode;Currency;ExchangeRateDate" Theme="Office2010Black" Font-Size="9pt" Font-Names="Segoe UI" 
        OnAfterPerformCallback="Grid_AfterPerformCallback" >            
        <ClientSideEvents EndCallback="Grid_EndCallback" CustomButtonClick="Grid_CustomButtonClick" />                                        
        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0" ButtonType="Link" Caption=" " >
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="edit" Text="Edit" />
                    <dx:GridViewCommandColumnCustomButton ID="delete" Text="Delete" />
                </CustomButtons>
                </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="BankCode" Caption="Bank Code"
                VisibleIndex="1" Width="100px" Settings-AutoFilterCondition="Contains"  Visible="false">
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="BankName" Caption="Bank Name"
                VisibleIndex="2" Width="300px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Currency" Caption="Currency Code"
                VisibleIndex="3" Width="120px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="CurrencyName" Caption="Currency Name"
                VisibleIndex="4" Width="120px" Settings-AutoFilterCondition="Contains"  Visible="false">
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ExchangeRateDate" Caption="Exchange Rate Date"
                VisibleIndex="5" Width="120px" Settings-AutoFilterCondition="Contains" >
                <PropertiesTextEdit DisplayFormatString="dd-MMM-yyyy" />
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="BuyingRate" Caption="Buying Rate"
                VisibleIndex="6" Width="100px" Settings-AutoFilterCondition="Contains" >
                <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MiddleRate" Caption="Middle Rate"
                VisibleIndex="7" Width="100px" Settings-AutoFilterCondition="Contains" >
                <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SellingRate" Caption="Selling Rate"
                VisibleIndex="8" Width="100px" Settings-AutoFilterCondition="Contains" >
                <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Remarks" Caption="Remarks"
                VisibleIndex="9" Width="250px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Register_By" Caption="Register By"
                VisibleIndex="10" Width="120px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Register_Date" Caption="Register Date"
                VisibleIndex="11" Width="130px" Settings-AutoFilterCondition="Contains" >
                <PropertiesTextEdit DisplayFormatString="dd MMM yyyy hh:mm:ss" />
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Update_By" Caption="Update By"
                VisibleIndex="12" Width="120px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Update_Date" Caption="Update Date"
                VisibleIndex="13" Width="130px" Settings-AutoFilterCondition="Contains" >
                <PropertiesTextEdit DisplayFormatString="dd MMM yyyy hh:mm:ss" />
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior ColumnResizeMode="Control" />
        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270" HorizontalScrollBarMode="Auto" />
        <Styles>
            <Header>
                <Paddings Padding="2px"></Paddings>
            </Header>
        </Styles>
    </dx:ASPxGridView> 
</div>

</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="IAPrice.aspx.vb" Inherits="IAMI_EPIC2.IAPrice" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

    function MessageBox(s, e) {

    }

    function downloadValidation(s, e) {
        if (Grid.GetVisibleRowsOnPage() == 0) {
            toastr.info('There is no data to download!', 'Info');
            e.processOnServer = false;
            return;
        }
    }
    
   

</script>

<style type="text/css">
    .hidden-div
        {
            display:none;
        }
</style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
            <tr style="height: 20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>      
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="IA Price Date">
                    </dx1:aspxlabel>                       
                </td>
                <td></td>
                <td style="width:120px">
                <dx:aspxdateedit ID="dtDateFrom" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateFrom"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:aspxdateedit>                       </td>
                <td style="width:30px">   
                    
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="To">
                    </dx1:aspxlabel>   
                                                     
                </td>
                <td>
                <dx:aspxdateedit ID="dtDateTo" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateTo"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                    </dx:aspxdateedit>   
                    
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>   
            <tr style="height: 40px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="IA Price Number">
                    </dx1:aspxlabel>                       
                    
                </td>
                <td></td>
                <td colspan="3">           
  <dx1:ASPxComboBox ID="cboIAPriceNo" runat="server" ClientInstanceName="cboIAPriceNo"
                            Width="200px" Font-Names="Segoe UI"  TextField="IA_Number"
                            ValueField="IA_Number" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="IA Number" FieldName="IA_Number" Width="100px" />
                       
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>   
            <tr style="height: 20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>   
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; " colspan="7">
                
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnRefresh" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {                          
                                                            
	                            Grid.PerformCallback('gridload|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|' + cboIAPriceNo.GetText());
                
                      
                            }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                     &nbsp;<dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnExcel" Theme="Default">                        
                        <ClientSideEvents Click="downloadValidation" />
                                    
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 &nbsp;<dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnAdd" Theme="Default">                        
                         <ClientSideEvents Click="function(s, e) {
	cbMessage.PerformCallback('');
}" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                
                </td>
                <td></td>
                <td></td>
            </tr>      
            <tr style="height: 20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>              
                     <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                     </dx:ASPxCallback>
                     <dx:ASPxCallback ID="cbMessage" runat="server" 
                        ClientInstanceName="cbMessage">
                        <ClientSideEvents CallbackComplete="MessageBox" />                                           
                     </dx:ASPxCallback>
                     <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                     </dx:ASPxGridViewExporter>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>   
            <tr>
                <td></td>
                <td colspan="4" style="display:none">
                    <dx1:ASPxTextBox ID="txtBack" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="50px" ClientInstanceName="txtBack" MaxLength="30" 
                        Height="25px" >
            </dx1:ASPxTextBox>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
                                                     
        </table>
    </div>

    <div style="padding: 5px 5px 5px 5px">
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" Width="100%" 
                Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="IA_Number;Revision_No;CE_Number" >

             <ClientSideEvents CustomButtonClick="function(s, e) {
		                       if(e.buttonID == 'edit'){                     
                                        var rowKey = Grid.GetRowKey(e.visibleIndex);  
                                                            
	                                    window.location.href= 'IAPriceDetail.aspx?ID=' + rowKey;
                                    }
                                }" />

             <Columns>
                 <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" FixedStyle="Left">
                     <CustomButtons>
                         <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                         </dx:GridViewCommandColumnCustomButton>
                     </CustomButtons>
                 </dx:GridViewCommandColumn>
                 <dx:GridViewDataTextColumn Caption="IA Price Number" VisibleIndex="1" FixedStyle="Left"
                     FieldName="IA_Number" Width="180px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="IA Price Date" VisibleIndex="2" FixedStyle="Left"
                     FieldName="IA_Date" Width="100px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Revision No" FieldName="Revision_No" FixedStyle="Left"
                     VisibleIndex="3" Width="100px">
                 </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="4" 
                     FieldName="IAStatusDescs" Width="100px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CE Number" VisibleIndex="5" 
                     FieldName="CE_Number" Width="180px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CE Date" VisibleIndex="6" 
                     FieldName="CE_Date" Width="100px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="PR Number" VisibleIndex="7" 
                     FieldName="PR_Number" Width="180px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="PR Date" VisibleIndex="8" 
                     FieldName="PR_Date" Width="100px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CP Number" VisibleIndex="9" 
                     FieldName="CP_Number" Width="0px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CP Date" VisibleIndex="10" 
                     FieldName="CP_Date" Width="0px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                 </dx:GridViewDataTextColumn>
                  <dx:GridViewBandColumn Caption="Prepared"  VisibleIndex="11">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Prepared By" FieldName="Register_By" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Prepared Date" FieldName="Register_Date" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn>
                 
                 
                <dx:GridViewBandColumn VisibleIndex="12" Caption="General Item Section Head">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="General_Item_Section_Head_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="General_Item_Section_Head_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="General_Item_Section_Head_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>

                  <dx:GridViewBandColumn VisibleIndex="13" Caption="Cost Control Department Head">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="Cost_Control_Department_Head_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="Cost_Control_Department_Head_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="Cost_Control_Department_Head_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>

                  <dx:GridViewBandColumn VisibleIndex="14" Caption="Purchasing Advisor">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="Purchasing_Advisor_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="Purchasing_Advisor_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="Purchasing_Advisor_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>

                  <dx:GridViewBandColumn VisibleIndex="15" Caption="Purchasing Division Head">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="Purchasing_Division_Head_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="Purchasing_Division_Head_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="Purchasing_Division_Head_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>

                  <dx:GridViewBandColumn VisibleIndex="16" Caption="Finance Division Head">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="Finance_Division_Head_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="Finance_Division_Head_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="Finance_Division_Head_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>
                 
             </Columns>

                    <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
             
                    <Settings ShowFilterRow="True" VerticalScrollableHeight="300" 
                HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>

         
                 <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
</EditFormColumnCaption>
                 </Styles>


         </dx:ASPxGridView>
    </div>

</div>
</asp:Content>

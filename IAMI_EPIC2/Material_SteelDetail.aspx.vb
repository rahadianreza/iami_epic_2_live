﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Drawing

Public Class Material_SteelDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim pID As String
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A160")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            pID = Request.QueryString("ID")
            If pID <> "" Then
                Dim pError As String = ""
                Dim ds As New DataSet

                cboType.Value = Split(pID, "|")(0)
                FillComboSupplier(Split(pID, "|")(0))
                Period.Value = CDate(Split(pID, "|")(1))
                cboMaterialCode.Value = Split(pID, "|")(2)
                cboSupplier.Value = Split(pID, "|")(3)
                ds = ClsMaterial_SteelDB.getlistdsDetail(Format(CDate(Split(pID, "|")(1)), "yyyyMM"), Split(pID, "|")(3), Split(pID, "|")(0), Split(pID, "|")(2), pError)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtPrice.Text = ds.Tables(0).Rows(0)("Price")
                    cboGroupItem.Value = ds.Tables(0).Rows(0)("GroupItemCode")
                    cboCurr.Value = ds.Tables(0).Rows(0)("CurrencyCode")
                    cboCategory.Value = ds.Tables(0).Rows(0)("CategoryCode")
                End If

                cboType.Enabled = False
                cboSupplier.Enabled = False
                cboMaterialCode.Enabled = False
                Period.Enabled = False
                btnClear.Enabled = False
            Else
                Period.Value = Now
            End If
        End If
    End Sub
#End Region

#Region "Control Event"

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub cboSupplier_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSupplier.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Type As String = Split(e.Parameter, "|")(0)

        ds = ClsMaterial_SteelDB.GetSupplier(Type, pmsg)
        If pmsg = "" Then
            cboSupplier.DataSource = ds
            cboSupplier.DataBind()

        End If
    End Sub

    Private Sub cboGroupItem_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupItem.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim MaterialCode As String = Split(e.Parameter, "|")(0)

        ds = ClsMaterial_SteelDB.GetGroupItem(MaterialCode, pmsg)
        If pmsg = "" Then
            cboGroupItem.DataSource = ds
            cboGroupItem.DataBind()

        End If
    End Sub

    Private Sub cboCategory_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCategory.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim GroupItem As String = Split(e.Parameter, "|")(0)

        ds = ClsMaterial_SteelDB.GetCategory(GroupItem, pmsg)
        If pmsg = "" Then
            cboCategory.DataSource = ds
            cboCategory.DataBind()

        End If
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim pError As String = ""
        If validation() = False Then
            Exit Sub
        End If

        Try

            If ClsMaterial_SteelDB.Insert(cboType.Text, cboSupplier.Value, Format(Period.Value, "yyyyMM"), cboMaterialCode.Value, cboGroupItem.Value, cboCategory.Value, cboCurr.Value, txtPrice.Text, 2, pUser, pError) = 1 Then
                If pError = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                Else
                    cbSave.JSProperties("cpMessage") = pError
                End If
            Else
                ClsMaterial_SteelDB.Insert(cboType.Text, cboSupplier.Value, Format(Period.Value, "yyyyMM"), cboMaterialCode.Value, cboGroupItem.Value, cboCategory.Value, cboCurr.Value, txtPrice.Text, 1, pUser, pError)
                If pError = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
                Else
                    cbSave.JSProperties("cpMessage") = pError
                End If
            End If

        Catch ex As Exception
            cbSave.JSProperties("cpMessage") = ex.Message
        End Try
    End Sub
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Material_Steel.aspx")
    End Sub
#End Region

#Region "Procedure"
    Public Function validation() As Boolean
        If cboType.Text = "" Then
            cbSave.JSProperties("cpMessage") = "Please Select Material Type"
            Return False
        ElseIf cboSupplier.Text = "" Then
            cbSave.JSProperties("cpMessage") = "Please Select Supplier"
            Return False
        ElseIf Period.Text = "" Then
            cbSave.JSProperties("cpMessage") = "Please Select Period"
            Return False
        ElseIf cboMaterialCode.Text = "" Then
            cbSave.JSProperties("cpMessage") = "Please Select Material Code"
            Return False
        ElseIf cboGroupItem.Text = "" Then
            cbSave.JSProperties("cpMessage") = "Please Select Group Item"
            Return False
        ElseIf cboCategory.Text = "" Then
            cbSave.JSProperties("cpMessage") = "Please Select Category"
            Return False
        ElseIf cboCurr.Text = "" Then
            cbSave.JSProperties("cpMessage") = "Please Select Currency"
            Return False
        ElseIf txtPrice.Text = 0 Then
            cbSave.JSProperties("cpMessage") = "Price cannot 0"
            Return False
        Else
            Return True
        End If
    End Function
    Private Sub FillComboSupplier(ByVal MaterialType As String)
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsMaterial_SteelDB.GetSupplier(MaterialType, pmsg)
        If pmsg = "" Then
            cboSupplier.DataSource = ds
            cboSupplier.DataBind()
        End If


    End Sub

    Private Sub FillComboGroupItem(ByVal MaterialCode As String)
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsMaterial_SteelDB.GetGroupItem(MaterialCode, pmsg)
        If pmsg = "" Then
            cboSupplier.DataSource = ds
            cboSupplier.DataBind()
        End If


    End Sub

#End Region



End Class
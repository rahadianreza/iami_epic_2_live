﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PRApprovalDetail.aspx.vb" Inherits="IAMI_EPIC2.PRApprovalDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

    function OnLoad(s, e) {
        //if (s.cpUrgent == '0') {
            txtUrgentNote.SetEnabled(false);
            dtRequestPO.SetEnabled(false);
            chkUrgent.SetEnabled(false);
        //}
        //else {
            //txtUrgentNote.SetEnabled(true);
            //dtRequestPO.SetEnabled(true);
        //}
    }

    function MessageBox(s, e) {
        if (s.cpMessage == "Data Has Been Approved Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            btnApprove.SetEnabled(false);
            btnReject.SetEnabled(false);
            txtApprovalNote.SetEnabled(false);
        }
        else if (s.cpMessage == "Data Has Been Reject Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            btnApprove.SetEnabled(false);
            btnReject.SetEnabled(false);
            txtApprovalNote.SetEnabled(false);
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

        
    }

    function tes(s, e) {        
        if (chkUrgent.GetChecked() == (true)) {
            txtUrgentNote.SetEnabled(true);
            dtRequestPO.SetEnabled(true);            
        }
        if (chkUrgent.GetChecked() == (false)) {            
            txtUrgentNote.SetEnabled(false);
            dtRequestPO.SetEnabled(false);            
        }
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">  
            <tr style="height: 2px">
                <td></td>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>      
            <tr style="height: 35px">
                <td style=" padding:0px 0px 0px 10px; width:100px">
                    <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Number">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td style="width:210px">           
                <dx1:ASPxTextBox ID="txtPRNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                            Width="200px" ClientInstanceName="txtPRNo" MaxLength="20" 
                            Height="25px" Font-Bold="True" 
                            ForeColor="Black" ReadOnly ="true"  BackColor="LightGray"  >
     
                </dx1:ASPxTextBox>
                </td>
                <td style="width:50px">
                    <dx1:ASPxTextBox ID="txtRev" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="50px" ClientInstanceName="txtRev" MaxLength="20" 
                                Height="25px" Font-Bold="True" 
                                ForeColor="Black" HorizontalAlign="Center" ReadOnly ="true" BackColor="LightGray" >
     
                    </dx1:ASPxTextBox>           
                </td>
                <td>&nbsp;</td>
                <td style="width:100px">&nbsp;</td>
                <td style="width:100px">
                    <dx1:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Department">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
                    
                    <dx1:ASPxComboBox ID="cboDepartment" runat="server" ClientInstanceName="cboDepartment"
                            Width="200px" Font-Names="Segoe UI" DataSourceID="SqlDataSource3" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" ReadOnly ="true" BackColor="LightGray" >                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
               </td>
                
                <td>&nbsp;     
                       </td>
                <td>&nbsp;</td>
            </tr>   
            <tr style="height: 35px">
                <td style=" padding:0px 0px 0px 10px; width:100px">
                    <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Date">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
                <dx:ASPxDateEdit ID="dtDate" runat="server" Theme="Office2010Black" 
                                        Width="120px" AutoPostBack="false" ClientInstanceName="dtDate"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px"  BackColor="LightGray"
                        ReadOnly ="true">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                </td>
                <td>
                    &nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Section">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
           <dx1:ASPxComboBox ID="cboSection" runat="server" ClientInstanceName="cboSection"
                            Width="200px" Font-Names="Segoe UI" DataSourceID="SqlDataSource4" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" ReadOnly ="true" BackColor="LightGray">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr> 
            <tr style="height: 35px">
                <td style=" padding:0px 0px 0px 10px; width:100px">
                    <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Budget" >
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
       <dx1:ASPxComboBox ID="cboPRBudget" runat="server" ClientInstanceName="cboPRBudget"
                            Width="200px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown"  
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" ReadOnly ="true" BackColor="LightGray">      
                            <ClientSideEvents SelectedIndexChanged="Description" />                           
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td>
           
                    &nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Cost Center">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
           <dx1:ASPxComboBox ID="cboCostCenter" runat="server" ClientInstanceName="cboCostCenter"
                            Width="200px" Font-Names="Segoe UI" DataSourceID="SqlDataSource5" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" ReadOnly ="true" BackColor="LightGray">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr> 
            <tr style="height: 35px">
                <td style=" padding:0px 0px 0px 10px; width:100px">
                    <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Type">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
                        <dx1:ASPxComboBox ID="cboPRType" runat="server" ClientInstanceName="cboPRType"
                            Width="200px" Font-Names="Segoe UI" DataSourceID="SqlDataSource2" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" ReadOnly ="true" BackColor="LightGray">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td>
           
                        &nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Project">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
            <dx1:ASPxTextBox ID="txtProject" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="200px" ClientInstanceName="txtProject" MaxLength="20" 
                        Height="25px" ReadOnly ="true" BackColor="LightGray">
            </dx1:ASPxTextBox>
           
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr> 
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
        </table>
    </div>

    <div style="padding: 5px 5px 5px 5px">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRBudget'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
             SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRType'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
             SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Department'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
             SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Section'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
             SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'CostCenter'">
                    </asp:SqlDataSource>
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="PRNo" >

             <Columns>
                 <dx:GridViewDataTextColumn Caption="Material No" VisibleIndex="1" 
                     FieldName="Material_No">
                     <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="2" FieldName="Description" Width="300px">
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Spesification" VisibleIndex="3" FieldName="Specification" Width="450px">
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Qty" VisibleIndex="4" FieldName="Qty" Width="60px">
  
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="UOM" VisibleIndex="5" FieldName="UOM" Width="60px">
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Remarks" VisibleIndex="6" 
                     FieldName="Remarks" Width="150px">
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="No" VisibleIndex="0" FieldName="No" Width="40px">
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
             </Columns>
                    <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                              <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
     
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" 
                        VerticalScrollableHeight="260" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>

            </dx:ASPxGridView>
    </div>

    <div style="padding: 5px 5px 5px 5px">
    <table style="width: 100%; border: 0px"> 
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>     
                    <dx:ASPxCallback ID="cbApprove" runat="server" 
                        ClientInstanceName="cbApprove">                                      
                    <ClientSideEvents CallbackComplete="MessageBox" init="OnLoad"></ClientSideEvents>
                </dx:ASPxCallback>     
                    <dx:ASPxCallback ID="cbReject" runat="server" 
                        ClientInstanceName="cbReject">
                    <ClientSideEvents CallbackComplete="MessageBox" />                                           
                </dx:ASPxCallback></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:100px">         
                <dx:ASPxCheckBox ID="chkUrgent" runat="server" ClientInstanceName="chkUrgent" 
                    Text="Urgent" ValueChecked="1" ValueType="System.Int32" ValueUnchecked="0" 
                        Font-Names="Segoe UI" Font-Size="9pt" CheckState="Unchecked">
                    <ClientSideEvents CheckedChanged="tes" />
                    
                    </dx:ASPxCheckBox></td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Req. PO Issue Date">
                    </dx1:ASPxLabel>                 
                </td>
                <td>
                <dx:ASPxDateEdit ID="dtRequestPO" runat="server" Theme="Office2010Black" 
                                        Width="100px" AutoPostBack="false" ClientInstanceName="dtRequestPO"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 70px">
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Urgent Notes/Reason">
                    </dx1:ASPxLabel>                 
                </td>
                <td>
                    <dx1:ASPxMemo ID="txtUrgentNote" runat="server" Height="50px" Width="300px"
                        ClientInstanceName="txtUrgentNote" Font-Names="Segoe UI" Font-Size="9pt"
                        Theme="Metropolis" MaxLength="300">                        
                    </dx1:ASPxMemo>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Approval Notes">
                    </dx1:ASPxLabel>                 
                </td>
                <td>
                    <dx1:ASPxMemo ID="txtApprovalNote" runat="server" Height="100px" Width="800px"
                        ClientInstanceName="txtApprovalNote" Font-Names="Segoe UI" Font-Size="9pt"
                        Theme="Metropolis" MaxLength="300">
                       
                    </dx1:ASPxMemo>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 2px">
                <td style=" padding:0px 0px 0px 10px" colspan="3">

                    <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>

                    &nbsp;&nbsp;

                    <dx1:ASPxButton ID="btnApprove" runat="server" Text="Approve" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnApprove" Theme="Default" >                        
                     
                        <ClientSideEvents Click="function(s, e) {

                                //alert(chkUrgent.GetValue());

                                
                                if (chkUrgent.GetValue() == 1 && dtRequestPO.GetDate() != null) {
                                        var prdate = dtDate.GetDate(); // get value date
                                        var reqPODate = dtRequestPO.GetDate(); // get value date

                                        var yearstartdate = prdate.getFullYear(); // where getFullYear returns the year (four digits)
                                        var monthstartdate = prdate.getMonth(); // where getMonth returns the month (from 0-11)
                                        var daystartdate = prdate.getDate();   // where getDate returns the day of the month (from 1-31)

                                        if (daystartdate &lt; 10) {
                                            daystartdate = '0'+ daystartdate  
                                        }

                                        if (monthstartdate &lt; 10) {
                                            monthstartdate = '0'+ monthstartdate  
                                        }

                                        var vStartDate = yearstartdate + '-' + monthstartdate + '-' + daystartdate;

                            
                                        var yearenddate = reqPODate.getFullYear(); // where getFullYear returns the year (four digits)
                                        var monthenddate = reqPODate.getMonth(); // where getMonth returns the month (from 0-11)
                                        var dayenddate = reqPODate.getDate();   // where getDate returns the day of the month (from 1-31)

                                        if (dayenddate &lt; 10) {
                                            dayenddate = '0'+ dayenddate  
                                        }

                                        if (monthenddate &lt; 10) {
                                            monthenddate = '0'+ monthenddate  
                                        }

                                        var vEndDate = yearenddate + '-' + monthenddate + '-' + dayenddate;

                                        if (vEndDate &lt; vStartDate) {
                                            toastr.warning('Request PO Issue Date must be bigger than PR Date', 'Warning');
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
                                            e.processOnServer = false;
                                            return;
                                        }
                                        else if (txtUrgentNote.GetText() == '') {
                                            toastr.warning('Please Input Urgent Notes !', 'Warning');
                                            txtUrgentNote.Focus();
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
		                                    e.processOnServer = false;
                                            return;                                        
                                    }              
                                }
                                else if (chkUrgent.GetValue() == 1 && dtRequestPO.GetDate() == null) {
                                        toastr.warning('Please Input Valid Request PO Issue Date!', 'Warning');
                                        dtRequestPO.Focus();
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;
		                                e.processOnServer = false;
                                        return;                                        
                                }


	                            var msg = confirm('Are you sure want to approve this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }

                                

                                cbApprove.PerformCallback();

                                }" />
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>


                    &nbsp;&nbsp;

                    <dx1:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnReject" Theme="Default" >                        
                     
                        <ClientSideEvents Click="function(s, e) {

                                if (txtApprovalNote.GetText() == ''){    
                                        toastr.warning('Please Input Approval Notes!', 'Warning');
                                        txtApprovalNote.Focus();
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;
		                                e.processOnServer = false;
                                        return;
                                 }

	                            var msg = confirm('Are you sure want to reject this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }
								
								cbReject.PerformCallback();

                            }" />
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>


                    &nbsp;

                    <dx1:ASPxButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnSubmit" Theme="Default">                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>


                    &nbsp;&nbsp;

                    
                    </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
    </table>
    </div>
</div>
</asp:Content>

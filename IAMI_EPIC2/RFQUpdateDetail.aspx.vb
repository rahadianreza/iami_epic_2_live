﻿
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.UI
Imports System.IO
Imports System.Drawing
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls.Style
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxUploadControl
Imports System
Imports DevExpress.Utils
Imports System.Collections.Generic
Imports OfficeOpenXml

Public Class RFQUpdateDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim pErr As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cpMessage") = ErrMsg
        Grid.JSProperties("cpType") = msgType
        Grid.JSProperties("cpVal") = pVal
    End Sub

    Private Sub up_FillCombo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsRFQUpdateDB.GetSupplier(pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboSupplier1.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
                cboSupplier2.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
                cboSupplier3.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
                cboSupplier4.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
                cboSupplier5.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If


    End Sub

    Private Sub up_DeleteTempRFQData(Optional ByRef pErr As String = "")
        Try
            clsRFQUpdateDB.DeleteTempDataRFQ(pErr)

        Catch ex As Exception
            gs_Message = ex.Message
        End Try
    End Sub

    Private Sub up_GridLoad(pPRNo As String, pRFQSetNo As String, pRev As Integer)
        Dim ErrMsg As String = ""


        Dim RFQNumber As String = ""

        Dim ds As New DataSet

        ds = clsRFQUpdateDB.GetDataHeader(pRFQSetNo, pRev, ErrMsg)

        If ds.Tables(0).Rows.Count > 0 Then

            If ds.Tables(0).Rows(0)("RFQ_Status") = 1 Or ds.Tables(0).Rows(0)("RFQ_Status") = 2 Or ds.Tables(0).Rows(0)("RFQ_Status") = 4 Then
                Grid.Enabled = False
                ScriptManager.RegisterStartupScript(txtRFQTitle, txtRFQTitle.GetType(), "txtRFQTitle", "txtRFQTitle.SetEnabled(false);", True)
                ScriptManager.RegisterStartupScript(dtDateline, dtDateline.GetType(), "dtDateline", "dtDateline.SetEnabled(false);", True)
                ScriptManager.RegisterStartupScript(cboCurrency, cboCurrency.GetType(), "cboCurrency", "cboCurrency.SetEnabled(false);", True)

            End If

            '07/02/2019
            Dim ds1 As New DataSet
            ds1 = clsRFQUpdateDB.CheckExistRFQDetail(pPRNo, pRFQSetNo, ErrMsg)
            If ds1.Tables(0).Rows.Count = 0 Then
                btnDraft.Enabled = False
            End If


            cboCurrency.Text = ds.Tables(0).Rows(0)("Currency_Code")
            txtRev.Text = ds.Tables(0).Rows(0)("Rev")
            gs_RFQNo = ds.Tables(0).Rows(0)("RFQ_Number")
            gs_RFQTitle = ds.Tables(0).Rows(0)("RFQ_Title")
            gs_SetRFQNo = ds.Tables(0).Rows(0)("RFQ_Set")
            gs_RFQDate = ds.Tables(0).Rows(0)("RFQ_Date")
            txtRFQSetNumber.Text = ds.Tables(0).Rows(0)("RFQ_Set")
            RFQNumber = ds.Tables(0).Rows(0)("RFQ_Number")
            dtDRFQDate.Value = ds.Tables(0).Rows(0)("RFQ_Date")
            txtPRNumber.Text = ds.Tables(0).Rows(0)("PR_Number")
            dtDateline.Value = ds.Tables(0).Rows(0)("RFQ_DueDate")
            txtRFQTitle.Text = ds.Tables(0).Rows(0)("RFQ_Title")

            For a = 0 To 4
                vSupplier(a) = ""
                vStatus(a) = ""
            Next

             

            For i = 0 To ds.Tables(0).Rows.Count - 1
                vSupplier(i) = ds.Tables(0).Rows(i)("Supplier_Code") & ""
                vRFQNo(i) = ds.Tables(0).Rows(i)("RFQ_Number") & ""
                vStatus(i) = ds.Tables(0).Rows(i)("RFQ_Status") & ""
            Next

            If vSupplier(0) <> "" Then
                cboSupplier1.SelectedIndex = cboSupplier1.Items.IndexOf(cboSupplier1.Items.FindByValue(Trim(vSupplier(0) & "")))
                If vStatus(0) <> "0"
                ScriptManager.RegisterStartupScript(cboSupplier1, cboSupplier1.GetType(), "cboSupplier1", "cboSupplier1.SetEnabled(false);", True)
                end If
                txtRFQNo1.Text = vRFQNo(0)
                ASPxLabelRFQNo1.Text = vRFQNo(0)
            Else
                cboSupplier1.Enabled = True
                vSupplier(0) = ""
                vRFQNo(0) = ""
            End If

            If vSupplier(1) <> "" Then
                cboSupplier2.SelectedIndex = cboSupplier2.Items.IndexOf(cboSupplier2.Items.FindByValue(Trim(vSupplier(1) & "")))
                txtRFQNo2.Text = vRFQNo(1)
                if vStatus(1) <> "0"
                ScriptManager.RegisterStartupScript(cboSupplier2, cboSupplier2.GetType(), "cboSupplier2", "cboSupplier2.SetEnabled(false);", True)
                end if
                ASPxLabelRFQNo2.Text = vRFQNo(1)
            Else
                cboSupplier2.Enabled = True
                vSupplier(1) = ""
                vRFQNo(1) = ""
            End If

            If vSupplier(2) <> "" Then
                cboSupplier3.SelectedIndex = cboSupplier3.Items.IndexOf(cboSupplier3.Items.FindByValue(Trim(vSupplier(2) & "")))
                txtRFQNo3.Text = vRFQNo(2)
                if vStatus(2) <> "0"
                ScriptManager.RegisterStartupScript(cboSupplier3, cboSupplier3.GetType(), "cboSupplier3", "cboSupplier3.SetEnabled(false);", True)
                end if
                ASPxLabelRFQNo3.Text = vRFQNo(2)
            Else
                cboSupplier3.Enabled = True
                vSupplier(2) = ""
                vRFQNo(2) = ""
            End If

            If vSupplier(3) <> "" Then
                cboSupplier4.SelectedIndex = cboSupplier4.Items.IndexOf(cboSupplier4.Items.FindByValue(Trim(vSupplier(3) & "")))
                txtRFQNo4.Text = vRFQNo(3)
                if vStatus(3) <> "0"
                ScriptManager.RegisterStartupScript(cboSupplier4, cboSupplier4.GetType(), "cboSupplier4", "cboSupplier4.SetEnabled(false);", True)
                end if
                ASPxLabelRFQNo4.Text = vRFQNo(3)
            Else
                cboSupplier4.Enabled = True
                vSupplier(3) = ""
                vRFQNo(3) = ""
            End If

            If vSupplier(4) <> "" Then
                cboSupplier5.SelectedIndex = cboSupplier5.Items.IndexOf(cboSupplier5.Items.FindByValue(Trim(vSupplier(4) & "")))
                txtRFQNo5.Text = vRFQNo(4)
                If vStatus(4) <> "0"
                ScriptManager.RegisterStartupScript(cboSupplier5, cboSupplier5.GetType(), "cboSupplier5", "cboSupplier5.SetEnabled(false);", True)
                End If
                ASPxLabelRFQNo5.Text = vRFQNo(4)
            Else
                cboSupplier5.Enabled = True
                vSupplier(4) = ""
                vRFQNo(4) = ""
            End If

        End If

        ds = clsRFQUpdateDB.GetDataDetail(pPRNo, pRFQSetNo, pRev, ErrMsg)

        If ErrMsg = "" Then

            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|C010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboCurrency)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("C060")
        Master.SiteTitle = "REQUEST FOR QUOTATION (RFQ) UPDATE DETAIL" 'sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "C060")

        Dim RFQSetNo As String = ""
        Dim PRNo As String = ""
        Dim Revision As Integer

        If Not IsNothing(Request.QueryString("ID")) Then
            RFQSetNo = Split(Request.QueryString("ID"), "|")(0)
            PRNo = Split(Request.QueryString("ID"), "|")(1)
            Revision = Split(Request.QueryString("ID"), "|")(2)
        End If

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            up_FillCombo()
            up_GridLoad(PRNo, RFQSetNo, Revision)

            up_DeleteTempRFQData()

            gs_Back = True
        End If

    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        gs_Back = True
        Response.Redirect("~/RFQUpdate.aspx")
    End Sub

    Private Sub up_UpdateDataDetail(pRFQNo As String, sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs)
        Dim ls_Check As String
        Dim clsRFQUpdateData As New clsRFQUpdate
        Dim ls_MaterialNo As String
        Dim li_Qty As Integer

        Dim a As Integer
        a = e.UpdateValues.Count

        clsRFQUpdateData.RFQSetNumber = gs_SetRFQNo
        clsRFQUpdateData.RFQNumber = pRFQNo 'gs_RFQNo
        clsRFQUpdateData.Rev = txtRev.Text
        For iLoop = 0 To a - 1
            ls_Check = (e.UpdateValues(iLoop).NewValues("AllowCheck").ToString())
            If ls_Check = True Then ls_Check = "1" Else ls_Check = "0"
            ls_MaterialNo = Trim(e.UpdateValues(iLoop).NewValues("Material_No").ToString())
            li_Qty = e.UpdateValues(iLoop).NewValues("Qty")

            clsRFQUpdateData.MaterialNo = ls_MaterialNo
            clsRFQUpdateData.Qty = li_Qty

            If ls_Check = "0" Then
                clsRFQUpdateDB.DeleteDetail(clsRFQUpdateData, pErr)
            Else
                clsRFQUpdateDB.InsertDetail(clsRFQUpdateData, pUser, pErr)
            End If

        Next
    End Sub

   
    '15/03/2019
    Public Sub getSupplierValue()
        Dim vsup As String
        Dim ds As New DataSet
        Dim ls_Bln As String
        Dim li_Bln As Integer = Format(Now, "MM")
        Dim ls_RFQNumber As String = ""
        Dim RFQCls As New clsRFQUpdate
        For a = 0 To 4
            vSupplier(a) = ""
        Next

        ds = clsRFQUpdateDB.GetRFQNumber(gs_SetRFQNo)

        For i = 0 To ds.Tables(0).Rows.Count - 1
            vSupplier(i) = ds.Tables(0).Rows(i)("Supplier_Code") & ""
            vRFQNo(i) = ds.Tables(0).Rows(i)("RFQ_Number") & ""
        Next


        'If pDataChange = False Then
        If cboSupplier1.SelectedIndex >= 0 Then
            txtRFQNo1.Text = vRFQNo(0)
            If vSupplier(0) <> cboSupplier1.SelectedItem.GetValue("Code").ToString() Then
                vsup = cboSupplier1.SelectedItem.GetValue("Code").ToString()
                clsRFQUpdateDB.UpdateHeader(vRFQNo(0), vsup, pUser, pErr)
            End If
        End If
        If cboSupplier2.SelectedIndex >= 0 Then
            If txtRFQNo2.Text <> "" Then
                If vSupplier(1) <> cboSupplier2.SelectedItem.GetValue("Code").ToString() And ASPxLabelRFQNo2.Text <> "" Then
                    vsup = cboSupplier2.SelectedItem.GetValue("Code").ToString()
                    clsRFQUpdateDB.UpdateHeader(txtRFQNo2.Text, vsup, pUser, pErr)
                End If
            Else
                ds = clsRFQUpdateDB.GetMaxRFQNo(li_Bln, pErr)
                If ds.Tables(0).Rows.Count > 0 Then
                    ls_Bln = uf_ConvertMonth(Format(Now, "MM"))

                    ls_RFQNumber = "IAMI/COST/RFQ/" & ls_Bln & "/" & Format(Now, "yy") & "/" & ds.Tables(0).Rows(0)("RFQ_Number")
                    RFQCls.RFQSetNumber = gs_SetRFQNo
                    RFQCls.RFQNumber = ls_RFQNumber
                    RFQCls.Supplier = cboSupplier2.SelectedItem.GetValue("Code").ToString()
                    RFQCls.RFQTitle = gs_RFQTitle
                    RFQCls.RFQDate = gs_RFQDate
                    RFQCls.Rev = txtRev.Text
                    clsRFQUpdateDB.InsertHeader(RFQCls, pUser, pErr)

                    'RFQCls.RFQNumber = gs_RFQNo
                    clsRFQUpdateDB.InsertDetailRFQData(RFQCls, ls_RFQNumber, pUser, pErr)

                    gs_RFQNo2 = ls_RFQNumber
                End If
            End If
        End If
        If cboSupplier3.SelectedIndex >= 0 Then
            If txtRFQNo3.Text <> "" Then
                If vSupplier(2) <> cboSupplier3.SelectedItem.GetValue("Code").ToString() And ASPxLabelRFQNo3.Text <> "" Then
                    vsup = cboSupplier3.SelectedItem.GetValue("Code").ToString()
                    clsRFQUpdateDB.UpdateHeader(txtRFQNo3.Text, vsup, pUser, pErr)
                End If
            Else
                ds = clsRFQUpdateDB.GetMaxRFQNo(li_Bln, pErr)
                If ds.Tables(0).Rows.Count > 0 Then
                    ls_Bln = uf_ConvertMonth(Format(Now, "MM"))

                    ls_RFQNumber = "IAMI/COST/RFQ/" & ls_Bln & "/" & Format(Now, "yy") & "/" & ds.Tables(0).Rows(0)("RFQ_Number")
                    RFQCls.RFQSetNumber = gs_SetRFQNo
                    RFQCls.RFQNumber = ls_RFQNumber
                    RFQCls.Supplier = cboSupplier3.SelectedItem.GetValue("Code").ToString()
                    RFQCls.RFQTitle = gs_RFQTitle
                    RFQCls.RFQDate = gs_RFQDate
                    RFQCls.Rev = txtRev.Text
                    clsRFQUpdateDB.InsertHeader(RFQCls, pUser, pErr)

                    'RFQCls.RFQNumber = gs_RFQNo
                    clsRFQUpdateDB.InsertDetailRFQData(RFQCls, ls_RFQNumber, pUser, pErr)
                    gs_RFQNo3 = ls_RFQNumber
                End If
            End If
        End If
        If cboSupplier4.SelectedIndex >= 0 Then
            If txtRFQNo4.Text <> "" Then
                If vSupplier(3) <> cboSupplier4.SelectedItem.GetValue("Code").ToString() And ASPxLabelRFQNo4.Text <> "" Then
                    vsup = cboSupplier4.SelectedItem.GetValue("Code").ToString()
                    clsRFQUpdateDB.UpdateHeader(txtRFQNo4.Text, vsup, pUser, pErr)
                End If
            Else
                ds = clsRFQUpdateDB.GetMaxRFQNo(li_Bln, pErr)
                If ds.Tables(0).Rows.Count > 0 Then
                    ls_Bln = uf_ConvertMonth(Format(Now, "MM"))

                    ls_RFQNumber = "IAMI/COST/RFQ/" & ls_Bln & "/" & Format(Now, "yy") & "/" & ds.Tables(0).Rows(0)("RFQ_Number")
                    RFQCls.RFQSetNumber = gs_SetRFQNo
                    RFQCls.RFQNumber = ls_RFQNumber
                    RFQCls.Supplier = cboSupplier4.SelectedItem.GetValue("Code").ToString()
                    RFQCls.RFQTitle = gs_RFQTitle
                    RFQCls.RFQDate = gs_RFQDate
                    RFQCls.Rev = txtRev.Text
                    clsRFQUpdateDB.InsertHeader(RFQCls, pUser, pErr)

                    'RFQCls.RFQNumber = gs_RFQNo
                    clsRFQUpdateDB.InsertDetailRFQData(RFQCls, ls_RFQNumber, pUser, pErr)
                    gs_RFQNo4 = ls_RFQNumber
                End If
            End If
        End If
        If cboSupplier5.SelectedIndex >= 0 Then
            If txtRFQNo5.Text <> "" Then
                If vSupplier(4) <> cboSupplier5.SelectedItem.GetValue("Code").ToString() And ASPxLabelRFQNo5.Text <> "" Then
                    vsup = cboSupplier5.SelectedItem.GetValue("Code").ToString()
                    clsRFQUpdateDB.UpdateHeader(txtRFQNo5.Text, vsup, pUser, pErr)
                End If
            Else
                ds = clsRFQUpdateDB.GetMaxRFQNo(li_Bln, pErr)
                If ds.Tables(0).Rows.Count > 0 Then
                    ls_Bln = uf_ConvertMonth(Format(Now, "MM"))

                    ls_RFQNumber = "IAMI/COST/RFQ/" & ls_Bln & "/" & Format(Now, "yy") & "/" & ds.Tables(0).Rows(0)("RFQ_Number")
                    RFQCls.RFQSetNumber = gs_SetRFQNo
                    RFQCls.RFQNumber = ls_RFQNumber
                    RFQCls.Supplier = cboSupplier5.SelectedItem.GetValue("Code").ToString()
                    RFQCls.RFQTitle = gs_RFQTitle
                    RFQCls.RFQDate = gs_RFQDate
                    RFQCls.Rev = txtRev.Text
                    clsRFQUpdateDB.InsertHeader(RFQCls, pUser, pErr)

                    'RFQCls.RFQNumber = gs_RFQNo
                    clsRFQUpdateDB.InsertDetailRFQData(RFQCls, ls_RFQNumber, pUser, pErr)
                    gs_RFQNo5 = ls_RFQNumber
                End If
            End If
        End If
    End Sub

    '15-03-2019
    Private Sub up_UpdateDataHeader(Optional ByRef pMsg As String = "")
        Dim vsup As String
        Dim ds As New DataSet
        Dim ls_Bln As String
        Dim li_Bln As Integer = Format(Now, "MM")
        Dim ls_RFQNumber As String = ""
        Dim RFQCls As New clsRFQUpdate

        'update supplier 1-5
        getSupplierValue()

        Dim rev As Integer

        If txtRev.Text = "" Then
            rev = 0
        Else
            rev = txtRev.Text
        End If


        Dim DDate As String
        DDate = Format(dtDateline.Value, "yyyy-MM-dd")
        clsRFQUpdateDB.UpdateDataRFQ(gs_SetRFQNo, rev, txtRFQTitle.Text, DDate, cboCurrency.Text, pUser, "2", pErr)
        'clsRFQUpdateDB.UpdateDataRFQ(RFQCls, pUser, pErr)

        If pErr = "" Then
            gs_Message = "Draft data saved successfull"
            '    clsRFQUpdateDB.UpdateStatus(gs_SetRFQNo, pErr)
        End If
        '    pMsg = pErr
        'End If

        pDataChange = False

    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "AllowCheck" Then
            'Editable
            e.Cell.BackColor = Color.White
        Else
            'Non-Editable
            e.Cell.BackColor = Color.LemonChiffon
        End If
    End Sub

    Private Function uf_ConvertMonth(pMonth As Integer) As String
        Dim angka As String = ""

        If pMonth = 1 Then
            angka = "I"
        ElseIf pMonth = 2 Then
            angka = "II"
        ElseIf pMonth = 3 Then
            angka = "III"
        ElseIf pMonth = 4 Then
            angka = "IV"
        ElseIf pMonth = 5 Then
            angka = "V"
        ElseIf pMonth = 6 Then
            angka = "VI"
        ElseIf pMonth = 7 Then
            angka = "VII"
        ElseIf pMonth = 8 Then
            angka = "VIII"
        ElseIf pMonth = 9 Then
            angka = "IX"
        ElseIf pMonth = 10 Then
            angka = "X"
        ElseIf pMonth = 11 Then
            angka = "XI"
        ElseIf pMonth = 12 Then
            angka = "XII"
        End If

        Return angka
    End Function

    Private Sub cbDraft_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDraft.Callback
        'Dim pPRNo As String = Split(e.Parameter, "|")(1)
        'Dim pRFQSet As String = Split(e.Parameter, "|")(2)
        'Dim pRev As Integer = Split(e.Parameter, "|")(3)
        'Dim perr As String = ""

        'If pFunction = "draft" Then
        '    'up_UpdateStatus(perr)
        'End If

        up_UpdateDataHeader()
        Dim rev As Integer
        rev = txtRev.Text
        Dim DDate As String
        DDate = Format(dtDateline.Value, "yyyy-MM-dd")
        clsRFQUpdateDB.UpdateStatus(gs_SetRFQNo, pUser, rev, DDate, txtRFQTitle.Text, cboCurrency.Text, pErr)


        'up_GridLoad(pPRNo, pRFQSet, pRev)

        'If gs_Message = "" Then
        cbDraft.JSProperties("cpMessage") = gs_Message '"Draft data saved successfull"


        'cbDraft.JSProperties("cpRevision") = gs_Revision
        'cbDraft.JSProperties("cpMessage") = gs_Message
        'cbDraft.JSProperties("cpRFQSetNo") = gs_SetRFQNo
        cbDraft.JSProperties("cpRFQNo1") = gs_RFQNo1
        cbDraft.JSProperties("cpRFQNo2") = gs_RFQNo2
        cbDraft.JSProperties("cpRFQNo3") = gs_RFQNo3
        cbDraft.JSProperties("cpRFQNo4") = gs_RFQNo4
        cbDraft.JSProperties("cpRFQNo5") = gs_RFQNo5

        'End If

        gs_Message = ""
    End Sub

    Private Sub cbSimpan_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSimpan.Callback
        'update supplier 1-5 (15-03-2019)
        getSupplierValue()

        Dim rev As Integer
        rev = txtRev.Text
        Dim DDate As String
        DDate = Format(dtDateline.Value, "yyyy-MM-dd")
        clsRFQUpdateDB.UpdateStatus(gs_SetRFQNo, pUser, rev, DDate, txtRFQTitle.Text, cboCurrency.Text, pErr)
        If pErr = "" Then
            cbSimpan.JSProperties("cpMessage") = "Data saved successfull"
        Else
            cbSimpan.JSProperties("cpMessage") = pErr
        End If

        cbSimpan.JSProperties("cpRFQNo1") = gs_RFQNo1
        cbSimpan.JSProperties("cpRFQNo2") = gs_RFQNo2
        cbSimpan.JSProperties("cpRFQNo3") = gs_RFQNo3
        cbSimpan.JSProperties("cpRFQNo4") = gs_RFQNo4
        cbSimpan.JSProperties("cpRFQNo5") = gs_RFQNo5
        Grid.Enabled = False
    End Sub

 
End Class
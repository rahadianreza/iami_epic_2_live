﻿Imports DevExpress.Web.ASPxGridView

Public Class Gate_I_List_Release
    Inherits System.Web.UI.Page
#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim grpcomm As String = ""
    Dim flag As Integer = 0
    Dim pt As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("J020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        prj = Request.QueryString("projectid")
        grp = Request.QueryString("groupid")
        comm = Request.QueryString("commodity")
        grpcomm = Request.QueryString("groupcomodity")
        pt = Request.QueryString("projecttype")

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            'If String.IsNullOrEmpty(prj) Then
            '    cboProjectType.SelectedIndex = 0
            '    cboProject.SelectedIndex = 0
            '    cboGroup.SelectedIndex = 0
            '    cboCommodity.SelectedIndex = 0
            '    cboGroupCommodity.SelectedIndex = 0
            'End If

            'cboProject.Value = prj
            'cboGroup.Value = grp
            'cboCommodity.Value = comm
            'cboGroupCommodity.Value = grpcomm
            'cboProjectType.Value = pt

            If String.IsNullOrEmpty(prj) Then
                cboProjectType.SelectedIndex = 0
                cboProject.SelectedIndex = 0
                cboGroup.SelectedIndex = 0
                cboCommodity.SelectedIndex = 0
                cboGroupCommodity.SelectedIndex = 0
            Else
                cboProject.Value = prj
                cboGroup.Value = grp
                cboCommodity.Value = comm
                cboGroupCommodity.Value = grpcomm

                If pt = Nothing Then
                    cboProjectType.SelectedIndex = 0
                Else
                    cboProjectType.Value = pt
                End If
            End If


            up_FillComboProjectType(cboProjectType, statusAdmin, pUser, "ProjectType")
            up_FillComboProject(cboProject, statusAdmin, pUser, cboProjectType.Value)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "1", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboGroup.Value, cboProject.Value, "", "2", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboGroup.Value, cboProject.Value, cboGroupCommodity.Value, "4", statusAdmin, pUser)
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub



    'Private Sub Grid_CustomButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonEventArgs) Handles Grid.CustomButtonInitialize
    '    If e.VisibleIndex = -1 Then
    '        Return
    '    End If
    'If e.CellType = GridViewTableCommandCellType.Filter Then
    '    Return
    'End If

    'If clsGateICheckSheetDB.CheckGateStatus(Grid.GetRowValues(e.VisibleIndex, "Project_ID"), Grid.GetRowValues(e.VisibleIndex, "Group_ID"), Grid.GetRowValues(e.VisibleIndex, "Commodity"), Grid.GetRowValues(e.VisibleIndex, "Buyer"), Grid.GetRowValues(e.VisibleIndex, "Group_Comodity")) = "Accepted" Then
    '    If e.ButtonID = "Confirm" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    '    If e.ButtonID = "Approve" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    '    If e.ButtonID = "AfterApprove" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    'End If
    'If clsGateICheckSheetDB.CheckGateStatus(Grid.GetRowValues(e.VisibleIndex, "Project_ID"), Grid.GetRowValues(e.VisibleIndex, "Group_ID"), Grid.GetRowValues(e.VisibleIndex, "Commodity"), Grid.GetRowValues(e.VisibleIndex, "Buyer"), Grid.GetRowValues(e.VisibleIndex, "Group_Comodity")) = "Completed" Then
    '    If e.ButtonID = "Submit" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    '    If e.ButtonID = "Approve" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    '    If e.ButtonID = "AfterApprove" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    'End If

    'If clsGateICheckSheetDB.CheckGateStatus(Grid.GetRowValues(e.VisibleIndex, "Project_ID"), Grid.GetRowValues(e.VisibleIndex, "Group_ID"), Grid.GetRowValues(e.VisibleIndex, "Commodity"), Grid.GetRowValues(e.VisibleIndex, "Buyer"), Grid.GetRowValues(e.VisibleIndex, "Group_Comodity")) = "Confirmed" Then
    '    If e.ButtonID = "Submit" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    '    If e.ButtonID = "Confirm" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    '    If e.ButtonID = "AfterApprove" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    'End If

    'Dim status As String = clsGateICheckSheetDB.CheckGateStatus(Grid.GetRowValues(e.VisibleIndex, "Project_ID"), Grid.GetRowValues(e.VisibleIndex, "Group_ID"), Grid.GetRowValues(e.VisibleIndex, "Commodity"), Grid.GetRowValues(e.VisibleIndex, "Buyer"), Grid.GetRowValues(e.VisibleIndex, "Group_Comodity"))
    'If status <> "Confirmed" And status <> "Completed" And status <> "Accepted" Then
    '    If e.ButtonID = "Submit" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    '    If e.ButtonID = "Confirm" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    '    If e.ButtonID = "Approve" Then
    '        e.Visible = DevExpress.Utils.DefaultBoolean.False
    '    End If
    'End If
    ' End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else
            up_GridLoad("1")
        End If

        Dim ds2 As New DataSet
        ds2 = clsGateICheckSheetDB.CheckStatusRelease(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, "ALL")

        If ds2.Tables(0).Rows.Count > 0 Then
            If ds2.Tables(0).Rows(0)("Data").ToString = "0" Then
                Grid.JSProperties("cpRelease") = "disabled"
            Else
                Grid.JSProperties("cpRelease") = "enabled"
            End If
        Else
            Grid.JSProperties("cpRelease") = "disabled"
        End If
    End Sub

    Private Sub up_FillComboProjectType(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                              pAdminStatus As String, pUserID As String, _
                              Optional ByRef pGroup As String = "", _
                              Optional ByRef pParent As String = "", _
                              Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboProjectTypeGateISheet(pUser, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_FillComboGroupCommodityPIC(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _param As String, _param2 As String, _param3 As String, _type As String, _
                            pAdminStatus As String, pUserID As String, _
                            Optional ByRef pGroup As String = "", _
                            Optional ByRef pParent As String = "", _
                            Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsItemListSourcingDB.getGroupCommodityPIC(_param, _param2, _param3, _type, "Y", "5", pUserID)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_FillComboProject(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                             pAdminStatus As String, pUserID As String, projtype As String, _
                             Optional ByRef pGroup As String = "", _
                             Optional ByRef pParent As String = "", _
                             Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboProjectISheetRelease(projtype, pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsItemListSourcingDB.getGateIListReleased(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, "ALL", ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()

            'For x As Integer = 0 To Ses.Tables(0).Rows.Count - 1
            '    GetApprovalPerson(Ses.Tables(0).Rows(x).Item("Project_ID").ToString(), Ses.Tables(0).Rows(x).Item("Group_ID").ToString(), Ses.Tables(0).Rows(x).Item("Commodity").ToString(), Ses.Tables(0).Rows(x).Item("Group_Comodity").ToString())
            'Next

        End If
    End Sub
#End Region

#Region "Event Control"
    Private Sub cboGroupCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProject As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, pProject, "4", statusAdmin, pUser, "")
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, pProject, "4", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboProject_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboProject.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProjectType As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboProject(cboProject, statusAdmin, pUser, "")

        ElseIf pFunction = "filter" Then
            up_FillComboProject(cboProject, statusAdmin, pUser, pProjectType)
        End If
    End Sub

    Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProject As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroup, pProject, "", "", "1", statusAdmin, pUser, "")
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroup, pProject, "", "", "1", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboGroup.Value, cboProject.Value, "", "2", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboGroup.Value, cboProject.Value, "", "2", statusAdmin, pUser)
        End If
    End Sub

    Public Sub GetApprovalPerson(_projectid As String, _groupid As String, _commodity As String, _groupcomodity As String)
        Dim ds As DataSet = clsItemListSourcingDB.getApprovalPerson(_projectid, _groupid, _commodity, _groupcomodity, pUser)

        If flag < 1 Then
            Dim bandColumn As GridViewBandColumn

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                For y As Integer = 4 To ds.Tables(0).Columns.Count - 1
                    If ds.Tables(0).Columns(y).ColumnName.Contains("o_Person") Then
                        bandColumn = New GridViewBandColumn()
                        bandColumn.Caption = ds.Tables(0).Rows(i)(y + 2).ToString
                        bandColumn.VisibleIndex = 15
                    End If

                    If ds.Tables(0).Columns(y).ColumnName.Contains("o_ApprovalName") Then
                        Grid.Columns.Add(bandColumn)
                    Else
                        Dim col As GridViewDataTextColumn = New GridViewDataTextColumn()
                        col.Caption = IIf(ds.Tables(0).Columns(y).ColumnName.Contains("o_Person"), "Approval Person", "Approval Date")
                        col.FieldName = ds.Tables(0).Columns(y).ColumnName
                        If col.Caption = "Approval Date" Then
                            col.PropertiesTextEdit.DisplayFormatString = "dd MMM yyyy"
                        End If
                        bandColumn.Columns.Add(col)
                    End If
                Next
            Next

            flag += 1
        End If
    End Sub


    Protected Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Dim ls_Check As String
        Dim ls_SQL As String = "",
            ls_ProjectID As String = "",
            ls_GroupID As String = "",
            ls_Commodity As String = "",
            ls_Group_Comodity As String = "",
            ls_Buyer As String = "",
            ls_Date As String = "",
            ls_StartTime As DateTime,
            ls_FinishTime As DateTime,
            ls_MeetingRoom As String = "",
            ls_StatusRelease As String = "",
            ls_MsgID As String = ""

        Dim iLoop As Long = 0, jLoop As Long = 0
        Dim ls_UserID As String = ""

        If e.UpdateValues.Count = 0 Then
            Exit Sub
        End If

        Dim a As Integer
        a = e.UpdateValues.Count
        Dim pErr As String = ""
        For iLoop = 0 To a - 1
            ls_Check = (e.UpdateValues(iLoop).NewValues("AllowCheck").ToString())
            If ls_Check = True Then ls_Check = "1" Else ls_Check = "0"

            ls_ProjectID = (e.UpdateValues(iLoop).NewValues("Project_ID").ToString())
            ls_GroupID = (e.UpdateValues(iLoop).NewValues("Group_ID").ToString())
            ls_Commodity = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Commodity")), "", e.UpdateValues(iLoop).NewValues("Commodity"))
            ls_Group_Comodity = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Group_Comodity")), "", e.UpdateValues(iLoop).NewValues("Group_Comodity"))
            ls_Buyer = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Buyer")), "", e.UpdateValues(iLoop).NewValues("Buyer"))
            ls_Date = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("KSH_Date")), "", e.UpdateValues(iLoop).NewValues("KSH_Date"))
            ls_StartTime = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Start_Time")), "", e.UpdateValues(iLoop).NewValues("Start_Time"))
            ls_FinishTime = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Finish_Time")), "", e.UpdateValues(iLoop).NewValues("Finish_Time"))
            ls_MeetingRoom = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("Meeting_Room")), "", e.UpdateValues(iLoop).NewValues("Meeting_Room"))
            ls_StatusRelease = IIf(String.IsNullOrEmpty(e.UpdateValues(iLoop).NewValues("StatusRelease")), "Draft", e.UpdateValues(iLoop).NewValues("StatusRelease"))

            'Dim invitationDate As String
            ls_Date = Format(CDate(ls_Date), "yyyy-MM-dd")

            ' ls_InvitationTime = Format(ls_InvitationTime, "yyyy-MM-dd hh:mm")
            Dim ls_Startime_Str As String
            ls_Startime_Str = Format(ls_StartTime, "HH:mm")
            ls_Startime_Str = ls_Date + " " + ls_Startime_Str

            Dim ls_FinishTime_Str As String
            ls_FinishTime_Str = Format(ls_FinishTime, "HH:mm")
            ls_FinishTime_Str = ls_Date + " " + ls_FinishTime_Str

            clsGateICheckSheetDB.InsertReleased(ls_ProjectID, ls_GroupID, ls_Commodity, ls_Group_Comodity, ls_Buyer, ls_Date, ls_Startime_Str, ls_FinishTime_Str, ls_MeetingRoom, pUser, ls_StatusRelease, pErr)
            btnSubmitKSH.ClientEnabled = False
            If pErr <> "" Then
                Exit For
            End If
        Next iLoop
        Grid.EndUpdate()
    End Sub

    Private Sub Grid_CustomButtonCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs) Handles Grid.CustomButtonCallback
        Dim grid As ASPxGridView = CType(sender, ASPxGridView)
        Dim prjtype As String = grid.GetRowValues(e.VisibleIndex, "Project_Type").ToString()
        Dim prjid As String = grid.GetRowValues(e.VisibleIndex, "Project_ID").ToString()
        Dim grpid As String = grid.GetRowValues(e.VisibleIndex, "Group_ID").ToString()
        Dim comm As String = grid.GetRowValues(e.VisibleIndex, "Commodity").ToString()
        Dim byr As String = grid.GetRowValues(e.VisibleIndex, "Buyer").ToString()
        Dim grpcom As String = grid.GetRowValues(e.VisibleIndex, "Group_Comodity").ToString()

        If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcom) = "Completed" Then
            Response.Redirect(String.Format("GetCheckSheetConfirm.aspx?projecttype=" + prjtype + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&groupcommodity=" + grpcom + "&buyer=" + byr + "&pagetype=release"))
        Else
            Response.Redirect(String.Format("GetCheckSheetView.aspx?projecttype=" + prjtype + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&groupcommodity=" + grpcom + "&buyer=" + byr + "&pagetype=release"))

        End If

    End Sub


    Public Sub ASPxButton1_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim button As DevExpress.Web.ASPxEditors.ASPxButton = CType(sender, DevExpress.Web.ASPxEditors.ASPxButton)
        Dim templateContainer As GridViewDataRowTemplateContainer = CType(button.NamingContainer, GridViewDataRowTemplateContainer)
        'Dim keyValue As String = Grid.GetRowValues(templateContainer.VisibleIndex, "EmployeeID").ToString()
        Dim prjtype As String = Grid.GetRowValues(templateContainer.VisibleIndex, "Project_Type").ToString()
        Dim prjid As String = Grid.GetRowValues(templateContainer.VisibleIndex, "Project_ID").ToString()
        Dim grpid As String = Grid.GetRowValues(templateContainer.VisibleIndex, "Group_ID").ToString()
        Dim comm As String = Grid.GetRowValues(templateContainer.VisibleIndex, "Commodity").ToString()
        Dim byr As String = Grid.GetRowValues(templateContainer.VisibleIndex, "Buyer").ToString()
        Dim grpcom As String = Grid.GetRowValues(templateContainer.VisibleIndex, "Group_Comodity").ToString()

        If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcom) = "Completed" Then
            Response.Redirect(String.Format("GetCheckSheetConfirm.aspx?projecttype=" + prjtype + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&groupcommodity=" + grpcom + "&buyer=" + byr + "&pagetype=release"))
        Else
            Response.Redirect(String.Format("GetCheckSheetView.aspx?projecttype=" + prjtype + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&groupcommodity=" + grpcom + "&buyer=" + byr + "&pagetype=release"))

        End If

    End Sub

    'Protected Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
    '    'If e.DataColumn.FieldName = "AllowCheck" Then
    '    If e.GetValue("AllowCheck") = "1" Then
    '        'e.Cell.Text = ""
    '        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
    '        e.Cell.Attributes.Add("readonly", "true")
    '    End If
    '    'End If
    'End Sub

#End Region


End Class
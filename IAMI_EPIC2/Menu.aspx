﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Menu.aspx.vb" Inherits="IAMI_EPIC2.Menu" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGlobalEvents" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    //toastr.options.timeOut = 1000;
                    //toastr.options.extendedTimeOut = 0;
                    //toastr.options.fadeOut = 250;
                    //toastr.options.fadeIn = 250;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    //toastr.options.timeOut = 1000;
                    //toastr.options.extendedTimeOut = 0;
                    //toastr.options.fadeOut = 250;
                    //toastr.options.fadeIn = 250;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    //toastr.options.timeOut = 1000;
                    //toastr.options.extendedTimeOut = 0;
                    //toastr.options.fadeOut = 250;
                    //toastr.options.fadeIn = 250;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function SelectRate(s, e) {
            txtRate.SetText(cboTax.GetSelectedItem().GetColumnText(2));
        }

        function OnGridFocusedRowChanged() {
            gridMenu.GetRowValues(gridMenu.GetFocusedRowIndex(), 'CatererID;MenuID;MenuName;Calorie;Description;Price;TaxCode;ActiveStatus;MenuType;MenuCategory;RecommendedStatus;SpicyStatus', OnGetRowValuesMenu);
        }
        function OnGetRowValuesMenu(values) {
            cboCaterer.SetText(values[0].trim());
            txtMenuID.SetText(values[1].trim());
            txtMenuName.SetText(values[2].trim());
            txtCalorie.SetText(values[3].trim());
            txtDesc.SetText(values[4].trim());
            txtPrice.SetText(values[5].trim());
            cboTax.SetText(values[6].trim());
            chkActive.SetValue(values[7].trim());
            cboType.SetValue(values[8].trim());
            txtRate.SetText(cboTax.GetSelectedItem().GetColumnText(2));
            cboCategory.SetValue(values[9].trim());
            chkSpecial.SetValue(values[10].trim());
            chkSpicy.SetValue(values[11].trim());

            btnSave.SetText('Update');
            btnSave.SetEnabled(true);
            btnDelete.SetEnabled(true);
            btnClear.SetEnabled(true);
                    
        }

        function DisableText(s, e) {
            txtRate.SetEnabled(false);
            txtRate.GetMainElement().style.backgroundColor = 'WhiteSmoke';
            txtRate.GetInputElement().style.backgroundColor = 'WhiteSmoke';
            txtRate.inputElement.style.color = 'Black';

            txtCatererName.SetEnabled(false);
            txtCatererName.GetMainElement().style.backgroundColor = 'WhiteSmoke';
            txtCatererName.GetInputElement().style.backgroundColor = 'WhiteSmoke';
            txtCatererName.inputElement.style.color = 'Black';

            txtCatererID.SetEnabled(false);
            txtCatererID.GetMainElement().style.backgroundColor = 'WhiteSmoke';
            txtCatererID.GetInputElement().style.backgroundColor = 'WhiteSmoke';
            txtCatererID.inputElement.style.color = 'Black';

            txtMenuID.SetText('(NEW)');
            txtMenuID.GetMainElement().style.backgroundColor = 'WhiteSmoke';
            txtMenuID.GetInputElement().style.backgroundColor = 'WhiteSmoke';
            txtMenuID.inputElement.style.color = 'Black';

            txtCatererID.SetText(cboCaterer.GetText());
            txtMenuID.SetText('(NEW)');
            txtMenuName.SetText('');
            txtCalorie.SetText('');
            txtPrice.SetText('');
            txtDesc.SetText('');
            txtRate.SetText('');
            cboType.SetText('');
            chkActive.SetChecked(true);
            cboTax.SetText('');
            cboType.SetText('');
            chkSpecial.SetChecked(false);
            chkSpicy.SetChecked(false);
            txtMenuName.Focus();

            gridMenu.SetFocusedRowIndex(-1);

            btnSave.SetText('Save');
            btnSave.SetEnabled(true);
            btnDelete.SetEnabled(false);
            btnClear.SetEnabled(false);            
        }


        function NewData(s, e) {
            if (cboCaterer.GetText() == '') {
                toastr.warning('Please select Caterer ID!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                e.processOnServer = false;
                return;
            }

            txtCatererID.SetText(cboCaterer.GetText());
            txtMenuID.SetText('(NEW)');
            txtMenuName.SetText('');
            txtCalorie.SetText('');
            txtPrice.SetText('');
            txtDesc.SetText('');
            txtRate.SetText('');
            cboType.SetText('');
            cboCategory.SetText('');
            chkActive.SetChecked(true);
            cboTax.SetText('');
            chkSpecial.SetChecked(false);
            chkSpicy.SetChecked(false);
            txtMenuName.Focus();

            gridMenu.SetFocusedRowIndex(-1);
            btnNew.SetEnabled(true);
            btnSave.SetText('Save');
            btnSave.SetEnabled(true);
            btnDelete.SetEnabled(false);
            btnClear.SetEnabled(false);
        }
    </script>

<script type="text/javascript">
    function LoadImage() {
        lblFile.SetText("");
        var img = document.getElementById("imgMenu");
        img.src = "img/noimage.png";
        var input = document.getElementById("uploader1");
        var fReader = new FileReader();
        fReader.readAsDataURL(input.files[0]);
        fReader.onloadend = function (event) {
            lblFile.SetText(input.files[0].name);
            img.src = event.target.result;
        }
    }

    function ClearImage(s, e) {
        var img = document.getElementById("imgMenu");
        img.src = "img/noimage.png";
        document.getElementById("uploader1").value = "";
    } 

</script>

    <style type="text/css">
        .style1
        {
            height: 19px;
        }
                
        .style2
        {
            width: 1%;
        }
                
        .style3
        {
            height: 28px;
        }
                
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

<table width="100%" style="padding: 2px; border: 1px solid silver; margin-bottom: 10px;">
    <tr style="height: 50px">
        <td style=" width:60px; padding:0px 0px 0px 10px">
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                Text="Caterer">
            </dx:ASPxLabel>
        </td>
        <td align="left" style="width: 105px">
    <dx:ASPxComboBox ID="cboCaterer" runat="server" ClientInstanceName="cboCaterer"
                        Width="100px" Font-Names="Segoe UI" DataSourceID="ss_Caterer" TextField="TaxCode"
                        ValueField="CatererID" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                        txtCatererID.SetText(cboCaterer.GetText());
                            txtCatererName.SetText(cboCaterer.GetSelectedItem().GetColumnText(1));
                            txtMenuID.SetText('(NEW)');
                            txtMenuName.SetText('');
                            txtCalorie.SetText('');
                            txtPrice.SetText('');                                    
                            txtDesc.SetText('');
                            txtRate.SetText('');
                            cboTax.SetText('');
                            chkActive.SetChecked(true);
                            chkSpecial.SetChecked(false);
                            chkSpicy.SetChecked(false);
                            cboType.SetText('');
                            cboCategory.SetText('');

                            gridMenu.PerformCallback('load|' + cboCaterer.GetText()  + '|' + cboFilterType.GetValue() + '|' + cboFilterActive.GetValue());
                            gridMenu.SetFocusedRowIndex(-1);

                            btnSave.SetText('Save');
                            btnSave.SetEnabled(true);
                            btnDelete.SetEnabled(false);
                            btnClear.SetEnabled(false);
}" />
                        <Columns>
                            <dx:ListBoxColumn Caption="ID" FieldName="CatererID" Width="50px" />
                            <dx:ListBoxColumn Caption="Name" FieldName="CatererName" Width="250px" />
                        </Columns>
                        <LoadingPanelStyle ImageSpacing="5px">
                        </LoadingPanelStyle>
                    </dx:ASPxComboBox>
        </td>
        <td style="width: 210px">

                            <dx:ASPxTextBox ID="txtCatererName" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Width="250px" ClientInstanceName="txtCatererName" MaxLength="50" 
                                Theme="Office2010Silver" ReadOnly="True" ClientEnabled="False">
                            </dx:ASPxTextBox>
                        
        </td>
        <td class="inbox-data-from" style="width: 20px">

                            &nbsp;</td>
        <td class="inbox-data-from" style="width: 70px">

                            <dx:ASPxLabel ID="ASPxLabel16" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Menu Type" Width="60px" Height="16px">
                            </dx:ASPxLabel>
                        
        </td>
        <td style="width: 100px" align="left">

                            <dx:ASPxComboBox ID="cboFilterType" runat="server" Font-Names="Segoe UI" 
                                Font-Size="9pt" Width="100px" ClientInstanceName="cboFilterType" 
                                Theme="Office2010Black" SelectedIndex="0">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                        txtCatererID.SetText(cboCaterer.GetText());
                            txtCatererName.SetText(cboCaterer.GetSelectedItem().GetColumnText(1));
                            txtMenuID.SetText('(NEW)');
                            txtMenuName.SetText('');
                            txtCalorie.SetText('');
                            txtPrice.SetText('');                                    
                            txtDesc.SetText('');
                            txtRate.SetText('');
                            cboTax.SetText('');
                            chkActive.SetChecked(true);
                            chkSpecial.SetChecked(false);
                            chkSpicy.SetChecked(false);
                            cboType.SetText('');
                            cboCategory.SetText('');

                            gridMenu.PerformCallback('load|' + cboCaterer.GetText() + '|' + cboFilterType.GetValue() + '|' + cboFilterActive.GetValue());
                            gridMenu.SetFocusedRowIndex(-1);

                            btnSave.SetText('Save');
                            btnSave.SetEnabled(true);
                            btnDelete.SetEnabled(false);
                            btnClear.SetEnabled(false);
                            }" />
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="CANTEEN" Value="0" />
                                    <dx:ListEditItem Text="PRONTO" Value="1" />
                                    <dx:ListEditItem Text="SET MENU" Value="2" />
                                </Items>
                            </dx:ASPxComboBox>

        </td>
        <td style="width: 20px">

                            &nbsp;</td>
        <td style="width: 85px">

                            <dx:ASPxLabel ID="ASPxLabel17" runat="server" 
                Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Active Status" Width="80px" Height="16px">
                            </dx:ASPxLabel>
                        
        </td>
        <td style="width: 100px">

                            <dx:ASPxComboBox ID="cboFilterActive" runat="server" Font-Names="Segoe UI" 
                                Font-Size="9pt" Width="100px" ClientInstanceName="cboFilterActive" 
                                Theme="Office2010Black" SelectedIndex="0">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                        txtCatererID.SetText(cboCaterer.GetText());
                            txtCatererName.SetText(cboCaterer.GetSelectedItem().GetColumnText(1));
                            txtMenuID.SetText('(NEW)');
                            txtMenuName.SetText('');
                            txtCalorie.SetText('');
                            txtPrice.SetText('');                                    
                            txtDesc.SetText('');
                            txtRate.SetText('');
                            cboTax.SetText('');
                            chkActive.SetChecked(true);
                            chkSpecial.SetChecked(false);
                            chkSpicy.SetChecked(false);
                            cboType.SetText('');
                            cboCategory.SetText('');

                            gridMenu.PerformCallback('load|' + cboCaterer.GetText() + '|' + cboFilterType.GetValue() + '|' + cboFilterActive.GetValue());
                            gridMenu.SetFocusedRowIndex(-1);

                            btnSave.SetText('Save');
                            btnSave.SetEnabled(true);
                            btnDelete.SetEnabled(false);
                            btnClear.SetEnabled(false);
                            }" />
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="ALL" Value="0" />
                                    <dx:ListEditItem Text="Active" Value="1" />
                                    <dx:ListEditItem Text="Inactive" Value="2" />
                                </Items>
                            </dx:ASPxComboBox>

        </td>
        <td></td>
    </tr>
</table>

<table width="100%">
    <tr style="height: 320px">
        <td style="width:50%">
         <dx:ASPxGridView ID="gridMenu" runat="server" AutoGenerateColumns="False" ClientIDMode="Predictable"
                                ClientInstanceName="gridMenu" Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="MenuID"
                                Theme="Office2010Black" Width="100%" UseFormLayout="true"  > 

<SettingsBehavior AllowSort="False" AllowFocusedRow="True" AllowSelectByRowClick="True" 
                                    ColumnResizeMode="Control" EnableRowHotTrack="True"></SettingsBehavior>

<Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="300"></Settings>

                                <ClientSideEvents EndCallback="OnEndCallback" FocusedRowChanged="function(s, e) {OnGridFocusedRowChanged();}" RowClick="function(s, e) {OnGridFocusedRowChanged();}" />
                                <Columns>

                <dx:GridViewDataTextColumn Caption="Caterer ID" FieldName="CatererID"
                    VisibleIndex="1" Width="80px" Settings-AutoFilterCondition="Contains" Visible="False">
                    <PropertiesTextEdit MaxLength="15" Width="150px">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>

<Settings AutoFilterCondition="Contains"></Settings>

                    <FilterCellStyle Paddings-PaddingRight="4px" BackColor="#f5f5f5">
<Paddings PaddingRight="4px"></Paddings>
                    </FilterCellStyle>
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle">
<Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Menu ID" FieldName="MenuID"
                    VisibleIndex="2" Width="80px" Settings-AutoFilterCondition="Contains">
                    <PropertiesTextEdit MaxLength="15" Width="150px">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>

<Settings AutoFilterCondition="Contains"></Settings>

                    <FilterCellStyle Paddings-PaddingRight="4px" BackColor="#f5f5f5">
<Paddings PaddingRight="4px"></Paddings>
                    </FilterCellStyle>
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle">
<Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                </dx:GridViewDataTextColumn>


                <dx:GridViewDataTextColumn Caption="Menu Name" FieldName="MenuName"
                    VisibleIndex="3" Width="230px" Settings-AutoFilterCondition="Contains">
                    <PropertiesTextEdit MaxLength="25" Width="150px">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>

<Settings AutoFilterCondition="Contains"></Settings>

                    <FilterCellStyle Paddings-PaddingRight="4px" BackColor="#f5f5f5">
<Paddings PaddingRight="4px"></Paddings>
                    </FilterCellStyle>
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle">
<Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                </dx:GridViewDataTextColumn>                                    


                <dx:GridViewDataTextColumn Caption="Calorie" FieldName="Calorie"
                    VisibleIndex="5" Width="90px" Settings-AutoFilterCondition="Contains" Visible="False">
                    <PropertiesTextEdit MaxLength="25" Width="90px">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>

<Settings AutoFilterCondition="Contains"></Settings>

                    <FilterCellStyle Paddings-PaddingRight="4px" BackColor="#f5f5f5">
<Paddings PaddingRight="4px"></Paddings>
                    </FilterCellStyle>
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle">
<Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                </dx:GridViewDataTextColumn>


                <dx:GridViewDataTextColumn Caption="Price" FieldName="Price"
                    VisibleIndex="7" Settings-AutoFilterCondition="Contains" Name="Price" Visible="False">
<Settings AutoFilterCondition="Contains"></Settings>
                </dx:GridViewDataTextColumn>


                                    <dx:GridViewDataTextColumn FieldName="TaxCode" VisibleIndex="8" Name="TaxCode" 
                                        Caption="TaxCode" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="9" 
                                        Name="Description" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="ActiveStatus" 
                                        VisibleIndex="10" Name="ActiveStatus" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataBinaryImageColumn FieldName="Picture" 
                                        VisibleIndex="4" Width="200px">
                                        <PropertiesBinaryImage ImageHeight="100px">
                                            
                                        </PropertiesBinaryImage>
                                    </dx:GridViewDataBinaryImageColumn>
                                    <dx:GridViewDataTextColumn Caption="MenuType" FieldName="MenuType" 
                                        Visible="False" VisibleIndex="13">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="MenuCategory" FieldName="MenuCategory" 
                                        Visible="False" VisibleIndex="14">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Special" FieldName="RecommendedStatus" 
                                        Visible="False" VisibleIndex="11">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Spicy" FieldName="SpicyStatus" 
                                        Visible="False" VisibleIndex="12">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSort="False"
                                    ColumnResizeMode="Control" EnableRowHotTrack="True" />
                                <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm" />
                                <SettingsPager NumericButtonCount="10" PageSize="11" AlwaysShowPager="True">
                                </SettingsPager>
                                <Settings ShowVerticalScrollBar="True" ShowFilterRow="True" VerticalScrollableHeight="300" />

                                <Styles>
                                    <Header>
                                        <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                    </Header>
                                </Styles>

                                <StylesEditors ButtonEditCellSpacing="0">
                                    <ProgressBar Height="21px">
                                    </ProgressBar>
                                </StylesEditors>

                            </dx:ASPxGridView>
        </td>
        <td class="style2">
        
        </td>
        <td style="width:49%">
            <table width="100%" 
                
                
                style="border: 1px solid silver; height:400px; top: 0px; margin-right: 10px;">
                <tr style="height:15px">
                    <td colspan="12"></td>
                </tr>
                    <tr style="height:28px">
                        <td style="width:10px">
                        
                        </td>
                        <td style="width:120px">
                        
                            <dx:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Caterer ID" Width="100px">
                            </dx:ASPxLabel>
                        
                        </td>
                        <td>
                        
                        </td>
                        <td colspan="3">
                        
                            <dx:ASPxTextBox ID="txtCatererID" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Width="60px" ClientInstanceName="txtCatererID" MaxLength="50" 
                                Theme="Office2010Silver" ReadOnly="True" ClientEnabled="False">
                            </dx:ASPxTextBox>
                        
                        </td>
                        <td colspan="6">


                        </td>
                    </tr>
                    <tr style="height:28px">
                        <td align="left" width="30px">
                            &nbsp;
                        </td>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Menu ID">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" width="10px">
                            &nbsp;
                        </td>
                        <td align="left" colspan="3">
                            <dx:ASPxTextBox ID="txtMenuID" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Width="60px" ClientInstanceName="txtMenuID" MaxLength="50" 
                                Theme="Office2010Silver" ReadOnly="True">
                            </dx:ASPxTextBox>
                        </td>
                        <td colspan="6">
                            &nbsp;</td>
                    </tr>
                    <tr style="height:28px">
                        <td align="left" width="30px" class="style1">
                            &nbsp;
                        </td>
                        <td align="left" class="style1">
                            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Menu Name" Width="100px">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" width="10px" class="style1">
                            &nbsp;
                        </td>
                        <td align="left" colspan="9" class="style1">
                            <dx:ASPxTextBox ID="txtMenuName" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Width="250px" ClientInstanceName="txtMenuName" Theme="Office2010Silver" 
                                MaxLength="25">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr style="height:28px">
                        <td align="left" width="30px">
                            &nbsp;
                        </td>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Calorie">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" width="10px">
                            &nbsp;
                        </td>
                        <td align="left" colspan="9">
                            <dx:ASPxTextBox ID="txtCalorie" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Width="60px" ClientInstanceName="txtCalorie" Theme="Office2010Silver" 
                                HorizontalAlign="Right" MaxLength="4">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>                    

                    <tr>
                        <td align="left" width="30px" class="style3">
                            &nbsp;
                        </td>
                        <td align="left" class="style3">
                            <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Price (RM)">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" width="10px" class="style3">
                            &nbsp;
                        </td>
                        <td align="left" colspan="9" class="style3">
                            <dx:ASPxTextBox ID="txtPrice" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Width="60px" ClientInstanceName="txtPrice" Theme="Office2010Silver" 
                                HorizontalAlign="Right" MaxLength="6">
                                <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..999g&gt;.&lt;00..99&gt;" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>

                    <tr style="height:28px">
                        <td align="left" width="30px">
                            &nbsp;
                        </td>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Category">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" width="10px">
                            &nbsp;
                        </td>
                        <td align="left" colspan="4">

                            <dx:ASPxComboBox ID="cboCategory" runat="server" Font-Names="Segoe UI" 
                                Font-Size="9pt" Width="100px" ClientInstanceName="cboCategory" 
                                Theme="Office2010Black">
                                <Items>
                                    <dx:ListEditItem Text="FOOD" Value="0" />
                                    <dx:ListEditItem Text="DRINKS" Value="1" />
                                    <dx:ListEditItem Text="OTHER" Value="2" />
                                </Items>
                            </dx:ASPxComboBox>

                        </td>
                        <td align="left" colspan="3">
                            &nbsp;</td>
                        <td align="left" colspan="2">
                            &nbsp;</td>
                    </tr>

                    <tr style="height:28px">
                        <td align="left" width="30px">
                            &nbsp;
                        </td>
                        <td align="left" style="width: 120px">
                            <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Tax Code">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" width="10px">
                           &nbsp;
                        </td>
                        <td align="left" style="width: 90px">
                            <dx:ASPxComboBox ID="cboTax" runat="server" ClientInstanceName="cboTax"
                                Width="100px" Font-Names="Segoe UI" DataSourceID="ss_Tax" TextField="TaxCode"
                                ValueField="TaxCode" TextFormatString="{0}" Font-Size="9pt" 
                                Theme="Office2010Black" DropDownStyle="DropDown" 
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                                Height="25px">
                                <ClientSideEvents SelectedIndexChanged="SelectRate" />
                                <Columns>
                                    <dx:ListBoxColumn Caption="Tax Code" FieldName="TaxCode" Width="50px" />
                                    <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="200px" />
                                    <dx:ListBoxColumn Caption="Rate" FieldName="Rate" Width="50px" />
                                </Columns>
                                <LoadingPanelStyle ImageSpacing="5px">
                                </LoadingPanelStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="right" style="width: 40px">
                            <dx:ASPxLabel ID="ASPxLabel12" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Rate" Width="80px">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width: 120px">
                            <dx:ASPxTextBox ID="txtRate" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Width="60px" ClientInstanceName="txtRate" Theme="Office2010Silver" HorizontalAlign="Right" ClientEnabled="False">
                                <ClientSideEvents Init="DisableText" />
                            </dx:ASPxTextBox>
                        </td>
                        <td style="witdth: 60px;" align="center" colspan="2">
                            &nbsp;</td>
                        <td style="width: 60px" align="left" colspan="3">
                            &nbsp;</td>
                        <td style="width:120px">
                        </td>
                    </tr>

                    <tr style="height:28px">
                        <td align="left" width="30px">
                            &nbsp;
                        </td>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Description">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" width="10px">
                            &nbsp;
                        </td>
                        <td align="left" colspan="9">
                            <dx:ASPxTextBox ID="txtDesc" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Width="285px" ClientInstanceName="txtDesc" MaxLength="50" 
                                Theme="Office2010Silver">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr style="height:28px">
                        <td align="left" width="30px">
                            &nbsp;
                        </td>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Menu Type">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" width="10px">
                            &nbsp;
                        </td>
                        <td align="left" colspan="3">

                            <dx:ASPxComboBox ID="cboType" runat="server" Font-Names="Segoe UI" 
                                Font-Size="9pt" Width="100px" ClientInstanceName="cboType" 
                                Theme="Office2010Black">
                                <Items>
                                    <dx:ListEditItem Text="CANTEEN" Value="0" />
                                    <dx:ListEditItem Text="PRONTO" Value="1" />
                                    <dx:ListEditItem Text="SET MENU" Value="2" />
                                </Items>
                            </dx:ASPxComboBox>

                        </td>
                        <td colspan="6">



                            &nbsp;</td>
                    </tr>
                    <tr style="height:25px">
                        <td align="left" width="30px">
                            &nbsp;
                        </td>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Status">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" width="10px">
                            &nbsp;
                        </td>
                        <td align="left">

                            <dx:ASPxCheckBox ID="chkActive" runat="server" Font-Names="Segoe UI" 
                                Font-Size="9pt" ClientInstanceName="chkActive"
                                Text="Active" ValueChecked="1" ValueType="System.String" ValueUnchecked="0" 
                                CheckState="Unchecked" Height="19px" Width="100px">
                            </dx:ASPxCheckBox>

                        </td>
                        <td align="left">

                            <dx:ASPxCheckBox ID="chkSpecial" runat="server" Font-Names="Segoe UI" 
                                Font-Size="9pt" ClientInstanceName="chkSpecial"
                                Text="Special" ValueChecked="1" ValueType="System.String" ValueUnchecked="0" 
                                CheckState="Unchecked" Height="19px" Width="100px">
                            </dx:ASPxCheckBox>

                        </td>
                        <td align="left">

                            <dx:ASPxCheckBox ID="chkSpicy" runat="server" Font-Names="Segoe UI" 
                                Font-Size="9pt" ClientInstanceName="chkSpicy"
                                Text="Spicy" ValueChecked="1" ValueType="System.String" ValueUnchecked="0" 
                                CheckState="Unchecked" Height="19px" Width="100px">
                            </dx:ASPxCheckBox>

                        </td>
                        <td colspan="3">&nbsp;</td>
                        <td colspan="3"></td>
                    </tr>       
                    <tr style="height:35px" valign="middle">
                        <td></td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel14" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Picture*">
                            </dx:ASPxLabel>
                        </td>
                        <td></td>
                        <td colspan="8" style="width: 500px">
                            <asp:FileUpload ID="uploader1" runat="server" Font-Names="Segoe UI" 
                                Font-Size="9pt" Width="250px" Height="20px" />
                        </td>       
                        <td>

                        </td>
                    </tr>             
                    <tr style="height:30px">
                    <td></td>
                        <td colspan="11">
                            <table>
                                <tr>
                                    <td>
                            <dx:ASPxLabel ID="ASPxLabel15" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="*Please use picture with landscape orientation" Font-Italic="True">
                            </dx:ASPxLabel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>   
                    <tr>
                        <td colspan="12">  </td>
                    </tr>                 
                </table>
        </td>
    </tr>
</table>
<table style="width: 100%">
    <tbody style="height: 40px">
    <tr valign="middle" style="height: 50px">
        <td>

            <dx:ASPxCallback ID="cbkSave" runat="server" ClientInstanceName="cbkSave">
                <ClientSideEvents Init="OnEndCallback" />
            </dx:ASPxCallback>

            <dx:ASPxCallback ID="cbkDelete" runat="server" ClientInstanceName="cbkDelete">
                <ClientSideEvents EndCallback="function(s, e) {
                                    var errMsg = s.cp_message;
                                    if (errMsg != '') {
                                        toastr.error(errMsg, 'Error');
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;
                                        e.processOnServer = false;
                                        return;
                                    }

                                    var pCatererID = cboCaterer.GetText();
                                    var pMenuType = cboFilterType.GetValue();
                                    var pMenuID = txtMenuID.GetText();

                                    var millisecondsToWait = 1000;
                                    setTimeout(function() {
                                        gridMenu.PerformCallback('afterdelete|'+ pCatererID + '|' + pMenuType);
                                    }, millisecondsToWait);

                                    
                                    txtMenuID.SetText('(NEW)');
                                    txtMenuName.SetText('');
                                    txtCalorie.SetText('');
                                    txtPrice.SetText('');
									cboTax.SetText('');
                                    chkActive.SetChecked(true);
                                    chkSpecial.SetChecked(false);
                                    chkSpicy.SetChecked(false);
                                    cboType.SetText('');
                                    txtDesc.SetText('');	
}" />
            </dx:ASPxCallback>

        </td>
        <td style="width:85px" align="right">

                            <dx:ASPxButton ID="btnNew" runat="server" AutoPostBack="False" Font-Names="Segoe UI"
                                Font-Size="9pt" Text="New" Theme="Office2010Silver" Width="80px" 
                                            ClientInstanceName="btnNew" Height="25px">
                                <ClientSideEvents Click="NewData" />
                            </dx:ASPxButton>

        </td>
        <td style="width:85px" align="right">

                            <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" ClientInstanceName="btnSave"
                                Font-Names="Segoe UI" Font-Size="9pt" Text="Save" Theme="Office2010Silver" 
                                            Width="80px" Height="25px">
                                <ClientSideEvents Click="function(s, e) {
	                                if (cboCaterer.GetText() == '') {
                                        toastr.warning('Please select Caterer ID!', 'Warning');
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;

                                        e.processOnServer = false;
                                        return;
                                    }

                                    if (txtMenuName.GetText() == '') {

                                        toastr.warning('Please input Menu Name!', 'Warning');
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;
                                        e.processOnServer = false;
                                        return;
                                    }	

                                    var price = Number(txtPrice.GetText().replace(',', '.'));
                                    if(isNaN(price)) price = 0;
                                    if (price == 0) {
                                        txtPrice.Focus();
                                        toastr.warning('Please input Price!', 'Warning');
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;
                                        e.processOnServer = false;
                                        return;
                                    }

	                                if (cboTax.GetText() == '') {
                                        toastr.warning('Please select Tax Code!', 'Warning');
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;

                                        e.processOnServer = false;
                                        return;
                                    }

	                                if (cboType.GetText() == '') {
                                        toastr.warning('Please select Menu Type!', 'Warning');
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;

                                        e.processOnServer = false;
                                        return;
                                    }
                                }" />
                            </dx:ASPxButton>

        </td>
        <td style="width:85px" align="right">

<dx:ASPxButton ID="btnDelete" runat="server" AutoPostBack="False" Font-Names="Segoe UI"
                                Font-Size="9pt" Text="Delete" Theme="Office2010Silver" 
                Width="80px" Height="25px" ClientInstanceName="btnDelete">
                                <ClientSideEvents Click="function(s, e) {
                                    if (txtMenuID.GetText() == '(NEW)') {
                                        toastr.warning('Please select data!', 'Warning');
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;
		                                e.processOnServer = false;
                                        return;
                                    } 
    
                                    var msg = confirm('Are you sure want to delete this data ?');                
                                    if (msg == false) {
                                        e.processOnServer = false;
                                        return;
                                    } 
                                    var pCatererID = cboCaterer.GetText();
                                    var pMenuType = cboFilterType.GetValue();
                                    var pMenuID = txtMenuID.GetText();

                                    cbkDelete.PerformCallback('delete|' + pCatererID + '|' + pMenuID + '|' + pMenuType);      

                    
                                }" />
                            </dx:ASPxButton>                            

        </td>
        <td style="width:111px" align="right">

                            <dx:ASPxButton ID="btnClear" runat="server" AutoPostBack="False" Font-Names="Segoe UI"
                                Font-Size="9pt" Text="Delete Picture" Theme="Office2010Silver" Width="100px" 
                                            ClientInstanceName="btnClear" Height="25px">
                                <ClientSideEvents Click="function(s, e) {
                                    if (gridMenu.GetFocusedRowIndex() == -1) {
                                        toastr.warning('Please select data!', 'Warning');
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;
		                                e.processOnServer = false;
                                        return;
                                    } 
    
                                    var msg = confirm('Are you sure want to delete picture?');                
                                    if (msg == false) {
                                        e.processOnServer = false;
                                        return;
                                    } 
                                    var pCatererID = cboCaterer.GetText();
                                    var pMenuID = txtMenuID.GetText();
                                    var pMenuType = cboFilterType.GetValue();

                                    gridMenu.PerformCallback('clear|' + pCatererID + '|' + pMenuID);      

                                    var millisecondsToWait = 1000;
                                    setTimeout(function() {
                                        gridMenu.PerformCallback('load|'+ pCatererID + '|' + pMenuType);
                                    }, millisecondsToWait);

                                    
                                    txtMenuID.SetText('(NEW)');
                                    txtMenuName.SetText('');
                                    txtCalorie.SetText('');
                                    txtPrice.SetText('');
									cboTax.SetText('');
                                    chkActive.SetChecked(true);
                                    chkSpecial.SetChecked(false);
                                    chkSpicy.SetChecked(false);
                                    cboType.SetText('');
                                    txtDesc.SetText('');                    
                                }" />
                            </dx:ASPxButton>

        </td>
    </tr>
</table>
    <table style="width: 100%; height: 10px;" hidden>
    <tr>
                        <td>
                            <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="ASPxCallback1">
                            </dx:ASPxCallback>
                        </td>
                        <td>
                                                    <dx:ASPxCallback ID="ASPxCallback2" runat="server" ClientInstanceName="ASPxCallback2">
                                <ClientSideEvents EndCallback="function(s, e) {
	                            txtCatererID.SetText(s.cpUserId);
                                txtMenuID.SetText(s.cpFullName);
                                chkActive.SetValue(s.cpActive);
                                chkActive.SetValue(s.cpSpecial);
                                chkActive.SetValue(s.cpSpicy);
                                txtDesc.SetText(s.cpDescription);
                            }" />
                            </dx:ASPxCallback>
                        </td>
                        <td>
                                <asp:SqlDataSource ID="ss_Tax" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                                
                                SelectCommand="SELECT rtrim(TaxCode) TaxCode, rtrim(Description) Description, Rate from Tax where ActiveStatus = '1'"></asp:SqlDataSource>                        
                        </td>
                        <td>
                                <asp:SqlDataSource ID="ss_Caterer" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                                
                                SelectCommand="SELECT rtrim(CatererID) CatererID, rtrim(CatererName) CatererName from Caterer where ActiveStatus = '1'"></asp:SqlDataSource>                        
                        </td>
    </tr>
    </table>
</asp:Content>



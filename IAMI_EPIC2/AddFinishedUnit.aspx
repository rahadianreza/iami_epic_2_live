﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddFinishedUnit.aspx.vb" Inherits="IAMI_EPIC2.AddFinishedUnit" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxImageZoom" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<script type="text/javascript" >

    function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtVariantCode.SetText(s.cpItemCode);
            setTimeout(function (){
                window.location.href = 'AddFinishedUnit.aspx?ID=' + s.cpItemCode;
            }, 1000);

        }
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtVariantCode.SetText(s.cpItemCode);
            setTimeout(function () {
                window.location.href = 'AddFinishedUnit.aspx?ID=' + s.cpItemCode;
            }, 1000);


        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtVariantCode.SetText('');
            txtVariantName.SetText('');
            txtMaterialFUNo.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtType.SetText('');
            txtVariantCode.Focus();

        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }


    function MessageDelete(s, e) {
        
        if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/ItemList.aspx";
            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/FinishedUnitList.aspx'
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                //alert(window.location.host);
                if (pathArray[1] == "AddFinishedUnit.aspx") {
                    window.location.href = window.location.origin + '/FinishedUnitList.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }

   
function OnBatchEditStartEditing(s, e) {
        currentColumnName = e.focusedColumn.fieldName;
        
        if (currentColumnName == 'FileType') {
            e.cancel = true;
            GridAtc.batchEditApi.EndEdit();

        } else if (currentColumnName == 'FileName') {
            e.cancel = true;
            GridAtc.batchEditApi.EndEdit();

        }
    }

    function InitGridAtc(s, e) {
        GridAtc.PerformCallback();
    }

    function LoadCompleted(s, e) {
        
        
        if (s.cpActionAfter == "DeleteAtc") {
            GridAtc.PerformCallback();
        }
    }

    function OnFileUploadStart(s, e) {
        btnAddFile.SetEnabled(false);
    }

    function OnTextChanged(s, e) {
        btnAddFile.SetEnabled(s.GetText() != "");
    }

    function OnFileUploadComplete(s, e) {
       
        btnAddFile.SetEnabled(s.GetText() != "");
       
        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }

        window.setTimeout(function () {
            delete s.cpType;
        }, 10);
    }
    function AddFile(){
        ucAtc.Upload();
        window.setTimeout(function () { GridAtc.PerformCallback(); }, 100);
    }

    function OnGetSelectedFieldValues(selectedValues) {
        //VALIDATE WHEN THERE IS NO SELECTED DATA
        if (selectedValues.length == 0) {
            toastr.warning('There is no selected data to delete!', 'Warning');
            return;
        }

        //GET FILE NAME
        var sValues;
        var result;
        result = "";

        for (i = 0; i < selectedValues.length; i++) {
            sValues = "";
            for (j = 0; j < selectedValues[i].length; j++) {
                sValues = sValues + selectedValues[i][j];
            }
            result = result + sValues + "|";
            //alert(result);
        }

        //CONFIRMATION
		//result = result.substring(0, result.length - 1);
		var C = confirm("Are you sure want to delete selected attachment (" + result.substring(0, result.length - 1) + ") ?");
        if (C == false) {
            return;
        }

        //KEEP FILE NAME INTO HIDDENFIELD
        hf.Set("DeleteAttachment", result);

        //EXECUTE TO DATABASE AFTER DELAY 300ms
        window.setTimeout(function () {
            
            cb.PerformCallback("DeleteAttachment");
        }, 300);
    }

    function DeleteAttachment() {
       
        //cbExist.PerformCallback('IsSubmitted');
        window.setTimeout(function () {
         
            if (btnSubmit.GetEnabled() == true) {
                hf.Set("DeleteAttachment", "");
                GridAtc.GetSelectedFieldValues("FileName", OnGetSelectedFieldValues);
            }

        }, 500);
    }
	

    if (s.cpActionAfter == "DeleteAtc") {
        GridAtc.PerformCallback();
    }

    var keyValue;
    function OnMoreInfoClick(element, key) {
        callbackPanel.SetContentHtml("");
        popup.ShowAtElement(element);
        keyValue = key;
    }

    function popup_Shown(s, e) {
        callbackPanel.PerformCallback(keyValue);
    }
</script>
    <style type="text/css">
        .tr-height
        {
            height:30px;
        }
        .td-col-l
        {
            padding-left:5px;
            width:120px;
            
        }
        .td-col-m
        {
            width:10px;
        }
        
        .td-col-f
        {
            width:50px;
        }
        
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
 <tr class="tr-height">
        <td class="td-col-f">&nbsp;</td>
        <%--<td style="width:50px">&nbsp;</td>--%>
        <td class="td-col-l">
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Variant Code">
            </dx:ASPxLabel>
        </td>
        <td class="td-col-m"> &nbsp;</td>
        <td > 

            <dx:ASPxTextBox ID="txtVariantCode" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Width="170px" ClientInstanceName="txtVariantCode" MaxLength="20"  ReadOnly="true" BackColor="#CCCCCC">
                <ClientSideEvents LostFocus="function(s, e) {
	            var iChars = &quot;!`@#$%^&amp;*()+=-[]\\\';,./{}|\&quot;:&lt;&gt;?~_&quot;;

                var data = txtVariantCode.GetText();
                for (var i = 0; i &lt; data.length; i++) {

                    if (iChars.indexOf(data.charAt(i)) != -1) {
                        alert('Special characters are not allowed.');
                        txtVariantCode.SetText('');
                        txtVariantCode.Focus();
                        return false;
                    }
                }
            }" />
            </dx:ASPxTextBox>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
 </tr>
 <tr class="tr-height">
        <td class="td-col-f">&nbsp;</td>
       <%-- <td style="width:50px">&nbsp;</td>--%>
        <td class="td-col-l">
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Variant Name">
            </dx:ASPxLabel>
        </td>
        <td class="td-col-m"> &nbsp;</td>
        <td> 
           <dx:ASPxTextBox ID="txtVariantName" runat="server" Font-Names="Segoe UI" Font-Size="9pt" AutoCompleteType="Disabled" 
                Width="280px" ClientInstanceName="txtVariantName" Height="16px" MaxLength="255">
            </dx:ASPxTextBox>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
 </tr>
 
 <tr class="tr-height">
        <td class="td-col-f">&nbsp;</td>
        <td class="td-col-l">
            <dx:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Material FU No">
            </dx:ASPxLabel>
        </td>
        <td class="td-col-m"> &nbsp;</td>
        <td> 
           <dx:ASPxTextBox ID="txtMaterialFUNo" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" AutoCompleteType="Disabled" 
                Width="280px" ClientInstanceName="txtMaterialFUNo" Height="16px" 
                MaxLength="255">
            </dx:ASPxTextBox>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
 </tr>
 
 <tr class="tr-height">
        <td class="td-col-f">&nbsp;</td>
       <%-- <td style="width:50px">&nbsp;</td>--%>
        <td class="td-col-l">
            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Type">
            </dx:ASPxLabel>
        </td>
        <td class="td-col-m"> &nbsp;</td>
        <td> 
            <%--                    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="ASPxCallback1">
                    <ClientSideEvents EndCallback="function (s, e) {
                        if (txtCardNo.GetEnabled(true)) {
                            txtRefundID.SetText(s.cpRefundID);
                        } else {
                            txtRefundID.SetText('');
                        }
                    }" CallbackComplete="OnEndCallback" />
                    </dx:ASPxCallback>--%>
           <dx:ASPxTextBox ID="txtType" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" AutoCompleteType="Disabled" 
                Width="280px" ClientInstanceName="txtType" Height="16px" 
                MaxLength="255">
            </dx:ASPxTextBox>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
 </tr>
 
 <tr class="tr-height">
        <td class="td-col-m">&nbsp;</td>
       <%-- <td style="width:50px">&nbsp;</td>--%>
        <td class="td-col-l">
            <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="UOM">
            </dx:ASPxLabel>
        </td>
        <td class="td-col-m"> &nbsp;</td>
        <td> 
           <dx:aspxcombobox ID="cboUOM" runat="server" ClientInstanceName="cboUOM"
                            Width="150px" Font-Names="Segoe UI" 
                            DataSourceID="SqlDataSource1" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">    
                            <ClientSideEvents SelectedIndexChanged="UOM" />                    
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx:aspxcombobox>
                                                
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
 </tr>
 <tr class="tr-height">
        <td class="td-col-f">&nbsp;</td>
       <%-- <td style="width:50px">&nbsp;</td>--%>
        <td class="td-col-l">
            <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI"  Width="100px" Font-Size="9pt" Text="Last IA Price">
            </dx:ASPxLabel>
        </td>
        <td class="td-col-m"> &nbsp;</td>
        <td> 
            <dx:ASPxTextBox ID="txtLastIAPrice" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Width="130px" ClientInstanceName="txtLastIAPrice" 
                MaxLength="15" ReadOnly="True" BackColor="#CCCCCC" >            
                <ClientSideEvents KeyUp="function(s, e) {
	var n = txtLastIAPrice.GetText();	
	txtPrice.SetText(String(n).replace(/(.)(?=(\d{3})+$)/g,'$1,'));
}" TextChanged="function(s, e) {
	var n = txtLastIAPrice.GetText();	
	txtPrice.SetText(String(n).replace(/(.)(?=(\d{3})+$)/g,'$1,'));
}" />
            <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999999&gt;.&lt;00..999&gt;" />            
            </dx:ASPxTextBox>
            <dx:ASPxTextBox ID="txtPrice" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                Width="130px" ClientInstanceName="txtPrice" MaxLength="15" 
                ClientVisible="False">            
            <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..99999999999&gt;.&lt;00..999&gt;" />            
            </dx:ASPxTextBox>
        </td>
        <td class="dxcpCurrentColor_Metropolis"></td>
        <td class="dxcpCurrentColor_Metropolis"></td>
 </tr>
 <tr class="tr-height">
        <td class="td-col-f">&nbsp;</td>
       <%-- <td style="width:50px">&nbsp;</td>--%>
        <td class="td-col-l">
            &nbsp;</td>
        <td class="td-col-m"> &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td>
        
        </td>
        <td>
        
            &nbsp;</td>
 </tr>
 </table>
 <br/>

<div style="padding: 5px 5px 5px 5px">
        <table style="width: 80%; border: 1px solid black; height: 100px;">  
            <tr>
                <td  style="height: 10px">
                </td>
                <td colspan="8"></td>
                <td></td>
            </tr>
            <tr>
                <td width="10px"></td>
                <td colspan="8">
                    <dx:aspxlabel ID="lblUploadImg" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Picture File :"></dx:aspxlabel>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="8">
                    <dx:ASPxGridView ID="GridAtc" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridAtc"
                        EnableTheming="True" Theme="Office2010Black"  
                         Settings-VerticalScrollBarMode="Visible"
                        Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="SequenceNo" 
                        width="80%"  >

                        <Columns>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Caption=" " Width="30px"></dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn Caption="Sequence No" FieldName="SequenceNo"
                                 VisibleIndex="1" Width="80px" Visible ="false">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="File Name" FieldName="FileName" VisibleIndex="2" >
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Image" FieldName="ImageBinary" EditFormSettings-Visible="False" VisibleIndex="3" Width="100px">
                                <DataItemTemplate >
                                    <a href="javascript:void(0);"
                                    onclick ="OnMoreInfoClick(this, '<%# Container.KeyValue %>')">
                                        <dx:ASPxBinaryImage ID="img" runat="server" Value='<%# Eval("ImageBinary") %>' Width="90px" Height="70px" ></dx:ASPxBinaryImage>
                                    </a>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                           
                           <%-- <dx:GridViewDataTextColumn Caption="Details" VisibleIndex="4" Width="150px"  EditFormSettings-Visible="False">
                                <DataItemTemplate >
                                <a href="javascript:void(0);" 
                                    onclick="OnMoreInfoClick(this, '<%# Container.KeyValue %>')">Show Picture...</a>
                                </DataItemTemplate>
                             </dx:GridViewDataTextColumn> --%>    
                         </Columns>
                         <SettingsPager PageSize="5" />
                         <SettingsEditing Mode="Batch">
                            <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                        </SettingsEditing>

                        <%--<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSort="False" EnableRowHotTrack="True" />
                         
                        <Settings  VerticalScrollableHeight="100" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>--%>
                        <Styles>
                            <Header CssClass="customHeader" HorizontalAlign="Center">
                            </Header>
                        </Styles>
                        <ClientSideEvents 
                            CallbackError="function(s, e) {e.handled = true;}"
                            Init="InitGridAtc"
                            EndCallback="LoadCompleted" 
                            BatchEditStartEditing="OnBatchEditStartEditing"
                        />

                    </dx:ASPxGridView>
                </td>
                <td></td>
               
            </tr>
            <tr>
                <td></td>
                <td colspan="8"></td>
                <td></td>
            </tr>
           
            <tr>
                <td></td>
                <td colspan="8" class="rowheight">
                    <dx:ASPxButton ID="btnDeleteAtc" runat="server" Text="Delete Selected Attachment" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="150px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDeleteAtc" Theme="Default">                        
                        <ClientSideEvents Click="DeleteAttachment" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td></td>
               
            </tr>
            <tr>
                <td></td>
                <td colspan="8"></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="8">
                    <dx:ASPxUploadControl ID="ucAtc" runat="server" ClientInstanceName="ucAtc"
                        Width="50%" Height="30px">
                        <ValidationSettings AllowedFileExtensions=".png,.jpg,.jpeg,.gif"></ValidationSettings>
                        <BrowseButton Text="Browse..."></BrowseButton>
                        <ClientSideEvents
                            FileUploadStart="OnFileUploadStart"
                            FilesUploadComplete="OnFileUploadComplete"
                            TextChanged="OnTextChanged"
                        />
                    </dx:ASPxUploadControl>
                    <dx:ASPxButton ID="btnAddFile" runat="server" Text="Add File" 
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnAddFile" Theme="Default">                        
                        <ClientSideEvents 
                            Click="AddFile" 
                        />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td></td>
            </tr>
            <tr class="rowheight">
                <td></td>
                <td colspan="8"><dx:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="*Available Document format : jpg " Font-Italic="True">
                             </dx:ASPxLabel><br><dx:ASPxLabel ID="ASPxLabel12" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="Maximum Size : Total 2 MB" Font-Italic="True">
                            </dx:ASPxLabel>
                </td>
                <td></td>
            </tr>
             <tr>
                <td></td>
                <td></td>
                 <td>
                     <dx:ASPxPopupControl ID="popup" ClientInstanceName="popup" runat="server" AllowDragging="true" Border-BorderStyle="None" HeaderStyle-BackColor="Transparent"
                        PopupHorizontalAlign="OutsideLeft"  Width="500px" Height="450px" PopupVerticalAlign="Middle" HeaderText="" HeaderStyle-Border-BorderStyle="None" BackColor="Transparent" ShowHeader="false"> 
                            
                        <ContentCollection>
                            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" >      
                                                  
                                <dx:ASPxCallbackPanel ID="callbackPanel" ClientInstanceName="callbackPanel" runat="server" 
                                    Width="210px" Height="210px" OnCallback="callbackPanel_Callback" RenderMode="Table">
                                    <PanelCollection >
                                        <dx:PanelContent ID="PanelContent1" runat="server" >
                                            <table class="InfoTable" style="width: 100%; border: 1px solid black; height: 100px; border-style:none; " >
                                                <tr>
                                                    <td style="border-style:none">
                                                        <dx:ASPxBinaryImage ID="edBinaryImage" runat="server" Width="490px" Height="440px" AlternateText="NO IMAGE ..." ImageAlign="Middle" Border-BorderStyle="None" CssClass="Image" >
                                                        </dx:ASPxBinaryImage>
                                                    </td>
                                                   
                                                </tr>
                                            </table>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxCallbackPanel>
                                
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                        <ClientSideEvents Shown="popup_Shown" />
                    </dx:ASPxPopupControl>
                </td>
            </tr>
            <tr>
                <td colspan="4"></td>
            </tr>
        </table>
</div>
<br />
 <tr style="height:25px;" >					   
  <td style="width:50px">&nbsp;</td>
        <td style="width:50px">&nbsp;</td>
        <td style="width:150px">
            &nbsp;</td>
        <td style="width:20px"> &nbsp;</td>
        <td style="width:50px"> 
       
        <table style="display:none">
        <tr style="height:25px">
        <td style="width:300px">                            
        <dx:ASPxBinaryImage ID="imgCover1" ClientInstanceName="ClientBinaryImage" Width="250" Height="250" ShowLoadingImage="true" runat="server">     
        </dx:ASPxBinaryImage>           
        </td>
        <td style="width:50px"> &nbsp; </td>
        <td style="width:200px">
           <dx:ASPxBinaryImage ID="imgCover2" ClientInstanceName="ClientBinaryImage" Width="250" Height="250" ShowLoadingImage="true" runat="server">     
        </dx:ASPxBinaryImage>        
        </td>
        <td style="width:50px"> &nbsp; </td>
        <td style="width:200px">
              <dx:ASPxBinaryImage ID="imgCover3" ClientInstanceName="ClientBinaryImage" Width="250" Height="250" ShowLoadingImage="true" runat="server">     
        </dx:ASPxBinaryImage>   
        </td>
		<td style="width:300px">                            
        <dx:ASPxBinaryImage ID="ASPxBinaryImage2" ClientInstanceName="ClientBinaryImage" Width="250" Height="250" ShowLoadingImage="true" runat="server">     
        </dx:ASPxBinaryImage>           
        </td>	 
        </tr>
        </table>
        </td>
        <td>
        
            &nbsp;</td>
        <td>
        
            &nbsp;</td>
 </tr>

 <tr style="height:25px">
        <td style="width:50px">&nbsp;</td>
        <td style="width:50px">&nbsp;</td>
        <td style="width:150px">
            &nbsp;</td>
        <td style="width:20px"> &nbsp;</td>
        <td style="width:50px"> 
       
        <table style="display:none">
        <tr style="height:25px">
        <td style="width:150px">                            
            <asp:FileUpload ID="uploader1" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Height="20px" Width="250px" Accept=".png,.jpg,.jpeg,.gif" />
        </td>
        <td style="width:150px">
     
            <asp:FileUpload ID="uploader2" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Height="20px" Width="250px" Accept=".png,.jpg,.jpeg,.gif"/>
        </td>
        <td style="width:150px">
                
            <asp:FileUpload ID="uploader3" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Height="20px" Width="250px" Accept=".png,.jpg,.jpeg,.gif"/>
        </td>
        </tr>
        </table>
        </td>
        <td>
        
            &nbsp;</td>
        <td>
        
            &nbsp;</td>
 </tr>


 
 <tr style="height:25px">
        <td style="width:50px">&nbsp;</td>
        <td style="width:50px">&nbsp;</td>
        <td style="width:150px">&nbsp;</td>
        <td style="width:20px"> &nbsp;</td>
        <td style="width:500px"> 
                    <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp;&nbsp;
                  <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnClear" Theme="Default" >                        
                        <ClientSideEvents Click="function(s, e) {
                                var msg = confirm('Are you sure want to cancel this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }	

                                //cbClear.PerformCallback();
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp;&nbsp;
                           <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnDelete" Theme="Default" Enabled="False"  >                        
                               <ClientSideEvents Click="function(s, e) {	                                                            
                                var msg = confirm('Are you sure want to delete this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }
                            
								cbDelete.PerformCallback();
                            
                            }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp;&nbsp;

                    <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnSubmit" Theme="Default" >                        
                        <ClientSideEvents Click="function(s, e) {
                                    
                                    //alert(cboCategory.GetSelectedIndex());

	                                if (txtVariantCode.GetText() == '') {
                                            toastr.warning('Please Input Part No!', 'Warning');
                                            txtVariantCode.Focus();
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
		                                    e.processOnServer = false;
                                            return;
	                                    }  

                                    else if (txtVariantName.GetText() == ''){    
                                            toastr.warning('Please Input Part Name!', 'Warning');
                                            txtVariantName.Focus();
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
		                                    e.processOnServer = false;
                                            return;
                                            }

                                    else if (txtMaterialFUNo.GetText() == ''){    
                                            toastr.warning('Please Input EPL Part No!', 'Warning');
                                            txtMaterialFUNo.Focus();
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
		                                    e.processOnServer = false;
                                            return;
                                        }
                                     else if (txtType.GetText() == ''){    
                                            toastr.warning('Please Input Variant!', 'Warning');
                                            txtType.Focus();
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
		                                    e.processOnServer = false;
                                            return;
                                        }

                                   else if (cboUOM.GetText() == ''){    
                                        toastr.warning('Please Input Select UOM!', 'Warning');
                                        cboUOM.Focus();
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;
		                                e.processOnServer = false;
                                        return;
                                    }
                                    
                                    var msg = confirm('Are you sure want to save this data ?');                
                                    if (msg == false) {
                                         e.processOnServer = false;
                                         return;
                                    }
                                        cbSave.PerformCallback('Submit');      
                            }"  />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
        </td>
	 <td>
        <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf"></dx:ASPxHiddenField>
    </td>
    <td colspan="6">              
                <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
                    <ClientSideEvents Init="MessageError" />                                           
                </dx:ASPxCallback>
    </td>
    <td>
    
        &nbsp;</td>
 
 
    <td colspan="6">              
                <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
                    <ClientSideEvents CallbackComplete="MessageDelete" />                                           
                </dx:ASPxCallback>
    </td>
    <td>
    
        &nbsp;</td>

    <td colspan="6">
    
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                    <ClientSideEvents Init="MessageError" />                                           
                </dx:ASPxCallback>

  <%--              <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                    <ClientSideEvents CallbackComplete="MessageError" />                                           
                </dx:ASPxCallback>--%>
    
           
            </td>
    <td>
    
        <dx:ASPxCallback ID="cb" runat="server" ClientInstanceName="cb">
                        <ClientSideEvents 
                            
                            CallbackComplete="LoadCompleted"
                        />
                    </dx:ASPxCallback></td>
        <td>&nbsp;<asp:SqlDataSource ID="SqlDataSource4" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            
                SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRType'"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            
                SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Category'"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="Select RTRIM(Par_Code) As Group_Code, RTRIM(Par_Description) As Group_Name From Mst_Parameter Where Par_Group = 'GroupItem'"></asp:SqlDataSource>
&nbsp;<asp:SqlDataSource ID="SqlDataSource1" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="Select Rtrim(Par_Code) AS Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'UOM'"></asp:SqlDataSource>
<%--                    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="ASPxCallback1">
                    <ClientSideEvents EndCallback="function (s, e) {
                        if (txtCardNo.GetEnabled(true)) {
                            txtRefundID.SetText(s.cpRefundID);
                        } else {
                            txtRefundID.SetText('');
                        }
                    }" CallbackComplete="OnEndCallback" />
                    </dx:ASPxCallback>--%>
        </td>
        <td style="width:20px">&nbsp;</td>
 </tr>
 </table>
</asp:Content>

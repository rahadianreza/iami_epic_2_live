﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="AddMasterTooling.aspx.vb" Inherits="IAMI_EPIC2.AddMasterTooling" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function MessageError(s, e) {
            if (s.cpMessage == "Data Saved Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                cboPeriod.SetText('');
                cboType.SetText('');
                cboMaterialCode.SetText('');
                txtMaterialName.SetText('');
                txtSpecification.SetText('');
                cboUoMUnitPrice.SetText('');
                txtUnitPrice.SetText('');
                cboUoMUnitConsump.SetText('');
                txtUnitConsump.SetText('');
                cboCategoryMaterial.SetText('');
                cboGroupItem.SetText('');
                cboCurrency.SetText('');
                cboMaterialCategory.SetText('');
            }
            else if (s.cpMessage == "Data Updated Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

            }
            else if (s.cpMessage == "Data Clear Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                cboPeriod.SetText('');
                cboType.SetText('');
                cboMaterialCode.SetText('');
                txtMaterialName.SetText('');
                txtSpecification.SetText('');
                cboUoMUnitPrice.SetText('');
                txtUnitPrice.SetText('');
                cboUoMUnitConsump.SetText('');
                txtUnitConsump.SetText('');
                cboCategoryMaterial.SetText('');
                cboGroupItem.SetText('');
                cboCurrency.SetText('');
                cboMaterialCategory.SetText('');
            }
            else if (s.cpMessage == "Data Cancel Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }

        }
        function GridLoad() {
            Grid.PerformCallback('refresh');
            tmpProjectType.SetText(cboProjectType.GetValue());
        };
        function fn_AllowonlyNumeric(s, e) {
            var theEvent = e.htmlEvent || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;

            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault)
                    theEvent.preventDefault();
            }
        };
        function MessageDelete(s, e) {

            if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                //window.location.href = "/ItemList.aspx";
                millisecondsToWait = 2000;
                setTimeout(function () {
                    var pathArray = window.location.pathname.split('/');
                    var url = window.location.origin + '/' + pathArray[1] + '/PRJList.aspx'
                    //window.location.href = url;
                    //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                    //alert(window.location.host);
                    if (pathArray[1] == "AddPRJList.aspx") {
                        window.location.href = window.location.origin + '/PRJList.aspx';
                    }
                    else {
                        window.location.href = url;
                    }
                }, millisecondsToWait);

            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function FilterGroup() {
            cboGroupItem.PerformCallback('filter|' + cboPRType.GetValue());
        }

        function FilterCategory() {
            cboCategory.PerformCallback('filter|' + cboGroupItem.GetValue());
        }
        function SetCode(s, e) {
            tmpProjectType.SetText(cboProjectType.GetValue());
        }

        document.getElementById('form').addEventListener('keypress', function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<br />
    <table style="width: 100%;">
        <tr style="height: 25px">
            <td style="width: 150px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Tooling ID">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td style="width: 100px;"> 
                <dx:ASPxTextBox ID="txtToolingId" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtToolingId" MaxLength="15">
                </dx:ASPxTextBox>
            </td>
            <td style="width:30px"></td>
            <td style="width: 150px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Tooling Name">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtToolingName" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtToolingName" MaxLength="50">
                </dx:ASPxTextBox>
            </td>
        </tr>

        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Asset No">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtAssetNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="170px" ClientInstanceName="txtAssetNo" MaxLength="15">
                </dx:ASPxTextBox>
            </td>
            <td></td>
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Number Of Process">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtNumberOfProcess" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="170px" ClientInstanceName="txtNumberOfProcess" MaxLength="10">
                </dx:ASPxTextBox>
            </td>
        </tr>

        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Contract No">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtContractNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtContractNo" MaxLength="30">
                </dx:ASPxTextBox>
            </td>
            <td></td>
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Dimesion">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtDimesion" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtDimesion" MaxLength="20">
                </dx:ASPxTextBox>
            </td>
        </tr>

        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Additional 1">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtAdditional1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtAdditional1" MaxLength="50">
                </dx:ASPxTextBox>
            </td>
            <td></td>
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Weight">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtWeight" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtWeight" MaxLength="9" HorizontalAlign="Right">
                     <MaskSettings Mask="<0..100000000g>" />
                </dx:ASPxTextBox>
            </td>
        </tr>

        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Additional 2">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtAdditional2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="170px" ClientInstanceName="txtAdditional2" MaxLength="50">
                </dx:ASPxTextBox>
            </td>
            <td></td>
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Price">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtPrice" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtPrice" MaxLength="9" HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>" />
                </dx:ASPxTextBox>
            </td>
        </tr>

        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Part No">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtPartNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtPartNo" MaxLength="10">
                </dx:ASPxTextBox>
            </td>
            <td></td>
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel14" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Model">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtModel" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtModel" MaxLength="20">
                </dx:ASPxTextBox>
            </td>
        </tr>

        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Part Name">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td colspan="5">
                <dx:ASPxTextBox ID="txtPartName" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="644px" ClientInstanceName="txtPartName" MaxLength="50">
                </dx:ASPxTextBox>
            </td>
        </tr>

        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel16" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="UoM">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtUoM" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtUoM" MaxLength="10">
                </dx:ASPxTextBox>
            </td>
            <td></td>
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel17" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Group">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtGroup" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtGroup" MaxLength="15">
                </dx:ASPxTextBox>
            </td>
        </tr>

         <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel18" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Qty">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtQty" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtQty" MaxLength="4" HorizontalAlign="Right">
                     <MaskSettings Mask="<0..100000000g>" />
                </dx:ASPxTextBox>
            </td>
            <td></td>
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel19" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Purchased Year">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtPurchasedYear" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtPurchasedYear" MaxLength="9">
                </dx:ASPxTextBox>
            </td>
        </tr>

        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
             
            </td>
            <td></td>
            <td>
               
            </td>
            <td></td>
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel21" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Investation No">
                </dx:ASPxLabel>
            </td>
            <td></td>
            <td>
                <dx:ASPxTextBox ID="txtInvestationNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                    Width="170px" ClientInstanceName="txtInvestationNo" MaxLength="15">
                </dx:ASPxTextBox>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table>
        <tr>
            <td style="padding-left:30px;">
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                 <dx:ASPxButton ID="btnUpdate" runat="server" Text="Update" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                 &nbsp;&nbsp;
                 <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnDelete" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                 &nbsp;&nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI" UseSubmitBehavior="false"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back to List" AutoPostBack="False" Font-Names="Segoe UI" UseSubmitBehavior="false"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
        </tr>     
    </table>

       <dx:ASPxCallback ID="cbDescriptions" ClientInstanceName="cbDescriptions" runat="server">
         <ClientSideEvents EndCallback="function(s,e) {
                txtMaterialName.SetValue(s.cp_Descs);
          }
          " />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                    <ClientSideEvents Init="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
            <ClientSideEvents Init="MessageError" />
        </dx:ASPxCallback>
</asp:Content>

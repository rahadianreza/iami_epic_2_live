﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class PO_ViewIAPriceReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UserID As String = ""
        Dim PR_Number As String = ""
        Dim CE_Number As String = ""
        Dim IA_Number As String = ""
        Dim IA_Revision As Integer

        UserID = Split(Request.QueryString("ID"), "|")(0)
        PR_Number = Split(Request.QueryString("ID"), "|")(1)
        CE_Number = Split(Request.QueryString("ID"), "|")(3)
        IA_Number = Split(Request.QueryString("ID"), "|")(6)
        IA_Revision = Split(Request.QueryString("ID"), "|")(7)
        up_LoadReport(IA_Number, IA_Revision, CE_Number)
    End Sub

    Private Sub up_LoadReport(pIANumber As String, pRev As Integer, pCENumber As String)
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptIAPrice

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_IA_Report '" & pCENumber & "' , '" & pIANumber & "' , " & pRev & ""

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "IAPrice_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report

                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim pPRNumber As String
        Dim pUserID As String
        pPRNumber = Split(Request.QueryString("ID"), "|")(1)
        pUserID = Split(Request.QueryString("ID"), "|")(0)

        Response.Redirect("~/PO_Notification.aspx?ID=" & pUserID & "|" & pPRNumber)

    End Sub

End Class
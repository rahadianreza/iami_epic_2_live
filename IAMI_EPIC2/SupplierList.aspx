﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SupplierList.aspx.vb" Inherits="IAMI_EPIC2.SupplierList" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
            <tr style="height: 50px">
                <td style=" width:100px; padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td style=" width:100px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td align="left">
                    
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Kabupaten'">
                    </asp:SqlDataSource>
                    &nbsp;
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Language'">
                    </asp:SqlDataSource>
                    &nbsp;
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Period'">
                    </asp:SqlDataSource>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                </dx:ASPxGridViewExporter>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div>
    <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" KeyFieldName="Supplier_Code" Theme="Office2010Black" 
                OnRowValidating="Grid_RowValidating" OnStartRowEditing="Grid_StartRowEditing"
                OnRowInserting="Grid_RowInserting" OnRowDeleting="Grid_RowDeleting" 
                OnAfterPerformCallback="Grid_AfterPerformCallback" Width="100%" 
                Font-Names="Segoe UI" Font-Size="9pt">
                <ClientSideEvents EndCallback="OnEndCallback" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Supplier Code" FieldName="Supplier_Code" 
                        VisibleIndex="1" Width="100px">
                        <PropertiesTextEdit MaxLength="15" Width="115px" ClientInstanceName="Supplier_Code">
                            <Style HorizontalAlign="Left">
                            </Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                        
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewCommandColumn
                        VisibleIndex="0" ShowEditButton="true" ShowClearFilterButton="true" 
                        Width="50px">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="Supplier_Name" 
                                        Width="250px" Caption="Supplier Name" VisibleIndex="2">
                    <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Supplier_Name">
                    <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>

                    
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn Caption="Address" FieldName="Address"
                        VisibleIndex="3" Width="350px">
                        <PropertiesTextEdit Width="350px" DisplayFormatInEditMode="True" ClientInstanceName="Address">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                       
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn Caption="Contact Person" FieldName="Contact_Person"
                        VisibleIndex="8" Width="100px">
                        <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="True" ClientInstanceName="Contact_Person">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
    <Paddings PaddingLeft="6px"></Paddings>
                        </HeaderStyle>
                        
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Email" FieldName="Email" VisibleIndex="9" 
                        Width="150px">
                        <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Email">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                        
                        
                    </dx:GridViewDataTextColumn>                
                    <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                        VisibleIndex="13" Width="150px">
                        <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                        </PropertiesTextEdit>
                        <EditFormSettings Visible="False" />
                        <Settings />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                        
                        <Settings AllowAutoFilter="False" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                        VisibleIndex="14" Width="100px">
                        <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                            EditFormatString="dd MMM yyyy">
                        </PropertiesDateEdit>
                        <EditFormSettings Visible="False" />
                        <Settings />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                        
                        <Settings AllowAutoFilter="False" />
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                        VisibleIndex="15" Width="150px">
                        <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                        </PropertiesTextEdit>
                        <EditFormSettings Visible="False" />
                        <Settings />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                        
                        <Settings AllowAutoFilter="False" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                        VisibleIndex="16" Width="100px">
                        <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                            EditFormatString="dd MMM yyyy">
                        </PropertiesDateEdit>
                        <EditFormSettings Visible="False" />
                        <Settings />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                        
                        <Settings AllowAutoFilter="False" />
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn Caption="Phone" FieldName="Phone" VisibleIndex="4" 
                        Width="100px">
                        <PropertiesTextEdit ClientInstanceName="Phone" DisplayFormatInEditMode="True" 
                            Width="100px">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                        
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Fax" FieldName="Fax" VisibleIndex="5" 
                        Width="100px">
                        <PropertiesTextEdit ClientInstanceName="Fax" DisplayFormatInEditMode="True" 
                            Width="100px">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                        
                    </dx:GridViewDataTextColumn>                   
                    <dx:GridViewDataTextColumn FieldName="Kota_Kabupaten" VisibleIndex="6" Width="120px"
                        Caption="City">                        
                        <Settings AutoFilterCondition="Contains" />
                        <EditFormSettings VisibleIndex="6" />
                        <HeaderStyle Paddings-PaddingLeft="7px" HorizontalAlign="Center" 
                            VerticalAlign="Middle">
                           <Paddings PaddingLeft="7px"></Paddings>
                        </HeaderStyle>
                        
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Country" FieldName="Country" 
                        VisibleIndex="7" Width="100px">
                        <PropertiesTextEdit ClientInstanceName="Country" DisplayFormatInEditMode="True" 
                            Width="100px">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                        
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Period" FieldName="Period_Code" 
                        VisibleIndex="10" Width="100px">
                        <PropertiesTextEdit ClientInstanceName="Period_Code" DisplayFormatInEditMode="True" 
                            Width="100px">
                        </PropertiesTextEdit>
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Language" FieldName="Language_Code" 
                        VisibleIndex="11" Width="120px">
                         <PropertiesTextEdit ClientInstanceName="Language_Code" DisplayFormatInEditMode="True" 
                            Width="100px">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                       
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Supplier Type" FieldName="SupplierType" 
                        VisibleIndex="12" Width="120px">
                        <PropertiesTextEdit ClientInstanceName="SupplierType" 
                            DisplayFormatInEditMode="True" Width="100px">
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                        
                    </dx:GridViewDataTextColumn>
                </Columns>

                <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                <SettingsPopup>
                    <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                </SettingsPopup>

                <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />

                <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                    <Header HorizontalAlign="Center">
                        <Paddings Padding="2px"></Paddings>
                    </Header>

                    <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                        <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                    </EditFormColumnCaption>
                </Styles>
                    
                <Templates>
                    <EditForm>
                        <div style="padding: 15px 15px 15px 15px">
                            <dx:ContentControl ID="ContentControl1" runat="server">
                                <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                    runat="server">
                                </dx:ASPxGridViewTemplateReplacement>
                            </dx:ContentControl>
                        </div>
                        <div style="text-align: left; padding: 5px 5px 5px 15px">
                            <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                runat="server">
                            </dx:ASPxGridViewTemplateReplacement>
                            <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                runat="server">
                            </dx:ASPxGridViewTemplateReplacement>
                        </div>
                    </EditForm>
                </Templates>
                    
             </dx:ASPxGridView>
    </div> 
</asp:Content>

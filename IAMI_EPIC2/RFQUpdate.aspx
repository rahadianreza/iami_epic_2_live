﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RFQUpdate.aspx.vb" Inherits="IAMI_EPIC2.RFQUpdate_aspx" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">

    function GetMessage(s, e) {
        //alert(s.cpMessage);
        if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            btnExcel.SetEnabled(false);
        }
        else if (s.cpMessage == "Grid Load Data Success") {
            btnExcel.SetEnabled(true);
        }
        else if (s.cpMessage == "backtolist") {
            //Grid.PerformCallback('gridload|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|' + cboRFQNumber.GetValue());
            txtBack.SetText("1");
            btnExcel.SetEnabled(true);
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
    }

    function FillComboRFQNumber() {
        cboRFQNumber.PerformCallback('filter|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText());
    }

    function LoadComboRFQNumber() {
        cboRFQNumber.PerformCallback('load|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText());
    }




</script>

<style type="text/css">
    .hidden-div
    {
        display: none;
    }
        
    .tr-all-height
    {
        height: 20px;
    }
        
    .td-col-left
    {
        width: 100px;
        padding: 0px 0px 0px 10px;
    }
        
    .td-col-left2
    {
        width: 140px;
    }
        
        
    .td-col-mid
    {
        width: 10px;
    }
    .td-col-right
    {
        width: 200px;
    }
    .td-col-free
    {
        width: 20px;
    }
        
    .div-pad-fil
    {
        padding: 5px 5px 5px 5px;
    }
        
    .td-margin
    {
        padding: 10px;
    }
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    

  <div style="padding: 5px 5px 5px 5px">
  <table style="width: 100%; border: 1px solid black; height: 100px;">  
         <tr style="height: 20px">
                <td class="td-col-left" colspan="11"></td>
         </tr>     
            <tr class="tr-all-height">
                <td class="td-col-left; td-margin">
                    <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="RFQ Date">
                    </dx1:aspxlabel>                 
                </td>
                <td class="td-col-mid"></td>
                <td class="td-col-left2">
                <dx:aspxdateedit ID="dtDateFrom" runat="server" Theme="Office2010Black" 
                                        Width="100px" AutoPostBack="false" ClientInstanceName="dtDateFrom"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:aspxdateedit>                       </td>
                <td class="td-col-mid">   
                    
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="To">
                    </dx1:aspxlabel>   
                                                     
                </td>
                <td class="td-col-mid"></td>
                <td class="td-col-right">
                <dx:aspxdateedit ID="dtDateTo" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateTo"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ClientSideEvents DateChanged="FillComboRFQNumber" />
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                    </dx:aspxdateedit>   
                    
                </td>
                <td colspan="5"></td>
                
         </tr>      
         <tr class="tr-all-height">
                <td class="td-col-left">
                    <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="RFQ Number">
                    </dx1:ASPxLabel>                 
                </td>
                <td class="td-col-mid"></td>
                <td colspan="9">           
  <dx1:ASPxComboBox ID="cboRFQNumber" runat="server" ClientInstanceName="cboRFQNumber"
                            Width="200px" Font-Names="Segoe UI"  TextField="RFQ_Number"
                            ValueField="RFQ_Number" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <ClientSideEvents Init="LoadComboRFQNumber" ValueChanged="function(s, e) {
		                        //Grid.PerformCallback('gridload|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|' + cboRFQNumber.GetValue());
      
                                if (txtBack.GetText() == '0') {
    
                                    Grid.PerformCallback('gridload|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|xxx');
                                }
                                else if (txtBack.GetText() == '1')  {
               
                                    Grid.PerformCallback('gridload|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|' + cboRFQNumber.GetValue());
                                    txtBack.SetText('0');
                                }
                                
                                btnExcel.SetEnabled(false);
}" />
                            <Columns>            
                            <dx:ListBoxColumn Caption="RFQ Number" FieldName="RFQ_Number" Width="100px" />
                       
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
            </tr>        
      <tr class="tr-all-height">
          <td colspan="11"></td>     
      </tr>     
      <tr class="tr-all-height">
                <td colspan="11" style=" padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnRefresh" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {

                            var startdate = dtDateFrom.GetDate(); // get value dateline date
                            var enddate = dtDateTo.GetDate(); // get value dateline date

                            if (startdate == null)
                            {
                                toastr.warning('Please input valid Start Date', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;                                
                            }
                            else if (enddate == null)
                            {
                                toastr.warning('Please input valid End Date', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;                                
                            }


                            var yearstartdate = startdate.getFullYear(); // where getFullYear returns the year (four digits)
                            var monthstartdate = startdate.getMonth(); // where getMonth returns the month (from 0-11)
                            var daystartdate = startdate.getDate();   // where getDate returns the day of the month (from 1-31)

                            if (daystartdate &lt; 10) {
                                daystartdate = '0'+ daystartdate  
                            }

                            if (monthstartdate &lt; 10) {
                                monthstartdate = '0'+ monthstartdate  
                            }

                            var vStartDate = yearstartdate + '-' + monthstartdate + '-' + daystartdate;

                            
                            var yearenddate = enddate.getFullYear(); // where getFullYear returns the year (four digits)
                            var monthenddate = enddate.getMonth(); // where getMonth returns the month (from 0-11)
                            var dayenddate = enddate.getDate();   // where getDate returns the day of the month (from 1-31)

                            if (dayenddate &lt; 10) {
                                dayenddate = '0'+ dayenddate  
                            }

                            if (monthenddate &lt; 10) {
                                monthenddate = '0'+ monthenddate  
                            }

                            var vEndDate = yearenddate + '-' + monthenddate + '-' + dayenddate;

                            if (vEndDate &lt; vStartDate) {
                                toastr.warning('End date must be bigger than Start Date', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;
                            }

                                                            
	                            Grid.PerformCallback('gridload|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|' + cboRFQNumber.GetValue());
                                txtBack.SetText('0');
                                //cbEnabled.PerformCallback('active|');
                            }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                     &nbsp;
                    <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnExcel" Theme="Default">                        
                        <ClientSideEvents Init="function(s, e) {
	btnExcel.SetEnabled(false);
}" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 </td>
            </tr>                                              
            <tr class="tr-all-height">
                <td colspan="11">&nbsp;</td>
                
            </tr>                                              
        </table>
    </div>
    <div class="hidden-div">
         <dx1:ASPxTextBox ID="txtBack" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="50px" ClientInstanceName="txtBack" MaxLength="30" 
                        Height="25px" >
            </dx1:ASPxTextBox>
    </div>
    <div>
         <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                         <ClientSideEvents Init="GetMessage" />
                                                        
          </dx:ASPxCallback>
          
            <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select RFQ_Number From RFQ_Master">
                    </asp:SqlDataSource>
                         <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
    </div>

    <div style="padding: 5px 5px 5px 5px">
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="RFQ_Set;PR_Number;Rev" >
             <ClientSideEvents CustomButtonClick="function(s, e) {
	                            if(e.buttonID == 'edit'){                     
                                        var rowKey = Grid.GetRowKey(e.visibleIndex);  
                                                            
	                                    window.location.href= 'RFQUpdateDetail.aspx?ID=' + rowKey + '|' + cboRFQNumber.GetText() + '|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText();
                                    }
                                }" EndCallback="GetMessage"  />
             <Columns>
                 <dx:GridViewDataTextColumn Caption="RFQ Set Number" FieldName="RFQ_Set" FixedStyle="Left"
                     VisibleIndex="1" Width="180px">
                  
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="RFQ Date" FieldName="RFQ_Date" FixedStyle="Left"
                     VisibleIndex="3">
                   </dx:GridViewDataTextColumn>
                 <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" FixedStyle="Left">
                     <CustomButtons>
                         <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                         </dx:GridViewCommandColumnCustomButton>
                     </CustomButtons>
                 </dx:GridViewCommandColumn>
                 <dx:GridViewDataTextColumn Caption="PR Number" FieldName="PR_Number" 
                     VisibleIndex="4" Width="170px">
                  </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="RFQ Number" FieldName="RFQ_Number" 
                     VisibleIndex="5" Width="170px">
                    </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="RFQ Status" FieldName="RFQ_Status" 
                     VisibleIndex="5" Width="170px">
                     
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Department" FieldName="DepartmentName" 
                     VisibleIndex="10" Width="150px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Section" FieldName="SectionName" 
                     VisibleIndex="11" Width="150px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="PR Type" FieldName="PRTypeDescription" 
                     VisibleIndex="12" Width="170px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Project" FieldName="Project" 
                     VisibleIndex="13" Width="180px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Urgent" FieldName="Urgent_Status" 
                     VisibleIndex="14">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Tender" FieldName="TenderName" 
                     VisibleIndex="15">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataDateColumn Caption="Deadline" FieldName="RFQ_DueDate" 
                     VisibleIndex="9">
                    
                 </dx:GridViewDataDateColumn>
                 <dx:GridViewDataTextColumn Caption="Revision" FieldName="Rev" VisibleIndex="2" FixedStyle="Left">
                 </dx:GridViewDataTextColumn>
                 
             </Columns>
                    <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
             
                    <Settings VerticalScrollableHeight="300" 
                HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>

         
                 <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
</EditFormColumnCaption>
                 </Styles>
        
        </dx:ASPxGridView>


        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 0px; height: 100px;">  
                  <tr style="height: 20px">
                    <td style=" padding:0px 0px 0px 10px; width:120px">
                    
                        &nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>  
                  <tr style="height: 20px">
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>  
                  <tr style="height: 20px">
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>  
                  <tr style="height: 20px">
                    <td>&nbsp;</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>  
            </table>
        </div>

    </div>


</div>
</asp:Content>

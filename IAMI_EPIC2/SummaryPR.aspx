﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SummaryPR.aspx.vb" Inherits="IAMI_EPIC2.SummaryPR" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //Function for Message
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;

                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;

                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;

                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function BatchEditStartEditing(s, e) {
            if (e.focusedColumn.fieldName == "CEPlanning") {
                e.cancel = false;
            } else {
                e.cancel = true;
            }
        }  

	</script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px; ">
    <table style="width: 100%;">
        <tr>
            <td>
                
            </td>
        </tr>
        <tr>
            <td></td>
            <td style=" padding:0px 0px 0px 0px; width:100px">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="From Date">
                    </dx1:aspxlabel>                       
            </td>
            <td width="8px"></td>
            <td colspan="1" style="width:120px">
                <dx:aspxdateedit ID="dtDateFrom" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateFrom"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:aspxdateedit>                       </td>
                <td style="width:30px">   
                    
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="To">
                    </dx1:aspxlabel>   
                                                     
                </td>
                <td>
                <dx:aspxdateedit ID="dtDateTo" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateTo"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                    </dx:aspxdateedit>   
                    
            </td>
            <td colspan="6"></td>
        </tr>
        <tr>
            <td colspan="7" style="height:10px"></td>
        </tr>
        
        <tr>
            
            <td width="5px">
                &nbsp;
            </td>
            <td width="100px">
                <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnRefresh" Theme="Default">   
                    <ClientSideEvents Click="function(s, e) {
                        Grid.PerformCallback('load|');
                        s.cp_val = 0;
                        s.cp_message = '';
                    }" />                      
                </dx:ASPxButton>
                &nbsp;
                
            </td>
            <td></td>
            <td colspan="2" width="100px">
                <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnExcel" Theme="Default"> 
                                         
                </dx:ASPxButton>
            </td>
            <td colspan="5"></td>
        </tr>
        <tr>
            <td colspan="7" style="height:10px"></td>
        </tr>
        <tr>
            <td colspan="7" style="height:10px">
                <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
            </td>
        </tr>
        </table>
</div>
<div style="padding: 5px 5px 5px 5px">
    <dx:ASPxGridView ID="Grid" runat="server" ClientInstanceName="Grid" Width="100%" 
        AutoGenerateColumns="false" EnableTheming="True" Theme="Office2010Black"  
        Font-Names="Segoe UI" Font-Size="8pt">
        <ClientSideEvents EndCallback="OnEndCallback" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="DEPARTMENT (USER)" FieldName="Department" VisibleIndex="1" Width="250px"  >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AllowAutoFilter="True" />
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="SECTION" FieldName="Section" VisibleIndex="2" Width="150px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AllowAutoFilter="True" />
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
          
            <dx:GridViewDataTextColumn Caption="PURCHASE REQUEST NO" FieldName="PRNo" VisibleIndex="3" Width="200px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="MATERIAL NO" FieldName="Material_No" VisibleIndex="4" Width="150px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ITEM DESCRIPTION" FieldName="Description" VisibleIndex="5" Width="250px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="SPESIFICATION" FieldName="Specification" VisibleIndex="6" Width="200px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="TENDER TYPE" FieldName="TenderType" VisibleIndex="7" Width="100px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="QTY" FieldName="Qty" VisibleIndex="8" Width="50px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle"></CellStyle>
                 <PropertiesTextEdit DisplayFormatString="{0:n0}"></PropertiesTextEdit>
                 <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="UOM" FieldName="UOM" VisibleIndex="9" Width="50px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="PRICE / PCS" FieldName="Price" VisibleIndex="10" Width="150px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle"></CellStyle>
                <PropertiesTextEdit DisplayFormatString="{0:n2}"></PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="AMOUNT" FieldName="TotalAmount" VisibleIndex="11" Width="180px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle"></CellStyle>
                <PropertiesTextEdit DisplayFormatString="{0:n2}"></PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <%--<dx:GridViewBandColumn Caption="TOTAL AMOUNT" VisibleIndex="9" Name="TotalAmount">
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                
                <Columns>
                    <dx:GridViewDataTextColumn Caption="(base on IA Final Price )" FieldName="TotalAmount" VisibleIndex="0" Width="150px">
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                        <CellStyle HorizontalAlign="Right" VerticalAlign="Middle"></CellStyle>
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:GridViewBandColumn>--%>
            <dx:GridViewDataTextColumn Caption="TOTAL BUDGET" FieldName="TotalBudget" VisibleIndex="12" Width="180px" >

                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle" ></CellStyle>
                <PropertiesTextEdit DisplayFormatString="{0:n2}"></PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="IA BUDGET NO" FieldName="IABudget" VisibleIndex="13" Width="180px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="SELECTED SUPPLIER" FieldName="SelectedSupplier" VisibleIndex="14" Width="250px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="PO DATE" FieldName="PODate" VisibleIndex="15" Width="120px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="PO NO" FieldName="PONo" VisibleIndex="16" Width="120px" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior ColumnResizeMode="Control" AllowSort="false" />
        <SettingsPager Mode="ShowAllRecords" AlwaysShowPager="true"></SettingsPager>
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" 
            VerticalScrollableHeight="320" HorizontalScrollBarMode="Auto" />
    </dx:ASPxGridView>
</div>
</asp:Content>

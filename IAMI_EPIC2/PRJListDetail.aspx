﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="PRJListDetail.aspx.vb" Inherits="IAMI_EPIC2.PRJListDetail" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };
         function fn_AllowonlyNumeric(s, e) {
            var theEvent = e.htmlEvent || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;

            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault)
                    theEvent.preventDefault();
            }
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
            <tr style="height: 50px">
                <td style="width: 90px; padding: 0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">
                        <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 90px; padding: 0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 90px; padding: 0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnBack" runat="server" Text="Back To List" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnAdd" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td align="left">
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Kabupaten'">
                    </asp:SqlDataSource>
                    &nbsp;
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Language'">
                    </asp:SqlDataSource>
                    &nbsp;
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Period'">
                    </asp:SqlDataSource>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
                        <ClientSideEvents CallbackComplete="MessageBox" />
                    </dx:ASPxCallback>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <div style="padding: 5px 0px 5px 0px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" KeyFieldName="Project_ID; Vehicle_Code" Theme="Office2010Black" OnRowValidating="Grid_RowValidating"
                OnStartRowEditing="Grid_StartRowEditing" OnRowInserting="Grid_RowInserting" OnRowDeleting="Grid_RowDeleting"
                OnAfterPerformCallback="Grid_AfterPerformCallback"   Width="100%" Font-Names="Segoe UI"
                Font-Size="9pt">
                <ClientSideEvents EndCallback="OnEndCallback" />
                <Columns>
                    <dx:GridViewCommandColumn VisibleIndex="0" Width="100px" ShowEditButton="true" ShowClearFilterButton="true"
                        ShowDeleteButton="false">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="Project_Name" Width="150px" Caption="Project Name"
                        VisibleIndex="1" EditFormSettings-Visible="false" Visible="false" >
                        <PropertiesTextEdit MaxLength="25" Width="150px" ClientInstanceName="Project_Name">
                            <Style HorizontalAlign="Left"></Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Project_Type" Width="150px" Caption="Project Type"
                        VisibleIndex="2" EditFormSettings-Visible="false"  Visible="false">
                        <PropertiesTextEdit MaxLength="25" Width="150px" ClientInstanceName="Project_Type">
                            <Style HorizontalAlign="Left"></Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Rate_USD_IDR" Width="150px" Caption="Rate USD"
                        VisibleIndex="2" EditFormSettings-Visible="false"  Visible="false">
                        <PropertiesTextEdit MaxLength="25" Width="150px" ClientInstanceName="Rate_USD_IDR">
                            <Style HorizontalAlign="Left"></Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Rate_YEN_IDR" Width="150px" Caption="Rate YEN"
                        VisibleIndex="2" EditFormSettings-Visible="false"  Visible="false">
                        <PropertiesTextEdit MaxLength="25" Width="150px" ClientInstanceName="Rate_YEN_IDR">
                            <Style HorizontalAlign="Left"></Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Rate_BATH_IDR" Width="150px" Caption="Rate BTH"
                        VisibleIndex="2" EditFormSettings-Visible="false"  Visible="false">
                        <PropertiesTextEdit MaxLength="25" Width="150px" ClientInstanceName="Rate_BATH_IDR">
                            <Style HorizontalAlign="Left"></Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Vehicle Code" FieldName="Vehicle_Code" VisibleIndex="3"
                        Width="250px" EditFormSettings-VisibleIndex="0" EditFormCaptionStyle-Paddings-Padding="5">
                        <PropertiesTextEdit  Width="200px" ClientInstanceName="Vehicle_Code" MaxLength="20">
                            <Style HorizontalAlign="Left" >
                                
                            </Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn VisibleIndex="4" Name ="Year1" Caption="Years" Visible ="false" >
                        <Columns>
                            
                            <dx:GridViewDataTextColumn Width="100px" Caption="YYYY" VisibleIndex="0" FieldName="Year1"
                                EditFormCaptionStyle-Paddings-Padding="5" EditFormSettings-Visible="False">
                                <PropertiesTextEdit Width="100px" MaxLength="8"  DisplayFormatString="#,###">
                                  <MaskSettings Mask="<0..1000000g>" IncludeLiterals="DecimalSymbol" />
                                </PropertiesTextEdit>
                                <HeaderStyle HorizontalAlign="Center" />
                                <Settings AllowAutoFilter="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="YYYY" VisibleIndex="1" FieldName="Year2" EditFormCaptionStyle-Paddings-Padding="5"
                                EditFormSettings-Visible="false">
                                <PropertiesTextEdit Width="100px" MaxLength="8"  DisplayFormatString="#,###">
                                    <MaskSettings Mask="<0..1000000g>"  IncludeLiterals="DecimalSymbol" />
                                </PropertiesTextEdit>
                                <HeaderStyle HorizontalAlign="Center" />
                                <Settings AllowAutoFilter="False" />
                               
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Width="100px" Caption="YYYY" VisibleIndex="2" FieldName="Year3" 
                                EditFormCaptionStyle-Paddings-Padding="5" EditFormSettings-Visible="false">
                                <PropertiesTextEdit Width="100px" MaxLength="8"  DisplayFormatString="#,###">
                                <MaskSettings Mask="<0..1000000g>"  IncludeLiterals="DecimalSymbol"/>
                                </PropertiesTextEdit>
                                <HeaderStyle HorizontalAlign="Center" />
                              
                                <Settings AllowAutoFilter="False" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewBandColumn>
                    <dx:GridViewDataTextColumn Caption="Total Volume RFQ" FieldName="Total_Volume_Rfq"
                        EditFormCaptionStyle-Paddings-Padding="5" Width="150px" VisibleIndex="5" EditFormSettings-Visible="False">
                        <PropertiesTextEdit Width="115px" ClientInstanceName="Total_Volume_Rfq" MaxLength="8"  DisplayFormatString="#,###">
                            <Style HorizontalAlign="Left">
                                
                            </Style>
                            <ClientSideEvents KeyPress="function(s,e){ fn_AllowonlyNumeric(s,e);}" />
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Remarks" FieldName="Remarks" Width="100px" VisibleIndex="25"
                        EditFormCaptionStyle-Paddings-Padding="5" EditFormSettings-VisibleIndex="25">
                        <PropertiesTextEdit Width="115px" ClientInstanceName="Remarks">
                            <Style HorizontalAlign="Left">
                                
                            </Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
                <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                </SettingsPager>
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
                <SettingsPopup>
                    <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                        Width="320" />
                </SettingsPopup>
                <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                    EditFormColumnCaption-Paddings-PaddingRight="10px">
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>
                    <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                        <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                        </Paddings>
                    </EditFormColumnCaption>
                    <CommandColumnItem ForeColor="Orange">
                    </CommandColumnItem>
                </Styles>
                <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
                <Templates>
                    <EditForm>
                        <div style="padding: 15px 15px 15px 15px">
                            <dx:ContentControl ID="ContentControl1" runat="server">
                                <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                    runat="server"></dx:ASPxGridViewTemplateReplacement>
                            </dx:ContentControl>
                        </div>
                        <div style="text-align: left; padding: 5px 5px 5px 15px">
                            <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                runat="server"></dx:ASPxGridViewTemplateReplacement>
                            <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                runat="server"></dx:ASPxGridViewTemplateReplacement>
                        </div>
                    </EditForm>
                </Templates>
            </dx:ASPxGridView>
        </div>
    </div>
</asp:Content>

﻿Imports System
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO

Public Class CPList_NonTender
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"
    Private Sub FillComboSupplier()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_FillCombo", "Combo", "Supplier", ErrMsg)

        If ErrMsg = "" Then
            cboSupplierCode.DataSource = ds.Tables(0)
            cboSupplierCode.DataBind()

            If cboSupplierCode.Items.Count > 0 Then
                cboSupplierCode.SelectedIndex = 0
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboSupplierCode)
        End If
    End Sub

    Private Sub FillComboCPNumber()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
        dtFrom = Format(dtQuotationFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtQuotationTo.Value, "yyyy-MM-dd")

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_FillCombo", "Combo|StartDate|EndDate|PRNumber|UserID", "CP|" & dtFrom & "|" & dtTo & "|" & cboPRNumber.Text & "|" & pUser, ErrMsg)

        If ErrMsg = "" Then
            cboCPNumber.DataSource = ds.Tables(0)
            cboCPNumber.DataBind()

            If cboCPNumber.Items.Count > 0 Then
                cboCPNumber.SelectedIndex = 0
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboCPNumber)
        End If
    End Sub

    Private Sub FillComboPRNumber()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
        dtFrom = Format(dtQuotationFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtQuotationTo.Value, "yyyy-MM-dd")

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_FillCombo", "Combo|StartDate|EndDate|UserID", "PRNT|" & dtFrom & "|" & dtTo & "|" & pUser, ErrMsg)

        If ErrMsg = "" Then
            cboPRNumber.DataSource = ds.Tables(0)
            cboPRNumber.DataBind()

            If cboPRNumber.Items.Count > 0 Then
                cboPRNumber.SelectedIndex = 0
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboPRNumber)
        End If
    End Sub

    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01", ls_CPNumber As String = ""
        dtFrom = Format(dtQuotationFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtQuotationTo.Value, "yyyy-MM-dd")
        ls_CPNumber = cboCPNumber.Text

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_List_NonTender", "StartDate|EndDate|SupplierCode|PRNumber|CPNumber|UserID", dtFrom & "|" & dtTo & "|" & cboSupplierCode.Value & "|" & cboPRNumber.Text & "|" & cboCPNumber.Text & "|" & pUser, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub ExcelGrid()
        Try
            Call GridLoad()

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=CounterProposalList_" & Format(CDate(Now), "yyyyMMdd_hhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            gs_Message = ex.Message
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|F010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAdd.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAdd, btnAdd.GetType(), "btnAdd", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        Dim dfrom As Date
        dfrom = Format(CDate(Now), "yyyy-MM-dd") 'Year(Now) & "-" & Month(Now) & "-" & Day(Now)
        dtQuotationFrom.Value = dfrom
        dtQuotationTo.Value = Now
        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboCPNumber)
            End Try
        End If
    End Sub
  

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("F010")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "F010")
        Dim dfrom As Date

        gs_Message = ""

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dtQuotationFrom.Value = dfrom
            dtQuotationTo.Value = Now

            'Set last value because trigger from btnBack in CPListDetail_NonTender.aspx
            If IsNothing(Session("btnBack_CPListDetail_NonTender")) = False Then
                dtQuotationFrom.Value = CDate(Split(Session("btnBack_CPListDetail_NonTender"), "|")(0))
                dtQuotationTo.Value = CDate(Split(Session("btnBack_CPListDetail_NonTender"), "|")(1))
                cboPRNumber.Text = Split(Session("btnBack_CPListDetail_NonTender"), "|")(2)
                cboSupplierCode.Text = Split(Session("btnBack_CPListDetail_NonTender"), "|")(3)
                cboCPNumber.Text = Split(Session("btnBack_CPListDetail_NonTender"), "|")(4)

                Call GridLoad()

                Grid.MakeRowVisible(Split(Session("btnBack_CPListDetail_NonTender"), "|")(5))
            End If
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Call GridLoad()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try
    End Sub

    Private Sub Grid_CustomButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonEventArgs) Handles Grid.CustomButtonInitialize
        If e.Column.Grid.GetRowValues(e.VisibleIndex, "CP_Number") = "" Then
            e.Visible = DevExpress.Utils.DefaultBoolean.False
        Else
            e.Visible = DevExpress.Utils.DefaultBoolean.True
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Call GridLoad()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        'Non-Editable
        If e.GetValue("CP_Number") <> "" Then
            'HEADER
            e.Cell.BackColor = Color.Silver
        Else
            'DETAIL
            e.Cell.BackColor = Color.LemonChiffon
        End If
    End Sub

    Private Sub Grid_HtmlRowPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles Grid.HtmlRowPrepared
        If IsNothing(e.GetValue("CP_Number")) = False Then
            If e.GetValue("CP_Number") = "" Then
                e.Row.BackColor = Color.LemonChiffon
            End If
        End If
    End Sub

    Private Sub cboPRNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboPRNumber.Callback
        Try
            Call FillComboPRNumber()

            'Set last value because trigger from btnBack in SupplierQuotationAcceptDetail
            If IsNothing(Session("btnBack_CPListDetail_NonTender")) = False Then
                cboPRNumber.SelectedItem = cboPRNumber.Items.FindByValue(Split(Session("btnBack_CPListDetail_NonTender"), "|")(2))
                Session.Remove("btnBack_CPListDetail_NonTender")
            End If

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboPRNumber)
        End Try
    End Sub

    Private Sub cboSupplierCode_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSupplierCode.Callback
        Try
            Call FillComboSupplier()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboSupplierCode)
        End Try
    End Sub

    Private Sub cboCPNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCPNumber.Callback
        Try
            Call FillComboCPNumber()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboCPNumber)
        End Try
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        Session("btnAdd_CPList_NonTender") = "1"
        Response.Redirect("~/CPListDetail_NonTender.aspx")
    End Sub

    Private Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Dim ErrMsg As String = ""
        Call ExcelGrid()
    End Sub
#End Region



End Class
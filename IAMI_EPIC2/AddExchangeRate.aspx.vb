﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data

Public Class AddExchangeRate
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cExchangeRate As New clsExchangeRate
        Dim action As String
        Dim dt As DataTable = Nothing
        Dim outError As String = ""

        sGlobal.getMenu("A060")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A060")

        action = Request.QueryString("action")

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            cbAction.JSProperties("cp_type") = 0
            cbAction.JSProperties("cp_message") = ""

            If action = "new" Then
            ElseIf action = "edit" Then
                cExchangeRate.BankCode = Request.QueryString("bankcode")
                cExchangeRate.CurrencyCode = Request.QueryString("currencycode")
                cExchangeRate.ExchangeRateDate = Request.QueryString("date")
                'dtExchangeRateDate.Value = Request.QueryString("date")
                dt = clsExchangeRateDB.getDataTableExchangeRateDetail(cExchangeRate, , , outError)
                If dt.Rows.Count > 0 Then
                    txtBuyingRate.Text = dt.Rows(dt.Rows.Count - 1)("BuyingRate") 'CDbl(dt.Rows(dt.Rows.Count - 1)("BuyingRate"))
                    txtMiddleRate.Text = dt.Rows(dt.Rows.Count - 1)("MiddleRate") 'CDbl(dt.Rows(dt.Rows.Count - 1)("MiddleRate"))
                    txtSellingRate.Text = dt.Rows(dt.Rows.Count - 1)("SellingRate") 'CDbl(dt.Rows(dt.Rows.Count - 1)("SellingRate"))
                    txtRemarks.Text = dt.Rows(dt.Rows.Count - 1)("Remarks").ToString.Trim
                End If
            End If
        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/ExchangeRate.aspx")
    End Sub


    Protected Sub cbAction_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbAction.Callback
        Dim cExchangeRate As New clsExchangeRate
        Dim pAction As String = ""
        Dim sBankCode As String = ""
        Dim sCurrencyCode As String = ""
        Dim sDate As String = ""
        Dim sBuyingRate As Double = Nothing
        Dim sMiddleRate As Double = Nothing
        Dim sSellingRate As Double = Nothing
        Dim sRemarks As String = ""
        Dim outError As String = ""

        Try
            pAction = Split(e.Parameter, "|")(0)
            If pAction = "insert" Then
                sBankCode = Split(e.Parameter, "|")(1)
                sCurrencyCode = Split(e.Parameter, "|")(2)
                sDate = Split(e.Parameter, "|")(3)
                sBuyingRate = CDbl(Split(e.Parameter, "|")(4))
                sMiddleRate = CDbl(Split(e.Parameter, "|")(5))
                sSellingRate = CDbl(Split(e.Parameter, "|")(6))
                sRemarks = Split(e.Parameter, "|")(7)

                cExchangeRate.BankCode = sBankCode
                cExchangeRate.CurrencyCode = sCurrencyCode
                cExchangeRate.ExchangeRateDate = sDate
                cExchangeRate.BuyingRate = sBuyingRate
                cExchangeRate.MiddleRate = sMiddleRate
                cExchangeRate.SellingRate = sSellingRate
                cExchangeRate.Remarks = sRemarks
                cExchangeRate.UserID = pUser

                If clsExchangeRateDB.insert(cExchangeRate, , , outError) = True Then
                    cbAction.JSProperties("cp_type") = 1
                    cbAction.JSProperties("cp_message") = "Insert data successfully!"
                Else
                    cbAction.JSProperties("cp_type") = 3
                    cbAction.JSProperties("cp_message") = outError
                End If
            ElseIf pAction = "update" Then
                sBankCode = Split(e.Parameter, "|")(1)
                sCurrencyCode = Split(e.Parameter, "|")(2)
                sDate = Split(e.Parameter, "|")(3)
                sBuyingRate = CDbl(Split(e.Parameter, "|")(4))
                sMiddleRate = CDbl(Split(e.Parameter, "|")(5))
                sSellingRate = CDbl(Split(e.Parameter, "|")(6))
                sRemarks = Split(e.Parameter, "|")(7)

                cExchangeRate.BankCode = sBankCode
                cExchangeRate.CurrencyCode = sCurrencyCode
                cExchangeRate.ExchangeRateDate = sDate
                cExchangeRate.BuyingRate = sBuyingRate
                cExchangeRate.MiddleRate = sMiddleRate
                cExchangeRate.SellingRate = sSellingRate
                cExchangeRate.Remarks = sRemarks
                cExchangeRate.UserID = pUser

                If clsExchangeRateDB.update(cExchangeRate, , , outError) = True Then
                    cbAction.JSProperties("cp_type") = 1
                    cbAction.JSProperties("cp_message") = "Update data successfully!"
                Else
                    cbAction.JSProperties("cp_type") = 3
                    cbAction.JSProperties("cp_message") = outError
                End If
            End If
            e.Result = pAction
        Catch ex As Exception
            cbAction.JSProperties("cp_type") = 3
            cbAction.JSProperties("cp_message") = ex.Message
            e.Result = pAction
        End Try
    End Sub
#End Region
    
End Class
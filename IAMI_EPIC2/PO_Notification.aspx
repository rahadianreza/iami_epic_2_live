﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site_PO.Master" CodeBehind="PO_Notification.aspx.vb" Inherits="IAMI_EPIC2.PO_Notification" %>
<%@ MasterType VirtualPath="~/Site_PO.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
  <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
    </div>
   <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="UserID;PR_Number;Urgent_Status;CE_Number;CE_Revision;IA_Budget_Number;IA_Number;IA_Revision;FileName_Acceptance;FilePath_Acceptance" >
             <ClientSideEvents CustomButtonClick="function(s, e) {
	              if(e.buttonID == 'edit'){
                      var rowkey = Grid.GetRowKey(e.visibleIndex); 
                      window.location.href= 'PO_ViewPRReport.aspx?ID=' + rowkey  ;
                       }
                  if(e.buttonID == 'cenumber'){
                      var rowkey = Grid.GetRowKey(e.visibleIndex); 
                      window.location.href= 'PO_ViewCEReport.aspx?ID=' + rowkey  ;
                       }

                  if(e.buttonID == 'ianumber'){
                      var rowkey = Grid.GetRowKey(e.visibleIndex); 
                      window.location.href= 'PO_ViewIAPriceReport.aspx?ID=' + rowkey  ;
                       }
    }" />
                      
            <Columns>
             <dx:GridViewCommandColumn  Name="PR" ButtonType="Link" Caption="PR Number" Width="20%" VisibleIndex="0" FixedStyle="Left">
                              <CustomButtons>
                                  <dx:GridViewCommandColumnCustomButton ID="edit">
                                  </dx:GridViewCommandColumnCustomButton>
                              </CustomButtons>
             </dx:GridViewCommandColumn>
             <dx:GridViewDataTextColumn Caption="PR Date" FieldName="PR_Date" 
                     VisibleIndex="1" Width="10%" Name="PR_Date">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                </dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Tender Status" FieldName="Tender_Status"
                     VisibleIndex="2" Width="10%" Name="Tender_Status">
                </dx:GridViewDataTextColumn>
				<dx:GridViewDataTextColumn Caption="Urgent Status" FieldName="Urgent_Status"
                     VisibleIndex="3" Width="10%" Name="Urgent_Status">
                </dx:GridViewDataTextColumn>
                <dx:GridViewCommandColumn  Name="CE" ButtonType="Link" Caption="CE Number" Width="20%" VisibleIndex="4" FixedStyle="Left">
                              <CustomButtons>
                                  <dx:GridViewCommandColumnCustomButton ID="cenumber">
                                  </dx:GridViewCommandColumnCustomButton>
                              </CustomButtons>
             </dx:GridViewCommandColumn>

               
             <dx:GridViewCommandColumn  Name="IAN" ButtonType="Link" Caption="IA Price Number" Width="15%" VisibleIndex="6" FixedStyle="Left">
                              <CustomButtons>
                                  <dx:GridViewCommandColumnCustomButton ID="ianumber">
                                  </dx:GridViewCommandColumnCustomButton>
                              </CustomButtons>
             </dx:GridViewCommandColumn>
                               <dx:GridViewDataTextColumn Caption="IA Budget Number" FieldName="FileName_Acceptance" VisibleIndex="7"
                                    Width="500px">
                                    <DataItemTemplate>
                                        <dx:ASPxHyperLink ID="linkFileName" runat="server" OnInit="keyFieldLink_Init">
                                        </dx:ASPxHyperLink>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                 <dx:GridViewDataTextColumn Caption="File Path" FieldName="FilePath_Acceptance" VisibleIndex="8"
                                    Width="0px" Visible="false">
                                </dx:GridViewDataTextColumn>
               
               
             </Columns>

            <SettingsBehavior AllowFocusedRow="True" AllowSort="False" AllowDragDrop="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
            <Settings ShowFilterRow="false" VerticalScrollableHeight="225" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" ShowStatusBar="Hidden"></Settings>
            
           
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" 
                AllowInsert="False" />

            <Styles Header-Paddings-Padding="2px" EditFormColumnCaption-Paddings-PaddingLeft="0px" EditFormColumnCaption-Paddings-PaddingRight="0px" >
                <Header CssClass="customHeader" HorizontalAlign="Center" Wrap="True">
<Paddings Padding="2px"></Paddings>
                </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="0px" PaddingRight="0px"></Paddings>
</EditFormColumnCaption>
            </Styles>
        </dx:ASPxGridView>
    </div>
    <%--<div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
      
        </table>
    </div>--%>
</div>
</asp:Content>

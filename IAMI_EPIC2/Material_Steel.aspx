﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Material_Steel.aspx.vb"
    Inherits="IAMI_EPIC2.Material_Steel" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };
        function MessageBox(s, e) {
            //alert(s.cpmessage);
            if (s.cbMessage == "Download Excel Successfully") {
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }


    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        .style4
        {
            width: 24px;
        }
        .style8
        {
            height: 38px;
        }
        .style9
        {
            width: 100px;
            height: 2px;
        }
        .style10
        {
            width: 24px;
            height: 2px;
        }
        .style11
        {
            height: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%; border: 1px solid black; height: 100px;">
<tr>
<td></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period From">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                    <dx:ASPxTimeEdit ID="Period_From" runat="server" DisplayFormatString="MMM yyyy" EditFormat="Custom" Theme="Office2010Black" EnableTheming="true"
                                            EditFormatString="MMM yyyy" Width="120px" ClientInstanceName="Period_From" Font-Size="9pt">
                      <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>   
                    </dx:ASPxTimeEdit>
            </td>
<td class="style4" align = "center">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period To">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                    <dx:ASPxTimeEdit ID="Period_To" runat="server" DisplayFormatString="MMM yyyy" EditFormat="Custom" Theme="Office2010Black" EnableTheming="true"
                                            EditFormatString="MMM yyyy" Width="120px" ClientInstanceName="Period_To" Font-Size="9pt">
                     <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>   
                    </dx:ASPxTimeEdit>
            
                    </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;</td>
<td class="style4">&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 200px">
                        &nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding: 0px 0px 0px 10px; " class="style9">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style9">
                </td>
<td class="style10" align = "center">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style9">
                </td>
<td class="style11"></td>
<td class="style11"></td>
<td class="style11"></td>
<td class="style11"></td>
<td class="style11"></td>
<td class="style11"></td>
</tr>
<tr style="height: 2px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text=" Type">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">

                        <dx:ASPxComboBox ID="cboType" runat="server" ClientInstanceName="cboType" Width="120px"
                            Font-Names="Segoe UI" TextField="Description" ValueField="Code" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3" DataSourceID="SqlDataSource1">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="150px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
<td class="style4" align = "center">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr>
<td style="padding: 20px 0px 0px 10px" class="style8" colspan="5">
                <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnShowData" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
                 
                            Grid.PerformCallback('load| ' + Period_From.GetValue() + '|' + Period_To.GetValue() + '|' + cboType.GetValue());
                        
                        }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton> &nbsp;
                <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnDownload" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton> &nbsp;
                <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnAdd" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                 &nbsp;
                        <dx:ASPxButton ID="btnChart" runat="server" Text="Chart" UseSubmitBehavior="false"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnChart"
                            Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
            </td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>

    <div style="padding: 5px 0px 5px 0px">
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="Select Code = 'ALL', Description = 'ALL' Union ALL Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'MaterialType'">
        </asp:SqlDataSource>

        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
            <ClientSideEvents CallbackComplete="SetCode" />
        </dx:ASPxCallback>
        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
        </dx:ASPxGridViewExporter>
      <%--  OnRowDeleting="Grid_RowDeleting"--%>

      <%--else if (e.buttonID == 'Delete'){
                           if (confirm('Are you sure want to delete ?')) {
                        var rowKey = Grid.GetRowKey(e.visibleIndex);
                          Grid.PerformCallback('delete |' + rowKey);
                    }
                    else
                    {
                    }
                  --%>
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" KeyFieldName="MaterialType;Period;MaterialCode;SupplierCode"
            Font-Names="Segoe UI" Font-Size="9pt"  
            >
            <ClientSideEvents EndCallback="OnEndCallback" CustomButtonClick="function(s, e) {
	              if(e.buttonID == 'Edit'){ 
                        var rowKey = Grid.GetRowKey(e.visibleIndex);
                        window.location.href= 'Material_SteelDetail.aspx?ID=' + rowKey;
                  }  if(e.buttonID == 'Delete'){ 
                        if (confirm('Are you sure want to delete ?')) {
                        var rowKey = Grid.GetRowKey(e.visibleIndex);
                          Grid.PerformCallback('delete |' + rowKey);
                    } else {
                     
                    }
                     
                    }
                  }"></ClientSideEvents>
            <Columns>    
             <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" 
                    Width="150px" ShowInCustomizationForm="True">                       
               <%--  <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" ShowEditButton="false" ShowDeleteButton="false" 
                                            ShowNewButtonInHeader="false" ShowClearFilterButton="true" Width="160px">--%>
                                            <CustomButtons>
                                                <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit">
                                                </dx:GridViewCommandColumnCustomButton>
                                            </CustomButtons>
                                            <CustomButtons>
                                                    <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete">
                                                    </dx:GridViewCommandColumnCustomButton>
                                                </CustomButtons>
                                        </dx:GridViewCommandColumn>


                    <dx:GridViewDataTextColumn Caption="Type" FieldName="MaterialType" VisibleIndex="2"
                        Width="100px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Supplier" FieldName="SupplierName" VisibleIndex="3"
                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Period" FieldName="Period" VisibleIndex="4"
                        Width="100px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>
                            <PropertiesTextEdit DisplayFormatString="MMM yyyy">
                                 </PropertiesTextEdit>
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Material Code" FieldName="MaterialCode" VisibleIndex="5"
                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Group Item" FieldName="GroupItemCode" VisibleIndex="6"
                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Category" FieldName="CategoryCode" VisibleIndex="7"
                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Currency" FieldName="CurrencyCode" VisibleIndex="8"
                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains" />

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Price" FieldName="Price" VisibleIndex="9"
                        Width="120px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                     <PropertiesTextEdit DisplayFormatString="#,###.00000">
                    </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                  
                    <dx:GridViewDataTextColumn Caption="Register By" FieldName="Register_By" VisibleIndex="10"
                        Width="100px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains"  AllowAutoFilter="False"/>

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Register Date" FieldName="Register_Date" VisibleIndex="11"
                        Width="100px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains"  AllowAutoFilter="False"/>

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                        <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                                 </PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Update By" FieldName="Update_By" VisibleIndex="12"
                        Width="100px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                       
                        <Settings AutoFilterCondition="Contains"  AllowAutoFilter="False"/>

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Update Date" FieldName="Update_Date" VisibleIndex="13"
                        Width="100px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                    
                        <Settings AutoFilterCondition="Contains" AllowAutoFilter="False"/>

<EditFormCaptionStyle>
<Paddings Padding="5px"></Paddings>
</EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                          <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                                 </PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn Caption="Supplier_Code" FieldName="Supplier_Code" VisibleIndex="20"
                        Width="100px"  EditFormCaptionStyle-Paddings-Padding="5" Visible=false   >
                       
                        <Settings AutoFilterCondition="Contains"  AllowAutoFilter="False"/>

                    <EditFormCaptionStyle>
                    <Paddings Padding="5px"></Paddings>
                    </EditFormCaptionStyle>

                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="6px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
            </Columns>
                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        
                                       <%-- <CommandColumnItem ForeColor="Orange"></CommandColumnItem>--%>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
        </dx:ASPxGridView>

    </div>
</asp:Content>

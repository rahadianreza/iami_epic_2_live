﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PRListDetail.aspx.vb" Inherits="IAMI_EPIC2.PRListDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server" >
    <script type="text/javascript">
        function MessageBox(s, e) {
            if (s.cpMessage == "Draft Data Saved Successfully") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                txtPRNo.SetText(s.cpPRNo);
               // alert(s.cpPRNo);
                //alert(s.cpRev);
                setTimeout(function () {
                    window.location.href = 'PRListDetail.aspx?ID=' + s.cpPRNo + "|Draft|" + s.cpRev;
                }, 1000);
            }
            else if (s.cpMessage == "Submit Data Saved Successfully") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                btnSubmit.SetEnabled(false);
                btnDraft.SetEnabled(false);
                btnModify.SetEnabled(false);
                btnDelete.SetEnabled(false);
                PRDate.SetEnabled(false);
                cboPRBudget.SetEnabled(false);                
                cboDepartment.SetEnabled(false);
                cboSection.SetEnabled(false);
                cboCostCenter.SetEnabled(false);
                txtProject.SetEnabled(false);
                chkUrgent.SetEnabled(false);
                PODate.SetEnabled(false);
                txtUrgentNote.SetEnabled(false);
                Grid.SetEnabled(false);

                txtPRNo.SetText(s.cpPRNo);
            }

            else if (s.cpMessage == "Delete Data Successfully") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                millisecondsToWait = 2000;
                setTimeout(function () {
                    window.location.href = "PRList.aspx";
                }, millisecondsToWait);

            }

            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }

    }

    function SetFilterData(rValue) {
        txtFilter.SetText(rValue);
    }

    function LoadForm(s, e) {
        if (txtPRNo.GetText() == '--NEW--') {
            btnPrint.SetEnabled(false);
            btnSubmit.SetEnabled(false);
            btnModify.SetEnabled(false);
            btnDelete.SetEnabled(false);
            cboDepartment.SetEnabled(true);         
        }
        else {
            btnPrint.SetEnabled(true);
            btnSubmit.SetEnabled(true);
            btnModify.SetEnabled(true);
            btnDelete.SetEnabled(true);
            cboDepartment.SetEnabled(false);
        }

        if (chkUrgent.GetChecked() == (true)) {
            txtUrgentNote.SetEnabled(true);
            PODate.SetEnabled(true);
        }
        else {
            txtUrgentNote.SetEnabled(false);
            PODate.SetEnabled(false);
        }
    }

    function Check(s, e) {
        if (chkUrgent.GetChecked() == (true)) {            
            txtUrgentNote.SetEnabled(true);
            PODate.SetEnabled(true);
            //alert(PRDate.GetDate());
        }
        if (chkUrgent.GetChecked() == (false)) {
            txtUrgentNote.SetText('');          
            txtUrgentNote.SetEnabled(false);
            PODate.SetEnabled(false);
            if (txtPRNo.GetText() == '--NEW--') {
                var a = addDays(PRDate.GetDate(), 21);
                    PODate.SetValue(a);
            }
          
        }
    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
     
    }

    function DateAdd() {
        var a = addDays(PRDate.GetDate(), 21);
        PODate.SetValue(a);
    }

    function DateAdd2() {
        var a = addDays(PODate.GetDate(), -21);
        PRDate.SetValue(a);
    }

    function OnBatchEditStartEditing(s, e) {
        currentColumnName = e.focusedColumn.fieldName;
         

        if (currentColumnName != "Qty" && currentColumnName != "Remarks") {
            e.cancel = true;
        }
        currentEditableVisibleIndex = e.visibleIndex;

    }

    
    function DraftProcess() {
        if (cboPRBudget.GetText() == '') {
            toastr.warning('Please Select Budget!', 'Warning');
            cboPRBudget.Focus();
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return;
        }


        
            var result = "";
            var startIndex = 0;
            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                qty = Grid.batchEditApi.GetCellValue(i, "Qty");
                MaterialNo = Grid.batchEditApi.GetCellValue(i, "Material_No");

                if (qty < 0) {
                    Grid.batchEditApi.SetCellValue(i, "TotalPrice", 0);
                    Grid.batchEditApi.SetCellValue(i, "Price", 0);
                    return false;
                }
                
                //var qty = s.batchEditApi.GetCellValue(e.visibleIndex, "Qty");
                if (qty == 0) {
                    toastr.warning('Qty must greater than zero', 'Warning'); //Information
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    
                    //e.Focus();
                }

                if (qty == 1) {
                    result = result + MaterialNo + '\n' ;
                }


            }

            
            if (result != "") {
                //CONFIRMATION
                result = result.substring(0, result.length - 1);
                var C = confirm("Some items still using default qty (1) MaterialNo:  \t \n" + result + " \nAre you sure want to continue ?");
                if (C == false) {
                    return;
                }
            }



        if (cboDepartment.GetText() == '') {
            toastr.warning('Please Select Department!', 'Warning');
            cboDepartment.Focus();
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return;
        }

       

        if (cboCostCenter.GetText() == '') {
            toastr.warning('Please Select Cost Center!', 'Warning');
            cboCostCenter.Focus();
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return;
        }

        //project dibuat mandatory lagi 
        if (txtProject.GetText() == '') {
            toastr.warning('Please Input Project!', 'Warning');
            txtProject.Focus();
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return;
        }

        if (chkUrgent.GetChecked() == (true)) {
            if (txtUrgentNote.GetText() == '') {
                toastr.warning('Please Input Urgent Note!', 'Warning');
                txtUrgentNote.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (PODate.GetDate() <= PRDate.GetDate()) {
                toastr.warning('Please Input Urgent Date Must Be Grather than PR Date!', 'Warning');
                PODate.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

            Grid.UpdateEdit();
            Grid.PerformCallback('Save|' + txtFilter2.GetText());
            cbApprove.PerformCallback('draft');
            btnPrint.SetEnabled(true);
            btnSubmit.SetEnabled(true);
            btnModify.SetEnabled(true);
            btnDelete.SetEnabled(true);
            cboDepartment.SetEnabled(false);
            
            Grid.PerformCallback('Load|0');
            Grid.CancelEdit();
    }

    function SubmitProcess() {
        if (chkUrgent.GetChecked() == (true)) {
            if (txtUrgentNote.GetText() == '') {
                toastr.warning('Please Input Urgent Note!', 'Warning');
                txtUrgentNote.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

        var result = "";
        var startIndex = 0;
        for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
            qty = Grid.batchEditApi.GetCellValue(i, "Qty");
            MaterialNo = Grid.batchEditApi.GetCellValue(i, "Material_No");

            if (qty < 0) {
                Grid.batchEditApi.SetCellValue(i, "TotalPrice", 0);
                Grid.batchEditApi.SetCellValue(i, "Price", 0);
                return false;
            }

            //var qty = s.batchEditApi.GetCellValue(e.visibleIndex, "Qty");
            if (qty == 0) {
                toastr.warning('Qty must greater than zero', 'Warning'); //Information
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;

                //e.Focus();
            }

            if (qty == 1) {
                result = result + MaterialNo + '\n';
            }


        }


        if (result != "") {
            //CONFIRMATION
            result = result.substring(0, result.length - 1);
            var C = confirm("Some items still using default qty (1) MaterialNo:  \t \n" + result + " \nAre you sure want to continue ?");
            if (C == false) {
                return;
            }
        }

        Grid.UpdateEdit();
        Grid.PerformCallback('Save|' + txtFilter2.GetText());
        cbApprove.PerformCallback('submit');
        Grid.PerformCallback('Load|0');
    }

    function GridLoad() {
        tmpDepartment.SetText(cboDepartment.GetValue());
         tmpSection.SetText(cboSection.GetValue());        
        tmpCostCenter.SetText(cboCostCenter.GetValue());
    }

    function FilterSection() {
       
        //cboDepartment.PerformCallback();
        cboSection.PerformCallback('filter|' + cboDepartment.GetValue());
        cboCostCenter.PerformCallback('filter|' + cboDepartment.GetValue());
    }

    function SetCode(s, e) {
        tmpDepartment.SetText(cboDepartment.GetValue());
        tmpSection.SetText(cboSection.GetValue());
        tmpCostCenter.SetText(cboCostCenter.GetValue());
    }

    function FilterCostCenter() {

    }
  
   

</script>
<style type="text/css">
    .hidden-div
    {
        display: none;
    }
        
    .tr-all-height
    {
        height: 30px;
    }
        
    .td-col-left
    {
        width: 100px;
        padding: 0px 0px 0px 10px;
    }
        
    .td-col-left2
    {
        width: 140px;
    }
        
        
    .td-col-mid
    {
        width: 10px;
    }
    .td-col-right
    {
        width: 200px;
    }
    .td-col-free
    {
        width: 20px;
    }
        
    .div-pad-fil
    {
        padding: 5px 5px 5px 5px;
    }
        
    .td-margin
    {
        padding: 10px;
    }
    .uppercase .dxeEditAreaSys  
            {  
                text-transform: uppercase;  
            }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">  
            <tr style="height:20px">
                <td class="td-col-left"></td>
                <td class="td-col-mid"></td>
                <td class="td-col-right"></td>

                <td class="td-col-free"></td>
                <td class="td-col-left"></td>
                <td class="td-col-mid"></td>
                <td class="td-col-right"></td>

                <td class="td-col-free"></td>
                <td class="td-col-left"></td>
                <td class="td-col-mid"></td>
                <td class="td-col-right"></td>

            </tr>      
            <tr class="tr-all-height">
                <td class="td-col-left">
                    <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Number">
                    </dx1:ASPxLabel>                 
                </td>
                <td class="td-col-mid"></td>
                <td class="td-col-right">
           
                    <dx1:ASPxTextBox ID="txtPRNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="300px" ClientInstanceName="txtPRNo" MaxLength="60" 
                                Height="25px" Text="--NEW--" BackColor="#CCCCCC" ReadOnly="True" >
                    </dx1:ASPxTextBox>
           
                </td>
                <td class="td-col-free"></td>
                <td class="td-col-left">
                    <dx1:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Type">
                    </dx1:ASPxLabel>                 
                </td>
                <td class="td-col-mid"></td>
                <td class="td-col-right">
           
                    <dx1:ASPxComboBox ID="cboPRType" runat="server" ClientInstanceName="cboPRType"
                            Width="150px" Font-Names="Segoe UI" DataSourceID="SqlDataSource2" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt"
                            Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" Enabled="False">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                         <ClientSideEvents KeyPress="function(s, e) {
                            if(e.htmlEvent.keyCode == 13) {
                              
                                ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
                            }
                        }" />
                        </dx1:ASPxComboBox>
           
                </td>
                <td class="td-col-free"></td>               
                <td class="td-col-left">
                    <dx1:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Cost Center">
                    </dx1:ASPxLabel>                 
                </td>
                <td class="td-col-mid">&nbsp;</td>
                <td class="td-col-right">
                    <dx1:ASPxComboBox ID="cboCostCenter" runat="server" ClientInstanceName="cboCostCenter"
                            Width="150px" Font-Names="Segoe UI" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <ClientSideEvents SelectedIndexChanged="GridLoad" />
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                      <ClientSideEvents KeyPress="function(s, e) {
                            if(e.htmlEvent.keyCode == 13) {
                              
                                ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
                            }
                        }" />
                     </dx1:ASPxComboBox>
                </td>
            </tr>   
            <tr class="tr-all-height">
                <td class="td-col-left">
                    <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Date">
                    </dx1:ASPxLabel>                 
                </td>
                <td class="td-col-mid"></td>
                <td class="td-col-right">
                   <dx:ASPxDateEdit ID="PRDate" runat="server" Theme="Office2010Black" 
                                        Width="150px" ClientInstanceName="PRDate"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom" Enabled="False" ReadOnly="True">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ClientSideEvents ValueChanged="DateAdd" />
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                </td>
                <td class="td-col-free"></td>
                <td class="td-col-left">
                    <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Department">
                    </dx1:ASPxLabel>                 
                </td>
                <td class="td-col-mid"></td>
                <td class="td-col-right">
           
                    <dx1:ASPxComboBox ID="cboDepartment" runat="server" ClientInstanceName="cboDepartment"
                            Width="181px" Font-Names="Segoe UI" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="19px">                            
                            <ClientSideEvents Init="function(s, e) {
	cbTmp.PerformCallback('Code');
}" SelectedIndexChanged="GridLoad" ValueChanged="FilterSection" />
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="180px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                         <ClientSideEvents KeyPress="function(s, e) {
                            if(e.htmlEvent.keyCode == 13) {
                              
                                ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
                            }
                        }" />
                        </dx1:ASPxComboBox>
           
                </td>                
                <td class="td-col-free"></td>   
                <td class="td-col-left">
                    <dx1:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Project">
                    </dx1:ASPxLabel>                 
                </td>    
                <td class="td-col-mid"></td>    
                <td class="td-col-right">
                    <dx1:ASPxTextBox ID="txtProject" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="150px" ClientInstanceName="txtProject" MaxLength="20" AutoCompleteType="Disabled"
                        Height="25px" >
                        <ClientSideEvents KeyPress="function(s, e) {
                            if(e.htmlEvent.keyCode == 13) {
                              
                                ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
                            }
                        }" />
                    </dx1:ASPxTextBox>
                </td>                     
            </tr> 
            <tr class="tr-all-height">
                <td class="td-col-left">
                    <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Budget">
                    </dx1:ASPxLabel>                 
                </td>
                <td class="td-col-mid"></td>
                <td class="td-col-right">
          
                   <dx1:ASPxComboBox ID="cboPRBudget" runat="server" ClientInstanceName="cboPRBudget"
                            Width="150px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">      
                            <ClientSideEvents SelectedIndexChanged="Description" />                           
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                         <ClientSideEvents KeyPress="function(s, e) {
                            if(e.htmlEvent.keyCode == 13) {
                              
                                ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
                            }
                        }" />
                        </dx1:ASPxComboBox>
           
                </td>
                <td class="td-col-free"></td>                
                <td class="td-col-left">
                    <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Section">
                    </dx1:ASPxLabel>                 
                </td>
                <td class="td-col-mid"></td>
                <td class="td-col-right">
                    <dx1:ASPxComboBox ID="cboSection" runat="server" ClientInstanceName="cboSection"
                            Width="183px" Font-Names="Segoe UI" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <ClientSideEvents SelectedIndexChanged="GridLoad"/>
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                             <ClientSideEvents KeyPress="function(s, e) {
                                if(e.htmlEvent.keyCode == 13) {
                              
                                    ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
                                }
                            }" />
                        </dx1:ASPxComboBox>
                </td>
                <td colspan="4"></td>
                
            </tr>              
            <tr style="height:20px" >
                <td colspan="11" >
                    
                </td>
             
            </tr>    
        </table>
    </div>
    <div class="hidden-div" >
        <dx:ASPxTextBox ID="txtFilter" runat="server" Width="60px" ClientInstanceName="txtFilter">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="txtFilter2" runat="server" Width="100px" ClientInstanceName="txtFilter2">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="txtNewPRNo" runat="server" Width="60px" 
            ClientInstanceName="txtNewPRNo">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="tmpDepartment" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                        Width="100px" ClientInstanceName="tmpDepartment" MaxLength="15" 
                        Theme="Office2010Silver">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="tmpCostCenter" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                        Width="100px" ClientInstanceName="tmpCostCenter" MaxLength="15" 
                        Theme="Office2010Silver">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="tmpSection" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                        Width="100px" ClientInstanceName="tmpSection" MaxLength="15" 
                        Theme="Office2010Silver">
        </dx:ASPxTextBox>
    </div>


    <div style="padding: 5px 5px 5px 5px">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRBudget'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
             SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRType'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
             SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Department'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
             SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Section'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
             SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'CostCenter'">
                    </asp:SqlDataSource>
                    <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
                    <ClientSideEvents CallbackComplete="SetCode" />
                </dx:ASPxCallback>
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Material_No" >

             <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing"  />

             <Columns>
                 <dx:GridViewDataTextColumn Caption="Material No" VisibleIndex="1" 
                     FieldName="Material_No">
                     <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="2" FieldName="Description" Width="300px">
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Spesification" VisibleIndex="3" FieldName="Specification" Width="450px">
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Qty" VisibleIndex="4" FieldName="Qty" Width="60px"  >
                     <PropertiesTextEdit MaxLength="6" DisplayFormatString="#,##0" >
                        <MaskSettings AllowMouseWheel="False" Mask="<0..99999g>" IncludeLiterals="DecimalSymbol" ></MaskSettings>
                     </PropertiesTextEdit>
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="UOM" VisibleIndex="5" FieldName="UOM" Width="60px">
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Remarks" VisibleIndex="6" FieldName="Remarks" Width="200px">
                     <PropertiesTextEdit MaxLength="50">
                     </PropertiesTextEdit>
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="Remarks" VisibleIndex="7" FieldName="No" Width="0px" Visible ="true">
                     <PropertiesTextEdit MaxLength="50">
                     </PropertiesTextEdit>
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="No" VisibleIndex="0" FieldName="No" Width="40px">
                 <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewDataTextColumn>
             </Columns>
             
             <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
             <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
             </SettingsPager>
               <SettingsEditing Mode="Batch" >
                <BatchEditSettings StartEditAction="Click"  ShowConfirmOnLosingChanges="False" />
            </SettingsEditing>
            <%-- <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                 <BatchEditSettings  ShowConfirmOnLosingChanges="false"  />
            </SettingsEditing>--%>
            <Settings VerticalScrollableHeight="260" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                            HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" />
                        <Styles>
                            <Header HorizontalAlign="Center">
                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                            </Header>
                        </Styles>
            <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors> 
            </dx:ASPxGridView>
    </div>

    <div style="padding: 5px 5px 5px 5px">
    <table style="width: 100%; border: 0px"> 
            <tr style="height: 2px">
                <td>
                
                </td>
                <td>&nbsp;</td>
                <td>     
                    <dx:ASPxCallback ID="cbApprove" runat="server" 
                        ClientInstanceName="cbApprove">
                    <ClientSideEvents CallbackComplete="MessageBox" Init="LoadForm" />                                           
                </dx:ASPxCallback></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:100px">         
                    <dx:ASPxCheckBox ID="chkUrgent" runat="server" ClientInstanceName="chkUrgent" 
                        Text="Urgent" ValueChecked="1" ValueType="System.Int32" ValueUnchecked="0" 
                            Font-Names="Segoe UI" Font-Size="9pt" CheckState="Unchecked">
                        <ClientSideEvents CheckedChanged="Check" />
                    
                    </dx:ASPxCheckBox>
                </td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Req. PO Issue Date">
                    </dx1:ASPxLabel>                 
                </td>
                <td>
                <dx:ASPxDateEdit ID="PODate" runat="server" Theme="Office2010Black" 
                                        Width="150px" ClientInstanceName="PODate"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 70px">
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Urgent Notes/Reason">
                    </dx1:ASPxLabel>                 
                </td>
                <td>
                    <dx1:ASPxMemo ID="txtUrgentNote" runat="server" Height="50px" Width="300px" 
                        ClientInstanceName="txtUrgentNote" Font-Names="Segoe UI" Font-Size="9pt"
                        Theme="Metropolis" MaxLength="300" CssClass ="uppercase">
                        
                    </dx1:ASPxMemo>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>        
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            
            <tr style="display:none">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr style="height: 2px">
                <td style=" padding:0px 0px 0px 10px" colspan="3">
               

                    &nbsp;&nbsp;
                    <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>

                    &nbsp;&nbsp;

                    <dx1:ASPxButton ID="btnDraft" runat="server" Text="Draft" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnDraft" Theme="Default" >                        
                     
                        <ClientSideEvents Click="DraftProcess" />
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>

                    &nbsp;&nbsp;

                    <dx1:ASPxButton ID="btnModify" runat="server" Text="Edit Item" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnModify" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>

                    &nbsp;

                    <dx1:ASPxButton ID="btnDelete" runat="server" Text="Delete" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnDelete" Theme="Default" >                        
                        <ClientSideEvents Click="function(s, e) {
	                            var msg = confirm('Are you sure want to delete this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }

                                cbApprove.PerformCallback('delete');
                        }" />
                     
                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>

                    &nbsp;

                    <dx1:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnSubmit" Theme="Default" >                        
                     
                        <ClientSideEvents Click="function(s, e) {
	                            var msg = confirm('Are you sure want to submit this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }
                                SubmitProcess();

                        }" />
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>


                    &nbsp;

                    <dx1:ASPxButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnPrint" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>


                    </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
    </table>
    </div>
</div>
</asp:Content>

﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="SupplierQuotationDetail.aspx.vb" Inherits="IAMI_EPIC2.SupplierQuotationDetail" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        window.onload = function () {
            var GetDocumentScrollTop = function () {
                var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollTop > 0;
                if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                    if (ASPx.Browser.MacOSMobilePlatform)
                        return window.pageYOffset;
                    else if (ASPx.Browser.WebKitFamily)
                        return document.documentElement.scrollTop || document.body.scrollTop;
                    return document.body.scrollTop;
                }
                else
                    return document.documentElement.scrollTop;
            };
            var _aspxGetDocumentScrollTop = function () {
                if (__aspxWebKitFamily) {
                    if (__aspxMacOSMobilePlatform)
                        return window.pageYOffset;
                    else
                        return document.documentElement.scrollTop || document.body.scrollTop;
                }
                else
                    return document.documentElement.scrollTop;
            }
            if (window._aspxGetDocumentScrollTop) {
                window._aspxGetDocumentScrollTop = _aspxGetDocumentScrollTop;
                window.ASPxClientUtils.GetDocumentScrollTop = _aspxGetDocumentScrollTop;
            } else {
                window.ASPx.GetDocumentScrollTop = GetDocumentScrollTop;
                window.ASPxClientUtils.GetDocumentScrollTop = GetDocumentScrollTop;
            }
            /* Begin -> Correct ScrollLeft  */
            var GetDocumentScrollLeft = function () {
                var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollLeft > 0;
                if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                    if (ASPx.Browser.MacOSMobilePlatform)
                        return window.pageXOffset;
                    else if (ASPx.Browser.WebKitFamily)
                        return document.documentElement.scrollLeft || document.body.scrollLeft;
                    return document.body.scrollLeft;
                }
                else
                    return document.documentElement.scrollLeft;
            };
            var _aspxGetDocumentScrollLeft = function () {
                if (__aspxWebKitFamily) {
                    if (__aspxMacOSMobilePlatform)
                        return window.pageXOffset;
                    else
                        return document.documentElement.scrollLeft || document.body.scrollLeft;
                }
                else
                    return document.documentElement.scrollLeft;
            }
            if (window._aspxGetDocumentScrollLeft) {
                window._aspxGetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
                window.ASPxClientUtils.GetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
            } else {
                window.ASPx.GetDocumentScrollLeft = GetDocumentScrollLeft;
                window.ASPxClientUtils.GetDocumentScrollLeft = GetDocumentScrollLeft;
            }
            /* End -> Correct ScrollLeft  */
        };

        var currentColumnName;
        var dataExist;
        var dataSubmitted;
        var CompleteInput;
        var QuotationExits;


        function FillComboRFQNumber() {
            cboRFQNumber.PerformCallback();
        }

        function GetInformation() {
            if (cboRFQNumber.GetSelectedIndex() == -1) {
                return;
            }

            cb.PerformCallback();
            cbExist.PerformCallback('exist');
            Grid.PerformCallback("load");
            //CHECK: If data exist, load attachment

            //alert(dataExist);
           
                //if (dataExist == "1") {
                  GridAtc.PerformCallback(); 
                //}
           
        }

        function IsDataExist(s, e) {
            cbExist.PerformCallback('exist');
            dataExist = s.cpResult;
            txtQuotationNo.SetEnabled(false);
            dtQuotationDate.readOnly = true;
            //dtQuotationDate.SetEnabled(false);
            cboRFQNumber.SetEnabled(false);
            //0=NOT EXIST, 1=EXIST
            return dataExist;
        }

        function SetInformation(s, e) {
            //alert('aa');
            txtQuotationNo.SetText(s.cpQuotationNo);
            txtRev.SetText(s.cpRevNo);
            txtRFQTitle.SetText(s.cpRFQTitle);
            memoNote.SetText(s.cpNote);
            dtQuotationDate.SetText(s.cpQuotationDate);
//            var dt = s.cpQuotationDate;
//            var YearFrom = dt.getFullYear();
//            var MonthFrom = dt.getMonth();
//            var DateFrom = dt.getDate();
//            var newdate = new Date(YearFrom, MonthFrom, DateFrom)

            //alert(s.cpDisabledHeader);
            if (s.cpDisabledHeader == "1") {
                txtQuotationNo.SetEnabled(false);
                //dtQuotationDate.SetEnabled(false);
                dtQuotationDate.readOnly = true;
                cboRFQNumber.SetEnabled(false);
            } else {
                txtQuotationNo.SetEnabled(true);
                dtQuotationDate.readOnly = false;
                //dtQuotationDate.SetEnabled(true);
                cboRFQNumber.SetEnabled(true);
            }

            if (s.cpDisabledButton == "1") {
                memoNote.SetEnabled(false);
                btnDraft.SetEnabled(false);
                btnSubmit.SetEnabled(false);
            } else {
                memoNote.SetEnabled(true);
                btnDraft.SetEnabled(true);
                btnSubmit.SetEnabled(true);
            }
        }



        function ShowConfirmOnLosingChanges() {
            var C = confirm("The data you have entered may not be saved. Are you sure want to leave?");
            if (C == true) {
                return true;
            } else {
                return false;
            }
        }


        function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;

            if (currentColumnName == 'Material_No') {
                e.cancel = true;
                Grid.batchEditApi.EndEdit();

            } else if (currentColumnName == 'Description') {
                e.cancel = true;
                Grid.batchEditApi.EndEdit();

            } else if (currentColumnName == 'Specification') {
                e.cancel = true;
                Grid.batchEditApi.EndEdit();

            } else if (currentColumnName == 'Qty') {
                e.cancel = true;
                Grid.batchEditApi.EndEdit();

            } else if (currentColumnName == 'UOM') {
                e.cancel = true;
                Grid.batchEditApi.EndEdit();

            } else if (currentColumnName == 'TotalPrice') {
                e.cancel = true;
                Grid.batchEditApi.EndEdit();

            } else if (currentColumnName == 'Quotation_No') {
                e.cancel = true;
                Grid.batchEditApi.EndEdit();

            } else if (currentColumnName == 'Currency') {
                e.cancel = true;
                Grid.batchEditApi.EndEdit();
                //            cbExist.PerformCallback('IsSubmitted');
                //            window.setTimeout(function () {
                //                if (dataSubmitted == "1") {
                //                    e.cancel = true;
                //                    Grid.batchEditApi.EndEdit();
                //                    window.setTimeout(function () { toastr.warning('Cannot update Currency! The data already submitted.', 'Warning'); }, 10);
                //                } else if (btnDraft.GetEnabled() == false) {
                //                    e.cancel = true;
                //                    Grid.batchEditApi.EndEdit();
                //                    window.setTimeout(function () { toastr.warning('Cannot update Currency! The data already submitted.', 'Warning'); }, 10);
                //                }
                //            }, 100);

            } else if (currentColumnName == 'Price') {
                cbExist.PerformCallback('IsSubmitted');
                window.setTimeout(function () {
                    if (dataSubmitted == "1") {
                        e.cancel = true;
                        Grid.batchEditApi.EndEdit();
                        window.setTimeout(function () { toastr.warning('Cannot update Price/Pcs! The data already submitted.', 'Warning'); }, 10);
                    } else if (btnDraft.GetEnabled() == false) {
                        e.cancel = true;
                        Grid.batchEditApi.EndEdit();
                        window.setTimeout(function () { toastr.warning('Cannot update Price/Pcs! The data already submitted.', 'Warning'); }, 10);
                    }
                }, 10);

            } else if (currentColumnName == 'FileType') {
                e.cancel = true;
                GridAtc.batchEditApi.EndEdit();

            } else if (currentColumnName == 'FileName') {
                e.cancel = true;
                GridAtc.batchEditApi.EndEdit();

            }
        }

        function OnBatchEditEndEditing(s, e) {
            window.setTimeout(function () {
                var price = s.batchEditApi.GetCellValue(e.visibleIndex, "Price");
                var qty = s.batchEditApi.GetCellValue(e.visibleIndex, "Qty");

                if (currentColumnName == "Price") {
                    s.batchEditApi.SetCellValue(e.visibleIndex, "TotalPrice", price * qty);
                }

                if (currentColumnName == "TotalPrice") {
                    s.batchEditApi.SetCellValue(e.visibleIndex, "TotalPrice", price * qty);
                }
            }, 10);
        }




        function OnFileUploadStart(s, e) {
            btnAddFile.SetEnabled(false);
        }

        function OnTextChanged(s, e) {
            btnAddFile.SetEnabled(s.GetText() != "");
        }

        function OnFileUploadComplete(s, e) {
            btnAddFile.SetEnabled(s.GetText() != "");

            if (s.cpType == "0") {
                //INFO
                toastr.info(s.cpMessage, 'Information');

            } else if (s.cpType == "1") {
                //SUCCESS
                toastr.success(s.cpMessage, 'Success');

            } else if (s.cpType == "2") {
                //WARNING
                toastr.warning(s.cpMessage, 'Warning');

            } else if (s.cpType == "3") {
                //ERROR
                toastr.error(s.cpMessage, 'Error');

            }

            window.setTimeout(function () {
                delete s.cpType;
            }, 10);
        }



        function inputValidation() {
            if (cboRFQNumber.GetText() == '') {
                cboRFQNumber.Focus();
                toastr.warning("Please select RFQ Number first!", "Warning");
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }

            if (cboRFQNumber.GetSelectedIndex() == -1) {
                cboRFQNumber.Focus();
                toastr.warning('Invalid RFQ Number!', 'Warning');
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }

            if (txtQuotationNo.GetText() == '') {
                txtQuotationNo.Focus();
                toastr.warning('Please input Quotation Number first!', 'Warning');
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }

            return true;
        }

        function cbExistEndCallback(s, e) {
            if (s.cpParameter == "exist") {
                
                QuotationExits = s.cpResultExits;
                dataExist = s.cpResult;
                
                //alert(s.cpResult);
            } else if (s.cpParameter == "IsSubmitted") {
                dataSubmitted = s.cpResult;
                
                //QuotationExits = s.cpResultExits;

            } else if (s.cpParameter == "draft") {
                txtQuotationNo.SetText(s.cpQuotationNo);
                txtRev.SetText(s.cpRevNo);
                memoNote.SetText(s.cpNote);

                var dt = s.cpQuotationDate;
                var YearFrom = dt.getFullYear();
                var MonthFrom = dt.getMonth();
                var DateFrom = dt.getDate();
                var newdate = new Date(YearFrom, MonthFrom, DateFrom)

                // dtQuotationDate.SetDate(newdate);

           
            } else if (s.cpParameter == "submit") {
                //alert(s.cpResultSubmit);
                if (s.cpResultSubmit == "Y") {
                    btnSubmit.SetEnabled(false);
                    btnDraft.SetEnabled(false);
                    
                } else if (s.cpResultSubmit == "N"){
                    btnDraft.SetEnabled(true);
                    btnSubmit.SetEnabled(true);
                } 
                ucAtc.SetEnabled(false);
            
            }
        }

        function Draft() {
            if (txtQuotationNo.GetText() == '') {
                toastr.warning('Please input Quotation No!', 'Warning');
                return false;
            }
            startIndex = 0;
            var price = 0;
            price = parseFloat(price);

            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                price = parseFloat(Grid.batchEditApi.GetCellValue(i, "Price"));

                if (price == 0) {
                    price = 0;
                } else if (price < 0) {
                    //alert('price must greater than zero ');
                    toastr.warning('price must greater than zero !', 'Warning');
                    Grid.batchEditApi.SetCellValue(i, "TotalPrice", 0);
                    Grid.batchEditApi.SetCellValue(i, "Price", 0);
                    return false;
                }
            }

            if (inputValidation() == true) {
                cbExist.PerformCallback('exist');

                window.setTimeout(function () {
                    //VALIDATE: Is data exist or not

                    if (dataExist == "1") {

                        //toastr.info('Data save successfully!', 'Information');

                        if (Grid.batchEditApi.HasChanges() == true) {
                            Grid.UpdateEdit();
                            window.setTimeout(Grid.PerformCallback('save'), 10);
                            Grid.CancelEdit();

                        } else {
                            cbExist.PerformCallback("draft");
                            window.setTimeout(Grid.PerformCallback('load'), 100);
                        }

                        cboRFQNumber.SetEnabled(false);
                        txtQuotationNo.SetEnabled(false);
                        dtQuotationDate.readOnly = true;
                       // dtQuotationDate.SetEnabled(false);

                        $('html, body').animate({ scrollTop: 0 }, 'slow');



                    } else {

                        if (Grid.batchEditApi.HasChanges() == true) {
                            Grid.UpdateEdit();
                            //window.setTimeout(SetInformation(), 10);
                            window.setTimeout(function () {
                                Grid.PerformCallback('save');
                            }, 200);
                            //Grid.CancelEdit();

                        } else {
                            toastr.warning('Please input/update the detail data!', 'Warning');
                        }

                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }
                }, 500);

            }
        }

        function Submit() {
            if (Grid.batchEditApi.HasChanges() == true) {
                //there is an update data
                toastr.warning('Please draft the data first before submit!', 'Warning');
                btnDraft.Focus();
                return;
            }

            cbExist.PerformCallback('exist');

            window.setTimeout(function () {
                //VALIDATE: Is data exist or not
                if (dataExist == "1") {
                    var C = confirm("Are you sure want to submit the data?");

                    if (C == true) {
                        cbExist.PerformCallback("submit");

                       
                        window.setTimeout(function () {
                           
                            txtQuotationNo.SetEnabled(false);
                            dtQuotationDate.readOnly = true;
                            //dtQuotationDate.SetEnabled(false);
                            memoNote.SetEnabled(false);
                            cboRFQNumber.SetEnabled(false);
                            btnDraft.SetEnabled(false);
                            btnSubmit.SetEnabled(false);
                            window.setTimeout(function () {
                                Grid.PerformCallback('load');
                            }, 50);

                            $('html, body').animate({ scrollTop: 0 }, 'slow');
                        }, 10);
                    }
                } else {
                    toastr.warning('Please draft the data first before submit!', 'Warning');
                    btnDraft.Focus();
                }
            }, 300);
        }

        function Back() {
            if (Grid.batchEditApi.HasChanges() == true) {
                //there is an update data
                if (ShowConfirmOnLosingChanges() == false) {
                    return;
                }
            }

            location.replace("SupplierQuotation.aspx");
        }

        function AddFile() {
            if (Grid.batchEditApi.HasChanges() == true) {
                //there is an update data
                toastr.warning('Please draft the data first before Add file!', 'Warning');
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                btnDraft.Focus();
                return;
            }
            
            if (cboRFQNumber.GetText() == "") {
                cboRFQNumber.Focus();
                toastr.warning('Please select RFQ Number first!', 'Warning');
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }

            if (cboRFQNumber.GetSelectedIndex() == -1) {
                toastr.warning('Invalid RFQ Number!', 'Warning');
                cboRFQNumber.Focus();
                return;
            }

            if (txtQuotationNo.GetText() == "") {
                txtQuotationNo.Focus();
                toastr.warning('Please input Quotation Number first!', 'Warning');
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }

            if (Grid.GetVisibleRowsOnPage() == 0) {
                toastr.warning('Invalid RFQ Number and Quotation Number!', 'Warning');
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            }

            cbExist.PerformCallback('IsSubmitted');
            window.setTimeout(function () {
                if (dataSubmitted == "1") {
                    toastr.warning('Cannot add attachment! The data already submitted.', 'Warning');
                    return false;

                } else {

                    if (btnSubmit.GetEnabled() == false && dataSubmitted == "1") {
                        toastr.warning('Cannot add attachment! The data already submitted/rejected.', 'Warning');
                        return false;
                    }

                    cbExist.PerformCallback('exist');
                    hf.Set("QuotationNo", txtQuotationNo.GetText());
                    hf.Set("RevisionNo", txtRev.GetText());
                    hf.Set("RFQNumber", cboRFQNumber.GetText());
                    window.setTimeout(function () {
                        if (dataExist == "1") {
                            ucAtc.Upload();
                            //alert('aaa');
                            
                        } else {
                            toastr.warning('Quotation Number does not exist! Please save the data first before add attachment.', 'Warning');
                        }
                        window.setTimeout(function () { GridAtc.PerformCallback(); }, 100);
                    }, 1000);

                }
            }, 500);

        }

        function OnGetSelectedFieldValues(selectedValues) {
            //VALIDATE WHEN THERE IS NO SELECTED DATA
            if (selectedValues.length == 0) {
                toastr.warning('There is no selected data to delete!', 'Warning');
                return;
            }

            //GET FILE NAME
            var sValues;
            var sValuesSeqNo;
            var resultSeqNo = "";
            var result;
            result = "";

            for (i = 0; i < selectedValues.length; i++) {
                sValues = "";
                sValuesSeqNo = "";
                for (j = 0; j < selectedValues[i].length; j++) {
                    sValues = sValues + selectedValues[i][j];
                    sValuesSeqNo = sValuesSeqNo + selectedValues[i][j];
                }
                result = result + sValues.split("|", 2).slice(-1)[0] + "|";
                resultSeqNo = resultSeqNo + sValuesSeqNo.split("|", 1)[0] + "|";
            }
           
            //CONFIRMATION
            var C = confirm("Are you sure want to delete selected attachment (" + result + ") ?");
            if (C == false) {
                return;
            }

            //KEEP FILE NAME INTO HIDDENFIELD
            hf.Set("DeleteAttachment", result);
            hf.Set("SeqNo", resultSeqNo);

            //EXECUTE TO DATABASE AFTER DELAY 300ms
            window.setTimeout(function () {
                cb.PerformCallback("DeleteAttachment");
            }, 300);
        }

        function DeleteAttachment() {
            cbExist.PerformCallback('IsSubmitted');
            window.setTimeout(function () {
                if (dataSubmitted == "1") {
                    toastr.warning('Cannot delete attachment! The data already submitted.', 'Warning');
                    return;

                } else {

                    if (btnSubmit.GetEnabled() == false) {
                        toastr.warning('Cannot delete attachment! The data already submitted/rejected.', 'Warning');
                        return;
                    }

                    //INITIALIZE PARAMETER
                    hf.Set("DeleteAttachment", "");
                    hf.Set("SeqNo", "");

                    //GridAtc.GetSelectedFieldValues("FileName", OnGetSelectedFieldValues);
                    GridAtc.GetSelectedFieldValues("Seq_No;Separator;FileName", OnGetSelectedFieldValues);
                }
            }, 500);
        }



        function LoadCompleted(s, e) {
            //alert("wkwkw");
            if (s.cpActionAfter == "save") {
                txtQuotationNo.SetText(s.cpQuotationNo);
                txtRev.SetText(s.cpRevNo);
                memoNote.SetText(s.cpNote);



//                var dt = s.cpQuotationDate;
//                //dt = new Date("2015-03-25");
//                if (dt == undefined) {
//                    dt = new Date();
//                    var YearFrom = dt.getFullYear();
//                    var MonthFrom = dt.getMonth();
//                    var DateFrom = dt.getDate();
//                    var newdate = new Date(YearFrom, MonthFrom, DateFrom);
//                    //dtQuotationDate.SetDate(newdate);
//                    //alert(newdate );

//                }


            }

            if (s.cpActionAfter == "DeleteAtc") {
                GridAtc.PerformCallback();
            }

            if (s.cpCompleteInput != "undefined") {
                CompleteInput = s.cpCompleteInput;

                if (CompleteInput == "Y") {
                    Grid.CancelEdit();

                    if (s.cpDisabledHeader == "1") {
                        txtQuotationNo.SetEnabled(false);
                        //dtQuotationDate.SetEnabled(false);

                        dtQuotationDate.readOnly = true;
                        cboRFQNumber.SetEnabled(false);
                        if (s.cpResult == "0") {
                            //alert("oke 2");
                            btnDraft.SetEnabled(true);
                            btnSubmit.SetEnabled(true);

                        } else if (s.cpResult == "1") {
                            btnDraft.SetEnabled(false);
                            btnSubmit.SetEnabled(false);
                        }    
                    } else {
                        txtQuotationNo.SetEnabled(true);
                        //dtQuotationDate.SetEnabled(true);
                        cboRFQNumber.SetEnabled(true);
                        dtQuotationDate.readOnly = false;
                    }
                }
            }

            if (s.cpType == "0") {
                //INFO
                toastr.info(s.cpMessage, 'Information');

            } else if (s.cpType == "1") {
                //SUCCESS
            //alert("oke");
                toastr.success(s.cpMessage, 'Success');
                       
            } else if (s.cpType == "2") {
                //WARNING
                toastr.warning(s.cpMessage, 'Warning');

            } else if (s.cpType == "3") {
                //ERROR
                toastr.error(s.cpMessage, 'Error');

            }


            window.setTimeout(function () {
                delete s.cpType;
            }, 10);
        }


        //popup image material
        var keyValue;
        function OnMoreInfoClick(contentUrl) { //element, key) {
            callbackPanel.SetContentHtml("");
            popup.Show(); //AtElement(element);
            keyValue = contentUrl;
            console.log(keyValue);
        }

        function OnMoreInfoClick2() {
            callbackPanel.SetContentHtml("");
            popup.Show();
            //keyValue = key;
        }

//        function OnMoreInfoClick(contentUrl) {
//            clientPopupControl.SetContentUrl(contentUrl);
//            clientPopupControl.Show();
//        }


        function OnMouseLeave() {
            callbackPanel.SetContentHtml("");
            popup.Hide(); // ShowAtElement(element);
            //keyValue = key;
        }

        function popup_Shown(s, e) {
            callbackPanel.PerformCallback(keyValue);
        }

        function getHeight(length, ratio) {
            var height = ((length) / (Math.sqrt((Math.pow(ratio, 2) + 1))));
            return Math.round(height);
        }

        function getWidth(length, ratio) {
            var width = ((length) / (Math.sqrt((1) / (Math.pow(ratio, 2) + 1))));
            return Math.round(width);
        }


        function resizeToMax() {
            var div1 = document.getElementById('Content_popup_PWC-1');

            myImage = new Image()
            var img = document.getElementById('Content_popup_callbackPanel_edBinaryImage');
            myImage.src = img.src;

            console.log(img.width);

            if (img.width != 200){
            //example
                var ratio = (9/16);
                var height = getHeight(500, ratio);
                var width = getWidth(height, ratio);
                img.style.width = width+"px";
                img.style.height = height+"px";
            }

//            console.log(width);
//            console.log(height);
//            if (myImage.width > 1024 || myImage.height > 1024) {
//                
//            }
             

//            if (myImage.width / div1.style.width > myImage.height / div1.style.height) {
//                img.style.width = "100%";
//                //img.style.width = "500px" ;
//            } else {
//                img.style.height = "100%";
//            }

            //ratio(myImage.width, myImage.height);

        }

        function resizePopupFitToImage() {
            var div1 = document.getElementById('Content_popup_PWC-1');
            if (div1.style.width / document.body.clientWidth > div1.height / document.body.clientHeight) {
                div1.style.width = "100%";
            }
            else {
                div1.style.height= "100%";
            }
        }
        // popup material


    </script>
    <style type="text/css">
        .colwidthbutton
        {
            width: 90px;
        }
        
        .rowheight
        {
            height: 35px;
        }
        
        .col1
        {
            width: 10px;
        }
        .col2
        {
            width: 133px;
        }
        .col3
        {
            width: 200px;
        }
        .col4
        {
            width: 10px;
        }
        .col5
        {
            width: 30px;
        }
        .col6
        {
            width: 80px;
        }
        .col7
        {
            width: 133px;
        }
        .col8
        {
            width: 100px;
        }
        
        .customHeader
        {
            height: 32px;
        }
        
         
        .Image
        {   
             border-radius: 10px;
             object-fit: scale-down;
        }
        
        .center-screen 
        {
            border-radius: 10px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr style="height: 10px">
                    <td colspan="9"></td>
                </tr>
                <tr class="rowheight">
                    <td class="col1">
                    </td>
                    <td class="col2">
                        <dx1:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier Code">
                        </dx1:ASPxLabel>
                    </td>
                    <td colspan="7">
                        <table>
                            <tr>
                                <td class="col2">
                                    <dx:ASPxTextBox ID="txtSupplierCode" runat="server" Width="120px" Height="25px" ClientInstanceName="txtSupplierCode"
                                        BackColor="#CCCCCC" ReadOnly="True" TabIndex="-1">
                                    </dx:ASPxTextBox>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="txtSupplierName" runat="server" Width="300px" Height="25px" ClientInstanceName="txtSupplierName"
                                        BackColor="#CCCCCC" ReadOnly="True" TabIndex="-1">
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="RFQ Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="col3" colspan="7">
                        <dx1:ASPxComboBox ID="cboRFQNumber" runat="server" ClientInstanceName="cboRFQNumber"
                            Width="200px" Font-Names="Segoe UI" TextField="RFQ_Number" ValueField="RFQ_Number"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents Init="FillComboRFQNumber" />
                            <Columns>
                                <dx:ListBoxColumn Caption="RFQ Number" FieldName="RFQ_Number" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                            <ClientSideEvents Init="FillComboRFQNumber" ValueChanged="GetInformation" />
                            <ValidationSettings CausesValidation="false" Display="Static">
                                <RequiredField IsRequired="true" ErrorText="*" />
                            </ValidationSettings>
                            <InvalidStyle BackColor="Red">
                            </InvalidStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    
                    
                    <%-- <td class="col6">
                    </td>
                <td class="col7">
                    </td>
                <td class="col8">
                    </td>
                <td>
                    </td>--%>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                        <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="RFQ Title">
                        </dx1:ASPxLabel>
                    </td>
                    <td colspan="7">
                        <dx:ASPxTextBox ID="txtRFQTitle" runat="server" Width="200px" Height="25px" ClientInstanceName="txtRFQTitle"
                            BackColor="#CCCCCC" ReadOnly="True" TabIndex="-1">
                        </dx:ASPxTextBox>
                    </td>
                   
                    <%-- <td>
                    </td>
                <td>
                    </td>
                <td>
                    &nbsp;</td>
                <td>
                    </td>--%>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                        <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Quotation Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td colspan="7">
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxTextBox ID="txtQuotationNo" runat="server" Width="200px" Height="25px" ClientInstanceName="txtQuotationNo" MaxLength="30" AutoCompleteType="Disabled" >
                                        <ValidationSettings CausesValidation="false" Display="None">
                                            <RequiredField IsRequired="true" ErrorText="*" />
                                        </ValidationSettings>
                                        <InvalidStyle BackColor="Red">
                                        </InvalidStyle>
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="col1"></td>
                                <td>
                                    <dx:ASPxTextBox ID="txtRev" runat="server" Width="30px" Height="25px" ClientInstanceName="txtRev"
                                        HorizontalAlign="Center" BackColor="#CCCCCC" ReadOnly="true" TabIndex="-1">
                                    </dx:ASPxTextBox>
                                </td>
                                <td class="col1"></td>
                                <td>
                                    <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                        Text="Quotation Date">
                                    </dx1:ASPxLabel>
                                </td>
                                <td class="col1"></td>
                                <td>
                                    <dx:ASPxDateEdit ID="dtQuotationDate" runat="server" Theme="Office2010Black" Width="120px"
                                        ClientInstanceName="dtQuotationDate" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" BackColor="#CCCCCC" ReadOnly="true" >
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                                        </ButtonStyle>
                                    </dx:ASPxDateEdit>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="9">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="8">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                        &nbsp;
                        <dx:ASPxCallback ID="cb" runat="server" ClientInstanceName="cb">
                            <ClientSideEvents EndCallback="SetInformation" CallbackComplete="LoadCompleted" />
                        </dx:ASPxCallback>
                        &nbsp;
                        <dx:ASPxCallback ID="cbExist" runat="server" ClientInstanceName="cbExist">
                            <ClientSideEvents EndCallback="cbExistEndCallback" CallbackComplete="LoadCompleted" />
                        </dx:ASPxCallback>
                        &nbsp;
                        <asp:SqlDataSource ID="dsCurr" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="SELECT Par_Code = RTRIM(Par_Code) FROM Mst_Parameter WHERE Par_Group = 'Currency'">
                        </asp:SqlDataSource>
                        &nbsp;
                        <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf">
                        </dx:ASPxHiddenField>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" Width="100%" Heigth="100px" Font-Names="Segoe UI"
                Font-Size="9pt" KeyFieldName="Material_No">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Material No" FieldName="Material_No" VisibleIndex="0" Width="120px">
                        <DataItemTemplate>
                            <dx:ASPxHyperLink ID="ItemNo" runat="server" Text='<%# Eval("Material_No") %>'  OnInit ="hyperLink_Init">
                                
                            </dx:ASPxHyperLink>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                   
                    <%-- <dx:GridViewDataTextColumn Caption="Image" FieldName="ImageBinary" EditFormSettings-Visible="False" VisibleIndex="3" Width="90px">
                                <DataItemTemplate>
                                    <a href="javascript:void(0);" 
                                    onclick ="OnMoreInfoClick(this, '<%# Container.KeyValue %>')">
                                        <dx:ASPxBinaryImage ID="img" runat="server" Value='<%# Eval("ImageBinary") %>' Width="70px" Height="60px" Border-BorderStyle="None" ></dx:ASPxBinaryImage>
                                    </a>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>--%>
                    <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" VisibleIndex="1"
                        Width="200px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Specification" FieldName="Specification" VisibleIndex="2"
                        Width="300px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Qty" FieldName="Qty" VisibleIndex="3" Width="60px">
                        <PropertiesTextEdit DisplayFormatString="###,###">
                        </PropertiesTextEdit>
                        <CellStyle HorizontalAlign="Right">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="UOM" FieldName="UOM" VisibleIndex="4" Width="50px">
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Currency" FieldName="Currency" VisibleIndex="5"
                        Width="70px">
                        <PropertiesComboBox DataSourceID="dsCurr" TextField="Par_Code" ValueField="Par_Code"
                            DropDownRows="5" Width="65px">
                        </PropertiesComboBox>
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataTextColumn Caption="Price/Pcs" FieldName="Price" 
                     VisibleIndex="6" Width="100px">
                     <PropertiesTextEdit DisplayFormatString="###,##0" Width="95px">
                         <Style HorizontalAlign="Right">
                         </Style>
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Right">
                     </CellStyle>
                </dx:GridViewDataTextColumn>
                  <%--  <dx:GridViewDataSpinEditColumn Caption="Price/Pcs" FieldName="Price" VisibleIndex="6"
                        Width="80px">
                        <PropertiesTextEdit DisplayFormatString="#,###.00" MaxLength="11">
                            <MaskSettings IncludeLiterals="DecimalSymbol" Mask="&lt;0..999999999g&gt;.&lt;00..99&gt;" />
                            <ValidationSettings ErrorDisplayMode="None">
                            </ValidationSettings>
                            <ClientSideEvents TextChanged="UpdateTotalAmount" />
                            <Style HorizontalAlign="Right"></Style>
                        </PropertiesTextEdit>
                        <propertiesspinedit displayformatstring="###,##0" width="75px" >
                            <style horizontalalign="right"></style>
                        </propertiesspinedit>
                     </dx:GridViewDataSpinEditColumn>--%>
                    <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice" VisibleIndex="7"
                        Width="100px">
                        <PropertiesTextEdit DisplayFormatString="###,##0">
                        </PropertiesTextEdit>
                        <EditCellStyle BackColor="LemonChiffon">
                        </EditCellStyle>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowSelectByRowClick="True" AllowSort="False" />
                <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                </SettingsPager>
                <SettingsEditing Mode="Batch">
                    <BatchEditSettings StartEditAction="Click" ShowConfirmOnLosingChanges="False" />
                </SettingsEditing>
                <ClientSideEvents CallbackError="function(s, e) {e.handled = true;}" EndCallback="LoadCompleted"
                    BatchEditStartEditing="OnBatchEditStartEditing" BatchEditEndEditing="OnBatchEditEndEditing" />
                <Settings HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" VerticalScrollableHeight="175"
                    ShowStatusBar="Hidden" />
                <Styles Header-Paddings-Padding="2px" EditFormColumnCaption-Paddings-PaddingLeft="0px"
                    EditFormColumnCaption-Paddings-PaddingRight="0px">
                    <Header CssClass="customHeader" HorizontalAlign="Center">
                    </Header>
                </Styles>
            </dx:ASPxGridView>
        </div>
        <br />
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
                <tr>
                    <td class="col1">
                    </td>
                    <td colspan="8">
                        <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Note :">
                        </dx1:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="7">
                        <dx:ASPxMemo ID="memoNote" runat="server" Height="45px" Width="100%" ClientInstanceName="memoNote"
                            MaxLength="300">
                        </dx:ASPxMemo>
                    </td>
                    <td class="col1">
                    </td>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="8">
                        <dx1:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Attachment :">
                        </dx1:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="7">
                        <dx:ASPxGridView ID="GridAtc" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridAtc"
                            EnableTheming="True" Theme="Office2010Black" Width="100%" Settings-VerticalScrollableHeight="80"
                            Settings-VerticalScrollBarMode="Visible" Font-Names="Segoe UI" Font-Size="9pt"
                            KeyFieldName="FileName;FilePath">
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Caption=" "
                                    Width="30px">
                                </dx:GridViewCommandColumn>
                                <%--<dx:GridViewDataCheckColumn Caption=" " Name="chk" VisibleIndex="0" Width="30px">
                                <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueType="System.Int32" >
                                </PropertiesCheckEdit>
                            </dx:GridViewDataCheckColumn>--%>
                                <dx:GridViewDataTextColumn Caption="Sequence No" FieldName="Seq_No" Visible ="false"
                                 VisibleIndex="1" Width="80px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="File Type" FieldName="FileType" VisibleIndex="2"
                                    Width="80px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="File Name" FieldName="FileName" VisibleIndex="3"
                                    Width="500px">
                                    <DataItemTemplate>
                                        <dx:ASPxHyperLink ID="linkFileName" runat="server" OnInit="keyFieldLink_Init">
                                        </dx:ASPxHyperLink>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="File Path" FieldName="FilePath" VisibleIndex="4"
                                    Width="0px" Visible="false">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Separator" FieldName="Separator" Visible ="false"
                                 VisibleIndex="5" Width="80px">
                            </dx:GridViewDataTextColumn>  
                            </Columns>
                            <SettingsEditing Mode="Batch">
                                <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                            </SettingsEditing>
                            <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSort="False"
                                EnableRowHotTrack="True" />
                            <Settings VerticalScrollableHeight="80" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto">
                            </Settings>
                            <Styles>
                                <Header CssClass="customHeader" HorizontalAlign="Center">
                                </Header>
                            </Styles>
                            <ClientSideEvents CallbackError="function(s, e) {e.handled = true;}" 
                                              EndCallback="LoadCompleted"
                                              BatchEditStartEditing="OnBatchEditStartEditing" />
                        </dx:ASPxGridView>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="rowheight">
                        <dx:ASPxButton ID="btnDeleteAtc" runat="server" Text="Delete Selected Attachment"
                            UseSubmitBehavior="False" Font-Names="Segoe UI" Font-Size="9pt" Width="150px"
                            Height="25px" AutoPostBack="False" ClientInstanceName="btnDeleteAtc" Theme="Default">
                            <ClientSideEvents Click="DeleteAttachment" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="7">
                        <dx:ASPxUploadControl ID="ucAtc" runat="server" ClientInstanceName="ucAtc" Width="50%"
                            Height="30px">
                            <ValidationSettings AllowedFileExtensions=".pdf">
                            </ValidationSettings>
                            <BrowseButton Text="Browse...">
                            </BrowseButton>
                            <ClientSideEvents FileUploadStart="OnFileUploadStart" FilesUploadComplete="OnFileUploadComplete"
                                TextChanged="OnTextChanged" />
                        </dx:ASPxUploadControl>
                        <dx:ASPxButton ID="btnAddFile" runat="server" Text="Add File" Font-Names="Segoe UI"
                            Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False" ClientInstanceName="btnAddFile"
                            Theme="Default">
                            <ClientSideEvents Click="AddFile" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                        <dx:ASPxLabel ID="ASPxLabel15" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="*Available Document format : pdf " Font-Italic="True">
                        </dx:ASPxLabel>
                        <br>
                        <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Maximum Size : Total 2 MB" Font-Italic="True">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="7">
                        <table>
                            <tr class="rowheight">
                                <td class="colwidthbutton">
                                    <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                                        ClientInstanceName="btnBack" Theme="Default">
                                        <Paddings Padding="2px" />
                                        <ClientSideEvents Click="Back" />
                                    </dx:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    <dx:ASPxButton ID="btnDraft" runat="server" Text="Draft" Font-Names="Segoe UI" Font-Size="9pt"
                                        Width="80px" Height="25px" AutoPostBack="False" ClientInstanceName="btnDraft"
                                        Theme="Default">
                                        <ClientSideEvents Click="Draft" />
                                        <Paddings Padding="2px" />
                                    </dx:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" UseSubmitBehavior="false"
                                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                        ClientInstanceName="btnSubmit" Theme="Default">
                                        <ClientSideEvents Click="Submit" />
                                        <Paddings Padding="2px" />
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
                <tr>
                <td></td>
                <td></td>
                 <td>
                     <dx:ASPxPopupControl ID="popupControl" runat="server" ClientInstanceName="clientPopupControl"
                         CloseAction="CloseButton" Height="200px" Modal="True" Width="850px" PopupHorizontalAlign="WindowCenter"
                         PopupVerticalAlign="WindowCenter">
                         <ContentCollection>
                             <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                             </dx:PopupControlContentControl>
                         </ContentCollection>
                     </dx:ASPxPopupControl>
                     <dx:ASPxPopupControl ID="popup" ClientInstanceName="popup"  runat="server" AllowDragging="true" CssClass="center-screen"
                        PopupHorizontalAlign="WindowCenter" Border-BorderStyle="None" HeaderStyle-BackColor="Transparent"  PopupVerticalAlign="WindowCenter" HeaderText="" HeaderStyle-Border-BorderStyle="None" BackColor="White" ShowHeader="false"> 
                           <ClientSideEvents Init="resizePopupFitToImage" />
                          
                        <ContentCollection>
                            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server"  >      
                                                  
                                <dx:ASPxCallbackPanel ID="callbackPanel" ClientInstanceName="callbackPanel" runat="server" 
                                     OnCallback="callbackPanel_Callback" RenderMode="Table"  >
                                    <PanelCollection >
                                        <dx:PanelContent ID="PanelContent1" runat="server"  >
                                            <table class="Image" style="width: 100%; border: 1px solid black;  border-style:none; " >
                                                <tr >
                                                    <td style="border-style:none">
                                                        <dx:ASPxBinaryImage  ID="edBinaryImage"  runat="server"   AlternateText=".... NO IMAGE ...." EmptyImage-Url="~/Styles/images/No-image-available.png" EmptyImage-Width="200px" EmptyImage-Height="200px" ImageAlign="Middle" Border-BorderStyle="None"   CssClass="Image" >
                                                            <ClientSideEvents Init="resizeToMax" />
                                                        </dx:ASPxBinaryImage>
                                                    </td>
                                                   <%-- popup : Width="500px" Height="450px"--%>
                                                   <%--binaryimage Width="490px" Height="440px"--%>
                                                   <%--callbackpanel Width="210px" Height="210px"--%>
                                                </tr>
                                            </table>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxCallbackPanel>
                                
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                        <ClientSideEvents Shown="popup_Shown" />
                    </dx:ASPxPopupControl>
                </td>
            </tr>
             <tr>
                <td colspan="9"></td>
            </tr>
            </table>
        </div>
    </div>
</asp:Content>
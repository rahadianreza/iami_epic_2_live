﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddMasterExchangeRateSupplier.aspx.vb" Inherits="IAMI_EPIC2.AddMasterExchangeRateSupplier" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxImageZoom" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" >
        function Message(paramType, paramMessage) {
            if (paramType == 0) { //Info
                toastr.info(paramMessage, 'Info');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 1) { //Success
                toastr.success(paramMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 2) { //Warning
                toastr.warning(paramMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (paramType == 3) { //Error
                toastr.error(paramMessage, 'Error');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function formatDateISO(date) {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        function cbAction_CallbackComplete(s, e) {
            var result = e.result;
            if (result == 'insert') {
                Message(s.cp_type, s.cp_message);
                if (s.cp_type == 1) {
                    cboSupplierCode.SetValue('');
                    txtSupplierDesc.SetText('');
                    cboCurrencyCode.SetValue('');
                    txtCurrencyDesc.SetText('');
                    dtExchangeRateDate.SetValue(null);
                    txtBuyingRate.SetValue(0);
                    txtMiddleRate.SetValue(0);
                    txtSellingRate.SetValue(0);
                    txtRemarks.SetText('');

                    cboSupplierCode.Focus();
                }
            } else if (result == 'update') {
                Message(s.cp_type, s.cp_message);
                if (s.cp_type == 1) {
                    millisecondsToWait = 2000;
                    setTimeout(function () {
                        var pathArray = window.location.pathname.split('/');
                        var url = window.location.origin + '/' + pathArray[1] + '/MasterExchangeRateSupplier.aspx'
                        if (pathArray[1] == "AddMasterExchangeRateSupplier.aspx") {
                            window.location.href = window.location.origin + '/MasterExchangeRateSupplier.aspx';
                        }
                        else {
                            window.location.href = url;
                        }
                    }, millisecondsToWait);
                }
            }
        }

        function btnClear_Click(s, e) {
            cboSupplierCode.SetValue('');
            txtSupplierDesc.SetText('');
            cboCurrencyCode.SetValue('');
            txtCurrencyDesc.SetText('');
            dtExchangeRateDate.SetValue(null);
            txtBuyingRate.SetValue(0);
            txtMiddleRate.SetValue(0);
            txtSellingRate.SetValue(0);
            txtRemarks.SetText('');

            cboSupplierCode.Focus();
        }

        function btnSubmit_Click(s, e) {
            var SupplierCode;
            var CurrencyCode;
            var ExchangeRateDate;
            var BuyingRate;
            var MiddleRate;
            var SellingRate;
            var Remarks;

            if (cboSupplierCode.GetEnabled() == true) {
                if (cboSupplierCode.GetText() == '') {
                    Message(2, 'Please select Supplier Code!');
                    cboSupplierCode.Focus();
                    e.processOnServer = false;
                    return;
                } else if (cboCurrencyCode.GetText() == '') {
                    Message(2, 'Please select Currency Code!');
                    cboCurrencyCode.Focus();
                    e.processOnServer = false;
                    return;
                } else if (dtExchangeRateDate.GetDate() == null) {
                    Message(2, 'Please select Exchange Rate Date!');
                    dtExchangeRateDate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtBuyingRate.GetValue() == null) {
                    Message(2, 'Please input Buying Rate!');
                    txtBuyingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtBuyingRate.GetValue() <= 0) {
                    Message(2, 'Please input Buying Rate!');
                    txtBuyingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtMiddleRate.GetValue() == null) {
                    Message(2, 'Please input Middle Rate!');
                    txtMiddleRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtMiddleRate.GetValue() <= 0) {
                    Message(2, 'Please input Middle Rate!');
                    txtMiddleRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtSellingRate.GetValue() == null) {
                    Message(2, 'Please input Selling Rate!');
                    txtSellingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtSellingRate.GetValue() <= 0) {
                    Message(2, 'Please input Selling Rate!');
                    txtSellingRate.Focus();
                    e.processOnServer = false;
                    return;
                }

                SupplierCode = cboSupplierCode.GetValue();
                CurrencyCode = cboCurrencyCode.GetValue();
                ExchangeRateDate = formatDateISO(dtExchangeRateDate.GetDate());
                BuyingRate = txtBuyingRate.GetValue();
                MiddleRate = txtMiddleRate.GetValue();
                SellingRate = txtSellingRate.GetValue();
                Remarks = txtRemarks.GetText();

                cbAction.PerformCallback('insert|' + SupplierCode + '|' + CurrencyCode + '|' + ExchangeRateDate + '|' + BuyingRate + '|' + MiddleRate + '|' + SellingRate + '|' + Remarks);
            } else {
                if (txtBuyingRate.GetValue() == null) {
                    Message(2, 'Please input Buying Rate!');
                    txtBuyingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtBuyingRate.GetValue() <= 0) {
                    Message(2, 'Please input Buying Rate!');
                    txtBuyingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtMiddleRate.GetValue() == null) {
                    Message(2, 'Please input Middle Rate!');
                    txtMiddleRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtMiddleRate.GetValue() <= 0) {
                    Message(2, 'Please input Middle Rate!');
                    txtMiddleRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtSellingRate.GetValue() == null) {
                    Message(2, 'Please input Selling Rate!');
                    txtSellingRate.Focus();
                    e.processOnServer = false;
                    return;
                } else if (txtSellingRate.GetValue() <= 0) {
                    Message(2, 'Please input Selling Rate!');
                    txtSellingRate.Focus();
                    e.processOnServer = false;
                    return;
                }

                SupplierCode = cboSupplierCode.GetValue();
                CurrencyCode = cboCurrencyCode.GetValue();
                ExchangeRateDate = formatDateISO(dtExchangeRateDate.GetDate());
                BuyingRate = txtBuyingRate.GetValue();
                MiddleRate = txtMiddleRate.GetValue();
                SellingRate = txtSellingRate.GetValue();
                Remarks = txtRemarks.GetText();

                cbAction.PerformCallback('update|' + SupplierCode + '|' + CurrencyCode + '|' + ExchangeRateDate + '|' + BuyingRate + '|' + MiddleRate + '|' + SellingRate + '|' + Remarks);
            }
        }

        function cboSupplierCode_Init(s, e) {
            var action;
            var SupplierCode;
            action = getURLParameters("action");
            if (action == 'new') {
                cboSupplierCode.SetText('');
                txtSupplierDesc.SetText('');
                cboSupplierCode.SetEnabled(true);
                cboSupplierCode.Focus();
            } else if (action == 'edit') {
                SupplierCode = getURLParameters("Suppliercode");
                cboSupplierCode.SetText(SupplierCode);
                txtSupplierDesc.SetText(cboSupplierCode.GetSelectedItem().GetColumnText('Description'));
                cboSupplierCode.SetEnabled(false);
            }
        }

        function cboCurrencyCode_Init(s, e) {
            var action;
            var currencyCode;
            action = getURLParameters("action");
            if (action == 'new') {
                cboCurrencyCode.SetText('');
                txtCurrencyDesc.SetText('');
                cboCurrencyCode.SetEnabled(true);
            } else if (action == 'edit') {
                currencyCode = getURLParameters("currencycode");
                cboCurrencyCode.SetText(currencyCode);
                txtCurrencyDesc.SetText(cboCurrencyCode.GetSelectedItem().GetColumnText('Description'));
                cboCurrencyCode.SetEnabled(false);
            }
        }

        function dtExchangeRateDate_Init(s, e) {
            var action;
            var date;
            action = getURLParameters("action");

            if (action == 'new') {
                dtExchangeRateDate.SetDate(null);
                dtExchangeRateDate.SetEnabled(true);
            } else if (action == 'edit') {
                //alert(getURLParameters("date"));
                date = new Date(getURLParameters("date"));
                date.valueOf(getURLParameters("date"));
                dtExchangeRateDate.SetDate(date);
                // dtExchangeRateDate.SetDate(getURLParameters("date"));
                dtExchangeRateDate.SetEnabled(false);
                txtBuyingRate.Focus();
            }
        }

        function cboSupplierCode_SelectedIndexChanged(s, e) {
            var desc = s.GetSelectedItem().GetColumnText('Description');
            txtSupplierDesc.SetText(desc);
        }

        function cboSupplierCode_LostFocus(s, e) {
            var id = s.GetValue();
            if (id == null) {
                txtSupplierDesc.SetText('');
            }
        }

        function cboCurrencyCode_SelectedIndexChanged(s, e) {
            var desc = s.GetSelectedItem().GetColumnText('Description');
            txtCurrencyDesc.SetText(desc);
        }

        function cboCurrencyCode_LostFocus(s, e) {
            var id = s.GetValue();
            if (id == null) {
                txtCurrencyDesc.SetText('');
            }
        }

        function txtCurrencyDesc_Init(s, e) {
            txtCurrencyDesc.SetEnabled(false);
        }

        function txtSupplierDesc_Init(s, e) {
            txtSupplierDesc.SetEnabled(false);
        }

        function getURLParameters(paramName) {
            var sURL = window.document.URL.toString();
            if (sURL.indexOf("?") > 0) {
                var arrParams = sURL.split("?");
                var arrURLParams = arrParams[1].split("&");
                var arrParamNames = new Array(arrURLParams.length);
                var arrParamValues = new Array(arrURLParams.length);

                var i = 0;
                for (i = 0; i < arrURLParams.length; i++) {
                    var sParam = arrURLParams[i].split("=");
                    arrParamNames[i] = sParam[0];
                    if (sParam[1] != "")
                        arrParamValues[i] = unescape(sParam[1]);
                    else
                        arrParamValues[i] = "No Value";
                }

                for (i = 0; i < arrURLParams.length; i++) {
                    if (arrParamNames[i] == paramName) {
                        //alert("Parameter:" + arrParamValues[i]);
                        return arrParamValues[i];
                    }
                }
                return "No Parameters Found";
            }
        }
    </script>
    <style type="text/css">        
        .table-col-space01
        {
            width: 100px;                     
        }
        .table-col-control01
        {
            width: 150px;        
        }   
        .table-col-control02
        {
            width: 100px;
        }
        .table-col-control03
        {
            width: 400px;
        }      
        .table-row-height
        {
            height: 35px      
        }            
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<table style="width: 100%;">
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Supplier Code">
            </dx:ASPxLabel>
        </td>
        <td class="table-col-control02"> 
            <dx:ASPxComboBox ID="cboSupplierCode" runat="server" ClientInstanceName="cboSupplierCode"
                Width="150px" Font-Names="Segoe UI" DataSourceID="dsSupplier" TextField="Description"
                ValueField="Code" TextFormatString="{0}" Font-Size="9pt" 
                Theme="Office2010Black" DropDownStyle="DropDown" 
                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                Height="25px">       
                <ClientSideEvents SelectedIndexChanged="cboSupplierCode_SelectedIndexChanged" LostFocus="cboSupplierCode_LostFocus" Init="cboSupplierCode_Init" />              
                <Columns>            
                    <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                    <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="350px" />
                </Columns>
                <ItemStyle Height="10px" Paddings-Padding="4px" >
                    <Paddings Padding="4px"></Paddings>
                </ItemStyle>
                <ButtonStyle Width="5px" Paddings-Padding="4px" >
                    <Paddings Padding="4px"></Paddings>
                </ButtonStyle>
            </dx:ASPxComboBox>
        </td>
        <td class="table-col-control03">
            <dx:ASPxTextBox ID="txtSupplierDesc" runat="server" Font-Names="Segoe UI" style="margin-left:10px" ClientInstanceName="txtSupplierDesc"
                Font-Size="9pt" Height="25px" Theme="Office2010Black" Width="340px">
            <ClientSideEvents Init="txtSupplierDesc_Init" />
            </dx:ASPxTextBox>
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel16" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Currency Code">
            </dx:ASPxLabel>
        </td>
        <td class="table-col-control02"> 
            <dx:ASPxComboBox ID="cboCurrencyCode" runat="server"
                ClientInstanceName="cboCurrencyCode" Width="150px" Font-Names="Segoe UI" 
                DataSourceID="dsCurrency" TextField="Description" ValueField="Code" 
                TextFormatString="{0}" Font-Size="9pt" 
                Theme="Office2010Black" DropDownStyle="DropDown" 
                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                Height="25px">  
            <ClientSideEvents SelectedIndexChanged="cboCurrencyCode_SelectedIndexChanged" LostFocus="cboCurrencyCode_LostFocus" Init="cboCurrencyCode_Init" />  
            <Columns>            
                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="100px" />
            </Columns>
            <ItemStyle Height="10px" Paddings-Padding="4px" >
                <Paddings Padding="4px"></Paddings>
            </ItemStyle>
            <ButtonStyle Width="5px" Paddings-Padding="4px" >
                <Paddings Padding="4px"></Paddings>
            </ButtonStyle>
            </dx:ASPxComboBox>                                                
        </td>
        <td class="table-col-control03">
            <dx:ASPxTextBox ID="txtCurrencyDesc" runat="server" Font-Names="Segoe UI" style="margin-left:10px" ClientInstanceName="txtCurrencyDesc"
                Font-Size="9pt" Height="25px" Theme="Office2010Black" Width="340px">
            <ClientSideEvents Init="txtCurrencyDesc_Init" />
            </dx:ASPxTextBox>
        </td>
        <td>
            <dx:ASPxCallback ID="cbAction" runat="server" ClientInstanceName="cbAction">
                <ClientSideEvents CallbackComplete="cbAction_CallbackComplete" />
            </dx:ASPxCallback>
        </td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Exchange Rate Date">
            </dx:ASPxLabel>
        </td>
        <td colspan="2"> 
            <dx:ASPxDateEdit ID="dtExchangeRateDate" ClientInstanceName="dtExchangeRateDate" runat="server" Font-Names="Segoe UI" HorizontalAlign="Right" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy"
                Font-Size="9pt" Height="25px" Theme="Office2010Black" Width="150px">
            <ClientSideEvents Init="dtExchangeRateDate_Init" />
            </dx:ASPxDateEdit>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Buying Rate">
            </dx:ASPxLabel>
        </td>
        <td colspan="2"> 
            <dx:ASPxTextBox ID="txtBuyingRate" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="150px" Height="25px" ClientInstanceName="txtBuyingRate" MaxLength="11" HorizontalAlign="Right">
                       <MaskSettings Mask="<0..100000000g>.<00..99>" />
             </dx:ASPxTextBox>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01"></td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Middle Rate">
            </dx:ASPxLabel>
        </td>
        <td colspan="2" class="table-row-height">                             
            <dx:ASPxTextBox ID="txtMiddleRate" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="150px" Height="25px" ClientInstanceName="txtMiddleRate" MaxLength="11" HorizontalAlign="Right">
                       <MaskSettings Mask="<0..100000000g>.<00..99>" />
             </dx:ASPxTextBox>
        </td>
        <td class="table-row-height"></td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Selling Rate">
            </dx:ASPxLabel>
        </td>
        <td colspan="2"> 
           
             <dx:ASPxTextBox ID="txtSellingRate" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="150px" Height="25px" ClientInstanceName="txtSellingRate" MaxLength="11" HorizontalAlign="Right">
                       <MaskSettings Mask="<0..100000000g>.<00..99>" />
             </dx:ASPxTextBox>
        </td>
        <td>
            <asp:SqlDataSource ID="dsSupplier" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"             
                SelectCommand="SELECT Supplier_Code AS Code, Supplier_Name AS Description FROM dbo.Mst_Supplier"></asp:SqlDataSource>
        </td>
    </tr>
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">
            <dx:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" 
                Font-Size="9pt" Text="Remarks">
            </dx:ASPxLabel>
        </td>
        <td colspan="2"> 
          
         <dx:ASPxMemo ID="txtRemarks" runat="server" Height="50px" Width="100%" ClientInstanceName="txtRemarks" MaxLength="50">
             </dx:ASPxMemo>     
        </td>
        <td><asp:SqlDataSource ID="dsCurrency" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"             
                SelectCommand="select rtrim(Par_Code) Code, rtrim(Par_Description) Description from Mst_Parameter where Par_Group = 'Currency'"></asp:SqlDataSource>
        </td>
    </tr>
   <td class="table-col-space01">&nbsp;</td>
    
    <tr class="table-row-height">
        <td class="table-col-space01">&nbsp;</td>
        <td class="table-col-control01">&nbsp;</td>
        <td colspan="2"> 
            <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="false"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnBack" Theme="Default" >                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>
            <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="false" style="margin-left:10px"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnClear" Theme="Default" >                        
                <ClientSideEvents Click="btnClear_Click"/>
                <Paddings Padding="2px" />
            </dx:ASPxButton>
            <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="false" style="margin-left:10px"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnSubmit" Theme="Default" >                        
                <ClientSideEvents Click="btnSubmit_Click"  />
                <Paddings Padding="2px" />
            </dx:ASPxButton>                    
        </td>
        <td>&nbsp;</td>        
    </tr>
</table>
</asp:Content>

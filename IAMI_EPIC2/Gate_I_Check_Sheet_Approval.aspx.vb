﻿Imports DevExpress.Web.ASPxGridView
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Data.SqlClient

Public Class Gate_I_Check_Sheet_Approval
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim py As String = ""
    Dim grpCommodity As String = ""
    Dim byr As String = ""
#End Region

#Region "Initilization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("J040")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        py = Request.QueryString("projecttype")
        prj = Request.QueryString("projectid")
        grp = Request.QueryString("groupid")
        comm = Request.QueryString("commodity")
        grpCommodity = Request.QueryString("groupcommodity")
        byr = Request.QueryString("buyer")

        btnShowDetail()
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub up_Excel()
        up_GridLoad("1")
        btnShowDetail()

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ItemListSourcing" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else
            up_GridLoad("1")
        End If
    End Sub

#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsGateICheckSheetDB.getHeaderGrid(prj, grp, comm, byr, pUser, grpCommodity, statusAdmin)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()

            Dim ds As New DataSet
            ds = clsGateICheckSheetDB.GetRateJPY_NewGuidePrice(prj)

            If ds.Tables(0).Rows.Count > 0 Then
                Grid.Columns("New_Guide_Price_Jpn").Caption = ds.Tables(0).Rows(0)("Rate_YEN_IDR").ToString()

                'Dim col As GridViewDataTextColumn = New GridViewDataTextColumn()
                'If col.FieldName = "New_Guide_Price_Jpn" Then
                '    col.Caption = ds.Tables(0).Rows(0)("Rate_YEN_IDR").ToString()
                'End If
            Else
                Grid.Columns("New_Guide_Price_Jpn").Caption = "New Guide Price JPN=Rp "
            End If
        End If
    End Sub

    Protected Sub Grid_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        'nothing
    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
    End Sub

    Public Function checkingCbo() As Boolean
      If String.IsNullOrEmpty(prj) = True Then
            cbSave.JSProperties("cpMessage") = "Project Name cannot be empty!"
            Return False
        ElseIf String.IsNullOrEmpty(grp) = True Then
            cbSave.JSProperties("cpMessage") = "Group ID cannot be empty!"
            Return False
        ElseIf String.IsNullOrEmpty(comm) = True Then
            Return False
        Else
            Return True
        End If
    End Function

    Protected Sub btnComplete_Click(sender As Object, e As EventArgs) Handles btnComplete.Click
        Dim pErr As String = ""
        If checkingCbo() = True Then
            clsGateICheckSheetDB.InsertGateSheetApproval(prj, grp, comm, grpCommodity, pUser, statusAdmin, pErr)
            cbSave.JSProperties("cpMessage") = "Approval Completed!"
            btnComplete.ClientEnabled = False
        End If
    End Sub
#End Region

#Region "Event Control"

   
#End Region

    Private Sub cbComplete_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbComplete.Callback
        If clsGateICheckSheetDB.CheckGateStatus(prj, grp, comm, pUser, grpCommodity) = "Accepted" Then
            cbComplete.JSProperties("cp_Complete") = "true"
        Else
            cbComplete.JSProperties("cp_Complete") = "false"
        End If
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.VisibleIndex < 0) Then
            Exit Sub
        End If
        If clsGateICheckSheetDB.CheckGateStatus(prj, grp, comm, pUser, grpCommodity) <> "Accepted" Then
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                e.Visible = False
            End If
        End If
    End Sub

#Region "Detail"
    Private Sub specInformation()
        getExistsData()
        getGridDepartment(GridCostControl, "CC")
    End Sub

    Private Sub bidderList()
        up_GridLoadBidderList("")
    End Sub

    Private Sub SourcingSchedule()
        up_GridLoadSourcingSchedule("")
    End Sub

    Private Sub getExistsData()
        Dim ds As DataSet
        Dim clsgatespec As New clsGateSpecInformation
        ds = clsGateICheckSheetDB.GetDateSpecInformation(prj, grp, comm, byr, pUser, grpCommodity, statusAdmin, clsgatespec)

        'txtTitle.Text = clsgatespec.Title
        cboBaseDrawing.Value = clsgatespec.BaseDrawing
        cboSOR.Value = clsgatespec.SOR
        txtMaterial.Text = clsgatespec.Material
        txtProcess.Text = clsgatespec.Process
        cboDevSchedule.Value = clsgatespec.CheckDevelopmentSchedule
        txtAdditionalInformation.Text = clsgatespec.AdditionalInformation
        KHSAttendance.Value = CDate(IIf(String.IsNullOrEmpty(clsgatespec.KSHAttendance), "1990-01-01", clsgatespec.KSHAttendance))
    End Sub

    Private Sub getGridDepartment(_gridView As ASPxGridView, _department As String)
        Dim ds As DataSet = clsGateICheckSheetDB.GetGridDepartment(prj, grp, comm, byr, _department, pUser, grpCommodity, statusAdmin)
        _gridView.DataSource = ds
        _gridView.DataBind()
    End Sub

    Private Sub up_GridLoadBidderList(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsGateICheckSheetDB.GetGridBidderList(prj, grp, comm, byr, pUser, grpCommodity, statusAdmin)
        If ErrMsg = "" Then
            GridBidderList.DataSource = Ses
            GridBidderList.DataBind()
        End If
    End Sub

    Private Sub up_GridLoadSourcingSchedule(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsGateICheckSheetDB.GetGridSourcingSchedule(prj, grp, comm, byr, pUser, grpCommodity, statusAdmin)
        If ErrMsg = "" Then
            GridSourcing.DataSource = Ses
            GridSourcing.DataBind()
        End If
    End Sub

    Protected Sub btnShowDetail()
        up_GridLoad("1")
        specInformation()
        bidderList()
        SourcingSchedule()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Gate_I_List_Approval.aspx?projecttype=" + py + "&projectid=" + prj + "&groupid=" + grp + "&commodity=" + comm + "&groupcomodity=" + grpCommodity)
    End Sub

    Protected Sub GridSourcing_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridSourcing.HtmlDataCellPrepared
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        e.Cell.BackColor = Color.LemonChiffon
        With e.DataColumn
            'If e.GetValue("Code_Activity").ToString() = "CA001" Or e.GetValue("Code_Activity").ToString() = "CA002" Then
            '    e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
            '    e.Cell.Attributes.Add("readonly", "true")
            '    e.Cell.BackColor = Color.LemonChiffon

            'End If

            If .FieldName = "Date_Actual" Then
                If Not IsDBNull(e.GetValue("Due_Date_Plan")) And Not IsDBNull(e.GetValue("Date_Actual")) Then
                    If DateDiff(DateInterval.Day, CDate(e.GetValue("Due_Date_Plan")), CDate(e.GetValue("Date_Actual"))) > 0 Then
                        e.Cell.BackColor = Color.Red
                    End If
                End If
            End If

        End With
    End Sub

    Protected Sub GridCostControl_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridCostControl.HtmlDataCellPrepared
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Protected Sub GridBidderList_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridBidderList.HtmlDataCellPrepared
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Protected Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If clsGateICheckSheetDB.CheckGateStatus(prj, grp, comm, pUser, grpCommodity) <> "Accepted" Then
            e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
            e.Cell.BackColor = Color.LemonChiffon
        End If
    End Sub
#End Region
End Class
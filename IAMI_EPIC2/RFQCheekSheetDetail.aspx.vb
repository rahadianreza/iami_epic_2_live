﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Drawing
Imports OfficeOpenXml
'Imports System.Drawing

Public Class RFQCheekSheetDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fRefresh As Boolean = False
    Dim PrjType As String = ""
    Dim PrjID As String = ""
    Dim Group As String = ""
    Dim commodity As String = ""
    Dim Groupcommodity As String = ""
    Dim Vendor As String = ""
    Dim Pos As String = ""
    Dim datefrom As String = ""
    Dim dateto As String = ""
    Dim projtype As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("J050")
        'Master.SiteTitle = sGlobal.menuName
        Master.SiteTitle = "REQUEST FOR QUOTATION CHECK SHEET DETAIL"
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        Dim dt As New DataTable

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            Pos = Request.QueryString("Pos")
            If IIf(Pos = Nothing, "0", Pos) = 1 Then
                'btnSubmit.ClientVisible = False
                If Request.QueryString("NoRFQ") <> Nothing Then
                    dateRfq.Text = Request.QueryString("DateRFQ")
                Else
                    dateRfq.Text = ""
                End If
            Else
                If Request.QueryString("NoRFQ") <> Nothing Then
                    dateRfq.Text = Format(CDate(Request.QueryString("DateRFQ")), "dd MMM yyyy")
                Else
                    dateRfq.Text = ""
                End If
            End If
            PrjID = Request.QueryString("projectid")
            Group = Request.QueryString("groupid")
            commodity = Request.QueryString("commodity")
            Groupcommodity = Request.QueryString("groupcommodity")
            Vendor = Request.QueryString("VendorCode")
            NoRfq.Text = Request.QueryString("NoRFQ")


            dt = ClsRFQCheekSheetDB.getDetailHeader(IIf(PrjID = Nothing, "", PrjID), IIf(Group = Nothing, "", Group), IIf(commodity = Nothing, "", commodity), IIf(Groupcommodity = Nothing, "", Groupcommodity), IIf(Vendor = Nothing, "", Vendor), "")
            If dt.Rows.Count > 0 Then
                txtTo.Text = Trim(dt.Rows(0).Item("To_RFQ") & "")
                txtFax.Text = Trim(dt.Rows(0).Item("Phone_RFQ") & "")
                txtPhone.Text = Trim(dt.Rows(0).Item("Fax_RFQ") & "")
                txtDeptHead.Text = Trim(dt.Rows(0).Item("Att_Dept_Head_Name") & "")
                txtDeptHeadEmail.Text = Trim(dt.Rows(0).Item("Email_Dept_Head") & "")
                txtCostPIC.Text = Trim(dt.Rows(0).Item("Att_Cost_PIC_Name") & "")
                txtCostPICEmail.Text = Trim(dt.Rows(0).Item("Email_Cost_PIC") & "")
                txtProjPIC.Text = Trim(dt.Rows(0).Item("Att_Project_PIC_Name") & "")
                txtProjPICEmail.Text = Trim(dt.Rows(0).Item("Email_Project_PIC") & "")
                txtVendorName.Text = Trim(dt.Rows(0).Item("Supplier_Name") & "")
                txtProjName.Text = Trim(dt.Rows(0).Item("Project_Name") & "")
                txtOTS.Text = Format(dt.Rows(0).Item("Shec_OTS_Sample"), "dd-MM-yyyy")
                txtSOP.Text = Format(dt.Rows(0).Item("Shec_SOP_Timing"), "dd-MM-yyyy")
                txtExchangeRate.Text = "USD/Rp = " & Trim(dt.Rows(0).Item("Rate_USD_IDR") & "") & ";  Yen/Rp = " & Trim(dt.Rows(0).Item("Rate_YEN_IDR") & "") & ";  Bath/Rp = " & Trim(dt.Rows(0).Item("Rate_BATH_IDR") & "")
                TxtDueDate.Text = Format(DateAdd(DateInterval.Day, 7, dt.Rows(0).Item("Date_RFQ")), "dd MMM yyyy")
            End If
            up_GridLoad(PrjID, Group, commodity, Groupcommodity, 0)

            If ClsRFQCheekSheetDB.isSubmitted(PrjID, Group, commodity, Groupcommodity, Vendor, NoRfq.Text) = True Then
                btnSubmit.ClientEnabled = False
            Else
                btnSubmit.ClientEnabled = True
            End If
        End If
    End Sub
#End Region

#Region "Control Event"

    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        PrjID = Request.QueryString("projectid")
        Group = Request.QueryString("groupid")
        commodity = Request.QueryString("commodity")
        Groupcommodity = Request.QueryString("groupcommodity")
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad(PrjID, Group, commodity, Groupcommodity, 0)
        End If

    End Sub


    'Private Sub up_Excel()
    '    PrjID = Request.QueryString("projectid")
    '    Group = Request.QueryString("groupid")
    '    commodity = Request.QueryString("commodity")
    '    Groupcommodity = Request.QueryString("groupcommodity")
    '    up_GridLoad(PrjID, Group, commodity, Groupcommodity, 1)

    '    Dim ps As New PrintingSystem()

    '    Dim link1 As New PrintableComponentLink(ps)
    '    link1.Component = GridExporter

    '    Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
    '    compositeLink.Links.AddRange(New Object() {link1})

    '    compositeLink.CreateDocument()
    '    Using stream As New MemoryStream()
    '        compositeLink.PrintingSystem.ExportToXlsx(stream)
    '        Response.Clear()
    '        Response.Buffer = False
    '        Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    '        Response.AppendHeader("Content-Disposition", "attachment; filename=RfQCheckSheet" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
    '        Response.BinaryWrite(stream.ToArray())
    '        Response.End()
    '    End Using
    '    ps.Dispose()
    'End Sub
    Private Sub up_Excel()
        Dim ds As New DataSet
        Dim dsvar As New DataSet
        Dim Checkitem As String = ""
        Dim pint As Integer = 0
        PrjID = Request.QueryString("projectid")
        Group = Request.QueryString("groupid")
        commodity = Request.QueryString("commodity")
        Groupcommodity = Request.QueryString("groupcommodity")
        With grid
            If .VisibleRowCount = 0 Then
                Return
            End If
        End With
        Using Pck As New ExcelPackage
            Dim ws As ExcelWorksheet = Pck.Workbook.Worksheets.Add("RFQCheekSheet")
            With ws
                dsvar = clsPRJListDB.getVariant(PrjID)
                ds = ClsRFQCheekSheetDB.getDetaillistds(PrjID, Group, commodity, Groupcommodity, "")
                Dim pTotalColVariant As Integer = ds.Tables(0).Columns.Count - 9

                .Cells(1, 1, 1, 1).Value = "REQUEST FOR QUOTATION"
                .Cells(1, 1, 1, 1).Style.Font.Bold = True
                .Cells(1, 1, 1, 1).Style.Font.Size = 14
                .Cells(1, 1, 1, ds.Tables(0).Columns.Count).Merge = True
                .Cells(1, 1, 1, ds.Tables(0).Columns.Count).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center

                .Cells(3, 2, 3, 2).Value = "No"
                .Cells(5, 2, 5, 2).Value = "Date"
                .Cells(7, 2, 7, 2).Value = "To"
                .Cells(9, 2, 9, 2).Value = "Phone"
                .Cells(11, 2, 11, 2).Value = "Fax"
                .Cells(13, 2, 13, 2).Value = "Vendor Name"
                .Cells(15, 2, 15, 2).Value = "Project Name"

                .Cells(3, 4, 3, 4).Value = ": " & NoRfq.Text
                .Cells(5, 4, 5, 4).Value = ": " & dateRfq.Text
                .Cells(7, 4, 7, 4).Value = ": " & txtTo.Text
                .Cells(9, 4, 9, 4).Value = ": " & txtPhone.Text
                .Cells(11, 4, 11, 4).Value = ": " & txtFax.Text
                .Cells(13, 4, 13, 4).Value = ": " & txtVendorName.Text
                .Cells(15, 4, 15, 4).Value = ": " & txtProjName.Text

                .Cells(3, 8, 3, 8).Value = "Attn : Dept Head"
                .Cells(5, 8, 5, 8).Value = "Project PIC"
                .Cells(7, 8, 7, 8).Value = "Cost PIC"
                .Cells(9, 8, 9, 8).Value = "Estimated SOP"
                .Cells(11, 8, 11, 8).Value = "Quotation base on"
                .Cells(13, 8, 13, 8).Value = "Due date of this inquiry is"

                .Cells(3, 10, 3, 10).Value = ": " & txtDeptHead.Text
                .Cells(4, 10, 4, 10).Value = ": " & txtDeptHeadEmail.Text
                .Cells(5, 10, 5, 10).Value = ": " & txtProjPIC.Text
                .Cells(6, 10, 6, 10).Value = ": " & txtProjPICEmail.Text
                .Cells(7, 10, 7, 10).Value = ": " & txtCostPIC.Text
                .Cells(8, 10, 8, 10).Value = ": " & txtCostPICEmail.Text
                .Cells(9, 10, 9, 10).Value = "OTS Sample"
                .Cells(9, 11, 9, 11).Value = ": " & txtOTS.Text
                .Cells(10, 10, 10, 10).Value = "SOP Timing"
                .Cells(10, 11, 10, 11).Value = ": " & txtSOP.Text
                .Cells(11, 10, 11, 10).Value = "Exchange Rate"
                .Cells(11, 11, 11, 11).Value = ": " & txtExchangeRate.Text
                .Cells(12, 10, 12, 10).Value = "Include standard packaging"
                .Cells(12, 11, 12, 11).Value = ": " & txtInclude.Text
                .Cells(13, 10, 13, 10).Value = ": " & TxtDueDate.Text

                .Cells(18, 1, 18, 1).Value = "No"
                .Cells(18, 1, 19, 1).Merge = True

                .Cells(18, 2, 18, 2).Value = "Upc"
                .Cells(18, 2, 19, 2).Merge = True
                .Cells(18, 3, 18, 3).Value = "Fna"
                .Cells(18, 3, 19, 3).Merge = True
                .Cells(18, 4, 18, 4).Value = "Dwg No"
                .Cells(18, 4, 19, 4).Merge = True
                .Cells(18, 5, 18, 5).Value = "Part No"
                .Cells(18, 5, 19, 5).Merge = True
                .Cells(18, 6, 18, 6).Value = "Part Name"
                .Cells(18, 6, 19, 6).Merge = True
                .Cells(18, 7, 18, 7).Value = "Qty"
                .Cells(18, 7, 19, 7).Merge = True

                If pTotalColVariant > 0 Then
                    .Cells(18, 8, 18, 8).Value = "Variant"
                    .Cells(18, 8, 18, 8 + pTotalColVariant - 1).Merge = True
                    .Cells(18, 8, 18, 8 + pTotalColVariant - 1).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                    .Cells(19, 8, 19, 8 + pTotalColVariant - 1).Style.WrapText = True
                    For xyz = 1 To pTotalColVariant
                        .Cells(19, 7 + xyz, 19, 7 + xyz).Value = dsvar.Tables(0).Rows(xyz - 1)(0).ToString()
                    Next
                End If
                .Cells(18, 8 + pTotalColVariant, 18, 8 + pTotalColVariant).Value = "Volume/Years"
                .Cells(18, 8 + pTotalColVariant, 19, 8 + pTotalColVariant).Merge = True
                .Cells(18, 9 + pTotalColVariant, 18, 9 + pTotalColVariant).Value = "Remarks"
                .Cells(18, 9 + pTotalColVariant, 19, 9 + pTotalColVariant).Merge = True
                .Cells(18, 1, 19, 9 + pTotalColVariant).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                .Cells(18, 1, 19, 9 + pTotalColVariant).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center

                Dim iRow As Integer = 0
                Dim r As Integer = 20
                Dim pcol As Integer = 0
                Dim pcolm As Integer = 2
                Dim pVolumeYears As String = ""
                For iRow = 0 To ds.Tables(0).Rows.Count - 1
                    .Cells(r, 1, r, 1).Value = ds.Tables(0).Rows(iRow)("No").ToString
                    .Cells(r, 2, r, 2).Value = ds.Tables(0).Rows(iRow)("Upc").ToString
                    .Cells(r, 3, r, 3).Value = ds.Tables(0).Rows(iRow)("Fna").ToString
                    .Cells(r, 4, r, 4).Value = ds.Tables(0).Rows(iRow)("Dwg_No").ToString
                    .Cells(r, 5, r, 5).Value = ds.Tables(0).Rows(iRow)("Part_No").ToString
                    .Cells(r, 6, r, 6).Value = ds.Tables(0).Rows(iRow)("Part_Name").ToString
                    .Cells(r, 7, r, 7).Value = ds.Tables(0).Rows(iRow)("Qty").ToString
                    For yxz = 1 To pTotalColVariant
                        .Cells(r, 7 + yxz, r, 7 + yxz).Value = ds.Tables(0).Rows(iRow)("Variant" & yxz).ToString
                    Next
                    pVolumeYears = ds.Tables(0).Rows(iRow)("VolumeYears").ToString
                    pVolumeYears = CDbl(pVolumeYears).ToString("#,###,###")
                    .Cells(r, 8 + pTotalColVariant, r, 8 + pTotalColVariant).Value = pVolumeYears
                    .Cells(r, 8 + pTotalColVariant, r, 8 + pTotalColVariant).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Right
                    '.Cells(r, 8 + pTotalColVariant, r, 8 + pTotalColVariant).Style.Numberformat.Format = "#0.00"
                    .Cells(r, 9 + pTotalColVariant, r, 9 + pTotalColVariant).Value = ds.Tables(0).Rows(iRow)("Remarks").ToString
                    r = r + 1
                Next
                .Cells(18, 1, r - 1, 9 + pTotalColVariant).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                .Cells(18, 1, r - 1, 9 + pTotalColVariant).Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
                .Cells(18, 1, r - 1, 9 + pTotalColVariant).Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
                .Cells(18, 1, r - 1, 9 + pTotalColVariant).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin

                .Column(4).Width = 11
                .Column(5).Width = 13
                .Column(6).Width = 16
                If pTotalColVariant > 0 Then
                    For xyz = 1 To pTotalColVariant
                        .Column(7 + xyz).Width = 14
                    Next
                End If
                .Column(pTotalColVariant + 8).Width = 14
                .Column(pTotalColVariant + 9).Width = 14

                .View.ShowGridLines = False
            End With

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment; filename=RfQCheckSheet_" & Format(Date.Now, "yyyy-MM-dd") & ".xlsx")

            Dim stream As MemoryStream = New MemoryStream(Pck.GetAsByteArray())

            Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length)
            Response.Flush()
            Response.Close()
        End Using

    End Sub
    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    'Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
    '    pGrid.JSProperties("cp_message") = ErrMsg
    '    pGrid.JSProperties("cp_type") = msgType
    '    pGrid.JSProperties("cp_val") = pVal
    'End Sub
#End Region

#Region "Procedure"
    Public Sub addVariant()
        PrjID = Request.QueryString("projectid")
        Dim ds As DataSet = clsPRJListDB.getVariant(PrjID)
        Dim bandColumn As GridViewBandColumn = New GridViewBandColumn()
        bandColumn.Caption = "Variant"
        bandColumn.VisibleIndex = 7

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim col As GridViewDataTextColumn = New GridViewDataTextColumn()
            col.Caption = ds.Tables(0).Rows(i)(0).ToString()
            col.FieldName = "Variant" + CStr(i + 1)
            bandColumn.Columns.Add(col)

        Next

        If ds.Tables(0).Rows.Count > 0 Then
            Grid.Columns.Add(bandColumn)
        End If

        'Grid.Columns.Add(bandColumn)
    End Sub
    Private Sub up_GridLoad(ByVal pProjectID As String, ByVal pGroup As String, ByVal pComodity As String, ByVal pGroupComodity As String, ByVal pExcel As String)
        Dim ErrMsg As String = ""
        Dim ds As DataSet
        'Dim Ses As New List(Of ClsRFQCheekSheet)
        'Ses = ClsRFQCheekSheetDB.getDetaillist(IIf(pProjectID = Nothing, "", pProjectID), IIf(pGroup = Nothing, "", pGroup), IIf(pComodity = Nothing, "", pComodity), IIf(pGroupComodity = Nothing, "", pGroupComodity), ErrMsg)
        ds = ClsRFQCheekSheetDB.getDetaillistds(IIf(pProjectID = Nothing, "", pProjectID), IIf(pGroup = Nothing, "", pGroup), IIf(pComodity = Nothing, "", pComodity), IIf(pGroupComodity = Nothing, "", pGroupComodity), ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
            If pExcel = 0 Then
                addVariant()
            End If
        End If
    End Sub

#End Region

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Pos = Request.QueryString("Pos")
        If IIf(Pos = Nothing, "0", Pos) = 1 Then
            Session("PrjIDdetail") = Request.QueryString("projectid")
            Session("Groupdetail") = Request.QueryString("groupid")
            Session("commoditydetail") = Request.QueryString("commodity")
            Session("Groupcommoditydetail") = Request.QueryString("groupcommodity")
            Session("datefromdetail") = Request.QueryString("datefrom")
            Session("datetodetail") = Request.QueryString("dateto")
            Session("projtypedetail") = Request.QueryString("ProjType")

        Else
            Session("PrjIDdetailApproval") = Request.QueryString("projectid")
            Session("GroupdetailApproval") = Request.QueryString("groupid")
            Session("commoditydetailApproval") = Request.QueryString("commodity")
            Session("GroupcommoditydetailApproval") = Request.QueryString("groupcommodity")
            'Response.Redirect("RFQCheekSheetApproval.aspx")
        End If
        Response.Redirect("RFQCheekSheet.aspx")
    End Sub

    'Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
    '    Dim pErr As String = ""
    '    Dim Ses As New ClsRFQCheekSheet With {.Project_ID = Request.QueryString("projectid"),
    '                                         .Group = Request.QueryString("groupid"),
    '                                         .Comodity = Request.QueryString("commodity"),
    '                                         .Group_Comodity = Request.QueryString("groupcommodity"),
    '                                         .VendorCode = Request.QueryString("VendorCode"),
    '                                        .UserID = pUser}

    '    ClsRFQCheekSheetDB.UpdateApproval(Ses, pErr)

    '    If pErr <> "" Then
    '        cbMessage.JSProperties("cp_message") = pErr
    '    Else
    '        btnApprove.ClientEnabled = False
    '        cbMessage.JSProperties("cp_message") = "Approve data successfully!"
    '    End If

    'End Sub
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub cbMessage_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbMessage.Callback
        Dim pErr As String = ""
        Dim Ses As New ClsRFQCheekSheet With {.Project_ID = Request.QueryString("projectid"),
                                             .Group_ID = Request.QueryString("groupid"),
                                             .Comodity = Request.QueryString("commodity"),
                                             .Group_Comodity = Request.QueryString("groupcommodity"),
                                             .VendorCode = Request.QueryString("VendorCode"),
                                              .NoRFQ = Request.QueryString("NoRFQ"),
                                            .UserID = pUser}

        ClsRFQCheekSheetDB.SubmitRFQCheckSheet(Ses, pErr)

        If pErr <> "" Then
            cbMessage.JSProperties("cp_message") = pErr
        Else
            btnSubmit.ClientEnabled = False
            cbMessage.JSProperties("cp_message") = "Data Saved Successfully!"
        End If
    End Sub
End Class
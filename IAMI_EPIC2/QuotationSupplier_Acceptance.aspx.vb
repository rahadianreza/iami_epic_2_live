﻿Imports DevExpress.XtraPrinting
Imports DevExpress.Web.ASPxGridView
Imports System.IO
Imports DevExpress.Web.ASPxEditors

Public Class QuotationSupplier_Acceptance
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim pErr As String = ""

    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim py As String = ""
    Dim grpcomm As String = ""
    Dim pg As String = ""
    Dim supp As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("K020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        prj = Request.QueryString("projectid")
        grp = Request.QueryString("groupid")
        comm = Request.QueryString("commodity")
        pg = Request.QueryString("page")
        supp = Request.QueryString("supplier")
        grpcomm = Request.QueryString("groupcomodity")

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then

            If clsQuotationSupplierDB.checkingStatus_Header(prj, grp, comm, supp) = "0" And clsQuotationSupplierDB.checkingStatusComplete_Header(prj, grp, comm, supp) = "1" Then
                btnAcceptAll.ClientEnabled = "true"
                btnRejectAll.ClientEnabled = "true"
                lblStatus.Text = "Pending"
            Else
                btnAcceptAll.ClientEnabled = "false"
                btnRejectAll.ClientEnabled = "false"
                lblStatus.Text = "Accept"
            End If

            cboProject.Value = prj
            cboGroup.Value = grp
            cboCommodity.Value = comm
            cboGroupCommodity.Value = grpcomm
            cboSupplier.Value = supp


            cboProject.ClientEnabled = False
            cboGroup.ClientEnabled = False
            cboCommodity.ClientEnabled = False
            cboGroupCommodity.ClientEnabled = False
            cboSupplier.ClientEnabled = False
            cboProject.BackColor = System.Drawing.Color.LightGray
            cboGroup.BackColor = System.Drawing.Color.LightGray
            cboCommodity.BackColor = System.Drawing.Color.LightGray
            cboGroupCommodity.BackColor = System.Drawing.Color.LightGray
            cboSupplier.BackColor = System.Drawing.Color.LightGray
            cboProject.ForeColor = System.Drawing.Color.Black
            cboGroup.ForeColor = System.Drawing.Color.Black
            cboCommodity.ForeColor = System.Drawing.Color.Black
            cboGroupCommodity.ForeColor = System.Drawing.Color.Black
            cboSupplier.ForeColor = System.Drawing.Color.Black

            If Not String.IsNullOrEmpty(prj) Then
                Dim ds As DataSet = clsPRJListDB.getDetailGrid(prj, pUser, "")

                lblUSDRate.Text = Format(CDec(ds.Tables(0).Rows(0)("Rate_USD_IDR").ToString()), "#,##0.00")
                lblJPYRate.Text = Format(CDec(ds.Tables(0).Rows(0)("Rate_YEN_IDR").ToString()), "#,##0.00")
                lblTHBRate.Text = Format(CDec(ds.Tables(0).Rows(0)("Rate_BATH_IDR").ToString()), "#,##0.00")
            End If

            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboSupplier, cboProject.Value, cboGroup.Value, cboCommodity.Value, "S", statusAdmin, pUser)
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_Excel()
        up_GridLoad("1")

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)
        'link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ItemListSourcing" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub up_FillComboGroupCommodityPIC(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _projid As String, _grpid As String, _comm As String, _type As String, _
                           pAdminStatus As String, pUserID As String, _
                           Optional ByRef pGroup As String = "", _
                           Optional ByRef pParent As String = "", _
                           Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsGateICheckSheetDB.FillComboProject("5", "", _projid, _grpid, _comm, _type, pUserID, statusAdmin, pmsg)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsQuotationSupplierDB.getHeaderAcceptanceGrid(cboProject.Value, cboGroup.Value, cboCommodity.Value, pUser, cboGroupCommodity.Value, cboSupplier.Value, statusAdmin)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()

        End If
    End Sub
#End Region

#Region "Event Control"
    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.VisibleIndex < 0) Then
            Exit Sub
        End If
        'If clsQuotationSupplierDB.checkingStatus_Header(prj, grp, comm, pUser) = "1" And clsQuotationSupplierDB.checkingStatusComplete_Header(prj, grp, comm, pUser) <> "2" Then
        If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
            e.Visible = False
        End If

        'End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else

            up_GridLoad("1")
        End If
    End Sub

    Public Function checkingCbo() As Boolean
        If String.IsNullOrEmpty(cboProject.Value) = True Then
            cbSave.JSProperties("cpMessage") = "Project Name cannot be empty!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboSupplier, cboProject.Value, cboGroup.Value, cboCommodity.Value, "S", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return False
        ElseIf String.IsNullOrEmpty(cboGroup.Value) = True Then
            cbSave.JSProperties("cpMessage") = "Group ID cannot be empty!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboSupplier, cboProject.Value, cboGroup.Value, cboCommodity.Value, "S", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return False
        ElseIf String.IsNullOrEmpty(cboCommodity.Value) = True Then
            cbSave.JSProperties("cpMessage") = "Commodity cannot be empty!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboSupplier, cboProject.Value, cboGroup.Value, cboCommodity.Value, "S", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return False

        ElseIf String.IsNullOrEmpty(cboSupplier.Value) = True Then
            cbSave.JSProperties("cpMessage") = "Supplier cannot be empty!"
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboSupplier, cboProject.Value, cboGroup.Value, cboCommodity.Value, "S", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return False
        Else
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboSupplier, cboProject.Value, cboGroup.Value, cboCommodity.Value, "S", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            Return True
        End If
    End Function

    Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProject As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroup, pProject, "", "", "1", statusAdmin, pUser, "")
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboGroupCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        End If
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "Remarks" Then
            e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
        End If

    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        clsQuotationSupplierDB.InsertQuotationSupplier(cboProject.Value, cboGroup.Value, cboCommodity.Value, e.Keys("Part_No"), e.NewValues("OverHead"), _
                                                       e.NewValues("Profit"), e.NewValues("Transportation"), e.NewValues("SellingPrice"), e.NewValues("Currency"), e.NewValues("AdditionalCost"), e.NewValues("Material_Check"), e.NewValues("Purchased_Check"), e.NewValues("Manufacturing_Check"), pUser, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
        End If
    End Sub

    Protected Sub btnMaterialCost_Click(sender As Object, e As EventArgs) Handles btnMaterialCost.Click
        If checkingCbo() = True Then
            Dim projecttype As String = clsQuotationSupplierDB.getProjectType(cboProject.Value)
            'Response.Redirect("QuotationSupplier_MaterialCost.aspx?projectid=" + cboProject.Value + "&groupid=" + cboGroup.Value + "&commodity=" + cboCommodity.Value + "&groupcomodity=" + cboGroupCommodity.Value + "&projecttype=" + projecttype + "&page=acc&supplier=" + cboSupplier.Value)
            Response.Redirect("QuotationSupplier_MaterialCost.aspx?projectid=" + cboProject.Value + "&groupid=" + cboGroup.Value + "&commodity=" + cboCommodity.Value + "&projecttype=" + projecttype + "&supplier=" + cboSupplier.Value + "&groupcommodity=" + cboGroupCommodity.Value + "&pic=" + pUser + "&page=acc")
        End If
    End Sub

    Protected Sub btnPurchasedPartCost_Click(sender As Object, e As EventArgs) Handles btnPurchasedPartCost.Click
        If checkingCbo() = True Then
            Dim projecttype As String = clsQuotationSupplierDB.getProjectType(cboProject.Value)
            'Response.Redirect("QuotationSupplier_PurchasedCost.aspx?projectid=" + cboProject.Value + "&groupid=" + cboGroup.Value + "&commodity=" + cboCommodity.Value + "&groupcomodity=" + cboGroupCommodity.Value + +"&projecttype=" + projecttype + "&page=acc&supplier=" + cboSupplier.Value)
            Response.Redirect("QuotationSupplier_PurchasedCost.aspx?projectid=" + cboProject.Value + "&groupid=" + cboGroup.Value + "&commodity=" + cboCommodity.Value + "&projecttype=" + projecttype + "&supplier=" + cboSupplier.Value + "&groupcommodity=" + cboGroupCommodity.Value + "&pic=" + pUser + "&page=acc")
        End If
    End Sub

    Protected Sub btnManufacturingCost_Click(sender As Object, e As EventArgs) Handles btnManufacturingCost.Click
        If checkingCbo() = True Then
            Dim projecttype As String = clsQuotationSupplierDB.getProjectType(cboProject.Value)
            'Response.Redirect("QuotationSupplier_ManufacturingCost.aspx?projectid=" + cboProject.Value + "&groupid=" + cboGroup.Value + "&commodity=" + cboCommodity.Value + "&groupcomodity=" + cboGroupCommodity.Value + +"&projecttype=" + projecttype + "&page=acc&supplier=" + cboSupplier.Value)
            Response.Redirect("QuotationSupplier_ManufacturingCost.aspx?projectid=" + cboProject.Value + "&groupid=" + cboGroup.Value + "&commodity=" + cboCommodity.Value + "&projecttype=" + projecttype + "&supplier=" + cboSupplier.Value + "&groupcommodity=" + cboGroupCommodity.Value + "&pic=" + pUser + "&page=acc")
        End If
    End Sub

    Protected Sub btnPackingChargeCost_Click(sender As Object, e As EventArgs) Handles btnPackingChargeCost.Click
        If checkingCbo() = True Then
            Dim projecttype As String = clsQuotationSupplierDB.getProjectType(cboProject.Value)
            'Response.Redirect("QuotationSupplier_PackingChargeCost.aspx?projectid=" + cboProject.Value + "&groupid=" + cboGroup.Value + "&commodity=" + cboCommodity.Value + "&groupcomodity=" + cboGroupCommodity.Value + +"&projecttype=" + projecttype + "&page=acc&supplier=" + cboSupplier.Value)
            Response.Redirect("QuotationSupplier_PackingChargeCost.aspx?projectid=" + cboProject.Value + "&groupid=" + cboGroup.Value + "&commodity=" + cboCommodity.Value + "&projecttype=" + projecttype + "&supplier=" + cboSupplier.Value + "&groupcommodity=" + cboGroupCommodity.Value + "&pic=" + pUser + "&page=acc")
        End If
    End Sub

    Protected Sub btnToolingCost_Click(sender As Object, e As EventArgs) Handles btnToolingCost.Click
        If checkingCbo() = True Then
            Dim projecttype As String = clsQuotationSupplierDB.getProjectType(cboProject.Value)
            Dim Status As String = ""

            'check acceptance & complete status
            If clsQuotationSupplierDB.checkingStatus_Header(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboSupplier.Value) = "0" And clsQuotationSupplierDB.checkingStatusComplete_Header(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboSupplier.Value) = "1" Then
                Status = "1" 'accept (0) & complete (1)
            ElseIf clsQuotationSupplierDB.checkingStatus_Header(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboSupplier.Value) = "1" And clsQuotationSupplierDB.checkingStatusComplete_Header(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboSupplier.Value) = "1" Then
                Status = "2" 'accept (1) & complete (1)
            Else
                Status = "4" 'reject (2) & complete (1)
            End If

            Response.Redirect("QuotationSupplier_ToolingCost.aspx?projectid=" + cboProject.Value + "&groupid=" + cboGroup.Value + "&commodity=" + cboCommodity.Value + "&projecttype=" + projecttype + "&supplier=" + cboSupplier.Value + "&groupcommodity=" + cboGroupCommodity.Value + "&pic=" + pUser + "&page=acc" + "&status=" + Status)
        End If
    End Sub

    Private Sub cbMessage_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbMessage.Callback

    End Sub

    Private Sub cbComplete_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbComplete.Callback
        If clsQuotationSupplierDB.checkingStatus_Header(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboSupplier.Value) = "0" And clsQuotationSupplierDB.checkingStatusComplete_Header(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboSupplier.Value) = "1" Then
            cbComplete.JSProperties("cp_statusComplete") = "true"
        Else
            cbComplete.JSProperties("cp_statusComplete") = "false"
        End If
    End Sub

    Private Sub cbStatus_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbStatus.Callback
        Dim a As String = clsQuotationSupplierDB.getStatus_Header(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboSupplier.Value, pErr)
        cbStatus.JSProperties("cp_StatusData") = a
    End Sub
#End Region


    Private Sub cboSupplier_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSupplier.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboSupplier, cboProject.Value, cboGroup.Value, "", "S", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboSupplier, cboProject.Value, cboGroup.Value, "", "S", statusAdmin, pUser)
        End If
    End Sub

    Protected Sub btnAcceptAll_Click(sender As Object, e As EventArgs) Handles btnAcceptAll.Click
        If checkingCbo() = True Then
            clsQuotationSupplierDB.AcceptAndReject("A", cboProject.Value, cboGroup.Value, cboCommodity.Value, "", cboSupplier.Value, pErr)

            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
                cbSave.JSProperties("cpMessage") = pErr
            Else
                Grid.CancelEdit()
                cbSave.JSProperties("cpMessage") = "Data Accept successfully!"
                'show_error(MsgTypeEnum.Success, "Data Saved successfully!", 1)
                btnAcceptAll.ClientEnabled = False
                btnRejectAll.ClientEnabled = False
            End If
        End If
    End Sub

    Protected Sub btnRejectAll_Click(sender As Object, e As EventArgs) Handles btnRejectAll.Click
        If checkingCbo() = True Then
            If String.IsNullOrEmpty(txtRemarks.Text) Then
                cbSave.JSProperties("cpMessage") = "Remarks cannot be empty!"
                up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
                up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
                up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
                up_FillComboGroupCommodityPIC(cboSupplier, cboProject.Value, cboGroup.Value, cboCommodity.Value, "S", statusAdmin, pUser)
            Else
                clsQuotationSupplierDB.AcceptAndReject("R", cboProject.Value, cboGroup.Value, cboCommodity.Value, txtRemarks.Text, cboSupplier.Value, pErr)

                If pErr <> "" Then
                    cbSave.JSProperties("cpMessage") = pErr
                    show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
                Else
                    Grid.CancelEdit()
                    'show_error(MsgTypeEnum.Success, "Data Saved successfully!", 1)
                    cbSave.JSProperties("cpMessage") = "Data Reject successfully!"
                    btnAcceptAll.ClientEnabled = False
                    btnRejectAll.ClientEnabled = False
                End If
            End If

        End If
    End Sub

    'Protected Sub btnRejectOnRow()
    '    For i As Integer = 0 To Grid.VisibleRowCount
    '        Dim txt As ASPxTextBox = TryCast(Grid.FindRowCellTemplateControl(i, Grid.DataColumns.Item(19), "txt"), ASPxTextBox)
    '        Dim partno As String = Grid.GetRowValues(i, "Part_No")
    '        If Not IsNothing(txt) Then
    '            clsQuotationSupplierDB.RejectOnRow(cboProject.Value, cboGroup.Value, cboCommodity.Value, txt.Text, cboSupplier.Value, pUser, partno, pErr)
    '            If pErr <> "" Then
    '                Exit For
    '            End If
    '        End If
    '    Next

    '    If pErr <> "" Then
    '        show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
    '    Else
    '        Grid.CancelEdit()
    '        show_error(MsgTypeEnum.Success, "Data Saved successfully!", 1)
    '    End If

    'End Sub

    Private Sub CurrCB_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles CurrCB.Callback
        Dim ds As DataSet = clsPRJListDB.getDetailGrid(cboProject.Value, pUser, "")

        CurrCB.JSProperties("cp_CBRateUSD") = ds.Tables(0).Rows(0)("Rate_USD_IDR").ToString()
        CurrCB.JSProperties("cp_CBRateJPY") = ds.Tables(0).Rows(0)("Rate_YEN_IDR").ToString()
        CurrCB.JSProperties("cp_CBRateTHB") = ds.Tables(0).Rows(0)("Rate_BATH_IDR").ToString()
    End Sub
   
End Class
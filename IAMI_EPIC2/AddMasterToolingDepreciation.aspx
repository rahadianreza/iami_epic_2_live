﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="AddMasterToolingDepreciation.aspx.vb" Inherits="IAMI_EPIC2.AddMasterToolingDepreciation" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function MessageError(s, e) {
            if (s.cpMessage == "Data Saved Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                cboPeriod.SetText('');
                cboType.SetText('');
                cboMaterialCode.SetText('');
                txtMaterialName.SetText('');
                txtSpecification.SetText('');
                cboUoMUnitPrice.SetText('');
                txtUnitPrice.SetText('');
                cboUoMUnitConsump.SetText('');
                txtUnitConsump.SetText('');
                cboCategoryMaterial.SetText('');
                cboGroupItem.SetText('');
                cboCurrency.SetText('');
                cboMaterialCategory.SetText('');
            }
            else if (s.cpMessage == "Data Updated Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

            }
            else if (s.cpMessage == "Data Clear Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                cboPeriod.SetText('');
                cboType.SetText('');
                cboMaterialCode.SetText('');
                txtMaterialName.SetText('');
                txtSpecification.SetText('');
                cboUoMUnitPrice.SetText('');
                txtUnitPrice.SetText('');
                cboUoMUnitConsump.SetText('');
                txtUnitConsump.SetText('');
                cboCategoryMaterial.SetText('');
                cboGroupItem.SetText('');
                cboCurrency.SetText('');
                cboMaterialCategory.SetText('');
            }
            else if (s.cpMessage == "Data Cancel Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function GridLoad() {
            Grid.PerformCallback('refresh');
            tmpProjectType.SetText(cboProjectType.GetValue());
        };

        function fn_AllowonlyNumeric(s, e) {
            var theEvent = e.htmlEvent || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;

            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault)
                    theEvent.preventDefault();
            }
        };

        function MessageDelete(s, e) {

            if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function FilterGroup() {
            cboGroupItem.PerformCallback('filter|' + cboPRType.GetValue());
        }

        function FilterCategory() {
            cboCategory.PerformCallback('filter|' + cboGroupItem.GetValue());
        }
        function SetCode(s, e) {
            tmpProjectType.SetText(cboProjectType.GetValue());
        }

        document.getElementById('form').addEventListener('keypress', function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <br />
    <table style="width: 100%;">
        <tr style="height: 25px">
            <td style="width: 150px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Part No">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtPartNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtPartNo"
                    MaxLength="15">
                </dx:ASPxTextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Part Name">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtPartName" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtPartName"
                    MaxLength="15">
                </dx:ASPxTextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Name">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtProjectName" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtProjectName"
                    MaxLength="30">
                </dx:ASPxTextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Supplier Name">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtSupplierName" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtSupplierName"
                    MaxLength="50">
                </dx:ASPxTextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Tooling Price">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtToolingPrice" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtToolingPrice"
                    MaxLength="50" HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>" />
                </dx:ASPxTextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Interest / Year">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtInterestYear" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtInterestYear"
                    MaxLength="10" HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>" />
                </dx:ASPxTextBox>
            </td>
            <td>
            </td>
            <td style="width: 180px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Depreciation Period (Year)">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtDepreciationPeriodYear" runat="server" Font-Names="Segoe UI"
                    Font-Size="9pt" ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtDepreciationPeriodYear"
                    MaxLength="10" HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Total Interest">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td colspan="3">
                <table>
                    <tr>
                        <td style="width: 170px">
                            <dx:ASPxTextBox ID="txtTotalInterest" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtTotalInterest"
                                MaxLength="50" HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>" />
                            </dx:ASPxTextBox>
                        </td>
                        <td style="text-align: left;">
                            <dx:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="*) Interest/Year * Depreciation Period (year)">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel16" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Qty PerUnit">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtQtyPerUnit" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtQtyPerUnit"
                    MaxLength="10" HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>" />
                </dx:ASPxTextBox>
            </td>
            <td>
            </td>
            <td style="width: 180px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel17" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Prod Unit Permonth">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtProjectProdUnitPermonth" runat="server" Font-Names="Segoe UI"
                    Font-Size="9pt" ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtProjectProdUnitPermonth"
                    MaxLength="15" HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel18" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Depreciation Period">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td colspan="3">
                <table>
                    <tr>
                        <td style="width: 170px">
                            <dx:ASPxTextBox ID="txtDepreciationPeriod" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtDepreciationPeriod"
                                MaxLength="4" HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>" />
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="*) Qty Perunit * (Project prod Unit Permonth * 12)">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Total Amount Interest">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td colspan="3">
                <table>
                    <tr>
                        <td style="width: 170px">
                            <dx:ASPxTextBox ID="txtTotalAmountInterest" runat="server" Font-Names="Segoe UI"
                                Font-Size="9pt" ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtTotalAmountInterest"
                                MaxLength="4" HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>" />
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel12" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="*) Total Price * Total Interest">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
            </td>
            <td colspan="4" align="left">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Total Tooling Cost">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td colspan="3">
                <table>
                    <tr>
                        <td style="width: 170px">
                            <dx:ASPxTextBox ID="txtTotalToolingCost" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtTotalToolingCost"
                                MaxLength="4" HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>" />
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel14" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="*) Tooling Price + Total Amount Interest">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 100px; padding-left: 30px;">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Depreciation Cost">
                </dx:ASPxLabel>
            </td>
            <td>
            </td>
            <td colspan="3">
                <table>
                    <tr>
                        <td style="width: 170px">
                            <dx:ASPxTextBox ID="txtDepreciationCost" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                ReadOnly="true" BackColor="LightGray" Width="170px" ClientInstanceName="txtDepreciationCost"
                                MaxLength="4" HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>" />
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel19" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                Text="*) Tooling Price + Total Amount Interest">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table>
        <tr>
            <td style="padding-left: 30px;">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back to List" AutoPostBack="False"
                    Font-Names="Segoe UI" UseSubmitBehavior="false" Font-Size="9pt" Width="80px"
                    Height="25px" ClientInstanceName="btnBack" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <dx:ASPxCallback ID="cbDescriptions" ClientInstanceName="cbDescriptions" runat="server">
        <ClientSideEvents EndCallback="function(s,e) {
                txtMaterialName.SetValue(s.cp_Descs);
          }
          " />
    </dx:ASPxCallback>
    <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
        <ClientSideEvents Init="MessageError" />
    </dx:ASPxCallback>
    <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
        <ClientSideEvents Init="MessageError" />
    </dx:ASPxCallback>
</asp:Content>

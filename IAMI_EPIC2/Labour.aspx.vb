﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports Microsoft.Office.Interop.Excel
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class Labour
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""

    Dim pHeader As Boolean
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
    Dim fRefresh As Boolean = False
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(pPeriodFrom As String, pPeriodTo As String, pProvince As String, pCity As String)
        Dim ErrMsg As String = ""
        'Dim Pro As List(Of clsLabor)
        Dim ds As New DataSet
        ds = ClsLabourDB.getListLabor(pPeriodFrom, pPeriodTo, pProvince, pCity, pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
            Dim Script
            If ds.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cpMessage") = "Success"
                Script = "btnDownload.SetEnabled(true);"
                ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)
            Else
                Grid.JSProperties("cpMessage") = "There is no data to show!"
                Script = "btnDownload.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)
            End If
        Else
            Grid.JSProperties("cpMessage") = ErrMsg
        End If
    End Sub

    Private Sub up_ExcelGridAuto(Optional ByRef pErr As String = "")
        Try
            up_GridLoad(ASPxSpinEditPeriodFrom.Text, ASPxSpinEditPeriodTo.Text, cboProvince.Value, cboRegion.Value)

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=LaborList" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using
            ps.Dispose()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    'filter region per province
    Private Sub up_FillComboRegion(pProvince As String, Optional ByRef pErr As String = "")
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsLabourDB.GetDataRegion(pProvince, "ALL", pUser, pmsg)
        If pmsg = "" Then
            cboRegion.DataSource = ds
            cboRegion.DataBind()
        End If
    End Sub
    Private Sub cboRegion_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboRegion.Callback
        Dim pProvince As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        up_FillComboRegion(pProvince, errmsg)

        If cboRegion.Items.Count > 0 Then
            cboRegion.SelectedIndex = 0
        End If
    End Sub
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        '    up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A070")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A070")

        'cbExcel.JSProperties("cpmessage") = ""

        If Not Page.IsCallback And Not Page.IsPostBack Then
            ASPxSpinEditPeriodFrom.Number = Date.Today.Year
            ASPxSpinEditPeriodTo.Number = Date.Today.Year
            cboProvince.SelectedIndex = 0
            cboRegion.SelectedIndex = 0

            Dim Script As String
            Script = "btnDownload.SetEnabled(false);"
            ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)
        End If

        'If Not Page.IsPostBack Then
        '    'If gs_Back = True Then
        '    up_GridLoad(ASPxSpinEditPeriodFrom.Text, ASPxSpinEditPeriodTo.Text)

        '    'If
        'End If

    End Sub
#End Region

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("~/LabourDetail.aspx")
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad(ASPxSpinEditPeriodFrom.Text, ASPxSpinEditPeriodTo.Text, cboProvince.Value, cboRegion.Value)
        End If
        fRefresh = False
        'pHeader = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback

        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pPeriodFrom As String
        Dim pPeriodTo As String
        Dim pProvince As String, pCity As String = ""


        pPeriodFrom = Split(e.Parameters, "|")(1)
        pPeriodTo = Split(e.Parameters, "|")(2)
        pProvince = Split(e.Parameters, "|")(3)

        If pFunction = "gridload" Then
            pCity = Split(e.Parameters, "|")(4)
        End If

        pHeader = True
        'If pFunction = "gridload" Then
        '    up_GridLoad(pPeriodFrom, pPeriodTo)
        'End If


        If pFunction = "gridload" Then
            up_GridLoad(pPeriodFrom, pPeriodTo, pProvince, pCity)
            If Grid.VisibleRowCount > 0 Then
                'If ds.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cp_disabled") = "N"
                'Script = "btnDownload.SetEnabled(true);"
            Else
                Grid.JSProperties("cp_disabled") = "Y"
                show_error(MsgTypeEnum.Info, "There is no data to show!", 1)
                'Grid.JSProperties("cp_Message") = "There is no data to show!"
            End If
            fRefresh = True
        ElseIf pFunction.Trim = "delete" Then
            Dim pErr As String = ""

            ClsLabourDB.DeleteLaborData(Split(e.Parameters, "|")(1), Split(e.Parameters, "|")(2), Split(e.Parameters, "|")(3), pUser, pErr)

            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
                up_GridLoad(pPeriodFrom, pPeriodTo, pProvince, pCity)
            End If
        End If



    End Sub

    Protected Sub BtnDownload_Click(sender As Object, e As EventArgs) Handles BtnDownload.Click
        'cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        Dim ErrMsg As String = ""
        'up_Excel(ErrMsg)
        up_ExcelGridAuto(ErrMsg)

        'If ErrMsg <> "" Then
        '    cbExcel.JSProperties("cpmessage") = ErrMsg
        'Else
        '    cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        'End If

    End Sub


    Protected Sub btnChart_Click(sender As Object, e As EventArgs) Handles btnChart.Click
        Response.Redirect("LabourChart.aspx")
    End Sub
End Class
﻿Imports DevExpress.Web.ASPxGridView
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports System.IO

Public Class PRJListDetail
    Inherits System.Web.UI.Page
#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
    Dim _projectID As String

#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        _projectID = Request.QueryString("ID") & ""
        ' GenerateColumnYear()
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            AddColumns()
            up_GridDetail("1")
        End If
    End Sub

    Private Sub up_GridLoad()
        Dim ErrMsg As String = ""
        Dim vPRType As String = ""
        Dim vProjectType As String = ""
        Dim vSection As String = ""
        Dim ls_Back As Boolean = False

        up_GridDetail()

        Dim ds As New DataSet
        ds = clsPRJListDB.getDetailGrid(_projectID, pUser, statusAdmin)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count = 0 Then
                up_GridDetail("1")
            End If

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Public Function NextYear(startYear As Integer) As String
        Return String.Format("{0},{1},{2}", startYear, startYear + 1, startYear + 2)
    End Function

    Private Sub up_GridDetail(Optional ByRef pLoad As String = "")
        Dim ErrMsg As String = ""
        Dim vPRJType As String = ""
        Dim ls_Back As Boolean = False
        Dim ds As New DataSet
        ds = clsPRJListDB.getDetailGrid(_projectID, pUser, statusAdmin, ErrMsg)
        Dim year As String = clsPRJListDB.GetStartYear(_projectID)
        Dim words As String() = NextYear(year).Split(New Char() {","c})

        ' Grid.Columns("Year1").Caption = words(0)
        'Grid.Columns("Year2").Caption = words(1)
        '  Grid.Columns("Year3").Caption = words(2)

    End Sub

    Private Sub up_Excel()
        up_GridLoad()

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=PRJList" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

#End Region

#Region "Control Event"
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Protected Sub Grid_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad()
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then

            up_GridDetail("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")
            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else
            up_GridLoad()
        End If
    End Sub

    'unused
    Protected Sub Grid_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsPRJList With {.ProjectId = _projectID,
                                        .VehicleCode = e.NewValues("Vehicle_Code"),
                                        .Year1 = e.NewValues("Year1"),
                                        .Year2 = e.NewValues("Year2"),
                                        .Year3 = e.NewValues("Year3"),
                                        .TotalVolumeRfq = e.NewValues("Total_Volume_Rfq"),
                                        .Remarks = e.NewValues("Remarks"),
                                        .RegisterBy = pUser}

        clsPRJListDB.InsertDetail(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1)
            up_GridLoad()
        End If
    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsPRJList

        Ses.ProjectId = _projectID
        Ses.VehicleCode = e.OldValues("Vehicle_Code")
        Ses.Remarks = e.NewValues("Remarks")
        Ses.RegisterBy = pUser

        Dim ds As New DataSet
        ds = DataSetGetYear(pErr)

        If pErr = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each c As DataColumn In ds.Tables(0).Columns
                    clsPRJListDB.UpdateDetail(Ses, e.NewValues("Vehicle_Code"), e.NewValues(c.ColumnName), c.ColumnName, pErr)
                Next
            End If

        End If

        'With {.ProjectId = _projectID,
        '                                .VehicleCode = e.OldValues("Vehicle_Code"),
        '                                .Year1 = e.NewValues("Year1"),
        '                                .Year2 = e.NewValues("Year2"),
        '                                .Year3 = e.NewValues("Year3"),
        '                                .TotalVolumeRfq = e.NewValues("Total_Volume_Rfq"),
        '                                .Remarks = e.NewValues("Remarks"),
        '                                .RegisterBy = pUser}



        If pErr <> "" Then
            'Grid.CancelEdit()
            up_GridLoad()
            show_error(MsgTypeEnum.Warning, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
            up_GridLoad()
        End If
    End Sub

    'unused
    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsPRJList With {.ProjectId = _projectID,
                                       .VehicleCode = e.Values("Vehicle_Code")}
        clsPRJListDB.DeleteDetail(Ses, pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
            up_GridLoad()
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        End If
    End Sub

    Protected Sub Grid_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In Grid.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "Vehicle_Code" Then
                If IsNothing(e.NewValues("Vehicle_Code")) OrElse e.NewValues("Vehicle_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Vehicle Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Year1" Then
                If IsNothing(e.NewValues("Year1")) OrElse e.NewValues("Year1").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Year!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Year2" Then
                If IsNothing(e.NewValues("Year2")) OrElse e.NewValues("Year2").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Year!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Year3" Then
                If IsNothing(e.NewValues("Year3")) OrElse e.NewValues("Year3").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Year!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1)
                    Exit Sub
                End If
            End If

           

        Next column
    End Sub

    Protected Sub Grid_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)

        'If (Not Grid.IsNewRowEditing) Then
        '    Grid.DoRowValidation()
        'End If
        'show_error(MsgTypeEnum.Info, "", 0)
    End Sub

    Private Sub Grid_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.BeforeGetCallbackResult
        If Grid.IsNewRowEditing Then
            Grid.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub Grid_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        'If Not Grid.IsNewRowEditing Then
        '    If e.Column.FieldName = "Vehicle_Code" Then
        '        e.Editor.ReadOnly = True
        '        e.Editor.ForeColor = Color.Silver
        '    End If
        'End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("PRJList.aspx")
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub Grid_HtmlRowPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles Grid.HtmlRowPrepared
        If e.RowType = DevExpress.Web.ASPxGridView.GridViewRowType.Group Then
            For Each cell As TableCell In e.Row.Cells
                cell.Style(HtmlTextWriterStyle.WhiteSpace) = "pre"
                cell.Style(HtmlTextWriterStyle.FontFamily) = "Monospace"
            Next cell
        End If
    End Sub

    Private Function DataSetGetYear(Optional ByRef pErr As String = "") As DataSet
        Return clsPRJListDB.GetYear(_projectID, pErr)
    End Function

    Private Sub AddColumns()
        Dim ds As New DataSet
        Dim pErr As String = ""
        ds = DataSetGetYear(pErr)
        Dim inc As Integer = 0
        If pErr = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim b1 As GridViewBandColumn = New GridViewBandColumn
                For Each c As DataColumn In ds.Tables(0).Columns
                    'AddTextColumn(c.ColumnName, inc)
                    'create band
                    If Grid.Columns.IndexOf(Grid.Columns("Year")) = -1 Then
                        b1.Caption = "Year"
                        b1.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                        b1.VisibleIndex = 4
                    End If

                    If Grid.Columns.IndexOf(Grid.Columns(c.ColumnName)) <> -1 Then Return
                    Dim year1 As GridViewDataTextColumn = New GridViewDataTextColumn
                    year1.FieldName = c.ColumnName
                    year1.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                    ' year1.CellStyle.HorizontalAlign = HorizontalAlign.Center
                    year1.FooterCellStyle.HorizontalAlign = HorizontalAlign.Center
                    year1.GroupFooterCellStyle.HorizontalAlign = HorizontalAlign.Center
                    year1.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False
                    year1.Width = 80
                    year1.VisibleIndex = inc
                    year1.EditFormSettings.VisibleIndex = 1 + inc
                    year1.PropertiesTextEdit.Style.HorizontalAlign = HorizontalAlign.Right
                    year1.PropertiesTextEdit.Width = "100"
                    year1.PropertiesTextEdit.MaxLength = "8"
                    year1.PropertiesTextEdit.DisplayFormatString = "#,###"
                    year1.PropertiesTextEdit.MaskSettings.Mask = "<0..1000000g>.<00..99>"
                    year1.PropertiesTextEdit.MaskSettings.IncludeLiterals = DevExpress.Web.ASPxEditors.MaskIncludeLiteralsMode.DecimalSymbol
                    b1.Columns.Add(year1)
                    inc += 1
                Next c
                Grid.Columns.Add(b1)
            Else
                Grid.Columns("Year1").Visible = True
            End If
        End If

    End Sub
#End Region

    'unused
    Private Sub GenerateColumnYear()

        'create band
        If Grid.Columns.IndexOf(Grid.Columns("Year")) <> -1 Then Return

        Dim b1 As GridViewBandColumn = New GridViewBandColumn
        b1.Caption = "Year"
        b1.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
        b1.VisibleIndex = 4
        Grid.Columns.Add(b1)

        'create detail band

        If Grid.Columns.IndexOf(Grid.Columns("2019")) <> -1 Then Return
        Dim year1 As GridViewDataTextColumn = New GridViewDataTextColumn
        year1.Caption = "2019"
        year1.UnboundType = DevExpress.Data.UnboundColumnType.Decimal
        year1.VisibleIndex = 0
        'year1.PropertiesTextEdit.DisplayFormatString = "c0"
        year1.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
        year1.CellStyle.HorizontalAlign = HorizontalAlign.Center
        year1.FooterCellStyle.HorizontalAlign = HorizontalAlign.Center
        year1.GroupFooterCellStyle.HorizontalAlign = HorizontalAlign.Center
        year1.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False
        year1.Width = 80
        b1.Columns.Add(year1)

        If Grid.Columns.IndexOf(Grid.Columns("2020")) <> -1 Then Return
        Dim year2 As GridViewDataTextColumn = New GridViewDataTextColumn
        year2.Caption = "2020"
        year2.UnboundType = DevExpress.Data.UnboundColumnType.Decimal
        year2.VisibleIndex = 1
        'year2.PropertiesTextEdit.DisplayFormatString = "c0"
        year2.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
        year2.CellStyle.HorizontalAlign = HorizontalAlign.Center
        year2.FooterCellStyle.HorizontalAlign = HorizontalAlign.Center
        year2.GroupFooterCellStyle.HorizontalAlign = HorizontalAlign.Center
        year2.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False
        year2.Width = 80
        b1.Columns.Add(year2)

        If Grid.Columns.IndexOf(Grid.Columns("2021")) <> -1 Then Return
        Dim year3 As GridViewDataTextColumn = New GridViewDataTextColumn
        year3.Caption = "2021"
        year3.UnboundType = DevExpress.Data.UnboundColumnType.Decimal
        year3.VisibleIndex = 2
        'year3.PropertiesTextEdit.DisplayFormatString = "c0"
        year3.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
        year3.CellStyle.HorizontalAlign = HorizontalAlign.Center
        year3.FooterCellStyle.HorizontalAlign = HorizontalAlign.Center
        year3.GroupFooterCellStyle.HorizontalAlign = HorizontalAlign.Center
        year3.Settings.AllowSort = DevExpress.Utils.DefaultBoolean.False
        year3.Width = 80
        b1.Columns.Add(year3)

    End Sub
End Class
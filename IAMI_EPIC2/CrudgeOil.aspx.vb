﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.UI
Imports System.IO
Imports System.Drawing
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls.Style
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxUploadControl
Imports System
Imports DevExpress.Utils
Imports System.Collections.Generic
Imports OfficeOpenXml
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Public Class CrudgeOil
    Inherits System.Web.UI.Page


#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A090")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            'up_FillCombo(cboMaterialCode, statusAdmin, pUser)
            Dim dfrom As Date
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dtPeriodFrom.Value = dfrom
            dtPeriodTo.Value = Now

            Dim Script As String
            Script = "btnDownload.SetEnabled(false);"
            ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)

        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad()
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters
        Dim pFunctionFirst As String = Split(e.Parameters, "|")(0)

        If pFunction = "refresh" Then
            up_GridLoad()
            Dim PeriodFrom As String = Format(dtPeriodFrom.Value, "yyyy-MM-dd")
            Dim PeriodTo As String = Format(dtPeriodTo.Value, "yyyy-MM-dd")
            ds = ClsCrudgeOilDB.getGrid(PeriodFrom, PeriodTo, pUser)

            'Grid.DataSource = ds
            'Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            If ds.Tables(0).Rows.Count > 0 Then
                'If ds.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cp_disabled") = "N"
                'Script = "btnDownload.SetEnabled(true);"
            Else
                Grid.JSProperties("cp_disabled") = "Y"
                show_error(MsgTypeEnum.Info, "There is no data to show!", 1)
                'Grid.JSProperties("cp_Message") = "There is no data to show!"
            End If

            fRefresh = True
        ElseIf pFunctionFirst.Trim = "delete" Then
            Dim pErr As String = ""

            ClsCrudgeOilDB.DeleteCrudgeOil(Split(e.Parameters, "|")(1), Split(e.Parameters, "|")(2), pUser, pErr)

            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
                up_GridLoad()
            End If
        Else

            up_GridLoad()
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_GridLoad()
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        Dim PeriodFrom As String = Format(dtPeriodFrom.Value, "yyyy-MM-dd")
        Dim PeriodTo As String = Format(dtPeriodTo.Value, "yyyy-MM-dd")
        ds = ClsCrudgeOilDB.getGrid(PeriodFrom, PeriodTo, pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
            Dim Script

            If ds.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cpMessage") = "Success"
                Script = "btnDownload.SetEnabled(true);"
                ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)
            Else
                Grid.JSProperties("cpMessage") = "There is no data to show!"
                Script = "btnDownload.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)
            End If
        End If
    End Sub

    'Private Sub up_FillCombo(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
    '                        pAdminStatus As String, pUserID As String, _
    '                        Optional ByRef pGroup As String = "", _
    '                        Optional ByRef pParent As String = "", _
    '                        Optional ByRef pErr As String = "")

    '    Dim ds As New DataSet
    '    Dim pmsg As String = ""

    '    ds = ClsMaterialPartDB.getMaterialCode(cboCategoryMaterial.Value, pUserID, pErr)
    '    If pErr = "" Then
    '        Dim a As Integer = ds.Tables(0).Rows.Count
    '        cbo.DataSource = ds
    '        cbo.DataBind()
    '    End If
    'End Sub
#End Region

#Region "Event Control"


    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("AddCrudgeOil.aspx")
    End Sub
#End Region
    Private Sub up_Excel()
        up_GridLoad()

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=CrudgeOil" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        Grid.JSProperties("cp_message") = "Download Data to Excel Has Been Successfully"
    End Sub

    Protected Sub btnChart_Click(sender As Object, e As EventArgs) Handles btnChart.Click
        Response.Redirect("CrudgeOilChart.aspx")
    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="IAPriceApproval.aspx.vb" Inherits="IAMI_EPIC2.IAPriceApproval" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function FillComboIANumber() {
        cboIAPriceNo.PerformCallback(dtDateFrom.GetText() + '|' + dtDateTo.GetText());
    }

    function downloadValidation(s, e) {
        if (Grid.GetVisibleRowsOnPage() == 0) {
            toastr.info('There is no data to download!', 'Info');
            e.processOnServer = false;
            return;
        }
    }
    

</script>

<style type="text/css">
    .hidden-div
        {
            display:none;
        }
</style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
            <tr style="height: 20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>      
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="IA Price Date">
                    </dx1:aspxlabel>                       
                </td>
                <td></td>
                <td style="width:120px">
                <dx:aspxdateedit ID="dtDateFrom" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateFrom"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                        
                                        <ClientSideEvents ValueChanged="FillComboIANumber" />
                                    </dx:aspxdateedit>                       </td>
                <td style="width:30px">   
                    
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="To">
                    </dx1:aspxlabel>   
                                                     
                </td>
                <td>
                <dx:aspxdateedit ID="dtDateTo" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateTo"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                        <ClientSideEvents ValueChanged="FillComboIANumber" />
                                    </dx:aspxdateedit>   
                    
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>   
            <tr style="height: 40px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="IA Price Number">
                    </dx1:aspxlabel>                       
                    
                </td>
                <td></td>
                <td colspan="3">           
  <dx1:ASPxComboBox ID="cboIAPriceNo" runat="server" ClientInstanceName="cboIAPriceNo"
                            Width="200px" Font-Names="Segoe UI"  TextField="IA_Number"
                            ValueField="IA_Number" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="IA Number" FieldName="IA_Number" Width="100px" />
                       
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>   
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                    <dx1:aspxlabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Status">
                    </dx1:aspxlabel>                 
                </td>
                <td></td>
                <td colspan="3">
                    <dx1:ASPxComboBox ID="cboStatus" runat="server" ClientInstanceName="cboStatus"
                            Width="150px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Status"
                            ValueField="Status" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" >                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Status" FieldName="Status" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
            </tr>
            <tr style="height: 20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>   
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; " colspan="7">
                
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnRefresh" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {                          
                                Grid.PerformCallback('gridload|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText() + '|' + cboIAPriceNo.GetText() + '|' + cboStatus.GetValue());
                            }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                     &nbsp;<dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnExcel" Theme="Default">                        
                       <ClientSideEvents Click="downloadValidation" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 &nbsp;</td>
                <td></td>
                <td></td>
            </tr>      
            <tr style="height: 20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>              
                   <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                   </dx:ASPxCallback>
                   <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select Rtrim(Status) as Status  From VW_Status_Approval">
                    </asp:SqlDataSource>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                     </dx:ASPxGridViewExporter>
                </td>
                <td class="hidden-div">
                            <dx1:ASPxTextBox ID="txtBack" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="50px" ClientInstanceName="txtBack" MaxLength="30" 
                        Height="25px" >
            </dx1:ASPxTextBox>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>   
                                                     
        </table>
    </div>

    <div style="padding: 5px 5px 5px 5px">
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" Width="100%" 
                Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="IA_Number;Revision_No;CE_Number" >

             <ClientSideEvents CustomButtonClick="function(s, e) {
		                       if(e.buttonID == 'edit'){                     
                                        var rowKey = Grid.GetRowKey(e.visibleIndex);  
                                                            
	                                    window.location.href= 'IAPriceApprovalDetail.aspx?ID=' + rowKey;
                                    }
                                }" />

            <Columns>
                 <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" FixedStyle="Left">
                     <CustomButtons>
                         <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                         </dx:GridViewCommandColumnCustomButton>
                     </CustomButtons>
                 </dx:GridViewCommandColumn>
                 <dx:GridViewDataTextColumn Caption="IA Price Number" VisibleIndex="1" FixedStyle="Left" 
                     FieldName="IA_Number" Width="180px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="IA Price Date" VisibleIndex="2" FixedStyle="Left"
                     FieldName="IA_Date" Width="100px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Revision No" FieldName="Revision_No" FixedStyle="Left" 
                     VisibleIndex="3" Width="100px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CE Number" VisibleIndex="4" 
                     FieldName="CE_Number" Width="180px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CE Date" VisibleIndex="5" 
                     FieldName="CE_Date" Width="100px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CP Number" VisibleIndex="6" 
                     FieldName="CP_Number" Width="200px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="CP Date" VisibleIndex="7" 
                     FieldName="CP_Date" Width="100px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                 </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Name="Status" Caption="Status" VisibleIndex="7" FieldName="Status" Width="150px">
                    
                    </dx:GridViewDataTextColumn>
                  <dx:GridViewBandColumn Caption="Prepared"  VisibleIndex="8">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Prepared By" FieldName="Register_By" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Prepared Date" FieldName="Register_Date" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn>
                 
                 
                 <dx:GridViewBandColumn VisibleIndex="9">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="BudgetDept_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="BudgetDept_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="BudgetDept_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn>
                 <dx:GridViewBandColumn VisibleIndex="10">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="PurcHead_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="PurcHead_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="PurcHead_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn>
                 <dx:GridViewBandColumn VisibleIndex="11">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="PurcAdv_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="PurcAdv_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="PurcAdv_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn>
                 <dx:GridViewBandColumn VisibleIndex="12">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="CostControl_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="CostControl_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="CostControl_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn>
                 <dx:GridViewBandColumn VisibleIndex="13">
                              <Columns>
                                  <dx:GridViewDataTextColumn Caption="Approved By" FieldName="GM_ApprovedBy" 
                                      VisibleIndex="4">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Date" FieldName="GM_ApprovedDate" 
                                      VisibleIndex="5">
                                      <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                              </PropertiesTextEdit>
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                                  <dx:GridViewDataTextColumn Caption="Approved Notes" FieldName="GM_ApprovedNote" 
                                      VisibleIndex="6">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <Settings AllowAutoFilter="False" />
                                  </dx:GridViewDataTextColumn>
                              </Columns>
                              <HeaderStyle HorizontalAlign="Center" />
                          </dx:GridViewBandColumn>
                 
             </Columns>

                    <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
             
                    <Settings ShowFilterRow="True" VerticalScrollableHeight="300" 
                HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>

         
                 <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
</EditFormColumnCaption>
                 </Styles>


         </dx:ASPxGridView>
    </div>

</div>
</asp:Content>

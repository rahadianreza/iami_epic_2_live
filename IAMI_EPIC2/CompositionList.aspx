﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="CompositionList.aspx.vb" Inherits="IAMI_EPIC2.CompositionList" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function MessageError(s, e) {
            if (s.cp_disabled == "N") {
                //alert(s.cp_disabled);
                btnDownload.SetEnabled(true);
            } else if (s.cp_disabled == "Y") {

                btnDownload.SetEnabled(false);
            }
            //alert(s.cpmessage);
            if (s.cpmessage == "Download Excel Successfully") {
                toastr.success(s.cpmessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpmessage == null || s.cpmessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpmessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

      </script>
    <style type="text/css">
        .style1
        {
            width: 82px;
        }
        .style5
        {
            height: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding: 5px 5px 5px 5px">
            <%--         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%"
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="ItemCode" DataSourceID="SqlDataSource1">--%>
            <table style="width: 100%; border: 1px solid black" >
                <tr style="height: 8px">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        
                        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                   
                    </td>
                    <td>
                    </td>
                    <td align="left" class="style5">
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td>
                        <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                            <ClientSideEvents Init="MessageError" />
                        </dx:ASPxCallback>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        <dx:ASPxCallback ID="cbExcel" runat="server" ClientInstanceName="cbExcel">
                            <ClientSideEvents Init="MessageError" />
                        </dx:ASPxCallback>
                    </td>
                </tr>
                <tr style="height: 25px">
                    <td style="padding: 0px 0px 0px 10px" class="style1" colspan="9">
                        <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnRefresh" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {                      
	                        Grid.PerformCallback('gridload|');
                        }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;<dx:ASPxButton ID="BtnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="BtnDownload" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
	                            cbExcel.PerformCallback();
                            }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;
                        <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="false" Font-Names="Segoe UI"
                            Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnAdd" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                </tr>
                <tr style="height: 0px">
                    <td style="padding: 0px 0px 0px 10px" class="style1" colspan="9">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" Width="100%" 
                Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Parent_MaterialCode" >

             <ClientSideEvents CustomButtonClick="function(s, e) {
		                           if(e.buttonID == &#39;Edit&#39;){                     
                                        var rowKey = Grid.GetRowKey(e.visibleIndex);  
                                        window.location.href= 'Composition_Detail.aspx?ID=' + rowKey;
                                   } else if (e.buttonID == &#39;Delete&#39;){
                                        if (confirm('Are you sure want to delete ?')) {
                                            var rowKey = Grid.GetRowKey(e.visibleIndex);
                                            Grid.PerformCallback('delete |' + rowKey);
                                        } else {
                     
                                        }
                                   }
                               }" />

             <Columns>
                 <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" FixedStyle="Left">
                     <CustomButtons>
                         <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit">
                         </dx:GridViewCommandColumnCustomButton>
                     </CustomButtons>
                     <CustomButtons>
                         <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete">
                         </dx:GridViewCommandColumnCustomButton>
                     </CustomButtons>
                 </dx:GridViewCommandColumn>
                  <dx:GridViewDataTextColumn Caption="" VisibleIndex="1" FixedStyle="Left" Visible="false"
                     FieldName="Sequence_No" Width="50px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Parent Material" VisibleIndex="2" FixedStyle="Left" 
                     FieldName="Parent_MaterialCode" Width="150px">
                     
                      <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Parent Material Name" VisibleIndex="3" FixedStyle="Left"
                     FieldName="Parent_MaterialName" Width="200px">
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Child Material" VisibleIndex="4" FixedStyle="Left"
                     FieldName="Child_MaterialCode" Width="150px">
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Child Material Name" VisibleIndex="5" FixedStyle="Left"
                     FieldName="Child_MaterialName" Width="200px">
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Composition %" VisibleIndex="6" 
                     FieldName="CompositionPercent" Width="120px">
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="UOM" VisibleIndex="7" FixedStyle="Left"
                     FieldName="UOMDesc" Width="100px">
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>

                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Register By" FieldName="Register_User" VisibleIndex="8"
                     Width="150px">
                     <PropertiesTextEdit Width="150px" ClientInstanceName="Register_User">
                     </PropertiesTextEdit>
                     <EditFormSettings Visible="False" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                     <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                     </CellStyle>
                     <Settings AllowAutoFilter="False" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataDateColumn Caption="Register Date" FieldName="Register_Date" VisibleIndex="9"
                     Width="100px">
                     <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd MMM yyyy">
                     </PropertiesDateEdit>
                     <EditFormSettings Visible="False" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                     <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                     </CellStyle>
                     <Settings AllowAutoFilter="False" />
                 </dx:GridViewDataDateColumn>
                 <dx:GridViewDataTextColumn Caption="Update By" FieldName="Update_User" VisibleIndex="10"
                     Width="150px">
                     <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateUser">
                     </PropertiesTextEdit>
                     <EditFormSettings Visible="False" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                     <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                     </CellStyle>
                     <Settings AllowAutoFilter="False" />
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataDateColumn Caption="Update Date" FieldName="Update_Date" VisibleIndex="11"
                     Width="100px">
                     <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd MMM yyyy">
                     </PropertiesDateEdit>
                     <EditFormSettings Visible="False" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                         Wrap="True">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                     <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                     </CellStyle>
                     <Settings AllowAutoFilter="False" />
                 </dx:GridViewDataDateColumn>
             </Columns>
                <SettingsBehavior ColumnResizeMode="Control" />
                <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                </SettingsPager>
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                    HorizontalScrollBarMode="Auto" />
                <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>
                    <EditFormColumnCaption>
                        <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
                    </EditFormColumnCaption>
                </Styles>
            </dx:ASPxGridView>
        </div>
    </div>

</asp:Content>

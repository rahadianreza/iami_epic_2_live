﻿Imports DevExpress.XtraPrinting
Imports System.IO
Imports OfficeOpenXml
Imports DevExpress.XtraPrintingLinks

Public Class DevelopmentListSupplier
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""

    Dim pHeader As Boolean
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
    Dim fRefresh As Boolean = False
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(pProjectName As String)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        ds = ClsDevelopmentSupplierDB.getListDevelopmentSupplier(pProjectName, pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
            Dim Script
            If ds.Tables(0).Rows.Count > 0 Then

                Grid.JSProperties("cpMessage") = "Success"
                Script = "btnDownload.SetEnabled(true);"
                ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)
            Else
                Grid.JSProperties("cpMessage") = "There is no data to show!"
                Script = "btnDownload.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)
            End If
        Else
            Grid.JSProperties("cpMessage") = ErrMsg
        End If
    End Sub

    Private Sub up_ExcelGridAuto(Optional ByRef pErr As String = "")
        Try

            up_GridLoad(cboProject.Value)

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=DevelopmentListSupplier" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            gs_Message = ex.Message
        End Try

    End Sub
   
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            up_GridLoad("")
            'Dim Script = "btnDownload.SetEnabled(false);"
            'ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("O030")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "O030")

        If Not Page.IsPostBack And Not Page.IsCallback Then
            cboProject.SelectedIndex = 0
        End If

    End Sub
#End Region

#Region "Event"
    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        up_GridLoad(cboProject.Value)
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback

        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pProjectName As String
        pProjectName = Split(e.Parameters, "|")(1)

        If pFunction = "load" Then
            up_GridLoad(pProjectName)
        End If
    End Sub

    'Protected Sub cbExcel_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbExcel.Callback
    '    Dim ErrMsg As String = ""
    '    'up_Excel(ErrMsg)
    '    up_ExcelGridAuto(ErrMsg)
    '    If ErrMsg <> "" Then
    '        cbExcel.JSProperties("cpmessage") = ErrMsg
    '    Else
    '        cbExcel.JSProperties("cpmessage") = "Download Excel Successfully!"
    '    End If
    'End Sub
    Protected Sub btnDownload_Click1(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim ErrMsg As String = ""
        'up_Excel(ErrMsg)
        up_ExcelGridAuto(ErrMsg)
        If ErrMsg <> "" Then
            Grid.JSProperties("cpmessage") = ErrMsg
        Else
            Grid.JSProperties("cpmessage") = "Download Excel Successfully!"
        End If
    End Sub
#End Region

End Class
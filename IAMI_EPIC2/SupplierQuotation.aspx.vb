﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO

Public Class SupplierQuotation
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"
    Private Sub GetSupplier()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_GetSupplier", "UserID", Session("user"), ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                txtSupplierCode.Text = ds.Tables(0).Rows(0).Item("Supplier_Code").ToString().Trim()
                txtSupplierName.Text = ds.Tables(0).Rows(0).Item("Supplier_Name").ToString().Trim()
            End If

            If txtSupplierCode.Text = "" Then
                Dim script As String = ""
                script = "btnAdd.SetEnabled(false);" & vbCrLf & _
                         "btnExcel.SetEnabled(false);" & vbCrLf & _
                         "btnShow.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAdd, btnAdd.GetType(), "btnAdd", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub FillComboQuotationNumber()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"
        dtFrom = Format(dtQuotationFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtQuotationTo.Value, "yyyy-MM-dd")

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_FillCombo", "SQA|StartDate|EndDate|SupplierCode", "0|" & dtFrom & "|" & dtTo & "|" & txtSupplierCode.Text, ErrMsg)

        If ErrMsg = "" Then
            cboQuotationNumber.DataSource = ds.Tables(0)
            cboQuotationNumber.DataBind()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboQuotationNumber)
        End If
    End Sub

    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01", ls_QuotationNo As String = ""
        dtFrom = Format(dtQuotationFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtQuotationTo.Value, "yyyy-MM-dd")
        ls_QuotationNo = cboQuotationNumber.Text

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_List", "QuotationDateFrom|QuotationDateTo|QuotationNo|SupplierCode", dtFrom & "|" & dtTo & "|" & ls_QuotationNo & "|" & txtSupplierCode.Text, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub ExcelGrid()
        Try
            Call GridLoad()

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=SupplierQuotationList_" & Format(CDate(Now), "yyyyMMdd_HHmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            gs_Message = ex.Message
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        Dim dfrom As Date
        dfrom = Year(Now) & "-" & Month(Now) & "-01"
        dtQuotationFrom.Value = dfrom
        dtQuotationTo.Value = Now

        If IsNothing(Session("user")) = False Then
            Try
                Call GetSupplier()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboQuotationNumber)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("C030")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "C030")
        Dim dfrom As Date

        gs_Message = ""

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dtQuotationFrom.Value = dfrom
            dtQuotationTo.Value = Now
        End If
    End Sub

    Private Sub cboQuotationNumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboQuotationNumber.Callback
        Call FillComboQuotationNumber()
        If cboQuotationNumber.Items.Count > 1 Then
            'If there are more than 1 data, then set the default selected index (ALL)
            cboQuotationNumber.SelectedIndex = 0
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        Call GridLoad()
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Call GridLoad()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        Dim ErrMsg As String = ""
        Call ExcelGrid()

        If gs_Message <> "" Then
            DisplayMessage(MsgTypeEnum.ErrorMsg, gs_Message, Grid)
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        Response.Redirect("~/SupplierQuotationDetail.aspx")
    End Sub
#End Region

End Class
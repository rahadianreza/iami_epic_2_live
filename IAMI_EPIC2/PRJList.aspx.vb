﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.XtraPrinting
Imports System.IO

Public Class PRJList
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("I010")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            Dim dfrom As Date
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            ProjectDate_From.Value = dfrom
            ProjectDate_To.Value = Now
            cboProjectType.SelectedIndex = 0

            up_FillCombo(cboProjectType, statusAdmin, pUser, "ProjectType")
        End If
    End Sub
#End Region

#Region "Control Event"

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad()
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CustomButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonEventArgs) Handles Grid.CustomButtonInitialize
        If e.VisibleIndex = -1 Then
            Return
        End If

        Dim pErr As String = ""
        If (e.CellType = GridViewTableCommandCellType.Filter) Then
            Exit Sub
        End If

        Dim grid = TryCast(sender, ASPxGridView)
        Dim obj = grid.GetRowValues(e.VisibleIndex, "Project_ID")
        Dim fechado = obj IsNot Nothing AndAlso CBool(obj)
        Dim col = TryCast(grid.Columns(0), GridViewCommandColumn)

        Dim ds As DataSet = clsPRJListDB.CheckDatauploaded(obj, pErr)

        If Not ds Is Nothing Then
            If ds.Tables(0).Rows(0)(0).ToString <> 0 Then
                If e.ButtonID = "btnDeleteGrid" Then
                    e.Visible = DevExpress.Utils.DefaultBoolean.False
                End If

                If e.ButtonID = "detailList" Then
                    e.Visible = DevExpress.Utils.DefaultBoolean.True
                End If
            Else
                If e.ButtonID = "detailList" Then
                    e.Visible = DevExpress.Utils.DefaultBoolean.False
                End If
            End If
        End If

    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet

        Dim pFunction As String = e.Parameters
        Dim pFunctionFirst As String = Split(e.Parameters, "|")(0)

        If pFunction = "refresh" Then
            up_GridHeader("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        ElseIf pFunctionFirst.Trim = "delete" Then
            Dim pErr As String = ""

            clsPRJListDB.DeleteHeaderData(Split(e.Parameters, "|")(1), pUser, pErr)

            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
                up_GridLoad()
            End If
        Else
            If ProjectDate_From.Value > ProjectDate_To.Value Then
                cbMessage.JSProperties("cbMessage") = "Project Date To must be higher than Project Date From"
                Exit Sub
            End If
            up_GridLoad()
        End If
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Response.Redirect("AddPRJList.aspx?ID=" & e.NewValues("Project_ID"))
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("~/AddPRJList.aspx")
    End Sub

    Private Sub cbTmp_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbTmp.Callback
        Dim pFunction As String = e.Parameter

        Select Case pFunction
            Case "Code"

        End Select
    End Sub
#End Region

#Region "Procedure"
 
    Private Sub up_Excel()
        up_GridLoad()

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=PRList" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub up_FillCombo(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                            pAdminStatus As String, pUserID As String, _
                            Optional ByRef pGroup As String = "", _
                            Optional ByRef pParent As String = "", _
                            Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsPRListDB.FillCombo(pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_GridLoad()
        Dim ErrMsg As String = ""
        Dim dtFrom, dtTo As String
        Dim vPRType As String = ""
        Dim vProjectType As String = ""
        Dim vSection As String = ""
        Dim ls_Back As Boolean = False

        dtFrom = Format(ProjectDate_From.Value, "yyyy-MM-dd")
        dtTo = Format(ProjectDate_To.Value, "yyyy-MM-dd")

        If cboProjectType.Text <> "" Then
            vProjectType = Trim(cboProjectType.Value) 'cboDepartment.SelectedItem.GetValue("Code").ToString()
        End If

        up_GridHeader()

        Dim ds As New DataSet
        ds = clsPRJListDB.getHeaderGrid(dtFrom, dtTo, vProjectType, pUser, statusAdmin)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count = 0 Then
                up_GridHeader("1")
            End If
            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_GridHeader(Optional ByRef pLoad As String = "")
        Dim ErrMsg As String = ""
        Dim dtFrom, dtTo As String
        Dim vPRJType As String = ""
        Dim ls_Back As Boolean = False

        dtFrom = Format(ProjectDate_From.Value, "yyyy-MM-dd")
        dtTo = Format(ProjectDate_To.Value, "yyyy-MM-dd")

        If cboProjectType.Text <> "" Then
            vPRJType = Trim(cboProjectType.Text)
        End If

        Dim ds As New DataSet
        ds = clsPRJListDB.getHeaderGrid(dtFrom, dtTo, vPRJType, pUser, vStatus, ErrMsg)
    End Sub

#End Region

   
End Class
﻿Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class PO_ViewPRReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim UserID As String = ""
        Dim PR_Number As String = ""
        Dim UrgentStatus As String = ""

        UserID = Split(Request.QueryString("ID"), "|")(0)
        PR_Number = Split(Request.QueryString("ID"), "|")(1)
        UrgentStatus = Split(Request.QueryString("ID"), "|")(2)

        If UrgentStatus = "NO" Then
            up_LoadReportNotUrgent(PR_Number)
        Else
            up_LoadReportUrgent(PR_Number)
        End If


    End Sub

    Private Sub up_LoadReportNotUrgent(pPRNumber As String)
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptPRList

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRList_Report"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pPRNumber)

                Dim da As New SqlDataAdapter(cmd)

                da.Fill(ds)

                Report.DataSource = ds
                Report.Name = "PRReport_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report
                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Sub up_LoadReportUrgent(pPRNumber As String)
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptPRListUrgent

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRList_Report"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pPRNumber)

                Dim da As New SqlDataAdapter(cmd)

                da.Fill(ds)

                Report.DataSource = ds
                Report.Name = "PRReport_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report
                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim pPRNumber As String
        Dim pUserID As String
        pPRNumber = Split(Request.QueryString("ID"), "|")(1)
        pUserID = Split(Request.QueryString("ID"), "|")(0)

        Response.Redirect("~/PO_Notification.aspx?ID=" & pUserID & "|" & pPRNumber)

    End Sub
End Class
﻿Imports System.Drawing
Imports DevExpress.Web.ASPxGridView

Public Class GateSourcingSchedule
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String

    Dim prjid As String
    Dim grpid As String
    Dim comm As String
    Dim pErr As String = ""
    Dim pg As String = ""
    Dim py As String = ""
    Dim byr As String = ""
    Dim grpcomm As String = ""
    Dim pagetype As String = ""
#End Region

#Region "Initialize"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")

        Master.SiteTitle = "Gate Sourcing Schedule"
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        prjid = Request.QueryString("Project_Id")
        grpid = Request.QueryString("Group_Id")
        comm = Request.QueryString("comm")
        pg = Request.QueryString("page")
        py = Request.QueryString("project_type")
        byr = Request.QueryString("buyer")
        grpcomm = Request.QueryString("groupcommodity")
        pagetype = Request.QueryString("pagetype")

        '        If pg = "J010" Then
        sGlobal.getMenu(pg)
        'End If

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            up_GridLoad("")
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsGateICheckSheetDB.GetGridSourcingSchedule(prjid, grpid, comm, byr, pUser, grpcomm, statusAdmin)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        up_GridLoad("")
    End Sub

    Private Sub Grid_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        'If e.Column.FieldName = "Due_Date_Plan" Then
        '    e.Editor.Enabled = False
        'End If

    End Sub

    Private Sub grid_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")

        With e.DataColumn
            If e.GetValue("Code_Activity").ToString() = "CA001" Or e.GetValue("Code_Activity").ToString() = "CA002" Then
                e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
                e.Cell.Attributes.Add("readonly", "true")
                e.Cell.BackColor = Color.LemonChiffon

            End If

            If .FieldName = "Date_Actual" Then
                If Not IsDBNull(e.GetValue("Due_Date_Plan")) And Not IsDBNull(e.GetValue("Date_Actual")) Then
                    If DateDiff(DateInterval.Day, CDate(e.GetValue("Due_Date_Plan")), CDate(e.GetValue("Date_Actual"))) > 0 Then
                        e.Cell.BackColor = Color.Red
                    End If
                End If
            End If

        End With

    End Sub


    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.VisibleIndex < 0) Then
            Exit Sub
        End If



        If pg = "J010" Then
            If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Accepted" Then
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                    e.Visible = False
                End If
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Delete Then
                    e.Visible = False
                End If
            End If
        ElseIf pg = "J020" Then
            If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Completed" Then
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                    e.Visible = False
                End If
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Delete Then
                    e.Visible = False
                End If
            End If
        ElseIf pg = "J030" Then
            If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Confirmed" Then
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                    e.Visible = False
                End If
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Delete Then
                    e.Visible = False
                End If
            End If
        End If

        Dim CodeActivity = (TryCast(sender, ASPxGridView)).GetRowValues(e.VisibleIndex, "Code_Activity")
        Dim col As Object = (TryCast(sender, ASPxGridView)).GetRowValues(e.VisibleIndex, "Code_Activity")
        If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
            If CodeActivity.ToString() = "CA001" OrElse CodeActivity.ToString() = "CA002" Then
                e.Visible = False
                ' e.Column.CellStyle.BackColor = Color.LemonChiffon
            End If
        End If

        If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Delete Then
            If CodeActivity.ToString() = "CA001" OrElse CodeActivity.ToString() = "CA002" Then
                e.Visible = False

            End If
        End If

    End Sub

    Public Function checking(value As String) As Boolean
        If String.IsNullOrEmpty(value) Or value = "0" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles Grid.RowDeleting
        e.Cancel = True
        clsGateICheckSheetDB.DeleteSourcingSchedule(prjid, grpid, comm, byr, e.Keys("Code_Activity"), pUser, grpcomm, statusAdmin)
        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
        End If
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True

        Dim aFlag As Integer = 0

        If checking(prjid) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
        ElseIf checking(grpid) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
        ElseIf checking(comm) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Comodity cannot empty", 1)
        ElseIf checking(byr) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Buyer cannot empty", 1)
        ElseIf checking(e.Keys("Code_Activity")) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Code Activity cannot empty", 1)
        ElseIf checking(e.NewValues("Due_Date_Plan")) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Due Date Plan cannot empty", 1)
            'ElseIf checking(e.NewValues("Date_Actual")) = False Then
            '    aFlag = 0
            '    show_error(MsgTypeEnum.Warning, "Date Actual cannot empty", 1)
            'ElseIf checking(e.NewValues("Comment")) = False Then
            '    aFlag = 0
            '    show_error(MsgTypeEnum.Warning, "Comment cannot empty", 1)
        Else
            aFlag = 1
        End If

        If aFlag = 1 Then
            clsGateICheckSheetDB.InsertSourcingSchedule(prjid, grpid, comm, byr, e.Keys("Code_Activity"), e.NewValues("Due_Date_Plan"), e.NewValues("Date_Actual"), e.NewValues("Comment"), pUser, grpcomm, statusAdmin)
            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
            End If
        End If

    End Sub

#End Region

#Region "Control Event"
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        If pg = "J010" Then
            If pagetype <> "submit" Then
                Response.Redirect("GetCheckSheetView.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
            Else
                Response.Redirect("Get_I_Check_Sheet.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
            End If

            Response.Redirect("Get_I_Check_Sheet.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
        ElseIf pg = "J020" Then
            Response.Redirect("GetCheckSheetConfirm.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
        ElseIf pg = "J030" Then
            Response.Redirect("GetCheckSheetApproval.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
        Else
            'nothing
        End If
    End Sub
#End Region

End Class
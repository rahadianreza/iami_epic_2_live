﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxImageZoom
Imports DevExpress.Web.Data

Public Class AddComposition
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

    Private Sub up_LoadData(pSequenceNo As Integer)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim cCompositionMaster As New ClsCompositionMaster

        'Try
        cCompositionMaster.CompositionSequenceNo = pSequenceNo
        ds = ClsCompositionMasterDB.GetDataItem(cCompositionMaster, pErr)

        If pErr = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                txtSequence.Text = IIf(IsDBNull(ds.Tables(0).Rows(0)("CompositionSequenceNo")), 0, ds.Tables(0).Rows(0)("CompositionSequenceNo"))
                cboParentItem.SelectedIndex = cboParentItem.Items.IndexOf(cboParentItem.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("ParentItemCode") & "")))
                txtParentItemQty.Text = Trim(ds.Tables(0).Rows(0)("ParentItemQty") & "")
                cboParentItemUOM.SelectedIndex = cboParentItemUOM.Items.IndexOf(cboParentItemUOM.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("ParentItemUOM") & "")))
             

            End If
        Else
            cboParentItem.Text = ""
            txtParentItemQty.Text = ""
            cboParentItemUOM.Text = ""
           

        End If
    End Sub

    Private Function uf_CheckDataIsExist(pSequenceNo As Integer) As Boolean
        Dim pErr As String = ""
        Dim ds As New DataSet
        Dim cCompositionMaster As New ClsCompositionMaster
        cCompositionMaster.CompositionSequenceNo = pSequenceNo
        ds = ClsCompositionMasterDB.GetDataItem(cCompositionMaster, pErr)

        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub Show_Error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbSave.JSProperties("cpMessage") = ErrMsg
    End Sub

    Private Sub Show_Delete(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbDelete.JSProperties("cpMessage") = ErrMsg
    End Sub

    Private Sub Show_Clear(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbClear.JSProperties("cpMessage") = ErrMsg
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|A140", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnDelete.SetEnabled(false);" & vbCrLf & _
                         "btnSubmit.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "btnSubmit", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboParentItem)
        End If
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboParentItem)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A140")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user").ToString.ToUpper()
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A140")
        Dim itemcode As String
        itemcode = Request.QueryString("ID") & ""



        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If itemcode <> "" Then


                FillCombo(itemcode)
                btnClear.Text = "Cancel"

                txtSequence.Text = itemcode
                txtSequence.BackColor = Color.LightYellow
                up_LoadData(itemcode)
                txtSequence.ReadOnly = True
                btnDelete.Enabled = True

            Else
                txtSequence.Text = "NEW"

            End If

        End If

        cbSave.JSProperties("cpMessage") = ""
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        gs_Back = True
        Response.Redirect("~/CompositionList.aspx")

    End Sub

    Private Sub cbDelete_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDelete.Callback
        Dim CompositionMaster As New ClsCompositionMaster
        Dim MsgErr As String = ""

        CompositionMaster.CompositionSequenceNo = Request.QueryString("ID")
        Try
            Dim i As Integer
            'delete mst item
            i = ClsCompositionMasterDB.Delete(CompositionMaster, pUser)

            cbDelete.JSProperties("cpMessage") = "Data Deleted Successfully!"
        Catch ex As Exception
            Show_Delete(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub
    Private Sub FillCombo(pItemCode As String)
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = ClsCompositionMasterDB.GetDataItemParent(pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboParentItem.Items.Add(Trim(ds.Tables(0).Rows(i)("MaterialCategory") & ""))
            Next
        End If

        ds = ClsCompositionMasterDB.GetDataUOM(pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboParentItemUOM.Items.Add(Trim(ds.Tables(0).Rows(i)("UOM") & ""))
            Next
        End If
    End Sub

    Private Sub up_Save(Optional ByRef pErr As String = "")
        Dim CompositionMaster As New ClsCompositionMaster
        'Dim pAction = e.Parameter

        Try

            Dim i As Integer

            Dim ds As New DataSet
            If txtSequence.Text = "NEW" Then
                ds = ClsCompositionMasterDB.GenerateNewMaterialNo()
                If ds.Tables(0).Rows.Count > 0 Then
                    CompositionMaster.CompositionSequenceNo = ds.Tables(0).Rows(0)("SequenceNo")

                End If
            Else
                CompositionMaster.CompositionSequenceNo = Request.QueryString("ID")
            End If

            CompositionMaster.ParentItemCode = cboParentItem.SelectedItem.GetValue("Code").ToString()
            CompositionMaster.ParentItemQty = txtParentItemQty.Text
            CompositionMaster.ParentItemUOM = cboParentItemUOM.SelectedItem.GetValue("Code").ToString()


            Dim MsgErr As String = ""

            If Request.QueryString("ID") & "" = "" Then
                i = ClsCompositionMasterDB.Insert(CompositionMaster, pUser, MsgErr)
                If MsgErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
                    cbSave.JSProperties("cpItemCode") = CompositionMaster.CompositionSequenceNo
                Else
                    cbSave.JSProperties("cpMessage") = MsgErr
                End If
            Else
                i = ClsCompositionMasterDB.Update(CompositionMaster, pUser, MsgErr)
                If MsgErr = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                    cbSave.JSProperties("cpItemCode") = CompositionMaster.CompositionSequenceNo
                Else
                    cbSave.JSProperties("cpMessage") = MsgErr
                End If

            End If



        Catch ex As Exception
            Show_Error(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        If txtSequence.ReadOnly = False Then
            If uf_CheckDataIsExist(txtSequence.Text) = True Then
                cbSave.JSProperties("cpMessage") = "Data Is Already Exist!"
                Exit Sub
            End If
        End If
        Dim errmsg As String = ""

        up_Save(errmsg)
    End Sub

    Private Sub cbSave_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbSave.Callback
        ' txtItemCode.Text = "--NEW--"
    End Sub

    Private Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.Click
        If txtSequence.ReadOnly = False Then
            cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"
        Else
            cbClear.JSProperties("cpMessage") = "Data Cancel Successfully!"
            up_LoadData(txtSequence.Text)
        End If
    End Sub

End Class
﻿<%@ Page Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeBehind="Material_Chart.aspx.vb" 
Inherits="IAMI_EPIC2.Material_Chart" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPCallback" TagPrefix="d" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register assembly="DevExpress.XtraCharts.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
     <style type="text/css">
        .table-col-title01
        {
            width: 160px;                     
        }
        .table-col-control01
        {
            width: 110px;        
        }   
        .table-col-control02
        {
            width: 250px; 
        }   
        .table-col-title02
        {
            width: 180px;
        }   
        .table-col-control03
        {
            width: 180px;
        }   
        .table-height
        {
            height: 35px      
        }    
        .td-col-l 
        {
            padding:0px 0px 0px 10px;
            width:50px;
        }
        .td-col-m
        {
            width:20px;
        }
        .td-col-r
        {
            width:150px;
        }
        .td-col-f
        {
            width:10px;
        }
        .tr-height
        {
            height: 35px;
        }
        
        .widthPanelChart
        {
            width : 100%;
            height : 400px;
        }    
        
    </style>
    <script type="text/javascript">
        function LoadChart() {
            ASPxCallbackPanel1.PerformCallback('show');
        }
      
        function OnEndCallback(s, e) {
           // alert(s.cpMessage);
            if ((s.cpMessage == "There's no data to show!") && s.cpMessage !== undefined) {
               // alert(s.cpMessage);
                toastr.info(s.cpMessage, 'Info');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
              
            } else {
                delete s.cpMessage;
            }

        }

        function FilterGroupItem() {
            cboGroupItem.PerformCallback('filter|' + cboMaterialType.GetValue());
            cboGroupItemTo.PerformCallback('filter|' + cboMaterialType.GetValue());

            //group item to hanya untuk steel
            if ((cboMaterialType.GetValue() == 'LME' || cboMaterialType.GetValue() == 'PLASTIC') && cboMaterialType.GetValue() != '') {
//                cboGroupItemTo.SetEnabled(false);
                cboSupplier.SetEnabled(false);
            } else {
//                cboGroupItemTo.SetEnabled(true);
                cboSupplier.SetEnabled(true);
            }
        }

        //popup calender month year
        function formatDateISO(date) {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        function Period_From_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            Period_From.SetDate(myDate);
        }

        function Period_From_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();

                var startDate = formatDateISO(Period_From.GetDate());
                var endDate = formatDateISO(Period_To.GetDate());

                if (startDate > endDate) {
                    Message(2, 'Period from must be lower than period to!');
                    Period_From.Focus();
                    return false;
                }
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }
        }

        //period to
        function Period_To_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            Period_To.SetDate(myDate);
        }

        function Period_To_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();

                var startDate = formatDateISO(Period_From.GetDate());
                var endDate = formatDateISO(Period_To.GetDate());

                if (startDate > endDate) {
                    Message(2, 'Period from must be lower than period to!');
                    Period_From.Focus();
                    return false;
                }
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }
        }
        //end popup calender

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
 
   <%-- <div style="padding : 5px 5px 5px 5px">
       <dxchartsui:WebChartControl ID="WebChartControl1" runat="server">
        </dxchartsui:WebChartControl>
        
    </div>--%>
    <div style="padding: 5px 5px 5px 5px"> 
        <div style="padding: 5px 5px 5px 5px"> 
            <table runat="server" style="border: 1px solid; width: 100%">
                <tr style="height: 10px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-f">
                    </td>
                   
                    <td style="width:120px">
                    </td>
                    <td colspan="2">
                    </td>
                    <td></td>
                </tr>
                <tr style="height: 35px">
                    <td style="padding: 0px 0px 0px 10px; width: 80px">
                        <dx:aspxlabel id="lblPeriodFrom" runat="server" font-names="Segoe UI" font-size="9pt"
                            text="Period From" clientinstancename="lblPeriodFrom">
                        </dx:aspxlabel>
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-r">
                        <dx:ASPxDateEdit ID="Period_From" runat="server" ClientInstanceName="Period_From"
                            EnableTheming="True" ShowShadow="false" Font-Names="Segoe UI" Theme="Office2010Black"
                            Font-Size="9pt" Height="22px" DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy"
                            Width="120px">
                            <ClientSideEvents DropDown="Period_From_OnDropDown" Init="Period_From_OnInit" />
                        </dx:ASPxDateEdit>
                    </td>
                    
                    <td style="width:90px; padding-left:20px">
                        <dx:aspxlabel id="lblPeriodTo" runat="server" font-names="Segoe UI" font-size="9pt"
                            text="Period To" clientinstancename="lblPeriodTo">
                        </dx:aspxlabel>
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-r">
                        <dx:ASPxDateEdit ID="Period_To" runat="server" ClientInstanceName="Period_To" EnableTheming="True"
                            ShowShadow="false" Font-Names="Segoe UI" Theme="Office2010Black" Font-Size="9pt"
                            Height="22px" DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy" Width="120px">
                            <ClientSideEvents DropDown="Period_To_OnDropDown" Init="Period_To_OnInit" />
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr style="height: 35px">
                    <td style="padding: 0px 0px 0px 10px; width: 80px">
                        <dx:ASPxLabel ID="lblType" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Material Type"
                            ClientInstanceName="lblType">
                        </dx:ASPxLabel>
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-r">
                        <dx:ASPxComboBox ID="cboMaterialType" runat="server" ClientInstanceName="cboMaterialType"
                            Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            DataSourceID="SqlDataSource2" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                            Height="22px">
                            <ClientSideEvents SelectedIndexChanged="FilterGroupItem" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="select COALESCE(Par_Code,'') as Code,COALESCE(Par_Description,'') as Description from Mst_Parameter where Par_Group='materialtype'">
                        </asp:SqlDataSource>
                    </td>
                    <td colspan="5">
                    </td>
                </tr>
                <tr style="height: 35px">
                    <td style="padding: 0px 0px 0px 10px; width: 80px">
                        <dx:aspxlabel id="lblGroupItem1" runat="server" font-names="Segoe UI" font-size="9pt"
                            text="Group Item" clientinstancename="lblGroupItem1">
                        </dx:aspxlabel>
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-r">
                        <dx:ASPxComboBox ID="cboGroupItem" runat="server" ClientInstanceName="cboGroupItem"
                            Width="170px" Font-Names="Segoe UI" TextField="GroupItemDesc" ValueField="GroupItem"
                             TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                            Height="22px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="GroupItem" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="GroupItemDesc" Width="250px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="select coalesce(Par_Code,'') Code,coalesce(Par_description,'') Description from Mst_Parameter where Par_Group ='MaterialGroupItem'">
                        </asp:SqlDataSource>
                    </td>
                   
                    <td style="width:90px; padding-left:20px">
                        <dx:ASPxLabel ID="lblGroupTo" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Group Item To" ClientInstanceName="lblGroupTo">
                        </dx:ASPxLabel>
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-r">
                        <dx:ASPxComboBox ID="cboGroupItemTo" runat="server" ClientInstanceName="cboGroupItemTo"
                            Width="170px" Font-Names="Segoe UI" TextField="GroupItemDesc" ValueField="GroupItem"
                            TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                            Height="22px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="GroupItem" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="GroupItemDesc" Width="250px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="select coalesce(Par_Code,'') Code,coalesce(Par_description,'') Description from Mst_Parameter where Par_Group ='MaterialGroupItem'">
                        </asp:SqlDataSource>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr style="height: 35px">
                    <td style="padding: 0px 0px 0px 10px; width: 80px">
                        <dx:aspxlabel id="lblSupplier" runat="server" font-names="Segoe UI" font-size="9pt"
                            text="Supplier" clientinstancename="lblSupplier">
                        </dx:aspxlabel>
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-r">
                        <dx:ASPxComboBox ID="cboSupplier" runat="server" ClientInstanceName="cboSupplier"
                            Width="170px" Font-Names="Segoe UI" TextField="SupplierName" ValueField="SupplierCode"
                            DataSourceID="SqlDataSource1" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                            Height="22px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="SupplierCode" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="SupplierName" Width="250px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="SELECT SupplierCode,SupplierName FROM VW_Mst_Material_getSupplier">
                        </asp:SqlDataSource>
                    </td>
                     <td colspan="5">
                    </td>
                </tr>
                <tr class="tr-height">
                    <td class="td-col-l">
                        <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Currency">
                        </dx:ASPxLabel>
                    </td>
                    <td class="td-col-m">
                    </td>
                    <td class="td-col-r">
                        <dx:ASPxComboBox ID="cboCurrency" runat="server" ClientInstanceName="cboCurrency"
                            Width="170px" Font-Names="Segoe UI" TextField="CurrencyDesc" ValueField="Currency"
                            DataSourceID="SqlDataSource6" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                            Height="22px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Currency" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="CurrencyDesc" Width="250px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="SELECT RTRIM(Par_Code) Currency,RTRIM(Par_Description) CurrencyDesc FROM Mst_Parameter WHERE Par_Group = 'Currency'">
                        </asp:SqlDataSource>
                    </td>
                    <td>
                    </td>
                    <td colspan="4"></td>
                </tr>
                <tr style="height: 10px">
                    <td class="td-col-r" colspan="8">
                    </td>
                </tr>
                <tr class="tr-height">
                    <td style="padding: 0px 0px 0px 10px" colspan="6">
                        <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnBack" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;&nbsp;
                        <dx:ASPxButton ID="btnShow" runat="server" Text="Show" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShow" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
                                 if ( Period_From.GetValue() &gt; Period_To.GetValue()){
						                toastr.warning('Period To must greater than Period From', 'Warning');
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;
                                        e.processOnServer = false;        
					             } 
                                 if (cboMaterialType.GetText()==''){
                                     toastr.warning('Please select Material Type', 'Warning');
                                     toastr.options.closeButton = false;
                                     toastr.options.debug = false;
                                     toastr.options.newestOnTop = false;
                                     toastr.options.progressBar = false;
                                     toastr.options.preventDuplicates = true;
                                     toastr.options.onclick = null;
                                     e.processOnServer = false;
                                     return;
                                 }     
                                 
                                 if (cboGroupItem.GetText()==''){
                                     toastr.warning('Please select Group Item', 'Warning');
                                     toastr.options.closeButton = false;
                                     toastr.options.debug = false;
                                     toastr.options.newestOnTop = false;
                                     toastr.options.progressBar = false;
                                     toastr.options.preventDuplicates = true;
                                     toastr.options.onclick = null;
                                     e.processOnServer = false;
                                     return;
                                 }  
                                 
                                 if (cboCurrency.GetText()==''){
                                     toastr.warning('Please select Currency', 'Warning');
                                     toastr.options.closeButton = false;
                                     toastr.options.debug = false;
                                     toastr.options.newestOnTop = false;
                                     toastr.options.progressBar = false;
                                     toastr.options.preventDuplicates = true;
                                     toastr.options.onclick = null;
                                     e.processOnServer = false;
                                     return;
                                 }    
                                 
                                 var date1 = Period_From.GetDate();  
                                 var date2 = Period_To.GetDate(); // new Date('12/04/2019');

                                 var diff = date2 - date1;
                                 var oneDay = 1000 * 60 * 60 * 24;
                                 var day = Math.floor(diff / oneDay) / 365.2425;

                                 if (day.toFixed(1) &gt; 6.0) {
									 toastr.warning('Period out of Range ..! Maximum Period is 6 Years', 'Warning');
                                     toastr.options.closeButton = false;
                                     toastr.options.debug = false;
                                     toastr.options.newestOnTop = false;
                                     toastr.options.progressBar = false;
                                     toastr.options.preventDuplicates = true;
                                     toastr.options.onclick = null;
                                     e.processOnServer = false;
                                     return;
                                 }    
                                                                   
                                 ASPxCallbackPanel1.PerformCallback('show|' + cboMaterialType.GetValue()+ '|' + cboCurrency.GetValue());
                            }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td colspan="2">
                    </td>
                </tr>
                <tr style="height: 10px">
                    <td colspan="8">
                    </td>
                </tr>
            </table>
          </div>
    </div>
    <div style="padding: 5px 5px 5px 5px"> 
        <div style="padding: 5px 5px 5px 5px"> 
            <table runat="server" style="width:100%; border : 1px solid">
                <tr>
                    <td style="padding:5px 5px 5px 5px">
                        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" ClientInstanceName="ASPxCallbackPanel1" Width="960px"  Height="400px"
                            runat="server" CssClass="widthPanelChart" >
                                
                            <ClientSideEvents EndCallback="OnEndCallback"  />
                            <PanelCollection>
                        <dx:PanelContent runat="server"></dx:PanelContent>
                        </PanelCollection>
                    
                        </dx:ASPxCallbackPanel>
                       
                     </td>
                </tr>
            </table>
        </div>
    </div>
    
    
</asp:Content>


﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Parameter.aspx.vb" Inherits="IAMI_EPIC2.Parameter" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function OnTabChanging(s, e) {
            var tabName = (pageControl.GetActiveTab()).name;
            e.cancel = !ASPxClientEdit.ValidateGroup('group' + tabName);
        }

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
            <tr style="height: 50px">
                <td style=" width:100px; padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td style="width: 100px">                    
                    <dx:ASPxGridViewExporter ID="GridExporterDept" runat="server" GridViewID="GridDepart">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterSec" runat="server" GridViewID="GridSection">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterCC" runat="server" GridViewID="GridCostCenter">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterPRType" runat="server" GridViewID="GridPRType">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterGroupItem" runat="server" GridViewID="GridGroupItem">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterCat" runat="server" GridViewID="GridCategory">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterUOM" runat="server" GridViewID="GridUOM">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterCur" runat="server" GridViewID="GridCurr">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterSup" runat="server" GridViewID="GridSupplier">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterProv" runat="server" GridViewID="GridProvinsi">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterKab" runat="server" GridViewID="GridKab">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterLang" runat="server" GridViewID="GridLanguage">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterPeriod" runat="server" GridViewID="GridPeriod">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterJobPos" runat="server" GridViewID="GridJobPos">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterProjType" runat="server" GridViewID="GridProjectType">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterBank" runat="server" GridViewID="GridBank">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterMatPrice" runat="server" GridViewID="GridMaterialPriceType">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterMatType" runat="server" GridViewID="GridMaterialType">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterMatCode" runat="server" GridViewID="GridMaterialCode">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterMatGroup" runat="server" GridViewID="GridMaterialGroup">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterMatCat" runat="server" GridViewID="GridMaterialCategoryItem">
                    </dx:ASPxGridViewExporter>
                    
                    <dx:ASPxGridViewExporter ID="GridExporterOilType" runat="server" GridViewID="GridOilType">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterPRBudget" runat="server" GridViewID="GridPRBudget">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterRateClass" runat="server" GridViewID="GridRateClass">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterSectoral" runat="server" GridViewID="GridSectoral">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterVaKind" runat="server" GridViewID="GridVAKind">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridViewExporter ID="GridExporterProcess" runat="server" GridViewID="GridProcess">
                    </dx:ASPxGridViewExporter>
                    
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td align="left">
               
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td>
                    
				    
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'GroupItem'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Provinsi'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'PRType'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Section'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Department'">
                    </asp:SqlDataSource> 
					<asp:SqlDataSource ID="SqlDataSource6" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'RateClass'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) Code,RTRIM(Par_Description) Description FROM Mst_Parameter WHERE Par_Group = 'MaterialGroupItem'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) Code,RTRIM(Par_Description) Description FROM Mst_Parameter WHERE Par_Group = 'MaterialCode'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource9" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) Code,RTRIM(Par_Description) Description FROM Mst_Parameter WHERE Par_Group = 'MaterialType'">
                    </asp:SqlDataSource>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
           <%-- <tr>
                <td colspan="10" style=" width:100px; padding:0px 0px 0px 10px">
                    
                </td>
            </tr>
            <tr><td colspan="10"></td></tr>--%>
        </table>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <tr style="height: 50px">
            <dx:aspxpagecontrol id="pageControl" runat="server" 
            clientinstancename="pageControl" EnableTabScrolling="true"
                activetabindex="0" enablehierarchyrecreation="True"
                width="100%">
                <ClientSideEvents ActiveTabChanging="OnTabChanging" />
                <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                </ActiveTabStyle>
                <TabPages>                    
                    <dx:TabPage Name="Department" Text="Department">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl2" runat="server">
                                 <dx:ASPxGridView ID="GridDepart" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridDepart"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridDepart_RowValidating" OnStartRowEditing="GridDepart_StartRowEditing"
                                    OnRowInserting="GridDepart_RowInserting" OnRowDeleting="GridDepart_RowDeleting" 
                                    OnAfterPerformCallback="GridDepart_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" 
                                            ShowClearFilterButton="true" Width="60px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>

                                            <Settings AutoFilterCondition="Contains" />

                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                     <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header HorizontalAlign="Center">
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Section" Text="Section">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                 <dx:ASPxGridView ID="GridSection" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridSection"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridSection_RowValidating" OnStartRowEditing="GridSection_StartRowEditing"
                                    OnRowInserting="GridSection_RowInserting" OnRowDeleting="GridSection_RowDeleting" 
                                    OnAfterPerformCallback="GridSection_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true"
                                            ShowClearFilterButton="true" Width="60px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>

                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>
                                        <Settings AutoFilterCondition="Contains" />
                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataComboBoxColumn Caption="Department" FieldName="Parent_Code" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource5" TextField="Description" 
                                                ValueField="ID" Width="145px" TextFormatString="{1}" ClientInstanceName="Parent_Code"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="ID" Caption="ID" Width="35px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="100px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <Settings FilterMode="DisplayText"  /> 
                                                                                   
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="4" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="5" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="6" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="7" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                     <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header HorizontalAlign="Center">
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="CostCenter" Text="Cost Center">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridCostCenter" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridCostCenter"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridCostCenter_RowValidating" OnStartRowEditing="GridCostCenter_StartRowEditing"
                                    OnRowInserting="GridCostCenter_RowInserting" OnRowDeleting="GridCostCenter_RowDeleting" 
                                    OnAfterPerformCallback="GridCostCenter_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" 
                                            ShowClearFilterButton="true" Width="60px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" Visible="false" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataComboBoxColumn Caption="Department" FieldName="Parent_Code" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource5" TextField="Description" 
                                                ValueField="ID" Width="145px" TextFormatString="{1}" ClientInstanceName="Parent_Code"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="Contains">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="ID" Caption="ID" Width="35px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="100px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                           <Settings FilterMode="DisplayText"  />
                                        </dx:GridViewDataComboBoxColumn>
										
                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="4" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="5" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="6" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="7" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header HorizontalAlign="Center">
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="PRType" Text="PR Type">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridPRType" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridPRType"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black"                                                                        
                                    OnAfterPerformCallback="GridPeriod_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <SettingsDataSecurity AllowDelete="False" AllowInsert="False" 
                                        AllowEdit="False" />

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header HorizontalAlign="Center">
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="GroupItem" Text="Group Item">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridGroupItem" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridGroupItem"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridGroupItem_RowValidating" OnStartRowEditing="GridGroupItem_StartRowEditing"
                                    OnRowInserting="GridGroupItem_RowInserting" OnRowDeleting="GridGroupItem_RowDeleting" 
                                    OnAfterPerformCallback="GridGroupItem_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" VisibleIndex="1"
                                            Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                    
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="General_Name" Width="200px" Caption="Description"
                                            VisibleIndex="2">
                                            <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                                <Style HorizontalAlign="Left"></Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                                                               
                                        <dx:GridViewDataComboBoxColumn Caption="PR Type" FieldName="Parent_Code" 
                                            VisibleIndex="3"  Width="150px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="Description" 
                                                ValueField="ID" Width="145px" TextFormatString="{1}" ClientInstanceName="Parent_Code"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="Contains">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="ID" Caption="ID" Width="35px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="100px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <Settings FilterMode="DisplayText"  />
                       
                                        </dx:GridViewDataComboBoxColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="4" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="5" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="6" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="7" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="CategoryItem" Text="Category Item">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridCategory" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridCategory"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridCategory_RowValidating" OnStartRowEditing="GridCategory_StartRowEditing"
                                    OnRowInserting="GridCategory_RowInserting" OnRowDeleting="GridCategory_RowDeleting" 
                                    OnAfterPerformCallback="GridCategory_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                       
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataComboBoxColumn Caption="Group Item" FieldName="Parent_Code" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource1" TextField="Description" 
                                                ValueField="ID" Width="145px" TextFormatString="{1}" ClientInstanceName="Parent_Code"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith" >                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="ID" Caption="ID" Width="35px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="100px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                             <Settings FilterMode="DisplayText"  />
                                          
                       
                                        </dx:GridViewDataComboBoxColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="4" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="5" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="6" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="7" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="UOM" Text="UOM">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridUOM" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridUOM"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridUOM_RowValidating" OnStartRowEditing="GridUOM_StartRowEditing"
                                    OnRowInserting="GridUOM_RowInserting" OnRowDeleting="GridUOM_RowDeleting" 
                                    OnAfterPerformCallback="GridUOM_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Currency" Text="Currency">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridCurr" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridCurr"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridCurr_RowValidating" OnStartRowEditing="GridCurr_StartRowEditing"
                                    OnRowInserting="GridCurr_RowInserting" OnRowDeleting="GridCurr_RowDeleting" 
                                    OnAfterPerformCallback="GridCurr_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="SupplierType" Text="Supplier Type">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridSupplier" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridSupplier"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridSupplier_RowValidating" OnStartRowEditing="GridSupplier_StartRowEditing"
                                    OnRowInserting="GridSupplier_RowInserting" OnRowDeleting="GridSupplier_RowDeleting" 
                                    OnAfterPerformCallback="GridSupplier_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true"
                                            ShowClearFilterButton="true" Width="60px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header HorizontalAlign="Center">
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Provinsi" Text="Province">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridProvinsi" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridProvinsi"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridProvinsi_RowValidating" OnStartRowEditing="GridProvinsi_StartRowEditing"
                                    OnRowInserting="GridProvinsi_RowInserting" OnRowDeleting="GridProvinsi_RowDeleting" 
                                    OnAfterPerformCallback="GridProvinsi_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="KotaKabupaten" Text="City">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridKab" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridKab"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridKab_RowValidating" OnStartRowEditing="GridKab_StartRowEditing"
                                    OnRowInserting="GridKab_RowInserting" OnRowDeleting="GridKab_RowDeleting" 
                                    OnAfterPerformCallback="GridKab_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataComboBoxColumn Caption="Province" FieldName="Parent_Code" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="Description" 
                                                ValueField="ID" Width="145px" TextFormatString="{1}" ClientInstanceName="Parent_Code"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="ID" Caption="ID" Width="35px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="100px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                                
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <Settings FilterMode="DisplayText"  />
                       
                                        </dx:GridViewDataComboBoxColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="4" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="5" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="6" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="7" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Language" Text="Language">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridLanguage" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridLanguage"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridLanguage_RowValidating" OnStartRowEditing="GridLanguage_StartRowEditing"  
                                    OnRowInserting="GridLanguage_RowInserting" OnRowDeleting="GridLanguage_RowDeleting"                                   
                                    OnAfterPerformCallback="GridLanguage_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true"
                                            ShowClearFilterButton="true" Width="60px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header HorizontalAlign="Center">
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Period" Text="Period">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridPeriod" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridPeriod"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridPeriod_RowValidating" OnStartRowEditing="GridPeriod_StartRowEditing"  
                                    OnRowInserting="GridPeriod_RowInserting" OnRowDeleting="GridPeriod_RowDeleting"                                    
                                    OnAfterPerformCallback="GridPeriod_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true"
                                            ShowClearFilterButton="true" Width="60px">                                            
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header HorizontalAlign="Center">
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="JobPosition" Text="Job Position">
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridJobPos" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridJobPos"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridJobPos_RowValidating" OnStartRowEditing="GridJobPos_StartRowEditing"
                                    OnRowInserting="GridJobPos_RowInserting" OnRowDeleting="GridJobPos_RowDeleting" 
                                    OnAfterPerformCallback="GridJobPos_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
					<dx:TabPage Name="ProjectType" Text="Project Type">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridProjectType" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridProjectType"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridProjectType_RowValidating" OnStartRowEditing="GridProjectType_StartRowEditing"
                                    OnRowInserting="GridProjectType_RowInserting" OnRowDeleting="GridProjectType_RowDeleting" 
                                    OnAfterPerformCallback="GridProjectType_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="false" 
                                            ShowNewButtonInHeader="false" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                    <%--    <dx:GridViewDataComboBoxColumn Caption="PR Type" FieldName="Parent_Code" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="Description" 
                                                ValueField="ID" Width="145px" TextFormatString="{1}" ClientInstanceName="Parent_Code"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="Contains">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="ID" Caption="ID" Width="35px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="100px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                       
                                        </dx:GridViewDataComboBoxColumn>--%>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <%--Bank--%>
                    <dx:TabPage Name="Bank" Text="Bank"> 
                        <ContentCollection>
                            <dx:ContentControl runat="server">
                                <dx:ASPxGridView ID="GridBank" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridBank"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridBank_RowValidating" OnStartRowEditing="GridBank_StartRowEditing"
                                    OnRowInserting="GridBank_RowInserting" OnRowDeleting="GridBank_RowDeleting" 
                                    OnAfterPerformCallback="GridBank_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <%--Material Price Type--%>
                    <dx:TabPage Name="MaterialPriceType" Text="Material Price Type"> 
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl3" runat="server">
                                <dx:ASPxGridView ID="GridMaterialPriceType" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridMaterialPriceType"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridMaterialPriceType_RowValidating" OnStartRowEditing="GridMaterialPriceType_StartRowEditing"
                                    OnRowInserting="GridMaterialPriceType_RowInserting" OnRowDeleting="GridMaterialPriceType_RowDeleting" 
                                    OnAfterPerformCallback="GridMaterialPriceType_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="false" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl4" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <%--Material Type--%>
                    <dx:TabPage Name="MaterialType" Text="Material Type"> 
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl4" runat="server">
                                <dx:ASPxGridView ID="GridMaterialType" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridMaterialType"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridMaterialType_RowValidating" OnStartRowEditing="GridMaterialType_StartRowEditing"
                                    OnRowInserting="GridMaterialType_RowInserting" OnRowDeleting="GridMaterialType_RowDeleting" 
                                    OnAfterPerformCallback="GridMaterialType_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="false" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl4" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                     
                   
                     <%--Material Group--%>
                    <dx:TabPage Name="MaterialGroup" Text="Material Group Item"> 
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl6" runat="server">
                                <dx:ASPxGridView ID="GridMaterialGroup" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridMaterialGroup"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridMaterialGroup_RowValidating" OnStartRowEditing="GridMaterialGroup_StartRowEditing"
                                    OnRowInserting="GridMaterialGroup_RowInserting" OnRowDeleting="GridMaterialGroup_RowDeleting" 
                                    OnAfterPerformCallback="GridMaterialGroup_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Parent_Code" Width="200px" Caption="Material Type"
                                            VisibleIndex="3">
                                            <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Parent_Code">
                                                <Style HorizontalAlign="Left"></Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <EditFormSettings Visible="False" />
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataComboBoxColumn Caption="Material Type" FieldName="Parent_Code" 
                                            VisibleIndex="3" Width="0px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource9" TextField="Code" 
                                                ValueField="Code" Width="145px" TextFormatString="{0}" ClientInstanceName="Parent_Code"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="Code" Caption="Code" Width="70px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Description" Width="130px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                       
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="4" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="5" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="6" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="7" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl4" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                     <%--Material Group category--%>
                    <dx:TabPage Name="MaterialGroupCategory" Text="Material Category Item"> 
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl7" runat="server">
                                <dx:ASPxGridView ID="GridMaterialCategoryItem" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridMaterialCategoryItem"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridMaterialCategoryItem_RowValidating" OnStartRowEditing="GridMaterialCategoryItem_StartRowEditing"
                                    OnRowInserting="GridMaterialCategoryItem_RowInserting" OnRowDeleting="GridMaterialCategoryItem_RowDeleting" 
                                    OnAfterPerformCallback="GridMaterialCategoryItem_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>
                                        
                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Parent_Code" Width="200px" Caption="Material Group Item"
                                            VisibleIndex="3">
                                            <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Parent_Code">
                                                <Style HorizontalAlign="Left"></Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <EditFormSettings Visible="False" />
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataComboBoxColumn Caption="Material Group Item" FieldName="Parent_Code" 
                                            VisibleIndex="3" Width="0px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource7" TextField="Description" 
                                                ValueField="Code" Width="145px" TextFormatString="{1}" ClientInstanceName="Parent_Code"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="Code" Caption="Code" Width="35px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="100px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                       
                                        </dx:GridViewDataComboBoxColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="4" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="5" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="6" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="7" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl4" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                     <%--Oil Type--%>
                    <dx:TabPage Name="OilType" Text="Oil Type"> 
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl8" runat="server">
                                <dx:ASPxGridView ID="GridOilType" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridOilType"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridOilType_RowValidating" OnStartRowEditing="GridOilType_StartRowEditing"
                                    OnRowInserting="GridOilType_RowInserting" OnRowDeleting="GridOilType_RowDeleting" 
                                    OnAfterPerformCallback="GridOilType_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl4" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <%--PR Budget--%>
                    <dx:TabPage Name="PRBudget" Text="PR Budget"> 
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl10" runat="server">
                                <dx:ASPxGridView ID="GridPRBudget" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridPRBudget"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridPRBudget_RowValidating" OnStartRowEditing="GridPRBudget_StartRowEditing"
                                    OnRowInserting="GridPRBudget_RowInserting" OnRowDeleting="GridPRBudget_RowDeleting" 
                                    OnAfterPerformCallback="GridPRBudget_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl4" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <%--Rate Class--%>
                    <dx:TabPage Name="RateClass" Text="Rate Class"> 
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl11" runat="server">
                                <dx:ASPxGridView ID="GridRateClass" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridRateClass"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridRateClass_RowValidating" OnStartRowEditing="GridRateClass_StartRowEditing"
                                    OnRowInserting="GridRateClass_RowInserting" OnRowDeleting="GridRateClass_RowDeleting" 
                                    OnAfterPerformCallback="GridRateClass_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl4" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <%--Sectoral--%>
                    <dx:TabPage Name="Sectoral" Text="Sectoral"> 
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl12" runat="server">
                                <dx:ASPxGridView ID="GridSectoral" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridSectoral"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridSectoral_RowValidating" OnStartRowEditing="GridSectoral_StartRowEditing"
                                    OnRowInserting="GridSectoral_RowInserting" OnRowDeleting="GridSectoral_RowDeleting" 
                                    OnAfterPerformCallback="GridSectoral_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl4" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <%--VAKind--%>
                    <dx:TabPage Name="VAKind" Text="VAKind"> 
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl13" runat="server">
                                <dx:ASPxGridView ID="GridVAKind" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridVAKind"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridVAKind_RowValidating" OnStartRowEditing="GridVAKind_StartRowEditing"
                                    OnRowInserting="GridVAKind_RowInserting" OnRowDeleting="GridVAKind_RowDeleting" 
                                    OnAfterPerformCallback="GridVAKind_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataComboBoxColumn Caption="Rate Class" FieldName="Parent_Code" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource6" TextField="Description" 
                                                ValueField="ID" Width="145px" TextFormatString="{1}" ClientInstanceName="Parent_Code"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="ID" Caption="ID" Width="35px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="100px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <Settings FilterMode="DisplayText"  />
                       
                                        </dx:GridViewDataComboBoxColumn>
                                     <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl4" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                                   <%--Material Code--%>
                 
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Process" Text="Process">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControlProc" runat="server">
                                 <dx:ASPxGridView ID="GridProcess" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridProcess"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridProcess_RowValidating" OnStartRowEditing="GridProcess_StartRowEditing"
                                    OnRowInserting="GridProcess_RowInserting" OnRowDeleting="GridProcess_RowDeleting" 
                                    OnAfterPerformCallback="GridProcess_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="3" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="4" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                       <dx:TabPage Name="MaterialCode" Text="Material Code" Visible="false"> 
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl5" runat="server" Visible="false">
                                <dx:ASPxGridView ID="GridMaterialCode" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridMaterialCode"
                                    EnableTheming="True" KeyFieldName="General_Code" Theme="Office2010Black" 
                                    OnRowValidating="GridMaterialCode_RowValidating" OnStartRowEditing="GridMaterialCode_StartRowEditing"
                                    OnRowInserting="GridMaterialCode_RowInserting" OnRowDeleting="GridMaterialCode_RowDeleting" 
                                    OnAfterPerformCallback="GridMaterialCode_AfterPerformCallback" Width="100%" 
                                    Font-Names="Segoe UI" Font-Size="9pt" Visible="false">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn
                                            VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                                            ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="General_Code" 
                                            VisibleIndex="1" Width="120px">
                                            <PropertiesTextEdit MaxLength="15" Width="115px" 
                                                ClientInstanceName="General_Code">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>                                        
                                        <dx:GridViewDataTextColumn FieldName="General_Name" 
                                                            Width="200px" Caption="Description" VisibleIndex="2">
                                        <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="General_Name">
                                        <Style HorizontalAlign="Left"></Style>
                                        </PropertiesTextEdit>
                                        <Settings AutoFilterCondition="Contains" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        <Paddings PaddingLeft="5px"></Paddings>
                                        </HeaderStyle>

                                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Parent_Code" Width="200px" Caption="Material Category"
                                            VisibleIndex="3">
                                            <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Parent_Code">
                                                <Style HorizontalAlign="Left"></Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <EditFormSettings Visible="False" />
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
<%--                                        <dx:GridViewDataComboBoxColumn FieldName="Parent_Code" Caption="Material Category" 
                                            VisibleIndex="4" Width="0px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource9" TextField="Description" 
                                                ValueField="Code" Width="145px" TextFormatString="{1}" ClientInstanceName="Parent_Code"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                    <dx:listboxcolumn FieldName="Code" Caption="Code" Width="80px" />
                                                    <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="120px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                       
                                        </dx:GridViewDataComboBoxColumn>
--%>
                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            VisibleIndex="5" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            VisibleIndex="6" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            VisibleIndex="7" Width="150px">
                                            <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                                            </PropertiesTextEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            VisibleIndex="8" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                                            <Settings AllowAutoFilter="False" />
                                        </dx:GridViewDataDateColumn>

                                    </Columns>

                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                    <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm"/>
                                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                    <SettingsPopup>
                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                    </SettingsPopup>

                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header>
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                    
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl5" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                        runat="server">
                                                    </dx:ASPxGridViewTemplateReplacement>
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                    runat="server">
                                                </dx:ASPxGridViewTemplateReplacement>
                                            </div>
                                        </EditForm>
                                    </Templates>
                    
                                 </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
            </dx:aspxpagecontrol>
        </tr>
    </div>
</asp:Content>

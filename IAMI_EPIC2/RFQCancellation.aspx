﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RFQCancellation.aspx.vb" Inherits="IAMI_EPIC2.RFQCancellation" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function GetMessage(s, e) {

        if (s.cp_message == null || s.cp_message == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            btnExcel.SetEnabled(false);
        }
        else if (s.cp_message == "Grid Load Data Success") {
            btnExcel.SetEnabled(true);
        }
        else {
            toastr.warning(s.cp_message, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
    }


</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
            <tr style="height: 20px">
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:120px">
                  <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="RFQ Date Range">
                    </dx1:aspxlabel>                   
                </td>
                <td style="width:30px"></td>
                <td style="width:120px">
                <dx:aspxdateedit ID="dtDateFrom" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateFrom"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                        <ClientSideEvents ValueChanged="function(s, e) {
    Grid.PerformCallback('gridload' + '|9999-12-31|9999-12-31');
	btnExcel.SetEnabled(false);
}" />
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                </dx:aspxdateedit>                        
                </td>
                <td style="width:30px">
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="To">
                    </dx1:aspxlabel>                   
                </td>
                <td style="width:100px">
 <dx:aspxdateedit ID="dtDateTo" runat="server" Theme="Office2010Black" 
                                        Width="100px" ClientInstanceName="dtDateTo"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        EditFormat="Custom">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </HeaderStyle>
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                            </DayStyle>
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                            </WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                            </FooterStyle>
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                            </ButtonStyle>
                                        </CalendarProperties>
                                      
                                        <ClientSideEvents ValueChanged="function(s, e) {
	btnExcel.SetEnabled(false);
    Grid.PerformCallback('gridload' + '|9999-12-31|9999-12-31');
}" />
                                      
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                        </ButtonStyle>
                                    </dx:aspxdateedit>                   
                </td>
                <td></td>
                <td></td>
                <td>
                         <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                </td>
            </tr>     
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; " colspan="8">
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnRefresh" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {
	                        Grid.PerformCallback('gridload' + '|' + dtDateFrom.GetText() + '|' + dtDateTo.GetText());
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 &nbsp;<dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnExcel" Theme="Default">                        
                        <ClientSideEvents Init="function(s, e) {
	btnExcel.SetEnabled(false);
}" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 </td>
            </tr>     
            <tr style="height: 20px">
                <td style=" padding:0px 0px 0px 10px; width:120px">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>     
        </table>
    </div>

    <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" 
            ClientInstanceName="Grid" EnableTheming="True" Theme="Office2010Black" 
            Width="100%" Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="RFQ_Set;Rev" >

            <ClientSideEvents CustomButtonClick="function(s, e) {
		                        if(e.buttonID == 'edit'){
                     
                                var rowKey = Grid.GetRowKey(e.visibleIndex);
                             
	                            window.location.href= 'RFQCancellationDetail.aspx?ID=' + rowKey;
                            }
}" EndCallback="GetMessage" />

            <Columns>
                <dx:GridViewDataTextColumn Caption="RFQ Set Number" FieldName="RFQ_Set" FixedStyle="Left"
                    VisibleIndex="1" Width="200px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="RFQ Date" FieldName="RFQ_Date" FixedStyle="Left"
                    VisibleIndex="2" Width="100px">
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
					<Settings AllowAutoFilter="False" />									
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="PR Number" FieldName="PR_Number" 
                    VisibleIndex="4" Width="170px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="PR Date" FieldName="PR_Date" 
                    VisibleIndex="5" Width="100px">
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
					<Settings AllowAutoFilter="False" />									
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Deadline" FieldName="DeadLine" 
                    VisibleIndex="7">
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Department" FieldName="DepartmentName" 
                    VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Section" FieldName="SectionName" 
                    VisibleIndex="9" Width="170px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="PR Type" FieldName="PRTypeName" 
                    VisibleIndex="10" Width="150px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Project" FieldName="Project" 
                    VisibleIndex="11" Width="200px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Urgent" FieldName="Urgent_Status" 
                    VisibleIndex="12">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Tender" FieldName="TenderName" 
                    VisibleIndex="13">
                </dx:GridViewDataTextColumn>
				<%-- minta di comment 22/03/2019 --%>									
                <%--<dx:GridViewBandColumn VisibleIndex="15">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Approved By" VisibleIndex="0">
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Approved Date" VisibleIndex="2">
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn VisibleIndex="16">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Approved By" VisibleIndex="0">
                         <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Approved Date" VisibleIndex="2">
                         <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn VisibleIndex="17">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Approved By" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Approved Date" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn VisibleIndex="18">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Approved By" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Approved Date" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </dx:GridViewBandColumn>
               
                <dx:GridViewBandColumn VisibleIndex="19">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Approved Date" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Approved By" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>--%>
				 <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" FixedStyle="Left">
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dx:GridViewCommandColumn>														
                <dx:GridViewDataTextColumn Caption="Revision" FieldName="Rev" VisibleIndex="3" FixedStyle="Left">
                </dx:GridViewDataTextColumn>
            </Columns>

            <Settings ShowFilterRow="True" VerticalScrollableHeight="300" 
                HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>

                 <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
</EditFormColumnCaption>
                 </Styles>
        </dx:ASPxGridView>
    </div>


    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 0px; height: 100px;">  
            <tr style="height: 20px">
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
            <tr style="height: 20px">
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
            <tr style="height: 20px">
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
            <tr style="height: 20px">
                <td>&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>     
        </table>
    </div>
</div>
</asp:Content>

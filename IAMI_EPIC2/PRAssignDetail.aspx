﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PRAssignDetail.aspx.vb" Inherits="IAMI_EPIC2.PRAssignDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnLoad(s, e) {
           // alert(s.cpMessage);

            if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }

//        if (s.cpUrgent == '0') {
            txtUrgentNote.SetEnabled(false);
            dtReqPOIssue.SetEnabled(false);
            chkUrgent.SetEnabled(false);
//        }
//        else {
//            txtUrgentNote.SetEnabled(true);
//            dtReqPOIssue.SetEnabled(true);
//        }
    }

    function MessageBox(s, e) {
        //alert(s.cpMessage);

        if (s.cpMessage == "Data Has Been Assign Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            btnAssign.SetEnabled(false);
            cboPIC.SetEnabled(false);
            cboTender.SetEnabled(false);
            cboPICPO.SetEnabled(false);
            cboPICPO2.SetEnabled(false);
            cboPICPO3.SetEnabled(false);

        }
        else if (s.cpMessage == "Data Has Been Reject Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }


    }


    function tes(s, e) {
        //alert(chkUrgent.GetChecked());

        if (chkUrgent.GetChecked() == (true)) {
            //txtUrgentNote.SetText('');
            txtUrgentNote.SetEnabled(true);
            dtRequestPO.SetEnabled(true);
            //alert('yes');
        }
        if (chkUrgent.GetChecked() == (false)) {
            //txtUrgentNote.SetText('');
            txtUrgentNote.SetEnabled(false);
            dtRequestPO.SetEnabled(false);
            //alert('no');
        }
    }



</script>
    <style type="text/css">
        .style1
        {
            width: 210px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">  
            <tr style="height: 2px">
                <td></td>
                <td></td>
                <td class="style1"></td>
                <td></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>      
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:100px">
                    <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Number">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td class="style1">
           
            <dx1:ASPxTextBox ID="txtPRNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="200px" ClientInstanceName="txtPRNo" MaxLength="20" 
                        Height="25px" BackColor="LightGray" Font-Bold="True" ForeColor="Black" ReadOnly ="true"  >
            </dx1:ASPxTextBox>
           
                </td>
                <td>
                    <dx1:ASPxTextBox ID="txtRev" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="50px" ClientInstanceName="txtRev" MaxLength="20" 
                                Height="25px" BackColor="LightGray" ReadOnly="True" Font-Bold="True" 
                                ForeColor="Black" HorizontalAlign="Center" >
     
                    </dx1:ASPxTextBox>           
                </td>
                <td>&nbsp;</td>
                <td style="width:100px">
                    <dx1:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Department">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
        <dx1:ASPxComboBox ID="cboDepartment" runat="server" ClientInstanceName="cboDepartment"
                            Width="200px" Font-Names="Segoe UI" DataSourceID="SqlDataSource3" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" BackColor="LightGray" Enabled="False" ForeColor="Black" 
                        ReadOnly="True">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>   
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:100px">
                    <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Date">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td class="style1">
                <dx:ASPxDateEdit ID="dtDate" runat="server" Theme="Office2010Black" 
                                        Width="100px" AutoPostBack="false" ClientInstanceName="dtDate"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" 
                        BackColor="LightGray" Enabled="False" ForeColor="Black" ReadOnly="True">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                </td>
                <td></td>
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Section">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
        <dx1:ASPxComboBox ID="cboSection" runat="server" ClientInstanceName="cboSection"
                            Width="200px" Font-Names="Segoe UI" DataSourceID="SqlDataSource4" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" BackColor="LightGray" Enabled="False" ForeColor="Black" 
                        ReadOnly="True">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr> 
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:100px">
                    <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Budget">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td class="style1">
           
        <dx1:ASPxComboBox ID="cboPRBudget" runat="server" ClientInstanceName="cboPRBudget"
                            Width="200px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" BackColor="LightGray" Enabled="False" ForeColor="Black" 
                        ReadOnly="True">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td></td>
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Cost Center">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
        <dx1:ASPxComboBox ID="cboCostCenter" runat="server" ClientInstanceName="cboCostCenter"
                            Width="200px" Font-Names="Segoe UI" DataSourceID="SqlDataSource5" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" BackColor="LightGray" Enabled="False" ForeColor="Black" 
                        ReadOnly="True">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr> 
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:100px">
                    <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Type">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td class="style1">
           
        <dx1:ASPxComboBox ID="cboPRType" runat="server" ClientInstanceName="cboPRType"
                            Width="200px" Font-Names="Segoe UI" DataSourceID="SqlDataSource2" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px" BackColor="LightGray" Enabled="False" ForeColor="Black" 
                        ReadOnly="True">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td></td>
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Project">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
            <dx1:ASPxTextBox ID="txtProject" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="200px" ClientInstanceName="txtItemCode" MaxLength="20" 
                        Height="25px" BackColor="LightGray" ReadOnly="True" >
            </dx1:ASPxTextBox>
           
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr> 
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="style1">
                    &nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
        </table>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRBudget'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
            SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRType'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
            SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Department'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
            SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Section'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
            SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'CostCenter'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource6" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
            
            SelectCommand="Select RTRIM(UserID) As Code, RTRIM(UserName) As Description From UserSetup Where UserType = '3' and Job_Position in ('01','02')">
                    </asp:SqlDataSource>

                     <asp:SqlDataSource ID="SqlDataSource10" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
            
            SelectCommand="Select RTRIM(UserID) As Code, RTRIM(UserName) As Description From UserSetup Where PIC_PO = '1'">
                    </asp:SqlDataSource>

                     <asp:SqlDataSource ID="SqlDataSource11" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
            
            SelectCommand="Select RTRIM(UserID) As Code, RTRIM(UserName) As Description From UserSetup Where PIC_PO = '1'">
                    </asp:SqlDataSource>

                     <asp:SqlDataSource ID="SqlDataSource12" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
            
            SelectCommand="Select RTRIM(UserID) As Code, RTRIM(UserName) As Description From UserSetup Where PIC_PO = '1'">
                    </asp:SqlDataSource>

                    <asp:SqlDataSource ID="SqlDataSource7" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
            
            SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Tender'">
                    </asp:SqlDataSource>
    </div>

    <div style="padding: 5px 5px 5px 5px">
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="PRNo" >

             <Columns>
                 <dx:GridViewDataTextColumn Caption="Material No" VisibleIndex="1" 
                     FieldName="Material_No">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="2" FieldName="Description" Width="170px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Spesification" VisibleIndex="3" FieldName="Specification" Width="350px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Qty" VisibleIndex="4" FieldName="Qty">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="UOM" VisibleIndex="5" FieldName="UOMDescription">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Remarks" VisibleIndex="6" FieldName="Remarks" Width="250px">
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="No" VisibleIndex="0" FieldName="No" Width="40px">
                 </dx:GridViewDataTextColumn>
             </Columns>

                    <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                              <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
     
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" 
                        VerticalScrollableHeight="260" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>

            </dx:ASPxGridView>
    </div>

    <div style="padding: 5px 5px 5px 5px">
    <table style="width: 100%; border: 0px"> 
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>     
                    <dx:ASPxCallback ID="cbAssign" runat="server" 
                        ClientInstanceName="cbAssign">
                    <ClientSideEvents CallbackComplete="MessageBox" Init="OnLoad" />                                           
                </dx:ASPxCallback></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 30px">
                <td style=" padding:0px 0px 0px 10px; width:100px">         
                <dx:ASPxCheckBox ID="chkUrgent" runat="server" ClientInstanceName="chkUrgent" 
                    Text="Urgent" ValueChecked="1" ValueType="System.Int32" ValueUnchecked="0" 
                        Font-Names="Segoe UI" Font-Size="9pt" CheckState="Unchecked">
                    <ClientSideEvents CheckedChanged="tes" />
                    
                    </dx:ASPxCheckBox></td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Req. PO Issue Date">
                    </dx1:ASPxLabel>                 
                </td>
                <td>
                <dx:ASPxDateEdit ID="dtReqPOIssue" runat="server" Theme="Office2010Black" 
                                        Width="100px" AutoPostBack="false" ClientInstanceName="dtReqPOIssue"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 70px">
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Urgent Notes/Reason">
                    </dx1:ASPxLabel>                 
                </td>
                <td>
                    <dx1:ASPxMemo ID="txtUrgentNote" runat="server" Height="50px" Width="300px"
                        ClientInstanceName="txtUrgentNote" Font-Names="Segoe UI" Font-Size="9pt"
                        Theme="Metropolis" MaxLength="300">
                       
                    </dx1:ASPxMemo>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height:30px">
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PIC">
                    </dx1:ASPxLabel>                 
                </td>
                <td>
           
        <dx1:ASPxComboBox ID="cboPIC" runat="server" ClientInstanceName="cboPIC"
                            Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource6" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height:35px">
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PIC PO">
                    </dx1:ASPxLabel>                 
                </td>
                <td>
                <table> <tr>
                <td>
                 <dx1:ASPxComboBox ID="cboPICPO" runat="server" ClientInstanceName="cboPICPO"
                            Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource10" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
                </td>
                <td align=center style="width:15px;">
                    <dx1:ASPxLabel ID="ASPxLabel14" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="-">
                    </dx1:ASPxLabel>                 
                    </td>
                  <td>
                 <dx1:ASPxComboBox ID="cboPICPO2" runat="server" ClientInstanceName="cboPICPO2"
                            Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource11" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
                </td>
                 <td align=center style="width:15px;">
                    <dx1:ASPxLabel ID="ASPxLabel15" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="-">
                    </dx1:ASPxLabel>                 
                    </td>
                     <td>
                 <dx1:ASPxComboBox ID="cboPICPO3" runat="server" ClientInstanceName="cboPICPO3"
                            Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource12" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
                </td>
                </tr></table>
           
       
           
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 40px">
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel12" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Tender">
                    </dx1:ASPxLabel>                 
                </td>
                <td>
           
         <dx:ASPxComboBox ID="cboTender" runat="server" Font-Names="Segoe UI" 
                                Font-Size="9pt" Width="120px" ClientInstanceName="cboTender" 
                                Theme="Office2010Black" Height="25px">
                                <Items>
                                    <dx:ListEditItem Text="NO" Value="NO" />
                                    <dx:ListEditItem Text="YES" Value="YES" />                                    
                                </Items>
                            </dx:ASPxComboBox>
           
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 2px">
                <td style=" padding:0px 0px 0px 10px" colspan="3">

                    <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>

                    &nbsp;&nbsp;

                    <dx1:ASPxButton ID="btnAssign" runat="server" Text="Assign" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnAssign" Theme="Default" >                        
                     
                        <ClientSideEvents Click="function(s, e) {
                                
                                        if (cboPIC.GetText() == '') {
                                            toastr.warning('Please Input PIC!', 'Warning');
                                            cboPIC.Focus();
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
		                                    e.processOnServer = false;
                                            return;
	                                    }  
                                        else if (cboTender.GetText() == ''){    
                                            toastr.warning('Please Input Tender!', 'Warning');
                                            cboTender.Focus();
                                            toastr.options.closeButton = false;
                                            toastr.options.debug = false;
                                            toastr.options.newestOnTop = false;
                                            toastr.options.progressBar = false;
                                            toastr.options.preventDuplicates = true;
                                            toastr.options.onclick = null;
		                                    e.processOnServer = false;
                                            return;
                                        }

					if (cboPICPO.GetSelectedIndex() == -1 && cboPICPO2.GetSelectedIndex() == -1 && cboPICPO3.GetSelectedIndex() == -1) {
                toastr.warning('Please Select PIC PO !', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                cboPICPO.Focus();
                return;
            }
            else if (cboPICPO.GetSelectedIndex() == -1) {
                toastr.warning('Please Select PIC PO 1!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                cboPICPO.Focus();
                return;
            }
            else if (cboPICPO3.GetText() != '') {
                if (cboPICPO3.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid PIC PO 3!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    cboPICPO3.Focus();
                    return;
                }
                else if (cboPICPO2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid PIC PO 2!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    cboPICPO2.Focus();
                    return;
                }
                else if (cboPICPO3.GetText() == cboPICPO.GetText() || cboPICPO3.GetText() == cboPICPO2.GetText()) {
                    toastr.warning('Duplicate PIC PO!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboPICPO2.GetText() == cboPICPO.GetText()) {
                    toastr.warning('Duplicate PIC PO!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }

            else if (cboPICPO2.GetText() != '') {
                if (cboPICPO2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid PIC PO 2!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboPICPO2.GetText() == cboPICPO.GetText()) {
                    toastr.warning('Duplicate PIC PO!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }



	                            var msg = confirm('Are you sure want to assign this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }

                                cbAssign.PerformCallback(cboPIC.GetValue() + '|' + cboTender.GetValue());
                                }" />
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>


                    &nbsp;&nbsp;

                    <dx1:ASPxButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnSubmit" Theme="Default" Visible="False" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>


                    </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
            <tr style="height: 2px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>    
    </table>
    </div>
</div>

</asp:Content>
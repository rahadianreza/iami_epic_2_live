﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Material_Detail.aspx.vb" Inherits="IAMI_EPIC2.Material_Detail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //alert(s.cpButtonText);
            btnSubmit.SetText(s.cpButtonText);
            //cboMaterialType.SetEnabled(false);
            txtMaterialCode.SetEnabled(false);
            //txtMaterialName.SetEnabled(false);
            //cboGroupItem.SetEnabled(false);
            //cboCategory.SetEnabled(false);
        }
       
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/Material_List.aspx';
                if (pathArray[1] == "Material_Detail.aspx") {
                    window.location.href = window.location.origin + '/Material_List.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //cboMaterialType.SetEnabled(true);
            txtMaterialCode.SetEnabled(true);
            //txtMaterialName.SetEnabled(true);
            //cboGroupItem.SetEnabled(true);
            //cboCategory.SetEnabled(true);

            txtMaterialCode.SetText('');
            txtMaterialName.SetText('');
            cboMaterialType.SetText('');
            cboGroupItem.SetText('');
            cboCategory.SetText('');
            cboCountryCls.SetText('');
        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }

    
    //groupitem berdasarkan materialcode
    function FilterGroupItem() {
        cboGroupItem.PerformCallback('filter|' + cboMaterialType.GetValue());
    }

    //category berdasarkan groupitem
    function FilterCategory() {
        cboCategory.PerformCallback('filter|' + cboGroupItem.GetValue());
    }

    function SubmitProcess(s, e) {
        if (s.GetText() == 'Update') {
            if (txtMaterialName.GetText() == '') {
                toastr.warning('Please Input Material Name !', 'Warning');
                txtMaterialName.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (cboCountryCls.GetText() == '' ) {
                toastr.warning('Please Select Country !', 'Warning');
                cboCountryCls.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            if (cboMaterialType.GetText() == '') {
                toastr.warning('Please Select Material Type !', 'Warning');
                cboMaterialType.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            
            if (cboGroupItem.GetText() == '') {
                toastr.warning('Please Select Group Item !', 'Warning');
                cboGroupItem.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
           
        }
        else {
            if (cboMaterialType.GetText() == '') {
                toastr.warning('Please Select Material Type !', 'Warning');
                cboMaterialType.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            
            if (txtMaterialCode.GetText() == '') {
                toastr.warning('Please Input Material Code !', 'Warning');
                txtMaterialCode.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (txtMaterialName.GetText() == '') {
                toastr.warning('Please Input Material Name !', 'Warning');
                txtMaterialName.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (cboCountryCls.GetText() == '') {
                toastr.warning('Please Select Country !', 'Warning');
                cboCountryCls.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (cboGroupItem.GetText() == '') {
                toastr.warning('Please Select Group Item !', 'Warning');
                cboGroupItem.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
//            if (cboMaterialType.GetText() == '') {
//                if (cboCategory.GetText() == '') {
//                    toastr.warning('Please Select Category !', 'Warning');
//                    cboCategory.Focus();
//                    toastr.options.closeButton = false;
//                    toastr.options.debug = false;
//                    toastr.options.newestOnTop = false;
//                    toastr.options.progressBar = false;
//                    toastr.options.preventDuplicates = true;
//                    toastr.options.onclick = null;
//                    e.processOnServer = false;
//                    return;
//                }
//            }
 
        }

        if (btnSubmit.GetText() != 'Update') {
            cbSave.PerformCallback('Save|' + txtMaterialCode.GetText() + '|' + txtMaterialName.GetText() +  '|' + cboMaterialType.GetValue() + '|' + cboGroupItem.GetValue() + '|' + cboCategory.GetValue() +'|' + cboCountryCls.GetValue() + '|' + cboUOM.GetValue());
        } else {
            cbSave.PerformCallback('Update|' + txtMaterialCode.GetText() + '|' + txtMaterialName.GetText() + '|' + cboMaterialType.GetValue() + '|' + cboGroupItem.GetValue() + '|' + cboCategory.GetValue()+'|' + cboCountryCls.GetValue() + '|' + cboUOM.GetValue());
        }
    }

    </script>
    <style type="text/css">
        .td-col-l 
        {
            padding:0px 0px 0px 10px;
            width:120px;
        }
        .td-col-m
        {
            width:20px;
        }
        .td-col-r
        {
            width:200px;
        }
        
        .tr-height
        {
            height: 35px;
        }
            
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr style="height:10px">
            <td colspan="5"></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material Code">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtMaterialCode" runat="server" Width="170px" Height="25px" ClientInstanceName="txtMaterialCode"
                    MaxLength="50" AutoCompleteType="Disabled">
                    <ValidationSettings CausesValidation="false" Display="None">
                        <RequiredField IsRequired="true" ErrorText="*" />
                    </ValidationSettings>
                    
                </dx:ASPxTextBox>
            </td>
            <td></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material Type">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m">
                &nbsp;
            </td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboMaterialType" runat="server" ClientInstanceName="cboMaterialType"
                    Width="170px" Font-Names="Segoe UI" TextField="MaterialTypeDesc" ValueField="MaterialTypeCode"
                    DataSourceID="SqlDataSource1" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    
                    <ClientSideEvents SelectedIndexChanged="FilterGroupItem"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="MaterialTypeCode" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="MaterialTypeDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="select Par_Code AS MaterialTypeCode,Par_Description AS MaterialTypeDesc from Mst_Parameter where Par_Group='MaterialType'">
                </asp:SqlDataSource>
            </td>
            <td></td>
        </tr>
         <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Group Item">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px;" >
                <dx:ASPxComboBox ID="cboGroupItem" runat="server" ClientInstanceName="cboGroupItem" 
                    Width="170px" Font-Names="Segoe UI" TextField="GroupItemDesc" ValueField="GroupItem" 
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                     <ClientSideEvents SelectedIndexChanged="FilterCategory" />
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="GroupItem" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="GroupItemDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) GroupItem,RTRIM(Par_Description) GroupItemDesc FROM Mst_Parameter WHERE Par_Group = 'MaterialGroupItem'">
                </asp:SqlDataSource>
            </td>
            <td></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Category">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px;" >
                <dx:ASPxComboBox ID="cboCategory" runat="server" ClientInstanceName="cboCategory"
                    Width="170px" Font-Names="Segoe UI" TextField="CategoryDesc" ValueField="Category" 
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                   
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Category" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="CategoryDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) Category,RTRIM(Par_Description) CategoryDesc FROM Mst_Parameter WHERE Par_Group = 'MaterialGroupCategory'">
                </asp:SqlDataSource>
            </td>
            <td></td>
       </tr>
        
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material Name">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtMaterialName" runat="server" Width="250px" Height="25px" ClientInstanceName="txtMaterialName"
                    MaxLength="200" AutoCompleteType="Disabled">
                </dx:ASPxTextBox>
            </td>
            <td></td>
        </tr>
         <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Country Cls">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r" >
                <dx:ASPxComboBox ID="cboCountryCls" runat="server" ClientInstanceName="cboCountryCls"
                    Width="170px" Font-Names="Segoe UI" TextField="Country_Desc" ValueField="Country_Cls"
                    DataSourceID="SqlDataSource3" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Country_Cls" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Country_Desc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT Code Country_Cls,Description Country_Desc FROM dbo.VW_Mst_Material_CountryCls">
                </asp:SqlDataSource>
            </td>
            <td></td>
        </tr>
        
       
        
       <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material UOM">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; ">
                <dx:ASPxComboBox ID="cboUOM" runat="server" ClientInstanceName="cboUOM" DataSourceID="SqlDataSource4"
                    Width="170px" Font-Names="Segoe UI" TextField="UOMDesc" ValueField="UOM" 
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="UOM" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="UOMDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) UOM,RTRIM(Par_Description) UOMDesc FROM Mst_Parameter WHERE Par_Group = 'UOM'">
                </asp:SqlDataSource>
            </td>
            <td></td>
       </tr>
       <tr >
           <td class="td-col-l"></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>

       </tr>
        <tr style="height: 25px;">
            <td class="td-col-l"></td>
            <td class="td-col-l"></td>
            <td></td>
            <td style="width: 500px">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="false"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack"
                    Theme="Default">
                    <Paddings Padding="2px" />
                    <ClientSideEvents Click="function(s, e) {
                    var pathArray = window.location.pathname.split('/');
                    var url = window.location.origin + '/' + pathArray[1] + '/Material_List.aspx'
                    if (pathArray[1] == 'Material_Detail.aspx') {
                        window.location.href = window.location.origin + '/Material_List.aspx';
                    }
                    else {
                        window.location.href = url;
                    }          

                    
}" />
                </dx:ASPxButton>

                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                   
                    <ClientSideEvents Click="function(s, e) {
	var msg = confirm('Are you sure want to submit this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }
	SubmitProcess(s,e);
    
   
}" />
                   
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
              <%--  <dx:ASPxButton ID="btnUpdate" runat="server" Text="Update" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnUpdate" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>--%>

            </td>
            <td></td>
           
        </tr>
        <tr style="display:none">
            <td colspan="9">
               
            </td>
        </tr>
    </table>
    <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
           <%-- <ClientSideEvents CallbackComplete="SetCode" />--%>
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
            <ClientSideEvents  EndCallback="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
            <ClientSideEvents Init="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
           <%-- <ClientSideEvents CallbackComplete="MessageDelete" />--%>
        </dx:ASPxCallback>
    </div>
</asp:Content>

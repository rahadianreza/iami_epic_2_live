﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CPApprovalDetail.aspx.vb" Inherits="IAMI_EPIC2.CPApprovalDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    var currentColumnName;
    var dataExist;
    var dataSubmitted;


    function CountingLength(s, e) {
        //Counting lenght
        var memo = memoNote.GetText();
        var lenMemo = memo.length;
        lenMemo = lenMemo;
        if (lenMemo >= 300) {
            lenMemo = 300;
        }

        //Set Text
        lblLenght.SetText(lenMemo + "/300");
    }

    function SetInformation(s, e) {
        txtCPNumber.SetText(s.cpCPNumber);
        txtRev.SetText(s.cpRev);
        txtPRNumber.SetText(s.cpPRNumber);
        txtCENumber.SetText(s.cpCENumber);
        memoNote.SetText(s.cpNotes);
        //alert(s.cpApprovalNotes);
        txtNotesApproval.SetText(s.cpApprovalNotes);

        var dt = s.cpInvitationDate;
        var YearFrom = dt.getFullYear();
        var MonthFrom = dt.getMonth();
        var DateFrom = dt.getDate();
        var newdate = new Date(YearFrom, MonthFrom, DateFrom)
        //txtInvitationDate.SetText(newdate);
        txtInvitationDate.SetText(YearFrom + '-' + MonthFrom + '-' + DateFrom);
    }



    function OnBatchEditStartEditing(s, e) {
        e.cancel = true;
        Grid.batchEditApi.EndEdit();
    }

    function OnBatchEditEndEditing(s, e) {

    }

    function ShowConfirmOnLosingChanges() {
        var C = confirm("The data you have entered may not be saved. Are you sure want to leave?");
        if (C == true) {
            return true;
        } else {
            return false;
        }
    }

    function GridLoad(s, e) {
        window.setTimeout(function () {
            Grid.PerformCallback();
        }, 200);
    }



    function cbExistEndCallback(s, e) {
        if (s.cpParameter == "exist") {
            dataExist = s.cpResult;

        } else if (s.cpParameter == "IsSubmitted") {
            dataSubmitted = s.cpResult;

        } else if (s.cpParameter == "approve" || s.cpParameter == "reject") {
            if (s.cpType == "0") {
                //INFO
                toastr.info(s.cpMessage, 'Information');

            } else if (s.cpType == "1") {
                //SUCCESS
                toastr.success(s.cpMessage, 'Success');

            } else if (s.cpType == "2") {
                //WARNING
                toastr.warning(s.cpMessage, 'Warning');

            } else if (s.cpType == "3") {
                //ERROR
                toastr.error(s.cpMessage, 'Error');
            }
        }


        window.setTimeout(function () {
            delete s.cpParameter;
        }, 10);
    }



    function Back(s, e) {
        if (txtCPNumber.GetEnabled() == false) {
            if (Grid.batchEditApi.HasChanges() == true) {
                //there is an update data
                if (ShowConfirmOnLosingChanges() == false) {
                    e.processOnServer = false;
                    return false;
                }
            }

            if (hf.Get("hfNotes") != memoNote.GetText()) {
                if (ShowConfirmOnLosingChanges() == false) {
                    e.processOnServer = false;
                    return false;
                }
            }
        }
        //location.replace("CPList.aspx");
    }

    function Approve(s, e) {
        var C = confirm("Are you sure want to approve the data?");

        if (C == true) {
            cbExist.PerformCallback("approve");

            btnApprove.SetEnabled(false);
            btnReject.SetEnabled(false);
        }
    }

    function Reject(s, e) {
        //reject notes harus diisi
        if (txtNotesApproval.GetText() == '') {
            txtNotesApproval.Focus();
            toastr.warning("Please Input Reject Notes!", "Warning");
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return false;
        }


        
        var C = confirm("Are you sure want to reject the data?");

        if (C == true) {
            cbExist.PerformCallback("reject");

            btnApprove.SetEnabled(false);
            btnReject.SetEnabled(false);
        }
    }

    function Print(s, e) {

    }



    function LoadCompleted(s, e) {
//        if (s.cpType == "0") {
//            //INFO
//            toastr.info(s.cpMessage, 'Information');

//        } else if (s.cpType == "1") {
//            //SUCCESS
//            toastr.success(s.cpMessage, 'Success');

//        } else if (s.cpType == "2") {
//            //WARNING
//            toastr.warning(s.cpMessage, 'Warning');

//        } else if (s.cpType == "3") {
//            //ERROR
//            toastr.error(s.cpMessage, 'Error');
//        }
    }


    function GridLoadCompleted(s, e) {
        //Set header caption Quotation
        LabelSup1.SetText(s.cpHeaderCaption1);
        LabelSup2.SetText(s.cpHeaderCaption2);
        LabelSup3.SetText(s.cpHeaderCaption3);
        LabelSup4.SetText(s.cpHeaderCaption4);
        LabelSup5.SetText(s.cpHeaderCaption5);

        
        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');
        }

        window.setTimeout(function () {
            delete s.cpSaveData;
        }, 10);
    }
</script>

<style type="text/css">
    .colwidthbutton
    {
        width: 90px;
    }
    
    .rowheight
    {
        height: 35px;
    }
       
    .col1
    {
        width: 10px;
    }
    .colLabel1
    {
        width: 150px;
    }
    .colLabel2
    {
        width: 113px;
    }
    .colInput1
    {
        width: 220px;
    }
    .colInput2
    {
        width: 133px;
    }
    .colSpace
    {
        width: 50px;
    }
        
    .customHeader {
        height: 15px;
    }
    .hidden-div
    {
        display: none;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
            <tr class="rowheight">
                <td class="col1">
                    </td>
                <td class="colLabel1">
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Counter Proposal Number"></dx1:aspxlabel>
                </td>
                <td class="colInput1">
                    <dx:ASPxTextBox ID="txtCPNumber" runat="server" width="200px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtCPNumber">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                        <ClientSideEvents TextChanged="GridLoad" />
                    </dx:ASPxTextBox>
                </td> 
                <td style="width:35px">
                    <dx:ASPxTextBox ID="txtRev" runat="server" width="30px" Height="25px" Font-Size="9pt" 
                        ClientInstanceName="txtRev" BackColor="#CCCCCC" ReadOnly="true" 
                        HorizontalAlign="Center" TabIndex="-1">
                    </dx:ASPxTextBox>
                </td>
                <td class="colSpace">
                    </td>
                <td class="colLabel2">
                 <div class="hidden-div" >
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Invitation Date"></dx1:aspxlabel>
                    </div>
                </td>
                <td class="colInput2">
                 <div class="hidden-div" >
                    <dx:ASPxTextBox ID="txtInvitationDate" runat="server" width="120px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtInvitationDate">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                    </dx:ASPxTextBox>
                    </div>
                </td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <tr class="rowheight">
                <td>
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="PR Number"></dx1:aspxlabel>
                </td>
                <td colspan="2">
                    <dx:ASPxTextBox ID="txtPRNumber" runat="server" width="200px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtPRNumber">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                    </dx:ASPxTextBox>
                </td>
                <td>
                    </td>
                <td> 
                    <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="CE Number"></dx1:aspxlabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="txtCENumber" runat="server" width="200px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtCENumber">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                    </dx:ASPxTextBox>
                </td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <%--<tr>
                <td></td>
                <td colspan="8">
                    
                </td>
            </tr>--%>
            <tr>
                <td style="height: 10px">
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td> 
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                    &nbsp;
                    <dx:ASPxCallback ID="cbExist" runat="server" ClientInstanceName="cbExist">
                        <ClientSideEvents 
                            EndCallback="cbExistEndCallback"
                            CallbackComplete="LoadCompleted"
                        />
                    </dx:ASPxCallback>
                    &nbsp;
                    <dx:ASPxCallback ID="cb" runat="server" ClientInstanceName="cb">
                        <ClientSideEvents 
                            Init="GridLoad"
                        />
                    </dx:ASPxCallback>
                    &nbsp;
                    <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf">
                    </dx:ASPxHiddenField>
                </td>
            </tr>
        </table>
    </div>
   <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Material_No">            
            <Columns>
                <dx:GridViewDataTextColumn Caption="Material No." FieldName="Material_No"
                     VisibleIndex="0" Width="120px" FixedStyle="Left">
                     <HeaderStyle CssClass="customHeader" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" 
                     VisibleIndex="1" Width="300px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Specification" FieldName="Specification" 
                     VisibleIndex="2" Width="300px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Qty" FieldName="Qty" 
                     VisibleIndex="3" Width="60px">
                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Right"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="UOM" FieldName="UOM" 
                     VisibleIndex="4" Width="50px">
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Remarks" FieldName="Remarks" 
                     VisibleIndex="5" Width="120px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Currency" FieldName="Curr_Code" 
                     VisibleIndex="6" Width="70px">
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewBandColumn Caption="Quotation" Name="Quotation" 
                    VisibleIndex="7">
                    <Columns>
                        <dx:GridViewBandColumn Caption="Supplier 1" Name="Supplier1" VisibleIndex="0">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup1" ClientInstanceName="LabelSup1" runat="server" Text="Supplier 1" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs1" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice1" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 2" Name="Supplier2" VisibleIndex="1">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup2" ClientInstanceName="LabelSup2" runat="server" Text="Supplier 2" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs2" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice2" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 3" Name="Supplier3" VisibleIndex="2">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup3" ClientInstanceName="LabelSup3" runat="server" Text="Supplier 3" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs3" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice3" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 4" Name="Supplier4" VisibleIndex="3">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup4" ClientInstanceName="LabelSup4" runat="server" Text="Supplier 4" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs4" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice4" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 5" Name="Supplier5" VisibleIndex="4">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup5" ClientInstanceName="LabelSup5" runat="server" Text="Supplier 5" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs5" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice5" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="IAMI Proposal" Name="IAMIProposal" 
                    VisibleIndex="8">
                    <Columns>
                        <dx:GridViewDataSpinEditColumn Caption="Price/Pcs" FieldName="ProposalPricePcs" 
                             VisibleIndex="0" Width="100px">
                            <PropertiesSpinEdit DisplayFormatString="###,###" Width="95px" MaxLength="18">
                                <Style HorizontalAlign="Right"></Style>
                            </PropertiesSpinEdit>
                        </dx:GridViewDataSpinEditColumn>
                        <%--<dx:GridViewDataTextColumn Caption="Price/pc" FieldName="ProposalPricePcs" 
                                VisibleIndex="0" Width="100px">
                                <PropertiesTextEdit DisplayFormatString="###,###" Width="75px"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                        </dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataTextColumn Caption="Total Price" FieldName="ProposalTotalPrice" 
                                VisibleIndex="1" Width="100px">
                                <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                <CellStyle HorizontalAlign="Right"></CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>                
             </Columns>

            <Settings HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" VerticalScrollableHeight="135" ShowStatusBar="Hidden" />
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsBehavior AllowSelectByRowClick="True" AllowSort="False" AllowDragDrop="false" ColumnResizeMode="Control" />
            <SettingsEditing Mode="Batch">
                <BatchEditSettings StartEditAction="Click" ShowConfirmOnLosingChanges="False" />
            </SettingsEditing>

            <ClientSideEvents 
                EndCallback="GridLoadCompleted"
                BatchEditStartEditing="OnBatchEditStartEditing"
                BatchEditEndEditing="OnBatchEditEndEditing" 
            />

            <Styles Header-Paddings-Padding="1px" EditFormColumnCaption-Paddings-PaddingLeft="0px" EditFormColumnCaption-Paddings-PaddingRight="0px" >
                <Header CssClass="customHeader" HorizontalAlign="Center"></Header>
            </Styles>
        </dx:ASPxGridView>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
            <tr>
                <td class="col1"></td>
                <td colspan="8">
                    <dx1:aspxlabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Note :"></dx1:aspxlabel>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <dx:ASPxMemo ID="memoNote" runat="server" Height="45px" Width="100%" ClientInstanceName="memoNote" MaxLength="300" Enabled="false" RootStyle-Paddings-PaddingLeft="3px">
                        <ClientSideEvents KeyDown="CountingLength" LostFocus="CountingLength" />
                    </dx:ASPxMemo>
                </td>
                <td class="col1"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7" align="right">
                    <dx1:aspxlabel ID="lblLenght" runat="server" Font-Names="Segoe UI" Font-Size="9pt" ClientInstanceName="lblLenght" Text="0/300" Visible="false"></dx1:aspxlabel>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="col1"></td>
                <td colspan="8">
                    <dx1:aspxlabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Approval/Reject Notes :"></dx1:aspxlabel>
                </td>
            </tr>
            <tr style="height:20px">
                <td></td>
                <td colspan="7">
                    <dx:ASPxMemo ID="txtNotesApproval" runat="server" Height="45px" Width="100%" ClientInstanceName="txtNotesApproval" MaxLength="300" RootStyle-Paddings-PaddingLeft="3px">
                        <ClientSideEvents KeyDown="CountingLength" LostFocus="CountingLength" />
                    </dx:ASPxMemo>
                </td>
                <td class="col1"></td>
               
            </tr>
            <tr>
                <td></td>
                <td colspan="7" align="right">
                    <dx1:aspxlabel ID="Aspxlabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt" ClientInstanceName="lblLenght" Text="0/300" Visible="false"></dx1:aspxlabel>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <table>
                        <tr class="rowheight">
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                                    ClientInstanceName="btnBack" Theme="Default">
                                    <Paddings Padding="2px" />
                                    <ClientSideEvents Click="Back" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnApprove" runat="server" Text="Approve" 
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnApprove" Theme="Default">                        
                                    <ClientSideEvents Click="Approve" />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnReject" runat="server" Text="Reject" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnReject" Theme="Default">
                                    <ClientSideEvents Click="Reject" />                        
                                    <Paddings Padding="2px" />                                    
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnPrint" runat="server" Text="Print" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnPrint" Theme="Default">
                                    <ClientSideEvents Click="Print" />                        
                                    <Paddings Padding="2px" />                                    
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
        </table>
    </div>
</div>
</asp:Content>

﻿Imports DevExpress.Web.ASPxUploadControl
Imports System.IO

Public Class Menu
    Inherits System.Web.UI.Page

    Public AuthUpdate As Boolean = False
    Dim UserID As String
    Dim MenuId As String = "B010"
    Dim AdminStatus As String = ""

    Dim FilePath As String = ""
    Dim FileName As String = ""
    Dim FileExt As String = ""
    Dim Ext As String = ""
    Dim FolderPath As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu(MenuId)
        Master.SiteTitle = sGlobal.menuName
        UserID = Session("user") & ""
        show_error(MsgTypeEnum.Info, "", 0)
        AuthUpdate = sGlobal.Auth_UserUpdate(UserID, "B010")

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            Dim CatererID As String = ""
            Dim MenuType As String = ""
            Dim ActiveStatus As String = ""
            Dim User As Cls_UserSetup = Cls_UserSetup_DB.GetData(UserID)
            If User IsNot Nothing Then
                CatererID = User.CatererID

                AdminStatus = User.AdminStatus
            End If

            gridMenu.FocusedRowIndex = -1
            If CatererID <> "" Then
                cboCaterer.Text = CatererID
                txtCatererID.Text = CatererID
                Dim Cat As ClsCaterer = Cls_UserSetup_DB.GetCaterer(CatererID)
                If Cat IsNot Nothing Then
                    txtCatererName.Text = Cat.CatererName
                End If
                cboCaterer.ClientEnabled = False
            Else
                cboCaterer.ClientEnabled = True
            End If
            MenuType = cboFilterType.Value
            ActiveStatus = cboFilterActive.Value
            up_GridLoad(CatererID, MenuType, ActiveStatus)
            btnNew.Enabled = AuthUpdate
            btnSave.Enabled = AuthUpdate
            btnDelete.Enabled = AuthUpdate
            btnClear.Enabled = AuthUpdate
            uploader1.Enabled = AuthUpdate
        End If
    End Sub

    Private Sub up_GridLoad(pCatererID As String, pType As String, pActive As String)
        Dim pErr As String = ""
        Dim ds As List(Of clsMenu)
        ds = clsMenuDB.GetList(pCatererID, pType, pActive)
        If pErr = "" Then
            gridMenu.DataSource = ds
            gridMenu.DataBind()
            If ds Is Nothing Or ds.Count = 0 Then
                show_error(MsgTypeEnum.Warning, "Data is not found!")
            End If
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr)
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        gridMenu.JSProperties("cp_message") = ErrMsg
        gridMenu.JSProperties("cp_type") = msgType
        gridMenu.JSProperties("cp_val") = pVal
    End Sub

    Private Sub show_save(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbkSave.JSProperties("cp_message") = ErrMsg
        cbkSave.JSProperties("cp_type") = msgType
        cbkSave.JSProperties("cp_val") = pVal
    End Sub

    Private Sub show_delete(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbkDelete.JSProperties("cp_message") = ErrMsg
        cbkDelete.JSProperties("cp_type") = msgType
        cbkDelete.JSProperties("cp_val") = pVal
    End Sub

    Private Sub gridMenu_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles gridMenu.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" And e.CallbackName <> "CUSTOMCALLBACK" Then
            up_GridLoad(cboCaterer.Text, cboFilterType.Value, cboFilterActive.Value)
        End If
    End Sub

    Private Sub gridMenu_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles gridMenu.CustomCallback
        Try
            Dim Idx As String = Split(e.Parameters, "|")(0)
            If Idx = "save" Then
                Dim CatererID As String = Split(e.Parameters, "|")(1)
                Dim MenuID As String = Split(e.Parameters, "|")(2)
                Dim MenuName As String = Split(e.Parameters, "|")(3)
                Dim Calorie As String = Split(e.Parameters, "|")(4)
                Dim Price As String = Split(e.Parameters, "|")(5)
                Dim TaxCode As String = Split(e.Parameters, "|")(6)
                Dim Description As String = Split(e.Parameters, "|")(7)
                Dim Active As String = Split(e.Parameters, "|")(8)
                Dim pPicture As String = Split(e.Parameters, "|")(9)
                Dim pPOS As String = Split(e.Parameters, "|")(10)
                Dim pCategory As String = Split(e.Parameters, "|")(11)
                Dim Menu As New clsMenu
                Menu.CatererID = CatererID
                Menu.MenuID = MenuID
                Menu.MenuName = MenuName
                Menu.Calorie = Calorie
                Menu.Price = Val(Price)
                Menu.MenuCategory = pCategory
                Menu.TaxCode = TaxCode
                Menu.Description = Description
                Menu.ActiveStatus = Active
                Menu.MenuType = pPOS
                Menu.CreateUser = Session("user") + ""

                If Menu.MenuID = "(NEW)" Or Menu.MenuID = "" Then
                    clsMenuDB.Insert(Menu)
                    show_error(MsgTypeEnum.Success, "Insert data successful")
                Else
                    clsMenuDB.Update(Menu)
                    show_error(MsgTypeEnum.Success, "Update data successful")
                End If
            ElseIf Idx = "load" Then
                Dim CatererID As String = Split(e.Parameters, "|")(1)
                Dim MenuType As String = Split(e.Parameters, "|")(2)
                Dim ActiveStatus As String = Split(e.Parameters, "|")(3)
                Call up_GridLoad(CatererID, MenuType, ActiveStatus)
                show_error(MsgTypeEnum.Info, "", 0)
            ElseIf Idx = "delete" Then
                Try
                    Dim CatererID As String = Split(e.Parameters, "|")(1)
                    Dim MenuID As String = Split(e.Parameters, "|")(2)
                    clsMenuDB.Delete(CatererID, MenuID)
                    show_error(MsgTypeEnum.Info, "Delete data successful")
                Catch ex As Exception
                    show_save(MsgTypeEnum.ErrorMsg, ex.Message)
                End Try
            ElseIf Idx = "afterdelete" Then
                Dim CatererID As String = Split(e.Parameters, "|")(1)
                Dim MenuType As String = Split(e.Parameters, "|")(2)
                Dim ActiveStatus As String = Split(e.Parameters, "|")(3)
                Call up_GridLoad(CatererID, MenuType, ActiveStatus)
                show_error(MsgTypeEnum.Success, "Delete data successful")
            ElseIf Idx = "clear" Then
                Dim CatererID As String = Split(e.Parameters, "|")(1)
                Dim MenuID As String = Split(e.Parameters, "|")(2)
                clsMenuDB.ClearImage(CatererID, MenuID)
            End If
        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub

    Protected Sub Uploader_FileUploadComplete(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)
        Try
            e.CallbackData = SavePostedFiles(e.UploadedFile)
        Catch ex As Exception
            e.IsValid = False
        End Try
    End Sub

    Private Function SavePostedFiles(ByVal uploadedFile As UploadedFile) As String
        If (Not uploadedFile.IsValid) Then
            Return String.Empty
        End If
        'FileName = Path.GetFileName(Uploader.PostedFile.FileName)
        'FileExt = Path.GetExtension(Uploader.PostedFile.FileName)
        'FilePath = Server.MapPath("\Import\" & FileName)
        'Uploader.SaveAs(FilePath)

        Ext = Path.Combine(MapPath(""))
        'FileName = Uploader.PostedFile.FileName
        FilePath = Ext & "\Import\" & FileName
        uploadedFile.SaveAs(FilePath)

        Return FilePath
    End Function

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim PostedFile As HttpPostedFile = uploader1.PostedFile
        Dim FileName As String = Path.GetFileName(PostedFile.FileName)
        Dim FileExtension As String = Path.GetExtension(FileName)
        Dim Stream As Stream = PostedFile.InputStream
        Dim Br As BinaryReader = New BinaryReader(Stream)
        Dim Bytes As Byte() = Br.ReadBytes(Stream.Length)

        Dim Menu As New clsMenu
        Menu.CatererID = cboCaterer.Text
        Menu.MenuID = txtMenuID.Text
        Menu.MenuName = txtMenuName.Text
        Menu.Calorie = txtCalorie.Text
        Menu.Price = Val(txtPrice.Text.Replace(",", "."))
        Menu.TaxCode = cboTax.Text
        Menu.Description = txtDesc.Text
        Menu.MenuCategory = cboCategory.Value
        If Bytes.Length > 0 Then
            Menu.Picture = Bytes
        End If
        Menu.MenuType = cboType.Value
        Menu.ActiveStatus = chkActive.Value
        Menu.RecommendedStatus = chkSpecial.Value
        Menu.SpicyStatus = chkSpicy.Value
        Menu.CreateUser = Session("user")
        Dim i As Integer = clsMenuDB.Update(Menu)
        If i = 0 Then
            clsMenuDB.Insert(Menu)
        End If

        txtMenuID.Text = ""
        txtMenuName.Text = ""
        txtCalorie.Text = ""
        txtPrice.Text = ""
        cboTax.Text = ""
        txtDesc.Text = ""
        cboCategory.Text = ""
        chkSpecial.Checked = False
        chkSpicy.Checked = False

        gridMenu.FocusedRowIndex = -1
        cboCaterer.Text = Menu.CatererID
        Dim Cat As ClsCaterer = Cls_UserSetup_DB.GetCaterer(Menu.CatererID)
        If Cat IsNot Nothing Then
            txtCatererName.Text = Cat.CatererName
        End If
        txtCatererID.Text = Menu.CatererID
        txtMenuID.Text = "(NEW)"

        Dim User As Cls_UserSetup = Cls_UserSetup_DB.GetData(UserID)
        If User IsNot Nothing Then
            AdminStatus = User.AdminStatus
        End If
        If AdminStatus = "1" Then
            cboCaterer.ClientEnabled = True
        Else
            cboCaterer.ClientEnabled = False
        End If
        Dim MenuType As String = cboFilterType.Value
        Dim ActiveStatus As String = cboFilterActive.Value
        up_GridLoad(Menu.CatererID, MenuType, ActiveStatus)
        show_save(MsgTypeEnum.Success, "Save data successful")
    End Sub

    Private Sub cbkDelete_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbkDelete.Callback
        Try
            Dim CatererID As String = Split(e.Parameter, "|")(1)
            Dim MenuID As String = Split(e.Parameter, "|")(2)
            clsMenuDB.Delete(CatererID, MenuID)
            show_delete(MsgTypeEnum.Success, "")
        Catch ex As Exception
            show_delete(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub
End Class
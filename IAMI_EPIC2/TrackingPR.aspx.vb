﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class TrackingPR
#Region "Declaration"
    Inherits System.Web.UI.Page
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Procedure"
    Private Sub up_LoadGrid()
        Dim ErrMsg As String = ""
        Dim dtFrom = Format(dtDateFrom.Value, "yyyy-MM-dd")
        Dim dtTo = Format(dtDateTo.Value, "yyyy-MM-dd")

        Dim Tracking As DataSet
        Tracking = clsTrackingPRDB.GetList(dtFrom, dtTo, Session("user"), ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Tracking
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
            Exit Sub
        End If
    End Sub

    Private Sub up_LoadExcel(Optional ByRef pErr As String = "")
        up_LoadGrid()
        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=TrackingPurchaseRequest_" & Format(CDate(Now), "yyyyMMdd_hhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using

        ps.Dispose()
        'Try
        '    Dim fi As New FileInfo(Server.MapPath("~\Download\Tracking Purchase Request.xlsx"))
        '    If fi.Exists Then
        '        fi.Delete()
        '        fi = New FileInfo(Server.MapPath("~\Download\Tracking Purchase Request.xlsx"))
        '    End If
        '    Dim exl As New ExcelPackage(fi)
        '    Dim ws As ExcelWorksheet
        '    ws = exl.Workbook.Worksheets.Add("Control Sheet")
        '    ws.View.ShowGridLines = False
        '    With ws
        '        .Cells(4, 2, 4, 23).Style.Font.Bold = True

        '        .Cells(4, 2, 4, 2).Value = "NO"
        '        .Cells(4, 2, 4, 2).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 2, 4, 2).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 2, 4, 2).Style.Font.Size = 10
        '        .Cells(4, 2, 4, 2).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 3, 4, 3).Value = "DEPARTMENT"
        '        .Cells(4, 3, 4, 3).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 3, 4, 3).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 3, 4, 3).Style.Font.Size = 10
        '        .Cells(4, 3, 4, 3).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 4, 4, 4).Value = "SECTION"
        '        .Cells(4, 4, 4, 4).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 4, 4, 4).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 4, 4, 4).Style.Font.Size = 10
        '        .Cells(4, 4, 4, 4).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 5, 4, 5).Value = "PROJECT"
        '        .Cells(4, 5, 4, 5).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 5, 4, 5).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 5, 4, 5).Style.Font.Size = 10
        '        .Cells(4, 5, 4, 5).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 6, 4, 6).Value = "PURCHASE REQUEST NO"
        '        .Cells(4, 6, 4, 6).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 6, 4, 6).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 6, 4, 6).Style.Font.Size = 10
        '        .Cells(4, 6, 4, 6).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 7, 4, 7).Value = "PR TYPE"
        '        .Cells(4, 7, 4, 7).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 7, 4, 7).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 7, 4, 7).Style.Font.Size = 10
        '        .Cells(4, 7, 4, 7).Style.Font.Name = "Segoe UI"


        '        .Cells(4, 8, 4, 8).Value = "ITEM DESCRIPTION"
        '        .Cells(4, 8, 4, 8).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 8, 4, 8).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 8, 4, 8).Style.Font.Size = 10
        '        .Cells(4, 8, 4, 8).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 9, 4, 9).Value = "SPECIFICATION"
        '        .Cells(4, 9, 4, 9).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 9, 4, 9).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 9, 4, 9).Style.WrapText = True
        '        .Cells(4, 9, 4, 9).Style.Font.Size = 10
        '        .Cells(4, 9, 4, 9).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 10, 4, 10).Value = "QTY"
        '        .Cells(4, 10, 4, 10).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 10, 4, 10).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 10, 4, 10).Style.WrapText = True
        '        .Cells(4, 10, 4, 10).Style.Font.Size = 10
        '        .Cells(4, 10, 4, 10).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 11, 4, 11).Value = "PIC USER"
        '        .Cells(4, 11, 4, 11).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 11, 4, 11).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 11, 4, 11).Style.WrapText = True
        '        .Cells(4, 11, 4, 11).Style.Font.Size = 10
        '        .Cells(4, 11, 4, 11).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 12, 4, 12).Value = "PIC COST CONTROL"
        '        .Cells(4, 12, 4, 12).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 12, 4, 12).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 12, 4, 12).Style.WrapText = True
        '        .Cells(4, 12, 4, 12).Style.Font.Size = 10
        '        .Cells(4, 12, 4, 12).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 13, 4, 13).Value = "PURCHASE REQUEST"
        '        .Cells(4, 13, 4, 13).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 13, 4, 13).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 13, 4, 13).Style.WrapText = True
        '        .Cells(4, 13, 4, 13).Style.Font.Size = 10
        '        .Cells(4, 13, 4, 13).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 14, 4, 14).Value = "COST ESTIMATION"
        '        .Cells(4, 14, 4, 14).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 14, 4, 14).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 14, 4, 14).Style.WrapText = True
        '        .Cells(4, 14, 4, 14).Style.Font.Size = 10
        '        .Cells(4, 14, 4, 14).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 15, 4, 15).Value = "IA BUDGET"
        '        .Cells(4, 15, 4, 15).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 15, 4, 15).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 15, 4, 15).Style.WrapText = True
        '        .Cells(4, 15, 4, 15).Style.Font.Size = 10
        '        .Cells(4, 15, 4, 15).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 16, 4, 16).Value = "COUNTER PROPOSAL"
        '        .Cells(4, 16, 4, 16).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 16, 4, 16).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 16, 4, 16).Style.WrapText = True
        '        .Cells(4, 16, 4, 16).Style.Font.Size = 10
        '        .Cells(4, 16, 4, 16).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 17, 4, 17).Value = "IA PRICE"
        '        .Cells(4, 17, 4, 17).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 17, 4, 17).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 17, 4, 17).Style.Font.Size = 10
        '        .Cells(4, 17, 4, 17).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 18, 4, 18).Value = "PO"
        '        .Cells(4, 18, 4, 18).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 18, 4, 18).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 18, 4, 18).Style.Font.Size = 10
        '        .Cells(4, 18, 4, 18).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 19, 4, 19).Value = "GOOD RECEIPT"
        '        .Cells(4, 19, 4, 19).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 19, 4, 19).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 19, 4, 19).Style.Font.Size = 10
        '        .Cells(4, 19, 4, 19).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 20, 4, 20).Value = "PO CREATED BY"
        '        .Cells(4, 20, 4, 20).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 20, 4, 20).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 20, 4, 20).Style.Font.Size = 10
        '        .Cells(4, 20, 4, 20).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 21, 4, 21).Value = "PO NO"
        '        .Cells(4, 21, 4, 21).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 21, 4, 21).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 21, 4, 21).Style.Font.Size = 10
        '        .Cells(4, 21, 4, 21).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 22, 4, 22).Value = "GR CREATED BY"
        '        .Cells(4, 22, 4, 22).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 22, 4, 22).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 22, 4, 22).Style.Font.Size = 10
        '        .Cells(4, 22, 4, 22).Style.Font.Name = "Segoe UI"

        '        .Cells(4, 23, 4, 23).Value = "GR NO"
        '        .Cells(4, 23, 4, 23).Style.HorizontalAlignment = HorzAlignment.Far
        '        .Cells(4, 23, 4, 23).Style.VerticalAlignment = VertAlignment.Center
        '        .Cells(4, 23, 4, 23).Style.Font.Size = 10
        '        .Cells(4, 23, 4, 23).Style.Font.Name = "Segoe UI"

        '        Dim ds As DataSet
        '        Dim ErrMsg As String = ""
        '        Dim dtFrom = Format(dtDateFrom.Value, "yyyy-MM-dd")
        '        Dim dtTo = Format(dtDateTo.Value, "yyyy-MM-dd")

        '        ds = clsTrackingPRDB.GetList(dtFrom, dtTo, pErr)

        '        Dim No As Integer
        '        No = 1

        '        For i = 0 To ds.Tables(0).Rows.Count - 1
        '            .Cells(i + 5, 2, i + 5, 2).Value = No
        '            .Cells(i + 5, 3, i + 5, 3).Value = ds.Tables(0).Rows(i)("Department")
        '            .Cells(i + 5, 4, i + 5, 4).Value = ds.Tables(0).Rows(i)("Section")
        '            .Cells(i + 5, 5, i + 5, 5).Value = ds.Tables(0).Rows(i)("Project")
        '            .Cells(i + 5, 6, i + 5, 6).Value = ds.Tables(0).Rows(i)("PRNo")
        '            .Cells(i + 5, 7, i + 5, 7).Value = ds.Tables(0).Rows(i)("PRType")
        '            .Cells(i + 5, 8, i + 5, 8).Value = ds.Tables(0).Rows(i)("Description")
        '            .Cells(i + 5, 9, i + 5, 9).Value = ds.Tables(0).Rows(i)("Specification")
        '            .Cells(i + 5, 10, i + 5, 10).Value = ds.Tables(0).Rows(i)("Qty")
        '            .Cells(i + 5, 11, i + 5, 11).Value = ds.Tables(0).Rows(i)("PICUserName")
        '            .Cells(i + 5, 12, i + 5, 12).Value = ds.Tables(0).Rows(i)("PICCostControl")
        '            .Cells(i + 5, 13, i + 5, 13).Value = ds.Tables(0).Rows(i)("PRReceived")
        '            .Cells(i + 5, 14, i + 5, 14).Value = ds.Tables(0).Rows(i)("CEActual")
        '            .Cells(i + 5, 15, i + 5, 15).Value = ds.Tables(0).Rows(i)("IABudget")
        '            .Cells(i + 5, 16, i + 5, 16).Value = ds.Tables(0).Rows(i)("CounterProposal")
        '            .Cells(i + 5, 17, i + 5, 17).Value = ds.Tables(0).Rows(i)("IAPrice")
        '            .Cells(i + 5, 18, i + 5, 18).Value = ds.Tables(0).Rows(i)("PODate")
        '            .Cells(i + 5, 19, i + 5, 19).Value = ds.Tables(0).Rows(i)("GRDate")
        '            .Cells(i + 5, 20, i + 5, 20).Value = ds.Tables(0).Rows(i)("POCreated")
        '            .Cells(i + 5, 21, i + 5, 21).Value = ds.Tables(0).Rows(i)("PONo")
        '            .Cells(i + 5, 22, i + 5, 22).Value = ds.Tables(0).Rows(i)("GRCreated")
        '            .Cells(i + 5, 23, i + 5, 23).Value = ds.Tables(0).Rows(i)("GRNo")

        '            No = No + 1
        '        Next

        '        FormatExcel(ws, ds)
        '        InsertHeader(ws)
        '    End With

        '    exl.Save()
        '    DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("Download/" & fi.Name)
        'Catch ex As Exception
        '    show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        'End Try
    End Sub

    Private Sub InsertHeader(ByVal pExl As ExcelWorksheet)
        With pExl
            .Cells(2, 2, 2, 2).Value = "TRACKING PURCHASE REQUEST"
            .Cells(2, 2, 2, 2).Style.HorizontalAlignment = HorzAlignment.Default
            .Cells(2, 2, 2, 2).Style.VerticalAlignment = VertAlignment.Center
            .Cells(2, 2, 2, 2).Style.Font.Bold = True
            .Cells(2, 2, 2, 2).Style.Font.Size = 16
            .Cells(2, 2, 2, 2).Style.Font.Name = "Segoe UI"
        End With
    End Sub

    Private Sub FormatExcel(ByVal pExl As ExcelWorksheet, ByVal ds As DataSet)
        With pExl
            .Column(1).Width = 2
            .Column(2).Width = 5
            .Column(3).Width = 40
            .Column(4).Width = 40
            .Column(5).Width = 30
            .Column(6).Width = 40
            .Column(7).Width = 30
            .Column(8).Width = 50
            .Column(9).Width = 50
            .Column(10).Width = 10
            .Column(11).Width = 25
            .Column(12).Width = 25
            .Column(13).Width = 20
            .Column(14).Width = 20
            .Column(15).Width = 20
            .Column(16).Width = 20
            .Column(17).Width = 20
            .Column(18).Width = 20
            .Column(19).Width = 20
            .Column(20).Width = 35
            .Column(21).Width = 35
            .Column(22).Width = 35
            .Column(23).Width = 35
            

            Dim rgAll As ExcelRange = .Cells(4, 2, ds.Tables(0).Rows.Count + 4, 23)
            DrawAllBorders(rgAll)

            Dim rgHeader As ExcelRange = .Cells(4, 2, 4, 23)
            rgHeader.Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
            rgHeader.Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
        End With
    End Sub

    Private Sub DrawAllBorders(ByVal Rg As ExcelRange)
        With Rg
            .Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
            .Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
            .Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
            .Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
            .Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
        End With
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

#End Region

#Region "Control Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("H020")
        Master.SiteTitle = sGlobal.menuName
        If Not Page.IsPostBack Then
            pUser = Session("user")
            show_error(MsgTypeEnum.Info, "", 0)
            AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "H020")

            Dim dfrom As Date
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dtDateFrom.Value = dfrom
            dtDateTo.Value = Now

            If AuthUpdate = False Then
                btnExcel.Enabled = False
            Else
                btnExcel.Enabled = True
            End If
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_LoadGrid()
        End If
    End Sub

    Private Sub Grid_CustomCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Select Case pFunction
            Case "load"
                up_LoadGrid()
                If Grid.VisibleRowCount = 0 Then
                    show_error(MsgTypeEnum.Warning, "The data you want to search does not exist!", 1)
                Else
                    show_error(MsgTypeEnum.Info, "", 0)
                End If
            Case "excel"
                ' up_LoadExcel()
        End Select

    End Sub
    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        up_LoadExcel()
    End Sub
#End Region
End Class
﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO
Imports System.Transactions
Imports System.Web.UI
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxGridView

Public Class CPAcceptanceDetail
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"
    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_List", "PRNumber|CENumber|Rev", txtPRNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub GridLoadAttachment()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", RevNo As String = ""

        Try
            If txtRev.Text = "" Then
                RevNo = hf.Get("RevisionNo")
            Else
                RevNo = txtRev.Text
            End If

            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Acceptance_Detail_Attachment", "CPNumber|Rev", txtCPNumber.Text & "|" & RevNo, ErrMsg)

            If ErrMsg = "" Then
                GridAtc.DataSource = ds.Tables(0)
                GridAtc.DataBind()

                If ds.Tables(0).Rows.Count = 0 Then
                    'DisplayMessage(MsgTypeEnum.Info, "There is no attachment to show!", GridAtc)
                End If
            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridAtc)
            End If

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridAtc)
        End Try
    End Sub

    Private Sub SetInformation(ByVal pObj As Object, Optional pCalledFromCPList As Boolean = False)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_SetHeader", "PRNumber|CENumber|Rev", txtPRNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text, ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_SetHeader", "PRNumber|CENumber|Rev", Split(Request.QueryString(0), "|")(2) & "|" & txtCENumber.Text & "|" & Split(Request.QueryString(0), "|")(1), ErrMsg)
        End If


        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                pObj.JSProperties("cpCPNumber") = ds.Tables(0).Rows(0).Item("CP_Number").ToString()
                pObj.JSProperties("cpRev") = ds.Tables(0).Rows(0).Item("Rev").ToString()
                pObj.JSProperties("cpCENumber") = ds.Tables(0).Rows(0).Item("CE_Number")
                pObj.JSProperties("cpPRNumber") = ds.Tables(0).Rows(0).Item("PR_Number")
                pObj.JSProperties("cpInvitationDate") = CDate(ds.Tables(0).Rows(0).Item("Invitation_Date")) 'Format(CDate(ds.Tables(0).Rows(0).Item("Invitation_Date")), "yyyy-MM-dd")
                pObj.JSProperties("cpNotes") = ds.Tables(0).Rows(0).Item("Notes").ToString()
            Else
                pObj.JSProperties("cpCPNumber") = ""
                pObj.JSProperties("cpRev") = ""
                pObj.JSProperties("cpCENumber") = ""
                pObj.JSProperties("cpPRNumber") = ""
                pObj.JSProperties("cpInvitationDate") = ""
                pObj.JSProperties("cpNotes") = ""

                Dim dt As Date
                dt = Year(Now) & "-" & Month(Now) & "-01"
                pObj.JSProperties("cpQuotationDate") = dt
            End If

            If pCalledFromCPList = True Then
                'add this parameter to prevent reset value of Notes when callback
                pObj.JSProperties("cpCalledFromCPList") = "1"
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, pObj)
        End If
    End Sub

    Private Sub SetInformationOnServerSide()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_SetHeader", "PRNumber|CENumber|Rev", txtPRNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text, ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_SetHeader", "PRNumber|CENumber|Rev", Split(Request.QueryString(0), "|")(3) & "|" & txtCENumber.Text & "|" & Split(Request.QueryString(0), "|")(1), ErrMsg)
        End If


        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                memoNote.Text = ds.Tables(0).Rows(0).Item("Notes").ToString()
                txtInvitationDate.Text = Format(CDate(ds.Tables(0).Rows(0).Item("Invitation_Date")), "yyyy-MM-dd")
            Else
                memoNote.Text = ""
                txtInvitationDate.Text = ""
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub SaveAttachment(ByVal pCPNumber As String, ByVal pRev As String, ByVal pFileName As String, ByVal pFilePath As String, pFileSize As Double)
        Dim ls_SQL As String = "", UserLogin As String = Session("user")

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                ls_SQL = "EXEC sp_CP_Acceptance_Insert_Attachment " & _
                         "'" & pCPNumber & "','" & pRev & "'," & _
                         "'" & pFileName & "','" & pFilePath & "','" & UserLogin & "'," & _
                         "'" & pFileSize & "'"
                Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                sqlComm.CommandType = CommandType.Text
                sqlComm.ExecuteNonQuery()
                sqlComm.Dispose()

                sqlTran.Commit()
            End Using
        End Using
    End Sub

    Private Sub DeleteAttachment(ByVal pDeleteOption As String, Optional pCPNumber As String = "", Optional pRev As String = "", Optional pFileName As String = "", Optional pSeqNo As Integer = 1)
        Select Case pDeleteOption
            Case "database"
                Dim ls_SQL As String = ""

                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()

                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                        ls_SQL = "EXEC sp_CP_Acceptance_DeleteAttachment" & _
                                 "'" & pCPNumber & "','" & pRev & "','" & pFileName & "'," & pSeqNo & ""

                        Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                        sqlComm.CommandType = CommandType.Text
                        sqlComm.ExecuteNonQuery()
                        sqlComm.Dispose()

                        sqlTran.Commit()
                    End Using
                End Using

            Case "file"
                Dim sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                sqlConn.Open()

                Dim ds As New DataSet
                Dim folderPath As String = Server.MapPath("~/AttachmentsCP/")
                ds = GetDataSource(CmdType.SQLScript, "SELECT REPLACE(FIlePath,LEFT(FIlePath,16),'') FileName FROM CP_Attachment WHERE CP_Number='" & pCPNumber & "' AND Rev='" & pRev & "' AND FileName='" & pFileName & "' AND Seq_No = '" & pSeqNo & "'")
                If ds.Tables(0).Rows.Count > 0 Then
                    If System.IO.File.Exists(folderPath & ds.Tables(0).Rows(0)("FileName").ToString().Trim()) = True Then
                        System.IO.File.Delete(folderPath & ds.Tables(0).Rows(0)("FileName").ToString().Trim())
                    End If
                End If

                ds.Dispose()
                sqlConn.Close()
        End Select
    End Sub
#End Region

#Region "FUNCTIONS"
    Private Function GetAllSupplierForGridHeaderCaption()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "SELECT Sup = dbo.MergeSupplier('" & txtCENumber.Text & "')", "", "", ErrMsg)

        If ErrMsg = "" Then
            retVal = ds.Tables(0).Rows(0).Item("Sup").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If

        Return retVal
    End Function

    Private Function IsDataExist() As String
        Dim retVal As String = ""
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Exist", "CPNumber", txtCPNumber.Text, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = ds.Tables(0).Rows(0).Item("Ret")
                Else
                    'NOT EXISTS
                    retVal = "N"
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataAccepted() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CE_Number = '" & txtCENumber.Text & "' AND ISNULL(CP_Status,'0') = '3'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST: ACCEPTED
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function GetApprovalIDByUser() As String
        Dim retVal As String = "1"
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            'ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '2'", "", "", ErrMsg)
            ds = GetDataSource(CmdType.SQLScript, "SELECT Approval_ID FROM CP_Approval WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND Approval_Person = '" & pUser & "'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    retVal = ds.Tables(0).Rows(0).Item("Approval_ID").ToString()
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function ConvertToFormatDate_yyyyMMdd(ByVal pDate As String) As String
        Dim retVal As String = "1900-01-01"
        Dim pYear As String = Split(pDate, "-")(0)
        Dim pMonth As String = Strings.Right("0" & Split(pDate, "-")(1), 2)
        Dim pDay As String = Strings.Right("0" & Split(pDate, "-")(2), 2)

        retVal = pYear & "-" & pMonth & "-" & pDay

        Return retVal
    End Function
#End Region

#Region "EVENTS"
    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|E040", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAccept.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAccept, btnAccept.GetType(), "btnAccept", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        Dim dDate As Date
        dDate = Format(CDate(Now), "yyyy-MM-dd")
        txtInvitationDate.Value = dDate
        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, txtCENumber)
            End Try
        End If
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "COUNTER PROPOSAL ACCEPTANCE (TENDER) DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E040")

        gs_Message = ""

        If Page.IsCallback Then
            ucAtc.Enabled = False
        End If

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            txtInvitationDate.Text = Format(CDate(Now), "yyyy-MM-dd")
            ScriptManager.RegisterStartupScript(btnAddFile, btnAddFile.GetType(), "btnAddFile", "btnAddFile.SetEnabled(false);", True)

            If Request.QueryString.Count > 0 Then
                Try
                    txtCPNumber.Text = Split(Request.QueryString(0), "|")(0)
                    txtRev.Text = Split(Request.QueryString(0), "|")(1)
                    txtCENumber.Text = Split(Request.QueryString(0), "|")(2)
                    txtPRNumber.Text = Split(Request.QueryString(0), "|")(3)

                    Call SetInformationOnServerSide()

                    If IsDataAccepted() = True Then
                        btnAccept.Enabled = False
                        ScriptManager.RegisterStartupScript(btnDeleteAtc, btnDeleteAtc.GetType(), "btnDeleteAtc", "btnDeleteAtc.SetEnabled(false);", True)
                        ScriptManager.RegisterStartupScript(btnAddFile, btnAddFile.GetType(), "btnAddFile", "btnAddFile.SetEnabled(false);", True)
                        'GridAtc.Enabled = False

                        ucAtc.Enabled = False
                    Else
                        btnAccept.Enabled = True
                        ScriptManager.RegisterStartupScript(btnDeleteAtc, btnDeleteAtc.GetType(), "btnDeleteAtc", "btnDeleteAtc.SetEnabled(true);", True)
                        ScriptManager.RegisterStartupScript(btnAddFile, btnAddFile.GetType(), "btnAddFile", "btnAddFile.SetEnabled(true);", True)
                        'GridAtc.Enabled = False

                        ucAtc.Enabled = True
                    End If

                Catch ex As Exception
                    Response.Redirect("CPAcceptanceDetail.aspx")
                End Try


                'Remove session after back from ViewCounterProposal.aspx
                If IsNothing(Session("E040_QueryString")) = False Then
                    Session.Remove("E040_QueryString")
                End If

            Else
                'Call GenerateCPNumber()
            End If
        End If
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim headerCaption As String = ""
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Call GridLoad()

            'SET HEADER CAPTION (SUPPLIER)
            '#Initialize
            Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
            Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
            Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
            Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
            Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
            Grid.Columns("Supplier1").Visible = True
            Grid.Columns("Supplier2").Visible = True
            Grid.Columns("Supplier3").Visible = True
            Grid.Columns("Supplier4").Visible = True
            Grid.Columns("Supplier5").Visible = True

            Grid.Columns("BestPriceSupplier1").Visible = True
            Grid.Columns("BestPriceSupplier2").Visible = True
            Grid.Columns("BestPriceSupplier3").Visible = True
            Grid.Columns("BestPriceSupplier4").Visible = True
            Grid.Columns("BestPriceSupplier5").Visible = True


            '#Set by condition
            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.Columns("Supplier1").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.Columns("Supplier2").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.Columns("Supplier3").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.Columns("Supplier4").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("Supplier5").Visible = False
            End Try



            '#Set by condition BestPriceSupplier
            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("BestPriceSupplier1").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.Columns("BestPriceSupplier1").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("BestPriceSupplier2").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.Columns("BestPriceSupplier2").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("BestPriceSupplier3").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.Columns("BestPriceSupplier3").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("BestPriceSupplier4").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.Columns("BestPriceSupplier4").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("BestPriceSupplier5").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("BestPriceSupplier5").Visible = False
            End Try


            'After Save Data
            If e.Parameters = "save" Then
                Grid.JSProperties("cpSaveData") = "1"

                If IsNothing(Session("E020_ErrMsg")) = True Then
                    Call SetInformation(Grid)
                    Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", Grid)
                Else
                    If Session("E040_ErrMsg") = "Please fill all of rows on the grid before save!" Then
                        Call DisplayMessage(MsgTypeEnum.Warning, Session("E040_ErrMsg"), Grid)
                        Session.Remove("E040_ErrMsg")
                    End If
                End If

            End If


        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        'Non-Editable
        e.Cell.BackColor = Color.LemonChiffon
    End Sub


    Private Sub GridAtc_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles GridAtc.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub GridAtc_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridAtc.CustomCallback
        Call GridLoadAttachment()
    End Sub

    Private Sub GridAtc_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridAtc.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "FileType" Or e.DataColumn.FieldName = "FileName" Then
            'Editable
            e.Cell.BackColor = Color.LemonChiffon
        Else
            'Non-Editable
            e.Cell.BackColor = Color.White
        End If
    End Sub

    Private Sub ucAtc_FileUploadComplete(sender As Object, e As DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs) Handles ucAtc.FileUploadComplete

        If IsNothing(e.UploadedFile.FileName) Then
            ucAtc.JSProperties("cpType") = "2"
            ucAtc.JSProperties("cpMessage") = "Upload file is required"
            Exit Sub
        End If

        If e.IsValid = False Then
            ucAtc.JSProperties("cpType") = "3"
            ucAtc.JSProperties("cpMessage") = "Invalid file!"
        Else
            Dim CPNumber As String = hf.Get("CPNumber")
            CPNumber = Replace(CPNumber, "/", "")
            CPNumber = Replace(CPNumber, "~", "")
            CPNumber = Replace(CPNumber, "\", "")
            CPNumber = Replace(CPNumber, ".", "")

            Dim Rev As String = hf.Get("Rev")

            Dim serverPath As String = Server.MapPath("~/AttachmentsCP/")
            Dim fileName As String = CPNumber & "_" & Format(CDate(Now), "yyyyMMdd_hhmmss") & ".pdf" 'e.UploadedFile.FileName
            Dim fullPath As String = String.Format("{0}{1}", serverPath, fileName)
            ' Dim SizeFileCompress As Single = (e.UploadedFile.ContentLength / 1024 / 1024)
            Dim SizeFile As Double = ((e.UploadedFile.ContentLength / 1024) / 1024)
            Dim FileName1 As String = ""

            Try
                Using conn As New SqlConnection(Sconn.Stringkoneksi)
                    conn.Open()
                    Dim Sql = "Select CASE WHEN ROUND(SUM(FileSize),2)=2 THEN ROUND(SUM(filesize),2) ELSE SUM(FileSize) END AS FileSize from CP_Attachment where CP_Number='" & hf.Get("CPNumber") & "' AND Rev = '" & Rev & "'  Group By CP_Number"
                    Dim cmd As New SqlCommand(Sql, conn)
                    cmd.CommandType = CommandType.Text
                    Dim da As New SqlDataAdapter(cmd)
                    Dim ds As New DataSet
                    da.Fill(ds)
                    If SizeFile > 2 Then
                        ucAtc.JSProperties("cpType") = "2"
                        ucAtc.JSProperties("cpMessage") = "File size must not exceed 2 MB"
                        Exit Sub
                    End If

                    If ds.Tables(0).Rows.Count > 0 Then
                        '    FileSizeDB = 10
                        '    If SizeFile > FileSizeDB Then
                        '        ucAtc.JSProperties("cpType") = "3"
                        '        ucAtc.JSProperties("cpMessage") = "File size limit max 10 MB"
                        '        Exit Sub
                        '    End If
                        'Else
                        Dim FileSizeDB = ds.Tables(0).Rows(0)("FileSize")
                        If (SizeFile + FileSizeDB) > 2 Then
                            ucAtc.JSProperties("cpType") = "2"
                            ucAtc.JSProperties("cpMessage") = "File size limit max 2 MB, remain size (" & IIf(Math.Ceiling((2 - FileSizeDB) * 1024) = 49, 48, Math.Ceiling((2 - FileSizeDB) * 1024)) & ") KB"
                            Exit Sub
                        End If
                    End If
                End Using
            Catch ex As Exception
                'ucAtc.JSProperties("cpType") = "3"
                'ucAtc.JSProperties("cpMessage") = ex.Message
            End Try


            Try

                'CHECK EXISTING FOLDER (Attachment)
                If (Not System.IO.Directory.Exists(serverPath)) Then
                    System.IO.Directory.CreateDirectory(serverPath)
                End If


                'SAVE ATTACHMENT
                e.UploadedFile.SaveAs(fullPath)

                ucAtc.JSProperties("cpType") = "1"
                ucAtc.JSProperties("cpMessage") = "Counter Proposal " & e.UploadedFile.FileName & " has been uploaded successfully!"

                Call SaveAttachment(hf.Get("CPNumber"), Rev, e.UploadedFile.FileName, "~/AttachmentsCP/" & fileName, SizeFile)

            Catch ex As Exception
                ucAtc.JSProperties("cpType") = "3"
                ucAtc.JSProperties("cpMessage") = Err.Description
            End Try
        End If
    End Sub

    Private Sub cb_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cb.Callback
        Select Case e.Parameter
            Case "DeleteAttachment"
                Dim iData As Integer = 0
                Dim iDataSeqNo As Integer = 0
                Dim param As String = hf.Get("DeleteAttachment")
                Dim paramSeqNo As String = hf.Get("SeqNo")
                'If param <> "" Then param = Strings.Left(param, Len(param) - 1)

                'IDENTIFY THE NUMBER OF DATA FILE NAME
                For idx As Integer = 1 To Len(param)
                    If Strings.Mid(param, idx, 1) = "|" Then
                        iData = iData + 1
                    End If
                Next idx

                For idx2 As Integer = 1 To Len(paramSeqNo)
                    If Strings.Mid(paramSeqNo, idx2, 1) = "|" Then
                        iDataSeqNo = iDataSeqNo + 1
                    End If
                Next idx2

                'DELETE ATTACHMENT
                For idx As Integer = 0 To iData - 1
                    For idx2 As Integer = 0 To iDataSeqNo - 1

                        'file
                        Call DeleteAttachment("file", txtCPNumber.Text, txtRev.Text, Split(param, "|")(idx), Split(paramSeqNo, "|")(idx2))

                        'database
                        Call DeleteAttachment("database", txtCPNumber.Text, txtRev.Text, Split(param, "|")(idx), Split(paramSeqNo, "|")(idx2))
                    Next idx2

                Next idx

                cb.JSProperties("cpActionAfter") = "DeleteAtc"
                DisplayMessage(MsgTypeEnum.Success, "Data deleted successfully!", cb)

            Case Else
                    Call SetInformation(cb)
        End Select
    End Sub

    Private Sub cbExist_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbExist.Callback
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        Select Case e.Parameter
            Case "exist"
                Dim ret As String = "N"
                cbExist.JSProperties("cpParameter") = "exist"

                ret = IsDataExist()
                cbExist.JSProperties("cpResult") = ret

            Case "IsAccepted"
                cbExist.JSProperties("cpParameter") = "IsAccepted"
                If IsDataAccepted() = True Then
                    cbExist.JSProperties("cpResult") = "Y"
                Else
                    cbExist.JSProperties("cpResult") = "N"
                End If

            Case "accept"
                Dim ls_SQL As String = "", UserLogin As String = Session("user"), ls_ApprovalID As String = "1"
                Dim idx As Integer = 0

                Try
                    cbExist.JSProperties("cpParameter") = "accept"

                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()
                        Dim sqlComm
                        Dim sql = "sp_CP_Acceptance_CheckExistAttachment"
                        sqlComm = New SqlCommand(sql, sqlConn)
                        sqlComm.CommandType = CommandType.StoredProcedure
                        sqlComm.Parameters.AddWithValue("CP_Number", txtCPNumber.Text)
                        sqlComm.Parameters.AddWithValue("Rev", txtRev.Text)

                        Dim da1 As New SqlDataAdapter(sqlComm)
                        Dim ds1 As New DataSet
                        da1.Fill(ds1)

                        If ds1.Tables(0).Rows.Count < 1 Then
                            cbExist.JSProperties("cpResult") = "Atc"
                            Call DisplayMessage(MsgTypeEnum.Warning, "Please Upload Document Attachment First", cbExist)
                            Exit Sub
                        End If
                    End Using

                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()

                        Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                            ls_SQL = "EXEC sp_CP_Acceptance_Detail_Accept " & _
                                     "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                     "'3','" & UserLogin & "'"

                            Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()

                            sqlTran.Commit()
                        End Using

                    End Using
                    cbExist.JSProperties("cpAccept") = "Y"
                    Call DisplayMessage(MsgTypeEnum.Success, "Data accepted successfully!", cbExist)


                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                End Try

        End Select
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Try
            If Request.QueryString.Count > 0 Then
                Dim pDateFrom As String = ConvertToFormatDate_yyyyMMdd(Split(Request.QueryString(0), "|")(6)), _
                    pDateTo As String = ConvertToFormatDate_yyyyMMdd(Split(Request.QueryString(0), "|")(7)), _
                    pPRNumber As String = Split(Request.QueryString(0), "|")(8), _
                    pSupplierCode As String = Split(Request.QueryString(0), "|")(9), _
                    pCPNumber As String = Split(Request.QueryString(0), "|")(10), _
                    pRowPos As String = Split(Request.QueryString(0), "|")(11)
                Session("btnBack_CPAcceptanceDetail") = pDateFrom & "|" & pDateTo & "|" & pPRNumber & "|" & pSupplierCode & "|" & pCPNumber & "|" & pRowPos
            End If

        Catch ex As Exception

        End Try

        Response.Redirect("CPAcceptance.aspx")
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click
        If Request.QueryString.Count > 0 Then
            Session("E040_QueryString") = Request.QueryString(0)
        End If

        Session("CP_calledFrom") = "E040"
        Session("E040_paramViewCP") = txtCPNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text
        Response.Redirect("ViewCounterProposal.aspx")
    End Sub

    Protected Sub keyFieldLink_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim link As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
        Dim container As GridViewDataItemTemplateContainer = TryCast(link.NamingContainer, GridViewDataItemTemplateContainer)

        If container.ItemIndex >= 0 Then
            link.Text = Split(container.KeyValue, "|")(0)
            link.Target = "_blank"
            link.NavigateUrl = Split(container.KeyValue, "|")(1)
        End If
    End Sub
#End Region



   
End Class
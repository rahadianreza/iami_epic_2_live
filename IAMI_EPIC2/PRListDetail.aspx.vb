﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data

Public Class PRListDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String
    Dim MatNo As String
    Dim status As String
    Dim rev As Integer
    Dim statusAdmin As String
    'Dim tempPRNo As String
#End Region

#Region "Initialization"

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|B010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnDraft.SetEnabled(false);" & vbCrLf & _
                         "btnDelete.SetEnabled(false);" & vbCrLf & _
                         "btnSubmit.SetEnabled(false);" & vbCrLf & _
                         "btnModify.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboDepartment)
            End Try
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim prno As String
        Master.SiteTitle = "PURCHASE REQUEST LIST DETAIL"
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        prno = Split(Request.QueryString("ID"), "|")(0)
        MatNo = Request.QueryString("MatNo") & ""
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            txtPRNo.Text = prno
            up_FillCombo()
            If prno <> "" Then
                status = Split(Request.QueryString("ID"), "|")(1)
                rev = Split(Request.QueryString("ID"), "|")(2)
                Session("rev") = rev
                up_LoadData(prno, "")
            Else
                up_LoadData("", MatNo)
                If txtPRNo.Text = "--NEW--" Then
                    gs_GeneratePRNo = "--NEW--"
                End If

            End If
        End If

    End Sub
#End Region

#Region "Procedure"
    Private Sub up_FillCombo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsPRAcceptanceDB.GetComboData("PRBudget", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPRBudget.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("PRType", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPRType.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        'ds = clsPRAcceptanceDB.GetComboData("Department", pErr)
        'If pErr = "" Then
        '    For i = 0 To ds.Tables(0).Rows.Count - 1
        '        cboDepartment.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
        '    Next
        'End If

        'ds = clsPRAcceptanceDB.GetComboData("Section", pErr)
        'If pErr = "" Then
        '    For i = 0 To ds.Tables(0).Rows.Count - 1
        '        cboSection.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
        '    Next
        'End If

        Dim ds1 As New DataSet
        Dim prno = Split(Request.QueryString("ID"), "|")(0)
        Try
            Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                sqlConn.Open()
                Dim Query = "select Department_Code from PR_Header where PR_Number='" & prno & "'"
                Dim cmd As New SqlCommand(Query, sqlConn)
                cmd.CommandType = CommandType.Text
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds1)

                If ds1.Tables(0).Rows.Count > 0 Then
                    Dim dept = ds1.Tables(0).Rows(0)("Department_Code").ToString
                    If statusAdmin = "1" Then
                        up_FillCombo2(cboDepartment, statusAdmin, pUser, "Department")
                        up_FillCombo2(cboSection, statusAdmin, pUser, "Section", dept)
                        up_FillCombo2(cboCostCenter, statusAdmin, pUser, "CostCenter", dept)
                    Else
                        up_FillCombo2(cboDepartment, statusAdmin, pUser, "Department")
                        up_FillCombo2(cboSection, statusAdmin, pUser, "Section")
                        up_FillCombo2(cboCostCenter, statusAdmin, pUser, "CostCenter", tmpSection.Text)
                    End If

                Else
                    If statusAdmin = "1" Then
                        up_FillCombo2(cboDepartment, statusAdmin, pUser, "Department")
                        up_FillCombo2(cboSection, statusAdmin, pUser, "Section")
                        up_FillCombo2(cboCostCenter, statusAdmin, pUser, "CostCenter")
                    Else
                        up_FillCombo2(cboDepartment, statusAdmin, pUser, "Department")
                        up_FillCombo2(cboSection, statusAdmin, pUser, "Section")
                        up_FillCombo2(cboCostCenter, statusAdmin, pUser, "CostCenter", tmpSection.Text)
                    End If
                End If



            End Using
        Catch ex As Exception

        End Try

        'cboDepartment.SelectedIndex = -1
        'cboSection.SelectedIndex = -1

        'ds = clsPRAcceptanceDB.GetComboData("CostCenter", pErr)
        'If pErr = "" Then
        '    For i = 0 To ds.Tables(0).Rows.Count - 1
        '        cboCostCenter.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
        '    Next
        'End If

    End Sub

    Private Sub up_LoadData(pPRNo As String, pMatNo As String)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim cPRList As New ClsPRList

        Try
            If pPRNo <> "" Then
                cPRList.PRNumber = pPRNo
                rev = ClsPRListDB.GetRev(pPRNo)
                ds = ClsPRListDB.GetDataPRDetail(cPRList, pPRNo, pUser, rev, pErr)

                If pErr = "" Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        gs_GeneratePRNo = ds.Tables(0).Rows(0)("PR_Number")

                        'If Left(gs_GeneratePRNo, 5) = "DRAFT" Then
                        '    PRDate.Value = Now 'ds.Tables(0).Rows(0)("PR_Date")
                        'Else
                        '    PRDate.Value = ds.Tables(0).Rows(0)("PR_Date")
                        'End If

                        PRDate.Value = ds.Tables(0).Rows(0)("PR_Date")
                        cboPRBudget.SelectedIndex = cboPRBudget.Items.IndexOf(cboPRBudget.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Budget_Code") & "")))
                        cboPRType.SelectedIndex = cboPRType.Items.IndexOf(cboPRType.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("PRType_Code") & "")))
                        cboDepartment.SelectedIndex = cboDepartment.Items.IndexOf(cboDepartment.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Department_Code") & "")))
                        cboSection.SelectedIndex = cboSection.Items.IndexOf(cboSection.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Section_Code") & "")))
                        cboCostCenter.SelectedIndex = cboCostCenter.Items.IndexOf(cboCostCenter.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("CostCenter") & "")))
                        txtProject.Text = ds.Tables(0).Rows(0)("Project")

                        Dim PRStatus As String
                        Dim UrgentStatus As String = ""

                        If IsDBNull(ds.Tables(0).Rows(0)("PR_Status")) Then
                            PRStatus = "0"
                        Else
                            PRStatus = ds.Tables(0).Rows(0)("PR_Status")
                        End If

                        If status = "Reject" Then
                            PRStatus = "4"
                            btnPrint.Enabled = False
                        End If

                        If PRStatus <> "0" Then
                            'Dim script As String = ""
                            'script = "btnDraft.SetEnabled(false);" & vbCrLf & _
                            '         "btnDelete.SetEnabled(false);" & vbCrLf & _
                            '         "btnSubmit.SetEnabled(false);" & vbCrLf & _
                            '         "btnModify.SetEnabled(false);"
                            'ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
                           
                            PRDate.Enabled = False
                            cboPRBudget.Enabled = False
                            cboPRType.Enabled = False
                            cboDepartment.Enabled = False
                            cboSection.Enabled = False
                            cboCostCenter.Enabled = False
                            txtProject.Enabled = False
                            chkUrgent.Enabled = False
                            PODate.Enabled = False

                            Grid.Enabled = False
                            txtUrgentNote.Enabled = False
                            btnDraft.Enabled = False
                            btnModify.Enabled = False
                            btnSubmit.Enabled = False
                            btnDelete.Enabled = False
                        Else
                            btnDraft.Enabled = True
                            btnModify.Enabled = True
                            btnSubmit.Enabled = True
                            btnDelete.Enabled = True
                        End If

                        If IsDBNull(ds.Tables(0).Rows(0)("Urgent_Status")) Then
                            UrgentStatus = "No"
                        Else
                            UrgentStatus = Trim(ds.Tables(0).Rows(0)("Urgent_Status"))
                        End If

                        If UrgentStatus = "Yes" Then
                            chkUrgent.Checked = True
                            'PODate.Enabled = True
                            'txtUrgentNote.Enabled = True
                        Else
                            chkUrgent.Checked = False
                            'PODate.Enabled = False
                            'txtUrgentNote.Enabled = False
                        End If

                        PODate.Value = ds.Tables(0).Rows(0)("Req_POIssueDate")
                        txtUrgentNote.Text = Trim(ds.Tables(0).Rows(0)("Urgent_Note") & "")

                        Grid.DataSource = ds
                        Grid.DataBind()


                    End If
                End If
            Else
                If gs_Modify = True Then
                    txtPRNo.Text = gs_GeneratePRNo
                    cPRList.PRNumber = gs_GeneratePRNo

                    rev = ClsPRListDB.GetRev(gs_GeneratePRNo)
                    ds = ClsPRListDB.GetDataPRDetail(cPRList, gs_GeneratePRNo, "", rev, pErr)

                    'If Left(gs_GeneratePRNo, 5) = "DRAFT" Then
                    '    PRDate.Value = Now 'ds.Tables(0).Rows(0)("PR_Date")
                    'Else
                    '    PRDate.Value = ds.Tables(0).Rows(0)("PR_Date")
                    'End If

                    PRDate.Value = ds.Tables(0).Rows(0)("PR_Date")
                    cboPRBudget.SelectedIndex = cboPRBudget.Items.IndexOf(cboPRBudget.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Budget_Code") & "")))
                    cboPRType.SelectedIndex = cboPRType.Items.IndexOf(cboPRType.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("PRType_Code") & "")))
                    cboDepartment.SelectedIndex = cboDepartment.Items.IndexOf(cboDepartment.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Department_Code") & "")))
                    cboSection.SelectedIndex = cboSection.Items.IndexOf(cboSection.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Section_Code") & "")))
                    cboCostCenter.SelectedIndex = cboCostCenter.Items.IndexOf(cboCostCenter.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("CostCenter") & "")))
                    txtProject.Text = ds.Tables(0).Rows(0)("Project")

                    Dim PRStatus As String
                    Dim UrgentStatus As String = ""

                    If IsDBNull(ds.Tables(0).Rows(0)("PR_Status")) Then
                        PRStatus = "0"
                    Else
                        PRStatus = ds.Tables(0).Rows(0)("PR_Status")
                    End If

                    cboPRType.Enabled = False

                    If PRStatus = "1" Or PRStatus = "4" Then
                        btnDraft.Enabled = False
                        btnModify.Enabled = False
                        btnSubmit.Enabled = False
                        btnDelete.Enabled = False
                        PRDate.Enabled = False
                        cboPRBudget.Enabled = False
                        cboDepartment.Enabled = False
                        cboSection.Enabled = False
                        cboCostCenter.Enabled = False
                        txtProject.Enabled = False
                        chkUrgent.Enabled = False
                        PODate.Enabled = False
                        txtUrgentNote.Enabled = False
                        Grid.Enabled = False
                    Else
                        btnDraft.Enabled = True
                        btnModify.Enabled = True
                        btnSubmit.Enabled = True
                        btnDelete.Enabled = True
                    End If

                    If IsDBNull(ds.Tables(0).Rows(0)("Urgent_Status")) Then
                        UrgentStatus = "No"
                    Else
                        UrgentStatus = Trim(ds.Tables(0).Rows(0)("Urgent_Status"))
                    End If

                    If UrgentStatus = "Yes" Then
                        chkUrgent.Checked = True
                        'PODate.Enabled = True
                        'txtUrgentNote.Enabled = True
                    Else
                        chkUrgent.Checked = False
                        'PODate.Enabled = False
                        'txtUrgentNote.Enabled = False
                    End If

                    PODate.Value = ds.Tables(0).Rows(0)("Req_POIssueDate")
                    txtUrgentNote.Text = Trim(ds.Tables(0).Rows(0)("Urgent_Note") & "")
                Else
                    txtPRNo.Text = "--NEW--"
                    'gs_GeneratePRNo = "--NEW--"
                    PRDate.Value = Now
                    PODate.Value = DateAdd(DateInterval.Day, 21, PRDate.Value)
                    cboPRType.SelectedIndex = gs_FilterPRTypeIndex
                End If

                txtFilter2.Text = pMatNo

                ds = ClsPRListDB.LoadNewItem(gs_GeneratePRNo, pMatNo)

                Grid.DataSource = ds
                Grid.DataBind()

            End If

        Catch ex As Exception
            cbApprove.JSProperties("cpMessage") = ex.Message
        End Try
    End Sub

    Private Sub up_SaveDetail(PRList As ClsPRList, sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs, Optional ByRef pErr As String = "")

        Dim a As Integer
        a = e.UpdateValues.Count
        Dim tempvalue(a) As String

        For iLoop = 0 To a - 1
            'If e.UpdateValues(iLoop).NewValues("Qty").ToString() = 1 Then
            '    tempvalue(iLoop) = e.UpdateValues(iLoop).NewValues("Qty").ToString()
            'End If

            If gs_MaterialNo_Edit = "" Then
                gs_MaterialNo_Edit = "'" & Trim(e.UpdateValues(iLoop).NewValues("Material_No").ToString()) & "'"
            Else
                gs_MaterialNo_Edit = gs_MaterialNo_Edit & "," & "'" & Trim(e.UpdateValues(iLoop).NewValues("Material_No").ToString()) & "'"
            End If

            PRList.MaterialNo = Trim(e.UpdateValues(iLoop).NewValues("Material_No").ToString())

            'If e.UpdateValues(iLoop).NewValues("Qty") <> Nothing Then
            PRList.Qty = e.UpdateValues(iLoop).NewValues("Qty").ToString()
            'End If
            If e.UpdateValues(iLoop).NewValues("Remarks") <> Nothing Then
                PRList.Remarks = Trim(e.UpdateValues(iLoop).NewValues("Remarks").ToString())
            Else
                PRList.Remarks = ""
            End If

            If pErr <> "" Then
                Exit For
            End If

            ClsPRListDB.PRDetail_InsUpd(PRList, pUser, pErr)

        Next

        'gs_Message = tempvalue(a)

    End Sub

    Private Sub up_FillCombo2(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                             pAdminStatus As String, pUserID As String, _
                             Optional ByRef pGroup As String = "", _
                             Optional ByRef pParent As String = "", _
                             Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsPRListDB.FillComboDetail(pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

#End Region

#Region "Control Event"
    Private Sub up_Print()
        Dim status_urgent As String = ""
        
        'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/ViewPRAcceptance.aspx")
        If Trim(txtPRNo.Text) <> "--NEW--" Then
            Dim param As String = ""
            If gs_GeneratePRNo <> "" Then param = Left(gs_GeneratePRNo, 5)

            If Request.QueryString("ID") = Nothing Then

                If param = "DRAFT" Then
                    Session("PRNumber") = txtPRNo.Text & "|Draft|0"
                Else
                    rev = ClsPRListDB.GetRev_Submit(gs_GeneratePRNo)
                    Session("PRNumber") = gs_GeneratePRNo & "|Need Approval|" & rev
                End If

            Else
                If gs_GeneratePRNo = "" Then
                    Session("PRNumber") = txtPRNo.Text & "|" & Split(Request.QueryString("ID"), "|")(1) & "|" & Split(Request.QueryString("ID"), "|")(2)
                Else
                    Session("PRNumber") = gs_GeneratePRNo & "|" & Split(Request.QueryString("ID"), "|")(1) & "|" & Split(Request.QueryString("ID"), "|")(2)
                End If

            End If
        Else
            
            Session("PRNumber") = gs_GeneratePRNo & "|Draft|0|"
        End If

        If chkUrgent.Checked Then
            Response.Redirect("~/ViewPRListDetailUrgent.aspx")

        Else
            Response.Redirect("~/ViewPRListDetail.aspx")
        End If

    End Sub

    Protected Sub btnModify_Click(sender As Object, e As EventArgs) Handles btnModify.Click
        If Trim(txtPRNo.Text) <> "--NEW--" Then
            gs_GeneratePRNo = Trim(txtPRNo.Text)
        End If
        If Request.QueryString("ID") Is Nothing Then
            rev = ClsPRListDB.GetRev(txtPRNo.Text)
            Session("GetID") = txtPRNo.Text & "|" & "Draft" & "|" & rev
        Else
            Session("GetID") = txtPRNo.Text & "|" & Split(Request.QueryString("ID"), "|")(1) & "|" & Split(Request.QueryString("ID"), "|")(2)
        End If
        gs_Modify = True
        gs_FilterPRType = Trim(cboPRType.Text)  'cboPRType.SelectedItem.GetValue("Code").ToString()
        gs_FilterPRTypeIndex = cboPRType.Text

        Response.Redirect("~/PRListItem.aspx?ID=" + Session("GetID")) 'Request.QueryString("MatNo"))

        'Response.Redirect("~/PRListItem.aspx")


    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Session("Action") = "1"
        up_Print()
    End Sub
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        gs_Back = True
        If gs_NewPR = "1" Then
            gs_dtFromBack = Year(PRDate.Value) & "-" & Month(PRDate.Value) & "-01"
            gs_dtToBack = Now
            gs_PRType = 0 'cboPRType.SelectedIndex
            gs_Department = 0 'cboDepartment.SelectedIndex
            gs_Section = 0 'cboSection.SelectedIndex

            gs_NewPR = ""
            Response.Redirect("~/PRList.aspx")
        Else
            Response.Redirect("~/PRListItem.aspx")
        End If

    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Dim clsPR As New ClsPRList
        Dim ls_Department As String = ""
        Dim pErr As String = ""
        Dim pPRDate = Format(Now, "yyyy-MM-dd")

        If cboDepartment.Text <> "" Then
            ls_Department = tmpDepartment.Text 'cboDepartment.SelectedItem.GetValue("Code").ToString()
        End If

        If gs_GeneratePRNo = "--NEW--" Then
            clsPR.PRNumber = txtPRNo.Text
            'tempPRNo = gs_GeneratePRNo
        Else
            clsPR.PRNumber = gs_GeneratePRNo
        End If

        clsPR.PRDate = Format(PRDate.Value, "yyyy-MM-dd")
        clsPR.PRBudget = cboPRBudget.SelectedItem.GetValue("Code").ToString()
        clsPR.PRType = cboPRType.SelectedItem.GetValue("Code").ToString()
        clsPR.Department = ls_Department
        clsPR.Section = tmpSection.Text 'cboSection.SelectedItem.GetValue("Code").ToString()
        clsPR.CostCenter = tmpCostCenter.Text 'cboCostCenter.SelectedItem.GetValue("Code").ToString()

        If Trim(txtProject.Text) = "" Then
            clsPR.Project = ""
        Else
            clsPR.Project = Trim(txtProject.Text)
        End If

        If chkUrgent.Checked = True Then
            clsPR.Urgent = "Yes"
        Else
            clsPR.Urgent = "No"
        End If
        clsPR.ReqPOIssueDate = Format(PODate.Value, "yyyy-MM-dd")
        clsPR.UrgentNote = Trim(txtUrgentNote.Text)
        clsPR.StatusCls = "0"

        Dim pPRNo_Output As String = ""
        If gs_GeneratePRNo = "--NEW--" Then
            ClsPRListDB.PRHeader_Insert(clsPR, pUser, "0", pPRNo_Output, pErr) 'insert
            gs_GeneratePRNo = pPRNo_Output
            clsPR.PRNumber = gs_GeneratePRNo
        Else
            ClsPRListDB.PRHeader_Insert(clsPR, pUser, "1", pPRNo_Output, pErr) 'update
        End If

        If gs_Modify = True Then
            ClsPRListDB.uf_PRModif_Upd(gs_GeneratePRNo, MatNo)
            gs_Modify = False
        End If

        up_SaveDetail(clsPR, sender, e, pErr)
        'gs_Message = "Draft Data Saved Successfully"
        'If gs_Message = "Draft Data Saved Successfully" Then

        ' pPRNo = gs_GeneratePRNo
        ' ds = ClsPRListDB.GetDataPRDetail(cPRList, pPRNo, pUser, rev, pErr)
        ' Grid.DataSource = ds
        'Grid.DataBind()

        'End If

    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pMaterialNo As String = Split(e.Parameters, "|")(1)

        Select Case pFunction
            Case "Load"
                up_LoadData(gs_GeneratePRNo, "")
            Case "Save"
                Dim clsPR As New ClsPRList
                Dim ls_NewGenerate As String
                Dim ls_Department As String = ""
               
                Dim pErr As String = ""
               
                'If gs_GeneratePRNo <> "--NEW--" Then
                '    gs_GeneratePRNo = Trim(txtPRNo.Text)

                'End If

                If cboDepartment.Text <> "" Then
                    ls_Department = tmpDepartment.Text 'cboDepartment.SelectedItem.GetValue("Code").ToString()
                End If

                If gs_GeneratePRNo = "--NEW--" Then

                    If pMaterialNo = "" Then
                        pMaterialNo = ""
                    End If

                    'clsPR.PRNumber = ls_NewGenerate
                    'gs_GeneratePRNo = ls_NewGenerate
                    '' tempPRNo = ls_NewGenerate
                    clsPR.PRNumber = Trim(txtPRNo.Text)
                Else
                    clsPR.PRNumber = gs_GeneratePRNo
                End If

                clsPR.PRDate = Format(PRDate.Value, "yyyy-MM-dd HH:mm:ss")
                clsPR.PRBudget = cboPRBudget.SelectedItem.GetValue("Code").ToString()
                clsPR.PRType = cboPRType.SelectedItem.GetValue("Code").ToString()
                clsPR.Department = ls_Department
                clsPR.Section = tmpSection.Text 'cboSection.SelectedItem.GetValue("Code").ToString()
                clsPR.CostCenter = tmpCostCenter.Text 'cboCostCenter.SelectedItem.GetValue("Code").ToString()
                If Trim(txtProject.Text) = "" Then
                    clsPR.Project = ""
                Else
                    clsPR.Project = Trim(txtProject.Text)
                End If
                If chkUrgent.Checked = True Then
                    clsPR.Urgent = "Yes"
                Else
                    clsPR.Urgent = "No"
                End If
                clsPR.ReqPOIssueDate = Format(PODate.Value, "yyyy-MM-dd")
                clsPR.UrgentNote = Trim(txtUrgentNote.Text)
                clsPR.StatusCls = "0"

                Dim pPRNo_Output As String = ""
                If gs_GeneratePRNo = "--NEW--" Then
                    ClsPRListDB.PRHeader_Insert(clsPR, pUser, "0", pPRNo_Output, pErr) 'insert
                    gs_GeneratePRNo = pPRNo_Output
                    clsPR.PRNumber = gs_GeneratePRNo
                Else
                    ClsPRListDB.PRHeader_Insert(clsPR, pUser, "1", pPRNo_Output, pErr) 'update
                End If
                
                If gs_Modify = True Then
                    ClsPRListDB.uf_PRModif_Upd(gs_GeneratePRNo, MatNo)
                    gs_Modify = False
                End If

                Dim ds As DataSet
                If gs_MaterialNo_Edit = "" Then
                    ds = ClsPRListDB.LoadNewItem(gs_GeneratePRNo, Trim(txtFilter2.Text))
                Else
                    ds = ClsPRListDB.LoadNewItem(gs_GeneratePRNo, Trim(txtFilter2.Text), gs_MaterialNo_Edit)
                End If

                gs_MaterialNo_Edit = ""

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    clsPR.MaterialNo = Trim(ds.Tables(0).Rows(i).Item("Material_No"))
                    clsPR.Qty = ds.Tables(0).Rows(i).Item("Qty")
                    clsPR.Remarks = Trim(ds.Tables(0).Rows(i).Item("Remarks"))

                    ClsPRListDB.PRDetail_InsUpd(clsPR, pUser, pErr)
                Next
                'If gs_Message = "" Then
                gs_Message = "Draft Data Saved Successfully"

                'End If
                up_LoadData(gs_GeneratePRNo, "")
        End Select

    End Sub

    Private Sub cbApprove_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbApprove.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim perr As String = ""
        Dim ls_NewGenerate As String
        Dim ld_PRDate_M As String
        Dim ld_PRDate_Y As String
        Dim ls_Urut As String = ""
        Dim ls_Bln As String = ""
        Dim ls_Department As String = ""

        Select Case pFunction
            Case "draft"
                cbApprove.JSProperties("cpMessage") = gs_Message
                cbApprove.JSProperties("cpPRNo") = gs_GeneratePRNo
                rev = ClsPRListDB.GetRev(gs_GeneratePRNo)
                cbApprove.JSProperties("cpRev") = rev
                gs_NewPR = "1"

            Case "submit"
                Dim ls_Cek As String = Left(txtPRNo.Text, 5)
                Dim pPRDate As String

                'If ls_Cek = "DRAFT" Then
                '    If cboDepartment.Text <> "" Then
                '        ls_Department = tmpDepartment.Text 'cboDepartment.SelectedItem.GetValue("Code").ToString()
                '    End If
                '    pPRDate = Format(Now, "yyyy-MM-dd")

                '    ld_PRDate_M = Format(Now, "MM")
                '    ld_PRDate_Y = Format(Now, "yyyy")

                '    ls_Urut = ClsPRListDB.GetNoUrut(ls_Department, ld_PRDate_M, ld_PRDate_Y, "1")
                '    ls_Bln = uf_ConvertMonth(Format(Now, "MM"))

                '    ls_NewGenerate = "IAMI/PR/" & ls_Department & "/" & ls_Urut & "/" & ls_Bln & "/" & Format(Now, "yyyy")
                '    rev = 0

                'Else
                rev = ClsPRListDB.GetRev(txtPRNo.Text)
                'ls_NewGenerate = Trim(txtPRNo.Text)
                pPRDate = Format(Now, "yyyy-MM-dd") ' Format(PRDate.Value, "yyyy-MM-dd")
                ' End If

                ClsPRListDB.PRSubmit(Trim(txtPRNo.Text), pPRDate, pUser, rev, ls_NewGenerate)
                gs_GeneratePRNo = ls_NewGenerate
                cbApprove.JSProperties("cpPRNo") = ls_NewGenerate
                cbApprove.JSProperties("cpMessage") = "Submit Data Saved Successfully"

            Case "load"
                If Trim(txtPRNo.Text) = "--NEW--" Then
                    btnDraft.Enabled = False
                    btnModify.Enabled = False
                    btnSubmit.Enabled = False
                    btnDelete.Enabled = False
                Else
                    btnDraft.Enabled = True
                    btnModify.Enabled = True
                    btnSubmit.Enabled = True
                    btnDelete.Enabled = True
                End If
            Case "delete"
                ClsPRListDB.PRDelete(Trim(txtPRNo.Text))
                cbApprove.JSProperties("cpMessage") = "Delete Data Successfully"

        End Select
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
    End Sub

    Private Sub Grid_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles Grid.RowInserting
        e.Cancel = True
    End Sub

    Private Sub cbTmp_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbTmp.Callback
        Dim pFunction As String = e.Parameter

        Select Case pFunction
            Case "Code"

        End Select
    End Sub

    'Private Sub cboDepartment_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboDepartment.Callback
    '    Dim Parentgroup As String = Split(e.Parameter, "|")(1)
    '    Dim errmsg As String = ""

    '    Using conn As New SqlConnection(Sconn.Stringkoneksi)
    '        conn.Open()
    '        Dim Sql = "sp_PRListDetail_FilterDataCostCenter"
    '        Dim ds As New DataSet
    '        Dim cmd As New SqlCommand(Sql, conn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.Parameters.AddWithValue("Group", "CostCenter")
    '        cmd.Parameters.AddWithValue("UserID", pUser)

    '        Dim da As New SqlDataAdapter(cmd)
    '        da.Fill(ds)

    '        If (ds.Tables(0).Rows.Count = 1) Then
    '            cboCostCenter.Value = ds.Tables(0).Rows(0)("Code").ToString()
    '        End If

    '    End Using

    'End Sub
    Private Sub cboSection_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSection.Callback
        Dim Parentgroup As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        up_FillCombo2(cboSection, statusAdmin, pUser, "Section", Parentgroup, errmsg)
        cboSection.SelectedIndex = -1
    End Sub

    Private Sub cboCostCenter_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCostCenter.Callback
        Dim Parentgroup As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""


        up_FillCombo2(cboCostCenter, statusAdmin, pUser, "CostCenter", Parentgroup, errmsg)
        cboCostCenter.SelectedIndex = -1

        Using conn As New SqlConnection(Sconn.Stringkoneksi)
            conn.Open()
            Dim Sql = "sp_PRListDetail_FilterDataCostCenter"
            Dim ds As New DataSet
            Dim cmd As New SqlCommand(Sql, conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("Group", "CostCenter")
            cmd.Parameters.AddWithValue("UserID", pUser)

            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)

            If (ds.Tables(0).Rows.Count = 1) Then
                cboCostCenter.Value = ds.Tables(0).Rows(0)("Code").ToString()
            End If

        End Using
    End Sub

    Private Sub Grid_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If e.Column.FieldName = "Remarks" Then
            TryCast(e.Editor, ASPxTextBox).AutoCompleteType = Web.UI.WebControls.AutoCompleteType.Disabled
        End If
    End Sub
#End Region



   
End Class
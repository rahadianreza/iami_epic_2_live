﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ApprovalSetup.aspx.vb" Inherits="IAMI_EPIC2.ApprovalSetup" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
            <tr style="height: 50px">
                <td style=" width:100px; padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" 
                        GridViewID="GridPR">
                </dx:ASPxGridViewExporter>
                </td>
            </tr>
        </table>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <tr style="height: 50px">
            <dx:ASPxGridView ID="GridPR" runat="server" AutoGenerateColumns="False" 
                                    ClientInstanceName="GridPR" EnableTheming="True" Font-Names="Segoe UI" 
                                    Font-Size="9pt" KeyFieldName="Approval_Group;Approval_ID" 
                                    OnAfterPerformCallback="GridPR_AfterPerformCallback" 
                                    OnRowDeleting="GridPR_RowDeleting" OnRowInserting="GridPR_RowInserting" 
                                    OnRowValidating="GridPR_RowValidating" 
                                    OnStartRowEditing="GridPR_StartRowEditing" Theme="Office2010Black" 
                                    Width="100%">
                                    <ClientSideEvents EndCallback="OnEndCallback" />
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowClearFilterButton="True" ShowDeleteButton="True" 
                                            ShowEditButton="True" ShowInCustomizationForm="True" 
                                            ShowNewButtonInHeader="True" VisibleIndex="0" Width="100px">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataComboBoxColumn Caption = "Approval Group"  FieldName="Approval_Group" VisibleIndex="1">
                                            <PropertiesComboBox TextField="Approval_Group" ValueField="Approval_Group" EnableSynchronization="False"
                                                IncrementalFilteringMode="StartsWith" DataSourceID="SqlDataSource1">
                                            </PropertiesComboBox>
                                        </dx:GridViewDataComboBoxColumn>
                                        <dx:GridViewDataTextColumn Caption="Approval ID" FieldName="Approval_ID" 
                                            ShowInCustomizationForm="True" VisibleIndex="1" Width="100px">
                                            <PropertiesTextEdit ClientInstanceName="Approval_ID" MaxLength="15" 
                                                Width="115px">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="6px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Approval Level" FieldName="Approval_Level" 
                                            ShowInCustomizationForm="True" VisibleIndex="2" Width="100px">
                                            <PropertiesTextEdit ClientInstanceName="Approval_Level" MaxLength="25" 
                                                Width="200px">
                                                <Style HorizontalAlign="Left">
                                                </Style>
                                            </PropertiesTextEdit>
                                            <Settings AutoFilterCondition="Contains" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            <Paddings PaddingLeft="5px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Register By" FieldName="RegisterBy" 
                                            ShowInCustomizationForm="True" VisibleIndex="4" Width="150px">
                                            <PropertiesTextEdit ClientInstanceName="RegisterBy" Width="150px">
                                            </PropertiesTextEdit>
                                            <Settings AllowAutoFilter="False" />
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                            <Paddings PaddingLeft="5px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Register Date" FieldName="RegisterDate" 
                                            ShowInCustomizationForm="True" VisibleIndex="5" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <Settings AllowAutoFilter="False" />
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                            <Paddings PaddingLeft="5px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Update By" FieldName="UpdateBy" 
                                            ShowInCustomizationForm="True" VisibleIndex="6" Width="150px">
                                            <PropertiesTextEdit ClientInstanceName="UpdateBy" Width="150px">
                                            </PropertiesTextEdit>
                                            <Settings AllowAutoFilter="False" />
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                            <Paddings PaddingLeft="5px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="Update Date" FieldName="UpdateDate" 
                                            ShowInCustomizationForm="True" VisibleIndex="7" Width="100px">
                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" 
                                                EditFormatString="dd MMM yyyy">
                                            </PropertiesDateEdit>
                                            <Settings AllowAutoFilter="False" />
                                            <EditFormSettings Visible="False" />
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                            <Paddings PaddingLeft="5px" />
                                            </HeaderStyle>
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn Caption="Approval Name" 
                                            ShowInCustomizationForm="True" VisibleIndex="3" Width="100px" 
                                            FieldName="Approval_Name">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True" />
                                    <SettingsPager AlwaysShowPager="True">
                                    </SettingsPager>
                                    <SettingsEditing Mode="PopupEditForm">
                                    </SettingsEditing>
                                    <Settings HorizontalScrollBarMode="Auto" ShowFilterRow="True" 
                                        VerticalScrollableHeight="260" VerticalScrollBarMode="Auto" />
                                    <SettingsText ConfirmDelete="Are you sure want to delete ?" />
                                    <SettingsPopup>
                                        <EditForm HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" 
                                            Width="320px" />
                                    </SettingsPopup>
                                    <Styles>
                                        <Header>
                                            <Paddings Padding="2px" />
                                        </Header>
                                        <CommandColumnItem ForeColor="Orange">
                                        </CommandColumnItem>
                                        <EditFormColumnCaption Font-Names="Segoe UI" Font-Size="9pt">
                                            <Paddings PaddingBottom="5px" PaddingLeft="15px" PaddingRight="15px" 
                                                PaddingTop="5px" />
                                        </EditFormColumnCaption>
                                    </Styles>
                                    <Templates>
                                        <EditForm>
                                            <div style="padding: 15px 15px 15px 15px">
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" runat="server" 
                                                        ReplacementType="EditFormEditors" />
                                                </dx:ContentControl>
                                            </div>
                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" runat="server" 
                                                    ReplacementType="EditFormUpdateButton" />
                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" runat="server" 
                                                    ReplacementType="EditFormCancelButton" />
                                            </div>
                                        </EditForm>
                                    </Templates>
                                </dx:ASPxGridView>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="select A.Approval_Group FROM(select 'CE' Approval_Group UNION ALL select 'CP Tender' Approval_Group  UNION ALL select 'CP Non Tender' Approval_Group  UNION ALL Select 'PR' Approval_Group  UNION ALL Select 'PR Urgent' Approval_Group  UNION ALL Select 'RFQ' Approval_Group  UNION ALL Select 'SR' Approval_Group UNION ALL Select 'IA' Approval_Group) A">
                    </asp:SqlDataSource>
        </tr>
    </div>
</asp:Content>

﻿Imports DevExpress.XtraCharts
Imports System.Drawing
Imports DevExpress.XtraCharts.Web
Imports DevExpress.Web.ASPxCallbackPanel
Imports System.Data.SqlClient

Public Class ExchangeRateChart
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A090")
        Master.SiteTitle = "Exchange Rate Chart"
        pUser = Session("user")

        If Not Page.IsCallback And Not Page.IsPostBack Then
            Dim dfrom As Date
            dfrom = Year(Now) & "-01-01"
            dtDateFrom.Value = dfrom
            dtDateTo.Value = Now

            'cboBankCode.Text = "BOF"
            ' cboCurrencyCode.Text = "JPY"
            
        End If
    End Sub

    Protected Sub ASPxCallbackPanel1_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel1.Callback
        ' Dim wbc As New WebChartControl()
        'Dim ds As New DataSet
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim Bank As String = Split(e.Parameter, "|")(1)
        Dim Currency As String = Split(e.Parameter, "|")(2)
        Dim PeriodFrom As Date = dtDateFrom.Value
        Dim PeriodTo As Date = dtDateTo.Value

        If pFunction = "show" Then
            Dim pErr As String = ""
            Dim ds As DataSet = CreateData(PeriodFrom, PeriodTo, Bank, Currency, pErr)

            If pErr <> "" Then
                Exit Sub
            End If

            If ds.Tables(0).Rows.Count = 0 Then
                ASPxCallbackPanel1.JSProperties("cpMessage") = "There's no data to show!"
                wbc.Visible = False
                Exit Sub
            Else
                ASPxCallbackPanel1.JSProperties("cpMessage") = "OK"
                wbc.Visible = True
            End If

            wbc.DataSource = ds
            wbc.EmptyChartText.Text = "NO DATA AVAILABLE ON CHART"
            ' wbc.EmptyChartText.Font("Tahoma, 18pt")

            Dim chartTitle1 As New ChartTitle()
            ' Define the text for the titles. 
            Dim CurrencyCvt As String = cboCurrencyCode.Text & " / IDR"
            chartTitle1.Text = CurrencyCvt
            chartTitle1.Alignment = StringAlignment.Center
            chartTitle1.Dock = ChartTitleDockStyle.Top
            chartTitle1.Antialiasing = True
            chartTitle1.Font = New Font("Segoe UI", 14, FontStyle.Bold)
            wbc.Titles.Add(chartTitle1)

            'diagram swift plot
            Dim swiftPlotSeries As Series = New Series(cboCurrencyCode.Text & " BOT TT Middle", ViewType.SwiftPlot)
            CType(swiftPlotSeries.View, SwiftPlotSeriesView).Color = Color.Cyan
            Dim swiftPlotSeries2 As Series = New Series("Applied 6 Month ", ViewType.SwiftPlot)
            CType(swiftPlotSeries2.View, SwiftPlotSeriesView).Color = Color.Red
            Dim swiftPlotSeries3 As Series = New Series("Applied 3 Month", ViewType.SwiftPlot)
            CType(swiftPlotSeries3.View, SwiftPlotSeriesView).Color = Color.Yellow
            wbc.Series.AddRange(New Series() {swiftPlotSeries, swiftPlotSeries2, swiftPlotSeries3})
            swiftPlotSeries.ArgumentDataMember = "MonthYear"
            swiftPlotSeries.ValueDataMembers(0) = "AvgRateMonth"


            swiftPlotSeries2.ArgumentDataMember = "MonthYear"
            swiftPlotSeries2.ValueDataMembers(0) = "AvgRate6Month"
            swiftPlotSeries3.ArgumentDataMember = "MonthYear"
            swiftPlotSeries3.ValueDataMembers(0) = "AvgRate3Month"
            swiftPlotSeries.CrosshairLabelPattern = "{S} : {V:#,##0}"
            swiftPlotSeries2.CrosshairLabelPattern = "{S} : {V:#,##0}"
            swiftPlotSeries3.CrosshairLabelPattern = "{S} : {V:#,##0}"
            Dim Diagram = TryCast(wbc.Diagram, SwiftPlotDiagram)
            ' Dim spd As SwiftPlotDiagram = TryCast(wbc.Diagram, SwiftPlotDiagram)  'New SwiftPlotDiagram()
            ' If (Diagram Is Nothing) Then Return
            Diagram.AxisY.WholeRange.AlwaysShowZeroLevel = False
            Diagram.AxisY.Label.TextPattern = "{V:#,##0}"
            If cboCurrencyCode.Text = "USD" Or cboCurrencyCode.Text = "EUR" Then
                Diagram.AxisY.WholeRange.Auto = False
                'Diagram.AxisY.WholeRange.SetMinMaxValues(10000, 16000)
                Diagram.AxisY.NumericScaleOptions.AutoGrid = False
                Diagram.AxisY.WholeRange.SetMinMaxValues(CDbl(txtLimitFrom.Text), CDbl(txtLimitTo.Text))
                Diagram.AxisY.NumericScaleOptions.ScaleMode = ScaleMode.Manual
                Diagram.AxisY.NumericScaleOptions.GridAlignment = NumericGridAlignment.Thousands
                'Diagram.AxisY.NumericScaleOptions.CustomGridAlignment = 1
                Diagram.AxisY.NumericScaleOptions.GridSpacing = IIf(CDbl(txtLimitTo.Text) Mod 2000 = 0, 2, 1)

            ElseIf cboCurrencyCode.Text = "JPN" Or cboCurrencyCode.Text = "JPY" Then
                Diagram.AxisY.WholeRange.Auto = False
                Diagram.AxisY.NumericScaleOptions.AutoGrid = False
                Diagram.AxisY.WholeRange.SetMinMaxValues(CDbl(txtLimitFrom.Text), CDbl(txtLimitTo.Text)) '100,180
                Diagram.AxisY.NumericScaleOptions.ScaleMode = ScaleMode.Manual
                Diagram.AxisY.NumericScaleOptions.GridAlignment = NumericGridAlignment.Tens
                'Diagram.AxisY.NumericScaleOptions.CustomGridAlignment = 1
                Diagram.AxisY.NumericScaleOptions.GridSpacing = IIf(CDbl(txtLimitTo.Text) Mod 20 = 0, 2, 1)
            ElseIf cboCurrencyCode.Text = "THB" Then
                Diagram.AxisY.WholeRange.Auto = False
                Diagram.AxisY.NumericScaleOptions.AutoGrid = False
                Diagram.AxisY.WholeRange.SetMinMaxValues(CDbl(txtLimitFrom.Text), CDbl(txtLimitTo.Text)) '200,600
                Diagram.AxisY.NumericScaleOptions.ScaleMode = ScaleMode.Manual
                Diagram.AxisY.NumericScaleOptions.GridSpacing = 100
                'Diagram.AxisY.NumericScaleOptions.GridAlignment = NumericGridAlignment.Hundreds
                'Diagram.AxisY.NumericScaleOptions.CustomGridAlignment = 1
                'Diagram.AxisY.NumericScaleOptions.GridSpacing = 1 'IIf(CDbl(txtLimitTo.Text) Mod 200 = 0, 2, 1)
            End If
            Diagram.AxisX.Label.TextColor = Color.Yellow
            Diagram.AxisX.Label.Angle = 270
            Diagram.AxisX.Label.Font = New Font("Segoe UI", 7, FontStyle.Regular)
            'CType(spd.AxisY, SwiftPlotDiagramAxisY).WholeRange.Auto = False
            ' CType(spd.AxisX, SwiftPlotDiagramAxisX).Label.Font = New Font("Segoe UI", 11, FontStyle.Regular)

            wbc.AppearanceName = "Dark"
            wbc.CrosshairEnabled = DevExpress.Utils.DefaultBoolean.True

            'position legend 
            wbc.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Center
            wbc.Legend.AlignmentVertical = LegendAlignmentVertical.BottomOutside
            wbc.Legend.Direction = LegendDirection.LeftToRight

            wbc.DataBind()

            'wbc.Width = ASPxCallbackPanel1.Width.Value
            'wbc.Height = ASPxCallbackPanel1.Height.Value

            wbc.Width = (TryCast(sender, ASPxCallbackPanel)).Width
            wbc.Height = (TryCast(sender, ASPxCallbackPanel)).Height

            TryCast(sender, ASPxCallbackPanel).Controls.Add(wbc)


            ' CType(sender, ASPxCallbackPanel).Controls.Add(wbc)
        End If
    End Sub

    Private Function CreateData(pPeriodFrom As Date, pPeriodTo As Date, pBankCd As String, pCurrency As String, Optional ByRef MsgError As String = "") As DataSet
        Dim ds As New DataSet
        'Dim pErr As String = ""
        Dim PeriodFrom, PeriodTo
        Try
            PeriodFrom = Format(pPeriodFrom, "yyyy-MM-dd")
            PeriodTo = Format(pPeriodTo, "yyyy-MM-dd")

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_ExchangeRate_Chart", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@ExchangeRateDateFrom", PeriodFrom)
                cmd.Parameters.AddWithValue("@ExchangeRateDateTo", PeriodTo)
                cmd.Parameters.AddWithValue("@BankCode", pBankCd)
                cmd.Parameters.AddWithValue("@CurrencyCode", pCurrency)
                'cmd.Parameters.AddWithValue("@Interval", 2) 'hardcode only accept value 1 or 2

                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                Return ds
            End Using

        Catch ex As Exception
            MsgError = ex.Message
            Return Nothing
        End Try
    End Function



    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("ExchangeRate.aspx")
    End Sub
End Class
﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxImageZoom
Imports DevExpress.Web.Data
Imports System.Drawing.Printing
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ESign
    Inherits System.Web.UI.Page
    Private Declare Function WriteProfileString Lib "kernel32" Alias "WriteProfileStringA" _
        (ByVal lpszSection As String, ByVal lpszKeyName As String, _
        ByVal lpszString As String) As Long
    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
         (ByVal hwnd As Long, ByVal wMsg As Long, _
         ByVal wParam As Long, ByVal lparam As String) As Long
    Private Const HWND_BROADCAST As Long = &HFFFF&
    Private Const WM_WININICHANGE As Long = &H1A
    Dim UserLogin As String = ""
    Dim UserSign As String = ""

    Dim filenamefile As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UserID As String = Trim(Session("user") & "")                
        Dim GlobalPrm As String = ""

        UserLogin = UserID

        If Request.QueryString("prm") Is Nothing Then
            Exit Sub
        End If

        Dim ID As String = Request.QueryString("prm").ToString()
        UserSign = ID
        Dim Img As Object = Cls_UserSetup_DB.GetPicture(ID)
        If Img IsNot Nothing Then
            imgSign.Value = Img
        End If
    End Sub
	Public Enum MessageType
        Success
        [Error]
        Info
        Warning
    End Enum
    Protected Sub ShowMessage(ByVal Message As String, ByVal type As MessageType)
        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), System.Guid.NewGuid().ToString(), "ShowMessage('" & Message & "','" & type.ToString() & "');", True)
    End Sub					   
    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim errmsg As String = ""

        up_Save(errmsg)

        If errmsg = "File size must not exceed 2 MB" Then

        End If
    End Sub

    Private Sub up_Save(Optional ByRef pErr As String = "")
        Dim Sign As New Cls_UserSetup

        Try
            Dim PostedFile As HttpPostedFile = uploader1.PostedFile
            Dim FileName As String = ""
            Dim FileExtension As String = ""
            Dim Stream As Stream
            Dim Br As BinaryReader
            Dim Bytes As Byte()
              If Not IsNothing(PostedFile) Then
                FileName = Path.GetFileName(PostedFile.FileName)
                FileExtension = Path.GetExtension(FileName)
                Stream = PostedFile.InputStream
                Br = New BinaryReader(Stream)
                Bytes = Br.ReadBytes(Stream.Length)

                If Bytes.Length > 0 Then
                    Sign.Pic_Sign = Bytes
                End If

                Dim FileSize As Single = PostedFile.ContentLength / 1024 / 1024
                If FileSize > 2 Then
                    'show_save(MsgTypeEnum.ErrorMsg, "File size must not exceed 1 MB!")
                    'cbSave.JSProperties("cpMessage") = "File size must not exceed 2 MB"
                    pErr = "File size must not exceed 2 MB"
					ShowMessage(pErr, MessageType.Error)
					
                    Exit Sub
                End If

                Dim i As Integer

                Sign.UserID = UserSign                
                i = Cls_UserSetup_DB.Update_E_Sign(Sign, UserLogin)
                If i = 1 Then
					ShowMessage("Data Updated Successfully!", MessageType.Success)
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                    cbSave.JSProperties("cpPrm") = "?prm=" & UserSign
                End If
            End If

			Dim ID As String = Request.QueryString("prm").ToString()
            UserSign = ID
            Dim Img As Object = Cls_UserSetup_DB.GetPicture(ID)
            If Img IsNot Nothing Then
                imgSign.Value = Img
            End If

        Catch ex As Exception
			ShowMessage(ex.Message, MessageType.Error)
        End Try
    End Sub


    'Private Sub up_Upload(Optional ByRef pErr As String = "")
    '    Dim Sign As New Cls_UserSetup

    '        Dim PostedFile As HttpPostedFile = uploader1.PostedFile
    '        Dim FileName As String = ""
    '        Dim FileExtension As String = ""
    '        Dim Stream As Stream
    '        Dim Br As BinaryReader
    '        Dim Bytes As Byte()

    '    FileName = Path.GetFileName(PostedFile.FileName)
    '    filenamefile = Path.GetFileName(PostedFile.FileName)

    '            FileExtension = Path.GetExtension(FileName)
    '            Stream = PostedFile.InputStream
    '            Br = New BinaryReader(Stream)
    '            Bytes = Br.ReadBytes(Stream.Length)

    '    If Bytes.Length > 0 Then
    '        imgSign.Value = Bytes
    '    End If
    'End Sub



End Class
﻿<%@ Page Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeBehind="LabourChart.aspx.vb" Inherits="IAMI_EPIC2.LabourChart" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register assembly="DevExpress.XtraCharts.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
     <style type="text/css">
        .td-col-l 
        {
            padding:0px 0px 0px 10px;
            width:70px;
        }
        .td-col-m
        {
            width:20px;
        }
        .td-col-r
        {
            width:200px;
        }
        .td-col-f
        {
            width:50px;
        }
        .tr-height
        {
            height: 35px;
        }
            
        
    </style>
    <script type="text/javascript">
        function LoadChart() {
            ASPxCallbackPanel1.PerformCallback('show'|+cboProvince.GetValue());
        }

        //filter combobox regioncity berdasarkan province yang dipilih
        function FilterRegion() {
            cboRegion.PerformCallback('filter|' + cboProvince.GetValue());
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
 
   <%-- <div style="padding : 5px 5px 5px 5px">
       <dxchartsui:WebChartControl ID="WebChartControl1" runat="server">
        </dxchartsui:WebChartControl>
        
    </div>--%>
    <div style="padding: 5px 5px 5px 5px"> 
        <div style="padding: 5px 5px 5px 5px"> 
            <table runat="server" style="border : 1px solid; width:100%">
                <tr style="height:10px">
                    
                    <td style="padding:0px 0px 0px 10px; width:80px" ></td>
                    <td class="td-col-m"></td>
                    <td class="td-col-r"></td>

                    <td class="td-col-f"></td>

                    <td class="td-col-l" ></td>
                    <td class="td-col-m"></td>
                    <td class="td-col-r"></td>

                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td style="padding:0px 0px 0px 10px; width:80px">
                        <dx:ASPxLabel ID="lblPeriodFrom" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                       Text="Period From" ClientInstanceName="lblPeriodFrom">
                        </dx:ASPxLabel>
                    </td>
                    <td class="td-col-m"></td>
                    <td class="td-col-r">
                         <dx:ASPxSpinEdit ID="ASPxSpinEditPeriodFrom" ClientInstanceName="ASPxSpinEditPeriodFrom" runat="server"  NumberType="Integer" 
                                          Width="100px" Height="25px" />
                    </td>
                    <td class="td-col-f"></td>

                    <td style="width:70px">
                        <dx:ASPxLabel ID="lblPeriodTo" runat="server"  Font-Names="Segoe UI" Font-Size="9pt" 
                                       Text="Period To" ClientInstanceName="lblPeriodTo">
                        </dx:ASPxLabel>
                    </td>
                    <td class="td-col-m"></td>
                    <td class="td-col-r" >
                        <dx:ASPxSpinEdit ID="ASPxSpinEditPeriodTo" ClientInstanceName="ASPxSpinEditPeriodTo" runat="server"  NumberType="Integer" 
                          Width="100px" Height="25px"  />
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr class="tr-height">
                    <td style="padding:0px 0px 0px 10px; width:80px">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Province">
                        </dx:ASPxLabel>
                    </td>
                    <td></td>
                    <td class="td-col-r">
                         <dx:ASPxComboBox ID="cboProvince" runat="server" ClientInstanceName="cboProvince"  DataSourceID="SqlDataSource1"
                            Width="170px" Font-Names="Segoe UI" TextField="ProvinceDesc" ValueField="Province"
                            TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="FilterRegion" />   
                            <%--<ClientSideEvents SelectedIndexChanged="LoadChart"/>--%>
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Province" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="ProvinceDesc" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                          </dx:ASPxComboBox>
                    </td>
                    <td colspan="6"></td>
                </tr>
                <tr style="height:10px">
                     <td style="padding:0px 0px 0px 10px; width:80px">
                        <dx:ASPxLabel ID="lblCity" runat="server" Font-Names="Segoe UI" Font-Size="9pt" ClientInstanceName="lblCity"
                            Text="City / Region">

                        </dx:ASPxLabel>
                    </td>
                    <td></td>
                    <td class="td-col-r">
                        <dx:ASPxComboBox ID="cboRegion" runat="server" ClientInstanceName="cboRegion"
                            Width="170px" Font-Names="Segoe UI" TextField="RegionDesc" ValueField="Region"
                            TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents ValueChanged="function(s, e) {
	        txtRegion.SetText(cboRegion.GetValue());
        }"  />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Region" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="RegionDesc" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtRegion" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="80px" ClientInstanceName="txtRegion" MaxLength="50" 
                            ClientVisible="False">                     
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr class="tr-height">
                    <td style="padding:0px 0px 0px 10px" colspan="6">
                        <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnBack" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;&nbsp;
                         <dx:ASPxButton ID="btnShow" runat="server" Text="Show" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShow" Theme="Default">
                         <ClientSideEvents Click="function(s, e) {
                             if (cboProvince.GetText()==''){
                                     toastr.warning('Please select Province', 'Warning');
                                     toastr.options.closeButton = false;
                                     toastr.options.debug = false;
                                     toastr.options.newestOnTop = false;
                                     toastr.options.progressBar = false;
                                     toastr.options.preventDuplicates = true;
                                     toastr.options.onclick = null;
                                     e.processOnServer = false;
                                     return;
                                 }         
                                 
                            if (cboRegion.GetText()==''){
                                     toastr.warning('Please select City/Region', 'Warning');
                                     toastr.options.closeButton = false;
                                     toastr.options.debug = false;
                                     toastr.options.newestOnTop = false;
                                     toastr.options.progressBar = false;
                                     toastr.options.preventDuplicates = true;
                                     toastr.options.onclick = null;
                                     e.processOnServer = false;
                                     return;
                            } 
                                             
	                        ASPxCallbackPanel1.PerformCallback('show|'+cboProvince.GetValue()+'|'+cboRegion.GetValue());
   
   
}" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td colspan="3"></td>
                </tr>
                <tr style="height:10px">
                    <td colspan="9"></td>
                </tr>
            </table>

        
          </div>
    </div>
    <div style="padding :5px 5px 5px 5px">
        <table runat="server" style="width:100%; border : 1px solid">
            <tr>
                <td style="padding:5px 5px 5px 5px">
                     <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" ClientInstanceName="ASPxCallbackPanel1" runat="server" >
                    </dx:ASPxCallbackPanel>
                </td>
            </tr>
        </table>
    </div>
    <div>
       
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Province, Rtrim(Par_Description) ProvinceDesc  From Mst_Parameter Where Par_Group = 'Provinsi'">
        </asp:SqlDataSource>
        <dx:ASPxCallback ID="ASPxCallback1" runat="server">
        </dx:ASPxCallback>
    </div>
    
</asp:Content>


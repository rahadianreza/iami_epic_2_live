﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class PRAcceptanceDetail
    Inherits System.Web.UI.Page

    Dim pUser As String
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False

    Private Sub up_FillCombo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsPRAcceptanceDB.GetComboData("PRBudget", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPRBudget.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("PRType", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPRType.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("Department", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboDepartment.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("Section", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboSection.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("CostCenter", pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboCostCenter.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If
    End Sub

    Private Sub up_LoadData(pPRNo As String, pRev As Integer)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim cPRAcceptance As New clsPRAcceptance

        Try
            cPRAcceptance.PRNumber = pPRNo
            cPRAcceptance.Revision = pRev

            ds = clsPRAcceptanceDB.GetDataPR(cPRAcceptance, pErr)
            If pErr = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    dtDate.Value = ds.Tables(0).Rows(0)("PR_Date")
                    cboPRBudget.SelectedIndex = cboPRBudget.Items.IndexOf(cboPRBudget.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Budget_Code") & "")))
                    cboPRType.SelectedIndex = cboPRType.Items.IndexOf(cboPRType.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("PRType_Code") & "")))
                    cboDepartment.SelectedIndex = cboDepartment.Items.IndexOf(cboDepartment.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Department_Code") & "")))
                    cboSection.SelectedIndex = cboSection.Items.IndexOf(cboSection.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("Section_Code") & "")))
                    cboCostCenter.SelectedIndex = cboCostCenter.Items.IndexOf(cboCostCenter.Items.FindByValue(Trim(ds.Tables(0).Rows(0)("CostCenter") & "")))
                    txtProject.Text = ds.Tables(0).Rows(0)("Project")
                    txtRev.Text = ds.Tables(0).Rows(0)("Rev")
					txtPRRegister.Text = ds.Tables(0).Rows(0)("PR_Register").ToString().Trim																		
                    Dim PRStatus As String

                    If IsDBNull(ds.Tables(0).Rows(0)("Urgent_Status")) Then
                        chkUrgent.Checked = False
                        cbApprove.JSProperties("cpUrgent") = "0"
                    ElseIf ds.Tables(0).Rows(0)("Urgent_Status").ToString.ToUpper = "YES" Then
                        chkUrgent.Checked = True
                        cbApprove.JSProperties("cpUrgent") = "1"
                    ElseIf ds.Tables(0).Rows(0)("Urgent_Status").ToString.ToUpper = "NO" Then
                        chkUrgent.Checked = False
                        cbApprove.JSProperties("cpUrgent") = "0"
                    End If

                    If Not IsDBNull(ds.Tables(0).Rows(0)("Req_POIssueDate")) Then
                        dtRequestPO.Value = ds.Tables(0).Rows(0)("Req_POIssueDate")
                    End If

                    If IsDBNull(ds.Tables(0).Rows(0)("Urgent_Note")) Then
                        txtUrgentNote.Text = ""
                    Else
                        txtUrgentNote.Text = ds.Tables(0).Rows(0)("Urgent_Note")
                    End If

                    If IsDBNull(ds.Tables(0).Rows(0)("Acceptance_Note")) Then
                        txtAcceptanceNote.Text = ""
                    Else
                        txtAcceptanceNote.Text = ds.Tables(0).Rows(0)("Acceptance_Note")
                    End If

                    If IsDBNull(ds.Tables(0).Rows(0)("PR_Status")) Then
                        PRStatus = "0"""
                    Else
                        PRStatus = ds.Tables(0).Rows(0)("PR_Status")
                    End If


                    If PRStatus = "3" Then
                        btnApprove.Enabled = False
                        btnReject.Enabled = False
                    End If


                    Grid.DataSource = ds
                    Grid.DataBind()
                End If
            End If
        Catch ex As Exception
            cbApprove.JSProperties("cpMessage") = ex.Message
        End Try
    End Sub

    Private Sub cbApprove_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbApprove.Callback
        up_Approve()
    End Sub

    Private Sub up_Approve()
        Dim PRAcceptance As New clsPRAcceptance
        Dim MsgErr As String = ""
        Dim i As Integer

        PRAcceptance.PRNumber = txtPRNo.Text
        PRAcceptance.AcceptanceNote = txtAcceptanceNote.Text
        PRAcceptance.UrgentNote = txtUrgentNote.Text
        PRAcceptance.Revision = txtRev.Text
        If chkUrgent.Value = 1 Then
            PRAcceptance.UrgentCls = "Yes"
            PRAcceptance.ReqPOIssueDate = Format(dtRequestPO.Value, "yyyy-MM-dd")
        Else
            PRAcceptance.UrgentCls = "No"
            PRAcceptance.ReqPOIssueDate = ""
        End If

        clsPRAcceptanceDB.ApproveFull(PRAcceptance, pUser, MsgErr)
        'clsPRAcceptanceDB.Approve(PRAcceptance, pUser, MsgErr)

        'If MsgErr = "" Then
        '    clsPRAcceptanceDB.ApprovePRApproval(PRAcceptance, pUser, MsgErr)
        'End If

        If MsgErr = "" Then
            cbApprove.JSProperties("cpMessage") = "Data Has Been Accepted Successfully"
        Else
            cbApprove.JSProperties("cpMessage") = MsgErr
        End If
    End Sub

    Private Sub up_Reject()
        Dim PRAcceptance As New clsPRAcceptance
        Dim MsgErr As String = ""

        PRAcceptance.PRNumber = txtPRNo.Text
        PRAcceptance.AcceptanceNote = txtAcceptanceNote.Text

        'clsPRAcceptanceDB.UpdateStatusPR(PRAcceptance, pUser, MsgErr)
        If MsgErr = "" Then
            clsPRAcceptanceDB.Reject(PRAcceptance, pUser, MsgErr)
        End If

        If MsgErr = "" Then
            cbReject.JSProperties("cpMessage") = "Data Has Been Reject Successfully"
        Else
            cbReject.JSProperties("cpMessage") = MsgErr
        End If
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|B030", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnApprove.SetEnabled(false);" & vbCrLf & _
                         "btnReject.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboDepartment)
            End Try
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim prno As String
        Dim rev As Integer

        pUser = Session("user")

        prno = Split(Request.QueryString("ID"), "|")(0)
        rev = Split(Request.QueryString("ID"), "|")(1)

        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "B030")
        If AuthUpdate = False Then
            btnApprove.Enabled = False
            btnReject.Enabled = False
        End If

        Master.SiteTitle = "PURCHASE REQUEST ACCEPTANCE DETAIL"

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            txtPRNo.Text = prno
            up_FillCombo()
            up_LoadData(prno, rev)

            gs_Back = True
        End If

        cbApprove.JSProperties("cpMessage") = ""
    End Sub

    Private Sub up_Print()
        'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/ViewPRAcceptance.aspx")
        Session("PRNumber") = txtPRNo.Text
        Session("Revision") = txtRev.Text
        Response.Redirect("~/ViewPRAcceptance.aspx")

    End Sub

   
    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        up_Print()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        gs_Back = True
        Response.Redirect("~/PRAcceptance.aspx")
    End Sub

  
    'Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
    '    up_Approve()
    'End Sub

    'Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
    '    up_Reject()
    'End Sub

   

    Private Sub cbReject_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbReject.Callback
        up_Reject()
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

End Class
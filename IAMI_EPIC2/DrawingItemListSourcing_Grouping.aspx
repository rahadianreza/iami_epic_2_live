﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="DrawingItemListSourcing_Grouping.aspx.vb" Inherits="IAMI_EPIC2.DrawingItemListSourcing_Grouping" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };

         function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "Part_No" || currentColumnName == "Commodity" || currentColumnName == "Upc") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        };  

        function SaveData(s, e) {
//            cbkValid.PerformCallback('save');
            Grid.UpdateEdit()
        }

        function disableenablebuttonsubmit(_val) {
        alert('adsds');
            if (_val = '1') {
                btnSave.SetEnabled(false)
            } else {
                btnSave.SetEnabled(true)
             }
        }

          function OnUpdateCheckedChanged(s, e) {
            if (s.GetValue() == -1) s.SetValue(1);
            Grid.SetFocusedRowIndex(-1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "Status_Group", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "Status_Group", s.GetValue());
                }                
            }
        }

    </script>
    <style>
        .statusBar
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="border: 1px solid black">
            <table style="width: 100%;">
                <tr style="height: 10px">
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Status Grouping">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">

                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--satu--%>
                        <dx:ASPxComboBox ID="cboStatusDrawing" runat="server" Value='<%# Bind("ACTIVE") %>'
                            ValueType="System.String" Height="25px" Font-Size="9pt" Theme="Office2010Black"
                            Width="120px" TabIndex="1">
                            <Items>
                                <dx:ListEditItem Text="ALL" Value="X" />
                                <dx:ListEditItem Text="Yes" Value="1" />
                                <dx:ListEditItem Text="No" Value="0" />
                            </Items>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Project Name">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Dua--%>
                        <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                            Font-Names="Segoe UI" TextField="Project_Name" ValueField="Project_ID" TextFormatString="{1}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboGroup.PerformCallback('filter|' + cboProject.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Commodity">
                        </dx:ASPxLabel>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Tiga--%>
                        <dx:ASPxComboBox ID="cboCommodity" runat="server" ClientInstanceName="cboCommodity"
                            Font-Names="Segoe UI" TextField="Commodity" ValueField="Commodity" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                     
                           
                        }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Commodity" FieldName="Commodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                <tr style="height: 10px">
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Project Type">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Empat--%>
                        <dx:ASPxComboBox ID="cboProjectType" runat="server" ClientInstanceName="cboProjectType"
                            TabIndex="2" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboProject.PerformCallback('filter|' + cboProjectType.GetSelectedItem().GetColumnText(0));
                                cboGroup.PerformCallback('filter| ' );
                                cboCommodity.PerformCallback('filter| ' );
                               
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                    <td style="padding: 10px 0px 0px 10px" class="style1">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Group">
                        </dx:ASPxLabel>
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Lima--%>
                        <dx:ASPxComboBox ID="cboGroup" runat="server" ClientInstanceName="cboGroup" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Group_ID" ValueField="Group_ID"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboCommodity.PerformCallback('filter|' + cboGroup.GetSelectedItem().GetColumnText(0));
                             
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Group ID" FieldName="Group_ID" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td align="left">
                    </td>
                    <td style="padding: 0px 0px 0px 10px" class="style1">
                        <%--<dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="PIC">
                        </dx:ASPxLabel>--%>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="padding: 10px 0px 0px 10px">
                        <%--Enam--%>
                  
                    </td>
                </tr>
            </table>
            <table>
                <tr style="height: 50px">
                    <td style="width: 80px; padding: 10px 0px 0px 10px">
                        <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShowData" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td style="width: 80px; padding: 10px 0px 0px 10px">
                        <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td style="width: 80px; padding: 10px 0px 0px 10px">
                        <dx:ASPxButton ID="btnSave" runat="server" Text="Grouping" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnSave" Theme="Default">
                            <Paddings Padding="2px" />
                            <ClientSideEvents Click="SaveData" />
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td style="width: 80px; padding: 10px 0px 0px 10px">
                        <dx:ASPxButton ID="btnSubmit" runat="server" Text="Acceptance" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnSubmit" Theme="Default" Visible="false">
                            <Paddings Padding="2px" />
                            <ClientSideEvents Click="function(s, e) {
	                        cbupload.PerformCallback('');
                        }" />
                        </dx:ASPxButton>
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="Select Project_ID, Project_Name From Proj_Header"></asp:SqlDataSource>
                        &nbsp;<dx:ASPxCallback ID="cbupload" runat="server" ClientInstanceName="cbupload">
                        </dx:ASPxCallback>
                        &nbsp;<asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                            SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Language'">
                        </asp:SqlDataSource>
                        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" KeyFieldName="Project_ID; Part_No; Group_ID; Commodity; Upc; Fna; Dtl_Upc; Dtl_Fna; Usg_Seq; Lvl; Hand; D_C; Dwg_No"
            Theme="Office2010Black" Width="100%" Font-Names="Segoe UI" Font-Size="9pt">
            <ClientSideEvents EndCallback="OnEndCallback" CustomButtonClick="OnCustomButtonClick"
                BatchEditStartEditing="BatchEditStartEditing" />
            <Columns>
                <dx:GridViewDataCheckColumn Caption=" " FieldName="Status_Group" Name="Status_Group"
                    VisibleIndex="0">
                    <PropertiesCheckEdit ValueChecked="1" ValueType="System.String" ValueUnchecked="0"
                        AllowGrayedByClick="false">
                    </PropertiesCheckEdit>
                  <%--  <HeaderCaptionTemplate>
                        <dx:ASPxCheckBox ID="chkDrawing" runat="server" ClientInstanceName="chkDrawing" ClientSideEvents-CheckedChanged="OnUpdateCheckedChanged"
                            ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="" Font-Names="Segoe UI"
                            Font-Size="8pt" ForeColor="White">
                        </dx:ASPxCheckBox>
                    </HeaderCaptionTemplate>--%>
                    <HeaderStyle HorizontalAlign="Center" />
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataTextColumn Caption="Group" FieldName="Group_ID" VisibleIndex="1"
                    Width="120px">
                    <PropertiesTextEdit MaxLength="15" Width="115px" ClientInstanceName="Group_ID">
                        <Style HorizontalAlign="Left">
                            
                        </Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Commodity" Width="120px" Caption="Commodity"
                    VisibleIndex="2">
                    <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Commodity">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn FieldName="Group_Comodity" Width="120px" Caption="Sourcing Group"
                    VisibleIndex="2">
                    <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Group_Comodity">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Upc" FieldName="Upc" VisibleIndex="3" Width="120px">
                    <PropertiesTextEdit Width="350px" DisplayFormatInEditMode="True" ClientInstanceName="Upc">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Fna" FieldName="Fna" VisibleIndex="4" Width="100px">
                    <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="True" ClientInstanceName="Fna">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Dtl Upc" FieldName="Dtl_Upc" VisibleIndex="5"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Dtl_Upc">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Dtl Fna" FieldName="Dtl_Fna" VisibleIndex="5"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Dtl_Fna">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Usg Seq" FieldName="Usg_Seq" VisibleIndex="6"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Usg_Seq">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="lvl" FieldName="Lvl" VisibleIndex="7" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Lvl">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Hand" FieldName="Hand" VisibleIndex="8" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Hand">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="D C" FieldName="D_C" VisibleIndex="9" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="D_C">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Dwg No" FieldName="Dwg_No" VisibleIndex="10"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Dwg_No">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Part No" FieldName="Part_No" VisibleIndex="11"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Part_No">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Part No 9 Digit" FieldName="Part_No_9_Digit"
                    VisibleIndex="12" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Part_No_9_Digit">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Part Name" FieldName="Part_Name" VisibleIndex="13"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Part_Name">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Qty" FieldName="Qty" VisibleIndex="14" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Qty">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <%--  Area of variant--%>
                <%--End Of Variant--%>
                <dx:GridViewDataTextColumn Caption="PIC" FieldName="PIC" VisibleIndex="16" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="PIC">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Guide Price" FieldName="Guide_Price" VisibleIndex="17"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Guide_Price">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Jpn Supplier" FieldName="Jpn_Supplier" VisibleIndex="18"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Jpn_Supplier">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Drawing Receive" FieldName="Drawing_Receive"
                    VisibleIndex="19" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Drawing_Receive" DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Send To Buyer" FieldName="Send_To_Buyer" VisibleIndex="20"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Send_To_Buyer" DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Drawing Check" FieldName="Drawing_Check" VisibleIndex="21"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Drawing_Check" DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Latest Cost Report" FieldName="Latest_Cost_Report"
                    VisibleIndex="22" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Latest_Cost_Report">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Part No Oes" FieldName="Part_No_Oes" VisibleIndex="23"
                    Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Part_No_Oes">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Purchasing Group" FieldName="Purchasing_Group"
                    VisibleIndex="24" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Purchasing_Group">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register By" FieldName="Register_By" VisibleIndex="25"
                    Width="150px">
                    <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Register Date" FieldName="Register_Date" VisibleIndex="26"
                    Width="100px">
                    <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd MMM yyyy">
                    </PropertiesDateEdit>
                    <EditFormSettings Visible="False" />
                    <Settings />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="Update By" FieldName="Update_By" VisibleIndex="27"
                    Width="150px">
                    <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Update Date" FieldName="Update_Date" VisibleIndex="28"
                    Width="100px">
                    <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd MMM yyyy">
                    </PropertiesDateEdit>
                    <EditFormSettings Visible="False" />
                    <Settings />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataDateColumn FieldName="Drawing_Complete" Visible="false" Width="100px">
                    <EditFormSettings Visible="False" />
                    <Settings />
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataDateColumn>
            </Columns>
            <ClientSideEvents SelectionChanged="grid_SelectionChanged" />
            <SettingsBehavior ConfirmDelete="True" AllowFocusedRow="True" AllowSelectByRowClick="True"
                AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
            <SettingsEditing EditFormColumnCount="2" Mode="Batch" />
            <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260"
                HorizontalScrollBarMode="Auto" />
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="320" />
            </SettingsPopup>
            <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header HorizontalAlign="Center">
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
                <StatusBar CssClass="statusBar">
                </StatusBar>
            </Styles>
        </dx:ASPxGridView>
        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbkValid" runat="server" ClientInstanceName="cbkValid">
            <ClientSideEvents EndCallback="SavePrivilege" />
        </dx:ASPxCallback>
     
    </div>
</asp:Content>

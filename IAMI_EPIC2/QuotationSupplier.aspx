﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="QuotationSupplier.aspx.vb" Inherits="IAMI_EPIC2.QuotationSupplier" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };

        function MessageError(s, e) {
            if (s.cpMessage == "Data completed successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
      
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }

        };
       

    function GetCheckBoxValue(s, e) {
        var value = s.GetChecked();

        if (value == true) {
            spr.SetText(0);
            spr.SetEnabled(false);
        } else {
            spr.SetEnabled(true);
        }
    }

//         function formatCurrency(angka, prefix) {
//          var number_string = angka.replace(/[^,\d]/g, "").toString(),
//            split = number_string.split(","),
//            sisa = split[0].length % 3,
//            rupiah = split[0].substr(0, sisa),
//            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

//          // tambahkan titik jika yang di input sudah menjadi angka ribuan
//          if (ribuan) {
//            separator = sisa ? "." : "";
//            rupiah += separator + ribuan.join(".");
//          }

//          rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
//          return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
//        }

    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2
    })

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="border: 1px solid black">
        <table>
            <tr>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Project ID">
                    </dx:ASPxLabel>
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                        Font-Names="Segoe UI" TextField="Project_Name" ValueField="Project_ID" TextFormatString="{1}"
                        Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                        EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                cboGroup.PerformCallback('filter|' + cboProject.GetSelectedItem().GetColumnText(0));
                    CurrCB.PerformCallback('');
                }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                            <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td>
                </td>
                <td style="padding: 10px 0px 0px 30px; width">
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Commodity">
                    </dx:ASPxLabel>
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxComboBox ID="cboCommodity" runat="server" ClientInstanceName="cboCommodity"
                        Font-Names="Segoe UI" TextField="Commodity" ValueField="Commodity" TextFormatString="{0}"
                        Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                        EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                            cboPIC.PerformCallback('filter|' + cboCommodity.GetSelectedItem().GetColumnText(0));
                            cboGroupCommodity.PerformCallback('filter|' + cboCommodity.GetSelectedItem().GetColumnText(0));
                        }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="Commodity" FieldName="Commodity" Width="100px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td>
                </td>
                <td style="padding: 10px 0px 0px 30px; width">
                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="PIC">
                    </dx:ASPxLabel>
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px">
                    <%--Enam--%>
                    <dx:ASPxComboBox ID="cboPIC" runat="server" ClientInstanceName="cboPIC" Width="120px"
                        TabIndex="6" Font-Names="Segoe UI" TextField="PIC" ValueField="PIC" TextFormatString="{0}"
                        Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                        EnableIncrementalFiltering="True" Height="25px">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                          cbComplete.PerformCallback('');
                            cbStatus.PerformCallback('');
                            }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="PIC" FieldName="PIC" Width="100px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td style="padding: 10px 0px 0px 150px; vertical-align: top;" rowspan="2">
                    <table>
                        <tr>
                            <td style="font-family: Segoe UI; font-size: 9pt;" colspan="3">
                               <b>Currency Rate</b> 
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                USD &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                : &nbsp
                            </td>
                            <td>
                                <dx1:ASPxLabel ID="lblUSDRate" runat="server" ClientInstanceName="lblUSDRate"  Font-Names="Segoe UI" Font-Size="9pt"></dx1:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                JPY &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                : &nbsp
                            </td>
                            <td>
                                <dx1:ASPxLabel ID="lblJPYRate" runat="server" ClientInstanceName="lblJPYRate"  Font-Names="Segoe UI" Font-Size="9pt"></dx1:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                THB &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                : &nbsp
                            </td>
                            <td>
                                <dx1:ASPxLabel ID="lblTHBRate" runat="server" ClientInstanceName="lblTHBRate" Font-Names="Segoe UI" Font-Size="9pt"></dx1:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Group ID">
                    </dx:ASPxLabel>
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxComboBox ID="cboGroup" runat="server" ClientInstanceName="cboGroup" Width="120px"
                        TabIndex="4" Font-Names="Segoe UI" TextField="Group_ID" ValueField="Group_ID"
                        TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                cboCommodity.PerformCallback('filter|' + cboGroup.GetSelectedItem().GetColumnText(0));
                    }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="Group ID" FieldName="Group_ID" Width="100px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td>
                </td>
                <td style="padding: 10px 0px 0px 10px" class="style1">
                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Sourcing Group">
                    </dx:ASPxLabel>
                </td>
                <td style="width: 20px">
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px">
                    <%--Lima--%>
                    <dx:ASPxComboBox ID="cboGroupCommodity" runat="server" ClientInstanceName="cboGroupCommodity"
                        Font-Names="Segoe UI" TextField="Group_Comodity" ValueField="Group_Comodity"
                        TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px"
                        TabIndex="5">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                    
                            }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="Sourcing Group" FieldName="Group_Comodity" Width="100px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td style="padding: 0 10px 0 10px;">
                    <asp:Label ID="Label1" runat="server" Text="Status" Font-Names="Segoe UI" Font-Size="9pt"></asp:Label>
                </td>
                <td>
                    :
                </td>
                <td style="padding: 0 10px 0 10px;">
                    <dx1:ASPxLabel ID="lblStatus" runat="server" Text=" " ClientInstanceName="lblStatus"
                        Font-Names="Segoe UI" Font-Size="9pt">
                    </dx1:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td style="padding: 0 10px 0 10px;">
                    <asp:Label ID="Label2" runat="server" Text="Remarks Status" Font-Names="Segoe UI"
                        Font-Size="9pt"></asp:Label>
                </td>
                <td>
                    :
                </td>
                <td style="padding: 0 10px 0 10px;">
                    <dx1:ASPxLabel ID="lblRemarksStatus" runat="server" Text="" ClientInstanceName="lblRemarksStatus"
                        Font-Names="Segoe UI" Font-Size="9pt">
                    </dx1:ASPxLabel>
                </td>
            </tr>
        </table>
        <table>
            <tr style="height: 50px">
                 <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnBacktoList" runat="server" Text="Back to List" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnBacktoList" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>

                </td>
                <td style="width: 5px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">
                        <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 5px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 5px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnMaterialCost" runat="server" Text="Material Cost" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnMaterialCost" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 5px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnPurchasedPartCost" runat="server" Text="Purchased Part Cost"
                        UseSubmitBehavior="False" Font-Names="Segoe UI" Font-Size="9pt" Width="80px"
                        Height="25px" AutoPostBack="False" ClientInstanceName="btnPurchasedPartCost"
                        Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 5px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnManufacturingCost" runat="server" Text="Manufacturing Cost"
                        UseSubmitBehavior="False" Font-Names="Segoe UI" Font-Size="9pt" Width="80px"
                        Height="25px" AutoPostBack="False" ClientInstanceName="btnManufacturingCost"
                        Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 5px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnPackingChargeCost" runat="server" Text="Packing Charge Cost"
                        UseSubmitBehavior="False" Font-Names="Segoe UI" Font-Size="9pt" Width="80px"
                        Height="25px" AutoPostBack="False" ClientInstanceName="btnPackingChargeCost"
                        Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 5px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnToolingCost" runat="server" Text="Tooling Cost" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnToolingCost" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 5px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnComplete" runat="server" Text="Complete" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnComplete" Theme="Default">
                        <ClientSideEvents Click="function(s, e) {
	 var msg = confirm('Are you sure want to complete this data ?');
                    if (msg == false) {
                        e.processOnServer = false;
                        return;
                    }
}" />
                        <Paddings Padding="2px" />
                        
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
            Font-Size="9pt" KeyFieldName="Part_No">
            <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataCheckColumn Caption="Material Check" FieldName="Material_Check" VisibleIndex="1"
                    Visible="false">
                    <EditFormSettings Visible="True" />
                    <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueType="System.Char">
                    </PropertiesCheckEdit>
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn Caption="Purchased Check" FieldName="Purchased_Check"
                    VisibleIndex="1" Visible="false">
                    <EditFormSettings Visible="True" />
                    <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueType="System.Char">
                    </PropertiesCheckEdit>
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn Caption="Manufacturing Check" FieldName="Manufacturing_Check"
                    VisibleIndex="1" Visible="false">
                    <EditFormSettings Visible="True" />
                    <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueType="System.Char">
                    </PropertiesCheckEdit>
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataTextColumn Caption="Part No" VisibleIndex="1" FieldName="Part_No"
                    Width="150px">
                    <EditFormSettings Visible="false" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Part Name" FieldName="Part_Name" VisibleIndex="2"
                    Width="150px">
                    <EditFormSettings Visible="false" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(A) <br/> Material Cost" VisibleIndex="3" FieldName="AmtMaterialCost"
                    Width="120px">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="true" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(B) <br/> Purchased Cost" VisibleIndex="4" FieldName="AmtPurchasedCost" Width="150">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(C) <br/> Manufacturing Cost" VisibleIndex="5" Width="130px"
                    FieldName="AmtManufacturingCost">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(A + B + C) <br/> Sub Total <br/> (D)" VisibleIndex="6" FieldName="SubTotal">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="OverHead" VisibleIndex="6" FieldName="OverHead">
                    <PropertiesTextEdit DisplayFormatString="{0}%" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g> %" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(E) <br/> Amount OverHead" VisibleIndex="6" FieldName="AmtOverHead" Width="170">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Profit" VisibleIndex="6" FieldName="Profit">
                    <PropertiesTextEdit DisplayFormatString="{0}%" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g> %" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(F) <br/> Amount Profit" VisibleIndex="6" FieldName="AmtProfit">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(G) <br/> PackingCharge" VisibleIndex="6" FieldName="PackingCharge">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Transportation Charge" VisibleIndex="6" FieldName="Transportation" Width="170">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(H) <br/> Transportation Charge (IDR)" VisibleIndex="6"
                    FieldName="Transportation_Charge_IDR" Width="170">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="false" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <%--<dx:GridViewDataTextColumn Caption="Currency" VisibleIndex="6" FieldName="Currency">
                <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                </PropertiesTextEdit>
                 <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>--%>
                <dx:GridViewDataComboBoxColumn Caption="Currency" FieldName="Currency" VisibleIndex="6"
                    Width="120px">
                    <PropertiesComboBox TextField="Par_Description" ValueField="Par_Code" DataSourceID="sdsCurrency"
                        Width="145px" TextFormatString="{1}" ClientInstanceName="Currency" DropDownStyle="DropDownList"
                        IncrementalFilteringMode="StartsWith">
                        <Columns>
                            <dx:ListBoxColumn FieldName="Par_Code" Caption="Code" Width="65px" />
                            <dx:ListBoxColumn FieldName="Par_Description" Caption="Name" Width="200px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="2px">
                            <Paddings Padding="2px"></Paddings>
                        </ButtonStyle>
                    </PropertiesComboBox>
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="true" />
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataTextColumn Caption="(I) <br/> Tooling Cost" VisibleIndex="6" FieldName="ToolingCost">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Additional Cost" VisibleIndex="6" FieldName="AdditionalCost" Width="150">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(J) <br/> Additional Cost (IDR)" VisibleIndex="6" FieldName="Additional_Cost_IDR"  Width="150">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="false" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(D + E + F + G + H + I + J)Total" VisibleIndex="6" FieldName="Total"  Width="170">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Selling Price" VisibleIndex="6" FieldName="SellingPriceRound">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right" ClientInstanceName="spr">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataCheckColumn Caption=" " VisibleIndex="6" 
                    Visible="false">
                    <EditFormSettings Visible="True" Caption="" />
                    <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueType="System.Char">
                    </PropertiesCheckEdit>
                    <EditItemTemplate>
                    <dx1:ASPxCheckBox ID="cbx" ClientInstanceName="cbx" runat="server">
                        <ClientSideEvents CheckedChanged="function(s, e) { 
                            GetCheckBoxValue(s, e); 
                        }" />
                    </dx1:ASPxCheckBox>
                    <dx1:ASPxLabel ID ="lbl" runat="server" Text="Auto Calculation Selling Price" Font-Names="Segoe UI" Font-Size="9"></dx1:ASPxLabel>
                    </EditItemTemplate>
                </dx:GridViewDataCheckColumn>

                <dx:GridViewDataTextColumn Caption="Remarks" VisibleIndex="6" FieldName="Remarks">
                    <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="6" FieldName="Register_By">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="6" FieldName="Register_Date">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="6" FieldName="Update_By">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="6" FieldName="Update_Date">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
            <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="380" />
            </SettingsPopup>
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header>
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
                <CommandColumnItem ForeColor="Orange">
                </CommandColumnItem>
            </Styles>
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
            <Templates>
                <EditForm>
                    <div style="padding: 15px 15px 15px 15px">
                        <dx:ContentControl ID="ContentControl1" runat="server">
                            <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                runat="server"></dx:ASPxGridViewTemplateReplacement>
                        </dx:ContentControl>
                    </div>
                    <div style="text-align: left; padding: 5px 5px 5px 15px">
                        <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                        <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                    </div>
                </EditForm>
            </Templates>
        </dx:ASPxGridView>
        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
         </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbkDrawingComplete" runat="server" ClientInstanceName="cbkDrawingComplete">
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbComplete" runat="server" ClientInstanceName="cbComplete">
            <ClientSideEvents EndCallback="function(s,e) {
        if (s.cp_statusComplete == 'true')
          {
            btnComplete.SetEnabled(true);
            }
            else
            {
     
             btnComplete.SetEnabled(false);
            }
           
      }
       " />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
            <ClientSideEvents Init="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbStatus" runat="server" ClientInstanceName="cbStatus">
            <ClientSideEvents EndCallback="function(s,e) {
                lblStatus.SetValue(s.cp_StatusData);  
             lblRemarksStatus.SetValue(s.cp_RemarksData)
      }
       " />
        </dx:ASPxCallback>
    </div>
    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
    </dx:ASPxGridViewExporter>
    <asp:SqlDataSource ID="sdsCurrency" runat="server" SelectCommand="SELECT Par_Code, Par_Description FROM Mst_Parameter WHERE Par_Group = 'Currency'"
        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" SelectCommandType="Text">
    </asp:SqlDataSource>
    <dx:ASPxCallback ID="CurrCB" runat="server" ClientInstanceName="CurrCB">
         <ClientSideEvents EndCallback="function(s,e) {
            lblUSDRate.SetValue(formatter.format(s.cp_CBRateUSD).replace('$', ''));  
            lblJPYRate.SetValue(formatter.format(s.cp_CBRateJPY).replace('$', ''));
            lblTHBRate.SetValue(formatter.format(s.cp_CBRateTHB).replace('$', ''));
          }
          " />
    </dx:ASPxCallback>
</asp:Content>

﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports Microsoft.Office.Interop.Excel
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class CompositionList
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""

    Dim pHeader As Boolean
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
    Dim fRefresh As Boolean = False
#End Region

#Region "Procedure"

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|A140", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAdd.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAdd, btnAdd.GetType(), "btnAdd", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub up_GridLoad()
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        ds = ClsComposition_MaterialDB.CompositionMaterial_List(ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_ExcelGridAuto(Optional ByRef pErr As String = "")

        Try

            up_GridLoad()

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=CompositionList" & Format(Date.Now, "ddMMyyyyHHmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            pErr = ex.Message
        End Try

    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A140")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A140")

        cbExcel.JSProperties("cpmessage") = ""
        Dim Script As String
        If Not Page.IsPostBack Then
            Script = "btnDownload.SetEnabled(false);"
            ScriptManager.RegisterStartupScript(BtnDownload, BtnDownload.GetType(), "btnDownload", Script, True)
            If gs_Back = True Then
                up_GridLoad()
            End If
        End If

    End Sub
#End Region

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("~/Composition_Detail.aspx")
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad()
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        pHeader = True
        If pFunction = "gridload" Then
            up_GridLoad()
            fRefresh = True
            If Grid.VisibleRowCount > 0 Then
                'If ds.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cp_disabled") = "N"

            Else
                Grid.JSProperties("cp_disabled") = "Y"
                show_error(MsgTypeEnum.Info, "There is no data to show!", 1)

            End If

        ElseIf pFunction.Trim = "delete" Then
            Dim pErr As String = ""
            Dim Parent_MaterialCode As String = Split(e.Parameters, "|")(1)

            ClsComposition_MaterialDB.DeleteData(Parent_MaterialCode, pErr)

            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
                up_GridLoad()
            End If
        End If
        'Dim pFunction As String = Split(e.Parameters, "|")(0)

        'pHeader = True
        'If pFunction = "gridload" Then

        '    up_GridLoad()

        'Else
        '    up_GridLoad()
        'End If

    End Sub

    Protected Sub BtnDownload_Click(sender As Object, e As EventArgs) Handles BtnDownload.Click
        'cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        Dim ErrMsg As String = ""
        'up_Excel(ErrMsg)
        up_ExcelGridAuto(ErrMsg)

        If ErrMsg <> "" Then
            cbExcel.JSProperties("cpmessage") = ErrMsg
        Else
            cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        End If

    End Sub


End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="IAPriceApprovalDetail.aspx.vb" Inherits="IAMI_EPIC2.IAPriceApprovalDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<script type="text/javascript">


    function MessageBox(s, e) {
        if (s.cpMessage == "Data Has Been Approved Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            btnApprove.SetEnabled(false);
            btnReject.SetEnabled(false);
        }
        else if (s.cpMessage == "Data Has Been Reject Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            btnApprove.SetEnabled(false);
            btnReject.SetEnabled(false);

        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }


    }


    function OnBatchEditStartEditing(s, e) {

        currentColumnName = e.focusedColumn.fieldName;
        if (currentColumnName != "Selected_Supplier" && currentColumnName != "Reference_Supplier") {
            e.cancel = true;

        }
        currentEditableVisibleIndex = e.visibleIndex;
    }

    function GetMessage(s, e) {

        //alert(s.cpMessage);

        if (s.cpMessage == 'Draft data saved successfull') {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;



            if (txtFlag.GetText() == '0') {
                //alert(s.cpStatus);
                txtRev.SetText(s.cpRevision);
                txtIAPriceNo.SetText(s.cpIANo);
                txtParameter.SetText(s.cpIANo + '|' + s.cpRevision + '|' + cboCENumber.GetText());
                // txtParameter.SetText(s.cpSRNo + '' + s.cpRevision + cboCPNumber.GetText());
                cboCENumber.SetEnabled(false);
                dtIADate.SetEnabled(false);

            }

        }
        else if (s.cpMessage == "Data saved successfull") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

          //  btnSubmit.SetEnabled(false);

            millisecondsToWait = 1000;
            setTimeout(function () {
                window.location.href = "/IAPriceApproval.aspx";
            }, millisecondsToWait);
        }

        else if (s.cpMessage == null || s.cpMessage == "") {
            if (s.cpView == "") {
                Grid.PerformCallback('view|xx|0|xx');
               
            }
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;


        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }



        var status = s.cpStatus;

        if (status == "0") {
            //  cbSuppNo.PerformCallback(s.cpCP);
          //  btnSubmit.SetEnabled(true);
            btnPrint.SetEnabled(true);
            
        }
        else if (status == "1") {
           // btnDraft.SetEnabled(false);
           // btnSubmit.SetEnabled(false);
            
        }
        else if (status == "") {
          //  btnSubmit.SetEnabled(false);
            btnPrint.SetEnabled(false);
           

        }

    }



    function SubmitProcess(s, e) {
        var msg = confirm('Are you sure want to submit this data ?');
        if (msg == false) {
            e.processOnServer = false;
            return;
        }

        //        cbSubmit.PerformCallback(txtCENumber.GetText());
        cbSubmit.PerformCallback();
    }


    function DraftProcess() {

        if (txtFlag.GetText() == '0') {
            if (cboCENumber.GetText() == '') {
                cboCENumber.Focus();
                toastr.warning("Please select CE Number first!", "Warning");
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return false;
            }

            if (cboCENumber.GetSelectedIndex() == -1) {
                cboCENumber.Focus();
                toastr.warning('Invalid CE Number!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return false;
            }

            if (txtDescs.GetText() == '') {
                txtDescs.Focus();
                toastr.warning("Please input Description first!", "Warning");
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return false;
            }

            if (memoNote.GetText() == '') {
                memoNote.Focus();
                toastr.warning("Please input Note first!", "Warning");
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return false;
            }

            alert(Grid.batchEditApi.VisibleRowCount);
            startIndex = 0;
            var supp1 = "";
            var y = 0;

            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                supp1 = Grid.batchEditApi.GetCellValue(i, "Selected_Supplier");

                if (supp1 == "") {
                    y = y + 1;
                }
            }


        }

        var msg = confirm('Are you sure want to draft this data ?');
        if (msg == false) {
            e.processOnServer = false;
            return;
        }
        else {

            Grid.UpdateEdit();
           
            millisecondsToWait = 1000;
            setTimeout(function () {
                cbDraf.PerformCallback();
            }, millisecondsToWait);

            setTimeout(function () {
             Grid.PerformCallback('draft|' + txtParameter.GetText());
              }, millisecondsToWait);

        }
    }



    

    function OnStartEditing(s, e) {
    }

    function GridLoad() {
        //cboSupplier.PerformCallback(cboRFQSetNumber.GetText());
        Grid.PerformCallback('gridload|xx|0|' + cboCENumber.GetText());
           //Grid.PerformCallback();
    }

    function GridLoadCompleted(s, e) {
        LabelSup1.SetText(s.cpHeaderCaption1);
        LabelSup2.SetText(s.cpHeaderCaption2);
        LabelSup3.SetText(s.cpHeaderCaption3);
        LabelSup4.SetText(s.cpHeaderCaption4);
        LabelSup5.SetText(s.cpHeaderCaption5);

    }
   

    </script>
    <style type="text/css">
        .colwidthbutton
        {
            width: 90px;
        }
        
         .hidden-div
        {
            display:none;
        }        
        
        .rowheight
        {
            height: 35px;
        }
        
        .col1
        {
            width: 10px;
        }
        .colLabel1
        {
            width: 150px;
        }
        .colLabel2
        {
            width: 113px;
        }
        .colInput1
        {
            width: 220px;
        }
        .colInput2
        {
            width: 133px;
        }
        .colSpace
        {
            width: 50px;
        }
        
        .customHeader
        {
            height: 15px;
        }
       
        .style1
        {
            width: 137px;
        }
       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">

        <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
                <tr class="rowheight">
                    <td class="col1">
                    </td>
                    <td class="colLabel1">
                     <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" 
                         Font-Size="9pt" Text="IA Price Number">
                    </dx1:aspxlabel>                   
                    </td>
                    <td style="width:200px" >

                            
                    <dx1:ASPxTextBox ID="txtIAPriceNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="170px" ClientInstanceName="txtIAPriceNo" MaxLength="60" 
                        Height="30px" ReadOnly="True" Font-Bold="True" BackColor="Silver" >
            </dx1:ASPxTextBox>
                    
                    </td>
                    <td style="width: 35px">

                            
                    <dx1:ASPxTextBox ID="txtRev" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="50px" ClientInstanceName="txtRev" MaxLength="20" 
                                Height="30px" ReadOnly="True" Font-Bold="True" 
                                ForeColor="Black" HorizontalAlign="Center" BackColor="Silver" >
     
                    </dx1:ASPxTextBox>
                        
                    </td>
                    <td class="colSpace">
                    </td>
                    <td class="colLabel2">
                
                     <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" 
                         Font-Size="9pt" Text="IA Price Date">
                    </dx1:aspxlabel>                   
                
                    </td>
                    <td class="colInput2">
           
                <dx:ASPxDateEdit ID="dtIADate" runat="server" Theme="Office2010Black" 
                                        Width="100px" AutoPostBack="false" ClientInstanceName="dtIADate"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    
                    </td>
                    <td>
                    </td>
                    <td class="hidden-div">

                    <dx1:ASPxTextBox ID="txtParameter" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="200px" ClientInstanceName="txtParameter" MaxLength="20" 
                                Height="30px" ReadOnly="True" Font-Bold="True" 
                                ForeColor="Black" HorizontalAlign="Center" >
     
                    </dx1:ASPxTextBox>           
                    </td>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                
                     <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" 
                         Font-Size="9pt" Text="CE Number">
                    </dx1:aspxlabel>                   
                
                    </td>
                    <td colspan="2">
           
        <dx1:ASPxComboBox ID="cboCENumber" runat="server" ClientInstanceName="cboCENumber"
                            Width="220px" Font-Names="Segoe UI"  TextField="Code"
                            ValueField="Code" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <ClientSideEvents SelectedIndexChanged="GridLoad" />
                            <Columns>            
                            <dx:ListBoxColumn Caption="CE Number" FieldName="Code" Width="100px" />
                        
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" ><Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
           
                    </td>
                    <td>
                    </td>
                    <td class="style1">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                    </td>
                    <td class="hidden-div">
                    <div><dx1:ASPxTextBox ID="txtFlag" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Width="50px" ClientInstanceName="txtFlag" MaxLength="20" 
                                Height="30px" ReadOnly="True" Font-Bold="True" 
                                ForeColor="Black" HorizontalAlign="Center" >
     
                    </dx1:ASPxTextBox> </div>
                              
                    
                    </td>
                </tr>
                <%--<tr>
                <td></td>
                <td colspan="8">
                    
                </td>
            </tr>--%>
                <tr>
                    <td style="height: 10px">
                    </td>
                    <td>
                
                     <dx1:aspxlabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" 
                         Font-Size="9pt" Text="Description">
                    </dx1:aspxlabel>                   
                
                    </td>
                    <td>

            <dx1:ASPxTextBox ID="txtDescs" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                        Width="250px" ClientInstanceName="txtDescs" MaxLength="30" 
                        Height="25px" Enabled="false" >
            </dx1:ASPxTextBox>
           
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        &nbsp;
                        &nbsp;&nbsp;<dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                         <ClientSideEvents Init="GetMessage" />
                                                        
                </dx:ASPxCallback>

                     <dx:ASPxCallback ID="cbSubmit" runat="server" ClientInstanceName="cbSubmit">
                         <ClientSideEvents Init="GetMessage" />
                                                        
                </dx:ASPxCallback>

                     <dx:ASPxCallback ID="cbDraf" runat="server" ClientInstanceName="cbDraf">
                         <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
                                                        
                </dx:ASPxCallback>

                    <asp:SqlDataSource ID="dsSupplier" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select Supplier_Code, Supplier_Name From Mst_Supplier"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>

                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>

    </div>
    <div style="padding: 5px 5px 5px 5px">

            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%"  
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Material_No" >
                <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing" 
                    EndCallback="GridLoadCompleted" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Material No" VisibleIndex="0" Width="120px" 
                        FieldName="Material_No" >

                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="1" Width="300px" 
                        FieldName="Description">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="UOM" VisibleIndex="3" Width="70px" 
                        FieldName="UOM">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Qty" VisibleIndex="2" Width="90px" 
                        FieldName="Qty">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Currency" VisibleIndex="4" Width="70px" 
                        FieldName="Curr_Code">
                    </dx:GridViewDataTextColumn>
                      <dx:GridViewBandColumn Caption="Supplier 1" Name="Supplier1" VisibleIndex="5">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup1" ClientInstanceName="LabelSup1" runat="server" Text="Supplier 1"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                            <dx:GridViewDataTextColumn Caption="Quotation" FieldName="QuoPrice1" 
                                                    VisibleIndex="1" Width="210px">
                                                    <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Final" FieldName="FinalPrice1" VisibleIndex="2"
                                                    Width="210px">
                                                   <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 2" 
                        Name="Supplier2" VisibleIndex="6">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup2" ClientInstanceName="LabelSup2" runat="server" Text="Supplier 2"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Quotation" FieldName="QuoPrice2" 
                                                    VisibleIndex="1" Width="210px">
                                                    <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Final" FieldName="FinalPrice2" VisibleIndex="2"
                                                    Width="210px">
                                                    <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 3" 
                        Name="Supplier3" VisibleIndex="7">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup3" ClientInstanceName="LabelSup3" runat="server" Text="Supplier 3"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Quotation" FieldName="QuoPrice3" 
                                                    VisibleIndex="1" Width="210px">
                                                    <PropertiesTextEdit DisplayFormatString="###,###">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Final" FieldName="FinalPrice3" VisibleIndex="2"
                                                    Width="210px">
                                                   <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 4" 
                        Name="Supplier4" VisibleIndex="8">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup4" ClientInstanceName="LabelSup4" runat="server" Text="Supplier 4"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Quotation" FieldName="QuoPrice4" 
                                                    VisibleIndex="1" Width="210px">
                                                    <PropertiesTextEdit DisplayFormatString="###,###">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Final" FieldName="FinalPrice4" VisibleIndex="2"
                                                    Width="210px">
                                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 5" 
                        Name="Supplier5" VisibleIndex="9">
                                            <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="LabelSup5" ClientInstanceName="LabelSup5" runat="server" Text="Supplier 5"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>
                                            <Columns>
                                           
                                                <dx:GridViewDataTextColumn Caption="Quotation" FieldName="QuoPrice5" 
                                                    VisibleIndex="1" Width="210px">
                                                    <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Final" FieldName="FinalPrice5" VisibleIndex="2"
                                                    Width="210px">
                                                    <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                
                                            </Columns>
                                        </dx:GridViewBandColumn>
                   
                    <dx:GridViewDataComboBoxColumn Caption="Selected Supplier" Width="150px"
                        FieldName="Selected_Supplier" VisibleIndex="11">
                        <PropertiesComboBox DataSourceID="dsSupplier" TextField="Supplier_Name" 
                            ValueField="Supplier_Code">
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                </Columns>

                 <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" 
                EnableRowHotTrack="True" AllowSelectByRowClick="True" />
                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                    </SettingsEditing>
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="200" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>



             </dx:ASPxGridView>
    </div>

    <div style="padding: 5px 5px 5px 5px">
       <table style="width: 100%; border: 1px solid black; height: 100px;">
                  <tr>
                    <td colspan="7">
                    <dx:ASPxCallback ID="cbApprove" runat="server" 
                        ClientInstanceName="cbApprove">                                      
                    <ClientSideEvents CallbackComplete="MessageBox"></ClientSideEvents>
                </dx:ASPxCallback>     
                    <dx:ASPxCallback ID="cbReject" runat="server" 
                        ClientInstanceName="cbReject">
                    <ClientSideEvents CallbackComplete="MessageBox" />                                           
                </dx:ASPxCallback>
                      </td>
                </tr>
                <tr>
                    <td class="col1">
                    </td>
                    <td colspan="8">
                        <dx1:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Note :">
                        </dx1:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>

                    <td colspan="7">
                        <dx:ASPxMemo ID="memoNote" runat="server" Height="45px" Width="100%" ClientInstanceName="memoNote"
                            MaxLength="300" Enabled="false">
                        </dx:ASPxMemo>
                    </td>
                    <td class="col1">
                    </td>
                </tr>



                <tr>
                    <td>
                    </td>
                    <td colspan="7" align="right">
                        <dx1:ASPxLabel ID="lblLenght" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            ClientInstanceName="lblLenght" Text="0/300" Visible ="false" >
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                    </td>
                </tr>



                <tr>
                    <td>
                        &nbsp;</td>
                    <td colspan="7">
                    <dx1:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Approval Notes :">
                    </dx1:ASPxLabel>                 
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>



                <tr>
                    <td>
                        &nbsp;</td>
                    <td colspan="7" align="right">
                        <dx:ASPxMemo ID="txtApprovalNote" runat="server" Height="45px" Width="100%" ClientInstanceName="txtApprovalNote"
                            MaxLength="300">
                        </dx:ASPxMemo>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr style="height: 20px">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="7">
                        <table>
                            <tr class="rowheight">
                                <td class="colwidthbutton">
                                    <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnBack" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>
                                </td>
                                <td class="colwidthbutton">

                    <dx1:ASPxButton ID="btnApprove" runat="server" Text="Approve" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnApprove" Theme="Default" >                        
                     
                        <ClientSideEvents Click="function(s, e) {

                                //alert(chkUrgent.GetValue());

                             


	                            var msg = confirm('Are you sure want to approve this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }

                                

                                cbApprove.PerformCallback();

                                }" />
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>


                                </td>
                                <td class="colwidthbutton">

                    <dx1:ASPxButton ID="btnReject" runat="server" Text="Reject" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnReject" Theme="Default" >                        
                     
                        <ClientSideEvents Click="function(s, e) {

                                if (txtApprovalNote.GetText() == ''){    
                                        toastr.warning('Please Input Approval Notes!', 'Warning');
                                        txtApprovalNote.Focus();
                                        toastr.options.closeButton = false;
                                        toastr.options.debug = false;
                                        toastr.options.newestOnTop = false;
                                        toastr.options.progressBar = false;
                                        toastr.options.preventDuplicates = true;
                                        toastr.options.onclick = null;
		                                e.processOnServer = false;
                                        return;
                                 }

	                            var msg = confirm('Are you sure want to reject this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }
								
								cbReject.PerformCallback();

                            }" />
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>


                                </td>

                                <td class="colwidthbutton">
                                    <dx1:ASPxButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                        ClientInstanceName="btnPrint" Theme="Default" >                        
                     
                        <Paddings Padding="2px" />
                    </dx1:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
            </table>
    </div>


</div>
</asp:Content>

﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class ApprovalSet
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim UserLogin As String = ""
    Dim UserSign As String = ""
#End Region

#Region "Initialization"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UserID As String = Trim(Session("user") & "")
        Dim GlobalPrm As String = ""

        UserLogin = UserID

        If Request.QueryString("prm") Is Nothing Then
            Exit Sub
        End If
        Dim ID As String = Request.QueryString("prm").ToString()
        UserSign = ID
        ' cbDraf.JSProperties("cpUser") = ID
        up_GridLoad(ID)

    End Sub

#End Region

#Region "Procedure"

    Private Sub up_GridLoad(ByVal UserID As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataTable
        Ses = Cls_UserSetup_DB.getlist(UserID)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

#End Region

#Region "Control Event"
    Private Sub Grid_CustomCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pAction As String = Split(e.Parameters, "|")(0)
        Try
            Select Case pAction
                
                Case "loadPrivilege"
                    Call up_GridLoad(UserSign)
            End Select

        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub

    Private Sub Grid_BatchUpdate(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Dim ls_SQL As String = "", ls_Approval_Group As String = "", ls_Approval_ID As String = "", ls_Approval_Level As String = "", ls_Approval_Name As String = "", ls_MsgID As String = ""
        Dim iLoop As Long = 0, jLoop As Long = 0
        Dim ls_AllowAccess As String = ""
        Dim ls_UserID As String = ""
        ls_UserID = UserSign
        If e.UpdateValues.Count = 0 Then
            show_error(MsgTypeEnum.Warning, "Data is not found!")
            Exit Sub
        End If
        Dim a As Integer
        a = e.UpdateValues.Count
        Dim pErr As String = ""
        For iLoop = 0 To a - 1
            ls_AllowAccess = (e.UpdateValues(iLoop).NewValues("isApproval").ToString())
          
            If ls_AllowAccess = True Then ls_AllowAccess = "1" Else ls_AllowAccess = "0"

            ls_Approval_Group = Trim(e.UpdateValues(iLoop).NewValues("Approval_Group").ToString())
            ls_Approval_ID = Trim(e.UpdateValues(iLoop).NewValues("Approval_ID").ToString())
            ls_Approval_Level = Trim(e.UpdateValues(iLoop).NewValues("Approval_Level").ToString())
            ls_Approval_Name = Trim(e.UpdateValues(iLoop).NewValues("Approval_Name").ToString())


            Dim UserPrevilege As New clsApprovalSetupUser With
                {.UserID = ls_UserID,
                 .Approval_Group = ls_Approval_Group,
                 .Approval_ID = ls_Approval_ID,
                 .Approval_Level = ls_Approval_Level,
                 .Approval_Name = ls_Approval_Name,
                 .isApproval = ls_AllowAccess}
            Cls_UserSetup_DB.SaveApproval(UserPrevilege, pErr)
            If pErr <> "" Then
                Exit For
            End If
        Next iLoop
        Grid.EndUpdate()
    End Sub

  

#End Region

End Class
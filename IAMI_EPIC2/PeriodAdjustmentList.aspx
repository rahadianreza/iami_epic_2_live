﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="PeriodAdjustmentList.aspx.vb" Inherits="IAMI_EPIC2.PeriodAdjustmentList" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function MessageError(s, e) {
            if (s.cpmessage == "Download Excel Successfully") {
                toastr.success(s.cpmessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpmessage == null || s.cpmessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

      </script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black" >
            <tr style="height:50px">
                <td style=" width:100px; padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnRefresh" Theme="Default">
                        <ClientSideEvents Click="function(s, e) {                      
	                        Grid.PerformCallback('gridload|');
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style=" width:5px">&nbsp;</td>
                <td style=" width:100px; padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="BtnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="BtnDownload" Theme="Default">
                        <ClientSideEvents Click="function(s, e) {
	                        cbExcel.PerformCallback();
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style=" width:5px">&nbsp;</td>
                <td style=" width:100px; padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="false" Font-Names="Segoe UI"
                        Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnAdd" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style=" width:5px">&nbsp;</td>
                <td style="width:250px">
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                </td>
                <td style="width:250px">
                    <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                        <ClientSideEvents Init="MessageError" />
                    </dx:ASPxCallback>
                </td>
                <td style="width:250px">
                    <dx:ASPxCallback ID="cbExcel" runat="server" ClientInstanceName="cbExcel">
                        <ClientSideEvents Init="MessageError" />
                    </dx:ASPxCallback>
                </td>
            </tr>
        </table>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="PeriodID">
            <ClientSideEvents CustomButtonClick="function(s, e) {
		        if(e.buttonID == 'edit'){                     
                    var rowKey = Grid.GetRowKey(e.visibleIndex);         
	                window.location.href= 'AddPeriodAdjustment.aspx?ID=' + rowKey;
                }
            }" />
            <Columns>
                <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" FixedStyle="Left" Width="50px">
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="" VisibleIndex="1"
                    FieldName="PeriodID" Width="80px">
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True"></HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Period Description" VisibleIndex="2"
                    FieldName="PeriodDescription" Width="200px">
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Period Type" VisibleIndex="3"
                    FieldName="PeriodType" Width="120px">
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Period From" VisibleIndex="4" 
                    FieldName="PeriodFrom" Width="100px">
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Period To" VisibleIndex="5" 
                    FieldName="PeriodTo" Width="100px">
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="PO Date From" VisibleIndex="6" 
                    FieldName="PODateFrom" Width="100px">
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="PO Date To" VisibleIndex="7" 
                    FieldName="PODateTo" Width="100px">
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy"></PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="8" 
                    FieldName="RegisterBy" Width="150px">
                    <PropertiesTextEdit Width="150px" ClientInstanceName="CreateUser"></PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Register Date" VisibleIndex="9"
                    FieldName="RegisterDate" Width="100px">
                    <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd MMM yyyy"></PropertiesDateEdit>
                    <EditFormSettings Visible="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="10"
                    FieldName="UpdateBy" Width="150px">
                    <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateUser"></PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Update Date" 
                    FieldName="UpdateDate" VisibleIndex="11" Width="100px">
                    <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd MMM yyyy">
                    </PropertiesDateEdit>
                    <EditFormSettings Visible="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataDateColumn> 
            </Columns>

            <SettingsBehavior ColumnResizeMode="Control" />
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                HorizontalScrollBarMode="Auto" />
            <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header>
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption>
                    <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
                </EditFormColumnCaption>
            </Styles>
        </dx:ASPxGridView>
    </div>
</asp:Content>

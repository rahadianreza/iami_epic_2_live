﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CPAcceptanceDetail.aspx.vb" Inherits="IAMI_EPIC2.CPAcceptanceDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxLoadingPanel" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    window.onload = function () {
        var GetDocumentScrollTop = function () {
            var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollTop > 0;
            if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                if (ASPx.Browser.MacOSMobilePlatform)
                    return window.pageYOffset;
                else if (ASPx.Browser.WebKitFamily)
                    return document.documentElement.scrollTop || document.body.scrollTop;
                return document.body.scrollTop;
            }
            else
                return document.documentElement.scrollTop;
        };
        var _aspxGetDocumentScrollTop = function () {
            if (__aspxWebKitFamily) {
                if (__aspxMacOSMobilePlatform)
                    return window.pageYOffset;
                else
                    return document.documentElement.scrollTop || document.body.scrollTop;
            }
            else
                return document.documentElement.scrollTop;
        }
        if (window._aspxGetDocumentScrollTop) {
            window._aspxGetDocumentScrollTop = _aspxGetDocumentScrollTop;
            window.ASPxClientUtils.GetDocumentScrollTop = _aspxGetDocumentScrollTop;
        } else {
            window.ASPx.GetDocumentScrollTop = GetDocumentScrollTop;
            window.ASPxClientUtils.GetDocumentScrollTop = GetDocumentScrollTop;
        }
        /* Begin -> Correct ScrollLeft  */
        var GetDocumentScrollLeft = function () {
            var isScrollBodyIE = ASPx.Browser.IE && ASPx.GetCurrentStyle(document.body).overflow == "hidden" && document.body.scrollLeft > 0;
            if (ASPx.Browser.WebKitFamily || isScrollBodyIE) {
                if (ASPx.Browser.MacOSMobilePlatform)
                    return window.pageXOffset;
                else if (ASPx.Browser.WebKitFamily)
                    return document.documentElement.scrollLeft || document.body.scrollLeft;
                return document.body.scrollLeft;
            }
            else
                return document.documentElement.scrollLeft;
        };
        var _aspxGetDocumentScrollLeft = function () {
            if (__aspxWebKitFamily) {
                if (__aspxMacOSMobilePlatform)
                    return window.pageXOffset;
                else
                    return document.documentElement.scrollLeft || document.body.scrollLeft;
            }
            else
                return document.documentElement.scrollLeft;
        }
        if (window._aspxGetDocumentScrollLeft) {
            window._aspxGetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
            window.ASPxClientUtils.GetDocumentScrollLeft = _aspxGetDocumentScrollLeft;
        } else {
            window.ASPx.GetDocumentScrollLeft = GetDocumentScrollLeft;
            window.ASPxClientUtils.GetDocumentScrollLeft = GetDocumentScrollLeft;
        }
        /* End -> Correct ScrollLeft  */
    };

    var currentColumnName;
    var dataExist;
    var dataSubmitted;
    var dataAccepted;


    function CountingLength(s, e) {
        //Counting lenght
        var memo = memoNote.GetText();
        var lenMemo = memo.length;
        lenMemo = lenMemo;
        if (lenMemo >= 300) {
            lenMemo = 300;
        }

        //Set Text
        lblLenght.SetText(lenMemo + "/300");
    }

    function SetInformation(s, e) {
        txtCPNumber.SetText(s.cpCPNumber);
        txtRev.SetText(s.cpRev);
        txtPRNumber.SetText(s.cpPRNumber);
        txtCENumber.SetText(s.cpCENumber);
        memoNote.SetText(s.cpNotes);

        var dt = s.cpInvitationDate;
        var YearFrom = dt.getFullYear();
        var MonthFrom = dt.getMonth();
        var DateFrom = dt.getDate();
        var newdate = new Date(YearFrom, MonthFrom, DateFrom)
        //txtInvitationDate.SetText(newdate);
        txtInvitationDate.SetText(YearFrom + '-' + MonthFrom + '-' + DateFrom);
    }


    function OnFileUploadStart(s, e) {
        btnAddFile.SetEnabled(false);
    }

    function OnTextChanged(s, e) {
        btnAddFile.SetEnabled(s.GetText() != "");
    }

    function OnFileUploadComplete(s, e) {
        btnAddFile.SetEnabled(s.GetText() != "");

        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }

        window.setTimeout(function () {
            delete s.cpType;
        }, 10);
    }
  
    function OnGetSelectedFieldValues(selectedValues) {
        //VALIDATE WHEN THERE IS NO SELECTED DATA
        if (selectedValues.length == 0) {
            toastr.warning('There is no selected data to delete!', 'Warning');
            return;
        }
      

        //GET FILE NAME
        var sValues;
        var sValuesSeqNo;
        var resultSeqNo="";
        var result;
        result = "";

        for (i = 0; i < selectedValues.length; i++) {
            sValues = "";
            sValuesSeqNo = "";
            for (j = 0; j < selectedValues[i].length; j++) {
                sValues = sValues + selectedValues[i][j];
                sValuesSeqNo = sValuesSeqNo + selectedValues[i][j];
                
            }
            //alert(sValuesSeqNo.split("|", 1));
            // alert(sValues.split("|", 2));
            result = result + sValues.split("|",2).slice(-1)[0] + "|";
            resultSeqNo = resultSeqNo + sValuesSeqNo.split("|", 1)[0] + "|"

        }
       
        //CONFIRMATION
        var C = confirm("Are you sure want to delete selected attachment (" + result + ") ?");
        if (C == false) {
            return;
        }

        //KEEP FILE NAME INTO HIDDENFIELD
        hf.Set("DeleteAttachment", result);
        hf.Set("SeqNo",resultSeqNo);

        //EXECUTE TO DATABASE AFTER DELAY 300ms
        window.setTimeout(function () {
            cb.PerformCallback("DeleteAttachment");
        }, 300);
    }


    function OnBatchEditStartEditing(s, e) {
        e.cancel = true;
        Grid.batchEditApi.EndEdit();
    }

    function OnBatchEditEndEditing(s, e) {

    }

    function ShowConfirmOnLosingChanges() {
        var C = confirm("The data you have entered may not be saved. Are you sure want to leave?");
        if (C == true) {
            return true;
        } else {
            return false;
        }
    }

    function GridLoad(s, e) {
        window.setTimeout(function () {
            Grid.PerformCallback();
        }, 100);
        window.setTimeout(function () {
            GridAtc.PerformCallback();
        }, 100);
    }



    function cbExistEndCallback(s, e) {
        if (s.cpParameter == "exist") {
            dataExist = s.cpResult;

        } else if (s.cpParameter == "IsAccepted") {
            dataAccepted = s.cpResult;

        } else if (s.cpParameter == "accept") {
            if (s.cpAccept == "Y") {
                btnDeleteAtc.SetEnabled(false);
                btnAccept.SetEnabled(false);
                //ucAtc.SetEnabled(false);
            }

//            if (s.cpResult = "Atc") {
//               
//            }
            if (s.cpType == "0") {
                //INFO
                toastr.info(s.cpMessage, 'Information');

            } else if (s.cpType == "1") {
                //SUCCESS
                toastr.success(s.cpMessage, 'Success');

            } else if (s.cpType == "2") {
                //WARNING
                toastr.warning(s.cpMessage, 'Warning');
                

            } else if (s.cpType == "3") {
                //ERROR
                toastr.error(s.cpMessage, 'Error');
            }

//            if (s.cpResult == "Atc") {
//                
//            }

        }


        window.setTimeout(function () {
            delete s.cpParameter;
        }, 10);
    }



    function Back(s, e) {
        if (txtCPNumber.GetEnabled() == false) {
            if (Grid.batchEditApi.HasChanges() == true) {
                //there is an update data
                if (ShowConfirmOnLosingChanges() == false) {
                    e.processOnServer = false;
                    return false;
                }
            }

            if (hf.Get("hfNotes") != memoNote.GetText()) {
                if (ShowConfirmOnLosingChanges() == false) {
                    e.processOnServer = false;
                    return false;
                }
            }
        }
        //location.replace("CPList.aspx");
    }

    function Accept(s, e) {
        var C = confirm("Are you sure want to accept the data?");

        if (C == true) {
            cbExist.PerformCallback("accept");

            //alert(s.cpAccept);
            //btnAccept.SetEnabled(false);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        }
    }

    
    function Print(s, e) {

    }


    function AddFile() {
        cbExist.PerformCallback('IsAccepted');
        window.setTimeout(function () {
            if (dataAccepted == "Y") {
                toastr.warning('Cannot add attachment! The data already accepted.', 'Warning');
                return false;

            } else {

                if (btnAccept.GetEnabled() == false) {
                    toastr.warning('Cannot add attachment! The data already accepted.', 'Warning');
                    return false;
                }

                hf.Set("CPNumber", txtCPNumber.GetText());
                hf.Set("Rev", txtRev.GetText());

                cbExist.PerformCallback('exist');

                window.setTimeout(function () {
                    if (dataExist == "Y") {
                        ucAtc.Upload();
                        window.setTimeout(function () { GridAtc.PerformCallback(); }, 100);
                    } else {
                        toastr.warning('CP Number does not exist! Please save the data first before add attachment.', 'Warning');
                    }
                }, 1000);

            }
        }, 500);

    }

    function DeleteAttachment() {
        cbExist.PerformCallback('IsAccepted');
        window.setTimeout(function () {
            if (dataAccepted == "Y") {
                toastr.warning('Cannot delete attachment! The data already accepted.', 'Warning');
                return;

            } else {

                if (btnAccept.GetEnabled() == false) {
                    toastr.warning('Cannot delete attachment! The data already accepted.', 'Warning');
                    return;
                }

                //INITIALIZE PARAMETER
                hf.Set("DeleteAttachment", "");
                hf.Set("SeqNo", "");
                GridAtc.GetSelectedFieldValues("Seq_No;Separator;FileName", OnGetSelectedFieldValues);
               
            }
        }, 100);
    }


    function LoadCompleted(s, e) {
        if (s.cpActionAfter == "DeleteAtc") {
            GridAtc.PerformCallback();
        }

        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }

        window.setTimeout(function () {
            delete s.cpType;
        }, 10);
    }


    function GridLoadCompleted(s, e) {
        //Set header caption Quotation
        LabelSup1.SetText(s.cpHeaderCaption1);
        LabelSup2.SetText(s.cpHeaderCaption2);
        LabelSup3.SetText(s.cpHeaderCaption3);
        LabelSup4.SetText(s.cpHeaderCaption4);
        LabelSup5.SetText(s.cpHeaderCaption5);

        //Set header caption Best Price
        LabelSup1_BestPrice.SetText(s.cpHeaderCaption1);
        LabelSup2_BestPrice.SetText(s.cpHeaderCaption2);
        LabelSup3_BestPrice.SetText(s.cpHeaderCaption3);
        LabelSup4_BestPrice.SetText(s.cpHeaderCaption4);
        LabelSup5_BestPrice.SetText(s.cpHeaderCaption5);


        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');
        }

        window.setTimeout(function () {
            delete s.cpSaveData;
        }, 10);
    }
</script>

<style type="text/css">
    .colwidthbutton
    {
        width: 90px;
    }
    
    .rowheight
    {
        height: 35px;
    }
       
    .col1
    {
        width: 10px;
    }
    .colLabel1
    {
        width: 150px;
    }
    .colLabel2
    {
        width: 113px;
    }
    .colInput1
    {
        width: 220px;
    }
    .colInput2
    {
        width: 133px;
    }
    .colSpace
    {
        width: 50px;
    }
        
    .customHeader {
        height: 15px;
    }
    
    .customHeaderAtc {
        height: 32px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
            <tr class="rowheight">
                <td class="col1">
                    </td>
                <td class="colLabel1">
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Counter Proposal Number"></dx1:aspxlabel>
                </td>
                <td class="colInput1">
                    <dx:ASPxTextBox ID="txtCPNumber" runat="server" width="200px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtCPNumber">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                        <ClientSideEvents TextChanged="GridLoad" />
                    </dx:ASPxTextBox>
                </td> 
                <td style="width:35px">
                    <dx:ASPxTextBox ID="txtRev" runat="server" width="30px" Height="25px" Font-Size="9pt" 
                        ClientInstanceName="txtRev" BackColor="#CCCCCC" ReadOnly="true" 
                        HorizontalAlign="Center" TabIndex="-1">
                    </dx:ASPxTextBox>
                </td>
                <td class="colSpace">
                    </td>
                <td class="colLabel2">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Invitation Date"></dx1:aspxlabel>
                </td>
                <td class="colInput2">
                    <dx:ASPxTextBox ID="txtInvitationDate" runat="server" width="120px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtInvitationDate">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                    </dx:ASPxTextBox>
                </td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <tr class="rowheight">
                <td>
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="PR Number"></dx1:aspxlabel>
                </td>
                <td colspan="2">
                    <dx:ASPxTextBox ID="txtPRNumber" runat="server" width="200px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtPRNumber">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                    </dx:ASPxTextBox>
                </td>
                <td>
                    </td>
                <td> 
                    <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="CE Number"></dx1:aspxlabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="txtCENumber" runat="server" width="200px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtCENumber">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                    </dx:ASPxTextBox>
                </td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <%--<tr>
                <td></td>
                <td colspan="8">
                    
                </td>
            </tr>--%>
            <tr>
                <td style="height: 10px">
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td> 
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                    &nbsp;
                    <dx:ASPxCallback ID="cbExist" runat="server" ClientInstanceName="cbExist">
                        <ClientSideEvents 
                            EndCallback="cbExistEndCallback"
                            
                        />
                        <%--CallbackComplete="LoadCompleted"--%>
                    </dx:ASPxCallback>
                    &nbsp;
                    <dx:ASPxCallback ID="cb" runat="server" ClientInstanceName="cb">
                        <ClientSideEvents 
                            Init="GridLoad"
                            CallbackComplete="LoadCompleted"
                        />
                    </dx:ASPxCallback>
                    &nbsp;
                    <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf">
                    </dx:ASPxHiddenField>
                </td>
            </tr>
        </table>
    </div>
   <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Material_No">            
            <Columns>
                <dx:GridViewDataTextColumn Caption="Material No." FieldName="Material_No"
                     VisibleIndex="0" Width="120px" >
                     <HeaderStyle CssClass="customHeader" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" 
                     VisibleIndex="1" Width="300px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Specification" FieldName="Specification" 
                     VisibleIndex="2" Width="300px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Qty" FieldName="Qty" 
                     VisibleIndex="3" Width="60px">
                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Right"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="UOM" FieldName="UOM" 
                     VisibleIndex="4" Width="50px">
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Remarks" FieldName="Remarks" 
                     VisibleIndex="5" Width="120px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Currency" FieldName="Curr_Code" 
                     VisibleIndex="6" Width="70px">
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewBandColumn Caption="Quotation" Name="Quotation" 
                    VisibleIndex="7">
                    <Columns>
                        <dx:GridViewBandColumn Caption="Supplier 1" Name="Supplier1" VisibleIndex="0">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup1" ClientInstanceName="LabelSup1" runat="server" Text="Supplier 1" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs1" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice1" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 2" Name="Supplier2" VisibleIndex="1">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup2" ClientInstanceName="LabelSup2" runat="server" Text="Supplier 2" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs2" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice2" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 3" Name="Supplier3" VisibleIndex="2">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup3" ClientInstanceName="LabelSup3" runat="server" Text="Supplier 3" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs3" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice3" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 4" Name="Supplier4" VisibleIndex="3">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup4" ClientInstanceName="LabelSup4" runat="server" Text="Supplier 4" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs4" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice4" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 5" Name="Supplier5" VisibleIndex="4">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup5" ClientInstanceName="LabelSup5" runat="server" Text="Supplier 5" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs5" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice5" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="IAMI Proposal" Name="IAMIProposal" 
                    VisibleIndex="8">
                    <Columns>
                        <dx:GridViewDataSpinEditColumn Caption="Price/Pcs" FieldName="ProposalPricePcs" 
                             VisibleIndex="0" Width="100px">
                            <PropertiesSpinEdit DisplayFormatString="###,###" Width="95px" MaxLength="18">
                                <Style HorizontalAlign="Right"></Style>
                            </PropertiesSpinEdit>
                        </dx:GridViewDataSpinEditColumn>
                        <%--<dx:GridViewDataTextColumn Caption="Price/pc" FieldName="ProposalPricePcs" 
                                VisibleIndex="0" Width="100px">
                                <PropertiesTextEdit DisplayFormatString="###,###" Width="75px"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                        </dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataTextColumn Caption="Total Price" FieldName="ProposalTotalPrice" 
                                VisibleIndex="1" Width="100px">
                                <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                <CellStyle HorizontalAlign="Right"></CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn> 
                <dx:GridViewBandColumn Caption="Best Price" Name="BestPrice" 
                    VisibleIndex="9">
                    <Columns>
                        <dx:GridViewBandColumn Caption="Supplier 1" Name="BestPriceSupplier1" VisibleIndex="0">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup1_BestPrice" ClientInstanceName="LabelSup1_BestPrice" runat="server" Text="Supplier 1" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="BestPricePcs1" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice1" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 2" Name="BestPriceSupplier2" VisibleIndex="1">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup2_BestPrice" ClientInstanceName="LabelSup2_BestPrice" runat="server" Text="Supplier 2" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="BestPricePcs2" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice2" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 3" Name="BestPriceSupplier3" VisibleIndex="2">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup3_BestPrice" ClientInstanceName="LabelSup3_BestPrice" runat="server" Text="Supplier 3" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="BestPricePcs3" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice3" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 4" Name="BestPriceSupplier4" VisibleIndex="3">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup4_BestPrice" ClientInstanceName="LabelSup4_BestPrice" runat="server" Text="Supplier 4" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="BestPricePcs4" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice4" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 5" Name="BestPriceSupplier5" VisibleIndex="4">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup5_BestPrice" ClientInstanceName="LabelSup5_BestPrice" runat="server" Text="Supplier 5" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="BestPricePcs5" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice5" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                    </Columns>
                </dx:GridViewBandColumn>               
             </Columns>

            <Settings HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" VerticalScrollableHeight="135" ShowStatusBar="Hidden" />
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsBehavior AllowSelectByRowClick="True" AllowSort="False" AllowDragDrop="false" ColumnResizeMode="Control" />
            <SettingsEditing Mode="Batch">
                <BatchEditSettings StartEditAction="Click" ShowConfirmOnLosingChanges="False" />
            </SettingsEditing>

            <ClientSideEvents 
                EndCallback="GridLoadCompleted"
                BatchEditStartEditing="OnBatchEditStartEditing"
                BatchEditEndEditing="OnBatchEditEndEditing" 
            />

            <Styles Header-Paddings-Padding="1px" EditFormColumnCaption-Paddings-PaddingLeft="0px" EditFormColumnCaption-Paddings-PaddingRight="0px" >
                <Header CssClass="customHeader" HorizontalAlign="Center"></Header>
            </Styles>
        </dx:ASPxGridView>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
             <tr>
                <td></td>
                <td colspan="8">
                    <dx1:aspxlabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Attachment :"></dx1:aspxlabel>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <dx:ASPxGridView ID="GridAtc" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridAtc"
                        EnableTheming="True" Theme="Office2010Black" Width="100%" Settings-VerticalScrollableHeight="80" Settings-VerticalScrollBarMode="Visible"
                        Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="FileName;FilePath" >
            
                        <Columns>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Caption=" " Width="30px"></dx:GridViewCommandColumn>
                            <%--<dx:GridViewDataCheckColumn Caption=" " Name="chk" VisibleIndex="0" Width="30px">
                                <PropertiesCheckEdit ValueChecked="1" ValueUnchecked="0" ValueType="System.Int32" >
                                </PropertiesCheckEdit>
                            </dx:GridViewDataCheckColumn>--%>
                            <dx:GridViewDataTextColumn Caption="Sequence No" FieldName="Seq_No" Visible ="false"
                                 VisibleIndex="1" Width="80px">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="File Type" FieldName="FileType"
                                 VisibleIndex="2" Width="80px">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="File Name" FieldName="FileName" 
                                 VisibleIndex="3" Width="500px">
                                 <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="linkFileName" runat="server" OnInit="keyFieldLink_Init">                                    
                                    </dx:ASPxHyperLink>
                                </DataItemTemplate>      
                            </dx:GridViewDataTextColumn>    
                            <dx:GridViewDataTextColumn Caption="File Size" FieldName="FileSize" Visible="true"
                                 VisibleIndex="4" Width="80px">
                            </dx:GridViewDataTextColumn>       
                            <dx:GridViewDataTextColumn Caption="Separator" FieldName="Separator" Visible ="false"
                                 VisibleIndex="5" Width="80px">
                            </dx:GridViewDataTextColumn>       
                                             
                         </Columns>
                         <SettingsEditing Mode="Batch">
                            <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                        </SettingsEditing>

                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSort="False" EnableRowHotTrack="True" />
                         
                        <Settings VerticalScrollableHeight="60" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" ShowStatusBar="Hidden"></Settings>
                        <Styles>
                            <Header CssClass="customHeaderAtc" HorizontalAlign="Center">
                            </Header>
                        </Styles>
                        <ClientSideEvents 
                            CallbackError="function(s, e) {e.handled = true;}"
                            EndCallback="LoadCompleted" 
                            BatchEditStartEditing="OnBatchEditStartEditing"
                        />
                    </dx:ASPxGridView>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="rowheight">
                    <dx:ASPxButton ID="btnDeleteAtc" runat="server" Text="Delete Selected Attachment" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="150px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDeleteAtc" Theme="Default">                        
                        <ClientSideEvents Click="DeleteAttachment" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <table width="100%">
                        <tr>
                            <td>
                                <dx:ASPxUploadControl ID="ucAtc" runat="server" ClientInstanceName="ucAtc"
                                    Width="50%" Height="30px" ShowProgressPanel="false">
                                    
                                    <ValidationSettings AllowedFileExtensions=".pdf" MaxFileSize="2000000" 
                                        MaxFileSizeErrorText="File size must not exceed 2 MB!"
                                        NotAllowedFileExtensionErrorText="The uploaded file format isn't allowed!"  
                                        >
                                        
                                       
                                    </ValidationSettings>
                                    <BrowseButton Text="Browse..."></BrowseButton>
                                    <ClientSideEvents 
                                        FileUploadStart="OnFileUploadStart"
                                        FilesUploadComplete="OnFileUploadComplete"
                                        TextChanged="OnTextChanged"
                                    />
                                </dx:ASPxUploadControl>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                    Text="*Available file format : pdf" Font-Italic="True">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                    Text="Maximum Size : Total 2 MB" Font-Italic="True">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxButton ID="btnAddFile" runat="server" Text="Add File" 
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnAddFile" Theme="Default">                        
                                    <ClientSideEvents 
                                        Click="AddFile" 
                                    />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="col1"></td>
                <td colspan="8">
                    <dx1:aspxlabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Note :"></dx1:aspxlabel>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <dx:ASPxMemo ID="memoNote" runat="server" Height="45px" Width="100%" ClientInstanceName="memoNote" MaxLength="300" ReadOnly="True" BackColor="#CCCCCC" RootStyle-Paddings-PaddingLeft="3px">
                        <ClientSideEvents KeyDown="CountingLength" LostFocus="CountingLength" />
                    </dx:ASPxMemo>
                </td>
                <td class="col1"></td>
            </tr>
            <tr class="rowheight">
                <td></td>
                <td colspan="7" align="right">
                    <dx1:aspxlabel ID="lblLenght" runat="server" Font-Names="Segoe UI" Font-Size="9pt" ClientInstanceName="lblLenght" Text="0/300" Visible="false"></dx1:aspxlabel>
                </td>
                <td></td>
            </tr>
            <tr class="rowheight">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <table>
                        <tr class="rowheight">
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                                    ClientInstanceName="btnBack" Theme="Default">
                                    <Paddings Padding="2px" />
                                    <ClientSideEvents Click="Back" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnAccept" runat="server" Text="Accept" 
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnAccept" Theme="Default">                        
                                    <ClientSideEvents Click="Accept" />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnPrint" runat="server" Text="Print" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnPrint" Theme="Default">
                                    <ClientSideEvents Click="Print" />                        
                                    <Paddings Padding="2px" />                                    
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
        </table>
    </div>
</div>
</asp:Content>


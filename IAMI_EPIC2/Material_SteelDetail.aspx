﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Material_SteelDetail.aspx.vb" Inherits="IAMI_EPIC2.Material_SteelDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function MessageError(s, e) {
            if (s.cpMessage == "Data Saved Successfully!") {
                cboType.SetEnabled(false)
                cboSupplier.SetEnabled(false)
                Period.SetEnabled(false)
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Updated Successfully!") {
                cboType.SetEnabled(false)
                cboSupplier.SetEnabled(false)
                Period.SetEnabled(false)
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
//                millisecondsToWait = 2000;
//                setTimeout(function () {
//                    var pathArray = window.location.pathname.split('/');
//                    var url = window.location.origin + '/' + pathArray[1] + '/PRJList.aspx';
//                }, millisecondsToWait);
            }
            else if (s.cpMessage == "Data Clear Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Cancel Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function MessageBox(s, e) {
            //alert(s.cpmessage);
            if (s.cbMessage == "Download Excel Successfully") {
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material Type">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 15px;">

                        <dx:ASPxComboBox ID="cboType" runat="server" ClientInstanceName="cboType" Width="120px"
                            Font-Names="Segoe UI" TextField="Description" ValueField="Code" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3" DataSourceID="SqlDataSource1">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboSupplier.PerformCallback(cboType.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                 
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Supplier">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                        <dx:ASPxComboBox ID="cboSupplier" runat="server" ClientInstanceName="cboSupplier" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Supplier_Name" ValueField="Supplier_Code"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Supplier" FieldName="Supplier_Name" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTimeEdit ID="Period" Width="120px" Theme="Office2010Black" Font-Names="Segoe UI"
                    Font-Size="9pt" EditFormat="Custom" ClientInstanceName="Period" runat="server"
                    DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy">
                    <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>   
                </dx:ASPxTimeEdit>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material Code ">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                        <dx:ASPxComboBox ID="cboMaterialCode" runat="server" 
                    ClientInstanceName="cboMaterialCode" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" 
                    EnableIncrementalFiltering="True" Height="25px"  DataSourceID="SqlDataSource2">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboGroupItem.PerformCallback(cboMaterialCode.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                 
            </td>
            <td class="hidden-div" style="display:none">
                        &nbsp;</td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Group Item ">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                        <dx:ASPxComboBox ID="cboGroupItem" runat="server" 
                    ClientInstanceName="cboGroupItem" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" 
                    EnableIncrementalFiltering="True" Height="25px">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboCategory.PerformCallback(cboGroupItem.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Category ">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;</td>
            <td style="width: 50px; padding-top: 10px;">
                        <dx:ASPxComboBox ID="cboCategory" runat="server" 
                    ClientInstanceName="cboCategory" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" 
                    EnableIncrementalFiltering="True" Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Currency ">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;</td>
            <td style="width: 50px; padding-top: 10px;">
                        <dx:ASPxComboBox ID="cboCurr" runat="server" 
                    ClientInstanceName="cboCurr" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" 
                    EnableIncrementalFiltering="True" Height="25px"  DataSourceID="SqlDataSource5">
                            <Columns>
                                <dx:ListBoxColumn Caption="Currency" FieldName="Description" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Price ">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;</td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtPrice" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtPrice" MaxLength="15" 
                    HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>.<00..99999>" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                &nbsp;</td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 400px; padding-top: 10px;"> 
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack"
                    Theme="Default">
                    <Paddings Padding="2px" /> 
                </dx:ASPxButton> 
                &nbsp; &nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
                            cboType.SetEnabled(true)
                            cboSupplier.SetEnabled(true)
                            Period.SetEnabled(true)
                            txtPrice.SetText(0.00000)
                            cboType.SetText('');
                            cboSupplier.ClearItems();
                        }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                  &nbsp;&nbsp;
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td style="height: 30px;">
                &nbsp;
            </td>
        </tr>
        <tr style="height: 25px;">
            <td style="width: 50px;">
                &nbsp;
            </td>
            <td style="width: 50px">
                &nbsp;
            </td>
            <td style="width: 500px">
                &nbsp;&nbsp;
                
              
            </td>
            <td>
                 <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'MaterialType'">
        </asp:SqlDataSource>
                 <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            
                     SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'MaterialCode' and Par_ParentCode='MC01'">
        </asp:SqlDataSource>
                 <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            
                     SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'MaterialGroupItem'">
        </asp:SqlDataSource>
                 <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            
                     SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'MaterialCategory'">
        </asp:SqlDataSource>
                 <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            
                     SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'Currency'">
        </asp:SqlDataSource>
                <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
                    <ClientSideEvents CallbackComplete="SetCode" />
                </dx:ASPxCallback>
                <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                    <ClientSideEvents Init="MessageError" />
                </dx:ASPxCallback>
                   <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
                    <ClientSideEvents Init="MessageError" />                                           
                </dx:ASPxCallback>
                   <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
                    <ClientSideEvents CallbackComplete="MessageDelete" />                                           
                </dx:ASPxCallback>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>

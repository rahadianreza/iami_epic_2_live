﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.UI
Imports System.IO
Imports System.Drawing
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls.Style
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxUploadControl
Imports System
Imports DevExpress.Utils
Imports System.Collections.Generic
Imports OfficeOpenXml
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class PRList
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim vStatus As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Initialization"

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|B010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAdd.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAdd, btnAdd.GetType(), "btnAdd", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboDepartment)
            End Try
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New DataSet
        sGlobal.getMenu("B010")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "B010")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        gs_GeneratePRNo = ""
        gs_Modify = False
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then

            Dim dfrom As Date
            Dim dto As Date
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dto = Year(Now) & "-" & Month(Now) & "-" & Day(Now)
            ReqDate_From.Value = dfrom
            ReqDate_To.Value = dto
            cboPRType.SelectedIndex = 0
            cboDepartment.SelectedIndex = 0
            cboSection.SelectedIndex = 0
            cboStatusPR.SelectedIndex = 0

            If statusAdmin = "1" Then
                up_FillCombo(cboDepartment, statusAdmin, pUser, "Department")
                up_FillCombo(cboSection, statusAdmin, pUser, "Section")
                If cboSection.Text = "" Then
                    cboSection.Text = "ALL"
                End If
            Else
                up_FillCombo(cboDepartment, statusAdmin, pUser, "Department")
                up_FillCombo(cboSection, statusAdmin, pUser, "Section", cboDepartment.Value)
                If cboSection.Text = "" Then
                    cboSection.Text = "ALL"
                End If
            End If

            up_GridHeader("1")

        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_GridLoad()
        Dim ErrMsg As String = ""
        Dim dtFrom, dtTo As String
        Dim vPRType As String = ""
        Dim vDepartment As String = ""
        Dim vSection As String = ""        
        Dim ls_Back As Boolean = False

        dtFrom = Format(ReqDate_From.Value, "yyyy-MM-dd")
        dtTo = Format(ReqDate_To.Value, "yyyy-MM-dd")

        If cboPRType.Text <> "" Then
            vPRType = cboPRType.SelectedItem.GetValue("Code").ToString()
        End If

        If cboDepartment.Text <> "" Then
            vDepartment = Trim(tmpDepartment.Text) 'cboDepartment.SelectedItem.GetValue("Code").ToString()
        End If

        If cboSection.Text <> "" Then
            vSection = Trim(tmpSection.Text) 'cboSection.SelectedItem.GetValue("Code").ToString()
        End If

        Select Case cboStatusPR.SelectedIndex
            Case 0
                vStatus = "ALL"
            Case 1 'Draft
                vStatus = "0"
            Case 2 'Submit
                vStatus = "1"
            Case 3 'Approved
                vStatus = "2"
            Case 4 'Accept
                vStatus = "3"
            Case 5 'Reject
                vStatus = "4"
            Case 6
                vStatus = "5"
        End Select

        up_GridHeader()

        Dim ds As New DataSet
        ds = ClsPRListDB.GetList(dtFrom, dtTo, vPRType, vDepartment, vSection, vStatus, ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count = 0 Then
                up_GridHeader("1")
            End If
            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_Excel()
        up_GridLoad()

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=PRList" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub up_FillCombo(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                             pAdminStatus As String, pUserID As String, _
                             Optional ByRef pGroup As String = "", _
                             Optional ByRef pParent As String = "", _
                             Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""
        If pGroup = "Section" Then
            ds = ClsPRListDB.FillComboSection(pUserID, pAdminStatus, pGroup, pParent, pErr)
        Else
            ds = ClsPRListDB.FillCombo(pUserID, pAdminStatus, pGroup, pParent, pErr)
        End If

        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_GridHeader(Optional ByRef pLoad As String = "")
        Dim ErrMsg As String = ""
        Dim dtFrom, dtTo As String
        Dim vPRType As String = ""
        Dim vDepartment As String = ""
        Dim vSection As String = ""
        Dim vApproval As String = ""
        Dim ls_Back As Boolean = False

        dtFrom = Format(ReqDate_From.Value, "yyyy-MM-dd")
        dtTo = Format(ReqDate_To.Value, "yyyy-MM-dd")

        If cboPRType.Text <> "" Then
            vPRType = cboPRType.SelectedItem.GetValue("Code").ToString()
        End If

        If cboDepartment.Text <> "" Then
            vDepartment = Trim(tmpDepartment.Text) 'cboDepartment.SelectedItem.GetValue("Code").ToString()
        End If

        If cboSection.Text <> "" Then
            vSection = Trim(tmpSection.Text) 'cboSection.SelectedItem.GetValue("Code").ToString()
        End If

        If pLoad = "1" Then
            vSection = "xx"
        End If

        Grid.Columns.Item(9).Visible = False
        Grid.Columns.Item(15).Visible = False
        Grid.Columns.Item(16).Visible = False
        Grid.Columns.Item(17).Visible = False

        Dim ds As New DataSet
        ds = ClsPRListDB.GetHeaderPRList(dtFrom, dtTo, vPRType, vDepartment, vSection, pUser, vStatus, ErrMsg)

        If ErrMsg = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If Trim(ds.Tables(0).Rows(i)("Approval_Name")) = "Department Head" Then
                    Grid.Columns.Item(15).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(15).Visible = True
                ElseIf Trim(ds.Tables(0).Rows(i)("Approval_Name")) = "Director" Then
                    Grid.Columns.Item(17).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(17).Visible = True
                Else
                    Grid.Columns.Item(16).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(16).Visible = True
                End If
            Next
        End If
    End Sub
#End Region

#Region "Control Event"
    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs)
        If fRefresh = False Then
            up_GridLoad()
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridHeader("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else
            If ReqDate_From.Value > ReqDate_To.Value Then
                cbMessage.JSProperties("cbMessage") = "Request Date To must be higher than Request Date From"
                Exit Sub
            End If
            up_GridLoad()

            gs_dtFromBack = ReqDate_From.Value
            gs_dtToBack = ReqDate_To.Value
            gs_PRType = cboPRType.SelectedIndex
            gs_PRTypeCode = cboPRType.SelectedItem.GetValue("Code").ToString()
            gs_Department = cboDepartment.SelectedIndex
            gs_DepartmentCode = Trim(tmpDepartment.Text) 'cboDepartment.SelectedItem.GetValue("Code").ToString()
            gs_Section = cboSection.SelectedIndex
            gs_SectionCode = Trim(tmpSection.Text) 'cboSection.SelectedItem.GetValue("Code").ToString()
            gs_NewPR = "1"
        End If
    End Sub

    'Protected Sub btnShowData_Click(sender As Object, e As EventArgs) Handles btnShowData.Click
    '    'up_GridLoad()

    '    'gs_dtFromBack = ReqDate_From.Value
    '    'gs_dtToBack = ReqDate_To.Value
    '    'gs_PRType = cboPRType.SelectedIndex
    '    'gs_PRTypeCode = cboPRType.SelectedItem.GetValue("Code").ToString()
    '    'gs_Department = cboDepartment.SelectedIndex
    '    'gs_DepartmentCode = cboDepartment.SelectedItem.GetValue("Code").ToString()
    '    'gs_Section = cboSection.SelectedIndex
    '    'gs_SectionCode = cboSection.SelectedItem.GetValue("Code").ToString()
    'End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub cbMessage_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbMessage.Callback
        DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/PRListItem.aspx")
    End Sub

    Private Sub cboSection_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSection.Callback
        Dim Parentgroup As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        up_FillCombo(cboSection, statusAdmin, pUser, "Section", Parentgroup, errmsg)
        cboSection.SelectedIndex = 0
    End Sub

    Private Sub cbTmp_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbTmp.Callback
        Dim pFunction As String = e.Parameter

        Select Case pFunction
            Case "Code"

        End Select
    End Sub

#End Region

End Class
﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class PO_ViewCEReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim UserID As String = ""
        Dim PR_Number As String = ""
        Dim CE_Number As String = ""
        Dim CE_Revision As Integer

        UserID = Split(Request.QueryString("ID"), "|")(0)
        PR_Number = Split(Request.QueryString("ID"), "|")(1)
        CE_Number = Split(Request.QueryString("ID"), "|")(3)
        CE_Revision = Split(Request.QueryString("ID"), "|")(4)




        up_LoadReport(CE_Number, CE_Revision)
        ' up_LoadReport_Resume()
    End Sub

    Private Sub up_LoadReport(pCENumber As String, pRev As Integer)
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptCostEstimation
      
        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_CE_CostEstimationReport '" & pCENumber & "' , " & pRev & ""

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "CostEstimation_" & Format(CDate(Now), "yyyyMMdd_HHmmss")

                'Dim sql2 As String
                'Dim ds2 As New DataSet
                'sql2 = "exec sp_CostEstimationResume_Report '" & pCENumber & "' , " & pRev & ""

                'Dim da2 As New SqlDataAdapter(sql2, con)
                'da2.Fill(ds2)
                'Report.XrSubreport1.Report.DataSource = ds2
                'Report.XrSubreport1.Report.ApplyFiltering()
                ASPxDocumentViewer1.Report = Report
                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
       Dim pPRNumber As String
        Dim pUserID As String
        pPRNumber = Split(Request.QueryString("ID"), "|")(1)
        pUserID = Split(Request.QueryString("ID"), "|")(0)

        Response.Redirect("~/PO_Notification.aspx?ID=" & pUserID & "|" & pPRNumber)

    End Sub

End Class
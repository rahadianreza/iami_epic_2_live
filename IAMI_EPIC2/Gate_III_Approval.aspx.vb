﻿Imports DevExpress.Web.ASPxGridView

Public Class Gate_III_Approval
    Inherits System.Web.UI.Page
#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim grpcomm As String = ""
    Dim flag As Integer = 0
    Dim pt As String = ""
    Dim prtno As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("M030")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        prj = Request.QueryString("projectid")
        grp = Request.QueryString("groupid")
        comm = Request.QueryString("commodity")
        grpcomm = Request.QueryString("groupcomodity")
        pt = Request.QueryString("projecttype")

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If String.IsNullOrEmpty(prj) Then
                cboProject.SelectedIndex = 0
                cboGroup.SelectedIndex = 0
                cboCommodity.SelectedIndex = 0
                cboGroupCommodity.SelectedIndex = 0
                cboPartNo.SelectedIndex = 0
            End If

            cboProject.Value = prj
            cboGroup.Value = grp
            cboCommodity.Value = comm
            cboGroupCommodity.Value = grpcomm
            cboPartNo.Value = prtno
   
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboPartNo, cboProject.Value, cboGroup.Value, cboCommodity.Value, "R", statusAdmin, pUser)
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub Grid_CustomButtonCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs) Handles Grid.CustomButtonCallback

    End Sub

    Private Sub Grid_CustomButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonEventArgs) Handles Grid.CustomButtonInitialize
        If e.VisibleIndex = -1 Then
            Return
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else
            up_GridLoad("1")
        End If
    End Sub

    Private Sub up_FillComboGroupCommodityPIC(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _projid As String, _grpid As String, _comm As String, _type As String, _
                           pAdminStatus As String, pUserID As String, _
                           Optional ByRef pGroup As String = "", _
                           Optional ByRef pParent As String = "", _
                           Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsGateICheckSheetDB.FillComboProject("11", "", _projid, _grpid, _comm, _type, pUserID, statusAdmin, pmsg)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsItemListSourcingDB.getGateIIIApproval(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, cboPartNo.Value, pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()

            For x As Integer = 0 To Ses.Tables(0).Rows.Count - 1
                GetApprovalPerson(Ses.Tables(0).Rows(x).Item("Project_ID").ToString(), Ses.Tables(0).Rows(x).Item("Group_ID").ToString(), Ses.Tables(0).Rows(x).Item("Commodity").ToString(), Ses.Tables(0).Rows(x).Item("Group_Comodity").ToString(), _
                                  Ses.Tables(0).Rows(x).Item("Part_No").ToString())
            Next
        End If
    End Sub
#End Region

#Region "Event Control"
   Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProject As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroup, pProject, "", "", "1", statusAdmin, pUser, "")
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboGroupCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboPartNo_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboPartNo.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboPartNo, cboProject.Value, cboGroup.Value, cboCommodity.Value, "R", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboPartNo, cboProject.Value, cboGroup.Value, cboCommodity.Value, "R", statusAdmin, pUser)
        End If
    End Sub

    Public Sub GetApprovalPerson(_projectid As String, _groupid As String, _commodity As String, _groupcomodity As String, _partno As String)
        Dim ds As DataSet = clsItemListSourcingDB.getApprovalGate3Person(_projectid, _groupid, _commodity, _groupcomodity, _partno, pUser)

        If flag < 1 Then
            Dim bandColumn As GridViewBandColumn

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                For y As Integer = 4 To ds.Tables(0).Columns.Count - 1
                    If ds.Tables(0).Columns(y).ColumnName.Contains("o_Person") Then
                        bandColumn = New GridViewBandColumn()
                        bandColumn.Caption = ds.Tables(0).Rows(i)(y + 2).ToString
                        bandColumn.VisibleIndex = 15
                    End If

                    If ds.Tables(0).Columns(y).ColumnName.Contains("o_ApprovalName") Then
                        Grid.Columns.Add(bandColumn)
                    Else
                        Dim col As GridViewDataTextColumn = New GridViewDataTextColumn()
                        col.Caption = IIf(ds.Tables(0).Columns(y).ColumnName.Contains("o_Person"), "Approval Person", "Approval Date")
                        col.FieldName = ds.Tables(0).Columns(y).ColumnName
                        If col.Caption = "Approval Date" Then
                            col.PropertiesTextEdit.DisplayFormatString = "dd MMM yyyy"
                        End If
                        bandColumn.Columns.Add(col)
                    End If
                Next
            Next
            flag += 1
        End If
    End Sub
#End Region
End Class
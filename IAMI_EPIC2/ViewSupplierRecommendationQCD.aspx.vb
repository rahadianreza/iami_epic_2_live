﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewSupplierRecommendationQCD
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        up_LoadReport()
    End Sub

    Private Sub up_LoadReport()
        Dim sql As String
        Dim ds As New DataSet
        Dim Report2 As New rptSupplierRecommendationQCDRow2
        Dim Report3 As New rptSupplierRecommendationQCDRow3
        Dim Report4 As New rptSupplierRecommendationQCDRow4
        Dim Report5 As New rptSupplierRecommendationQCDRow5
        Dim pSRNumber As String
        Dim pRev As Integer
        Dim pCPNumber As String
        Dim pSupplierRecommend As String
        Dim pConsideration As String
        Dim pNotes As String
        Dim pStatus As String
        Dim pJumlah As String

        pJumlah = Session("Jumlah")
        pSRNumber = Session("SRNumber")
        pRev = Session("Revision")
        pCPNumber = Session("CPNumber")
        pSupplierRecommend = Session("SupplierRecommend")
        pConsideration = Session("Consideration")
        pNotes = Session("Notes")
        pStatus = Session("Status")

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_SR_ReportSupplierRecommendationQCD '" & pSRNumber & "' , " & pRev & ""

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)

                If pJumlah = "2" Then
                    Report2.DataSource = ds
                    Report2.Name = "SupplierRecommendationQCD" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                    ASPxDocumentViewer1.Report = Report2
                ElseIf pJumlah = "3" Then
                    Report3.DataSource = ds
                    Report3.Name = "SupplierRecommendationQCD" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                    ASPxDocumentViewer1.Report = Report3
                ElseIf pJumlah = "4" Then
                    Report4.DataSource = ds
                    Report4.Name = "SupplierRecommendationQCD" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                    ASPxDocumentViewer1.Report = Report4
                Else
                    Report5.DataSource = ds
                    Report5.Name = "SupplierRecommendationQCD" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                    ASPxDocumentViewer1.Report = Report5
                End If

                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim pSRNumber As String
        Dim pRev As Integer
        Dim pCPNumber As String
        Dim pSupplierRecommend As String
        Dim pConsideration As String
        Dim pNotes As String
        Dim pStatus As String
        Dim pJumlah As String

        pSRNumber = Session("SRNumber")
        pRev = Session("Revision")
        pCPNumber = Session("CPNumber")
        pSupplierRecommend = Session("SupplierRecommend")
        pConsideration = Session("Consideration")
        pNotes = Session("Notes")
        pStatus = Session("Status")
        pJumlah = Session("Jumlah")

        Response.Redirect("~/QDCComparisonDetail.aspx")
    End Sub

End Class
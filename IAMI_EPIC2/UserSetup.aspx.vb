﻿Imports System.Drawing
Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxEditors
Imports IAMIEngine

Public Class UserSetup
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim thispage As String = "~/UserSetup.aspx"
    Dim menuId As String = "Z020"
    Dim nullval As Boolean = False
    Dim statusAdmin As String
    Dim pUser As String

    Public lb_AuthInsert As Boolean = False, _
           lb_AuthUpdate As Boolean = False, _
           lb_AuthDelete As Boolean = False
    Dim clsDESEncryption As New clsDESEncryption("TOS")    
#End Region

#Region "Procedure"
    Private Sub clear()
        txtUserId.Text = ""
        txtFullName.Text = ""
        txtPasswordUS.Text = ""
        txtConfPassword.Text = ""
        cboUserGroup.SelectedIndex = -1
        txtDesc.Text = ""
        txtEmail.Text = ""
    End Sub

    Private Sub up_GridLoadUser()
        Dim cUserSetup As New clsUserSetup
        Dim pErr As String = ""
        'show_error(MsgTypeEnum.Info, "Ready")
        Dim ds As List(Of clsUserSetup)        
        cUserSetup.AppID = sGlobal.appID
        ds = clsUserSetupDB.GetList(cUserSetup, pErr)
        If pErr = "" Then
            gridUser.DataSource = ds
            gridUser.DataBind()
            If ds Is Nothing Or ds.Count = 0 Then
                show_error(MsgTypeEnum.Warning, "Data is not found!")
            End If
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr)
        End If
    End Sub

    Private Sub up_GridLoadUserSearch()
        Dim cUserSetup As New clsUserSetup
        Dim pErr As String = ""
        'show_error(MsgTypeEnum.Info, "Ready")
        Dim ds As New DataSet

        cUserSetup.UserID = txtsearch.Text.Trim
        ds = clsUserSetupDB.GetListUserIDSearch(cUserSetup, pErr)
        If pErr = "" Then
            With ASPxCallback2
                If ds.Tables(0).Rows.Count > 0 Then
                    .JSProperties("cpUserId") = Trim(ds.Tables(0).Rows(0)("UserID") & "")
                    .JSProperties("cpFullName") = Trim(ds.Tables(0).Rows(0)("UserName") & "")
                    .JSProperties("cpLocked") = ds.Tables(0).Rows(0)("Locked")
                    .JSProperties("cpStatusAdmin") = ds.Tables(0).Rows(0)("AdminStatus")
                    .JSProperties("cpDescription") = Trim(ds.Tables(0).Rows(0)("Description") & "")
                    .JSProperties("cpEmail") = Trim(ds.Tables(0).Rows(0)("User_Email") & "")
                End If
            End With
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr)
        End If
    End Sub

    Private Sub up_GridLoadMenu(ByVal pUserID As String)
        Dim cUserSetup As New clsUserSetup
        Dim pErr As String = ""
        'show_error(MsgTypeEnum.Info, "Ready")
        Dim dsMenu As List(Of clsUserMenu)

        cUserSetup.UserID = pUserID
        cUserSetup.AppID = sGlobal.appID
        dsMenu = clsUserSetupDB.GetListMenuPrivilege(cUserSetup, pErr)
        If pErr = "" Then
            gridMenu.DataSource = dsMenu
            gridMenu.DataBind()
            If dsMenu Is Nothing Then
                show_error(MsgTypeEnum.Warning, "Data is not found!")
            End If
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr)
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        gridUser.JSProperties("cp_message") = ErrMsg
        gridUser.JSProperties("cp_type") = msgType
        gridUser.JSProperties("cp_val") = pVal
    End Sub

    Private Function ValidasiJSOX(ByVal pUserID As String, pPassword As String) As String
        Try
            Dim cJSOXSetup As New clsJSOXSetup            
            cJSOXSetup.RuleID = "1"
            Dim jsox As clsJSOXSetup = clsJSOXSetupDB.GetRule(cJSOXSetup)
            Dim MinimumLength As Integer = jsox.ParamValue
            Dim sMsg As String = ""
            If jsox.Enable And Len(pPassword) < MinimumLength Then
                sMsg = "Password length minimum " & MinimumLength & " characters"
                txtPasswordUS.Focus()
                Return sMsg
            End If

            cJSOXSetup.RuleID = "7"
            jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
            If jsox.Enable = False And InStr(pPassword.ToUpper, pUserID.ToUpper) > 0 Then
                sMsg = "Password cannot contain User ID"
                txtPasswordUS.Focus()
                Return sMsg
            End If

            cJSOXSetup.RuleID = "8"
            jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
            If jsox.Enable And Not (pPassword.Contains("1") Or pPassword.Contains("2") Or pPassword.Contains("3") Or pPassword.Contains("4") Or pPassword.Contains("5") Or pPassword.Contains("6") Or pPassword.Contains("7") Or pPassword.Contains("8") Or pPassword.Contains("9") Or pPassword.Contains("0")) Then
                sMsg = "Password must contain minimum 1 numeric"
                txtPasswordUS.Focus()
                Return sMsg
            End If

            cJSOXSetup.RuleID = "9"
            jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
            If jsox.Enable = False Then
                Dim PrevChar As String = Mid(pPassword, 1, 1)
                For i As Integer = 2 To pPassword.Length
                    If Mid(pPassword, i, 1) = PrevChar Then
                        sMsg = "Password cannot contain repeating characters"
                        show_error(MsgTypeEnum.Warning, sMsg)
                        txtPasswordUS.Focus()
                        Return sMsg
                    Else
                        PrevChar = Mid(pPassword, i, 1)
                    End If
                Next
            End If

            cJSOXSetup.RuleID = 5
            jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
            'If jsox.Enable And jsox.ParamValue > 0 Then
            '    Dim st As New clsCon
            '    st.CONN_METHOD = clsCon.CONNECTION_METHOD.ASPNET                
            '    Dim PassList As List(Of clsPasswordHistory) = clsPasswordHistoryDB.GetList(pUserID, jsox.ParamValue, st.ConnectionString)
            '    For Each Pwd In PassList
            '        Dim OldPwd As String = clsDESEncryption.DecryptData(Pwd.Password)
            '        If pPassword = OldPwd Then
            '            sMsg = "Password cannot be the same with your last " & jsox.ParamValue & " passwords"
            '            show_error(MsgTypeEnum.Warning, sMsg)
            '            Return sMsg
            '        End If
            '    Next
            'End If

            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function uf_SaveData(ByVal pUserID As String,
                            Optional ByVal pFullName As String = "",
                            Optional ByVal pPwd As String = "",
                            Optional ByVal pStatusAdmin As String = "",
                            Optional ByVal pDesc As String = "",
                            Optional ByVal pEmail As String = "",
                            Optional ByVal pLock As String = "",
                            Optional ByVal pCatererID As String = "",
                            Optional ByVal pSupplier As String = "",
                            Optional ByVal pDepartment As String = "",
                            Optional ByVal pSection As String = "",
                            Optional ByVal pJobPos As String = "",
                            Optional ByVal pPIC_PO As String = "") As String
        Dim pErr As String = ""
        pPwd = clsDESEncryption.EncryptData(Trim(pPwd))
        Dim UserSetup As New Cls_UserSetup With {.UserID = pUserID, .UserName = pFullName, .Password = pPwd, .Email = pEmail, .Description = pDesc, .AdminStatus = pStatusAdmin,
                                                 .Locked = pLock, .CatererID = pCatererID, .UserType = pStatusAdmin, .Supplier = pSupplier, .Department = pDepartment,
                                                 .Section = pSection, .JobPos = pJobPos, .PIC_PO = pPIC_PO}
        Cls_UserSetup_DB.Insert(UserSetup, pErr)
        If pErr = "" Then
            Dim His As New clsPasswordHistory
            His.UserID = pUserID
            His.Password = pPwd
            clsPasswordHistoryDB.Insert(His)
            Return ""
        End If
        Return pErr
    End Function

    Private Function uf_SaveDataByUP(ByVal pbyUserID As String) As String
        Dim ls_SQL As String = "", ls_MenuID As String = "", ls_MsgID As String = ""
        Dim iLoop As Long = 0, jLoop As Long = 0
        Dim ls_AllowAccess As String = "", ls_AllowUpdate As String = "", ls_AllowSpecial As String = ""
        Dim ls_UserID As String = ""
        Dim ds As New DataSet
        ls_UserID = Trim(txtUserId.Text)

        Dim pErr As String = ""
        ds = Cls_UserSetup_DB.GetMenuDS(pbyUserID, pErr)
        If pErr = "" Then
            If ds Is Nothing Then
                Return "Data is not found"
            End If
        Else
            Return pErr
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0)
                For iLoop = 0 To .Rows.Count - 1
                    ls_MenuID = ds.Tables(0).Rows(iLoop)("MenuID")
                    ls_AllowAccess = ds.Tables(0).Rows(iLoop)("AllowAccess")
                    ls_AllowUpdate = ds.Tables(0).Rows(iLoop)("AllowUpdate")

                    Dim UserPrevilege As New Cls_ss_UserPrivilege With {.UserID = ls_UserID, .MenuID = ls_MenuID, .AllowAccess = ls_AllowAccess, .AllowUpdate = ls_AllowUpdate}
                    Cls_ss_UserPrivilegeDB.Save(UserPrevilege, pErr)
                    If pErr <> "" Then
                        Return pErr
                    End If
                Next
            End With
        End If
        Return ""
    End Function

    Private Sub up_FillComboCategory(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, pParent As String, pGroup As String, pPRType As String, Optional ByRef pErr As String = "")
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim vParent As String
        Dim vGroup As String

        vParent = pParent
        vGroup = pGroup

        'ds = ClsPRListDB.GetDataCategory(vParent, vGroup, pPRType)
        ds = ClsPRListDB.FillComboDetail(pUser, statusAdmin, vGroup, vParent)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

#End Region

#Region "Control Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu(menuId)
        Master.SiteTitle = sGlobal.menuName
        show_error(MsgTypeEnum.Info, "", 0)
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            gridUser.FocusedRowIndex = -1
            gridMenu.FocusedRowIndex = -1
            Call clear()
            Call up_GridLoadUser()
            Call up_GridLoadMenu("")
        End If
        lb_AuthUpdate = sGlobal.Auth_UserUpdate(Session("user"), menuId)

        If lb_AuthUpdate = False Then
            btnSubmit.Enabled = False
            btnDelete.Enabled = False
        End If
    End Sub

    Private Sub gridUser_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles gridUser.AfterPerformCallback
        Call up_GridLoadUser()
    End Sub

    Private Sub gridUser_CustomCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles gridUser.CustomCallback
        Dim pAction As String = Split(e.Parameters, "|")(0)
        Dim pUserID As String = Split(e.Parameters, "|")(1)
        Dim pPassword As String = ""
        If UBound(Split(e.Parameters)) > 2 Then
            pPassword = Split(e.Parameters, "|")(3)
        End If
        gridUser.JSProperties("cpMessage") = ""
        Try
            Select Case pAction
                Case "load"
                    Call up_GridLoadUser()
                    show_error(MsgTypeEnum.Info, "", 0)
                Case "save"
                    Call up_GridLoadUser()
                    show_error(MsgTypeEnum.Success, "Save data successful")
                Case "delete"
                    Dim pErr As String = ""
                    Dim UserSetup As New Cls_UserSetup With {.UserID = pUserID}
                    Cls_UserSetup_DB.Delete(UserSetup, pErr)
                    If pErr <> "" Then
                        show_error(MsgTypeEnum.ErrorMsg, pErr)
                    Else
                        txtUserIDTemp.Text = ""
                        txtFullName.Text = ""
                        txtPasswordUS.Text = ""
                        txtConfPassword.Text = ""
                        Call up_GridLoadUser()
                        Call up_GridLoadMenu(pUserID)
                        show_error(MsgTypeEnum.Success, "Delete data successful")
                        clear()
                    End If
            End Select
            'txtUserIDTemp.ForeColor = Color.FromName("#96C8FF")
        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub

    Private Sub gridMenu_CustomCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles gridMenu.CustomCallback
        Dim pAction As String = Split(e.Parameters, "|")(0)
        Dim pUserID As String = Split(e.Parameters, "|")(1)
        gridUser.JSProperties("cpMessage") = ""
        Try
            Select Case pAction
                Case "load"
                    If pUserID <> "kosong" Then
                        Call up_GridLoadMenu(pUserID)
                        gridMenu.PageIndex = 0
                    Else
                        Call up_GridLoadMenu("")
                    End If

                Case "loadPrivilege"
                    If cboUserGroup.Text <> "" Then
                        Call up_GridLoadMenu(Split(e.Parameters, "|")(1))
                    Else
                        Call up_GridLoadMenu(Split(e.Parameters, "|")(1))
                    End If
            End Select

        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message)
        End Try
    End Sub

    Private Sub gridMenu_BatchUpdate(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles gridMenu.BatchUpdate
        Dim ls_SQL As String = "", ls_MenuID As String = "", ls_MsgID As String = ""
        Dim iLoop As Long = 0, jLoop As Long = 0
        Dim ls_AllowAccess As String = "", ls_AllowUpdate As String = "", ls_AllowSpecial As String = "", ls_Active As String = ""
        Dim ls_UserID As String = ""
        ls_UserID = Trim(txtUserId.Text)
        If e.UpdateValues.Count = 0 Then
            show_error(MsgTypeEnum.Warning, "Data is not found!")
            Exit Sub
        End If
        Dim a As Integer
        a = e.UpdateValues.Count
        Dim pErr As String = ""
        For iLoop = 0 To a - 1
            ls_AllowAccess = (e.UpdateValues(iLoop).NewValues("AllowAccess").ToString())
            ls_AllowUpdate = (e.UpdateValues(iLoop).NewValues("AllowUpdate").ToString())

            If ls_AllowAccess = True Then ls_AllowAccess = "1" Else ls_AllowAccess = "0"
            If ls_AllowUpdate = True Then ls_AllowUpdate = "1" Else ls_AllowUpdate = "0"

            ls_MenuID = Trim(e.UpdateValues(iLoop).NewValues("MenuID").ToString())


            Dim UserPrevilege As New Cls_ss_UserPrivilege With {.UserID = ls_UserID, .MenuID = ls_MenuID, .AllowAccess = ls_AllowAccess, .AllowUpdate = ls_AllowUpdate}
            Cls_ss_UserPrivilegeDB.Save(UserPrevilege, pErr)
            If pErr <> "" Then
                Exit For
            End If
        Next iLoop
        clear()
        gridMenu.EndUpdate()
    End Sub

    Private Sub gridMenu_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles gridMenu.CellEditorInitialize
        If (e.Column.FieldName = "GroupID" Or e.Column.FieldName = "MenuID" Or e.Column.FieldName = "MenuDesc") And CType(sender, DevExpress.Web.ASPxGridView.ASPxGridView).IsNewRowEditing = False Then
            e.Editor.ReadOnly = True
        Else
            e.Editor.ReadOnly = False
        End If
    End Sub

    Private Sub ASPxCallback1_Callback(ByVal source As Object, ByVal e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles ASPxCallback1.Callback
        Dim cUserSetup As New clsUserSetup
        Dim pErr As String = ""
        Dim ls_SQL As String = ""
        Dim pwd As String = ""
        Dim pAction As String = Split(e.Parameter, "|")(0)

        Dim dsUser As New DataSet
        cUserSetup.UserID = txtUserId.Text.Trim
        dsUser = clsUserSetupDB.GetListUserIDSearch(cUserSetup, pErr)

        If dsUser.Tables(0).Rows.Count > 0 Then
            pwd = dsUser.Tables(0).Rows(0)("Password")
            pwd = clsDESEncryption.DecryptData(pwd)
        End If
        If pAction = "search" Then
            cUserSetup.UserID = txtsearch.Text.Trim
            dsUser = clsUserSetupDB.GetListUserIDSearch(cUserSetup, pErr)

            If dsUser.Tables(0).Rows.Count > 0 Then
                pwd = dsUser.Tables(0).Rows(0)("Password")
                pwd = clsDESEncryption.DecryptData(pwd)
            End If
        End If
        e.Result = pwd
    End Sub

    Protected Sub ASPxTextBox_CustomJSProperties(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxClasses.CustomJSPropertiesEventArgs)
        Dim cUserSetup As New clsUserSetup
        Dim ls_SQL As String = ""
        Dim pwd As String = ""
        Dim pErr As String = ""
        If txtUserIDTemp.Text <> "" Then
            Dim dsUser As New DataSet

            cUserSetup.UserID = txtUserId.Text.Trim
            dsUser = clsUserSetupDB.GetListUserIDSearch(cUserSetup, pErr)

            If dsUser.Tables(0).Rows.Count > 0 Then
                pwd = dsUser.Tables(0).Rows(0)("Password")
                pwd = clsDESEncryption.DecryptData(pwd)
            End If
        End If
        e.Properties("cp_myPassword") = pwd
    End Sub

    Private Sub ASPxCallback2_Callback(ByVal source As Object, ByVal e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles ASPxCallback2.Callback
        Dim pAction As String = Split(e.Parameter, "|")(0)
        Select Case pAction
            Case "search"
                Call up_GridLoadUserSearch()
        End Select

    End Sub

    Private Sub cbkValid_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbkValid.Callback
        Dim pAction As String = Split(e.Parameter, "|")(0)
        Dim pUserID As String = Split(e.Parameter, "|")(1)
        Dim pUserName As String = Split(e.Parameter, "|")(2)
        Dim pPassword As String = ""
        Dim pStatusAdmin As String = ""
        Dim pDesc As String = ""
        Dim pGroup As String = ""
        Dim pLock As String = ""
        Dim pCatererID As String = ""
        Dim pSupplier As String = ""
        Dim pDepartment As String = ""
        Dim pSection As String = ""
        Dim pJobPos As String = ""
        Dim pEmail As String = ""
        Dim pPIC_PO As String = ""
        If UBound(Split(e.Parameter, "|")) > 2 Then
            pPassword = Split(e.Parameter, "|")(3)
            pStatusAdmin = Split(e.Parameter, "|")(4)
            pDesc = Split(e.Parameter, "|")(5)
            pGroup = Split(e.Parameter, "|")(6)
            pLock = Split(e.Parameter, "|")(7)
            pCatererID = ""
            pSupplier = Split(e.Parameter, "|")(8)
            pDepartment = Split(e.Parameter, "|")(9)
            pSection = Split(e.Parameter, "|")(10)
            pJobPos = Split(e.Parameter, "|")(11)
            pEmail = Split(e.Parameter, "|")(12)
            pPIC_PO = Split(e.Parameter, "|")(13)

        End If
        Dim strMsg As String = "" 'ValidasiJSOX(pUserID, pPassword)
        If strMsg = "" Then
            strMsg = uf_SaveData(pUserID, pUserName, pPassword, pStatusAdmin, pDesc, pEmail, pLock, pCatererID, pSupplier, pDepartment, pSection, pJobPos, pPIC_PO)
        End If
        If strMsg = "" And pGroup <> "" Then
            strMsg = uf_SaveDataByUP(pGroup)
        End If
        cbkValid.JSProperties("cpValidationMsg") = strMsg
    End Sub

    Private Sub gridMenu_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles gridMenu.RowUpdating
        e.Cancel = True
    End Sub

    Private Sub cbTmp_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbTmp.Callback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameter
        Dim code As String
        Dim sql As String

        Select Case pFunction
            Case "Code" 'tmpSupplier
                If cboCaterer.SelectedIndex = -1 Then
                    code = ""
                Else
                    code = cboCaterer.SelectedItem.GetValue("SupplierID").ToString()
                End If

                cbTmp.JSProperties("cpCode") = code
                cbTmp.JSProperties("cpCode2") = ""
                cbTmp.JSProperties("cpCode3") = ""
                cbTmp.JSProperties("cpCode4") = ""
                cbTmp.JSProperties("cpDepartment") = ""
                cbTmp.JSProperties("cpSection") = ""
                cbTmp.JSProperties("cpJobPos") = ""
                cbTmp.JSProperties("cpSupplier") = ""

            Case "Code2" 'tmpDepartment
                If cboDepartment.SelectedIndex = -1 Then
                    code = ""
                Else
                    code = cboDepartment.SelectedItem.GetValue("Code").ToString()
                End If

                cbTmp.JSProperties("cpCode") = ""
                cbTmp.JSProperties("cpCode2") = code
                cbTmp.JSProperties("cpCode3") = ""
                cbTmp.JSProperties("cpCode4") = ""
                cbTmp.JSProperties("cpDepartment") = ""
                cbTmp.JSProperties("cpSection") = ""
                cbTmp.JSProperties("cpJobPos") = ""
                cbTmp.JSProperties("cpSupplier") = ""

            Case "Code3" 'tmpSection
                If cboSection.Text = "" Then
                    code = ""
                Else
                    code = clsUserSetupDB.GetSectionCode(cboSection.Text)
                End If

                cbTmp.JSProperties("cpCode") = ""
                cbTmp.JSProperties("cpCode2") = ""
                cbTmp.JSProperties("cpCode3") = code
                cbTmp.JSProperties("cpCode4") = ""
                cbTmp.JSProperties("cpDepartment") = ""
                cbTmp.JSProperties("cpSection") = ""
                cbTmp.JSProperties("cpJobPos") = ""
                cbTmp.JSProperties("cpSupplier") = ""

            Case "Code4" 'tmpJobPos
                If cboJobPos.SelectedIndex = -1 Then
                    code = ""
                Else
                    code = cboJobPos.SelectedItem.GetValue("Code").ToString()
                End If

                cbTmp.JSProperties("cpCode") = ""
                cbTmp.JSProperties("cpCode2") = ""
                cbTmp.JSProperties("cpCode3") = ""
                cbTmp.JSProperties("cpCode4") = code
                cbTmp.JSProperties("cpDepartment") = ""
                cbTmp.JSProperties("cpSection") = ""
                cbTmp.JSProperties("cpJobPos") = ""
                cbTmp.JSProperties("cpSupplier") = ""

            Case "FillCombo"

                ds = clsUserSetupDB.FillCombo_ReLoad(Trim(tmpDepartment.Text), Trim(tmpSection.Text), Trim(tmpJobPos.Text), Trim(tmpSupplierCode.Text))


                If ds.Tables(0).Rows.Count = 0 Then
                    cbTmp.JSProperties("cpDepartment") = ""
                Else
                    cbTmp.JSProperties("cpDepartment") = ds.Tables(0).Rows(0).Item("Par_Description")
                End If

                If ds.Tables(1).Rows.Count = 0 Then
                    cbTmp.JSProperties("cpSection") = ""
                Else
                    cbTmp.JSProperties("cpSection") = ds.Tables(1).Rows(0).Item("Par_Description")
                End If

                If ds.Tables(2).Rows.Count = 0 Then
                    cbTmp.JSProperties("cpJobPos") = ""
                Else
                    cbTmp.JSProperties("cpJobPos") = ds.Tables(2).Rows(0).Item("Par_Description")
                End If

                If ds.Tables(3).Rows.Count = 0 Then
                    cbTmp.JSProperties("cpSupplier") = ""
                Else
                    cbTmp.JSProperties("cpSupplier") = ds.Tables(3).Rows(0).Item("Supplier_Name")
                End If

                cbTmp.JSProperties("cpCode") = ""
                cbTmp.JSProperties("cpCode2") = ""
                cbTmp.JSProperties("cpCode3") = ""
                cbTmp.JSProperties("cpCode4") = ""

        End Select

    End Sub

    Private Sub cboSection_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSection.Callback
        Dim Parentgroup As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        up_FillComboCategory(cboSection, Parentgroup, "Section", "", errmsg)
        cboSection.SelectedIndex = -1
    End Sub

    'Private Function GetIsEmailExist(ByVal email As String) As Boolean
    '    Using connection As OleDbConnection = GetConnection()
    '        Dim cmd As OleDbCommand = New OleDbCommand("SELECT count(*) FROM [Users] WHERE Email = @Email", connection)
    '        cmd.Parameters.AddWithValue("Email", email)
    '        connection.Open()
    '        Dim total As Integer = CType(cmd.ExecuteScalar(), Int32)
    '        connection.Close()
    '        Return total > 0
    '    End Using
    'End Function

    Protected Sub txtEmail_Validation(sender As Object, e As DevExpress.Web.ASPxEditors.ValidationEventArgs) Handles txtEmail.Validation
        'If Not Page.IsCallback Then
        e.IsValid = e.Value IsNot Nothing

        If Not e.IsValid Then
            e.ErrorText = "Email is required"
            Exit Sub
        End If

        Dim r As Regex = New Regex("^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[;]{0,1}\s*)+$")   '("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
        e.IsValid = r.IsMatch(e.Value.ToString())

        If Not e.IsValid Then
            e.ErrorText = "Email is invalid"
            Exit Sub
        End If

        'e.IsValid = Not GetIsEmailExist(e.Value.ToString())

        'If Not e.IsValid Then
        '    e.ErrorText = "Sorry, this e-mail belongs to an existing account"
        '    Return
        'End If
        ' End If
    End Sub
#End Region


End Class
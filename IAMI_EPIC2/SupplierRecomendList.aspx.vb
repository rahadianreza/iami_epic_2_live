﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class SupplierRecomendList_aspx
    Inherits System.Web.UI.Page
   
#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

    Private Sub up_Excel()
        up_GridLoad(gs_DateFrom, gs_DateTo, gs_Supplier)

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=SupplierRecommendationList.xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using

        ps.Dispose()

    End Sub

    Private Sub up_GridHeader(Optional ByRef pLoad As String = "")
        Dim ErrMsg As String = ""
        Dim dtFrom, dtTo As String
        Dim vApproval As String = ""
        Dim ls_Back As Boolean = False

        dtFrom = Format(dtDateFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtDateTo.Value, "yyyy-MM-dd")

        

        Grid.Columns.Item(7).Visible = False
        Grid.Columns.Item(8).Visible = False
        Grid.Columns.Item(9).Visible = False
        Grid.Columns.Item(10).Visible = False
      
        Dim ds As New DataSet
        ds = clsSupplierRecommendationDB.GetHeaderSRList(dtFrom, dtTo, pUser, ErrMsg)

        If ErrMsg = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If Trim(ds.Tables(0).Rows(i)("Approval_Name")) = "Department Head User" Then 'old"COST CONTROL DEPT" Then
                    Grid.Columns.Item(7).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(7).Visible = True
                ElseIf Trim(ds.Tables(0).Rows(i)("Approval_Name")) = "Cost Control Department Head" Then  'old "ACCOUNTING DEPT" Then
                    Grid.Columns.Item(8).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(8).Visible = True
                ElseIf Trim(ds.Tables(0).Rows(i)("Approval_Name")) = "Accounting Dept Head" Then  'old "GA-ESSR" Then
                    Grid.Columns.Item(9).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(9).Visible = True
                Else
                    Grid.Columns.Item(10).Caption = ds.Tables(0).Rows(i)("Approval_Name") 'GA-ESSR
                    Grid.Columns.Item(10).Visible = True
                End If
            Next
        End If
    End Sub


    Private Sub up_GridLoad(pDateFrom As String, pDateTo As String, pSupplier As String)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        Dim clsSR As New clsSupplierRecommendation


        clsSR.SupplierRecommend = pSupplier
        clsSR.FromDate = pDateFrom
        clsSR.ToDate = pDateTo
        up_GridHeader()
        ds = clsSupplierRecommendationDB.GetListMaster(pUser, clsSR, ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count = 0 Then
                up_GridHeader("1")
            End If
            Grid.DataSource = ds
            Grid.DataBind()
            Grid.JSProperties("cpmessage") = "Grid Load Data Success"
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_FillComboCENumber()
        Dim ds As New DataSet
        Dim pErr As String = ""
        
        '  ds = clsCostEstimationDB.GetComboData(dtfrom, dtto, pErr)
        If pErr = "" Then

            cboSupplier.DataSource = Nothing
            cboSupplier.DataBind()
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|E050", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAdd.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAdd, btnAdd.GetType(), "btnAdd", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
         If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboSupplier)
            End Try
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("E050")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E050")

        Dim dfrom As Date
        dfrom = Year(Now) & "-" & Month(Now) & "-01"
        dtDateFrom.Value = dfrom
        dtDateTo.Value = Now
        up_GridHeader("1")
    End Sub

    'Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
    'gs_DateFrom = Format(dtDateFrom.Value, "yyyy-MM-dd")
    'gs_DateTo = Format(dtDateTo.Value, "yyyy-MM-dd")
    'Response.Redirect("~/CEDetail.aspx")
    'End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        up_GridLoad(gs_DateFrom, gs_DateTo, gs_Supplier)
    End Sub

    'Private Sub btnRefresh_Click(sender As Object, e As System.EventArgs) Handles btnRefresh.Click
    '    up_GridLoad()
    'End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Dim pDateFrom As Date = Split(e.Parameters, "|")(1)
        Dim pDateTo As Date = Split(e.Parameters, "|")(2)
        Dim pSupplierNo As String = Split(e.Parameters, "|")(3)

        Dim vDateFrom As String = Format(pDateFrom, "yyyy-MM-dd")
        Dim vDateTo As String = Format(pDateTo, "yyyy-MM-dd")

        gs_Supplier = pSupplierNo

        gs_DateFrom = vDateFrom
        gs_DateTo = vDateTo

        If pFunction = "gridload" Then
            up_GridHeader("1")
            up_GridLoad(vDateFrom, vDateTo, pSupplierNo)

        End If

    End Sub

    Private Sub cboSupplier_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboSupplier.Callback
       
        up_FillComboCENumber()

        If cboSupplier.Items.Count > 0 Then
            cboSupplier.SelectedIndex = 0
        End If

    End Sub


    'Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
    '    Dim err As String = ""
    '    up_Excel(err)

    '    'Grid.JSProperties("cpmessage") = "Download Excel Successfull"
    'End Sub

    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        up_Excel()
    End Sub


    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("~/SupplierRecomendListDetail.aspx?Date=" & dtDateFrom.Value & "|" & dtDateTo.Value & "")

    End Sub
End Class
﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Drawing

Public Class Material_Steel
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A160")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            Period_From.Value = Now
            Period_To.Value = Now
            cboType.SelectedIndex = 0

           
        End If
    End Sub
#End Region

#Region "Control Event"
    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "load"
                Dim pFrom As String = Split(e.Parameters, "|")(1)
                Dim pTo As String = Split(e.Parameters, "|")(2)
                Dim Type As String = Split(e.Parameters, "|")(3)
                If Format(Period_From.Value, "yyyyMM") > Format(Period_To.Value, "yyyyMM") Then
                    show_error(MsgTypeEnum.Warning, "Period From(" & Format(Period_From.Value, "MMM yyyy") & ") Cannot more than Period To " & Format(Period_To.Value, "MMM yyyy"), 1, Grid)
                Else
                    up_GridLoad(Format(Period_From.Value, "yyyyMM"), Format(Period_To.Value, "yyyyMM"), Type)
                End If

            Case "delete "
                Dim pErr As String = ""
                ClsMaterial_SteelDB.Delete(Split(e.Parameters, "|")(1), Split(e.Parameters, "|")(2), Format(CDate(Split(e.Parameters, "|")(3)), "yyyyMM"), pUser, pErr)
                If pErr = "" Then
                    show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, Grid)
                    up_GridLoad(Format(Period_From.Value, "yyyyMM"), Format(Period_To.Value, "yyyyMM"), Split(e.Parameters, "|")(1))
                Else
                    show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
                End If
        End Select
    End Sub

    Private Sub up_Excel()
        up_GridLoad(Format(Period_From.Value, "yyyyMM"), Format(Period_To.Value, "yyyyMM"), cboType.Value)

        If Grid.VisibleRowCount < 1 Then
            Exit Sub
        End If

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=Electricity" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub
    Protected Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        ClsMaterial_SteelDB.Delete(e.Keys("MaterialType"), e.Keys("VAKind"), e.Keys("Period"), pUser, pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, Grid)
            up_GridLoad(Format(Period_From.Value, "yyyyMM"), Format(Period_To.Value, "yyyyMM"), e.Keys("MaterialType"))
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
        End If
    End Sub

     Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("Material_SteelDetail.aspx")
    End Sub
#End Region

#Region "Procedure"

    Private Sub up_GridLoad(ByVal pFrom As String, ByVal pTo As String, ByVal Type As String)
        Dim ErrMsg As String = ""
        Dim Ses As New DataSet
        Ses = ClsMaterial_SteelDB.getlistds(pFrom, pTo, Type, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub

    Protected Sub btnChart_Click(sender As Object, e As EventArgs) Handles btnChart.Click
        Response.Redirect("Material_SteelChart.aspx")
    End Sub
#End Region



End Class
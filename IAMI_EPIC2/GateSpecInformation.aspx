﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="GateSpecInformation.aspx.vb" Inherits="IAMI_EPIC2.GateSpecInformation" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };

      function MessageError(s, e) {
       // alert(s.cpMessage);
        if (s.cpMessage == "Data Saved Successfully!") {
            //alert(s.cpMessage);
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
         
        }
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/ItemList.aspx";

            

        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    };

         function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "Part_No" || currentColumnName == "Commodity" || currentColumnName == "Upc") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        };  

//        function SaveData(s, e) {
//            cbkValid.PerformCallback('save');
//            Grid.UpdateEdit()
//        }

        function disableenablebuttonsubmit(_val) {
        alert('adsds');
            if (_val = '1') {
                btnSave.SetEnabled(false)
            } else {
                btnSave.SetEnabled(true)
             }
        }

          function OnUpdateCheckedChanged(s, e) {
            if (s.GetValue() == -1) s.SetValue(1);
            Grid.SetFocusedRowIndex(-1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "Status_Drawing", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "Status_Drawing", s.GetValue());
                }                
            }
        }
          function OnTabChanging(s, e) {
            var tabName = (pageControl.GetActiveTab()).name;
            e.cancel = !ASPxClientEdit.ValidateGroup('group' + tabName);
        }

//        function SelectedChanged(s,e){
//            callback1.PerformCallback(s.GetValue());
//        }

//        function GetValueMainPartNCustomer(s, e) {
//           Grid.GetEditor("Main_Parts").SetValue(s.cpGetValue1);
//           Grid.GetEditor("Main_Customer").SetValue(s.cpGetValue2);
//          // alert(s.cpGetValue1);
//        }

        function OnChanged(cmbParent) {
		    if(isUpdating)
			    return;
		    var comboValue = cmbParent.GetSelectedItem().value;
		    if(comboValue)
			    GridCostControl.GetEditor("UserID").PerformCallback(comboValue.toString());
                GridCostControl.GetEditor("UserID").SetValue("");
                GridCostControl.GetEditor("UserName").SetValue("");
	    }

        function OnChangedUserID(cmbParent){
            if(isUpdating)
            return;

            var UserName = cmbParent.GetSelectedItem().GetColumnText('UserName');
            GridCostControl.GetEditor("UserName").SetValue(UserName);
            GridCostControl.GetEditor("UserName").SetEnabled(false);
            //alert(UserName);
        }
        var isUpdating = false;

        function SaveData (s,e){
            cbSave.PerformCallback();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Base Drawing">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboBaseDrawing" runat="server" Value='<%# Bind("ACTIVE") %>' TabIndex="1"
                    ValueType="System.String" Height="25px" Font-Size="9pt" Theme="Office2010Black"
                    Width="120px">
                    <Items>
                        <dx:ListEditItem Text="Isuzu" Value="ISZ" />
                        <dx:ListEditItem Text="Supplier" Value="SPL" />
                    </Items>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
            </td>
            <td style="width: 400px;">
                &nbsp;
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="SOR">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboSOR" runat="server" Value='<%# Bind("ACTIVE") %>' ValueType="System.String"
                    Height="25px" Font-Size="9pt" Theme="Office2010Black" Width="120px" TabIndex="2">
                    <Items>
                        <dx:ListEditItem Text="Need" Value="1" />
                        <dx:ListEditItem Text="No Need" Value="0" />
                    </Items>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
            </td>
            <td style="width: 400px;">
                &nbsp;
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtMaterial" runat="server" Font-Names="Segoe UI" Font-Size="9pt" TabIndex="3"
                    Width="170px" ClientInstanceName="txtMaterial" MaxLength="30">
                </dx:ASPxTextBox>
            </td>
            <td style="width: 400px;">
                &nbsp;
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Process">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtProcess" runat="server" Font-Names="Segoe UI" Font-Size="9pt" TabIndex="4"
                    Width="170px" ClientInstanceName="txtProcess" MaxLength="30">
                </dx:ASPxTextBox>
            </td>
            <td style="width: 400px;">
                &nbsp;
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Check Development Schedule">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboDevSchedule" runat="server" Value='<%# Bind("ACTIVE") %>'
                    ValueType="System.String" Height="25px" Font-Size="9pt" Theme="Office2010Black"
                    Width="120px" TabIndex="5">
                    <Items>
                        <dx:ListEditItem Text="Yes" Value="1" />
                        <dx:ListEditItem Text="No" Value="0" />
                    </Items>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
            </td>
            <td style="width: 400px;">
                &nbsp;
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Additional Information">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtAdditionalInformation" runat="server" Font-Names="Segoe UI" TabIndex="6"
                    Font-Size="9pt" Width="170px" ClientInstanceName="txtAdditionalInformation" MaxLength="30">
                </dx:ASPxTextBox>
            </td>
            <td style="width: 400px;">
                &nbsp;
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="KSH Attendance">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxDateEdit ID="KHSAttendance" runat="server" Theme="Office2010Black" Width="120px" TabIndex="7"
                    ClientInstanceName="KHSAttendance" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <%--  <ClientSideEvents ValueChanged="GridLoad" />--%>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>
            </td>
            <td style="width: 400px;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td colspan="2" style="padding: 20px 0 0 0">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back to List" UseSubmitBehavior="False" TabIndex="8"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnBack" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;
                <%--<dx:ASPxButton ID="btnSave" runat="server" Text="Save"  TabIndex="9"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnSave" Theme="Default">
                    <ClientSideEvents Click="SaveData" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>--%>
                 <dx:ASPxButton ID="btnSubmit" runat="server" Text="Save" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                   
                    <ClientSideEvents Click="SaveData" />
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
        <ClientSideEvents EndCallback="MessageError" />
    </dx:ASPxCallback>
    <br />
    <br />
    <div>
        <dx:ASPxGridView ID="GridCostControl" runat="server" AutoGenerateColumns="False"
            ClientInstanceName="GridCostControl" EnableTheming="True" KeyFieldName="Department_Code;UserID;"
            Theme="Office2010Black" Width="100%" Font-Names="Segoe UI" Font-Size="9pt" >
            <ClientSideEvents EndCallback="OnEndCallback" />
               
            <Columns>
                <dx:GridViewCommandColumn VisibleIndex="0" Width="100px" ShowClearFilterButton="true"
                    ShowDeleteButton="true" ShowEditButton="true"  ShowNewButtonInHeader="true">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataComboBoxColumn Caption="Department" FieldName="Department_Code" VisibleIndex="1"
                    Width="350px">
                    
                    <PropertiesComboBox DataSourceID="sdsDepartment" TextField="Par_Description" ValueField="Par_Code"
                        Width="145px" TextFormatString="{1}" ClientInstanceName="Department" DropDownStyle="DropDownList"
                        IncrementalFilteringMode="StartsWith">
                         <ClientSideEvents SelectedIndexChanged="function(s, e) { OnChanged(s); }" />
                        <Columns>
                            <dx:ListBoxColumn FieldName="Par_Code" Caption="Code" Width="65px" />
                            <dx:ListBoxColumn FieldName="Par_Description" Caption="Description" Width="200px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="2px">
                            <Paddings Padding="2px"></Paddings>
                        </ButtonStyle>
                    </PropertiesComboBox>
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                </dx:GridViewDataComboBoxColumn>
                
                 <dx:GridViewDataTextColumn FieldName="User" Width="150px" Caption="User ID"
                    VisibleIndex="2">
                    <EditFormSettings Visible="False" />
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" /> 
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataComboBoxColumn Caption="User ID" FieldName="UserID" VisibleIndex="3" 
                    Width="0px" >
                    <PropertiesComboBox DataSourceID="sdsCCAll" TextField="UserName" ValueField="UserID"
                        Width="145px" TextFormatString="{0}" ClientInstanceName="UserID" DropDownStyle="DropDownList"
                        IncrementalFilteringMode="StartsWith">
                        <ClientSideEvents SelectedIndexChanged="function (s,e){OnChangedUserID(s);}" />
                       <Columns>
                            <dx:ListBoxColumn FieldName="UserID" Caption="ID" Width="60px" />
                            <dx:ListBoxColumn FieldName="UserName" Caption="Name" Width="200px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="2px">
                            <Paddings Padding="2px"></Paddings>
                        </ButtonStyle>
                    </PropertiesComboBox>
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataTextColumn FieldName="UserName" Width="250px" Caption="User Name"
                    VisibleIndex="4">
                    <EditFormSettings Visible="True" />
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" /> 
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
            <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="320" />
            </SettingsPopup>
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header>
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
                <CommandColumnItem ForeColor="Orange">
                </CommandColumnItem>
            </Styles>
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
            <Templates>
                <EditForm>
                    <div style="padding: 15px 15px 15px 15px">
                        <dx:ContentControl ID="ContentControl1" runat="server">
                            <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                runat="server"></dx:ASPxGridViewTemplateReplacement>
                        </dx:ContentControl>
                    </div>
                    <div style="text-align: left; padding: 5px 5px 5px 15px">
                        <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                        <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                    </div>
                </EditForm>
            </Templates>
        </dx:ASPxGridView>
        <dx1:ASPxLabel ID="lblCostControl" runat="server" Text="CC" Visible="false">
        </dx1:ASPxLabel>
        <%--<asp:SqlDataSource ID="sdsCC" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="sp_SpecInformation_GetDepartment" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="lblCostControl" PropertyName="Text" Name="Department_Code"
                    Type="String" DefaultValue="CC" />
            </SelectParameters>
        </asp:SqlDataSource>--%>
        <asp:SqlDataSource ID="sdsCC" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
		    SelectCommand="SELECT UserID,UserName FROM UserSetup WHERE Department_Code=@Department_Code AND UserId NOT IN (SELECT UserID FROM Gate_I_CS_Spec_Information_User WHERE Department_Code=@Department_Code) AND UserID<>''">
		    <SelectParameters>
			    <asp:Parameter DefaultValue="" Name="Department_Code" Type="String"  />
		    </SelectParameters>
	    </asp:SqlDataSource>
          <asp:SqlDataSource ID="sdsCCAll" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
		    SelectCommand="SELECT UserID,UserName FROM UserSetup">
	    </asp:SqlDataSource>
        <asp:SqlDataSource ID="sdsDepartment" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="SELECT Par_Code, Par_Description FROM Mst_Parameter WHERE Par_Group = 'Department'">
        </asp:SqlDataSource>
         <dx:ASPxCallback ID="callback1" runat="server" ClientInstanceName="callback1" >
        <%--<ClientSideEvents EndCallback="GetValueMainPartNCustomer" />--%>
    </dx:ASPxCallback>
    </div>
</asp:Content>

﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class ExchangeRateReport
    Inherits System.Web.UI.Page

#Region "Declaration"

    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Procedure"

    Private Sub up_LoadGrid()
        Dim ErrMsg As String = ""
        Dim dtFrom = Format(dtDateFrom.Value, "yyyy-MM-dd")
        Dim dtTo = Format(dtDateTo.Value, "yyyy-MM-dd")

        Dim Summary As DataSet
        Summary = clsExchangeRateDB.GetList(dtFrom, dtTo, cboPeriodID.Text, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Summary
            Grid.DataBind()
            Dim Script
            If Summary.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cpMessage") = "Success"
                Script = "btnExcel.SetEnabled(true);"
                ScriptManager.RegisterStartupScript(btnExcel, btnExcel.GetType(), "btnExcel", Script, True)
            Else
                Grid.JSProperties("cpMessage") = "There is no data to show!"
                Script = "btnExcel.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnExcel, btnExcel.GetType(), "btnExcel", Script, True)
            End If

        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
            Exit Sub
        End If
    End Sub

    Private Sub epplusExportExcel(Optional ByRef pErr As String = "")

        Try
            Dim tempFile As String = "ExchangeRateReport.xlsx" '" & Format(Now, "yyyyMMdd hhmmss") & ".xlsx"
            Dim NewFileName As String = Server.MapPath("~\Import\" & tempFile & "")
            Dim fi As New FileInfo(Server.MapPath("~\Export\ExchangeRateReport.xlsx"))
            If fi.Exists Then
                fi.Delete()
                fi = New FileInfo(Server.MapPath("~\Export\ExchangeRateReport.xlsx"))
            End If
            Dim exl As New ExcelPackage(fi)
            Dim ws As ExcelWorksheet
            ws = exl.Workbook.Worksheets.Add("Exchange Rate Report")
            ws.View.ShowGridLines = True

            'ws.View.FreezePanes(7, 1)

            'With ExcelRange
            '    .Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
            '    .Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
            '    '.Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
            '    '.Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
            '    '.Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
            'End With
            Dim ds As DataSet
            ds = clsExchangeRateDB.GetList(Format(dtDateFrom.Value, "yyyy-MM-dd"), Format(dtDateTo.Value, "yyyy-MM-dd"), cboPeriodID.Text)
            With ws
                .Cells(2, 2, 2, 2).Value = "MASTER UPDATE"
                .Cells(2, 2, 2, 2).Style.HorizontalAlignment = HorzAlignment.Default
                .Cells(2, 2, 2, 2).Style.VerticalAlignment = VertAlignment.Center
                .Cells(2, 2, 2, 2).Style.Font.Bold = True
                .Cells(2, 2, 2, 2).Style.Font.Italic = True
                .Cells(2, 2, 2, 2).Style.Font.Size = 14
                .Cells(2, 2, 2, 2).Style.Font.Name = "Arial"
                .Cells(2, 2, 2, 2).Style.Font.Color.SetColor(Color.Blue)

                .Cells(3, 2, 3, 2).Value = "PERIODE ADJUSTMENT " & Left(txtPeriodType.Text, 1) & " BULAN"
                .Cells(3, 2, 3, 2).Style.HorizontalAlignment = HorzAlignment.Default
                .Cells(3, 2, 3, 2).Style.VerticalAlignment = VertAlignment.Center
                .Cells(3, 2, 3, 2).Style.Font.Bold = True

                .Cells(3, 2, 3, 2).Style.Font.Size = 11
                .Cells(3, 2, 3, 2).Style.Font.Name = "Calibri"

                If ds.Tables(0).Rows.Count > 0 Then
                    .Cells(4, 2, 4, 2).Value = ds.Tables(0).Rows(0).Item("PeriodDesc").ToString()
                Else
                    .Cells(4, 2, 4, 2).Value = ""
                End If
                .Cells(4, 2, 4, 2).Style.HorizontalAlignment = HorzAlignment.Default
                .Cells(4, 2, 4, 2).Style.VerticalAlignment = VertAlignment.Center
                .Cells(4, 2, 4, 2).Style.Font.Bold = False
                .Cells(4, 2, 4, 2).Style.Font.Size = 11
                .Cells(4, 2, 4, 2).Style.Font.Name = "Calibri"


                .Cells(6, 2, 6, 2).Style.Font.Bold = False
                .Cells(6, 2, 6, 2).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                .Cells(6, 2, 6, 2).Value = " " 'n-2,3,4
                .Cells(6, 2, 6, 2).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(6, 2, 6, 2).Style.VerticalAlignment = VertAlignment.Center
                '.Cells6, 2, 6, 2).Merge = True
                .Cells(6, 2, 6, 2).Style.Font.Size = 10
                .Cells(6, 2, 6, 2).Style.Font.Name = "Calibri"

                .Cells(6, 3, 6, 3).Value = " " 'period
                .Cells(6, 3, 6, 3).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                .Cells(6, 3, 6, 3).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(6, 3, 6, 3).Style.VerticalAlignment = VertAlignment.Center
                '.Cells6, 3, 6, 3).Merge = True
                .Cells(6, 3, 6, 3).Style.Font.Size = 10
                .Cells(6, 3, 6, 3).Style.Font.Name = "Calibri"


                .Cells(6, 4, 6, 4).Value = "USD/Rp"
                .Cells(6, 4, 6, 4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                .Cells(6, 4, 6, 4).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                '.Cells6, 4, 6, 4).Merge = True
                .Cells(6, 4, 6, 4).Style.VerticalAlignment = VertAlignment.Center
                .Cells(6, 4, 6, 4).Style.Font.Size = 10
                .Cells(6, 4, 6, 4).Style.Font.Name = "Calibri"

                .Cells(6, 5, 6, 5).Value = "Yen/Rp"
                .Cells(6, 5, 6, 5).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                .Cells(6, 5, 6, 5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                .Cells(6, 5, 6, 5).Style.VerticalAlignment = VertAlignment.Center
                '.Cells64,5, 6, 5).Merge = True
                .Cells(6, 5, 6, 5).Style.Font.Size = 10
                .Cells(6, 5, 6, 5).Style.Font.Name = "Calibri"


                .Cells(6, 6, 6, 6).Value = "Baht/Rp"
                .Cells(6, 6, 6, 6).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                .Cells(6, 6, 6, 6).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                .Cells(6, 6, 6, 6).Style.VerticalAlignment = VertAlignment.Center
                '.Cells(6,65,6, 6).Merge = True
                .Cells(6, 6, 6, 6).Style.Font.Size = 10
                .Cells(6, 6, 6, 6).Style.Font.Name = "Calibri"


                .Cells(6, 7, 6, 7).Value = "Eur/Rp"
                .Cells(6, 7, 6, 7).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                .Cells(6, 7, 6, 7).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                .Cells(6, 7, 6, 7).Style.VerticalAlignment = VertAlignment.Center
                '.Cells64,76,6, 7).Merge = True
                .Cells(6, 7, 6, 7).Style.Font.Size = 10
                .Cells(6, 7, 6, 7).Style.Font.Name = "Calibri"



                Dim iRow = 7
                If ds.Tables(0).Rows.Count > 0 Then
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        'column 1

                        .Cells(i + 7, 2, i + 7, 2).Value = ds.Tables(0).Rows(i).Item("RNo").ToString
                        .Cells(i + 7, 2, i + 7, 2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                        .Cells(i + 7, 2, i + 7, 2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(i + 7, 2, i + 7, 2).Style.Font.Size = 10
                        .Cells(i + 7, 2, i + 7, 2).Style.Font.Name = "Calibri"

                        'column 2
                        .Cells(i + 7, 3, i + 7, 3).Value = ds.Tables(0).Rows(i).Item("Period").ToString
                        .Cells(i + 7, 3, i + 7, 3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left
                        .Cells(i + 7, 3, i + 7, 3).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(i + 7, 3, i + 7, 3).Style.Font.Size = 10
                        .Cells(i + 7, 3, i + 7, 3).Style.Font.Name = "Calibri"
                        If ds.Tables(0).Rows(i).Item("Period").ToString = "Average" Or ds.Tables(0).Rows(i).Item("Period").ToString = "Up From Previous" Then
                            .Cells(i + 7, 3, i + 7, 3).Style.Font.Bold = True
                        End If

                        'column 3
                        .Cells(i + 7, 4, i + 7, 4).Value = ds.Tables(0).Rows(i).Item("CurrentUSD").ToString
                        .Cells(i + 7, 4, i + 7, 4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Right
                        .Cells(i + 7, 4, i + 7, 4).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(i + 7, 4, i + 7, 4).Style.Font.Size = 10
                        .Cells(i + 7, 4, i + 7, 4).Style.Font.Name = "Calibri"
                        If ds.Tables(0).Rows(i).Item("Period").ToString = "Average" Or ds.Tables(0).Rows(i).Item("Period").ToString = "Up From Previous" Then
                            .Cells(i + 7, 4, i + 7, 4).Style.Font.Bold = True
                        End If

                        'column 4
                        .Cells(i + 7, 5, i + 7, 5).Value = ds.Tables(0).Rows(i).Item("CurrentJPN").ToString
                        .Cells(i + 7, 5, i + 7, 5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Right
                        .Cells(i + 7, 5, i + 7, 5).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(i + 7, 5, i + 7, 5).Style.Font.Size = 10
                        .Cells(i + 7, 5, i + 7, 5).Style.Font.Name = "Calibri"
                        If ds.Tables(0).Rows(i).Item("Period").ToString = "Average" Or ds.Tables(0).Rows(i).Item("Period").ToString = "Up From Previous" Then
                            .Cells(i + 7, 5, i + 7, 5).Style.Font.Bold = True
                        End If

                        'column 5
                        .Cells(i + 7, 6, i + 7, 6).Value = ds.Tables(0).Rows(i).Item("CurrentTHB").ToString
                        .Cells(i + 7, 6, i + 7, 6).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Right
                        .Cells(i + 7, 6, i + 7, 6).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(i + 7, 6, i + 7, 6).Style.Font.Size = 10
                        .Cells(i + 7, 6, i + 7, 6).Style.Font.Name = "Calibri"
                        If ds.Tables(0).Rows(i).Item("Period").ToString = "Average" Or ds.Tables(0).Rows(i).Item("Period").ToString = "Up From Previous" Then
                            .Cells(i + 7, 6, i + 7, 6).Style.Font.Bold = True
                        End If

                        'column 6
                        .Cells(i + 7, 7, i + 7, 7).Value = ds.Tables(0).Rows(i).Item("CurrentEUR").ToString
                        .Cells(i + 7, 7, i + 7, 7).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Right
                        .Cells(i + 7, 7, i + 7, 7).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(i + 7, 7, i + 7, 7).Style.Font.Size = 10
                        .Cells(i + 7, 7, i + 7, 7).Style.Font.Name = "Calibri"
                        If ds.Tables(0).Rows(i).Item("Period").ToString = "Average" Or ds.Tables(0).Rows(i).Item("Period").ToString = "Up From Previous" Then
                            .Cells(i + 7, 7, i + 7, 7).Style.Font.Bold = True
                        End If

                        If (ds.Tables(0).Rows(i).Item("Period").ToString = "Average") Then
                            .Cells(i + 7, 2, i + 7, 2).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 3, i + 7, 3).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 4, i + 7, 4).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 5, i + 7, 5).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 6, i + 7, 6).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 7, i + 7, 7).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin

                            .Cells(i + 7, 2, i + 7, 2).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 3, i + 7, 3).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 4, i + 7, 4).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 5, i + 7, 5).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 6, i + 7, 6).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 7, i + 7, 7).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                        End If

                        If (ds.Tables(0).Rows(i).Item("Period").ToString = "Up From Previous") Then
                            .Cells(i + 7, 2, i + 7, 2).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 3, i + 7, 3).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 4, i + 7, 4).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 5, i + 7, 5).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 6, i + 7, 6).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(i + 7, 7, i + 7, 7).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                        End If


                        iRow = iRow + 1
                    Next

                    iRow = iRow + 2
                    If ds.Tables(0).Rows.Count > 0 Then
                        .Cells(iRow, 2, iRow, 2).Value = ds.Tables(0).Rows(0).Item("PrevPeriodDesc").ToString()
                    Else
                        .Cells(iRow, 2, iRow, 2).Value = ""
                    End If
                    .Cells(iRow, 2, iRow, 2).Style.HorizontalAlignment = HorzAlignment.Default
                    .Cells(iRow, 2, iRow, 2).Style.VerticalAlignment = VertAlignment.Center
                    .Cells(iRow, 2, iRow, 2).Style.Font.Size = 11
                    .Cells(iRow, 2, iRow, 2).Style.Font.Name = "Calibri"
                    .Cells(iRow, 2, iRow, 7).Merge = True


                    iRow = iRow + 1
                    'header
                    .Cells(iRow, 2, iRow, 2).Value = " " 'n-2,3,4
                    .Cells(iRow, 2, iRow, 2).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                    .Cells(iRow, 2, iRow, 2).Style.HorizontalAlignment = HorzAlignment.Default
                    .Cells(iRow, 2, iRow, 2).Style.VerticalAlignment = VertAlignment.Center
                    .Cells(iRow, 2, iRow, 2).Style.Font.Size = 10
                    .Cells(iRow, 2, iRow, 2).Style.Font.Name = "Calibri"

                    .Cells(iRow, 3, iRow, 3).Value = " " 'period
                    .Cells(iRow, 3, iRow, 3).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                    .Cells(iRow, 3, iRow, 3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left
                    .Cells(iRow, 3, iRow, 3).Style.VerticalAlignment = VertAlignment.Center
                    .Cells(iRow, 3, iRow, 3).Style.Font.Size = 10
                    .Cells(iRow, 3, iRow, 3).Style.Font.Name = "Calibri"

                    .Cells(iRow, 4, iRow, 4).Value = "USD/Rp"
                    .Cells(iRow, 4, iRow, 4).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                    .Cells(iRow, 4, iRow, 4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                    .Cells(iRow, 4, iRow, 4).Style.VerticalAlignment = VertAlignment.Center
                    .Cells(iRow, 4, iRow, 4).Style.Font.Size = 10
                    .Cells(iRow, 4, iRow, 4).Style.Font.Name = "Calibri"

                    .Cells(iRow, 5, iRow, 5).Value = "Yen/Rp"
                    .Cells(iRow, 5, iRow, 5).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                    .Cells(iRow, 5, iRow, 5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                    .Cells(iRow, 5, iRow, 5).Style.VerticalAlignment = VertAlignment.Center
                    .Cells(iRow, 5, iRow, 5).Style.Font.Size = 10
                    .Cells(iRow, 5, iRow, 5).Style.Font.Name = "Calibri"

                    .Cells(iRow, 6, iRow, 6).Value = "Baht/Rp"
                    .Cells(iRow, 6, iRow, 6).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                    .Cells(iRow, 6, iRow, 6).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                    .Cells(iRow, 6, iRow, 6).Style.VerticalAlignment = VertAlignment.Center
                    .Cells(iRow, 6, iRow, 6).Style.Font.Size = 10
                    .Cells(iRow, 6, iRow, 6).Style.Font.Name = "Calibri"

                    .Cells(iRow, 7, iRow, 7).Value = "Eur/Rp"
                    .Cells(iRow, 7, iRow, 7).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                    .Cells(iRow, 7, iRow, 7).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                    .Cells(iRow, 7, iRow, 7).Style.VerticalAlignment = VertAlignment.Center
                    .Cells(iRow, 7, iRow, 7).Style.Font.Size = 10
                    .Cells(iRow, 7, iRow, 7).Style.Font.Name = "Calibri"


                    iRow = iRow + 1
                    For j = 0 To ds.Tables(0).Rows.Count - 2
                        'column 1
                        .Cells(iRow + j, 2, iRow + j, 2).Value = ds.Tables(0).Rows(j).Item("RNo").ToString
                        .Cells(iRow + j, 2, iRow + j, 2).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                        .Cells(iRow + j, 2, iRow + j, 2).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(iRow + j, 2, iRow + j, 2).Style.Font.Size = 10
                        .Cells(iRow + j, 2, iRow + j, 2).Style.Font.Name = "Calibri"


                        'column 2
                        .Cells(iRow + j, 3, iRow + j, 3).Value = ds.Tables(0).Rows(j).Item("PeriodPrev").ToString
                        .Cells(iRow + j, 3, iRow + j, 3).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Left
                        .Cells(iRow + j, 3, iRow + j, 3).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(iRow + j, 3, iRow + j, 3).Style.Font.Size = 10
                        .Cells(iRow + j, 3, iRow + j, 3).Style.Font.Name = "Calibri"
                        If RTrim(ds.Tables(0).Rows(j).Item("PeriodPrev").ToString) = "Average" Then
                            .Cells(iRow + j, 3, iRow + j, 3).Style.Font.Bold = True
                        End If

                        'column 3
                        .Cells(iRow + j, 4, iRow + j, 4).Value = ds.Tables(0).Rows(j).Item("PrevUSD").ToString
                        .Cells(iRow + j, 4, iRow + j, 4).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Right
                        .Cells(iRow + j, 4, iRow + j, 4).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(iRow + j, 4, iRow + j, 4).Style.Font.Size = 10
                        .Cells(iRow + j, 4, iRow + j, 4).Style.Font.Name = "Calibri"
                        If ds.Tables(0).Rows(j).Item("PeriodPrev").ToString = "Average" Then
                            .Cells(iRow + j, 4, iRow + j, 4).Style.Font.Bold = True
                        End If


                        'column 4
                        .Cells(iRow + j, 5, iRow + j, 5).Value = ds.Tables(0).Rows(j).Item("PrevJPN").ToString
                        .Cells(iRow + j, 5, iRow + j, 5).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Right
                        .Cells(iRow + j, 5, iRow + j, 5).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(iRow + j, 5, iRow + j, 5).Style.Font.Size = 10
                        .Cells(iRow + j, 5, iRow + j, 5).Style.Font.Name = "Calibri"
                        If ds.Tables(0).Rows(j).Item("PeriodPrev").ToString = "Average" Then
                            .Cells(iRow + j, 5, iRow + j, 5).Style.Font.Bold = True
                        End If

                        'column 5
                        .Cells(iRow + j, 6, iRow + j, 6).Value = ds.Tables(0).Rows(j).Item("PrevTHB").ToString
                        .Cells(iRow + j, 6, iRow + j, 6).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Right
                        .Cells(iRow + j, 6, iRow + j, 6).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(iRow + j, 6, iRow + j, 6).Style.Font.Size = 10
                        .Cells(iRow + j, 6, iRow + j, 6).Style.Font.Name = "Calibri"
                        If ds.Tables(0).Rows(j).Item("PeriodPrev").ToString = "Average" Then
                            .Cells(iRow + j, 6, iRow + j, 6).Style.Font.Bold = True
                        End If

                        'column 6
                        .Cells(iRow + j, 7, iRow + j, 7).Value = ds.Tables(0).Rows(j).Item("PrevEUR").ToString
                        .Cells(iRow + j, 7, iRow + j, 7).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Right
                        .Cells(iRow + j, 7, iRow + j, 7).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        .Cells(iRow + j, 7, iRow + j, 7).Style.Font.Size = 10
                        .Cells(iRow + j, 7, iRow + j, 7).Style.Font.Name = "Calibri"
                        If ds.Tables(0).Rows(j).Item("PeriodPrev").ToString = "Average" Then
                            .Cells(iRow + j, 7, iRow + j, 7).Style.Font.Bold = True
                        End If


                        If (ds.Tables(0).Rows(j).Item("PeriodPrev").ToString = "Average") Then
                            .Cells(iRow + j, 2, iRow + j, 2).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(iRow + j, 3, iRow + j, 3).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(iRow + j, 4, iRow + j, 4).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(iRow + j, 5, iRow + j, 5).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(iRow + j, 6, iRow + j, 6).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                            .Cells(iRow + j, 7, iRow + j, 7).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin

                            .Cells(iRow + j, 2, iRow + j, 2).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                            .Cells(iRow + j, 3, iRow + j, 3).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                            .Cells(iRow + j, 4, iRow + j, 4).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                            .Cells(iRow + j, 5, iRow + j, 5).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                            .Cells(iRow + j, 6, iRow + j, 6).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                            .Cells(iRow + j, 7, iRow + j, 7).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                        End If

                    Next

                End If

                FormatExcel(ws, ds)
                'InsertHeader(ws)
            End With

            exl.Save()
            'DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("Download/" & fi.Name)

            DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~\Export\" & tempFile & "")

            exl = Nothing
        Catch ex As Exception
            pErr = ex.Message
        End Try

    End Sub

  
    Private Sub InsertHeader(ByVal pExl As ExcelWorksheet)
        With pExl
            .Cells(2, 2, 2, 2).Value = "MASTER UPDATE"
            .Cells(2, 2, 2, 2).Style.HorizontalAlignment = HorzAlignment.Default
            .Cells(2, 2, 2, 2).Style.VerticalAlignment = VertAlignment.Center
            .Cells(2, 2, 2, 2).Style.Font.Bold = True
            .Cells(2, 2, 2, 2).Style.Font.Italic = True
            .Cells(2, 2, 2, 2).Style.Font.Size = 14
            .Cells(2, 2, 2, 2).Style.Font.Name = "Arial"
            .Cells(2, 2, 2, 2).Style.Font.Color.SetColor(Color.Blue)

            .Cells(3, 2, 3, 2).Value = "PERIODE ADJUSTMENT " & Left(txtPeriodType.Text, 1) & " BULAN"
            .Cells(3, 2, 3, 2).Style.HorizontalAlignment = HorzAlignment.Default
            .Cells(3, 2, 3, 2).Style.VerticalAlignment = VertAlignment.Center
            .Cells(3, 2, 3, 2).Style.Font.Bold = True

            .Cells(3, 2, 3, 2).Style.Font.Size = 11
            .Cells(3, 2, 3, 2).Style.Font.Name = "Calibri"

            .Cells(4, 2, 4, 2).Value = txtPeriodDescription.Text   '"PO DATE 18 OKTOBER  2018 - 17 APRIL 2019"
            .Cells(4, 2, 4, 2).Style.HorizontalAlignment = HorzAlignment.Default
            .Cells(4, 2, 4, 2).Style.VerticalAlignment = VertAlignment.Center
            .Cells(4, 2, 4, 2).Style.Font.Bold = False
            .Cells(4, 2, 4, 2).Style.Font.Size = 11
            .Cells(4, 2, 4, 2).Style.Font.Name = "Calibri"

        End With
    End Sub

    Private Sub FormatExcel(ByVal pExl As ExcelWorksheet, ByVal ds As DataSet)
        With pExl
            .Column(1).Width = 5
            .Column(2).Width = 5
            .Column(3).Width = 15
            .Column(4).Width = 12
            .Column(5).Width = 12
            .Column(6).Width = 12
            .Column(7).Width = 12


            'If Left(txtPeriodType.Text, 1) = "6" Then

            '    Dim rgAll As ExcelRange = .Cells(6, 2, ds.Tables(0).Rows.Count + 6, 7)
            '    DrawAllBorders(rgAll)

            '    Dim rgAll2 As ExcelRange = .Cells(18, 2, ds.Tables(0).Rows.Count + 18, 7)
            '    DrawAllBorders(rgAll2)
            'Else
            '    Dim rgAll As ExcelRange = .Cells(6, 2, ds.Tables(0).Rows.Count + 6, 7)
            '    DrawAllBorders(rgAll)

            '    Dim rgAll2 As ExcelRange = .Cells(18, 2, ds.Tables(0).Rows.Count + (18 - 3), 7)
            '    DrawAllBorders(rgAll2)

            'End If
            
           
        End With
    End Sub

    Private Sub DrawAllBorders(ByVal Rg As ExcelRange)
        With Rg
            .Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
            .Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
            '.Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
            '.Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
            '.Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
        End With
    End Sub


    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Control Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A171")
        Master.SiteTitle = sGlobal.menuName
        If Not Page.IsPostBack Then
            pUser = Session("user")
            show_error(MsgTypeEnum.Info, "", 0)
            AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A171")

            Dim dfrom As Date
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dtDateFrom.Value = dfrom
            dtDateTo.Value = Now

            If AuthUpdate = False Then
                btnExcel.Enabled = False
            Else
                btnExcel.Enabled = True
            End If


            Dim Script As String
            Script = "btnExcel.SetEnabled(false);"
            ScriptManager.RegisterStartupScript(btnExcel, btnExcel.GetType(), "btnExcel", Script, True)
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_LoadGrid()
        End If
    End Sub

    Private Sub Grid_CustomCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Select Case pFunction
            Case "load"
                up_LoadGrid()
                If Grid.VisibleRowCount = 0 Then
                    Grid.JSProperties("cp_disabled") = "Y"
                    show_error(MsgTypeEnum.Warning, "The data you want to search does not exist!", 1)
                Else
                    Grid.JSProperties("cp_disabled") = "N"
                    show_error(MsgTypeEnum.Info, "", 0)
                End If
            Case "excel"
                If Grid.VisibleRowCount > 0 Then
                    Call epplusExportExcel()
                End If
        End Select
    End Sub

    'Private Sub Grid_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles Grid.CustomColumnDisplayText
    '    Select Case e.Column.FieldName
    '        Case "CurrentUSD"
    '            If IsDBNull(e.Value) Then
    '                e.DisplayText = ""
    '            ElseIf e.Value = 0 Then
    '                e.DisplayText = ""
    '            Else
    '                If e.GetFieldValue("Period") = "Up From Previous" Then
    '                    e.DisplayText = FormatPercent(e.Value)

    '                Else
    '                    e.DisplayText = FormatNumber(e.Value, 2, TriState.True)
    '                End If

    '            End If
    '        Case "CurrentJPN"
    '            If IsDBNull(e.Value) Then
    '                e.DisplayText = ""
    '            ElseIf e.Value = 0 Then
    '                e.DisplayText = ""
    '            Else
    '                If e.GetFieldValue("Period") = "Up From Previous" Then
    '                    e.DisplayText = FormatPercent(e.Value)
    '                Else
    '                    e.DisplayText = FormatNumber(e.Value, 2, TriState.True)
    '                End If

    '            End If
    '    End Select
    'End Sub


    Private Sub cbPeriod_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbPeriod.Callback
        Dim perr As String = ""
        Dim ds As New DataSet
        ds = clsExchangeRateDB.GetPeriodAdjustment(cboPeriodID.Text, perr)
        If ds.Tables(0).Rows.Count > 0 Then
            cbPeriod.JSProperties("cpPeriodType") = ds.Tables(0).Rows(0)("PeriodTypeDescs").ToString
            cbPeriod.JSProperties("cpPeriodDescs") = ds.Tables(0).Rows(0)("Period_Description").ToString
            cbPeriod.JSProperties("cpDateFrom") = ds.Tables(0).Rows(0)("Period_From")
            cbPeriod.JSProperties("cpDateTo") = ds.Tables(0).Rows(0)("Period_To")
        End If


    End Sub

    'Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
    '    up_LoadExcel()
    'End Sub
#End Region
End Class
﻿Imports DevExpress.XtraPrinting
Imports System.IO

Public Class MaterialPart
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A040")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            'up_FillCombo(cboMaterialCode, statusAdmin, pUser)\\
            cboPeriodFrom.Value = CDate(Now.ToShortDateString)
            cboPeriodTo.Value = CDate(Now.ToShortDateString)
            cboType.SelectedIndex = 0
            cboCategoryMaterial.SelectedIndex = 0
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad()
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters
        Dim pFunctionFirst As String = Split(e.Parameters, "|")(0)

        If pFunction = "refresh" Then
            up_GridLoad()
            ds = ClsMaterialPartDB.getGrid(cboCategoryMaterial.Value, "", pUser, cboType.Value, cboPeriodFrom.Value, cboPeriodTo.Value)

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        ElseIf pFunctionFirst.Trim = "delete" Then
            Dim pErr As String = ""

            ClsMaterialPartDB.DeleteMaterialCode(Split(e.Parameters, "|")(1), Split(e.Parameters, "|")(2), pUser, pErr)

            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
                up_GridLoad()
            End If
        Else

            up_GridLoad()
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_GridLoad()
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        Dim PeriodFrom As String = Format(cboPeriodFrom.Value, "yyyy-MM-dd")
        Dim PeriodTo As String = Format(cboPeriodTo.Value, "yyyy-MM-dd")

        ds = ClsMaterialPartDB.getGrid(cboCategoryMaterial.Value, "", pUser, cboType.Value, PeriodFrom, PeriodTo, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()

        End If
    End Sub
#End Region

#Region "Event Control"
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("AddMaterialPart.aspx")
    End Sub
#End Region

    Private Sub up_Excel()
        up_GridLoad()

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=MaterialPart_" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
    End Sub

    Private Sub btnChart_Click(sender As Object, e As System.EventArgs) Handles btnChart.Click
        Response.Redirect("MaterialPartChart.aspx")
    End Sub
End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ShowPicture.aspx.vb" Inherits="IAMI_EPIC2.ShowPicture" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Show Picture</title>
    <style type="text/css">
        .templateTable
        {
            border-collapse: collapse;
            width: 100%;
        }
        .templateTable td
        {
            border: solid 1px #C2D4DA;
            padding: 6px;
        }
        .templateTable td.value
        {
            font-weight: bold;
        }
        .imageCell
        {
            width: 160px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <dx:ASPxGridView ID="grid" runat="server" 
        KeyFieldName="MaterialNo" Width="50%" EnableRowsCache="false"  >
        <Columns>
         
            <dx:GridViewDataColumn FieldName="Picture1" Caption="" width="200px"/>
            <dx:GridViewDataColumn FieldName="Picture2" Caption="" width="200px"/>
            <dx:GridViewDataColumn FieldName="Picture3" Caption="" width="200px"/>
        </Columns>
        <SettingsPager PageSize="2" />
        <Settings ShowColumnHeaders="false" />
        <Templates>
            <DataRow>
                <div style="padding: 2px">
                    <table class="templateTable" width="100%">
                        <tr>
                            <td class="imageCell" rowspan="4" align="center">
                                
                                <dx:ASPxBinaryImage ID="Image1"  runat="server" AlternateText="No Image"  Width="200px" Height="200px" Value='<%# Eval("Picture1") %>' EmptyImage-Url="~/img/Default.png"></dx:ASPxBinaryImage>
                            </td>
                           
                        </tr>
                        <tr>
                            <td class="imageCell" rowspan="4" align="center">
                                <dx:ASPxBinaryImage ID="ASPxBinaryImage2" runat="server" AlternateText="No Image..." Width="200px" Height="200px" Value='<%# Eval("Picture2") %>' EmptyImage-Url="~/img/Default.png"/>
                            </td>
                           
                        </tr>
                        <tr>
                            <td class="imageCell" rowspan="4" align="center">
                                <dx:ASPxBinaryImage ID="ASPxBinaryImage3" runat="server" AlternateText="No Image..." Width="200px" Height="200px" Value='<%# Eval("Picture3") %>' EmptyImage-Url="~/img/Default.png"/>
                            </td>
                           
                        </tr>
                        
                    </table>
                </div>
            </DataRow>
        </Templates>
    </dx:ASPxGridView>
    </div>
    </form>
</body>
</html>

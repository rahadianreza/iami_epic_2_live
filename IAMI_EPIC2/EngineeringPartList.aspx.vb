﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class EngineeringPartList
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            up_GridLoad("")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("I020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "I020")
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal pProjectID As String)
        Dim ErrMsg As String = ""
        Dim Ses As New List(Of ClsEngineeringPartList)
        Ses = ClsEngineeringPartListDB.getlist(pProjectID, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Control Event"

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        up_GridLoad(cboProject.Value)
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pProjectID As String = Split(e.Parameters, "|")(1)
        Dim pdata As New List(Of ClsUploadEngineeringPartList)
        Dim pnext As Integer
        Select Case pFunction
            Case "load"
                up_GridLoad(pProjectID)
                pdata = ClsEngineeringPartListDB.getCountVariant(pProjectID, "")
                'variant visible
                For Each xdata In pdata
                    Grid.Columns("Variant" & xdata.No).Visible = True
                    Grid.Columns("Variant" & xdata.No).Caption = xdata.pVariant
                    Grid.Columns("Variant_Supply_Cntry_Code" & xdata.No).Visible = True
                    Grid.Columns("Variant_KD" & xdata.No).Visible = True
                    Grid.Columns("Variant_PID" & xdata.No).Visible = True
                    pnext = pnext + 1
                Next

                If pnext = 0 Then
                    pnext = 1
                ElseIf pnext <> 36 Then
                    pnext = pnext + 1
                End If
                For x = pnext To 36
                    Grid.Columns("Variant" & x).Visible = False
                Next
        End Select
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim pdata As New List(Of ClsUploadEngineeringPartList)
        up_GridLoad(cboProject.Value)
        pdata = ClsEngineeringPartListDB.getCountVariant(cboProject.Value, "")
        'rename header name variant 
        For Each xdata In pdata
            Grid.Columns("Variant" & xdata.No).Caption = xdata.pVariant
        Next

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=EngineeringPartList" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()

    End Sub
    Private Sub cbupload_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbupload.Callback
        DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/UploadEngineeringPartList.aspx")
    End Sub

#End Region


 

End Class
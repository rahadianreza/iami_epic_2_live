﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks


Public Class CEApproval
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cpMessage") = ErrMsg
        Grid.JSProperties("cpType") = msgType
        Grid.JSProperties("cpVal") = pVal
    End Sub

    Private Sub up_FillComboCENumber(pDate1 As Date, pDate2 As Date)
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim dtfrom As String = Format(pDate1, "yyyy-MM-dd")
        Dim dtto As String = Format(pDate2, "yyyy-MM-dd")

        ds = clsCostEstimationDB.GetComboCEApproval(dtfrom, dtto, pUser, pErr)
        If pErr = "" Then

            cboCENumber.DataSource = ds
            cboCENumber.DataBind()
        End If
    End Sub

    Private Sub up_GridLoad(pDateFrom As String, pDateTo As String, pCENo As String, pStatus As String)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        Dim clsCE As New clsCostEstimation

        'If pCENo = "ALL" Then
        '    pCENo = ""
        'End If
        clsCE.CENumber = pCENo
        clsCE.CEDateFrom = pDateFrom
        clsCE.CEDateTo = pDateTo
        clsCE.StatusApproval = pStatus

        ds = clsCostEstimationDB.GetListApproval(clsCE, pUser, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_Excel()
        Try
            up_GridLoad(gs_DateFrom, gs_DateTo, gs_CENumber, gs_StatusApproval)

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=CEApproval" & Format(Date.Now, "ddMMyyyyHHmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            show_error(MsgTypeEnum.Warning, ex.Message, 2)
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("D020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "D020 ")

        If (Not Page.IsPostBack) Then
            Dim dfrom As Date
            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dtDateFrom.Value = dfrom
            dtDateTo.Value = Now
            cboStatus.SelectedIndex = 1
        End If
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        up_Excel()
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        up_GridLoad(gs_DateFrom, gs_DateTo, gs_CENumber, gs_StatusApproval)
        'pHeader = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pDateFrom As Date = Split(e.Parameters, "|")(1)
        Dim pDateTo As Date = Split(e.Parameters, "|")(2)
        Dim pCENo As String = Split(e.Parameters, "|")(3)
        Dim pStatus As String = Split(e.Parameters, "|")(4)

        Dim vDateFrom As String = Format(pDateFrom, "yyyy-MM-dd")
        Dim vDateTo As String = Format(pDateTo, "yyyy-MM-dd")

        gs_CENumber = pCENo

        gs_DateFrom = vDateFrom
        gs_DateTo = vDateTo
        gs_StatusApproval = pStatus

        If pFunction = "gridload" Then
            up_GridLoad(vDateFrom, vDateTo, pCENo, pStatus)

        End If
    End Sub

    Private Sub cboCENumber_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCENumber.Callback
        Dim pDateFrom As Date = Split(e.Parameter, "|")(0)
        Dim pDateTo As Date = Split(e.Parameter, "|")(1)

        up_FillComboCENumber(pDateFrom, pDateTo)

        If cboCENumber.Items.Count > 0 Then
            cboCENumber.SelectedIndex = 0
        End If
    End Sub
End Class
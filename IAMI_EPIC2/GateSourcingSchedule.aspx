﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="GateSourcingSchedule.aspx.vb" Inherits="IAMI_EPIC2.GateSourcingSchedule" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };

         function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtItemCode.Focus();
            cboGroupItem.SetText('');
            cboCategory.SetText('');
            cboPRType.SetText('');
            txtSAPNumber.SetText('');
        }
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/ItemList.aspx";

            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/ItemList.aspx';
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                if (pathArray[1] == "AddItemMaster.aspx") {
                    window.location.href = window.location.origin + '/ItemList.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtSAPNumber.SetText('');
            txtItemCode.Focus();

        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    };

         function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "Part_No" || currentColumnName == "Commodity" || currentColumnName == "Upc") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        };  

        function SaveData(s, e) {
            Grid.UpdateEdit()
        }

        function disableenablebuttonsubmit(_val) {
        alert('adsds');
            if (_val = '1') {
                btnSave.SetEnabled(false)
            } else {
                btnSave.SetEnabled(true)
             }
        }

        function OnUpdateCheckedChanged(s, e) {
        if (s.GetValue() == -1) s.SetValue(1);
        Grid.SetFocusedRowIndex(-1);
        for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
            if (Grid.batchEditApi.GetCellValue(i, "Status_Drawing", false) != s.GetValue()) {
                Grid.batchEditApi.SetCellValue(i, "Status_Drawing", s.GetValue());
            }                
        }
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding:0 0 10px 0">
    <dx:ASPxButton ID="btnBack" runat="server" Text="Back to List" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnBack" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>&nbsp;

</div>
 <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
        EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
        Font-Size="9pt" KeyFieldName="Code_Activity">
        <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="true" ShowDeleteButton="true">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="Activity" VisibleIndex="1" FieldName="Code_Activity"  Width="150px" Visible="false">
                <PropertiesTextEdit Style-HorizontalAlign="Center">
                    <Style HorizontalAlign="Center"></Style>
                </PropertiesTextEdit>
                <EditFormSettings Visible="false" />
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Activity" VisibleIndex="1" FieldName="Activity"  Width="150px">
                <PropertiesTextEdit Style-HorizontalAlign="Center">
                    <Style HorizontalAlign="Center"></Style>
                </PropertiesTextEdit>
                <EditFormSettings Visible="false" />
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
              <dx:GridViewDataDateColumn Caption="Due Date Plan" FieldName="Due_Date_Plan"
                VisibleIndex="2" Width="150px">
                <PropertiesDateEdit Width="100px" DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd/MMM/yyyy">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                </PropertiesDateEdit>
                <Settings AllowAutoFilter="False" />
                   <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataDateColumn Caption="Date Actual" FieldName="Date_Actual"
                VisibleIndex="3" Width="150px">
                <PropertiesDateEdit Width="100px" DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd/MMM/yyyy">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                </PropertiesDateEdit>
                <Settings AllowAutoFilter="False" />
                   <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataDateColumn>
            <dx:GridViewDataTextColumn Caption="Comment" FieldName="Comment" VisibleIndex="4"
                Width="200px">
                <PropertiesTextEdit Width="200px">
                </PropertiesTextEdit>
                <EditFormSettings Visible="default" />
                <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
        <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
        </SettingsPager>
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
        <SettingsPopup>
            <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                Width="320" />
        </SettingsPopup>
        <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
            EditFormColumnCaption-Paddings-PaddingRight="10px">
            <Header>
                <Paddings Padding="2px"></Paddings>
            </Header>
            <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                </Paddings>
            </EditFormColumnCaption>
            <CommandColumnItem ForeColor="Orange">
            </CommandColumnItem>
        </Styles>
        <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
        <Templates>
            <EditForm>
                <div style="padding: 15px 15px 15px 15px">
                    <dx:ContentControl ID="ContentControl1" runat="server">
                        <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                    </dx:ContentControl>
                </div>
                <div style="text-align: left; padding: 5px 5px 5px 15px">
                    <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                    <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                </div>
            </EditForm>
        </Templates>
    </dx:ASPxGridView>
</asp:Content>

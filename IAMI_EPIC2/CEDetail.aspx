﻿<%@ Page Title="" Language="vb" ValidateRequest ="false" AutoEventWireup="true" MasterPageFile="~/Site.Master"
    CodeBehind="CEDetail.aspx.vb" Inherits="IAMI_EPIC2.CEDetail" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %><%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function GetMessage(s, e) {


           //alert(s.cpMessage);
            if (s.cpMessage == 'Draft data saved successfull') {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                dtCEDate.SetEnabled(false);
                if (txtFlag.GetText() == '0') {
                   
                    txtRev.SetText(s.cpRevision);
                    txtCENumber.SetText(s.cpCENo);
                    txtParameter.SetText(s.cpCENo + '|' + s.cpRevision + '|' + cboRFQSetNumber.GetText());

                    setTimeout(function () {
                        window.location.href = 'CEDetail.aspx?ID=' + s.cpCENo + '|' + s.cpRevision + '|' + cboRFQSetNumber.GetText();
                    }, 1000);
                    //alert(s.cpCENo + '' + s.cpRevision + cboRFQSetNumber.GetText());
                    // txtParameter.SetText(s.cpCENo + '' + s.cpRevision + cboRFQSetNumber.GetText());
                }
                document.getElementById("<%= uploaderFile.ClientID %>").disabled = false;
                btnSubmit.SetEnabled(true);
                if (chkSkip.GetChecked() == false) {
                    LinkFile.SetText("No File");
                }
            }
            else if (s.cpMessage == "Data saved successfull") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                if (chkSkip.GetChecked() == false) {
                    LinkFile.SetText("No File");
                }

                //millisecondsToWait = 1000;
                //setTimeout(function () {
                //    var pathArray = window.location.pathname.split('/');
                //    var url = window.location.origin + '/' + pathArray[1] + '/CEList.aspx';
                //    if (pathArray[1] == "CEDetail.aspx") {
                //        window.location.href = window.location.origin + '/CEList.aspx';
                //    }
                //    else {
                //        window.location.href = url;
                //    }
                //}, millisecondsToWait);
            }
            else if (s.cpMessage == "Upload File Succesfull") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                LinkFile.SetText(s.cpFileName);
                //var url = window.location.origin + '/Files/' + s.cpFileName;
                //LinkFile.SetNavigateUrl(url);

            }
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }

            var skip = s.cpSkip;
            var status = s.cpStatus;

            // alert(status);
            if (status == "") {
                btnSubmit.SetEnabled(false);
                btnAddFile.SetEnabled(false);
                btnPrint.SetEnabled(false);
                //cbIABudget.SetEnabled(false);
                if (chkSkip.GetChecked() == (true)) {
                    document.getElementById("<%= uploaderFile.ClientID %>").disabled = false;
                    btnAddFile.SetEnabled(true);
                    txtReason.SetEnabled(true);
                } else {
                    document.getElementById("<%= uploaderFile.ClientID %>").disabled = true;
                    btnAddFile.SetEnabled(false);
                    txtReason.SetEnabled(false);
                }
            }
            if (status == "0") {
                btnSubmit.SetEnabled(true);
                //cbIABudget.SetEnabled(false);
                cboPRNo.SetEnabled(false);
                if (chkSkip.GetChecked() == (true)) {
                    document.getElementById("<%= uploaderFile.ClientID %>").disabled = false;
                    btnAddFile.SetEnabled(true);
                    txtReason.SetEnabled(true);
                } else {
                    document.getElementById("<%= uploaderFile.ClientID %>").disabled = true;
                    btnAddFile.SetEnabled(false);
                    txtReason.SetEnabled(false);
                }

                txtNotes.SetEnabled(true);
                //txtReason.SetEnabled(true);
                chkSkip.SetEnabled(true);
                btnPrint.SetEnabled(true);
            }
            else if (status == "1") {
                btnDraft.SetEnabled(false);
                btnSubmit.SetEnabled(false);
                btnAddFile.SetEnabled(false);
                //cbIABudget.SetEnabled(false);
                cboRFQSetNumber.SetEnabled(false);
                cboPRNo.SetEnabled(false);
                document.getElementById("<%= uploaderFile.ClientID %>").disabled = true;
                chkSkip.SetEnabled(false);
                txtNotes.SetEnabled(false);
                txtReason.SetEnabled(false);
                Grid.SetEnabled(false);
            }
            else if (status == "2") {
                btnDraft.SetEnabled(false);
                btnSubmit.SetEnabled(false);
                btnAddFile.SetEnabled(false);
                //cbIABudget.SetEnabled(false);
                cboRFQSetNumber.SetEnabled(false);
                cboPRNo.SetEnabled(false);
                document.getElementById("<%= uploaderFile.ClientID %>").disabled = true;
                chkSkip.SetEnabled(false);
                txtNotes.SetEnabled(false);
                txtReason.SetEnabled(false);
                Grid.SetEnabled(false);
            }
            else if (status == "3") {
                btnDraft.SetEnabled(false);
                btnSubmit.SetEnabled(false);
                btnAddFile.SetEnabled(false);
                //cbIABudget.SetEnabled(false);
                cboRFQSetNumber.SetEnabled(false);
                cboPRNo.SetEnabled(false);
                document.getElementById("<%= uploaderFile.ClientID %>").disabled = true;
                chkSkip.SetEnabled(false);
                txtNotes.SetEnabled(false);
                txtReason.SetEnabled(false);
                Grid.SetEnabled(false);
            }

            //if (skip == "" || skip == "0") {
            //    txtreason.setenabled(false);
            //}
            //else if (status == "1") {
            //    txtreason.setenabled(true);
            //}
        }

        function OnBatchEditStartEditing(s, e) {

            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName != "PriceEst" && currentColumnName != "Remarks") {
                e.cancel = true;

            }
            currentEditableVisibleIndex = e.visibleIndex;
        }

        function GridLoad() {
            //cboSupplier.PerformCallback(cboRFQSetNumber.GetText());

            Grid.PerformCallback('gridload|xx|0|' + cboRFQSetNumber.GetText());
        }

        function UpdateTotalAmount(s, e) {
            startIndex = 0;
            var qty = 0;
            var price = 0;
            var amount = 0;

            qty = parseFloat(qty);
            price = parseFloat(price);
            amount = parseFloat(amount);

            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                qty = parseFloat(Grid.batchEditApi.GetCellValue(i, "Qty"));
                price = parseFloat(Grid.batchEditApi.GetCellValue(i, "PriceEst"));
                amount = qty * price;

                Grid.batchEditApi.SetCellValue(i, "Amount", amount);
            }

            //alert(qty);
            //alert(price);
        }

        function CheckBoxSkip(s, e) {
            
                
                if (chkSkip.GetChecked() == (true)) {
                    if (txtCENumber.GetText() != "--NEW--") {
                        //alert(txtCENumber.GetText());
                        btnAddFile.SetEnabled(true);
                        document.getElementById("<%= uploaderFile.ClientID %>").disabled = false;
                        //uploaderFile.SetEnabled(true);
                    } else {
                        
                        btnAddFile.SetEnabled(false);
                        document.getElementById("<%= uploaderFile.ClientID %>").disabled = true;
                    }
                    txtReason.SetEnabled(true);
                }
                if (chkSkip.GetChecked() == (false)) {
                    txtReason.SetEnabled(false);
                    txtReason.SetText('');
                    btnAddFile.SetEnabled(false);
                    document.getElementById("<%= uploaderFile.ClientID %>").disabled = true;
                    //uploaderFile.SetEnabled(false);
                }
            
        }

        function SubmitProcess(s, e) {
            //  alert(txtParameter.GetText());
            startIndex = 0;
            var price = 0;
            price = parseFloat(price);

            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                price = parseFloat(Grid.batchEditApi.GetCellValue(i, "PriceEst"));

                if (price == 0) {
                    price = 0;
                }
            }

           
            if (price > 0) {
                
            }
            else {
                toastr.warning('Please Input Price Estimate !', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        
            var msg = confirm('Are you sure want to submit this data ?');
            if (msg == false) {
                e.processOnServer = false;
                return;
            }

            //        cbSubmit.PerformCallback(txtCENumber.GetText());
            //   cbSubmit.PerformCallback();
        }

        function DraftProcess() {
           // alert(txtFlag.GetText());
            if (txtFlag.GetText() == '0') {
                if (cboPRNo.GetText() == '') {
                    toastr.warning('Please Select PR No!', 'Warning');
                    cboPRNo.Focus();
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                if (cboRFQSetNumber.GetText() == '') {
                    toastr.warning('Please Select RFQ Set No!', 'Warning');
                    cboRFQSetNumber.Focus();
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                //28-02-2019
                if (chkSkip.GetChecked() == true && LinkFile.GetText() == 'No File' && txtCENumber.GetText() != "--NEW--") {
                   // alert("ok");
                    toastr.warning('Please Upload Document', 'Warning');
                    cboRFQSetNumber.Focus();
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                if (chkSkip.GetChecked() == true && txtReason.GetText() == '') {
                    toastr.warning('Please Input Note or Reason Skip Budget!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }

            else {
                if (chkSkip.GetChecked() == true && LinkFile.GetText() == 'No File' && txtCENumber.GetText() != "--NEW--") {
                   
                    toastr.warning('Please click Add File first', 'Warning');
                    cboRFQSetNumber.Focus();
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }


            //  alert(txtParameter.GetText());
            startIndex = 0;
            var price = 0;
            price = parseFloat(price);

            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                price = parseFloat(Grid.batchEditApi.GetCellValue(i, "PriceEst"));

                if (price == 0) {
                    price = 0;
                }
            }

           
            if (price > 0) {
               
                //var msg = confirm('Are you sure want to draft this data ?');
                //if (msg == false) {
                //    e.processOnServer = false;
                //    return;
                //}
                //else {

                Grid.UpdateEdit();

                millisecondsToWait = 1000;
                setTimeout(function () {
                    cbDraft.PerformCallback();
                }, millisecondsToWait);

                setTimeout(function () {
                    Grid.PerformCallback('draft|' + txtParameter.GetText());
                }, millisecondsToWait);

              
                //window.setTimeout(cbDraft.PerformCallback(), 500);

                //window.setTimeout(Grid.PerformCallback('draft|' + txtParameter.GetText()), 500);
                //cbGrid.PerformCallback('draft|' + txtParameter.GetText());
               // Grid.CancelEdit();
                //}

            }
            else {
                toastr.warning('Please Input Price Estimate !', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

        function OnFileUploadStart(s, e) {
            btnAddFile.SetEnabled(false);
        }

        function OnTextChanged(s, e) {
            btnAddFile.SetEnabled(s.GetText() != "");
        }

        function OnFileUploadComplete(s, e) {
            btnAddFile.SetEnabled(s.GetText() != "");

            if (s.cpType == "0") {
                //INFO
                toastr.info(s.cpMessage, 'Information');

            } else if (s.cpType == "1") {
                //SUCCESS
                toastr.success(s.cpMessage, 'Success');

            } else if (s.cpType == "2") {
                //WARNING
                toastr.warning(s.cpMessage, 'Warning');

            } else if (s.cpType == "3") {
                //ERROR
                toastr.error(s.cpMessage, 'Error');

            }
            //cbIABudget.SetEnabled(false);							 

            window.setTimeout(function () {
                delete s.cpType;
            }, 10);
        }

        function AddFile() {
            var doc = document.getElementById("<%= uploaderFile.ClientID %>").files.length;

            if (doc == 0) {
                toastr.warning("There is no uploaded data", 'Warning');
                e.processOnServer = false;
                return;
            }

            //        var f = document.getElementById("<%= uploaderFile.ClientID %>").value;
            //        var fileName = f.substring(f.lastIndexOf("\\") + 1);

            //        millisecondsToWait = 1000;
            //        setTimeout(function () {
            //            LinkFile.SetText(fileName);
            //            LinkFile.SetNavigateUrl(f);
            //        }, millisecondsToWait);

        }

        function LoadCompleted(s, e) {
            IABudgetNo.Set("hfLink", s.cpLink);
            IALinkFile.SetText(s.cpCEBudget);
            return false;     
        }

    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
    </style>
    <style type="text/css">
        .tr-all-height
        {
            height: 30px;
        }
        
        .td-col-left
        {
            width: 100px;
            padding: 0px 0px 0px 10px;
        }
        
        .td-col-left2
        {
            width: 140px;
        }
        
        
        .td-col-mid
        {
            width: 10px;
        }
        .td-col-right
        {
            width: 200px;
        }
        .td-col-free
        {
            width: 20px;
        }
        
        .div-pad-fil
        {
            padding: 5px 5px 5px 5px;
        }
        
        .td-margin
        {
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr style="height: 10px">
                    <td colspan="5"> </td>
                    
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="CE Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    <dx1:ASPxTextBox ID="txtCENumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                        Width="170px" ClientInstanceName="txtCENumber" MaxLength="15" Height="25px" ReadOnly="True"
                                        Font-Bold="True" BackColor="Silver">
                                    </dx1:ASPxTextBox>
                                </td>
                                <td class="td-col-mid">
                                </td>
                                <td class="td-col-right">
                                    <dx1:ASPxTextBox ID="txtRev" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                                        Width="45px" ClientInstanceName="txtRev" MaxLength="20" Height="25px" ReadOnly="True"
                                        Font-Bold="True" ForeColor="Black" HorizontalAlign="Center" BackColor="Silver">
                                    </dx1:ASPxTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td></td>
                </tr>
                <tr class="tr-all-height">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                        <dx1:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="CE Date">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                        &nbsp;
                    </td>
                    <td class="td-col-right" colspan="2">
                        <dx:ASPxDateEdit ID="dtCEDate" runat="server" Theme="Office2010Black" Width="100px"
                            AutoPostBack="false" ClientInstanceName="dtCEDate" EditFormatString="dd-MMM-yyyy"
                            DisplayFormatString="dd-MMM-yyyy" Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                            <CalendarProperties>
                                <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                </WeekNumberStyle>
                                <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                </ButtonStyle>
                            </CalendarProperties>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="PR No">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                        &nbsp;
                    </td>
                    <td class="td-col-right" colspan="2">
                        <dx1:ASPxComboBox ID="cboPRNo" runat="server" ClientInstanceName="cboPRNo" Width="220px"
                            Font-Names="Segoe UI" TextField="Code" ValueField="Code" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	cboRFQSetNumber.PerformCallback(cboPRNo.GetText());
}" EndCallback="LoadCompleted" />
                            <Columns>
                                <dx:ListBoxColumn Caption="PR Number" FieldName="Code" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td></td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="RFQ Set Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right" colspan="2">
                        <dx1:ASPxComboBox ID="cboRFQSetNumber" runat="server" ClientInstanceName="cboRFQSetNumber"
                            Width="220px" Font-Names="Segoe UI" TextField="Code" ValueField="Code" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="GridLoad" />
                            <Columns>
                                <dx:ListBoxColumn Caption="RFQ Number" FieldName="Code" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td></td>
                </tr>
                <tr class="hidden-div">
                    <td class="td-col-left">
                        <dx1:ASPxTextBox ID="txtFlag" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="50px" ClientInstanceName="txtFlag" MaxLength="20" Height="30px" ReadOnly="True"
                            Font-Bold="True" ForeColor="Black" HorizontalAlign="Center">
                        </dx1:ASPxTextBox>
                    </td>
                    <td class="td-col-mid">
                        &nbsp;
                    </td>
                    <td class="td-col-right" colspan="2">
                        <dx1:ASPxTextBox ID="txtParameter" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="200px" ClientInstanceName="txtParameter" MaxLength="20" Height="30px"
                            ReadOnly="True" Font-Bold="True" ForeColor="Black" HorizontalAlign="Center">
                        </dx1:ASPxTextBox>
                    </td>
                    <td></td>
                </tr>
                <tr style="height: 10px">
                    <td colspan="5"> </td>
                    
                </tr>
            </table>
        </div>
        <div>
            <dx:ASPxCallback ID="cbDraft" runat="server" ClientInstanceName="cbDraft">
                <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbSubmit" runat="server" ClientInstanceName="cbSubmit">
                <ClientSideEvents Init="GetMessage" />
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                <ClientSideEvents Init="GetMessage" />
            </dx:ASPxCallback>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
                Font-Size="9pt" KeyFieldName="Item_Code">
                <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing"   />
                <Columns>
                    <dx:GridViewDataTextColumn Name="Remarks" Caption="Condition Of Price" VisibleIndex="13" FieldName="Remarks"
                        Width="200px">
                        <PropertiesTextEdit MaxLength="100" ClientInstanceName="Remarks">
                            <Style HorizontalAlign="Left"></Style>
                        </PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Name="ItemCode" Caption="Material No" FieldName="Item_Code"
                        VisibleIndex="0" Width="120px" FixedStyle="Left">
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataTextColumn Name="Description" Caption="Description" VisibleIndex="1"
                        Width="200px" FieldName="Description">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Name="Specification" Caption="Specification" VisibleIndex="2"
                        Width="250px" FieldName="Specification">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Name="UOM" Caption="UoM" VisibleIndex="4" FieldName="UOM"
                        Width="70px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Name="Currency" Caption="Currency" VisibleIndex="5" FieldName="CurrencyCd"
                        Width="70px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="6" Width="120px" FieldName="Supplier1">
                        <PropertiesTextEdit DisplayFormatString="#,###">
                        </PropertiesTextEdit>
                        <HeaderStyle Wrap="True" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="7" Width="120px" FieldName="Supplier2">
                        <PropertiesTextEdit DisplayFormatString="#,###">
                        </PropertiesTextEdit>
                        <HeaderStyle Wrap="True" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="8" Width="120px" FieldName="Supplier3">
                        <PropertiesTextEdit DisplayFormatString="#,###">
                        </PropertiesTextEdit>
                        <HeaderStyle Wrap="True" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="9" Width="120px" FieldName="Supplier4">
                        <PropertiesTextEdit DisplayFormatString="#,###">
                        </PropertiesTextEdit>
                        <HeaderStyle Wrap="True" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn VisibleIndex="10" Width="120px" FieldName="Supplier5">
                        <PropertiesTextEdit DisplayFormatString="#,###">
                        </PropertiesTextEdit>
                        <HeaderStyle Wrap="True" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Name="Amount" Caption="Amount" VisibleIndex="12" FieldName="Amount"
                        Width="120px">
                        <PropertiesTextEdit DisplayFormatString="#,###">
                        </PropertiesTextEdit>
                        <CellStyle HorizontalAlign="Right">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Price Estimation" FieldName="PriceEst" Name="PriceEst"
                        VisibleIndex="11" Width="100px">
                        <PropertiesTextEdit DisplayFormatString="#,###" MaxLength="11">
                            <MaskSettings IncludeLiterals="DecimalSymbol"   />
                            <ValidationSettings ErrorDisplayMode="None">
                            </ValidationSettings>
                            <ClientSideEvents TextChanged="UpdateTotalAmount" />
                            <Style HorizontalAlign="Right"></Style>
                        </PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Quantity" FieldName="Qty" Name="Qty" VisibleIndex="3"
                        Width="70px">
                        <PropertiesTextEdit DisplayFormatString="#,###" MaxLength="11">
                            <MaskSettings IncludeLiterals="DecimalSymbol" />
                            <ValidationSettings ErrorDisplayMode="None">
                            </ValidationSettings>
                            <ClientSideEvents TextChanged="UpdateTotalAmount" />
                            <Style HorizontalAlign="Right"></Style>
                        </PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True"
                    AllowSelectByRowClick="True" />
                <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                </SettingsPager>
                <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                    <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                </SettingsEditing>
                <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                    VerticalScrollableHeight="200" VerticalScrollBarMode="Visible" />
                <Styles>
                    <Header HorizontalAlign="Center">
                        <Paddings PaddingBottom="5px" PaddingTop="5px" />
                    </Header>
                </Styles>
                <StylesEditors ButtonEditCellSpacing="0">
                    <ProgressBar Height="21px">
                    </ProgressBar>
                </StylesEditors>
            </dx:ASPxGridView>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 0px; height: 100px;">
                <tr style="height: 20px">
                    <td style="padding: 0px 0px 0px 10px; width: 180px">
                        <dx1:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Notes">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                    </td>
                    <td>
                        <dx1:ASPxMemo ID="txtNotes" runat="server" Height="50px" Width="500px" ClientInstanceName="txtNotes"
                            Font-Names="Segoe UI" Font-Size="9pt" Theme="Metropolis" MaxLength="300">
                        </dx1:ASPxMemo>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr style="height: 30px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                        <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="IA Budget Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                    </td>
                    <td>
                        <%--<dx1:ASPxTextBox ID="txtIABudgetNumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                         Width="150px" ClientInstanceName="txtIABudgetNumber" MaxLength="20" Enabled="false"
                                         Height="25px" >
                        </dx1:ASPxTextBox>		--%>
                        <asp:Label ID="lblIABudgetNo" runat="server" Font-Names="Segoe UI" Font-Size="9pt"></asp:Label>
                  &nbsp;<dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf">
                    </dx:ASPxHiddenField>
                    <dx:ASPxHiddenField ID="IABudgetNo" runat="server" ClientInstanceName="IABudgetNo">
                    </dx:ASPxHiddenField>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr style="height: 30px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                        <dx1:ASPxLabel ID="ASPxLabel12" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="IA Budget Document">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        <dx:ASPxHyperLink ID="IALinkFile"  ClientInstanceName="IALinkFile" runat="server" 
                            Text="No File"  >  
                        <ClientSideEvents  Click="function(s, e) {
                            //alert(LinkFile.GetText());

                            if (IALinkFile.GetText() != 'No File' || IALinkFile.GetText()!='') {
                                var fileName, fileExtension;
                                fileName = IALinkFile.GetText();
                                fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
                                
                                if (fileExtension =='pdf'){
                                    var pathArray = window.location.pathname.split('/');
                                    var url = window.location.origin + '/Files/' + IALinkFile.GetText();
                                    window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
                                }
                                var pathArray = window.location.pathname.split('/');

                                //alert(window.location.origin + '/Files/' + IALinkFile.GetText());
                               
                                //var url = window.location.origin + '/' + pathArray[1] + '/ItemList.aspx';                                
                                //window.location.href = window.location.origin + '/ItemList.aspx';

                            }
	                        
                        }" />
                    </dx:ASPxHyperLink>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr style="height: 20px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr style="height: 20px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                        <dx:ASPxCheckBox ID="chkSkip" runat="server" ClientInstanceName="chkSkip" Text="Skip Budget"
                            ValueChecked="1" ValueType="System.Int32" ValueUnchecked="0" Font-Names="Segoe UI"
                            Font-Size="9pt" CheckState="Unchecked">
                            <ClientSideEvents CheckedChanged="CheckBoxSkip" />
                        </dx:ASPxCheckBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="height: 20px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                        <dx1:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Notes/Reason">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <dx1:ASPxMemo ID="txtReason" runat="server" Height="50px" Width="500px" ClientInstanceName="txtReason"
                            Font-Names="Segoe UI" Font-Size="9pt" Theme="Metropolis" MaxLength="300">
                        </dx1:ASPxMemo>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="height: 30px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                        <dx1:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Document Upload">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:FileUpload ID="uploaderFile" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Height="20px" Width="250px" Enabled="False" Accept=".docx,.pdf,.jpeg,.xlsx" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="height: 30px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <dx1:ASPxButton ID="btnAddFile" runat="server" Text="Add File" Font-Names="Segoe UI"
                            Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False" UseSubmitBehavior="False"
                            ClientInstanceName="btnAddFile" Theme="Default">
                            <ClientSideEvents Click="AddFile" />
                        </dx1:ASPxButton>
                        
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="height: 20px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                        <dx1:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="File">
                        </dx1:ASPxLabel>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <dx:ASPxHyperLink ID="LinkFile" ClientInstanceName="LinkFile" runat="server" Text="No File">
                            <ClientSideEvents Click="function(s, e) {
                            //alert(LinkFile.GetText());

                            if (LinkFile.GetText() != 'No File' || LinkFile.GetText()!='') {
                                var fileName, fileExtension;
                                fileName = LinkFile.GetText();
                                fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
                                                                              
                                if (fileExtension =='pdf'){
                                    var pathArray = window.location.pathname.split('/');
                                    var url = window.location.origin  + '/Files/' +  LinkFile.GetText();
                                    window.open(encodeURI(url), '_blank', 'toolbar=0,location=0,menubar=0');
                                }
                                var pathArray = window.location.pathname.split('/');

                                //alert(window.location.origin + '/Files/' + LinkFile.GetText());
                               
                                //var url = window.location.origin + '/' + pathArray[1] + '/ItemList.aspx';                                
                                //window.location.href = window.location.origin + '/ItemList.aspx';

                            }
	                        
                        }" />
                        </dx:ASPxHyperLink>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="height: 20px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <dx:ASPxCallback ID="cbFile" runat="server" ClientInstanceName="cbFile">
                            <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
                        </dx:ASPxCallback>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr style="height: 30px">
                    <td style="padding: 0px 0px 0px 10px;" colspan="8">
                        <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False" Font-Names="Segoe UI"
                            Font-Size="9pt" Width="80px" Height="25px" UseSubmitBehavior="False" ClientInstanceName="btnBack"
                            Theme="Default">
                            <Paddings Padding="2px" />
                        </dx1:ASPxButton>
                        &nbsp;
                        <dx1:ASPxButton ID="btnDraft" runat="server" Text="Draft" AutoPostBack="False" Font-Names="Segoe UI"
                            Font-Size="9pt" Width="80px" Height="25px" UseSubmitBehavior="False" ClientInstanceName="btnDraft"
                            Theme="Default">
                            <ClientSideEvents Click="DraftProcess" />
                            <Paddings Padding="2px" />
                        </dx1:ASPxButton>
                        &nbsp;
                        <dx1:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" UseSubmitBehavior="False"
                            ClientInstanceName="btnSubmit" Theme="Default">
                            <ClientSideEvents Click="SubmitProcess" />
                            <Paddings Padding="2px" />
                        </dx1:ASPxButton>
                        &nbsp;
                        <dx1:ASPxButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="False" Font-Names="Segoe UI"
                            Font-Size="9pt" Width="80px" Height="25px" UseSubmitBehavior="False" ClientInstanceName="btnPrint"
                            Theme="Default">
                            <Paddings Padding="2px" />
                        </dx1:ASPxButton>
                    </td>
                </tr>
                <tr style="height: 20px">
                    <td style="padding: 0px 0px 0px 10px; width: 120px">
                    </td>
                    <td>
                    </td>
                    <td>
                        <dx:ASPxUploadControl ID="ucAtc" runat="server" ClientInstanceName="ucAtc" Width="50%"
                            Height="30px" Visible="False">
                            <ValidationSettings AllowedFileExtensions=".pdf">
                            </ValidationSettings>
                            <BrowseButton Text="...">
                            </BrowseButton>
                            <ClientSideEvents FileUploadStart="OnFileUploadStart" FilesUploadComplete="OnFileUploadComplete"
                                TextChanged="OnTextChanged" />
                        </dx:ASPxUploadControl>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

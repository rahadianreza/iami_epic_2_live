﻿Public Class AddPRJList
    Inherits System.Web.UI.Page
#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        Dim _projectID As String
        _projectID = Request.QueryString("ID") & ""

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            'If _projectID <> "" Then
            Dim dfrom As Date
            dfrom = Now
            ProjectDate.Value = dfrom
            OTSSample.Value = dfrom
            SOPTiming.Value = dfrom
            cboProjectType.SelectedIndex = 0
            StartPeriod.Value = Now
            EndPeriod.Value = Now

            up_FillCombo(cboProjectType, statusAdmin, pUser, "ProjectType")
            up_LoadData(_projectID)
            If txtProjectID.Text <> "(Auto Generated)" Then
                btnClear.Enabled = False
            End If
            'End If

            'Else
            ''up_LoadData(_projectID)

        End If

    End Sub

    Private Sub Show_Delete(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbDelete.JSProperties("cpMessage") = ErrMsg
    End Sub

#End Region

#Region "Control Event"
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("PRJList.aspx")
    End Sub

    Private Sub cbTmp_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbTmp.Callback
        Dim pFunction As String = e.Parameter

        Select Case pFunction
            Case "Code"

        End Select
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"
        up_FillCombo(cboProjectType, statusAdmin, pUser, "ProjectType")
    End Sub

    Public Function validation() As Boolean
        If txtProjectName.Text = "" Then
            'show_error(MsgTypeEnum.ErrorMsg, "Project Type is required!", 1)
            cbSave.JSProperties("cpMessage") = "Please Input Project Name"
            Return False
        ElseIf txtRateUSD.Text = "0" Then
            'show_error(MsgTypeEnum.ErrorMsg, "Rate USD/IDR is required!", 1)
            cbSave.JSProperties("cpMessage") = "Rate USD/IDR cannot 0"
            Return False
        ElseIf txtRateYEN.Text = "0" Then
            'show_error(MsgTypeEnum.ErrorMsg, "Rate YEN/IDR is required!", 1)
            cbSave.JSProperties("cpMessage") = "Rate YEN/IDR cannot 0"
            Return False
        ElseIf txtRateBATH.Text = "0" Then
            'show_error(MsgTypeEnum.ErrorMsg, "Rate BATH/IDR is required!", 1)
            cbSave.JSProperties("cpMessage") = "Rate BTH/IDR cannot 0"
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        cbSave.JSProperties("cp_message") = ErrMsg
        cbSave.JSProperties("cp_type") = msgType
        cbSave.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Procedure"

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim pErr As String = ""
        Dim ProjectIDOutput As String = ""
        Dim RateUSD As Double = CDbl(txtRateUSD.Text)
        Dim RateYEN As Double = txtRateYEN.Text
        Dim RateBTH As Double = txtRateBATH.Text

        If validation() = False Then
            Exit Sub
        End If
        Try
            If Request.QueryString("ID") & "" = "" Then
                clsPRJListDB.InsertHeaderData(txtProjectName.Text, Format(ProjectDate.Value, "yyyy-MM-dd"), cboProjectType.Value, Format(StartPeriod.Value, "yyyyMM"), Format(EndPeriod.Value, "yyyyMM"), RateUSD, RateYEN, RateBTH, Format(OTSSample.Value, "yyyy-MM-dd"), Format(SOPTiming.Value, "yyyy-MM-dd"), pUser, pErr, ProjectIDOutput)
                cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
                cbSave.JSProperties("cpID") = ProjectIDOutput
                'If pErr = "" Then
                '    Response.Redirect("AddPRJList.aspx?ID=" + ProjectIDOutput)
                'End If
            Else
                clsPRJListDB.UpdateHeaderData(txtProjectID.Text, txtProjectName.Text, Format(ProjectDate.Value, "yyyy-MM-dd"), RateUSD, RateYEN, RateBTH, Format(OTSSample.Value, "yyyy-MM-dd"), Format(SOPTiming.Value, "yyyy-MM-dd"), pUser, pErr)
                cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
            End If

            If pErr = "" Then
                If Request.QueryString("ID") & "" = "" Then
                    up_LoadData(ProjectIDOutput)
                Else
                    up_LoadData(txtProjectID.Text)
                End If

            End If
            up_FillCombo(cboProjectType, statusAdmin, pUser, "ProjectType")
        Catch ex As Exception
            cbSave.JSProperties("cpMessage") = ex.Message
        End Try

    End Sub

    Private Sub up_FillCombo(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                              pAdminStatus As String, pUserID As String, _
                              Optional ByRef pGroup As String = "", _
                              Optional ByRef pParent As String = "", _
                              Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillCombo(pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_LoadData(projid As String)
        Dim ds As New DataSet
        Dim pErr As String = ""
        ds = clsPRJListDB.GetDataPRJ(projid, pErr)
        Dim script As String
        If pErr = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                txtProjectID.Text = Trim(ds.Tables(0).Rows(0)("Project_ID") & "")
                txtProjectName.Text = Trim(ds.Tables(0).Rows(0)("Project_Name") & "")
                ProjectDate.Value = ds.Tables(0).Rows(0)("Project_Date")
                cboProjectType.Value = ds.Tables(0).Rows(0)("Project_Type")
                'cboProjectType.Enabled = False
                StartPeriod.Value = CDate(Trim(ds.Tables(0).Rows(0)("Start_Period")))
                ' StartPeriod.Enabled = False
                EndPeriod.Value = CDate(Trim(ds.Tables(0).Rows(0)("End_Period")))
                'EndPeriod.Enabled = False

                txtRateUSD.Text = Trim(ds.Tables(0).Rows(0)("Rate_USD_IDR"))
                txtRateYEN.Text = Trim(ds.Tables(0).Rows(0)("Rate_YEN_IDR"))
                txtRateBATH.Text = Trim(ds.Tables(0).Rows(0)("Rate_BATH_IDR"))
                OTSSample.Value = ds.Tables(0).Rows(0)("Shec_OTS_Sample")
                SOPTiming.Value = ds.Tables(0).Rows(0)("Shec_SOP_Timing")

                script = "cboProjectType.SetEnabled(false);" & vbCrLf & _
                         "ProjectDate.SetEnabled(false);" & vbCrLf & _
                         "StartPeriod.SetEnabled(false);"
                '"EndPeriod.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(cboProjectType, cboProjectType.GetType(), "cboProjectType", script, True)
            End If
        Else
            txtProjectID.Text = ""
            txtProjectName.Text = ""
            ProjectDate.Text = ""
            cboProjectType.Text = ""
            StartPeriod.Text = ""
            EndPeriod.Text = ""
            txtRateUSD.Text = ""
            txtRateYEN.Text = ""
            txtRateBATH.Text = ""
            OTSSample.Text = ""
            SOPTiming.Text = ""
        End If
        'Catch ex As Exception
        '    Show_Error(MsgTypeEnum.ErrorMsg, ex.Message)
        'End Try

    End Sub

#End Region

End Class
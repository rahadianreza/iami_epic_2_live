﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class IAPriceApproval
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cpMessage") = ErrMsg
        Grid.JSProperties("cpType") = msgType
        Grid.JSProperties("cpVal") = pVal
    End Sub
    Private Sub up_GridHeader(Optional ByRef pLoad As String = "")
        Dim ErrMsg As String = ""
        Dim dtFrom, dtTo As String
        Dim vApproval As String = ""
        Dim ls_Back As Boolean = False

        dtFrom = Format(dtDateFrom.Value, "yyyy-MM-dd")
        dtTo = Format(dtDateTo.Value, "yyyy-MM-dd")



        Grid.Columns.Item(9).Visible = False
        Grid.Columns.Item(10).Visible = False
        Grid.Columns.Item(11).Visible = False
        Grid.Columns.Item(12).Visible = False
        Grid.Columns.Item(13).Visible = False

        Dim ds As New DataSet
        ds = clsIAPriceDB.GetHeaderIAList(dtFrom, dtTo, pUser, ErrMsg)

        If ErrMsg = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                If Trim(ds.Tables(0).Rows(i)("Approval_Name")) = "BUDGET DEPT" Then
                    Grid.Columns.Item(9).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(9).Visible = True
                ElseIf Trim(ds.Tables(0).Rows(i)("Approval_Name")) = "PURCHASING DIV" Then
                    Grid.Columns.Item(10).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(10).Visible = True
                ElseIf Trim(ds.Tables(0).Rows(i)("Approval_Name")) = "PURCHASING ADVISOR" Then
                    Grid.Columns.Item(11).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(11).Visible = True
                ElseIf Trim(ds.Tables(0).Rows(i)("Approval_Name")) = "COST CONTROL DEPT" Then
                    Grid.Columns.Item(12).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(12).Visible = True
                Else
                    Grid.Columns.Item(13).Caption = ds.Tables(0).Rows(i)("Approval_Name")
                    Grid.Columns.Item(13).Visible = True
                End If
            Next
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("G020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "G020")

        If Not Page.IsPostBack Then
            Dim dfrom As Date
            Dim dto As Date = Now

            dfrom = Year(Now) & "-" & Month(Now) & "-01"
            dtDateFrom.Value = dfrom
            dtDateTo.Value = Now
            up_GridHeader("1")
            up_FillComboIAPriceNumber()
            cboStatus.SelectedIndex = 1
            cboIAPriceNo.SelectedIndex = 0
        End If

    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pfunction As String = Split(e.Parameters, "|")(0)
        Dim pDate1 As Date = Split(e.Parameters, "|")(1)
        Dim pDate2 As Date = Split(e.Parameters, "|")(2)
        Dim pIAPriceNo As String = Split(e.Parameters, "|")(3)
        Dim pStatus As String = Split(e.Parameters, "|")(4)

        Dim vDateFrom As String = Format(pDate1, "yyyy-MM-dd")
        Dim vDateTo As String = Format(pDate2, "yyyy-MM-dd")

        If pfunction = "gridload" Then
            up_GridHeader("1")
            up_GridLoad(vDateFrom, vDateTo, pIAPriceNo, pStatus)
        End If
    End Sub


    Private Sub up_FillComboIAPriceNumber()
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim dtfrom As String
        Dim dtto As String
        dtfrom = Format(dtDateFrom.Value, "yyyy-MM-dd")
        dtto = Format(dtDateTo.Value, "yyyy-MM-dd")

        ds = clsIAPriceDB.GetComboDataApproval(dtfrom, dtto, pUser, pErr)
        If pErr = "" Then

            cboIAPriceNo.DataSource = ds
            cboIAPriceNo.DataBind()
        End If
    End Sub

    Private Sub cboIAPriceNo_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboIAPriceNo.Callback
        Try
            Call up_FillComboIAPriceNumber()

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboIAPriceNo)
        End Try
    End Sub

    Private Sub up_GridLoad(pDateFrom As String, pDateTo As String, pIAPriceNo As String, pStatus As String)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        Dim clsIAPrice As New clsIAPrice

        'If pIAPriceNo = "ALL" Then
        '    pIAPriceNo = ""
        'End If
        clsIAPrice.IAPriceNo = pIAPriceNo
        clsIAPrice.IAPriceDateFrom = pDateFrom
        clsIAPrice.IAPriceDateTo = pDateTo
        clsIAPrice.StatusApproval = pStatus
        up_GridHeader()
        ds = clsIAPriceDB.GetListApproval(clsIAPrice, pUser, ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count = 0 Then
                up_GridHeader("1")
            End If
            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub ExcelGrid()
        Try
            Dim dtfrom As String
            Dim dtto As String
            dtfrom = Format(dtDateFrom.Value, "yyyy-MM-dd")
            dtto = Format(dtDateTo.Value, "yyyy-MM-dd")

            Call up_GridLoad(dtfrom, dtto, cboIAPriceNo.Text, cboStatus.Text)

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=IAPriceApproval_" & Format(CDate(Now), "yyyyMMdd_hhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            gs_Message = ex.Message
        End Try
    End Sub

    Private Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Dim ErrMsg As String = ""
        Call ExcelGrid()
    End Sub
   
End Class
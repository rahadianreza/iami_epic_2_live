﻿Imports DevExpress.Web.ASPxClasses

Public Class BiddersList
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String

    Dim prjid As String
    Dim grpid As String
    Dim comm As String
    Dim pErr As String = ""
    Dim pg As String = ""
    Dim py As String = ""
    Dim byr As String = ""
    Dim grpcomm As String = ""
    Dim pagetype As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        ' sGlobal.getMenu("J010")
        Master.SiteTitle = "Bidder List"
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        prjid = Request.QueryString("Project_Id")
        grpid = Request.QueryString("Group_Id")
        comm = Request.QueryString("comm")
        pg = Request.QueryString("page")
        py = Request.QueryString("project_type")
        byr = Request.QueryString("buyer")
        grpcomm = Request.QueryString("groupcommodity")
        pagetype = Request.QueryString("pagetype")

        sGlobal.getMenu(pg)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            up_GridLoad("")
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsGateICheckSheetDB.GetGridBidderList(prjid, grpid, comm, byr, pUser, grpcomm, statusAdmin)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()

            'Dim ds As New DataSet
            'ds = clsGateICheckSheetDB.GetRateJPY_NewGuidePrice(prjid)

            'If ds.Tables(0).Rows.Count > 0 Then
            '    Grid.Columns("New_Guide_Price_Jpn").Caption = ds.Tables(0).Rows(0)("Rate_YEN_IDR")
            'Else
            '    Grid.Columns("New_Guide_Price_Jpn").Caption = "New Guide Price JPN=Rp "
            'End If
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        up_GridLoad("")
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.VisibleIndex < 0) Then
            Exit Sub
        End If
        If pg = "J010" Then
            If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Accepted" Then
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                    e.Visible = False
                End If

                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Delete Then
                    e.Visible = False
                End If
            End If
        ElseIf pg = "J020" Then
            If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Completed" Then
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                    e.Visible = False
                End If

                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Delete Then
                    e.Visible = False
                End If
            End If
        ElseIf pg = "J030" Then
            If clsGateICheckSheetDB.CheckGateStatus(prjid, grpid, comm, byr, grpcomm) <> "Confirmed" Then
                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                    e.Visible = False
                End If

                If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Delete Then
                    e.Visible = False
                End If
            End If
        End If
    End Sub

    Public Function checking(value As String) As Boolean
        If String.IsNullOrEmpty(value) Or value = "0" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles Grid.RowDeleting
        e.Cancel = True
        clsGateICheckSheetDB.DeleteBiddersList(prjid, grpid, comm, e.Keys("SeqNo"), byr, pUser, grpcomm, statusAdmin, pErr)
        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
        End If
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True

        Dim aFlag As Integer = 0

        If checking(prjid) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
        ElseIf checking(grpid) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
        ElseIf checking(comm) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Commodity cannot empty", 1)
        ElseIf checking(byr) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Buyer cannot empty", 1)
        ElseIf checking(e.Keys("SeqNo")) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Sequence No cannot empty", 1)
        ElseIf checking(e.NewValues("Supplier_Code")) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Supplier Code cannot empty", 1)
        ElseIf checking(e.NewValues("Main_Parts")) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Main Part cannot empty", 1)
        ElseIf checking(e.NewValues("Main_Customer")) = False Then
            aFlag = 0
            show_error(MsgTypeEnum.Warning, "Main Customer cannot empty", 1)
            'ElseIf checking(e.NewValues("Explanation_meetings")) = False Then
            '    aFlag = 0
            '    show_error(MsgTypeEnum.Warning, "Explanation meetings cannot empty", 1)
        Else
            aFlag = 1
        End If

        If aFlag = 1 Then
            clsGateICheckSheetDB.InsertBiddersList(prjid, grpid, comm, byr, e.Keys("SeqNo"), e.NewValues("Supplier_Code"), e.NewValues("Main_Parts"), _
                                              e.NewValues("Main_Customer"), e.NewValues("Explanation_meetings"), e.NewValues("Remarks"), grpcomm, pUser, statusAdmin, pErr)
            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
            End If
        End If

    End Sub

#End Region

#Region "Control Event"
    Private Sub cboSupplier_OnCallback(ByVal source As Object, ByVal e As CallbackEventArgsBase)

    End Sub


    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        If pg = "J010" Then
            If pagetype <> "submit" Then
                Response.Redirect("GetCheckSheetView.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
            Else
                Response.Redirect("Get_I_Check_Sheet.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
            End If
            'Response.Redirect("Get_I_Check_Sheet.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
        ElseIf pg = "J020" Then
            Response.Redirect("GetCheckSheetConfirm.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
        ElseIf pg = "J030" Then
            Response.Redirect("GetCheckSheetApproval.aspx?projecttype=" + py + "&projectid=" + prjid + "&groupid=" + grpid + "&commodity=" + comm + "&buyer=" + byr + "&groupcommodity=" + grpcomm + "&pagetype=" + pagetype)
        Else
            'nothing
        End If
    End Sub

    'get value bidderlist after selected supplier combobox
    Protected Sub callback1_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles callback1.Callback
        Dim errmsg As String = ""
        Dim ds As New DataSet
        Dim pSupplierCode As String = Split(e.Parameter, "|")(0)
        ds = clsGateICheckSheetDB.GetMainPartNCustomer(pSupplierCode, errmsg)

        If ds.Tables(0).Rows.Count > 0 Then
            If errmsg = "" Then
                callback1.JSProperties("cpGetValue1") = ds.Tables(0).Rows(0)("Main_Parts").ToString()
                callback1.JSProperties("cpGetValue2") = ds.Tables(0).Rows(0)("Main_Customer").ToString()
            Else                callback1.JSProperties("cpGetValue1") = ""
                callback1.JSProperties("cpGetValue2") = ""
            End If
        Else
            callback1.JSProperties("cpGetValue1") = ""
            callback1.JSProperties("cpGetValue2") = ""

        End If
    End Sub

#End Region




End Class
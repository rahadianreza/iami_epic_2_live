﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Material_List.aspx.vb" 
Inherits="IAMI_EPIC2.Material_List" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
   
    //GET MESSAGE
        function GetMessage(s, e) {
            if (s.cp_disabled == "N") {
                //alert(s.cp_disabled);
                btnDownload.SetEnabled(true);
            } else if (s.cp_disabled == "Y") {

                btnDownload.SetEnabled(false);
            }

            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        
    }

    function ItemCodeValidation(s, e) {
        if (ItemCode.GetValue() == null) {            
              e.isValid = false;           
        }
      }

   function downloadValidation(s, e) {
       if (Grid.GetVisibleRowsOnPage() == 0) {
           btnDownload.SetEnabled(false);
           toastr.info('There is no data to download!', 'Info');
           e.processOnServer = false;
           return;
       } else {
           btnDownload.SetEnabled(true);
       }
   }

   function showDataValidation(s, e) {
       if (Grid.GetVisibleRowsOnPage() == 0) {
           toastr.info('There is no data to show!', 'Info');
           e.processOnServer = false;
           return;
       }
   }

</script>

    <style type="text/css">
        .style1
        {
            width: 82px;
        }
        .style5
        {
            height: 2px;
        }
    </style>
    <style type="text/css">
        .td-col-l
        {
            padding:0px 0px 0px 10px;
            width:80px;
            
        }
        .td-col-m
        {
            width:10px;
        }
        .td-col-r
        {
            width:100px;
        }
        .td-col-f
        {
            width:50px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">

<%--         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%"
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="ItemCode" DataSourceID="SqlDataSource1">--%>

<table style="width: 100%; border: 1px solid black">  
    <tr style="height:35px">
        <td style="width:100px; padding-left:10px">
             <dx1:ASPxLabel ID="lblType" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Material Type" ClientInstanceName="lblType">
            </dx1:ASPxLabel>
        </td>
        <td class="td-col-m"></td>
        <td class="td-col-r">
            <dx:ASPxComboBox ID="cboMaterialType" runat="server" ClientInstanceName="cboMaterialType"
                Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                DataSourceID="SqlDataSource1" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                Height="22px">
                
                <Columns>
                    <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                    <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                </Columns>
                <ItemStyle Height="10px" Paddings-Padding="4px">
                    <Paddings Padding="4px"></Paddings>
                </ItemStyle>
                <ButtonStyle Width="5px" Paddings-Padding="4px">
                    <Paddings Padding="4px"></Paddings>
                </ButtonStyle>
            </dx:ASPxComboBox>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="select 'ALL' Code , 'ALL' Description UNION ALL select Par_Code,Par_Description from Mst_Parameter where Par_Group='MaterialType'">
            </asp:SqlDataSource>
        </td>
        <td colspan="5"></td>
    </tr>
    <tr style="height:10px">
        <td class="td-col-r" colspan="9"></td>
    </tr>
    <tr style="height: 35px">
        <td style=" padding:0px 0px 0px 10px" colspan="9">            
            <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                ClientInstanceName="btnRefresh" Theme="Default">                        
                <ClientSideEvents Click="function(s, e) {
                    Grid.PerformCallback('gridload|' +  cboMaterialType.GetValue());   
	              }" />
                <Paddings Padding="2px" />
            </dx:ASPxButton>
        &nbsp;<dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                ClientInstanceName="btnDownload" Theme="Default" >      
                 <ClientSideEvents Click="downloadValidation" />                   
                <Paddings Padding="2px" />
            </dx:ASPxButton>
            &nbsp;
            <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="false"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px"
                ClientInstanceName="btnAdd" Theme="Default" >                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>&nbsp;
            <dx:ASPxButton ID="btnChart" runat="server" Text="Chart" UseSubmitBehavior="false"
                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" Enabled ="false" visible="false"
                ClientInstanceName="btnChart" Theme="Default" >                        
                <Paddings Padding="2px" />
            </dx:ASPxButton>
        </td>
    </tr>   
    <tr style="height: 0px">
        <td style=" padding:0px 0px 0px 10px" class="style1" colspan="9">            
            &nbsp;</td>
    </tr>   
</table>
      <div style="padding:5px 5px 5px 5px">
          <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
              <ClientSideEvents EndCallback="GetMessage" />
          </dx:ASPxCallback>
          <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
          </dx:ASPxGridViewExporter>
          <dx:ASPxCallback ID="cbExcel" runat="server" ClientInstanceName="cbExcel">
              <ClientSideEvents EndCallback="GetMessage" />
          </dx:ASPxCallback>
      </div>
      </div>
<div style="padding: 5px 5px 5px 5px">
    <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
        EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Size="9pt" Font-Names="Segoe UI"
        KeyFieldName="Material_Code;Material_Type;GroupItem"  >
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
            HorizontalScrollBarMode="Auto"  />
        <ClientSideEvents EndCallback="GetMessage"  CustomButtonClick="function(s, e) {
	                  if(e.buttonID == &#39;Edit&#39;){ 
                            var rowKey = Grid.GetRowKey(e.visibleIndex);
                            window.location.href= &#39;Material_Detail.aspx?ID=&#39; + rowKey;
                      }
                      else if (e.buttonID == &#39;Delete&#39;){
                           if (confirm('Are you sure want to delete ?')) {
                               var rowKey = Grid.GetRowKey(e.visibleIndex);
                               Grid.PerformCallback('delete |' + rowKey);
                           } else {
                     
                           }
                     }
                  }"></ClientSideEvents>
        <Columns>
            <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" Width="120px">
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Detail">
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete">
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="Material_Code" Caption="Material Code" VisibleIndex="1" Width="100px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn FieldName="Material_Name" Caption="Material Name" VisibleIndex="2" Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn FieldName="Country_Desc" Caption="Country" VisibleIndex="3" Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Material Type" FieldName="Material_Type" VisibleIndex="4" Visible="false"
                Settings-AutoFilterCondition="Contains">
                <PropertiesTextEdit MaxLength="50" Width="90px" ClientInstanceName="Material_Type">
                    <%--<ClientSideEvents Validation="ItemCodeValidation" />--%>
                    <Style HorizontalAlign="Left"></Style>
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains"></Settings>
                <EditFormSettings VisibleIndex="1" />
                <EditFormSettings VisibleIndex="1"></EditFormSettings>
                <FilterCellStyle Paddings-PaddingRight="4px">
                    <Paddings PaddingRight="4px"></Paddings>
                </FilterCellStyle>
                <HeaderStyle Paddings-PaddingLeft="8px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="8px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Material Type" FieldName="MaterialTypeDesc"  VisibleIndex="5" Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GroupItem" Caption="Group Item" VisibleIndex="6" Width="150px" Visible="false">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GroupItemDesc" Caption="Group Item" VisibleIndex="7" Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="CategoryDesc" Caption="Category" VisibleIndex="8" Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="UOMDesc" Caption="UOM" VisibleIndex="9" Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="10" FieldName="Register_User">
                    <Settings AllowAutoFilter="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="11" FieldName="Register_Date">
                <Settings AllowAutoFilter="False" />
                <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="12" FieldName="Update_User">
                <Settings AllowAutoFilter="False" />
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="13" FieldName="Update_Date">
                <Settings AllowAutoFilter="False" />
                <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
        <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
        </SettingsPager>
        <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
            HorizontalScrollBarMode="Auto" />
        <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
            <Header>
                <Paddings Padding="2px"></Paddings>
            </Header>
            <EditFormColumnCaption>
                <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
            </EditFormColumnCaption>
        </Styles>
    </dx:ASPxGridView>
   
    </div>
</div>


</asp:Content>

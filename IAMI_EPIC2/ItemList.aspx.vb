﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports Microsoft.Office.Interop.Excel
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class ItemList
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""

    Dim pHeader As Boolean
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Procedure"
    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|A030", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAdd.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAdd, btnAdd.GetType(), "btnAdd", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub


    Private Sub FillCombo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsItemListDB.GetDataGroup(pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboGroupItem.Items.Add(Trim(ds.Tables(0).Rows(i)("Group_Code") & ""))
            Next
        End If

        'ds = clsItemListDB.GetFilterDataCategory(pErr)
        'If pErr = "" Then
        '    For i = 0 To ds.Tables(0).Rows.Count - 1
        '        cboCategory.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
        '    Next
        'End If

        ds = clsItemListDB.GetDataPRType(pErr)
        If pErr = "" Then
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPRType.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If



    End Sub

    Private Sub up_FillComboCategory(pGroup As String, Optional ByRef pErr As String = "")
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim vGroup As String

        vGroup = pGroup

        ds = clsItemListDB.GetFilterDataCategory(vGroup, pmsg)
        If pmsg = "" Then
            cboCategory.DataSource = ds
            cboCategory.DataBind()
        End If
    End Sub

    Private Sub up_GridLoad(pGroup As String, pCategory As String, pPRType As String, pSupplier As String)
        Dim ErrMsg As String = ""
        Dim Pro As List(Of clsItemList)
        Pro = clsItemListDB.GetList(pGroup, pCategory, pPRType, pSupplier, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Pro
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_ExcelGridAuto(Optional ByRef pErr As String = "")

        Try
            'If cboGroupItem.Text <> "" Then
            '    gs_FilterGroup = cboGroupItem.SelectedItem.GetValue("Group_Code").ToString()
            'ElseIf cboGroupItem.Text = "" Then
            '    gs_FilterGroup = ""
            'End If
            'If cboCategory.Text <> "" Then
            '    gs_FilterCategory = cboCategory.Text 'cboCategory.SelectedItem.GetValue("Group_Code").ToString()
            'Else
            '    gs_FilterCategory = ""
            'End If
            'If cboLastSupplier.Text <> "" Then
            '    gs_FilterSupplier = cboLastSupplier.Text 'cboLastSupplier.SelectedItem.GetValue("Group_Code").ToString()
            'End If
            'If cboPRType.Text <> "" Then
            '    gs_FilterPRType = cboPRType.Text 'cboPRType.SelectedItem.GetValue("Group_Code").ToString()
            'End If

            up_GridLoad(gs_FilterGroup, gs_FilterCategory, gs_FilterPRType, gs_FilterSupplier)

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=ItemList" & Format(Date.Now, "ddMMyyyyHHmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            pErr = ex.Message
        End Try

    End Sub

    'Private Sub up_Excel(Optional ByRef pErr As String = "")
    '    Try
    '        Dim fi As New FileInfo(Server.MapPath("~\Download\ItemList.xlsx"))
    '        If fi.Exists Then
    '            fi.Delete()
    '            fi = New FileInfo(Server.MapPath("~\Download\ItemList.xlsx"))
    '        End If
    '        Dim exl As New ExcelPackage(fi)
    '        Dim ws As ExcelWorksheet
    '        ws = exl.Workbook.Worksheets.Add("Sheet1")
    '        ws.View.ShowGridLines = False

    '        With ws
    '            'Background Header Table
    '            Dim rgHeader As ExcelRange = .Cells(8, 1, 9, 9)
    '            rgHeader.Style.Fill.PatternType = Style.ExcelFillStyle.Solid
    '            rgHeader.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.DimGray)
    '            rgHeader.Style.Fill.PatternType = Style.ExcelFillStyle.Solid



    '            .Cells(1, 1, 1, 1).Value = "IAMI EPIC - Item List"
    '            .Cells(1, 1, 1, 1).Style.Font.Size = 14
    '            .Cells(1, 1, 1, 1).Style.Font.Bold = True

    '            .Cells(3, 1, 3, 1).Value = "Group Item"
    '            .Cells(3, 2, 3, 2).Value = ": " & cboGroupItem.Text

    '            .Cells(4, 1, 4, 1).Value = "Category Item"
    '            .Cells(4, 2, 4, 2).Value = ": " & cboCategory.Text

    '            .Cells(5, 1, 5, 1).Value = "PR Type"
    '            .Cells(5, 2, 5, 2).Value = ": " & cboPRType.Text

    '            .Cells(6, 1, 6, 1).Value = "Last Supplier"
    '            .Cells(6, 2, 6, 2).Value = ": " & cboLastSupplier.Text


    '            .Cells(8, 1, 8, 1).Value = "Item Code"
    '            .Cells(8, 1, 9, 1).Merge = True
    '            .Cells(8, 1, 8, 1).Style.HorizontalAlignment = HorzAlignment.Center
    '            .Cells(8, 1, 8, 1).Style.VerticalAlignment = VertAlignment.Bottom
    '            .Cells(8, 1, 8, 1).Style.Font.Size = 9
    '            .Cells(8, 1, 8, 1).Style.Font.Name = "Arial Unicode MS"
    '            .Cells(8, 1, 8, 1).Style.Font.Color.SetColor(Color.White)

    '            .Cells(8, 2, 8, 2).Value = "Description"
    '            .Cells(8, 2, 9, 2).Merge = True
    '            .Cells(8, 2, 8, 2).Style.HorizontalAlignment = HorzAlignment.Center
    '            .Cells(8, 2, 8, 2).Style.VerticalAlignment = VertAlignment.Bottom
    '            .Cells(8, 2, 8, 2).Style.Font.Size = 9
    '            .Cells(8, 2, 8, 2).Style.Font.Name = "Arial Unicode MS"
    '            .Cells(8, 2, 8, 2).Style.Font.Color.SetColor(Color.White)

    '            .Cells(8, 3, 8, 3).Value = "Specification"
    '            .Cells(8, 3, 9, 3).Merge = True
    '            .Cells(8, 3, 8, 3).Style.HorizontalAlignment = HorzAlignment.Center
    '            .Cells(8, 3, 8, 3).Style.VerticalAlignment = VertAlignment.Bottom
    '            .Cells(8, 3, 8, 3).Style.Font.Size = 9
    '            .Cells(8, 3, 8, 3).Style.Font.Name = "Arial Unicode MS"
    '            .Cells(8, 3, 8, 3).Style.Font.Color.SetColor(Color.White)

    '            .Cells(8, 4, 8, 4).Value = "Group Item"
    '            .Cells(8, 4, 9, 4).Merge = True
    '            .Cells(8, 4, 8, 4).Style.HorizontalAlignment = HorzAlignment.Center
    '            .Cells(8, 4, 8, 4).Style.VerticalAlignment = VertAlignment.Bottom
    '            .Cells(8, 4, 8, 4).Style.Font.Size = 9
    '            .Cells(8, 4, 8, 4).Style.Font.Name = "Arial Unicode MS"
    '            .Cells(8, 4, 8, 4).Style.Font.Color.SetColor(Color.White)

    '            .Cells(8, 5, 8, 5).Value = "Category Item"
    '            .Cells(8, 5, 9, 5).Merge = True
    '            .Cells(8, 5, 8, 5).Style.HorizontalAlignment = HorzAlignment.Center
    '            .Cells(8, 5, 8, 5).Style.VerticalAlignment = VertAlignment.Bottom
    '            .Cells(8, 5, 8, 5).Style.Font.Size = 9
    '            .Cells(8, 5, 8, 5).Style.Font.Name = "Arial Unicode MS"
    '            .Cells(8, 5, 8, 5).Style.Font.Color.SetColor(Color.White)

    '            .Cells(8, 6, 8, 6).Value = "PR Type"
    '            .Cells(8, 6, 9, 6).Merge = True
    '            .Cells(8, 6, 8, 6).Style.HorizontalAlignment = HorzAlignment.Center
    '            .Cells(8, 6, 8, 6).Style.VerticalAlignment = VertAlignment.Bottom
    '            .Cells(8, 6, 8, 6).Style.Font.Size = 9
    '            .Cells(8, 6, 8, 6).Style.Font.Name = "Arial Unicode MS"
    '            .Cells(8, 6, 8, 6).Style.Font.Color.SetColor(Color.White)

    '            .Cells(8, 7, 8, 7).Value = "Last IA Price"
    '            .Cells(8, 7, 9, 7).Merge = True
    '            .Cells(8, 7, 8, 7).Style.HorizontalAlignment = HorzAlignment.Center
    '            .Cells(8, 7, 8, 7).Style.VerticalAlignment = VertAlignment.Bottom
    '            .Cells(8, 7, 8, 7).Style.Font.Size = 9
    '            .Cells(8, 7, 8, 7).Style.Font.Name = "Arial Unicode MS"
    '            .Cells(8, 7, 8, 7).Style.Font.Color.SetColor(Color.White)

    '            .Cells(8, 8, 8, 8).Value = "Last Supplier"
    '            .Cells(8, 8, 9, 8).Merge = True
    '            .Cells(8, 8, 8, 8).Style.HorizontalAlignment = HorzAlignment.Center
    '            .Cells(8, 8, 8, 8).Style.VerticalAlignment = VertAlignment.Bottom
    '            .Cells(8, 8, 8, 8).Style.Font.Size = 9
    '            .Cells(8, 8, 8, 8).Style.Font.Name = "Arial Unicode MS"
    '            .Cells(8, 8, 8, 8).Style.Font.Color.SetColor(Color.White)

    '            .Cells(8, 9, 8, 9).Value = "UOM"
    '            .Cells(8, 9, 9, 9).Merge = True
    '            .Cells(8, 9, 8, 9).Style.HorizontalAlignment = HorzAlignment.Center
    '            .Cells(8, 9, 8, 9).Style.VerticalAlignment = VertAlignment.Bottom
    '            .Cells(8, 9, 8, 9).Style.Font.Size = 9
    '            .Cells(8, 9, 8, 9).Style.Font.Name = "Arial Unicode MS"
    '            .Cells(8, 9, 8, 9).Style.Font.Color.SetColor(Color.White)

    '            Dim ds As DataSet
    '            ds = clsItemListDB.GetDataForExcel(gs_FilterGroup, pErr)

    '            For i = 0 To ds.Tables(0).Rows.Count - 1
    '                .Cells(i + 10, 1, i + 10, 1).Value = ds.Tables(0).Rows(i)("ItemCode")
    '                .Cells(i + 10, 2, i + 10, 2).Value = ds.Tables(0).Rows(i)("Description")
    '                .Cells(i + 10, 3, i + 10, 3).Value = ds.Tables(0).Rows(i)("Specification")
    '                .Cells(i + 10, 4, i + 10, 4).Value = ds.Tables(0).Rows(i)("GroupItem")
    '                .Cells(i + 10, 5, i + 10, 5).Value = ds.Tables(0).Rows(i)("CategoryItem")
    '                .Cells(i + 10, 6, i + 10, 6).Value = ds.Tables(0).Rows(i)("PRType")
    '                .Cells(i + 10, 7, i + 10, 7).Value = ds.Tables(0).Rows(i)("LastIAPrice")
    '                .Cells(i + 10, 8, i + 10, 8).Value = ds.Tables(0).Rows(i)("LastSupplier")
    '                .Cells(i + 10, 9, i + 10, 9).Value = ds.Tables(0).Rows(i)("UOM")
    '            Next

    '            'Set Column
    '            .Column(1).Width = 15
    '            .Column(2).Width = 15
    '            .Column(3).Width = 15
    '            .Column(4).Width = 15
    '            .Column(5).Width = 15
    '            .Column(6).Width = 15
    '            .Column(7).Width = 15
    '            .Column(8).Width = 15
    '            .Column(9).Width = 15

    '            'Set Font Size
    '            .Cells(2, 1, 10 + ds.Tables(0).Rows.Count, 9).Style.Font.Size = 9
    '            'Set Font Name 
    '            .Cells(1, 1, 10 + ds.Tables(0).Rows.Count, 9).Style.Font.Name = "Arial Unicode MS"

    '            .Cells(8, 1, 10 + ds.Tables(0).Rows.Count - 1, 9).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
    '            .Cells(8, 1, 10 + ds.Tables(0).Rows.Count - 1, 9).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
    '            .Cells(8, 1, 10 + ds.Tables(0).Rows.Count - 1, 9).Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
    '            .Cells(8, 1, 10 + ds.Tables(0).Rows.Count - 1, 9).Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
    '            .Cells(8, 1, 10 + ds.Tables(0).Rows.Count - 1, 9).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin

    '            'For i = 0 To Grid.VisibleRowCount - 1
    '            '    .Cells(i + 8, 1, i + 8, 1).Value = Grid.GetRowValues(i, "ItemCode")

    '            'Next


    '        End With

    '        exl.Save()

    '        DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("Download/" & fi.Name)
    '    Catch ex As Exception
    '        'show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
    '        pErr = ex.Message
    '    End Try

    'End Sub



    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboGroupItem)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A030")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A030")

        cbExcel.JSProperties("cpmessage") = ""
        Dim i As Integer

        FillCombo()

        If Not Page.IsPostBack Then
            ' If gs_Back = True Then
            '    up_GridLoad(gs_FilterGroup, gs_FilterCategory, gs_FilterPRType, gs_FilterSupplier)
            '    cboGroupItem.SelectedIndex = gs_FilterGroupIndex
            '    cboCategory.SelectedIndex = gs_FilterCategoryIndex
            '    cboPRType.SelectedIndex = gs_FilterPRTypeIndex
            '    cboLastSupplier.SelectedIndex = gs_FilterSupplierIndex
            'Else
            cboGroupItem.SelectedIndex = 0
            cboCategory.SelectedIndex = 0
            cboPRType.SelectedIndex = 0
            cboLastSupplier.SelectedIndex = 0
        End If
        'End If
    
    End Sub
#End Region

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("~/AddItemMaster.aspx")
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback


        up_GridLoad(gs_FilterGroup, gs_FilterCategory, gs_FilterPRType, gs_FilterSupplier)
        'pHeader = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback

        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pgroup As String
        Dim pcategory As String
        Dim pprtype As String
        Dim psupplier As String

        pgroup = Split(e.Parameters, "|")(1)
        pcategory = Split(e.Parameters, "|")(2)
        pprtype = Split(e.Parameters, "|")(3)
        psupplier = Split(e.Parameters, "|")(4)

        pHeader = True
        If pFunction = "gridload" Then
            If pGroup = "00" Then
                gs_FilterGroup = ""
            Else
                gs_FilterGroup = pgroup
            End If
            If pCategory = "00" Then
                gs_FilterCategory = ""
            Else
                gs_FilterCategory = pcategory
            End If
            If pPRType = "00" Then
                gs_FilterPRType = ""
            Else
                gs_FilterPRType = pprtype
            End If
            If pSupplier = "ALL" Then
                gs_FilterSupplier = ""
            Else
                gs_FilterSupplier = psupplier
            End If

            up_GridLoad(gs_FilterGroup, gs_FilterCategory, gs_FilterPRType, gs_FilterSupplier)

            gs_FilterGroupIndex = cboGroupItem.SelectedIndex
            gs_FilterCategoryIndex = cboCategory.SelectedIndex
            gs_FilterPRTypeIndex = cboPRType.SelectedIndex
            gs_FilterSupplierIndex = cboLastSupplier.SelectedIndex
        Else
            gs_FilterGroup = "x"
            gs_FilterCategory = "x"
            gs_FilterPRType = "x"
            gs_FilterSupplier = "x"
            up_GridLoad(gs_FilterGroup, gs_FilterCategory, gs_FilterPRType, gs_FilterSupplier)
        End If

    End Sub

    'Private Sub Grid_RowUpdated(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatedEventArgs) Handles Grid.RowUpdated
    '    Response.Redirect("~/AddItemMaster.aspx")
    'End Sub


    'Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
    '       Response.Redirect("~/AddItemMaster.aspx")

    'End Sub

    'Private Sub Grid_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs) Handles Grid.StartRowEditing
    '    'Response.Redirect("~/AddItemMaster.aspx")          

    '    DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("~/AddItemMaster.aspx?")
    'End Sub

    'Protected Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click

    '    If cboGroupItem.Text <> "" Then
    '        fgroup = cboGroupItem.SelectedItem.GetValue("Group_Code").ToString()
    '    ElseIf cboGroupItem.Text = "" Then
    '        fgroup = ""
    '    End If
    '    If cboCategory.Text <> "" Then
    '        fcategroy = cboCategory.Text 'cboCategory.SelectedItem.GetValue("Group_Code").ToString()
    '    End If
    '    If cboLastSupplier.Text <> "" Then
    '        flastsupplier = cboLastSupplier.Text 'cboLastSupplier.SelectedItem.GetValue("Group_Code").ToString()
    '    End If
    '    If cboPRType.Text <> "" Then
    '        fprtype = cboPRType.Text 'cboPRType.SelectedItem.GetValue("Group_Code").ToString()
    '    End If

    '    up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

    'End Sub

    Protected Sub BtnDownload_Click(sender As Object, e As EventArgs) Handles BtnDownload.Click
        'cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        Dim ErrMsg As String = ""
        'up_Excel(ErrMsg)
        up_ExcelGridAuto(ErrMsg)

        If ErrMsg <> "" Then
            cbExcel.JSProperties("cpmessage") = ErrMsg
        Else
            cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        End If

    End Sub

    Private Sub cboCategory_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCategory.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboCategory("")

        ElseIf pFunction = "filter" Then
            up_FillComboCategory(pGroup)
        End If

        If cboCategory.Items.Count > 0 Then
            cboCategory.SelectedIndex = 0
        End If


    End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SupplierQuotation.aspx.vb" Inherits="IAMI_EPIC2.SupplierQuotation" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function FillComboQuotationNumber() {
        cboQuotationNumber.PerformCallback(dtQuotationFrom.GetText() + '|' + dtQuotationTo.GetText());
    }

    function GridLoad(s, e) {
        if (cboQuotationNumber.GetText() == '') {
            toastr.warning('Please select Quotation Number first!', 'Warning');
            cboQuotationNumber.Focus();
            return;
        }
        if (cboQuotationNumber.GetSelectedIndex() == -1) {
            toastr.warning('Invalid Quotation Number!', 'Warning');
            cboQuotationNumber.Focus();
            return;
        }
        
        var dtFrom = dtQuotationFrom.GetDate();
        var YearFrom = dtFrom.getFullYear();
        var MonthFrom = dtFrom.getMonth();
        var DateFrom = dtFrom.getDate();
        var dtQFrom = new Date(YearFrom, MonthFrom, DateFrom)
        
        var dtTo = dtQuotationTo.GetDate();
        var YearTo = dtTo.getFullYear();
        var MonthTo = dtTo.getMonth();
        var DateTo = dtTo.getDate();
        var dtQTo = new Date(YearTo, MonthTo, DateTo)

        if (dtQFrom > dtQTo) {
            toastr.warning('Quotation Date From cannot be greater than Quotation Date To!', 'Warning');
            dtQuotationFrom.Focus();
            return
        }

        Grid.PerformCallback('load|' + dtQuotationFrom.GetText() + '|' + dtQuotationTo.GetText());
    }

    function downloadValidation(s, e) {
        if (Grid.GetVisibleRowsOnPage() == 0) {
            toastr.info('There is no data to download!', 'Info');
            e.processOnServer = false;
            return;
        }
    }

    function GridCustomButtonClick(s, e) {
        if(e.buttonID == 'det') {                     
            var rowKey = Grid.GetRowKey(e.visibleIndex);                             
	        window.location.href= 'SupplierQuotationDetail.aspx?ID=' + rowKey;
        }
    }

    function LoadCompleted(s, e) {
        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }
    }
</script>

<style type="text/css">
    .colwidthbutton
    {
        width: 90px;
    }
    
    .rowheight
    {
        height: 35px;
    }
       
    .col1
    {
        width: 10px;
    }
    .col2
    {
        width: 133px;
    }
    .col3
    {
        width: 133px;
    }
    .col4
    {
        width: 25px;
    }
    .col5
    {
        width: 133px;
    }
    
    .customHeader {
        height: 30px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">
            <tr>
                <td colspan="9" style="height: 10px">
                    </td>
               </tr>
            <tr class="rowheight">
                <td class="col1">
                    </td>
                <td class="col2">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Supplier Code"></dx1:aspxlabel>
                </td>
                <td class="col3">
                    <dx:ASPxTextBox ID="txtSupplierCode" runat="server" width="120px" Height="25px" 
                        ClientInstanceName="txtSupplierCode" BackColor="#CCCCCC" ReadOnly="True">
                    </dx:ASPxTextBox>
                </td>
                <td colspan="3">
                    <dx:ASPxTextBox ID="txtSupplierName" runat="server" width="300px" Height="25px" 
                        ClientInstanceName="txtSupplierName" BackColor="#CCCCCC" ReadOnly="True">
                    </dx:ASPxTextBox> 
                </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <tr class="rowheight">
                <td>
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Quotation Date Range"></dx1:aspxlabel>
                </td>
                <td>
                    <dx:aspxdateedit ID="dtQuotationFrom" runat="server" Theme="Office2010Black" 
                        Width="120px" ClientInstanceName="dtQuotationFrom"
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        <ClientSideEvents ValueChanged="FillComboQuotationNumber" />
                    </dx:aspxdateedit>
                </td>
                <td style="width:25px">
                    <dx1:aspxlabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="To"></dx1:aspxlabel>
                </td>
                <td>
                    <dx:aspxdateedit ID="dtQuotationTo" runat="server" Theme="Office2010Black" 
                        Width="120px" AutoPostBack="false" ClientInstanceName="dtQuotationTo"
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        <ClientSideEvents ValueChanged="FillComboQuotationNumber" />
                    </dx:aspxdateedit> 
                </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <tr class="rowheight">
                <td>
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Quotation Number"></dx1:aspxlabel>
                </td>
                <td colspan="3">
                    <dx1:ASPxComboBox ID="cboQuotationNumber" runat="server" ClientInstanceName="cboQuotationNumber"
                        Width="200px" Font-Names="Segoe UI"  TextField="Quotation_No"
                        ValueField="Quotation_No" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">
                        <ClientSideEvents Init="FillComboQuotationNumber" Validation="" />
                        <Columns>            
                            <dx:ListBoxColumn Caption="Quotation Number" FieldName="Quotation_No" Width="100px" />                       
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx1:ASPxComboBox>
                </td>
                <td>
                    </td>
                <td> 
                    </td>
                <td>
                    </td>
                <td></td>
            </tr>
            <tr style="height:2px">
                <td colspan="9">
                    </td>
               </tr>
            <tr class="rowheight">
                <td></td>
                <td colspan="8">
                    <table>
                        <tr class="rowheight">
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnShow" runat="server" Text="Show Data" UseSubmitBehavior="False"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                                    ClientInstanceName="btnShow" Theme="Default">                        
                                    <ClientSideEvents Click="GridLoad" />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnExcel" Theme="Default">                        
                                    <Paddings Padding="2px" />
                                    <ClientSideEvents
                                        Click="downloadValidation"
                                    />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnAdd" Theme="Default">                        
                                    <Paddings Padding="2px" />                                    
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 10px">
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td> 
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                </td>
            </tr>
        </table>
    </div>
   <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Quotation_No;RFQ_Number;Rev" >
            
            <Columns>
                <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" FixedStyle="Left">                    
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="det" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>                    
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Quotation Number" FieldName="Quotation_No"  FixedStyle="Left"
                     VisibleIndex="1" Width="180px">
                     <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Rev" FieldName="Rev"  FixedStyle="Left"
                     VisibleIndex="2" Width="50px">
                     <CellStyle HorizontalAlign="Center">
                     </CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Quotation Date" FieldName="Quotation_Date" FixedStyle="Left"
                     VisibleIndex="3" Width="120px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center">
                     </CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="RFQ Number" FieldName="RFQ_Number" 
                     VisibleIndex="4" Width="180px">
                     <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="RFQ Date" FieldName="RFQ_Date" 
                     VisibleIndex="5" Width="120px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center">
                     </CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Deadline Date" FieldName="RFQ_DueDate" 
                     VisibleIndex="6" Width="120px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center">
                     </CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Status" FieldName="Quotation_Status" 
                     VisibleIndex="7" Width="120px">
                     <CellStyle HorizontalAlign="Center">
                     </CellStyle>
                     <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn> 
                
                  <dx:GridViewDataTextColumn Caption="Notes" FieldName="Reject_Notes" 
                     VisibleIndex="8" Width="220px">
                     <CellStyle HorizontalAlign="Left">
                     </CellStyle>
                     <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>                 
             </Columns>

            <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
            <Settings ShowFilterRow="True" VerticalScrollableHeight="225" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>
            
            <ClientSideEvents 
                CustomButtonClick="GridCustomButtonClick"
                EndCallback="LoadCompleted"
            />
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" 
                AllowInsert="False" />

            <Styles Header-Paddings-Padding="2px" EditFormColumnCaption-Paddings-PaddingLeft="0px" EditFormColumnCaption-Paddings-PaddingRight="0px" >
                <Header CssClass="customHeader" HorizontalAlign="Center">
                </Header>
            </Styles>
        </dx:ASPxGridView>
    </div>
    <%--<div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
      
        </table>
    </div>--%>
</div>
</asp:Content>
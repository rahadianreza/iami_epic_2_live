﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="QuotationSupplier_MaterialCost.aspx.vb" Inherits="IAMI_EPIC2.QuotationSupplier_MaterialCost" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                  
                    if (s.cp_AdjustmentType == "F") {
                        VisibilityFormula(true);
                        VisibilityTotalAmount(false);
                    } else {
                        VisibilityFormula(false);
                        VisibilityTotalAmount(true);
                    }

                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                      
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };

        function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "Part_No" || currentColumnName == "Commodity" || currentColumnName == "Upc") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        };  

        function SaveData(s, e) {
            Grid.UpdateEdit()
        }

        function disableenablebuttonsubmit(_val) {
            if (_val = '1') {
                btnSave.SetEnabled(false)
            } else {
                btnSave.SetEnabled(true)
             }
        };

        function OnUpdateCheckedChanged(s, e) {
            if (s.GetValue() == -1) s.SetValue(1);
            Grid.SetFocusedRowIndex(-1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "Status_Drawing", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "Status_Drawing", s.GetValue());
                }                
            }
        };
       
       function selectedValueCbo(cmbCategory) {
            SelectValueType1();
            if(Grid.GetEditor("Material_Code").InCallback())
            {
                lastCountry = cmbCategory.GetValue().toString();
            }
            else 
            {
                Grid.GetEditor("Material_Code").PerformCallback(cmbCategory.GetValue().toString());
            }
       };

       function SelectValueType1(s,e){
      
            if (AdjustmentType.GetValue() == 'F'){
                VisibilityFormula(true);
                VisibilityTotalAmount(false);
            }
            if (AdjustmentType.GetValue() == 'M'){
                VisibilityFormula(false);
                VisibilityTotalAmount(true);
            }
      };

       function VisibilityFormula(visible) {
            var disp = visible ? 'table-cell' : 'none';
            $('td.Hidden').css('display', disp);
       };

       function VisibilityTotalAmount(visible) {
            var disp = visible ? 'table-cell' : 'none';
            $('td.Amount').css('display', disp);
       };

        const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2
    })

    function OnChanged(cmbParent) {
//	    if(isUpdating)
//		    return;
//	    var comboValue = cmbParent.GetSelectedItem().value;
//	    if(comboValue)
            
           var thicknes = Grid.GetEditor("Dimension_Thickness").GetValue();
           var width = Grid.GetEditor("Dimension_Width").GetValue();
           var length = Grid.GetEditor("Dimension_Length").GetValue();
           var density = Grid.GetEditor("Dimension_Density").GetValue();
           var factorunit = Grid.GetEditor("Dimension_FactorUnit").GetValue();

           var weight = ((length*width*thicknes)*density/factorunit)
           Grid.GetEditor("Weight").SetValue(weight); 
           //Grid.GetEditor("Weight").SetEnabled(false);

	    
    }

    </script>
    <style type="text/css">
        .Amount
        {
            display: none;
        }
        .AmountVisible
        {
            
        }
        .Hidden
        {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="border: 1px solid black">
        <table>
            <tr>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Part No">
                    </dx:ASPxLabel>
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxComboBox ID="cboPartNo" runat="server" ClientInstanceName="cboPartNo" Width="120px"
                        Font-Names="Segoe UI" TextField="Part_Name" ValueField="Part_No" TextFormatString="{1}"
                        Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                        EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                        <Columns>
                            <dx:ListBoxColumn Caption="Part No" FieldName="Part_No" Width="100px" />
                            <dx:ListBoxColumn Caption="Part Name" FieldName="Part_Name" Width="120px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td>
                </td>
                <td style="padding: 10px 0px 0px 50px; vertical-align: top;" rowspan="2">
                    <table>
                        <tr>
                            <td style="font-family: Segoe UI; font-size: 9pt;" colspan="3">
                               <b>Currency Rate</b> 
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                USD &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                : &nbsp
                            </td>
                            <td>
                                <dx1:ASPxLabel ID="lblUSDRate" runat="server" ClientInstanceName="lblUSDRate"  Font-Names="Segoe UI" Font-Size="9pt"></dx1:ASPxLabel> &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                JPY &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                : &nbsp
                            </td>
                            <td>
                                <dx1:ASPxLabel ID="lblJPYRate" runat="server" ClientInstanceName="lblJPYRate"  Font-Names="Segoe UI" Font-Size="9pt"></dx1:ASPxLabel> &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                THB &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                : &nbsp
                            </td>
                            <td>
                                <dx1:ASPxLabel ID="lblTHBRate" runat="server" ClientInstanceName="lblTHBRate" Font-Names="Segoe UI" Font-Size="9pt"></dx1:ASPxLabel> &nbsp
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr style="height: 50px">
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnBack" runat="server" Text="Back to List" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnBack" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">
                        <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
            Font-Size="9pt" KeyFieldName="Category_Material;Material_Code">
            <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" ShowNewButtonInHeader="true" ShowDeleteButton="true">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataComboBoxColumn Caption="Input Type" FieldName="AdjustmentType" HeaderStyle-HorizontalAlign="Center"
                    VisibleIndex="2" Width="120px">
                    <PropertiesComboBox Width="145px" TextFormatString="{1}" ClientInstanceName="AdjustmentType"
                        DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">
                        <ClientSideEvents SelectedIndexChanged="SelectValueType1" />
                        <Items>
                            <dx:ListEditItem Text="Formula" Value="F" />
                            <dx:ListEditItem Text="Value" Value="M" />
                        </Items>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="2px">
                            <Paddings Padding="2px"></Paddings>
                        </ButtonStyle>
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn Caption="Category Material" FieldName="Category_Material"
                    VisibleIndex="2" Width="120px">
                    <PropertiesComboBox DataSourceID="sdsCategoryMaterial" TextField="Description"
                        ValueField="CategoryCode" Width="145px" TextFormatString="{1}" ClientInstanceName="Category_Material"
                        DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) { selectedValueCbo(s); }" />
                        <Columns>
                            <dx:ListBoxColumn FieldName="CategoryCode" Caption="Code" Width="65px" />
                            <dx:ListBoxColumn FieldName="Description" Caption="Description" Width="200px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="2px">
                            <Paddings Padding="2px"></Paddings>
                        </ButtonStyle>
                    </PropertiesComboBox>
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="true" />
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn Caption="Material Code" FieldName="Material_Code"
                    VisibleIndex="2" Width="120px">
                    <PropertiesComboBox TextField="Material_Name" ValueField="Material_Code" Width="145px"
                        TextFormatString="{1}" ClientInstanceName="Material_Code" DropDownStyle="DropDownList"
                        IncrementalFilteringMode="StartsWith">
                        <Columns>
                            <dx:ListBoxColumn FieldName="Material_Code" Caption="Code" Width="65px" />
                            <dx:ListBoxColumn FieldName="Material_Name" Caption="Name" Width="200px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="2px">
                            <Paddings Padding="2px"></Paddings>
                        </ButtonStyle>
                    </PropertiesComboBox>
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="true" />
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataTextColumn Caption="Material Name" VisibleIndex="4" FieldName="Material_Name">
                    <EditFormSettings Visible="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn Caption="Local/Import" FieldName="Local_Import" VisibleIndex="2"
                    Width="120px">
                    <PropertiesComboBox TextField="Local_Import" ValueField="Local_Import" Width="145px"
                        TextFormatString="{1}" ClientInstanceName="Local_Import" DropDownStyle="DropDownList"
                        IncrementalFilteringMode="StartsWith">
                        <Items>
                            <dx:ListEditItem Text="Lokal" Value="L" />
                            <dx:ListEditItem Text="Import" Value="I" />
                        </Items>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="2px">
                            <Paddings Padding="2px"></Paddings>
                        </ButtonStyle>
                    </PropertiesComboBox>
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="true" />
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataTextColumn Caption="Country" VisibleIndex="6" FieldName="Country">
                    <PropertiesTextEdit MaxLength="20">
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewBandColumn Caption="Dimension" VisibleIndex="6" Name="Dimension" HeaderStyle-HorizontalAlign="Center"
                    Visible="true">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="(A) <br/> Thickness" VisibleIndex="6" FieldName="Dimension_Thickness">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                 <ClientSideEvents KeyUp="function (s,e){OnChanged(s);}" TextChanged="function (s,e){OnChanged(s);}" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="true" />
                            <EditCellStyle CssClass="Hidden">
                            </EditCellStyle>
                            <EditFormCaptionStyle CssClass="Hidden">
                            </EditFormCaptionStyle>
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption=" (B) <br/> Length" VisibleIndex="6" FieldName="Dimension_Length">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                 <ClientSideEvents KeyUp="function (s,e){OnChanged(s);}" TextChanged="function (s,e){OnChanged(s);}" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="true" />
                            <EditCellStyle CssClass="Hidden">
                            </EditCellStyle>
                            <EditFormCaptionStyle CssClass="Hidden">
                            </EditFormCaptionStyle>
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="(C) <br/> Width" VisibleIndex="6" FieldName="Dimension_Width">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                 <ClientSideEvents KeyUp="function (s,e){OnChanged(s);}" TextChanged="function (s,e){OnChanged(s);}" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="true" />
                            <EditCellStyle CssClass="Hidden">
                            </EditCellStyle>
                            <EditFormCaptionStyle CssClass="Hidden">
                            </EditFormCaptionStyle>
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Diameter" VisibleIndex="6" FieldName="Dimension_Diameter">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                 <ClientSideEvents TextChanged="function (s,e){OnChanged(s);}" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="true" />
                            <EditCellStyle CssClass="Hidden">
                            </EditCellStyle>
                            <EditFormCaptionStyle CssClass="Hidden">
                            </EditFormCaptionStyle>
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Density" VisibleIndex="6" FieldName="Dimension_Density">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                 <ClientSideEvents KeyUp="function (s,e){OnChanged(s);}" TextChanged="function (s,e){OnChanged(s);}" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="true" />
                            <EditCellStyle CssClass="Hidden">
                            </EditCellStyle>
                            <EditFormCaptionStyle CssClass="Hidden">
                            </EditFormCaptionStyle>
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Factor Unit" VisibleIndex="6" FieldName="Dimension_FactorUnit">
                            <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                <MaskSettings Mask="<0..100000000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                <ClientSideEvents KeyUp="function (s,e){OnChanged(s);}" TextChanged="function (s,e){OnChanged(s);}" />
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="true" />
                            <EditCellStyle CssClass="Hidden">
                            </EditCellStyle>
                            <EditFormCaptionStyle CssClass="Hidden">
                            </EditFormCaptionStyle>
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                            
                            
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Weight" VisibleIndex="6" FieldName="Weight" ReadOnly="true">
                            <PropertiesTextEdit  DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right" ReadOnlyStyle-BackColor="LightGray">
                                <MaskSettings Mask="<0..100000000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="true" />
                            
                            <EditCellStyle CssClass="Hidden">

                            </EditCellStyle>
                            <EditFormCaptionStyle CssClass="Hidden">
                            </EditFormCaptionStyle>
                            <Settings AutoFilterCondition="Contains" />
                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                Wrap="True">
                                <Paddings PaddingLeft="5px"></Paddings>
                            </HeaderStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewDataTextColumn Caption="((A * B * C) * 7.85 / 1000000) <br/> Material Weight <br/> (D) " VisibleIndex="6" FieldName="Qty_Usage" Width="200px">
                    <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="false" />
                    <EditCellStyle CssClass="Hidden">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Hidden">
                    </EditFormCaptionStyle>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn Caption="Currency" FieldName="Currency" VisibleIndex="2"
                    Width="120px">
                    <PropertiesComboBox TextField="Par_Description" ValueField="Par_Code" DataSourceID="sdsCurrency"
                        Width="145px" TextFormatString="{1}" ClientInstanceName="Currency" DropDownStyle="DropDownList"
                        IncrementalFilteringMode="StartsWith">
                        <Columns>
                            <dx:ListBoxColumn FieldName="Par_Code" Caption="Code" Width="65px" />
                            <dx:ListBoxColumn FieldName="Par_Description" Caption="Name" Width="200px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="2px">
                            <Paddings Padding="2px"></Paddings>
                        </ButtonStyle>
                    </PropertiesComboBox>
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="true" />
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataTextColumn Caption="Rate" VisibleIndex="6" FieldName="Rate" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="false" />
                    <EditCellStyle CssClass="Hidden">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Hidden">
                    </EditFormCaptionStyle>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Unit" VisibleIndex="6" FieldName="Unit" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="false" />
                    <EditCellStyle CssClass="Hidden">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Hidden">
                    </EditFormCaptionStyle>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(E) <br/> Material Unit Price " VisibleIndex="6" FieldName="Unit_Cost" Width="150px">
                    <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <EditCellStyle CssClass="Hidden">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Hidden">
                    </EditFormCaptionStyle>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(F) <br/> Scrap Weight " VisibleIndex="6" FieldName="Scrap_Weight" Width="150px">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <EditCellStyle CssClass="Hidden">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Hidden">
                    </EditFormCaptionStyle>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(G) <br/> Scrap Unit Price" VisibleIndex="6" FieldName="Scrap_Unit_Price" Width="150px">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <EditCellStyle CssClass="Hidden">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Hidden">
                    </EditFormCaptionStyle>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="((D * E) + (F * G)) <br/> Material Cost" VisibleIndex="6" FieldName="Amount"  Width="150px">
                    <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000000g>.<00..99g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditCellStyle CssClass="Amount">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Amount">
                    </EditFormCaptionStyle>
                    <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Material Cost (IDR)" VisibleIndex="6" FieldName="BaseAmount">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditCellStyle CssClass="Amount">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Amount">
                    </EditFormCaptionStyle>
                    <EditFormSettings Visible="false" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="6" FieldName="Register_By">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="6" FieldName="Register_Date">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="6" FieldName="Update_By">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="6" FieldName="Update_Date">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
            <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="320" />
            </SettingsPopup>
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header>
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
                <CommandColumnItem ForeColor="Orange">
                </CommandColumnItem>
            </Styles>
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
            <Templates>
                <EditForm>
                    <div style="padding: 15px 15px 15px 15px">
                        <dx:ContentControl ID="ContentControl1" runat="server">
                            <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                runat="server"></dx:ASPxGridViewTemplateReplacement>
                        </dx:ContentControl>
                    </div>
                    <div style="text-align: left; padding: 5px 5px 5px 15px">
                        <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                        <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                    </div>
                </EditForm>
            </Templates>
        </dx:ASPxGridView>
        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbkDrawingComplete" runat="server" ClientInstanceName="cbkDrawingComplete">
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
            <ClientSideEvents Init="MessageError" />
        </dx:ASPxCallback>
        <asp:SqlDataSource ID="sdsCategoryMaterial" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommandType="Text"></asp:SqlDataSource>
        <asp:SqlDataSource ID="sdsCurrency" runat="server" SelectCommand="SELECT Par_Code, Par_Description FROM Mst_Parameter WHERE Par_Group = 'Currency'"
            ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" SelectCommandType="Text">
        </asp:SqlDataSource>
        <dx:ASPxCallback ID="cbDS" runat="server" ClientInstanceName="cbDS">
        </dx:ASPxCallback>
        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
        </dx:ASPxGridViewExporter>
    </div>
</asp:Content>

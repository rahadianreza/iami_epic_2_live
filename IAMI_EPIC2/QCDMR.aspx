﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="QCDMR.aspx.vb" Inherits="IAMI_EPIC2.QCDMR" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };
        function MessageBox(s, e) {
            //alert(s.cpmessage);
            if (s.cbMessage == "Download Excel Successfully") {
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cbMessage == "Request Date To must be higher than Request Date From") {
                toastr.success(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

      
    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        .style4
        {
            width: 24px;
        }
        .style8
        {
            height: 38px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<table style="width: 100%; border: 1px solid black; height: 100px;">
<tr>
<td></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Name">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " colspan="3">
<%--.GetValue().toString()
cboProject.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0));
 cboGroup.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '| ');
                                    cboCommodity.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '| ' + '| ' );
                                    cboCommodityGroup.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '| ' + '| ' + '| ' );
                                    cboPartNo.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '| ' + '| ' + '| ' + '| ' );
--%>
                <%--<dx:ASPxComboBox ID="cboProjectType" runat="server" ClientInstanceName="cboProjectType"
                    Width="240px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1"  TextField="Description" ValueField="Code"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                    cboProject.PerformCallback(cboProjectType.GetValue().toString());
                                    cboGroup.PerformCallback(cboProjectType.GetValue().toString() + '| ');
                                    cboCommodity.PerformCallback(cboProjectType.GetValue().toString() + '| ' + '| ' );
                                    cboCommodityGroup.PerformCallback(cboProjectType.GetValue().toString() + '| ' + '| ' + '| ' );
                                    cboPartNo.PerformCallback(cboProjectType.GetValue().toString() + '| ' + '| ' + '| ' + '| ' );
                                   }"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>--%>
                        <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                            Font-Names="Segoe UI"  TextField="Project_Name" ValueField="Project_ID" TextFormatString="{1}"  
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                  cboGroup.PerformCallback(cboProject.GetValue().toString());
                                  cboCommodity.ClearItems();
                                  cboCommodityGroup.ClearItems();
                                  cboPartNo.ClearItems();
                         
                }" />        <%-- cboCommodity.PerformCallback(cboProject.GetValue().toString() + '| ');
                                  cboCommodityGroup.PerformCallback(cboProject.GetValue().toString() + '| '+ '| ');
                                  cboPartNo.PerformCallback(cboProject.GetValue().toString() + '| '+ '| '+ '| ');--%>
                            <Columns>
                                <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
<td class="style4">&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Commodity">
                        </dx:ASPxLabel>
                    </td>
<%--.GetValue().toString()
cboCommodity.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0)  + '|' + cboGroup.GetSelectedItem().GetColumnText(0) );
                                 cboCommodityGroup.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '| ');
                                 cboPartNo.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '| '+ '| ');--%>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxComboBox ID="cboCommodity" runat="server" ClientInstanceName="cboCommodity"
                            Font-Names="Segoe UI" TextField="Commodity" ValueField="Commodity" TextFormatString="{0}" OnCallback="cboCommodity_Callback"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                  cboCommodityGroup.PerformCallback(cboProject.GetValue().toString() + '|' + cboGroup.GetValue().toString() + '|' + cboCommodity.GetValue().toString());
                                  cboPartNo.ClearItems();
                                 
                    }" /> <%--cboPartNo.PerformCallback(cboProject.GetValue().toString() + '|' + cboGroup.GetValue().toString() + '|' + cboCommodity.GetValue().toString() + '| ' );--%>
                            <Columns>
                                <dx:ListBoxColumn Caption="Commodity" FieldName="Commodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td class="style4">&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Part No">
                        </dx:ASPxLabel>
                    </td>
<td style="padding: 0px 0px 0px 10px; width: 200px">
<%--cmbCountry.GetValue().toString()
cboPartNo.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '|' + cboCommodity.GetSelectedItem().GetColumnText(0) + '|' + cboCommodityGroup.GetSelectedItem().GetColumnText(0));
 --%>                       
                        <dx:ASPxComboBox ID="cboPartNo" runat="server" ClientInstanceName="cboPartNo"
                            Font-Names="Segoe UI" TextField="Part No" ValueField="Part_No" TextFormatString="{0}" OnCallback="cboPartNo_Callback"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                            <Columns>
                                <dx:ListBoxColumn Caption="PartNo" FieldName="Part_No" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td>&nbsp;</td>
</tr>
<tr style="height: 2px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td class="style4" align = "center">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Group">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 200px" colspan="3" >

<%--.GetValue().toString()
   cboGroup.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) );
                                  cboCommodity.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '| ');
                                  cboCommodityGroup.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '| '+ '| ');
                                  cboPartNo.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '| '+ '| '+ '| ');--%>
                        <dx:ASPxComboBox ID="cboGroup" runat="server" ClientInstanceName="cboGroup" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Group_ID" ValueField="Group_ID" OnCallback="cboGroup_Callback"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                 cboCommodity.PerformCallback(cboProject.GetValue().toString()  + '|' + cboGroup.GetValue().toString());
                                  cboCommodityGroup.ClearItems();
                                  cboPartNo.ClearItems();
                                
                            }" /> <%--cboCommodityGroup.PerformCallback(cboProject.GetValue().toString() + '|' + cboGroup.GetValue().toString() + '| ');
                                 cboPartNo.PerformCallback(cboProject.GetValue().toString() + '|' + cboGroup.GetValue().toString() + '| '+ '| ');--%>
                            <Columns>
                                <dx:ListBoxColumn Caption="Group ID" FieldName="Group_ID" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Sourcing Group">
                        </dx:ASPxLabel>
            </td>
<%--            .GetValue().toString()
             cboCommodityGroup.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '|' + cboCommodity.GetSelectedItem().GetColumnText(0));
                                  cboPartNo.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '|' + cboCommodity.GetSelectedItem().GetColumnText(0) + '| ' );--%>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxComboBox ID="cboCommodityGroup" runat="server" ClientInstanceName="cboCommodityGroup"
                            Font-Names="Segoe UI" TextField="Group_Comodity" 
                            ValueField="Group_Comodity" TextFormatString="{0}" OnCallback="cboCommodityGroup_Callback"
                                                Font-Size="9pt" Theme="Office2010Black" 
                            DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                EnableIncrementalFiltering="True" Height="25px" 
                            TabIndex="5">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                             cboPartNo.PerformCallback(cboProject.GetValue().toString() + '|' + cboGroup.GetValue().toString() + '|' + cboCommodity.GetValue().toString() + '|' + cboCommodityGroup.GetValue().toString());
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Sourcing Group" FieldName="Group_Comodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 200px">
                        &nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr>
<td style="padding: 20px 0px 0px 10px" class="style8" colspan="4">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnBack" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnShowData" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
                 
                            Grid.PerformCallback('load|' + cboProject.GetValue() + '|' + cboGroup.GetValue() + '|' + cboCommodity.GetValue() + '|' + cboCommodityGroup.GetValue() + '|' + cboPartNo.GetValue() );
                            GridTooling.PerformCallback('load|' + cboProject.GetValue() + '|' + cboGroup.GetValue() + '|' + cboCommodity.GetValue() + '|' + cboCommodityGroup.GetValue() + '|' + cboPartNo.GetValue() );
                        
                        }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnDownload" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
         
            </td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>

<%--<table style="width: 100%; border: 1px solid black; height: 100px;">
<tr>
<td></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Type">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " colspan="3">

                <dx:ASPxComboBox ID="cboProjectType" runat="server" ClientInstanceName="cboProjectType"
                    Width="240px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1"  TextField="Description" ValueField="Code"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                    cboProject.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0));
                                    cboGroup.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '| ');
                                    cboCommodity.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '| ' + '| ' );
                                    cboCommodityGroup.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '| ' + '| ' + '| ' );
                                    cboPartNo.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '| ' + '| ' + '| ' + '| ' );
                                   }"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
            </td>
<td class="style4">&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Group">
                        </dx:ASPxLabel>
                    </td>

<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxComboBox ID="cboGroup" runat="server" ClientInstanceName="cboGroup" Width="120px"
                            TabIndex="4" Font-Names="Segoe UI" TextField="Group_ID" ValueField="Group_ID"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                 cboCommodity.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0)  + '|' + cboGroup.GetSelectedItem().GetColumnText(0) );                                 
                                 cboCommodityGroup.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '| ');
                                 cboPartNo.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '| '+ '| ');
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Group ID" FieldName="Group_ID" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td class="style4">&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Commodity Group">
                        </dx:ASPxLabel>
                    </td>
<td style="padding: 0px 0px 0px 10px; width: 200px">
                        <dx:ASPxComboBox ID="cboCommodityGroup" runat="server" ClientInstanceName="cboCommodityGroup"
                            Font-Names="Segoe UI" TextField="Group_Comodity" 
                            ValueField="Group_Comodity" TextFormatString="{0}"
                                                Font-Size="9pt" Theme="Office2010Black" 
                            DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                EnableIncrementalFiltering="True" Height="25px" 
                            TabIndex="5">
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                          cboPartNo.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '|' + cboCommodity.GetSelectedItem().GetColumnText(0) + '|' + cboCommodityGroup.GetSelectedItem().GetColumnText(0));
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Group Comodity" FieldName="Group_Comodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox></td>
<td>&nbsp;</td>
</tr>
<tr style="height: 2px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td class="style4" align = "center">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; width: 100px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Name">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 200px" colspan="3" >


                        <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                            Font-Names="Segoe UI" TextField="Project_Name" ValueField="Project_ID" TextFormatString="{1}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                            alert(cboProject.GetSelectedItem().GetColumnText(0));
                                  cboGroup.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) );
                                  cboCommodity.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '| ');
                                  Group_Comodity.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '| '+ '| ');
                                  cboPartNo.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '| '+ '| '+ '| ');
                            }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
            </td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Commodity">
                        </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxComboBox ID="cboCommodity" runat="server" ClientInstanceName="cboCommodity"
                            Font-Names="Segoe UI" TextField="Commodity" ValueField="Commodity" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                  Group_Comodity.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '|' + cboCommodity.GetSelectedItem().GetColumnText(0));
                                  cboPartNo.PerformCallback(cboProjectType.GetSelectedItem().GetColumnText(0) + '|' + cboProject.GetSelectedItem().GetColumnText(0) + '|' + cboGroup.GetSelectedItem().GetColumnText(0) + '|' + cboCommodity.GetSelectedItem().GetColumnText(0) + '| ' );
                    }" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Commodity" FieldName="Commodity" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Part No">
                        </dx:ASPxLabel>
                    </td>
<td  style="padding: 0px 0px 0px 10px; width: 200px">
                        <dx:ASPxComboBox ID="cboPartNo" runat="server" ClientInstanceName="cboPartNo"
                            Font-Names="Segoe UI" TextField="Part No" ValueField="Part_No" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="5">
                            <Columns>
                                <dx:ListBoxColumn Caption="PartNo" FieldName="Part_No" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
<td>&nbsp;</td>
</tr>

<tr>
<td style="padding: 20px 0px 0px 10px" class="style8" colspan="4">
                <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnShowData" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
                 
                            Grid.PerformCallback('load|' + cboProject.GetValue() + '|' + cboGroup.GetValue() + '|' + cboCommodity.GetValue() + '|' + Group_Comodity.GetValue() + '|' + cboPartNo.GetValue() );
                        
                        }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
     
                <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnDownload" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
         
            </td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
<td class="style8"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>--%>

    <div style="padding: 5px 0px 5px 0px">
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"          
            SelectCommand=" SELECT DISTINCT A.Project_ID, 
				B.Project_Name 
			FROM Gate_III_CS_QCDMR A
			INNER JOIN Proj_Header B ON A.Project_ID = B.Project_ID">
        </asp:SqlDataSource>
 <%-- SelectCommand="Select RTRIM(Par_Code) As Code, RTRIM(Par_Description) As Description From Mst_Parameter Where Par_Group = 'ProjectType'">--%>
      <%--   <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommand="select UserID, UserName from UserSetup">
        </asp:SqlDataSource>--%>
       <%--     <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            
            SelectCommand="SELECT GS.Supplier_Code, MS.Supplier_Name FROM Drawing_Acceptance DA
            INNER JOIN Gate_I_CS_Bidders_List_N_Supplier_Information GS ON DA.Project_ID = GS.Project_ID And DA.Group_ID = GS.Group_ID AND DA.Commodity = GS.Commodity
            INNER JOIN Mst_Supplier MS ON GS.Supplier_Code = MS.Supplier_Code 
            WHERE DA.Project_ID = @ProjectCode AND DA.Group_ID = @Group AND DA.Commodity = @Commodity">
                <SelectParameters>
                    <asp:ControlParameter ControlID="cboProject" Name="ProjectCode" 
                        PropertyName="Value" />
                    <asp:ControlParameter ControlID="cboGroup" Name="Group" PropertyName="Value" />
                    <asp:ControlParameter ControlID="cboCommodity" Name="Commodity" 
                        PropertyName="Value" />
                </SelectParameters>
        </asp:SqlDataSource>--%>
        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
            <ClientSideEvents CallbackComplete="SetCode" />
        </dx:ASPxCallback>
        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
        </dx:ASPxGridViewExporter>


        
    </div>
    <div style="padding: 5px 0px 5px 0px">
        <table style="width: 100%;">
            <tr>
                <td>
    
                                        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                                            EnableTheming="True" Theme="Office2010Black" Width="100%" 
                                            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="ID" OnStartRowEditing="Grid_StartRowEditing"
                                            OnAfterPerformCallback="Grid_AfterPerformCallback">
                                            <ClientSideEvents EndCallback="OnEndCallback" ></ClientSideEvents>
                                            <Columns>                           
                                                   <dx:GridViewCommandColumn VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="false" 
                                            ShowNewButtonInHeader="false" ShowClearFilterButton="true" Width="100px">
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                    <Paddings PaddingLeft="6px" />
                                                </HeaderStyle>
                                        </dx:GridViewCommandColumn>
                                          <dx:GridViewDataTextColumn Caption="seq no group" FieldName="seqnogroup" VisibleIndex="1"
                                                Width="0px"  EditFormCaptionStyle-Paddings-Padding="5"  EditFormSettings-Visible="False"  >
                       
                                                <Settings AutoFilterCondition="Contains" />
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                    <Paddings PaddingLeft="6px" />
                                                </HeaderStyle>
                                            </dx:GridViewDataTextColumn>
                                 
                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" VisibleIndex="2"
                                                        Width="0px"  EditFormCaptionStyle-Paddings-Padding="5"  EditFormSettings-Visible="False"  >
                       
                                                        <Settings AutoFilterCondition="Contains" />

                                                    <EditFormCaptionStyle>
                                                    <Paddings Padding="5px"></Paddings>
                                                    </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                     <dx:GridViewDataTextColumn Caption="Aspect" FieldName="Aspect" VisibleIndex="3"
                                                        Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                        <PropertiesTextEdit Width="150px"  >
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                    <EditFormCaptionStyle>
                                                    <Paddings Padding="5px"></Paddings>
                                                    </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                     <dx:GridViewDataTextColumn Caption="Item" FieldName="Item" VisibleIndex="4"
                                                        Width="370px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                            <PropertiesTextEdit Width="370px"  >
                                                                </PropertiesTextEdit>
                                                        <Settings AutoFilterCondition="Contains" />

                                                    <EditFormCaptionStyle>
                                                    <Paddings Padding="5px"></Paddings>
                                                    </EditFormCaptionStyle>

                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                            <Paddings PaddingLeft="6px" />
                                                        </HeaderStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewBandColumn Caption="Vendor Candidate" VisibleIndex="5" Name="VendorCandidate"  Visible="false" HeaderStyle-HorizontalAlign="Center" >

                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="vendorCode1" FieldName="VendorCode1" VisibleIndex="6"
                                                                Width="0px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                                <PropertiesTextEdit Width="150px"  >
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />

                                                                <EditFormCaptionStyle>
                                                                <Paddings Padding="5px"></Paddings>
                                                                </EditFormCaptionStyle>

                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="pValue1" FieldName="pValue1" VisibleIndex="7"
                                                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                            
                                                                 <PropertiesTextEdit DisplayFormatString="#,##0.00"  Style-HorizontalAlign="Right" Width="80px"  MaxLength="7">
                                                                   <MaskSettings Mask="<0..1000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>

                                                                <dx:GridViewDataTextColumn Caption="pValueSubTotal1" FieldName="pValueSubTotal1" VisibleIndex="8"
                                                                Width="80px"  EditFormCaptionStyle-Paddings-Padding="5"  EditFormSettings-Visible="False"  >
                                                                <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                                                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>
                                                   

                                                                <dx:GridViewDataTextColumn Caption="vendorCode2" FieldName="VendorCode2" VisibleIndex="9"
                                                                Width="0px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                                <PropertiesTextEdit Width="150px"  >
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />

                                                                <EditFormCaptionStyle>
                                                                <Paddings Padding="5px"></Paddings>
                                                                </EditFormCaptionStyle>

                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                                                                </dx:GridViewDataTextColumn>

                                                                 <dx:GridViewDataTextColumn Caption="pValue2" FieldName="pValue2" VisibleIndex="10"
                                                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                            
                                                                 <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right" Width="80px"  MaxLength="7">
                                                                   <MaskSettings Mask="<0..1000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>
                                                             
                                                                 <dx:GridViewDataTextColumn Caption="pValueSubTotal2" FieldName="pValueSubTotal2" VisibleIndex="11"
                                                                Width="80px"  EditFormCaptionStyle-Paddings-Padding="5"  EditFormSettings-Visible="False"  >
                                                                <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                                                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                              <%--  </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="pValueSubTotal2" FieldName="pValueSubTotal2" VisibleIndex="11"
                                                                Width="80px"  EditFormCaptionStyle-Paddings-Padding="5"  EditFormSettings-Visible="False"  >
                                                                <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>--%>
                                                    

                                                                <dx:GridViewDataTextColumn Caption="vendorCode3" FieldName="VendorCode3" VisibleIndex="12"
                                                                Width="0px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                                <PropertiesTextEdit Width="150px"  >
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />

                                                                <EditFormCaptionStyle>
                                                                <Paddings Padding="5px"></Paddings>
                                                                </EditFormCaptionStyle>

                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                                                                </dx:GridViewDataTextColumn>

                                                                     <dx:GridViewDataTextColumn Caption="pValue3" FieldName="pValue3" VisibleIndex="13"
                                                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                            
                                                                 <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right" Width="80px"  MaxLength="7">
                                                                   <MaskSettings Mask="<0..1000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>
                                                             
                                                               <dx:GridViewDataTextColumn Caption="pValueSubTotal3" FieldName="pValueSubTotal3" VisibleIndex="14"
                                                                Width="80px"  EditFormCaptionStyle-Paddings-Padding="5"  EditFormSettings-Visible="False"  >
                                                                <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                                                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                                                                </dx:GridViewDataTextColumn>
                                                               
                                                              <%--  <dx:GridViewDataTextColumn Caption="pValueSubTotal3" FieldName="pValueSubTotal3" VisibleIndex="14"
                                                                Width="80px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                                <PropertiesTextEdit Width="150px" MaxLength="18"  DisplayFormatString="#,###">
                                                                <MaskSettings Mask="<0..100000000000g>"  IncludeLiterals="DecimalSymbol"/>
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>--%>

                                                    
                                                                <dx:GridViewDataTextColumn Caption="vendorCode4" FieldName="VendorCode4" VisibleIndex="16"
                                                                Width="0px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                                <PropertiesTextEdit Width="150px"  >
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />

                                                                <EditFormCaptionStyle>
                                                                <Paddings Padding="5px"></Paddings>
                                                                </EditFormCaptionStyle>

                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                                                                </dx:GridViewDataTextColumn>

                                                                    <dx:GridViewDataTextColumn Caption="pValue4" FieldName="pValue4" VisibleIndex="17"
                                                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                            
                                                                <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right" Width="80px"  MaxLength="7">
                                                                   <MaskSettings Mask="<0..1000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>
                                                              
                                                               <dx:GridViewDataTextColumn Caption="pValueSubTotal4" FieldName="pValueSubTotal4" VisibleIndex="18"
                                                                Width="80px"  EditFormCaptionStyle-Paddings-Padding="5"  EditFormSettings-Visible="False"  >
                                                                <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                                                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>
                                                    
                                                    
                                                                <dx:GridViewDataTextColumn Caption="vendorCode5" FieldName="VendorCode5" VisibleIndex="20"
                                                                Width="0px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                                <PropertiesTextEdit Width="150px"  >
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />

                                                                <EditFormCaptionStyle>
                                                                <Paddings Padding="5px"></Paddings>
                                                                </EditFormCaptionStyle>

                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                                                                </dx:GridViewDataTextColumn>

                                                                     <dx:GridViewDataTextColumn Caption="pValue5" FieldName="pValue5" VisibleIndex="21"
                                                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                            
                                                                <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right" Width="80px"  MaxLength="7">
                                                                   <MaskSettings Mask="<0..1000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>
                                                             
                                                                <dx:GridViewDataTextColumn Caption="pValueSubTotal5" FieldName="pValueSubTotal5" VisibleIndex="22"
                                                                Width="80px"  EditFormCaptionStyle-Paddings-Padding="5"  EditFormSettings-Visible="False"  >
                                                                <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                                                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>


                                                                <dx:GridViewDataTextColumn Caption="vendorCode6" FieldName="VendorCode6" VisibleIndex="24"
                                                                Width="0px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                                <PropertiesTextEdit Width="150px"  >
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />

                                                                <EditFormCaptionStyle>
                                                                <Paddings Padding="5px"></Paddings>
                                                                </EditFormCaptionStyle>

                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                                                                </dx:GridViewDataTextColumn>

                                                                  <dx:GridViewDataTextColumn Caption="pValue6" FieldName="pValue6" VisibleIndex="25"
                                                                Width="150px"  EditFormCaptionStyle-Paddings-Padding="5"   >
                                                            
                                                                  <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right" Width="80px"  MaxLength="7">
                                                                   <MaskSettings Mask="<0..1000g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>
                                                              
                                                                 <dx:GridViewDataTextColumn Caption="pValueSubTotal6" FieldName="pValueSubTotal6" VisibleIndex="26"
                                                                Width="80px"  EditFormCaptionStyle-Paddings-Padding="5"  EditFormSettings-Visible="False"  >
                                                                <PropertiesTextEdit DisplayFormatString="#,##0.00" Style-HorizontalAlign="Right">
                                                                <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                                                                </PropertiesTextEdit>
                                                                <Settings AutoFilterCondition="Contains" />
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                                                <Paddings PaddingLeft="6px" />
                                                                </HeaderStyle>
                       
                                                                </dx:GridViewDataTextColumn>

                                                    </Columns>
                                                    </dx:GridViewBandColumn>
                                            </Columns>
                                                                    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                                                                    <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm"/>
                                                                    <SettingsPager Mode="ShowPager" PageSize="16" AlwaysShowPager="true"></SettingsPager>
                                                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="390" HorizontalScrollBarMode="Auto" />
                                                                    <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>

                                                                    <SettingsPopup>
                                                                        <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
                                                                    </SettingsPopup>

                                                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                                                        <Header>
                                                                            <Paddings Padding="2px"></Paddings>
                                                                        </Header>

                                                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                                                        </EditFormColumnCaption>
                                                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                                                    </Styles>
                    
                                                                    <Templates>
                                                                        <EditForm>
                                                                            <div style="padding: 15px 15px 15px 15px">
                                                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                                                    <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                                                                        runat="server">
                                                                                    </dx:ASPxGridViewTemplateReplacement>
                                                                                </dx:ContentControl>
                                                                            </div>
                                                                            <div style="text-align: left; padding: 5px 5px 5px 15px">
                                                                                <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                                                                                    runat="server">
                                                                                </dx:ASPxGridViewTemplateReplacement>
                                                                                <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                                                                                    runat="server">
                                                                                </dx:ASPxGridViewTemplateReplacement>
                                                                            </div>
                                                                        </EditForm>
                                                                    </Templates>
                                        </dx:ASPxGridView>

                                  
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports System.Data
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data.OleDb
Imports System.Web.UI.WebControls.Style
Imports DevExpress.Web.ASPxUploadControl
Imports OfficeOpenXml
Imports DevExpress.Web.ASPxDataView

Public Class PRListItem
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String
    Dim fRefresh As Boolean = False
    Dim ls_GroupItem As String = ""
    Dim ls_Category As String = ""
    Dim showTop As Integer = 15
#End Region

#Region "Initialization"

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|B010", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnDraft.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnDraft, btnDraft.GetType(), "btnDraft", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, ASPxDataView1)
        End If
    End Sub

    Protected Sub cb_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim cb As ASPxCheckBox = CType(sender, ASPxCheckBox)
        Dim container As DataViewItemTemplateContainer = CType(cb.NamingContainer, DataViewItemTemplateContainer)
        Dim data As ASPxDataView = CType(ASPxDataView1, ASPxDataView)
        cb.ClientInstanceName = "" + DataBinder.Eval(container.DataItem, "Material_No")
        cb.ClientSideEvents.CheckedChanged = String.Format("function(s,e) {{ hf1.Set('{0}', s.GetChecked());  }}", cb.ClientInstanceName)
        If hf1.Contains(cb.ClientInstanceName) Then cb.Checked = CBool(hf1.[Get](cb.ClientInstanceName))

        Dim Key As String = DataBinder.Eval(container.DataItem, "Material_No")
        Dim s As String = DataBinder.Eval(container.DataItem, "Selected")


        If s = "1" Then
            If hf1.Contains(cb.ClientInstanceName) Then
                cb.Checked = CBool(hf1.[Get](cb.ClientInstanceName))

            Else
                hf1.Set(Key, True)
            End If
            'If CBool(hf1.[Get](cb.ClientInstanceName)) Then hf1.Set(Key, False)
            'hf1.Set(Key, True)
            'Else
            '    If hf1.Contains(cb.ClientInstanceName) Then
            '        hf1.Set(Key, False)
            '    Else
            '        cb.Checked = CBool(hf1.[Get](cb.ClientInstanceName))
            '    End If
            'cb.ClientSideEvents.Init.Contains(String.Format("function (s,e){{s.SetChecked(hf.Get('{0}')); }}", cb.ClientInstanceName))

        End If

        'Session("checkboxState") = hf1.Contains(cb.ClientInstanceName)
        If hf1.Contains(cb.ClientInstanceName) Then cb.Checked = CBool(hf1.[Get](cb.ClientInstanceName))

    End Sub

    Protected Sub image_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim imageBinary As ASPxBinaryImage = CType(sender, ASPxBinaryImage)
        Dim container As DataViewItemTemplateContainer = CType(imageBinary.NamingContainer, DataViewItemTemplateContainer)
        Dim MaterialNo = DataBinder.Eval(container.DataItem, "Material_No")
        If MaterialNo <> "" Then
            'Dim A As String = 
            imageBinary.ClientSideEvents.Click = String.Format("function (s,e) {{OnMoreInfoClick(s,{0}); popup_Shown({0}); }}", "'" & MaterialNo & "'")
        End If

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboGroupItem)
            End Try
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "PURCHASE REQUEST ITEM LIST"
        pUser = Session("user")
        'Dim Parentgroup As String = Split(e.Parameter, "|")(1)
        'Dim errmsg As String = ""

        If IsPostBack Then
            If Session("Back") = True Then
                Exit Sub

            End If

            If gs_GeneratePRNo = Nothing Then
                gs_GeneratePRNo = ""
            End If

            gs_FilterGroup = IIf(cboGroupItem.Value = Nothing, "ALL", cboGroupItem.Value)
            gs_FilterCategory = IIf(cboCategory.Value = Nothing, "ALL", cboCategory.Value)

            If gs_Modify = True Then
                'Dim ds As New DataSet
                up_GridLoad(gs_FilterPRType, gs_FilterGroup, gs_FilterCategory, 1, gs_GeneratePRNo, showTop)
                's = ClsPRListDB.uf_ModifyMaterial(gs_GeneratePRNo, gs_FilterPRType)
            Else
                up_GridLoad(gs_FilterPRType, gs_FilterGroup, gs_FilterCategory, 0, gs_GeneratePRNo, showTop)
            End If

            'ASPxDataView1.JSProperties("cpCount") = ASPxDataView1.Items.Count()
        End If


        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            'cbDraft.JSProperties("cpMessage") = "not post"
            'cboGroupItem.SelectedIndex = 0
            ' Dim PRType As String = cboPRType.SelectedIndex = gs_FilterPRTypeIndex - 1
            ' up_FillComboCategory(cboGroupItem, PRType, "GroupItem", "", "")
            cboGroupItem.SelectedIndex = 0
            cboCategory.SelectedIndex = 0
            cboLastSupplier.SelectedIndex = 0

            If gs_Modify = True Then
                cboPRType.SelectedIndex = gs_FilterPRTypeIndex - 1
                ScriptManager.RegisterStartupScript(cboPRType, cboPRType.GetType(), "cboPRType", "cboPRType.SetEnabled(false);", True)
                'cboPRType.ReadOnly = True
                Dim Parent As String = gs_FilterPRType

                up_FillComboCategory(cboGroupItem, Parent, "GroupItem", "", "")
                up_FillComboCategory(cboCategory, "ALL", "Category", Parent, "")

                gs_FilterGroup = "ALL"
                gs_FilterCategory = "ALL"


                up_GridLoad(gs_FilterPRType, gs_FilterGroup, gs_FilterCategory, 1, gs_GeneratePRNo, showTop)
                ASPxDataView1.JSProperties("cpCount") = ASPxDataView1.Items.Count()
                'lblCount.Text = 50
                'Dim ds As New DataSet
                'ds = ClsPRListDB.uf_ModifyMaterial(gs_GeneratePRNo, gs_FilterPRType)

                'ASPxDataView1.DataSource = ds
                'ASPxDataView1.DataBind()
                'grid.Columns.Item(6).Visible = False

            End If
        End If

    End Sub
#End Region

#Region "Procedure"
    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        ASPxDataView1.JSProperties("cp_message") = ErrMsg
        ASPxDataView1.JSProperties("cp_type") = msgType
        ASPxDataView1.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_GridLoad(pPRTypeCd As String, pGroupItem As String, pCategory As String, ByVal pTypeTrx As Integer, ByVal pPRNo As String, ByVal pShow As Integer)
        Dim ErrMsg As String = ""
        Dim vGroupItem As String = ""
        Dim vCategory As String = ""
        Dim vLastSupplier As String = ""
        Dim vPRType As String = ""
        Dim vKeyWord As String = ""
        Dim ds As New DataSet

        If cboGroupItem.Text <> "" Then
            vGroupItem = pGroupItem
        End If

        If cboCategory.Text <> "" Then
            vCategory = pCategory
        End If

        If cboLastSupplier.Text <> "" Then
            vLastSupplier = cboLastSupplier.SelectedItem.GetValue("Code").ToString()
        End If



        'If cboPRType.Text <> "" Then
        '    vPRType = cboPRType.SelectedItem.GetValue("Code").ToString()
        '    gs_FilterPRType = vPRType
        'End If

        If txtKeyword.Text <> "" Then
            vKeyWord = Trim(txtKeyword.Text)
        End If

        ds = ClsPRListDB.GetListItem(vGroupItem, vCategory, vLastSupplier, vKeyWord, pPRTypeCd, pTypeTrx, pPRNo, pShow, ErrMsg)

        If ErrMsg = "" Then
            ASPxDataView1.DataSource = ds
            ASPxDataView1.DataBind()

            'ASPxDataView1.JSProperties("cpCount") = ASPxDataView1.Items.Count()    'ds.Tables(0).Rows.Count

            'lblCount.Text = ds.Tables(0).Rows.Count
            'grid.Columns.Item(6).Visible = False
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_FillComboCategory(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, pParent As String, pGroup As String, pPRType As String, Optional ByRef pErr As String = "")
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim vParent As String
        Dim vGroup As String

        vParent = pParent
        vGroup = pGroup

        ds = ClsPRListDB.GetDataCategory(vParent, vGroup, pPRType)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub
#End Region

#Region "Control Event"
    Protected Sub ASPxDataView1_CustomCallback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxDataView1.CustomCallback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim ds As New DataSet

        If pFunction = "refresh" Then
            'If gs_Modify = True Then

            'If gs_GeneratePRNo = Nothing Then
            'gs_GeneratePRNo = ""
            ' End If
            ' ds = ClsPRListDB.uf_ModifyMaterial(gs_GeneratePRNo, gs_FilterPRType)
            'Else
            ds = ClsPRListDB.GetListItem("", "", "", "", "", 0, "", 0, "")
            'End If

            ASPxDataView1.DataSource = ds
            ASPxDataView1.DataBind()

            'fRefresh = True
        ElseIf pFunction = "showMore" Then
            gs_FilterPRType = Split(e.Parameter, "|")(1)
            gs_FilterGroup = Split(e.Parameter, "|")(2)
            gs_FilterCategory = Split(e.Parameter, "|")(3)

            gs_FilterPRTypeIndex = cboPRType.SelectedIndex

            If Not IsNothing(Session("ShowTop")) Then
                Session("ShowTop") = Session("ShowTop") + 15
            Else
                Session("showTop") = 15
            End If


            If gs_Modify = True Then
                up_GridLoad(gs_FilterPRType, gs_FilterGroup, gs_FilterCategory, 1, gs_GeneratePRNo, Session("ShowTop"))
            Else
                up_GridLoad(gs_FilterPRType, gs_FilterGroup, gs_FilterCategory, 0, gs_GeneratePRNo, Session("ShowTop"))

            End If

            ASPxDataView1.JSProperties("cpCount") = ASPxDataView1.Items.Count()

        Else 'dari pr detail / show data button click
            gs_FilterPRType = Split(e.Parameter, "|")(1)
            gs_FilterGroup = Split(e.Parameter, "|")(2)
            gs_FilterCategory = Split(e.Parameter, "|")(3)
            gs_FilterPRTypeIndex = cboPRType.SelectedIndex

            If gs_Modify = True Then

                up_GridLoad(gs_FilterPRType, gs_FilterGroup, gs_FilterCategory, 1, gs_GeneratePRNo, showTop) 'top 25
            Else
                up_GridLoad(gs_FilterPRType, gs_FilterGroup, gs_FilterCategory, 0, gs_GeneratePRNo, showTop) 'top 25

            End If

            Session("ShowTop") = showTop

            ASPxDataView1.JSProperties("cpCount") = ASPxDataView1.Items.Count()

        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Session("Back") = True
        Session("getID") = Request.QueryString("ID")

        If Request.QueryString("ID") <> "" Or Request.QueryString("MatNo") <> "" Then
            Response.Redirect("~/PRListDetail.aspx?ID=" + Session("getID"))
        Else
            Response.Redirect("~/PRList.aspx")
        End If

    End Sub

    Protected Sub callbackPanel_Callback(ByVal source As Object, ByVal e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase)
        Dim MaterialNo = e.Parameter
        Dim ds As New DataSet
        ds = ClsPRListDB.ShowDetailItems(MaterialNo, "")

        If ds.Tables(0).Rows.Count > 0 Then
            txtMaterialNo.Text = MaterialNo
            txtDescription.Text = ds.Tables(0).Rows(0)("Description").ToString
            txtSpecification.Text = ds.Tables(0).Rows(0)("Specification").ToString
            txtPRType.Text = ds.Tables(0).Rows(0)("PRType").ToString
            txtGroupItem.Text = ds.Tables(0).Rows(0)("GroupItem").ToString
            txtCategoryItem.Text = ds.Tables(0).Rows(0)("CategoryItem").ToString
            txtUOM.Text = ds.Tables(0).Rows(0)("UOM").ToString
            txtIAPrice.Text = ds.Tables(0).Rows(0)("LastIAPrice").ToString
            dtLastIADate.Text = ds.Tables(0).Rows(0)("LastIADate").ToString
            txtLastSupplier.Text = ds.Tables(0).Rows(0)("LastSupplier").ToString

        End If

    End Sub

    Private Sub cboCategory_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCategory.Callback
        Dim Parentgroup As String = Split(e.Parameter, "|")(1)
        Dim PRType As String = Split(e.Parameter, "|")(2)
        Dim errmsg As String = ""

        up_FillComboCategory(cboCategory, Parentgroup, "Category", PRType, errmsg)
        cboCategory.SelectedIndex = 0
    End Sub

    Private Sub cboGroupItem_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupItem.Callback
        Dim Parentgroup As String = Split(e.Parameter, "|")(1)
        Dim errmsg As String = ""

        up_FillComboCategory(cboGroupItem, Parentgroup, "GroupItem", "", errmsg)
        cboGroupItem.SelectedIndex = 0
    End Sub

    Protected Sub cbDraft_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbDraft.Callback
        Dim MatNo As String = ""
        For Each item In hf1
            'lisbox.Items.Add(item.Key)
            'lisbox.Items.Remove(item.Value)
            Dim ds As New DataSet
            If gs_Modify = True Then
                ds = ClsPRListDB.GetListItem_CheckExist(gs_FilterGroup, gs_FilterCategory, IIf(cboLastSupplier.Text <> "", cboLastSupplier.SelectedItem.GetValue("Code").ToString(), "ALL"), IIf(txtKeyword.Text <> "", Trim(txtKeyword.Text), ""), gs_FilterPRType, 1, gs_GeneratePRNo, item.Key, Session("ShowTop"), "")
            Else
                ds = ClsPRListDB.GetListItem_CheckExist(gs_FilterGroup, gs_FilterCategory, IIf(cboLastSupplier.Text <> "", cboLastSupplier.SelectedItem.GetValue("Code").ToString(), "ALL"), IIf(txtKeyword.Text <> "", Trim(txtKeyword.Text), ""), gs_FilterPRType, 0, gs_GeneratePRNo, item.Key, Session("ShowTop"), "")
            End If

            If ds.Tables(0).Rows.Count > 0 Then
                Dim MaterialNoDB = ds.Tables(0).Rows(0)("Material_No")
                Dim SelectedDB = IIf(ds.Tables(0).Rows(0)("Selected") = "1", True, False)
                If item.Value = True Then
                    MatNo = MatNo & item.Key & ","
                End If

            End If

        Next

        If MatNo.Length <= 0 Then
            cbDraft.JSProperties("cpMessage") = "No Data to Draft, Please Select Material No !"
            Exit Sub
        Else
            cbDraft.JSProperties("cpMessage") = MatNo.Remove(MatNo.Length - 1)
        End If
    End Sub

#End Region

End Class
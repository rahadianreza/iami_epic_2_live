﻿Imports DevExpress.XtraPrinting
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports OfficeOpenXml

Public Class ItemListSourcing
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            up_GridLoad("0")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("I030")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            cboProjectType.SelectedIndex = 0
            cboProject.SelectedIndex = 0
            up_FillComboProjectType(cboProjectType, statusAdmin, pUser, "ProjectType")
            up_FillComboProject(cboProject, statusAdmin, pUser, cboProjectType.Value)
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
        End If
        fRefresh = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet

        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else

            up_GridLoad("1")
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsItemListSourcingDB.getHeaderGrid(cboProject.Value, pUser)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
            addVariant()
            End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Control Event"
    Private Sub up_FillComboProjectType(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                            pAdminStatus As String, pUserID As String, _
                            Optional ByRef pGroup As String = "", _
                            Optional ByRef pParent As String = "", _
                            Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboOnlyPT01PT02(pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_Excel()
        up_GridLoad("1")

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ItemListSourcing" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Response.Redirect("UploadItemListSourcing.aspx")
    End Sub

    Public Sub addVariant()
        Dim ds As DataSet = clsPRJListDB.getVariant(cboProject.Value)

        Dim colCheck As Object = Grid.Columns("Variant")
        If colCheck IsNot Nothing Then
            Return
        End If

        Dim bandColumn As GridViewBandColumn = New GridViewBandColumn()
        bandColumn.Caption = "Variant"
        bandColumn.VisibleIndex = 15

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim col As GridViewDataTextColumn = New GridViewDataTextColumn()
            col.Caption = ds.Tables(0).Rows(i)(0).ToString()
            col.FieldName = "Variant" + CStr(i + 1)
            bandColumn.Columns.Add(col)

        Next

        If ds.Tables(0).Rows.Count > 0 Then
            Grid.Columns.Add(bandColumn)
        End If

        'Grid.Columns.Add(bandColumn)
    End Sub

    Public Sub EPL_Excel(prjtype As String)
        Dim rowstart As Integer = 18
        Dim dsEPL As DataSet
        Dim dsDataEPL As DataSet

        Using exl As ExcelPackage = New ExcelPackage()
            Dim ws As ExcelWorksheet = exl.Workbook.Worksheets.Add("Sheet1")
            ws.Cells.Style.Font.Size = 11
            If prjtype <> "PT01" Then
                ws.Cells("B4").Value = "No"
                ws.Cells("C4").Value = "GROUP"
                ws.Cells("D4").Value = "COMODITY"
                ws.Cells("E4").Value = "UPC"
                ws.Cells("F4").Value = "FNA"
                ws.Cells("G4").Value = "DTL UPC"
                ws.Cells("H4").Value = "DTL FNA"
                ws.Cells("I4").Value = "USG/SEQ"
                ws.Cells("J4").Value = "LVL"
                ws.Cells("K4").Value = "HAND"
                ws.Cells("L4").Value = "D/C"
                ws.Cells("M4").Value = "DWG NO"
                ws.Cells("N4").Value = "PART NO"
                ws.Cells("O4").Value = "PART NO 9 DIGIT"
                ws.Cells("P4").Value = "PART NAME"
                ws.Cells("Q4").Value = "QTY"
                ws.Cells("R4").Value = "VARIANT"
                ws.Cells("S4").Value = "PIC"
                ws.Cells("T4").Value = "GUIDE PRICE"
                ws.Cells("U4").Value = "CURRENCY"
                ws.Cells("V4").Value = "JPN SUPLIER"
                ws.Cells("W4").Value = "DRAWING RECEIVE"
                ws.Cells("X4").Value = "LATEST COST REPORT"
                ws.Cells("Y4").Value = "PART NO OES"
                ws.Cells("Z4").Value = "PURCHASING GROUP"

                For bb As Integer = 2 To 27
                    If bb = 18 Then
                        'nothing
                        ws.Cells(4, bb, 5, bb).Style.Border.Left.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(4, bb, 5, bb).Style.Border.Left.Color.SetColor(System.Drawing.Color.Black)
                        ws.Cells(4, bb, 5, bb).Style.Border.Right.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(4, bb, 5, bb).Style.Border.Right.Color.SetColor(System.Drawing.Color.Black)
                        ws.Cells(4, bb, 5, bb).Style.Border.Top.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(4, bb, 5, bb).Style.Border.Top.Color.SetColor(System.Drawing.Color.Black)
                        ws.Cells(4, bb, 5, bb).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(4, bb, 5, bb).Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black)
                    Else
                        ws.Cells(4, bb, 5, bb).Merge = True
                        ws.Cells(4, bb, 5, bb).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                        ws.Cells(4, bb, 5, bb).Style.Border.Left.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(4, bb, 5, bb).Style.Border.Left.Color.SetColor(System.Drawing.Color.Black)
                        ws.Cells(4, bb, 5, bb).Style.Border.Right.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(4, bb, 5, bb).Style.Border.Right.Color.SetColor(System.Drawing.Color.Black)
                        ws.Cells(4, bb, 5, bb).Style.Border.Top.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(4, bb, 5, bb).Style.Border.Top.Color.SetColor(System.Drawing.Color.Black)
                        ws.Cells(4, bb, 5, bb).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(4, bb, 5, bb).Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black)
                    End If
                Next
            Else
                ws.Cells("B4").Value = "No"
                ws.Cells("C4").Value = "GROUP"
                ws.Cells("D4").Value = "COMODITY"
                ws.Cells("E4").Value = "UPC"
                ws.Cells("F4").Value = "FNA"
                ws.Cells("G4").Value = "DTL UPC"
                ws.Cells("H4").Value = "DTL FNA"
                ws.Cells("I4").Value = "USG/SEQ"
                ws.Cells("J4").Value = "LVL"
                ws.Cells("K4").Value = "HAND"
                ws.Cells("L4").Value = "D/C"
                ws.Cells("M4").Value = "DWG NO"
                ws.Cells("N4").Value = "PART NO"
                ws.Cells("O4").Value = "PART NO 9 DIGIT"
                ws.Cells("P4").Value = "PART NAME"
                ws.Cells("Q4").Value = "QTY"

                dsEPL = clsItemListSourcingDB.getVariantPT01(cboProject.Value)
                dsDataEPL = clsItemListSourcingDB.getDataEPL(cboProject.Value, pUser)

                If dsEPL.Tables(0).Rows.Count > 0 Then

                    Dim startdata As Integer = 6
                    For xn As Integer = 0 To dsDataEPL.Tables(0).Rows.Count - 1
                        ws.Cells(startdata + xn, 2).Value = xn + 1
                        ws.Cells(startdata + xn, 5).Value = dsDataEPL.Tables(0).Rows(xn)("UPC")
                        ws.Cells(startdata + xn, 6).Value = dsDataEPL.Tables(0).Rows(xn)("FNA")
                        ws.Cells(startdata + xn, 7).Value = dsDataEPL.Tables(0).Rows(xn)("Dtl_Upc")
                        ws.Cells(startdata + xn, 8).Value = dsDataEPL.Tables(0).Rows(xn)("Dtl_Fna")
                        ws.Cells(startdata + xn, 9).Value = dsDataEPL.Tables(0).Rows(xn)("Usg_Seq")
                        ws.Cells(startdata + xn, 10).Value = dsDataEPL.Tables(0).Rows(xn)("Lvl")
                        ws.Cells(startdata + xn, 11).Value = dsDataEPL.Tables(0).Rows(xn)("Hand")
                        ws.Cells(startdata + xn, 12).Value = dsDataEPL.Tables(0).Rows(xn)("D_C")
                        ws.Cells(startdata + xn, 13).Value = dsDataEPL.Tables(0).Rows(xn)("Dwg_No")
                        ws.Cells(startdata + xn, 14).Value = dsDataEPL.Tables(0).Rows(xn)("Part_No")
                        ws.Cells(startdata + xn, 16).Value = dsDataEPL.Tables(0).Rows(xn)("Part_Name")
                        ws.Cells(startdata + xn, 17).Value = dsDataEPL.Tables(0).Rows(xn)("Qty")

                        For yn As Integer = 2 To 17 + (dsEPL.Tables(0).Rows.Count) + 9
                            ws.Cells(startdata + xn, yn).Style.Border.Left.Style = Style.ExcelBorderStyle.Medium
                            ws.Cells(startdata + xn, yn).Style.Border.Left.Color.SetColor(System.Drawing.Color.Black)
                            ws.Cells(startdata + xn, yn).Style.Border.Right.Style = Style.ExcelBorderStyle.Medium
                            ws.Cells(startdata + xn, yn).Style.Border.Right.Color.SetColor(System.Drawing.Color.Black)
                            ws.Cells(startdata + xn, yn).Style.Border.Top.Style = Style.ExcelBorderStyle.Medium
                            ws.Cells(startdata + xn, yn).Style.Border.Top.Color.SetColor(System.Drawing.Color.Black)
                            ws.Cells(startdata + xn, yn).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Medium
                            ws.Cells(startdata + xn, yn).Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black)
                        Next
                    Next

                    For i As Integer = 0 To dsEPL.Tables(0).Rows.Count - 1
                        ws.Cells(5, i + rowstart).Value = dsEPL.Tables(0).Rows(i)(0)
                        ws.Cells(5, i + rowstart).Style.Border.Left.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(5, i + rowstart).Style.Border.Left.Color.SetColor(System.Drawing.Color.Black)
                        ws.Cells(5, i + rowstart).Style.Border.Right.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(5, i + rowstart).Style.Border.Right.Color.SetColor(System.Drawing.Color.Black)
                        ws.Cells(5, i + rowstart).Style.Border.Top.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(5, i + rowstart).Style.Border.Top.Color.SetColor(System.Drawing.Color.Black)
                        ws.Cells(5, i + rowstart).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Medium
                        ws.Cells(5, i + rowstart).Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black)

                        If i = dsEPL.Tables(0).Rows.Count - 1 Then
                            ws.Cells(4, rowstart, 4, i + rowstart).Merge = True
                            ws.Cells(4, rowstart).Value = "VARIANT"

                            ws.Cells(4, rowstart, 4, i + rowstart).Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                            ws.Cells(4, rowstart, 4, i + rowstart).Style.Border.Left.Style = Style.ExcelBorderStyle.Medium
                            ws.Cells(4, rowstart, 4, i + rowstart).Style.Border.Left.Color.SetColor(System.Drawing.Color.Black)
                            ws.Cells(4, rowstart, 4, i + rowstart).Style.Border.Right.Style = Style.ExcelBorderStyle.Medium
                            ws.Cells(4, rowstart, 4, i + rowstart).Style.Border.Right.Color.SetColor(System.Drawing.Color.Black)
                            ws.Cells(4, rowstart, 4, i + rowstart).Style.Border.Top.Style = Style.ExcelBorderStyle.Medium
                            ws.Cells(4, rowstart, 4, i + rowstart).Style.Border.Top.Color.SetColor(System.Drawing.Color.Black)
                            ws.Cells(4, rowstart, 4, i + rowstart).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Medium
                            ws.Cells(4, rowstart, 4, i + rowstart).Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black)
                        End If
                    Next
                End If

                Dim plus As Integer = 0
                If dsEPL.Tables(0).Rows.Count <= 0 Then
                    plus = 1
                    ws.Cells("R4").Value = "VARIANT"
                End If

                ws.Cells(4, rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 1).Value = "PIC"
                ws.Cells(4, rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 2).Value = "GUIDE PRICE"
                ws.Cells(4, rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 3).Value = "CURRENCY"
                ws.Cells(4, rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 4).Value = "JPN SUPLIER"
                ws.Cells(4, rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 5).Value = "DRAWING RECEIVE"
                ws.Cells(4, rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 6).Value = "LATEST COST REPORT"
                ws.Cells(4, rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 7).Value = "PART NO OES"
                ws.Cells(4, rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 8).Value = "SENT TO BUYER"
                ws.Cells(4, rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 9).Value = "BUYER DRAWING CHECK"

                For bb As Integer = 2 To 18
                    ws.Cells(4, bb, 5, bb).Merge = True
                    ws.Cells(4, bb, 5, bb).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                    ws.Cells(4, bb, 5, bb).Style.Border.Left.Style = Style.ExcelBorderStyle.Medium
                    ws.Cells(4, bb, 5, bb).Style.Border.Left.Color.SetColor(System.Drawing.Color.Black)
                    ws.Cells(4, bb, 5, bb).Style.Border.Right.Style = Style.ExcelBorderStyle.Medium
                    ws.Cells(4, bb, 5, bb).Style.Border.Right.Color.SetColor(System.Drawing.Color.Black)
                    ws.Cells(4, bb, 5, bb).Style.Border.Top.Style = Style.ExcelBorderStyle.Medium
                    ws.Cells(4, bb, 5, bb).Style.Border.Top.Color.SetColor(System.Drawing.Color.Black)
                    ws.Cells(4, bb, 5, bb).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Medium
                    ws.Cells(4, bb, 5, bb).Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black)
                Next

                For cc As Integer = rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 1 To rowstart + dsEPL.Tables(0).Rows.Count - 1 + plus + 9
                    ws.Cells(4, cc, 5, cc).Merge = True
                    ws.Cells(4, cc, 5, cc).Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                    ws.Cells(4, cc, 5, cc).Style.Border.Left.Style = Style.ExcelBorderStyle.Medium
                    ws.Cells(4, cc, 5, cc).Style.Border.Left.Color.SetColor(System.Drawing.Color.Black)
                    ws.Cells(4, cc, 5, cc).Style.Border.Right.Style = Style.ExcelBorderStyle.Medium
                    ws.Cells(4, cc, 5, cc).Style.Border.Right.Color.SetColor(System.Drawing.Color.Black)
                    ws.Cells(4, cc, 5, cc).Style.Border.Top.Style = Style.ExcelBorderStyle.Medium
                    ws.Cells(4, cc, 5, cc).Style.Border.Top.Color.SetColor(System.Drawing.Color.Black)
                    ws.Cells(4, cc, 5, cc).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Medium
                    ws.Cells(4, cc, 5, cc).Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black)
                Next

            End If
            Dim rg As ExcelRange = ws.Cells(4, 1, 4, 50)
            Dim rgvariant As ExcelRange = ws.Cells(5, 1, 5, 50)
            rg.AutoFitColumns()
            rgvariant.AutoFitColumns()

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment; filename=ItemListSourcingEPL_" + Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd") + ".xlsx")

            Dim stream As MemoryStream = New MemoryStream(exl.GetAsByteArray())
            Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length)
            'Response.Flush()
            Response.End()
        End Using
    End Sub

    Protected Sub btnDownloadEPL_Click(sender As Object, e As EventArgs) Handles btnDownloadEPL.Click
        EPL_Excel("PT01")
    End Sub

    Private Sub btnDownloadTemplate_Click(sender As Object, e As System.EventArgs) Handles btnDownloadTemplate.Click
        EPL_Excel("")
    End Sub

    Private Sub cboProject_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboProject.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProjectType As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboProject(cboProject, statusAdmin, pUser, "")

        ElseIf pFunction = "filter" Then
            up_FillComboProject(cboProject, statusAdmin, pUser, pProjectType)
        End If

        If cboProject.Items.Count > 0 Then
            cboProject.SelectedIndex = 0
        End If
    End Sub



    Private Sub up_FillComboProject(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                            pAdminStatus As String, pUserID As String, projtype As String, _
                            Optional ByRef pGroup As String = "", _
                            Optional ByRef pParent As String = "", _
                            Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboProject(projtype, pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

#End Region
End Class
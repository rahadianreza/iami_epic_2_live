﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CPListDetail.aspx.vb" Inherits="IAMI_EPIC2.CPListDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHiddenField" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>


<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    var currentColumnName;
    var dataExist;
    var dataSubmitted;
    var dataApproved;
    var dataAccepted;
    var dataRejected;


    function FillComboSupplier() {
        cboSupplierCode.PerformCallback();
    }

    function FillComboPRNumber() {
        cboPRNumber.PerformCallback();
    }

    function FillComboCENumber() {
        cboCENumber.PerformCallback(cboPRNumber.GetText());
    }

    function cboPRValueChanged() {
        FillComboCENumber();
        txtPRNumberTemp.SetText(cboPRNumber.GetText());
    }

    function CountingLength(s, e) {
        //Counting lenght
        var memo = memoNote.GetText();
        var lenMemo = memo.length;
        lenMemo = lenMemo;
        if (lenMemo >= 300) {
            lenMemo = 300;
        }

        //Set Text
        lblLenght.SetText(lenMemo + "/300");
    }

    function inputValidation() {
        if (txtCPNumber.GetText() == '') {
            txtCPNumber.Focus();
            toastr.warning("Please input Counter Proposal Number first!", "Warning");
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return false;
        }

        if (cboPRNumber.GetText() == '') {
            cboPRNumber.Focus();
            toastr.warning("Please select PR Number first!", "Warning");
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return false;
        }

        if (cboPRNumber.GetSelectedIndex() == -1) {
            cboPRNumber.Focus();
            toastr.warning('Invalid PR Number!', 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return false;
        }

        if (cboCENumber.GetText() == '') {
            cboCENumber.Focus();
            toastr.warning("Please select CE Number first!", "Warning");
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return false;
        }

        if (cboCENumber.GetSelectedIndex() == -1) {
            cboCENumber.Focus();
            toastr.warning('Invalid CE Number!', 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return false;
        }
        return true;
    }

    function OnBatchEditStartEditing(s, e) {
        currentColumnName = e.focusedColumn.fieldName;

        if ((currentColumnName == 'Material_No') || (currentColumnName == 'Description') || (currentColumnName == 'Specification') || (currentColumnName == 'Qty') || (currentColumnName == 'UOM') || (currentColumnName == 'Remarks') || (currentColumnName == 'Curr_Code')) {
            e.cancel = true;
            Grid.batchEditApi.EndEdit();

        } else if ((currentColumnName == 'PricePcs1') || (currentColumnName == 'TotalPrice1') || (currentColumnName == 'PricePcs2') || (currentColumnName == 'TotalPrice2') || (currentColumnName == 'PricePcs3') || (currentColumnName == 'TotalPrice3') || (currentColumnName == 'PricePcs4') || (currentColumnName == 'TotalPrice4') || (currentColumnName == 'PricePcs5') || (currentColumnName == 'TotalPrice5') || (currentColumnName == 'ProposalTotalPrice')) {
            e.cancel = true;
            Grid.batchEditApi.EndEdit();

        } else if ((currentColumnName == 'BestPricePcs1') || (currentColumnName == 'TotalBestPrice1') || (currentColumnName == 'BestPricePcs2') || (currentColumnName == 'TotalBestPrice2') || (currentColumnName == 'BestPricePcs3') || (currentColumnName == 'TotalBestPrice3') || (currentColumnName == 'BestPricePcs4') || (currentColumnName == 'TotalBestPrice4') || (currentColumnName == 'BestPricePcs5') || (currentColumnName == 'TotalBestPrice5') || (currentColumnName == 'ProposalTotalPrice')) {
            e.cancel = true;
            Grid.batchEditApi.EndEdit();
            
        } else if (currentColumnName == 'ProposalPricePcs') {
            window.setTimeout(function () {
                cbExist.PerformCallback('IsSubmitted');
            }, 10);

            window.setTimeout(function () {
                if ((dataSubmitted == "1") || (dataApproved == "1") || (dataAccepted == "1") || (dataRejected == "1")) {
                    e.cancel = true;
                    Grid.CancelEdit();
                    Grid.batchEditApi.EndEdit();
                }
            }, 10);

        }
    }

    function OnBatchEditEndEditing(s, e) {
        window.setTimeout(function () {
            var price = s.batchEditApi.GetCellValue(e.visibleIndex, "ProposalPricePcs");
            var qty = s.batchEditApi.GetCellValue(e.visibleIndex, "Qty");
            //alert(price);
            if (price == null)  {
                // e.rowValues[e.visibleIndex].value = "0";
                s.batchEditApi.SetCellValue(e.visibleIndex, "ProposalPricePcs", 0);
            }

            if ((currentColumnName == "ProposalPricePcs") || (currentColumnName == "ProposalTotalPrice")) {
                s.batchEditApi.SetCellValue(e.visibleIndex, "ProposalTotalPrice", price * qty);
            }
        }, 10);

        window.setTimeout(function () {
            cbExist.PerformCallback('IsSubmitted');
        }, 10);


        window.setTimeout(function () {
            if (dataSubmitted == "1") {
                e.cancel = true;
                Grid.CancelEdit();
                Grid.batchEditApi.EndEdit();
                //window.setTimeout(function () { toastr.warning('Cannot update IAMI Proposal Price/pc! The data already submitted.', 'Warning'); }, 10);

            } else if (dataApproved == "1") {
                e.cancel = true;
                Grid.CancelEdit();
                Grid.batchEditApi.EndEdit();
                window.setTimeout(function () { toastr.warning('Cannot update IAMI Proposal Price/pc! The data already approved.', 'Warning'); }, 10);

            } else if (dataAccepted == "1") {
                e.cancel = true;
                Grid.CancelEdit();
                Grid.batchEditApi.EndEdit();
                window.setTimeout(function () { toastr.warning('Cannot update IAMI Proposal Price/pc! The data already accepted.', 'Warning'); }, 10);

            } else if (dataRejected == "1") {
                e.cancel = true;
                Grid.CancelEdit();
                Grid.batchEditApi.EndEdit();
                window.setTimeout(function () { toastr.warning('Cannot update IAMI Proposal Price/pc! The data already rejected.', 'Warning'); }, 10);
            }
        }, 100);
    }

    function ShowConfirmOnLosingChanges() {
        var C = confirm("The data you have entered may not be saved. Are you sure want to leave?");
        if (C == true) {
            return true;
        } else {
            return false;
        }
    }

    function GetMessage(s, e) {
        //alert(s.cpMessage);
        if (s.cpParameter == "exist") {
            dataExist = s.cpResult;
            //alert(s.cpMessage);
        } else if (s.cpParameter == "IsSubmitted") {
            dataSubmitted = s.cpResult;
            dataApproved = s.cpResult2;
            dataAccepted = s.cpResult3;
            dataRejected = s.cpResult4;
        }
        else if (s.cpMessage == 'Data saved successfully!') {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

        }
        else if (s.cpMessage == 'Data updated successfully!') {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == 'Data submitted successfully!') {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            if (s.cpView == "") {
                Grid.PerformCallback('view');

            }
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

        window.setTimeout(function () {
            delete s.cpParameter;
        }, 10);
    }

    function GridLoad(s, e) {
        if (cboPRNumber.GetText() == '') {
            toastr.warning('Please select PR Number first!', 'Warning');
            cboPRNumber.Focus();
            return;
        }
        if (cboPRNumber.GetSelectedIndex() == -1) {
            toastr.warning('Invalid PR Number!', 'Warning');
            cboPRNumber.Focus();
            return;
        }
        txtCENumberTemp.SetText(cboCENumber.GetText());
        Grid.PerformCallback('gridload');
        IABudgetNo.Set("hfLink", s.cpLink);
        IALinkFile.SetText(s.cpCEBudget);
        memoNote.SetText(s.cpNotes);
        hf.Set("hfNotes", s.cpNotes);
    }

    function LoadCompleted(s, e) {
        if (cboPRNumber.GetEnabled() == false) {
            //Called from CPList.aspx by clicked on the detail grid
            if (s.cpCalledFromCPList == '1') {
                // alert(s.cpNotes);
                memoNote.SetText(s.cpNotes);
                dtInvitationDate.SetDate(s.cpInvitationDate);
                hf.Set("hfNotes", s.cpNotes);

                var memo = s.cpNotes;
                var lenMemo = memo.length;
                lblLenght.SetText(lenMemo + "/300");
            }
            IABudgetNo.Set("hfLink", s.cpLink);
            IALinkFile.SetText(s.cpCEBudget);
            return false;
        }
    }

    function GridLoadCompleted(s, e) {
        //Set header caption Quotation
        LabelSup1.SetText(s.cpHeaderCaption1);
        LabelSup2.SetText(s.cpHeaderCaption2);
        LabelSup3.SetText(s.cpHeaderCaption3);
        LabelSup4.SetText(s.cpHeaderCaption4);
        LabelSup5.SetText(s.cpHeaderCaption5);

        //Set header caption Best Price
        LabelSup1_BestPrice.SetText(s.cpHeaderCaption1);
        LabelSup2_BestPrice.SetText(s.cpHeaderCaption2);
        LabelSup3_BestPrice.SetText(s.cpHeaderCaption3);
        LabelSup4_BestPrice.SetText(s.cpHeaderCaption4);
        LabelSup5_BestPrice.SetText(s.cpHeaderCaption5);

        if (s.cpSaveData != "1") {
            if (cboPRNumber.GetEnabled() == false) {
                //Called from CPList.aspx by clicked on the detail grid
                return false;
            }

        } else {
            //Uncomplete fill on the grid.
            if (s.cpMessage == 'Please fill all of rows on the grid before save!') {
                //txtCPNumber.SetEnabled(true);
                cboPRNumber.SetEnabled(true);
                cboCENumber.SetEnabled(true);
            } else {
                //Grid.CancelEdit();
                txtCPNumber.SetText(s.cpCPNumber);

            }
        }

        IABudgetNo.Set("hfLink", s.cpLink);
        IALinkFile.SetText(s.cpCEBudget);
        //alert(IALinkFile.GetText());        
         window.setTimeout(function () {
            delete s.cpSaveData;
        }, 10);
    }

    function Back(s, e) {
        if (cboPRNumber.GetEnabled() == false) {
            if (Grid.batchEditApi.HasChanges() == true) {
                //there is an update data
                if (ShowConfirmOnLosingChanges() == false) {
                    e.processOnServer = false;
                    return false;
                }
            }
            
            if (hf.Get("hfNotes") != memoNote.GetText()) {
                if (ShowConfirmOnLosingChanges() == false) {
                    e.processOnServer = false;
                    return false;
                }
            }
        }
        //location.replace("CPList.aspx");
    }

    function Draft(s, e) {
        startIndex = 0;
        var ProposalPricePcs = 0;
        ProposalPricePcs = parseFloat(ProposalPricePcs);

        for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
            ProposalPricePcs = parseFloat(Grid.batchEditApi.GetCellValue(i, "ProposalPricePcs"));
  
            if ( (ProposalPricePcs == 0) || isNaN(ProposalPricePcs)) {
                ProposalPricePcs = 0;
                
            }
            
            if (ProposalPricePcs <= 0 || isNaN(ProposalPricePcs)) {
                //alert('price must greater than zero ');
                toastr.warning('Price must greater than zero !', 'Warning');
                Grid.batchEditApi.SetCellValue(i, "ProposalTotalPrice", 0);
                return false;
            }
        }
        if (inputValidation() == true) {
            window.setTimeout(function () {
                cbExist.PerformCallback('exist');
            }, 100);

            window.setTimeout(function () {
                //VALIDATE: Is data exist or not
                if (dataExist == "Y" && cboPRNumber.GetEnabled() == true) {
                    toastr.warning('Counter Proposal Number already used!', 'Warning');
                    txtCPNumber.Focus()
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return false;

                } else if (dataExist == "Y" && cboPRNumber.GetEnabled() == false) {

                    //CHECK, Is there any data change?
                    if (hf.Get("hfNotes") == memoNote.GetText() && Grid.batchEditApi.HasChanges() == false) {
                        toastr.info('There is no data change.', 'Info');
                        e.processOnServer = false;
                        return false;
                    }

                    //CONFIRMATION Before update data
                    var C = confirm("Are you sure want to revise the data?");
                    if (C == true) {
                        //toastr.info('Data save successfully!', 'Information');
                        Grid.UpdateEdit();

                        millisecondsToWait = 1000;
                        setTimeout(function () {
                            Grid.PerformCallback('save');
                        }
                        , millisecondsToWait);

                        setTimeout(function () {
                            cbExist.PerformCallback('draft');
                        }, millisecondsToWait);

                        setTimeout(function () {
                            Grid.PerformCallback('draft');
                        }, millisecondsToWait);

                        hf.Set("hfNotes", memoNote.GetText());

//                        if (Grid.batchEditApi.HasChanges() == true) {
//                            Grid.UpdateEdit();
//                            window.setTimeout(Grid.PerformCallback('save'), 500);
//                            Grid.CancelEdit();


//                        } else {
//                            cbExist.PerformCallback("draft");
//                        }

                        
                        //$('html, body').animate({ scrollTop: 0 }, 'slow');

                    } else {
                        //toastr.info('There is no data changes!', 'Information');
                        return false;
                    }

                } else {

                    if (Grid.batchEditApi.HasChanges() == true) {
                        Grid.UpdateEdit();
                        //window.setTimeout(SetInformation(), 10);
                        millisecondsToWait = 1000;
                        setTimeout(function () {
                            Grid.PerformCallback('save');
                        }
                        , millisecondsToWait);

                        setTimeout(function () {
                            cbExist.PerformCallback('draft');
                        }, millisecondsToWait);

                        setTimeout(function () {
                            Grid.PerformCallback('draft');
                        }, millisecondsToWait);

                        Grid.CancelEdit();
                        btnSubmit.SetEnabled(true);
                        btnPrint.SetEnabled(true);
                        //txtCPNumber.SetEnabled(false);

                        cboPRNumber.SetEnabled(false);
                        cboCENumber.SetEnabled(false);
                        hf.Set("hfNotes", memoNote.GetText());

                    } else {
                        toastr.warning('Please input/update the detail data on the grid!', 'Warning');
                    }

                    //$('html, body').animate({ scrollTop: 0 }, 'slow');
                }
            }, 500);
        }
    }

     function Submit(s, e) {
         if (Grid.batchEditApi.HasChanges() == true || hf.Get("hfNotes") != memoNote.GetText()) {
           //there is an update data
            toastr.warning('Please draft the data first before submit!', 'Warning');
            btnDraft.Focus();
            return;
        }

        cbExist.PerformCallback('exist');

        window.setTimeout(function () {
            //VALIDATE: Is data exist or not
            if (dataExist == "Y") {
                //alert(dataExist);
                var C = confirm("Are you sure want to submit the data?");

                if (C == true) {
                    cbExist.PerformCallback("submit");

                    window.setTimeout(function () {
                        //txtCPNumber.SetEnabled(false);
                        dtInvitationDate.SetEnabled(false);
                        cboPRNumber.SetEnabled(false);
                        cboCENumber.SetEnabled(false);
                        memoNote.SetEnabled(false);
                        btnDraft.SetEnabled(false);
                        btnSubmit.SetEnabled(false);
                        btnInvitTender.SetEnabled(false);

                        window.setTimeout(function () {
                            Grid.PerformCallback('gridload');
                        }, 50);

                        //$('html, body').animate({ scrollTop: 0 }, 'slow');
                    }, 10);


                }
            } else {
                //alert(dataExist);
                toastr.warning('Please draft the data first before submit!', 'Warning');
                btnDraft.Focus();
            }
        }, 300);
    }

    //invitation tender state
    function cbInvitationEndCallback(s, e) {

        if (s.cpParameter == "IsSubmitted") {
            dataSubmitted = s.cpResult;

            if (dataSubmitted == "0") {
                btnSubmitInvitation.SetEnabled(false);
            } else {
                btnSubmitInvitation.SetEnabled(true);
            }

        }

        if (s.cpParameter == "IsSubmitInvitation") {
            var dataSubmitInvitation = s.cpResult;

            if (dataSubmitInvitation == "0") {
                btnSubmitInvitation.SetEnabled(false);
            } else {
                btnSubmitInvitation.SetEnabled(true);
            }

        }

        window.setTimeout(function () {
            delete s.cpParameter;
        }, 10);
    }


    function OnBatchEditStartEditingInvitation(s, e) {
        var dataExist;
        dataExist = s.cpExist;
        if (dataExist == "1") {
            // alert(dataExist);
            e.cancel = true;
            GridInvitation.CancelEdit();
            GridInvitation.batchEditApi.EndEdit();
            return false;
        }
        currentColumnName = e.focusedColumn.fieldName;
        if (currentColumnName == "Supplier_Code" || currentColumnName == "Supplier_Name") {
            e.cancel = true;

        }
        if (btnSubmitInvitation.GetEnabled() == false && currentColumnName != "AllowCheck") {

            e.cancel = true;
        }
        currentEditableVisibleIndex = e.visibleIndex;
    }

    function SubmitInvitation(s, e) {
        startIndex = 0;
        var InvitationDate;
        var InvitationTime;
        var Location ;

        for (var i = startIndex; i < startIndex + GridInvitation.GetVisibleRowsOnPage(); i++) {
            InvitationDate = GridInvitation.batchEditApi.GetCellValue(i, "Invitation_Date");
            Location = GridInvitation.batchEditApi.GetCellValue(i, "Location_Meeting");
            //alert(InvitationDate);
            var dt = InvitationDate;
            var yy = dt.getFullYear();
            var mm = dt.getMonth();
            var dd = dt.getDate();
            var dtInvDate = new Date(yy, mm, dd, 0, 0, 0);

            var dt2 = new Date();
            var yy = dt2.getFullYear();
            var mm = dt2.getMonth();
            var dd = dt2.getDate();
            var dtComp = new Date(yy, mm, dd, 0, 0, 0);

            if (dtInvDate <= dtComp) {

                toastr.warning("Invitation Date cannot be less than today!", "Warning");
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return false;
                //GridInvitation.batchEditApi.SetCellValue(i, "Invitation_Date", dtComp);
            }

            if (Location == '') {
                toastr.warning("Please Input Location Meeting!", "Warning");
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return false;
            }
        }

        GridInvitation.UpdateEdit();
        millisecondsToWait = 1000;
        setTimeout(function () {
            GridInvitation.PerformCallback('loadInvitation|00');
            //alert('Data save successfully!');
            toastr.success("Data save successfully!", "Success");
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }, millisecondsToWait);
        cbInvitation.PerformCallback('IsSubmitInvitation');
    }

    function ResendInvitation(s, e) {
        startIndex = 0;
        var check = "";
        var x = false;

        for (var i = startIndex; i < startIndex + GridInvitation.GetVisibleRowsOnPage(); i++) {
            check = parseFloat(GridInvitation.batchEditApi.GetCellValue(i, "AllowCheck"));

            if (check == "1") {
                x = true;
                var InvitationDate;
                    InvitationDate = GridInvitation.batchEditApi.GetCellValue(i, "Invitation_Date");

                    var dt = InvitationDate;
                    var yy = dt.getFullYear();
                    var mm = dt.getMonth();
                    var dd = dt.getDate();
                    var dtInvDate = new Date(yy, mm, dd, 0, 0, 0);

                    var dt2 = new Date();
                    var yy = dt2.getFullYear();
                    var mm = dt2.getMonth();
                    var dd = dt2.getDate();
                    var dtComp = new Date(yy, mm, dd, 0, 0, 0);

                    if (dtInvDate <= dtComp) {

                        toastr.warning("Cannot resend data, Invitation Date is expired!", "Warning");
                        toastr.options.closeButton = false;
                        toastr.options.debug = false;
                        toastr.options.newestOnTop = false;
                        toastr.options.progressBar = false;
                        toastr.options.preventDuplicates = true;
                        toastr.options.onclick = null;
                        e.processOnServer = false;
                        return false;

                    }
                
            }
        }

        if (x == false) {
            toastr.warning('Please select data to resend email invitation', 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return false;
        }

      
        GridInvitation.UpdateEdit();
        millisecondsToWait = 1000;
        setTimeout(function () {
            GridInvitation.PerformCallback('loadInvitation|00');
            //alert('Resend Invitation successfully!');
            toastr.success("Resend Invitation successfully!", "Success");
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }, millisecondsToWait);

    }

    function CheckedChanged(s, e) {

        //Grid.SetFocusedRowIndex(-1);
        if (s.GetValue() == -1) s.SetValue(1);

        for (var i = 0; i < GridInvitation.GetVisibleRowsOnPage(); i++) {
            if (GridInvitation.batchEditApi.GetCellValue(i, "AllowCheck", false) != s.GetValue()) {
                GridInvitation.batchEditApi.SetCellValue(i, "AllowCheck", s.GetValue());
            }
        }
    }

    function ShowInvitationWindow() {
        pcInvitation.Show();
    }
    //end invitation
  
//    function cbExistEndCallback(s, e) {
//        if (s.cpParameter == "exist") {
//            dataExist = s.cpResult;

//        } else if (s.cpParameter == "IsSubmitted") {
//            dataSubmitted = s.cpResult;
//            dataApproved = s.cpResult2;
//            dataAccepted = s.cpResult3;
//            dataRejected = s.cpResult4;
//                       
//        } else if (s.cpParameter == "draft" || s.cpParameter == "submit") {            
//            if (s.cpType == "0") {
//                //INFO
//                toastr.info(s.cpMessage, 'Information');

//            } else if (s.cpType == "1") {
//                //SUCCESS
//                toastr.success(s.cpMessage, 'Success');

//            } else if (s.cpType == "2") {
//                //WARNING
//                toastr.warning(s.cpMessage, 'Warning');

//            } else if (s.cpType == "3") {
//                //ERROR
//                toastr.error(s.cpMessage, 'Error');
//            }
////            alert(s.cpCPNumber);
////            txtCPNumber.SetText(s.cpCPNumber);
////            
//        }


//        window.setTimeout(function () {
//            delete s.cpParameter;
//        }, 10);
//    }

</script>

<style type="text/css">
    .colwidthbutton
    {
        width: 90px;
    }
    
    .rowheight
    {
        height: 35px;
    }
       
    .col1
    {
        width: 10px;
    }
    .colLabel1
    {
        width: 150px;
    }
    .colLabel2
    {
        width: 113px;
    }
    .colInput1
    {
        width: 220px;
    }
    .colInput2
    {
        width: 133px;
    }
    .colSpace
    {
        width: 50px;
    }
        
    .customHeader {
        height: 15px;
    }
     
    .style1
    {
        width: 10px;
        height: 35px;
    }
    .style2
    {
        width: 150px;
        height: 35px;
    }
    .style3
    {
        width: 220px;
        height: 35px;
    }
    .style4
    {
        width: 35px;
        height: 35px;
    }
    .style5
    {
        width: 50px;
        height: 35px;
    }
    .style6
    {
        width: 113px;
        height: 35px;
    }
    .style7
    {
        width: 133px;
        height: 35px;
    }
     .hidden-div
    {
        display: none;
    }
     
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
            <tr class="rowheight">
                <td class="style1">
                    </td>
                <td class="style2">
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Counter Proposal Number"></dx1:aspxlabel>
                </td>
                <td class="style3">
                    <dx:ASPxTextBox ID="txtCPNumber" runat="server" width="200px" Height="25px" Font-Size="9pt" ReadOnly="True" BackColor="#CCCCCC"
                        ClientInstanceName="txtCPNumber">
                        <ValidationSettings CausesValidation="false" Display="None">                            
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                    </dx:ASPxTextBox>
                </td> 
                <td class="style4">
                    <dx:ASPxTextBox ID="txtRev" runat="server" width="30px" Height="25px" Font-Size="9pt" 
                        ClientInstanceName="txtRev" BackColor="#CCCCCC" ReadOnly="true" 
                        HorizontalAlign="Center" TabIndex="-1">
                    </dx:ASPxTextBox>
                </td>
                <td class="style5">
                    </td>
                <td class="style6" style="display:none">
                
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Invitation Date"></dx1:aspxlabel>
                    
                </td>
                <td class="style7" style="display:none">
                 
                    <dx:aspxdateedit ID="dtInvitationDate" runat="server" Theme="Office2010Black" 
                        Width="120px" AutoPostBack="false" ClientInstanceName="dtInvitationDate" 
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                    </dx:aspxdateedit>
                    
                </td>
                <td class="rowheight">
                    </td>
                <td class="rowheight">
                    </td>
            </tr>
            <tr class="rowheight">
                <td class="rowheight">
                    </td>
                <td class="rowheight">
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="PR Number"></dx1:aspxlabel>
                </td>
                <td colspan="2" class="rowheight">
                    <dx1:ASPxComboBox ID="cboPRNumber" runat="server" ClientInstanceName="cboPRNumber"
                        Width="200px" Font-Names="Segoe UI"  TextField="PR_Number"
                        ValueField="PR_Number" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">
                        <ClientSideEvents Init="FillComboPRNumber" ValueChanged="cboPRValueChanged"  />
                        <Columns>            
                            <dx:ListBoxColumn Caption="PR Number" FieldName="PR_Number" Width="100px" />                       
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx1:ASPxComboBox>
                </td>
                <td class="rowheight">
                    </td>
                <td class="rowheight"> 
                 <div class="hidden-div" >
                    <dx1:aspxlabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="Invitation Time"></dx1:aspxlabel>
                        </div>
                    
                </td>
                <td class="rowheight">
                 <div class="hidden-div" >
                    <dx:ASPxTextBox ID="txtTime" runat="server" width="200px" Height="25px" 
                        Font-Size="9pt" 
                        ClientInstanceName="txtTime">
                    </dx:ASPxTextBox>
                    </div>
                    </td>
                <td class="rowheight">
                    </td>
                <td class="rowheight">
                    </td>
            </tr>
            <%--<tr>
                <td></td>
                <td colspan="8">
                    
                </td>
            </tr>--%>
            <tr class="rowheight">
                <td>
                    &nbsp;</td>
                <td>

                    <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="CE Number"></dx1:aspxlabel>
                </td>
                <td colspan="2">
                    <dx1:ASPxComboBox ID="cboCENumber" runat="server" ClientInstanceName="cboCENumber"
                        Width="200px" Font-Names="Segoe UI"  TextField="CE_Number"
                        ValueField="CE_Number" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">
                        <ClientSideEvents  Validation="" Init="FillComboCENumber" EndCallback="LoadCompleted" ValueChanged="GridLoad" />
                        <Columns>            
                            <dx:ListBoxColumn Caption="CE Number" FieldName="CE_Number" Width="100px" />                       
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx1:ASPxComboBox>
                </td>
                <td>
                    &nbsp;</td>
                <td> 
                 <div class="hidden-div" >
                    <dx1:aspxlabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" 
                        Font-Size="9pt" Text="Tender Location"></dx1:aspxlabel>
                        </div>
                    
                </td>
                <td>
                 <div class="hidden-div" >
                        <dx1:ASPxComboBox ID="cboLocation" runat="server" ClientInstanceName="cboLocation" Width="280px"
                            Font-Names="Segoe UI"  TextField="Description" ValueField="Description" TextFormatString="{0}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                             IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">
                             <ClientSideEvents Validation="" />
                            <Columns>
                                <dx:ListBoxColumn Caption="Location" FieldName="Description" Width="200px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                        </div>
                    </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr style="display: none">
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <dx:ASPxTextBox ID="txtCENumberTemp" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Width="170px" ClientInstanceName="txtCENumberTemp" MaxLength="100">
                    </dx:ASPxTextBox>
                    <dx:ASPxTextBox ID="txtPRNumberTemp" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Width="170px" ClientInstanceName="txtPRNumberTemp" MaxLength="100">
                    </dx:ASPxTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="height: 10px">
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td> 
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                    &nbsp;
                    <dx:ASPxCallback ID="cbExist" runat="server" ClientInstanceName="cbExist">
                        <ClientSideEvents 
                            EndCallback="GetMessage"
                        />
                    </dx:ASPxCallback>
                    &nbsp;
                    <dx:ASPxCallback ID="cbInvitation" runat="server" ClientInstanceName="cbInvitation">
                        <ClientSideEvents 
                            EndCallback="cbInvitationEndCallback"
                            
                        />
                    </dx:ASPxCallback>
                    <dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf">
                    </dx:ASPxHiddenField>

                    <dx:ASPxHiddenField ID="IABudgetNo" runat="server" ClientInstanceName="IABudgetNo">
                    </dx:ASPxHiddenField>
                     
                </td>
            </tr>
        </table>
    </div>
   <div style="padding: 5px 5px 5px 5px">
  
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Material_No">            
            <Columns >
                <dx:GridViewDataTextColumn Caption="Material No." FieldName="Material_No"
                     VisibleIndex="0" Width="120px" >
                     <HeaderStyle CssClass="customHeader" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" 
                     VisibleIndex="1" Width="300px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Specification" FieldName="Specification" 
                     VisibleIndex="2" Width="300px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Qty" FieldName="Qty" 
                     VisibleIndex="3" Width="60px">
                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Right"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="UOM" FieldName="UOM" 
                     VisibleIndex="4" Width="50px">
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Remarks" FieldName="Remarks" 
                     VisibleIndex="5" Width="120px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Currency" FieldName="Curr_Code" 
                     VisibleIndex="6" Width="70px">
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewBandColumn Caption="Quotation" Name="Quotation" 
                    VisibleIndex="7">
                    <Columns>
                        <dx:GridViewBandColumn Caption="Supplier 1" Name="Supplier1" VisibleIndex="0">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup1" ClientInstanceName="LabelSup1" runat="server" Text="Supplier 1" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs1" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice1" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 2" Name="Supplier2" VisibleIndex="1">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup2" ClientInstanceName="LabelSup2" runat="server" Text="Supplier 2" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs2" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice2" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 3" Name="Supplier3" VisibleIndex="2">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup3" ClientInstanceName="LabelSup3" runat="server" Text="Supplier 3" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs3" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice3" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 4" Name="Supplier4" VisibleIndex="3">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup4" ClientInstanceName="LabelSup4" runat="server" Text="Supplier 4" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs4" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice4" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 5" Name="Supplier5" VisibleIndex="4">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup5" ClientInstanceName="LabelSup5" runat="server" Text="Supplier 5" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="PricePcs5" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalPrice5" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="IAMI Proposal" Name="IAMIProposal" 
                    VisibleIndex="8">
                    <Columns>
                        <dx:GridViewDataSpinEditColumn Caption="Price/Pc" FieldName="ProposalPricePcs" 
                             VisibleIndex="0" Width="100px">
                            <PropertiesSpinEdit DisplayFormatString="###,###" Width="95px" MaxLength="18">
                                <Style HorizontalAlign="Right"></Style>
                            </PropertiesSpinEdit>
                        </dx:GridViewDataSpinEditColumn>
                        <%--<dx:GridViewDataTextColumn Caption="Price/pc" FieldName="ProposalPricePcs" 
                                VisibleIndex="0" Width="100px">
                                <PropertiesTextEdit DisplayFormatString="###,###" Width="75px"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                        </dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataTextColumn Caption="Total Price" FieldName="ProposalTotalPrice" 
                                VisibleIndex="1" Width="100px">
                                <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                <CellStyle HorizontalAlign="Right"></CellStyle>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>
                <dx:GridViewBandColumn Caption="Best Price" Name="BestPrice" 
                    VisibleIndex="9">
                    <Columns >
                        <dx:GridViewBandColumn Caption="Supplier 1" Name="BestPriceSupplier1" VisibleIndex="0">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup1_BestPrice" ClientInstanceName="LabelSup1_BestPrice" runat="server" Text="Supplier 1" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="BestPricePcs1" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice1" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 2" Name="BestPriceSupplier2" VisibleIndex="1">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup2_BestPrice" ClientInstanceName="LabelSup2_BestPrice" runat="server" Text="Supplier 2" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="BestPricePcs2" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice2" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 3" Name="BestPriceSupplier3" VisibleIndex="2">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup3_BestPrice" ClientInstanceName="LabelSup3_BestPrice" runat="server" Text="Supplier 3" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="BestPricePcs3" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice3" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 4" Name="BestPriceSupplier4" VisibleIndex="3">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup4_BestPrice" ClientInstanceName="LabelSup4_BestPrice" runat="server" Text="Supplier 4" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="BestPricePcs4" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice4" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Supplier 5" Name="BestPriceSupplier5" VisibleIndex="4">
                            <HeaderCaptionTemplate>
                                <dx:ASPxLabel ID="LabelSup5_BestPrice" ClientInstanceName="LabelSup5_BestPrice" runat="server" Text="Supplier 5" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxLabel>
                            </HeaderCaptionTemplate>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Price/pc" FieldName="BestPricePcs5" 
                                     VisibleIndex="0" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Total Price" FieldName="TotalBestPrice5" 
                                     VisibleIndex="1" Width="110px">
                                     <PropertiesTextEdit DisplayFormatString="###,###"></PropertiesTextEdit>
                                     <CellStyle HorizontalAlign="Right"></CellStyle>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                    </Columns>
                </dx:GridViewBandColumn>
             </Columns>
            <Settings HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" VerticalScrollableHeight="135" ShowStatusBar="Hidden" />
            <SettingsPager Mode="ShowAllRecords"></SettingsPager>
            <SettingsBehavior AllowSelectByRowClick="True" AllowSort="False" AllowDragDrop="false" ColumnResizeMode="Control" />
            <SettingsEditing Mode="Batch" >
                <BatchEditSettings StartEditAction="Click" ShowConfirmOnLosingChanges="False"  />
            </SettingsEditing>

            <ClientSideEvents 
                EndCallback="GridLoadCompleted"
                BatchEditStartEditing="OnBatchEditStartEditing"
                BatchEditEndEditing="OnBatchEditEndEditing" 
            />

            <Styles Header-Paddings-Padding="1px" EditFormColumnCaption-Paddings-PaddingLeft="0px" EditFormColumnCaption-Paddings-PaddingRight="0px" >
                <Header CssClass="customHeader" HorizontalAlign="Center">
<Paddings Padding="1px"></Paddings>
                </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="0px" PaddingRight="0px"></Paddings>
</EditFormColumnCaption>
            </Styles>
        </dx:ASPxGridView>
     
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
            <tr>
                <td class="col1"></td>
                <td colspan="7">
                    <dx1:aspxlabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="IA Budget Number :"></dx1:aspxlabel>
                  &nbsp; &nbsp;
                  <dx:ASPxHyperLink ID="IALinkFile"  ClientInstanceName="IALinkFile" runat="server" Text="No File">  
                        <ClientSideEvents Click="function(s, e) {
                             var File = IABudgetNo.Get('hfLink');
                            //alert(IABudgetNo.Get('hfLink'));
							
                            if (File != 'No File') {
                                var fileName, fileExtension;
                                fileName = File;
                                fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
                                var pathArray = window.location.pathname.split('/');
                                var url = window.location.origin  + '/Files/' + File;
                               
                                if (fileExtension =='pdf'){
                                    window.open(url, '_blank', 'toolbar=0,location=0,menubar=0');
                                }else{
                                    IALinkFile.SetNavigateUrl(url);
                                }
                                

                              }
	                        
                        }" />
                    </dx:ASPxHyperLink>
                
                </td>
                <td class="col1"></td>
            </tr>
             <tr>
                <td class="col1"></td>
                <td colspan="8" style="height: 10px">
                </td>
            </tr>
            <tr>
                <td class="col1"></td>
                <td colspan="8">
                    <dx1:aspxlabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Note :"></dx1:aspxlabel>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <dx:ASPxMemo ID="memoNote" runat="server" Height="45px" Width="100%" ClientInstanceName="memoNote" MaxLength="300">
                        <ClientSideEvents KeyDown="CountingLength" LostFocus="CountingLength" />
                    </dx:ASPxMemo>
                </td>
                <td class="col1"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7" align="right">
                    <dx1:aspxlabel ID="lblLenght" runat="server" Font-Names="Segoe UI" Font-Size="9pt" ClientInstanceName="lblLenght" Text="0/300"></dx1:aspxlabel>
                </td>
                <td></td>
            </tr>
            <tr style="height:20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <table>
                        <tr class="rowheight">
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                                    ClientInstanceName="btnBack" Theme="Default">
                                    <Paddings Padding="2px" />
                                    <ClientSideEvents Click="Back" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnDraft" runat="server" Text="Draft" 
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnDraft" Theme="Default">                        
                                    <ClientSideEvents Click="Draft" />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnSubmit" Theme="Default">
                                    <ClientSideEvents Click="Submit"  />                        
                                    <Paddings Padding="2px" />                                    
                                </dx:ASPxButton>
                            </td>

                           <%--  <td class="colwidthbutton" style="display:none">
                                <dx:ASPxButton ID="btnInvitation" runat="server" Text="Invitation" UseSubmitBehavior="False"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False" 
                                    ClientInstanceName="btnInvitation" Theme="Default">
                                    <ClientSideEvents   Click="function(s, e) {cbInvitation.PerformCallback('IsSubmitted');  ShowInvitationWindow(); }" />                      
                                    <Paddings Padding="2px" />                                    
                                </dx:ASPxButton>

                               
                            </td>--%>
                            <td class="colwidthbutton">
                                 <dx:ASPxButton ID="btnInvitTender" runat="server" Text="Invitation" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnInvitTender" Theme="Default">
                                    <ClientSideEvents Click="function(s, e) {cbInvitation.PerformCallback('IsSubmitted');  ShowInvitationWindow(); }"   />                        
                                    <Paddings Padding="2px" />                                    
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnPrint" runat="server" Text="Print" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnPrint" Theme="Default">
                                    <Paddings Padding="2px" />                                    
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
        </table>
    </div>
</div>
  <dx:ASPxPopupControl ID="pcInvitation" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcInvitation"
        HeaderText="CP Invitation" AllowDragging="True" PopupAnimationType="None" 
        EnableViewState="False" Width="900px" Height="371px">
        
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dx:ASPxPanel ID="Panel1" runat="server" DefaultButton="btOK">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent1" runat="server">
                            <table width="100%" border="0">
                                <tr>
                                    <td rowspan="4">
                                        <div >
                                        </div>
                                    </td>
                                    <td width="200px" style="padding-left:5px">
                                   <dx:ASPxLabel runat="server" Text="Counter Proposal Number" Font-Names="Segoe UI" 
                                            Font-Size="9pt" ID="ASPxLabel9"></dx:ASPxLabel>

                                    </td>
                                    <td width="200px">
                                        <dx:ASPxTextBox ID="txtCPNumberInvitation" runat="server" Width="200px" 
                                            ClientInstanceName="txtCPNumberInvitation" BackColor="#CCCCCC" Font-Size="9pt" 
                                            Height="25px" ReadOnly="True">
                                            <ValidationSettings Display="None">
                                            </ValidationSettings>
                                            <InvalidStyle BackColor="Red">
                                            </InvalidStyle>
                                        </dx:ASPxTextBox>
                                    </td>
                                     <td >
                                         &nbsp;</td>
                                      
                                </tr>
                                <tr>
                                    <td style="padding-left:5px" >
                                        <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" 
                                            Font-Size="9pt" Text="Revision">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td >
                                        <dx:ASPxTextBox ID="txtRevInvitation" runat="server" BackColor="#CCCCCC" 
                                            ClientInstanceName="txtRevInvitation" Font-Size="9pt" Height="25px" 
                                            HorizontalAlign="Center" ReadOnly="True" TabIndex="-1" Width="30px">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                      <div style="padding: 5px 5px 5px 5px">
          <dx:ASPxGridView ID="GridInvitation" runat="server" AutoGenerateColumns="False" ClientInstanceName="GridInvitation"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Supplier_Code">

            <ClientSideEvents 
                                        BatchEditStartEditing="OnBatchEditStartEditingInvitation" 
                                        EndCallback="function(s, e) { 
	                                            GridInvitation.CancelEdit();
                                            }" CallbackError="function(s, e) {
	                                        e.Cancel=True;
                                        }" />  
             
              <Columns>
                   <dx:GridViewDataTextColumn Caption="Supplier Code" VisibleIndex="1" Width="0px"
                      FieldName="Supplier_Code">
                  </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="Supplier Name" VisibleIndex="2" 
                      Width="200px" FieldName="Supplier_Name">
                  </dx:GridViewDataTextColumn>
                 
                  <dx:GridViewDataDateColumn Caption="Invitation Date" VisibleIndex="4" 
                      Width="100px" FieldName="Invitation_Date">
                       <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormatString="dd MMM yyyy" EditFormat="Date" >
                       </PropertiesDateEdit>
                       <CellStyle HorizontalAlign="Center">
                       </CellStyle>
                  </dx:GridViewDataDateColumn>
                  <dx:GridViewDataTimeEditColumn Caption="Invitation Time" VisibleIndex="5" Width="150px"
                      FieldName="Invitation_Time">
                      <PropertiesTimeEdit Width="80px" DisplayFormatString="HH:mm" EditFormatString="HH:mm">
                          <ButtonStyle Width="5px" Paddings-Padding="3px" HorizontalAlign="Center" VerticalAlign="Top">
                          </ButtonStyle>
                      </PropertiesTimeEdit>
                      <EditFormSettings VisibleIndex="5" />
                      <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle" />
                      <CellStyle HorizontalAlign="Left">
                      </CellStyle>
                  </dx:GridViewDataTimeEditColumn>
                                 <dx:GridViewDataComboBoxColumn Caption="Location" FieldName="Location_Meeting" 
                                            VisibleIndex="6" Width="250px">
                                            <PropertiesComboBox DataSourceID="SqlDataSource5" TextField="Description" 
                                                ValueField="Description" Width="250px" TextFormatString="{0}" ClientInstanceName="Location_Meeting"
                                                DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">                            
                                                <Columns>
                                                     <dx:listboxcolumn FieldName="Description" Caption="Desc" Width="100px" />
                                                </Columns>
                                                <ItemStyle Height="10px" Paddings-Padding="4px" >
                                                    <Paddings Padding="4px"></Paddings>
                                                </ItemStyle>
                                               <ButtonStyle Width="5px" Paddings-Padding="2px">
                                                    <Paddings Padding="2px"></Paddings>
                                                </ButtonStyle>
                                            </PropertiesComboBox>
                                            <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                                VerticalAlign="Middle" Wrap="True">
                                                <Paddings PaddingLeft="5px"></Paddings>
                                            </HeaderStyle>   
                                                                                   
                                        </dx:GridViewDataComboBoxColumn>

                 <dx:GridViewDataCheckColumn Caption=" " VisibleIndex="0" Width="35px" 
                      FieldName="AllowCheck" Name="AllowCheck">

                       <PropertiesCheckEdit ValueChecked="1" ValueType="System.String" AllowGrayedByClick="false" ValueUnchecked="0" >
                       </PropertiesCheckEdit>

                        <HeaderCaptionTemplate>
                        <dx:ASPxCheckBox ID="checkBox" runat="server" ClientInstanceName="checkBox"  ClientSideEvents-CheckedChanged="CheckedChanged"  ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="" Font-Names="Segoe UI" Font-Size="8pt" ForeColor="White" > 
                        </dx:ASPxCheckBox>

                        </HeaderCaptionTemplate> 

                  </dx:GridViewDataCheckColumn>
                  
              </Columns>
                    <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                    </SettingsEditing>
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="160" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>
             </dx:ASPxGridView>
    </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                       <dx:ASPxButton ID="btnSubmitInvitation" runat="server" AutoPostBack="False" ClientInstanceName="btnSubmitInvitation"
                                Font-Names="Segoe UI" Font-Size="8pt" Text="Save" Theme="Office2010Silver" 
                                Width="80px">
                                <ClientSideEvents Click="SubmitInvitation" />
                            </dx:ASPxButton>&nbsp;&nbsp;
                            <dx:ASPxButton ID="btnResendInvitation" runat="server" AutoPostBack="False" ClientInstanceName="btnResendInvitation"
                                Font-Names="Segoe UI" Font-Size="8pt" Text="Resend" Theme="Office2010Silver" 
                                Width="80px">
                                <ClientSideEvents Click="ResendInvitation" />
                            </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
               
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
      <asp:SqlDataSource ID="SqlDataSource5" runat="server"
                    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                    SelectCommand="SELECT Description FROM VW_Location">
                </asp:SqlDataSource>
</asp:Content>
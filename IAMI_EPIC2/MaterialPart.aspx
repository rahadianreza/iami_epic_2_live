﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="MaterialPart.aspx.vb" Inherits="IAMI_EPIC2.MaterialPart" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };
        function MessageBox(s, e) {
            //alert(s.cpmessage);
            if (s.cbMessage == "Download Excel Successfully") {
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cbMessage == "Request Date To must be higher than Request Date From") {
                toastr.success(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function ItemCodeValidation(s, e) {
            if (ItemCode.GetValue() == null) {
                e.isValid = false;
            }
        }

        function GridLoad() {
            Grid.PerformCallback('refresh');
            tmpProjectType.SetText(cboProjectType.GetValue());
        }

        function SetCode(s, e) {
            tmpProjectType.SetText(cboProjectType.GetValue());
        }

    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        .style1
        {
            height: 30px;
        }
        .style2
        {
            height: 50px;
        }
        .style3
        {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%; border: 1px solid black; height: 100px;">
        <tr>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period From">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 10px 0 0 10px;">
                <%--<dx:ASPxDateEdit ID="cboPeriodFrom" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="cboPeriodFrom" EditFormatString="MMM-yyyy" DisplayFormatString="MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <ClientSideEvents ValueChanged="GridLoad" />
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>--%>
                <dx:ASPxTimeEdit ID="cboPeriodFrom" Theme="Office2010Black" Font-Names="Segoe UI"  Width="100px"
                    Font-Size="9pt" EditFormat="Custom" ClientInstanceName="cboPeriodFrom" runat="server"
                    DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy">
                      <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                </dx:ASPxTimeEdit>
            </td>
            <td style="padding: 10px 0 0 0;">
            </td>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period To">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 10px 0 0 10px;">
               <%-- <dx:ASPxDateEdit ID="cboPeriodTo" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="cboPeriodTo" EditFormatString="MMM-yyyy" DisplayFormatString="MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <ClientSideEvents ValueChanged="GridLoad" />
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>--%>
                <dx:ASPxTimeEdit ID="cboPeriodTo" Theme="Office2010Black" Font-Names="Segoe UI" Width="100px"
                    Font-Size="9pt" EditFormat="Custom" ClientInstanceName="cboPeriodTo" runat="server"
                    DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy">
                      <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                </dx:ASPxTimeEdit>
            </td>
            <td style="width: 50%;">
            </td>
        </tr>

        <tr>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Type">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxComboBox ID="cboType" runat="server" ClientInstanceName="cboType" Width="150px"
                    Font-Names="Segoe UI" TextField="Description" ValueField="Code" DataSourceID="SqlDataSource2"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <Paddings Padding="4px"></Paddings>
                     <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select 'ALL' Code, 'ALL' Description UNION ALL SELECT Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialType'">
                </asp:SqlDataSource>
            </td>
            <td colspan="5" style="width: 70%;">
            </td>
        </tr>
        <tr>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material Type">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxComboBox ID="cboCategoryMaterial" runat="server" ClientInstanceName="cboCategoryMaterial"
                    Width="150px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    DataSourceID="SqlDataSource1" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <Paddings Padding="4px"></Paddings>
                     <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT 'ALL' Code, 'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialCategory'">
                </asp:SqlDataSource>
            </td>
            <td colspan="5" style="width: 70%;">
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0px 10px 10px" colspan="6" class="style1">
                <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnShowData" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                 <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>

                <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False" ClientInstanceName="btnAdd"
                    Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                 <dx:ASPxButton ID="btnChart" runat="server" Text="Chart" UseSubmitBehavior="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False" ClientInstanceName="btnChart"
                    Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
        EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
        Font-Size="9pt" KeyFieldName="Category_Material; Material_Code; Period; Type">
        <ClientSideEvents EndCallback="OnEndCallback" CustomButtonClick="function(s, e) {
	                  if(e.buttonID == &#39;Edit&#39;){ 
                            var rowKey = Grid.GetRowKey(e.visibleIndex);
                            window.location.href= &#39;AddMaterialPart.aspx?ID=&#39; + rowKey;
                      }
                      else if (e.buttonID == &#39;Delete&#39;){
                           if (confirm('Are you sure want to delete ?')) {
                        var rowKey = Grid.GetRowKey(e.visibleIndex);
                          Grid.PerformCallback('delete |' + rowKey);
                    } else {
                     
                    }
                      }
                  }"></ClientSideEvents>
        <Columns>
            <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" Width="150px">
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Edit">
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Delete" Text="Delete">
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="Period" VisibleIndex="1" FieldName="Period" Width="150px">
                <PropertiesTextEdit DisplayFormatString="MMM yyyy">
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Material No" FieldName="Material_Code" VisibleIndex="2"
                Width="150px">
                <Settings AutoFilterCondition="Contains" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="3" FieldName="Material_Name"
                Width="120px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Specification" VisibleIndex="4" FieldName="Specification">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Material Type" VisibleIndex="5" FieldName="Category_Material"
                Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Group Item" VisibleIndex="6" FieldName="GroupItem"
                Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Category Material" VisibleIndex="7" FieldName="Category_Material"
                Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Currency" VisibleIndex="8" FieldName="Currency"
                Width="150px">
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="Price" VisibleIndex="9">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="UOM" VisibleIndex="9" FieldName="UoMUnitPrice"
                        Width="150px">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                            Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Price" VisibleIndex="9" FieldName="UnitPrice"
                        Width="150px">
                        <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                            Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="Consump" VisibleIndex="10">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="UOM" VisibleIndex="10" FieldName="UoMUnitConsump"
                        Width="150px">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                            Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Price" VisibleIndex="10" FieldName="UnitConsump"
                        Width="150px">
                        <PropertiesTextEdit DisplayFormatString="#,##0.00"></PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                            Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="11" FieldName="Register_By">
                <Settings AllowAutoFilter="False" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="12" FieldName="Register_Date">
                <Settings AllowAutoFilter="False" />
                <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="13" FieldName="Update_By">
                <Settings AllowAutoFilter="False" />
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="14" FieldName="Update_Date">
                <Settings AllowAutoFilter="False" />
                <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                </PropertiesTextEdit>
                <Settings AutoFilterCondition="Contains" />
                <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                    Wrap="True">
                    <Paddings PaddingLeft="5px"></Paddings>
                </HeaderStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
        </SettingsPager>
        <Settings ShowFilterRow="True" VerticalScrollableHeight="300" HorizontalScrollBarMode="Auto"
            VerticalScrollBarMode="Auto"></Settings>
        <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
            EditFormColumnCaption-Paddings-PaddingRight="10px">
            <Header HorizontalAlign="Center">
                <Paddings Padding="2px"></Paddings>
            </Header>
            <EditFormColumnCaption>
                <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
            </EditFormColumnCaption>
        </Styles>
        <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
        <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
    </dx:ASPxGridView>
     <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
    </dx:ASPxGridViewExporter>
</asp:Content>

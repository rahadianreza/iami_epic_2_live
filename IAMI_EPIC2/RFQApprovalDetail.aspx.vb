﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.UI
Imports System.IO
Imports System.Drawing
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls.Style
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxUploadControl
Imports System
Imports DevExpress.Utils
Imports System.Collections.Generic
Imports OfficeOpenXml
Imports DevExpress.Web.ASPxEditors

Public Class RFQApprovalDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim pErr As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cpmessage") = ErrMsg
        Grid.JSProperties("cptype") = msgType
        Grid.JSProperties("cpval") = pVal
    End Sub

    Private Sub up_GridLoad(pRFQSetNo As String, pRevision As Integer)
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        Dim rfqstatus As String
        Dim i As Integer

        ds = clsRFQApprovalDB.GetDataRFQ(pRFQSetNo, pRevision, ErrMsg)

        If ds.Tables(0).Rows.Count > 0 Then
            txtRFQSetNo.Text = ds.Tables(0).Rows(0)("RFQ_Set")
            dtDRFQDate.Value = ds.Tables(0).Rows(0)("RFQ_Date")
            txtPRNumber.Text = ds.Tables(0).Rows(0)("PR_Number")
            dtDatelineDate.Value = ds.Tables(0).Rows(0)("RFQ_DueDate")
            txtRev.Text = ds.Tables(0).Rows(0)("Rev")
            rfqstatus = IIf(IsDBNull(ds.Tables(0).Rows(0)("RFQ_Status")), "0", ds.Tables(0).Rows(0)("RFQ_Status"))

            '01/04/2019
            If rfqstatus = "2" Then
                txtNote.Text = ds.Tables(0).Rows(0)("Approval_Notes")
                '            btnApprove.Enabled = False
                '            btnReject.Enabled = False						 
            End If



        End If

        ds = clsRFQApprovalDB.GetDataDetailRFQ(pRFQSetNo, pRevision, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If

    End Sub

    Private Sub up_UpdateDataDetail(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs)
        Dim ls_Check As String
        Dim clsRFQData As New clsRFQApproval
        Dim ls_RFQ_Number As String
        Dim MsgErr As String = ""

        Dim a As Integer
        a = e.UpdateValues.Count

        clsRFQData.RFQSetNumber = txtRFQSetNo.Text
        clsRFQData.Revision = txtRev.Text
        clsRFQData.ApproveNotes = txtNote.Text
        For iLoop = 0 To a - 1
            ls_Check = (e.UpdateValues(iLoop).NewValues("AllowCheck").ToString())
            If ls_Check = True Then ls_Check = "1" Else ls_Check = "0"
            ls_RFQ_Number = Trim(e.UpdateValues(iLoop).NewValues("RFQ_Number").ToString())

            clsRFQData.RFQNumber = ls_RFQ_Number

            If ls_Check = "1" Then
                clsRFQApprovalDB.Approve(clsRFQData, pUser, MsgErr)
            End If

            If MsgErr = "" Then
                cbProcess.JSProperties("cpMessage") = "Data Has Been Approved Successfully"
            End If

        Next
    End Sub

    Private Sub up_VoidDataDetail(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs)
        Dim ls_Check As String
        Dim clsRFQData As New clsRFQApproval
        Dim ls_RFQ_Number As String
        Dim MsgErr As String = ""

        Dim a As Integer
        a = e.UpdateValues.Count
        clsRFQData.RFQSetNumber = txtRFQSetNo.Text
        clsRFQData.Revision = txtRev.Text
        clsRFQData.ApproveNotes = txtNote.Text
        For iLoop = 0 To a - 1
            ls_Check = (e.UpdateValues(iLoop).NewValues("AllowCheck").ToString())
            If ls_Check = True Then ls_Check = "1" Else ls_Check = "0"
            ls_RFQ_Number = Trim(e.UpdateValues(iLoop).NewValues("RFQ_Number").ToString())

            clsRFQData.RFQNumber = ls_RFQ_Number

            If ls_Check = "1" Then
                clsRFQApprovalDB.Void(clsRFQData, pUser, MsgErr)
            End If

            If MsgErr = "" Then
                cbProcess.JSProperties("cpMessage") = "Data Has Been Void Successfully"
            End If

        Next
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        If e.UpdateValues.Count = 0 Then
            gs_Message = "Data is not found"
            pDataChange = False
            Exit Sub
        End If

        pDataChange = True
        If hdnValue.Value = "1" Then
            up_UpdateDataDetail(sender, e)
        Else
            up_VoidDataDetail(sender, e)

        End If
        'Grid.EndUpdate()
    End Sub

    Private Sub up_Drop(pRFQNo As String)
        Dim MsgErr As String = ""

        Dim i As Integer
        Dim RFQCls As New clsRFQApproval
        RFQCls.RFQNumber = pRFQNo
        i = clsRFQApprovalDB.Drop(RFQCls, pUser, MsgErr)

        If MsgErr = "" Then
            cbProcess.JSProperties("cpMessage") = "Data Has Been Drop Successfully"
        End If

    End Sub

    Private Sub Grid_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles Grid.RowInserting
        e.Cancel = True
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
    End Sub
    'Private Sub up_Approve(pRFQSetNo As String, pAppNotes As String)
    '    Dim MsgErr As String = ""

    '    Dim i As Integer
    '    Dim RFQCls As New clsRFQApproval
    '    RFQCls.RFQSetNumber = pRFQSetNo
    '    RFQCls.ApproveNotes = pAppNotes

    '    i = clsRFQApprovalDB.Approve(RFQCls, pUser, MsgErr)

    '    If MsgErr = "" Then
    '        cbProcess.JSProperties("cpMessage") = "Data Has Been Approved Successfully"
    '    End If

    'End Sub

    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|C020", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnApprove.SetEnabled(false);" & vbCrLf & _
                         "btnReject.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub


    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, txtPRNumber)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("C020")
        Master.SiteTitle = sGlobal.menuName & " DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "C020")

        Dim rfqsetno As String = ""
        Dim revision As Integer = 0
        Dim ds As New DataSet

        If Not IsNothing(Request.QueryString("ID")) Then
            rfqsetno = Split(Request.QueryString("ID"), "|")(0)
            revision = Split(Request.QueryString("ID"), "|")(1)
        End If


        If (Not Page.IsPostBack) Then
            Dim ErrMsg As String = ""
            ds = clsRFQApprovalDB.CheckRFQApproval(rfqsetno, revision, pUser, ErrMsg)

            If ErrMsg = "" Then
                Dim script As String = ""
                Dim script2 As String = ""
                If ds.Tables(0).Rows.Count > 0 Then

                    Script = "btnApprove.SetEnabled(true);" & vbCrLf & _
                             "btnReject.SetEnabled(true);"
                    ScriptManager.RegisterStartupScript(txtNote, txtNote.GetType(), "txtNote", "txtNote.SetEnabled(true);", True)
                    ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", Script, True)
                Else
                    script = "btnApprove.SetEnabled(false);" & vbCrLf & _
                            "btnReject.SetEnabled(false);"
                    ScriptManager.RegisterStartupScript(txtNote, txtNote.GetType(), "txtNote", "txtNote.SetEnabled(false);", True)
                    ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", script, True)
                End If
            End If
            up_GridLoad(rfqsetno, revision)
        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/RFQApproval.aspx")
    End Sub

    Private Sub cbProcess_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbProcess.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)


        If pFunction = "approve" Then
            cbProcess.JSProperties("cpMessage") = "Data Has Been Approved Successfully"
        Else
            cbProcess.JSProperties("cpMessage") = "Data Has Been Reject Successfully"
        End If
        'If pFunction = "reject" Then
        '    up_Reject(pRFQSetNo)
        'ElseIf pFunction = "drop" Then
        '    up_Drop(pRFQSetNo)
        'End If
    End Sub

    Protected Sub cbPrint_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbPrint.Callback
        cbProcess.JSProperties("cpMessage") = "Data Has Been Approved Successfully"
    End Sub

    Protected Sub Grid_CustomButtonCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs) Handles Grid.CustomButtonCallback
        Dim grid As ASPxGridView = DirectCast(sender, ASPxGridView)
        Session("RFQ_Number") = grid.GetRowValues(e.VisibleIndex, "RFQ_Number").ToString()
        Session("RFQSet") = txtRFQSetNo.Text
        Session("Revision") = IIf(txtRev.Text = "", "0", txtRev.Text)
        Session("Approval") = "1"
        DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("ViewRFQ.aspx")
        'Response.Redirect("~/ViewRFQ.aspx")

      

    End Sub

    'Protected Sub Change_TheThings(ByVal sender As Object, ByVal e As ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
    '    If e.DataColumn.FieldName Is "Print" Then
    '        e.DataColumn
    '        Dim link As HyperLinkDisplayControl = TryCast(e.Cell.Controls(0), HyperLinkDisplayControl)
    '        'Dim val As String = (CType(sender, ASPxGridView)).GetRowValues(e.VisibleIndex, "Print").ToString()
    '        'link.Text = val.Substring(val.Length)
    '        link.NavigateUrl = "ViewRFQ.aspx" '& val.Substring(15, val.Length - 15)
    '    End If
    'End Sub

    Protected Sub Grid_CustomJSProperties(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewClientJSPropertiesEventArgs) Handles Grid.CustomJSProperties
        Dim ErrMsg As String = ""
        Dim ds As New DataSet
        ds = clsRFQApprovalDB.CheckRFQApproval(txtRFQSetNo.Text, txtRev.Text, pUser, ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cpExist") = "0"
            Else
                Grid.JSProperties("cpExist") = "1"
            End If
        End If
    End Sub
End Class
﻿Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewPRAssignDetailUrgent
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        up_LoadReport()

    End Sub

    Private Sub up_LoadReport()
        Dim sql As String
        Dim pPRNumber As String
        Dim ds As New DataSet
        Dim Report As New rptPRListUrgent

        pPRNumber = Split(Session("PRNumber"), "|")(0)

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "sp_PRList_Report"
                Dim cmd As New SqlCommand(sql, con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("PRNo", pPRNumber)

                Dim da As New SqlDataAdapter(cmd)

                da.Fill(ds)

                Report.DataSource = ds
                Report.Name = "PRAssignUrgent_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report

                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim pPRNumber As String

        pPRNumber = Session("PRNumber")

        Response.Redirect("~/PRAssignDetail.aspx?ID=" & pPRNumber)
    End Sub
End Class
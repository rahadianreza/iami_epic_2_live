﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Drawing

Public Class ElectricityDetail
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim pID As String
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A080")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            pID = Request.QueryString("ID")
            If pID <> "" Then
                Dim pError As String = ""
                Dim ds As New DataSet

                cboRateClass.Value = Split(pID, "|")(0)
                FillCombo(Split(pID, "|")(0))
                cboVAKind.Value = Split(pID, "|")(1)
                Period.Value = CDate(Split(pID, "|")(2))
                ds = ClsElectricityDB.getlistds(Format(CDate(Split(pID, "|")(2)), "yyyyMM"), Format(CDate(Split(pID, "|")(2)), "yyyyMM"), Split(pID, "|")(0), Split(pID, "|")(1), pError)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtRegulerPostPaid.Text = ds.Tables(0).Rows(0)("RegulerPostPaid")
                    txtRegulerPostPaidWBP.Text = ds.Tables(0).Rows(0)("RegulerPostPaidWBP")
                    txtRegulerPostPaidLWBP.Text = ds.Tables(0).Rows(0)("RegulerPostPaidLWBP")
                    txtPrePostPaid.Text = ds.Tables(0).Rows(0)("PrePostPaid")
                End If
          
                cboRateClass.Enabled = False
                cboVAKind.Enabled = False
                Period.Enabled = False
                btnClear.Enabled = False
            Else
                Period.Value = Now
            End If
        End If
    End Sub
#End Region

#Region "Control Event"

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub
 

    Private Sub cboVAKind_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboVAKind.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim RateClass As String = Split(e.Parameter, "|")(0)

        ds = ClsElectricityDB.getVAKindAdd(RateClass, pmsg)
        If pmsg = "" Then
            cboVAKind.DataSource = ds
            cboVAKind.DataBind()

        End If
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim pError As String = ""
        If validation() = False Then
            Exit Sub
        End If

        Try

            If ClsElectricityDB.Insert(cboRateClass.Text, cboVAKind.Value, _
                                       Format(Period.Value, "yyyyMM"), _
                                       txtRegulerPostPaid.Text, _
                                       txtRegulerPostPaidWBP.Text, _
                                       txtRegulerPostPaidLWBP.Text, _
                                       txtPrePostPaid.Text, 2, pUser, pError) = 1 Then
                If pError = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
                Else
                    cbSave.JSProperties("cpMessage") = pError
                End If
            Else
                ClsElectricityDB.Insert(cboRateClass.Text, _
                                        cboVAKind.Value, _
                                        Format(Period.Value, "yyyyMM"), _
                                        txtRegulerPostPaid.Text, _
                                        txtRegulerPostPaidWBP.Text, _
                                        txtRegulerPostPaidLWBP.Text, _
                                        txtPrePostPaid.Text, 1, pUser, pError)
                If pError = "" Then
                    cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
                Else
                    cbSave.JSProperties("cpMessage") = pError
                End If
            End If
           
        Catch ex As Exception
            cbSave.JSProperties("cpMessage") = ex.Message
        End Try
    End Sub
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Electricity.aspx")
    End Sub
#End Region

#Region "Procedure"
    Public Function validation() As Boolean
        If cboRateClass.Text = "" Then
            cbSave.JSProperties("cpMessage") = "Please Select Rate Class"
            Return False
        ElseIf cboVAKind.Text = "" Then
            cbSave.JSProperties("cpMessage") = "Please Select VA Kind"
            Return False
        ElseIf Period.Text = "" Then
            cbSave.JSProperties("cpMessage") = "Please Select Period"
            Return False
        ElseIf txtRegulerPostPaid.Text = 0 Then
            cbSave.JSProperties("cpMessage") = "Reguler Post Paid (kVA) must greater than zero"
            Return False
            'ElseIf txtPrePostPaid.Text = 0 Then
            '    cbSave.JSProperties("cpMessage") = "Pre Post Paid must greater than zero"
            '    Return False
        Else
            Return True
        End If
    End Function
    Private Sub FillCombo(ByVal RateClass As String)
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsElectricityDB.getVAKindAdd(RateClass, pmsg)
        If pmsg = "" Then
            cboVAKind.DataSource = ds
            cboVAKind.DataBind()
        End If

    End Sub
  
#End Region



End Class
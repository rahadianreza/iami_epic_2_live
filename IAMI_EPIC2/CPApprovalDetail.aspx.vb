﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports System.IO
Imports System.Transactions
Imports System.Web.UI

Public Class CPApprovalDetail
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Dim pUser As String = ""

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "PROCEDURE"
    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim dtFrom As String = "1900-01-01", dtTo As String = "1900-01-01"

        ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Detail_List", "PRNumber|CENumber|Rev", txtPRNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub SetInformation(ByVal pObj As Object, Optional pCalledFromCPList As Boolean = False)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Approval_Detail_SetHeader", "PRNumber|CENumber|Rev|UserID", txtPRNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text & "|" & pUser, ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Approval_Detail_SetHeader", "PRNumber|CENumber|Rev|UserID", Split(Request.QueryString(0), "|")(2) & "|" & txtCENumber.Text & "|" & Split(Request.QueryString(0), "|")(1) & pUser, ErrMsg)
        End If

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                pObj.JSProperties("cpCPNumber") = ds.Tables(0).Rows(0).Item("CP_Number").ToString()
                pObj.JSProperties("cpRev") = ds.Tables(0).Rows(0).Item("Rev").ToString()
                pObj.JSProperties("cpCENumber") = ds.Tables(0).Rows(0).Item("CE_Number")
                pObj.JSProperties("cpPRNumber") = ds.Tables(0).Rows(0).Item("PR_Number")
                pObj.JSProperties("cpInvitationDate") = CDate(ds.Tables(0).Rows(0).Item("Invitation_Date")) 'Format(CDate(ds.Tables(0).Rows(0).Item("Invitation_Date")), "yyyy-MM-dd")
                pObj.JSProperties("cpNotes") = ds.Tables(0).Rows(0).Item("Notes").ToString()
                pObj.JSProperties("cpApprovalNotes") = ds.Tables(0).Rows(0).Item("Approval_Notes").ToString()

            Else
                pObj.JSProperties("cpCPNumber") = ""
                pObj.JSProperties("cpRev") = ""
                pObj.JSProperties("cpCENumber") = ""
                pObj.JSProperties("cpPRNumber") = ""
                pObj.JSProperties("cpInvitationDate") = ""
                pObj.JSProperties("cpNotes") = ""
                pObj.JSProperties("cpApprovalNotes") = ""

                Dim dt As Date
                dt = Year(Now) & "-" & Month(Now) & "-01"
                pObj.JSProperties("cpQuotationDate") = dt
            End If

            If pCalledFromCPList = True Then
                'add this parameter to prevent reset value of Notes when callback
                pObj.JSProperties("cpCalledFromCPList") = "1"
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, pObj)
        End If
    End Sub

    Private Sub SetInformationOnServerSide()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Approval_Detail_SetHeader", "PRNumber|CENumber|Rev|UserID", txtPRNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text & "|" & pUser, ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Approval_Detail_SetHeader", "PRNumber|CENumber|Rev|UserID", Split(Request.QueryString(0), "|")(3) & "|" & txtCENumber.Text & "|" & Split(Request.QueryString(0), "|")(1) & "|" & pUser, ErrMsg)
        End If

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                memoNote.Text = ds.Tables(0).Rows(0).Item("Notes").ToString()
                txtInvitationDate.Text = Format(CDate(ds.Tables(0).Rows(0).Item("Invitation_Date")), "yyyy-MM-dd")
                txtNotesApproval.Text = ds.Tables(0).Rows(0).Item("Approval_Notes").ToString()
            Else
                memoNote.Text = ""
                txtInvitationDate.Text = ""
                txtNotesApproval.Text = ""
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
#End Region

#Region "FUNCTIONS"
    Private Function GetAllSupplierForGridHeaderCaption()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "SELECT Sup = dbo.MergeSupplier('" & txtCENumber.Text & "')", "", "", ErrMsg)

        If ErrMsg = "" Then
            retVal = ds.Tables(0).Rows(0).Item("Sup").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If

        Return retVal
    End Function

    Private Function IsDataExist() As String
        Dim retVal As String = ""
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Exist", "CPNumber", txtCPNumber.Text, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST
                    retVal = ds.Tables(0).Rows(0).Item("Ret")
                Else
                    'NOT EXISTS
                    retVal = "N"
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataApproved() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            'ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '2'", "", "", ErrMsg)
            ds = GetDataSource(CmdType.SQLScript, "SELECT Approval_Status = ISNULL(Approval_Status,'0') FROM CP_Approval WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND Approval_Person = '" & pUser & "' AND ISNULL(Approval_Status,'0') = '1'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST: APPROVED
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function IsDataRejected() As Boolean
        Dim retVal As Boolean = False
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            'ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '2'", "", "", ErrMsg)
            ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND ISNULL(CP_Status,'0') = '4'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    'EXIST: REJECTED
                    retVal = True
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function GetApprovalIDByUser() As String
        Dim retVal As String = "1"
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            'ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '2'", "", "", ErrMsg)
            ds = GetDataSource(CmdType.SQLScript, "SELECT Approval_ID FROM CP_Approval WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND Approval_Person = '" & pUser & "'", "", "", ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    retVal = ds.Tables(0).Rows(0).Item("Approval_ID").ToString()
                End If

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function GetUserIDApproval(ApprovalID As String, UserLogin As String) As String
        Dim retVal As String = ""
        Dim ErrMsg As String = ""

        Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
            sqlConn.Open()

            Dim ds As New DataSet
            'ds = GetDataSource(CmdType.SQLScript, "SELECT CP_Number FROM CP_Header WHERE CP_Number = '" & txtCPNumber.Text & "' AND Rev = '" & txtRev.Text & "' AND CP_Status = '2'", "", "", ErrMsg)
            ds = GetDataSource(CmdType.StoreProcedure, "sp_CP_Approval_Detail_CheckUserApproval", "CP_Number|Rev|UserID|ApprovalID", txtCPNumber.Text & "|" & txtRev.Text & "|" & UserLogin & "|" & ApprovalID, ErrMsg)

            If ErrMsg = "" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    retVal = ds.Tables(0).Rows(0).Item("Approval_Person").ToString()
                End If
            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cbExist)
                Return retVal
            End If
        End Using

        Return retVal
    End Function

    Private Function ConvertToFormatDate_yyyyMMdd(ByVal pDate As String) As String
        Dim retVal As String = "1900-01-01"
        Dim pYear As String = Split(pDate, "-")(0)
        Dim pMonth As String = Strings.Right("0" & Split(pDate, "-")(1), 2)
        Dim pDay As String = Strings.Right("0" & Split(pDate, "-")(2), 2)

        retVal = pYear & "-" & pMonth & "-" & pDay

        Return retVal
    End Function
#End Region

#Region "EVENTS"
    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|E020", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnApprove.SetEnabled(false);" & vbCrLf & _
                          "btnReject.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnApprove, btnApprove.GetType(), "btnApprove", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        Dim dDate As Date
        dDate = Format(CDate(Now), "yyyy-MM-dd")
        txtInvitationDate.Value = dDate
        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, txtCENumber)
            End Try
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "COUNTER PROPOSAL APPROVAL (TENDER) DETAIL"
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "E020")

        gs_Message = ""

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            txtInvitationDate.Text = Format(CDate(Now), "yyyy-MM-dd")

            If Request.QueryString.Count > 0 Then
                Try
                    txtCPNumber.Text = Split(Request.QueryString(0), "|")(0)
                    txtRev.Text = Split(Request.QueryString(0), "|")(1)
                    txtCENumber.Text = Split(Request.QueryString(0), "|")(2)
                    txtPRNumber.Text = Split(Request.QueryString(0), "|")(3)

                    Call SetInformationOnServerSide()

                    Dim script As String = ""
                    If IsDataApproved() = True Then
                        script = "txtNotesApproval.SetEnabled(false);" & vbCrLf & _
                                  "txtNotesApproval.SetEnabled(false);"
                        ScriptManager.RegisterStartupScript(txtNotesApproval, txtNotesApproval.GetType(), "txtNotesApproval", script, True)
                        btnApprove.Enabled = False
                        btnReject.Enabled = False
                    Else
                        If IsDataRejected() = True Then
                            script = "txtNotesApproval.SetEnabled(false);" & vbCrLf & _
                                  "txtNotesApproval.SetEnabled(false);"
                            ScriptManager.RegisterStartupScript(txtNotesApproval, txtNotesApproval.GetType(), "txtNotesApproval", script, True)
                            btnApprove.Enabled = False
                            btnReject.Enabled = False
                        Else

                            btnApprove.Enabled = True
                            btnReject.Enabled = True
                        End If

                    End If

                Catch ex As Exception
                    Response.Redirect("CPApprovalDetail.aspx")
                End Try


                'Remove session after back from ViewCounterProposal.aspx
                If IsNothing(Session("E020_QueryString")) = False Then
                    Session.Remove("E020_QueryString")
                End If

            Else
                'Call GenerateCPNumber()
            End If
        End If
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Try
            If e.UpdateValues.Count > 0 Then
                Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
                Dim idx As Integer = 0

                Dim lb_IsDataExist As String = IsDataExist()

                If lb_IsDataExist = "N" Then
                    If e.UpdateValues.Count <> Grid.VisibleRowCount Then
                        Session("E020_ErrMsg") = "Please fill all of rows on the grid before save!"
                        Exit Sub
                    End If
                End If

                Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                    sqlConn.Open()

                    Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                        '#HEADER
                        If lb_IsDataExist = "N" Then
                            ls_SQL = "EXEC sp_CP_Insert_Header " & _
                                     "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                     "'" & Format(CDate(txtInvitationDate.Text), "yyyy-MM-dd") & "','0','" & memoNote.Text & "','" & UserLogin & "'"
                        Else
                            ls_SQL = "EXEC sp_CP_Update_Header " & _
                                     "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                     "'" & Format(CDate(txtInvitationDate.Text), "yyyy-MM-dd") & "','0','" & memoNote.Text & "','" & UserLogin & "'"
                        End If
                        Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                        sqlComm.CommandType = CommandType.Text
                        sqlComm.ExecuteNonQuery()
                        sqlComm.Dispose()


                        '#DETAIL
                        For idx = 0 To e.UpdateValues.Count - 1
                            If lb_IsDataExist = "N" Then
                                ls_SQL = "EXEC sp_CP_Insert_Detail "
                            Else
                                ls_SQL = "EXEC sp_CP_Update_Detail "
                            End If

                            ls_SQL = ls_SQL + _
                                          "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtPRNumber.Text & "','" & txtCENumber.Text & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Material_No").ToString() & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("Qty").ToString() & "'," & _
                                          "'" & e.UpdateValues(idx).NewValues("ProposalPricePcs").ToString() & "'," & _
                                          "'0'," & _
                                          "'" & UserLogin & "'"

                            sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()
                        Next idx

                        sqlTran.Commit()
                    End Using
                End Using

            End If

        Catch ex As Exception
            Session("E020_ErrMsg") = Err.Description
        End Try
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim headerCaption As String = ""
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Try
            Call GridLoad()

            'SET HEADER CAPTION (SUPPLIER)
            '#Initialize
            Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
            Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
            Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
            Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
            Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
            Grid.Columns("Supplier1").Visible = True
            Grid.Columns("Supplier2").Visible = True
            Grid.Columns("Supplier3").Visible = True
            Grid.Columns("Supplier4").Visible = True
            Grid.Columns("Supplier5").Visible = True


            '#Set by condition
            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(0)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption1") = headerCaption : Grid.Columns("Supplier1").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption1") = "Supplier 1"
                Grid.Columns("Supplier1").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(1)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption2") = headerCaption : Grid.Columns("Supplier2").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption2") = "Supplier 2"
                Grid.Columns("Supplier2").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(2)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption3") = headerCaption : Grid.Columns("Supplier3").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption3") = "Supplier 3"
                Grid.Columns("Supplier3").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(3)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption4") = headerCaption : Grid.Columns("Supplier4").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption4") = "Supplier 4"
                Grid.Columns("Supplier4").Visible = False
            End Try

            Try
                headerCaption = Split(GetAllSupplierForGridHeaderCaption(), "|")(4)
                If headerCaption <> "" Then Grid.JSProperties("cpHeaderCaption5") = headerCaption : Grid.Columns("Supplier5").Visible = True
            Catch ex As Exception
                Grid.JSProperties("cpHeaderCaption5") = "Supplier 5"
                Grid.Columns("Supplier5").Visible = False
            End Try


            'After Save Data
            If e.Parameters = "save" Then
                Grid.JSProperties("cpSaveData") = "1"

                If IsNothing(Session("E020_ErrMsg")) = True Then
                    Call SetInformation(Grid)
                    Call DisplayMessage(MsgTypeEnum.Success, "Data saved successfully!", Grid)
                Else
                    If Session("E020_ErrMsg") = "Please fill all of rows on the grid before save!" Then
                        Call DisplayMessage(MsgTypeEnum.Warning, Session("E020_ErrMsg"), Grid)
                        Session.Remove("E020_ErrMsg")
                    End If
                End If

            End If


        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, Grid)
        End Try
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        'Non-Editable
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Private Sub cbExist_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbExist.Callback
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        Select Case e.Parameter
            Case "exist"
                Dim ret As String = "N"
                cbExist.JSProperties("cpParameter") = "exist"

                ret = IsDataExist()
                cbExist.JSProperties("cpResult") = ret

            Case "IsApproved"
                cbExist.JSProperties("cpParameter") = "IsApproved"
                If IsDataApproved() = True Then
                    cbExist.JSProperties("cpResult") = "1"
                Else
                    cbExist.JSProperties("cpResult") = "0"
                End If

            Case "approve"
                Dim ls_SQL As String = "", UserLogin As String = Session("user"), ls_ApprovalID As String = "1", ls_ApprovalPerson As String = ""
                Dim idx As Integer = 0

                Try
                    cbExist.JSProperties("cpParameter") = "approve"

                    ls_ApprovalID = GetApprovalIDByUser()

                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()

                        Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                            Select Case ls_ApprovalID
                                Case "3" 'Last Approval (Level 3)
                                    ls_SQL = "EXEC sp_CP_Approval_Detail_Approve " & _
                                             "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                             "'2','" & UserLogin & "','" & UserLogin & "','" & txtNotesApproval.Text & "' "

                                Case Else 'Approval Level 1 & 2
                                    ls_ApprovalPerson = GetUserIDApproval(ls_ApprovalID, UserLogin)
                                    If ls_ApprovalPerson = UserLogin Then
                                        ls_SQL = "EXEC sp_CP_Approval_Detail_Approve " & _
                                             "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                             "'2','" & UserLogin & "','" & UserLogin & "','" & txtNotesApproval.Text & "' "
                                    Else
                                        ls_SQL = "EXEC sp_CP_Approval_Detail_Approve " & _
                                             "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                             "'1','" & UserLogin & "','" & UserLogin & "','" & txtNotesApproval.Text & "' "
                                    End If
                                    
                            End Select

                            Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()

                            sqlTran.Commit()
                        End Using

                    End Using

                    Call DisplayMessage(MsgTypeEnum.Success, "Data approved successfully!", cbExist)

                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                End Try

            Case "reject"
                Dim ls_SQL As String = "", UserLogin As String = Session("user")
                Dim idx As Integer = 0

                Try
                    cbExist.JSProperties("cpParameter") = "reject"

                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()

                        Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()
                            ls_SQL = "EXEC sp_CP_Approval_Detail_Reject " & _
                                     "'" & txtCPNumber.Text & "','" & txtRev.Text & "','" & txtCENumber.Text & "'," & _
                                     "'4','" & UserLogin & "','" & UserLogin & "','" & txtNotesApproval.Text & "' "

                            Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()
                            sqlTran.Commit()
                        End Using
                    End Using

                    Call DisplayMessage(MsgTypeEnum.Success, "Data rejected successfully!", cbExist)

                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                End Try
        End Select
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Try
            If Request.QueryString.Count > 0 Then
                Dim pDateFrom As String = ConvertToFormatDate_yyyyMMdd(Split(Request.QueryString(0), "|")(6)), _
                    pDateTo As String = ConvertToFormatDate_yyyyMMdd(Split(Request.QueryString(0), "|")(7)), _
                    pPRNumber As String = Split(Request.QueryString(0), "|")(8), _
                    pSupplierCode As String = Split(Request.QueryString(0), "|")(9), _
                    pCPNumber As String = Split(Request.QueryString(0), "|")(10), _
                    pRowPos As String = Split(Request.QueryString(0), "|")(11)
                Session("btnBack_CPApprovalDetail") = pDateFrom & "|" & pDateTo & "|" & pPRNumber & "|" & pSupplierCode & "|" & pCPNumber & "|" & pRowPos
            End If

        Catch ex As Exception

        End Try

        Response.Redirect("CPApproval.aspx")
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click
        If Request.QueryString.Count > 0 Then
            Session("E020_QueryString") = Request.QueryString(0)
        End If

        Session("CP_calledFrom") = "E020"
        Session("E020_paramViewCP") = txtCPNumber.Text & "|" & txtCENumber.Text & "|" & txtRev.Text
        Response.Redirect("ViewCounterProposal.aspx")
    End Sub
#End Region

End Class
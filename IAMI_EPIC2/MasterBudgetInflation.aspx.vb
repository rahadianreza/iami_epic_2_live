﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
'Imports Microsoft.Office.Interop.Excel
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class MasterBudgetInflation
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Functions"
    Private Sub up_Excel(Optional ByRef outError As String = "")
        Try
            Dim ps As New PrintingSystem()

            Dim dt As System.Data.DataTable
            Dim cMasterBudgetInflation As New ClsMasterBudgetInflation

            cMasterBudgetInflation.SupplierCode = CboSupplierCode.Value
            cMasterBudgetInflation.VariantCode = CboVariantCode.Value
            cMasterBudgetInflation.Type = CboType.Value

            dt = ClsMasterBudgetInflationDB.getData(cMasterBudgetInflation, , , outError)

            Grid.DataSource = dt
            Grid.DataBind()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=MasterBudgetInflation" & Format(Date.Now, "ddMMyyyyHHmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            outError = ex.Message
        End Try
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A190")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A190")

        Grid.JSProperties("cp_type") = ""
        Grid.JSProperties("cp_message") = ""

    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim ErrMsg As String = ""
        up_Excel(ErrMsg)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("~/AddMasterBudgetInflation.aspx")
    End Sub

    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        Dim cMasterBudgetInflation As New ClsMasterBudgetInflation
        Dim pAction As String = ""
        Dim outError As String = ""

        Dim dt As System.Data.DataTable

        cMasterBudgetInflation.SupplierCode = CboSupplierCode.Value
        cMasterBudgetInflation.VariantCode = cboVariantCode.Value
        cMasterBudgetInflation.Type = CboType.Value

        dt = ClsMasterBudgetInflationDB.getData(cMasterBudgetInflation, , , outError)

        If outError <> "" Then
            cbAction.JSProperties("cp_type") = 3
            cbAction.JSProperties("cp_message") = outError
            Exit Sub
        End If

        Grid.DataSource = dt
        Grid.DataBind()
    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim dt As New DataTable
        Dim cls As New ClsMasterBudgetInflation
        cls.SupplierCode = e.NewValues("SupplierCode")
        cls.Type = e.NewValues("Type")
        cls.VariantCode = e.NewValues("VariantCode")
        cls.Material = e.NewValues("Material")
        cls.Process = e.NewValues("Process")
        cls.User = pUser

        ClsMasterBudgetInflationDB.Update(cls, , , pErr)

        If pErr <> "" Then
            Grid.CancelEdit()
            Grid.JSProperties("cp_disabled") = ""
            Grid.JSProperties("cp_type") = "3"
            Grid.JSProperties("cp_message") = pErr
        Else
            Grid.CancelEdit()
            Grid.JSProperties("cp_disabled") = ""
            Grid.JSProperties("cp_type") = "1"
            Grid.JSProperties("cp_message") = "Update data successfully!"
            dt = ClsMasterBudgetInflationDB.getData(cls, , , pErr)
            Grid.DataSource = dt
            Grid.DataBind()
        End If
    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim dt As New DataTable
        Dim cls As New ClsMasterBudgetInflation
        cls.Type = e.Values("Type")
        cls.VariantCode = e.Values("VariantCode")
        cls.SupplierCode = e.Values("SupplierCode")

        ClsMasterBudgetInflationDB.Delete(cls, , , pErr)
        If pErr = "" Then
            Grid.JSProperties("cp_disabled") = ""
            Grid.JSProperties("cp_type") = "1"
            Grid.JSProperties("cp_message") = "Delete data successfully!"
            dt = ClsMasterBudgetInflationDB.getData(cls, , , pErr)
            Grid.DataSource = dt
            Grid.DataBind()
        Else
            Grid.JSProperties("cp_disabled") = ""
            Grid.JSProperties("cp_type") = "3"
            Grid.JSProperties("cp_message") = pErr
        End If
    End Sub

    Private Sub Grid_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If e.Column.FieldName = "SupplierCode" Or e.Column.FieldName = "SupplierName" Or e.Column.FieldName = "Type" Or e.Column.FieldName = "VariantCode" Or e.Column.FieldName = "VariantName" Then
            e.Editor.ReadOnly = True
            e.Editor.ForeColor = Color.Silver
        End If
    End Sub

    Protected Sub Grid_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not Grid.IsNewRowEditing) Then
            Grid.DoRowValidation()
        End If
    End Sub

    Protected Sub Grid_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In Grid.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "Material" Then
                If IsNothing(e.NewValues("Material")) OrElse e.NewValues("Material").ToString.Trim = "" OrElse e.NewValues("Material").ToString.Trim = 0 Then
                    e.Errors(dataColumn) = "Please Input Material !"
                    Grid.JSProperties("cp_disabled") = ""
                    Grid.JSProperties("cp_type") = "2"
                    Grid.JSProperties("cp_message") = "Please input Materiall !"
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Process" Then
                If IsNothing(e.NewValues("Process")) OrElse e.NewValues("Process").ToString.Trim = "" OrElse e.NewValues("Process").ToString.Trim = 0 Then
                    e.Errors(dataColumn) = "Please Input Process !"
                    Grid.JSProperties("cp_disabled") = ""
                    Grid.JSProperties("cp_type") = "2"
                    Grid.JSProperties("cp_message") = "Please input Processs !"
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim cMasterBudgetInflation As New ClsMasterBudgetInflation
        Dim pAction As String = ""
        Dim outError As String = ""

        Dim dt As System.Data.DataTable

        pAction = Split(e.Parameters, "|")(0)

        If pAction = "load" Then
            cMasterBudgetInflation.SupplierCode = Split(e.Parameters, "|")(1)
            cMasterBudgetInflation.Type = Split(e.Parameters, "|")(2)
            cMasterBudgetInflation.VariantCode = Split(e.Parameters, "|")(3)

            dt = ClsMasterBudgetInflationDB.getData(cMasterBudgetInflation, , , outError)

            If outError <> "" Then
                cbAction.JSProperties("cp_type") = 3
                cbAction.JSProperties("cp_message") = outError
                Exit Sub
            End If

            Grid.DataSource = dt
            Grid.DataBind()

            If Grid.VisibleRowCount > 0 Then
                Grid.JSProperties("cp_disabled") = "N"
            Else
                Grid.JSProperties("cp_disabled") = "Y"
                Grid.JSProperties("cp_message") = "There is no data to show!"
            End If
        End If

    End Sub

#End Region

End Class
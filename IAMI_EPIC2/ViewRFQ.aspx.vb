﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewRFQ

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        up_LoadReport()
    End Sub

    Private Sub up_LoadReport()
        Dim sql As String
        Dim pRFQSet As String
        Dim ds As New DataSet
        Dim Report As New rptRFQ
        Dim pRev As Integer
        Dim pRFQ_Number As String

        pRFQSet = Session("RFQSet")
        pRev = Session("Revision")
        pRFQ_Number = IIf(String.IsNullOrEmpty(Session("RFQ_Number")), "", Session("RFQ_Number"))

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                If Session("Approval") = "1" Then
                    sql = "exec sp_RFQ_RFQListReport '" & pRFQSet & "' , " & pRev & " , '" & pRFQ_Number & "'"
                Else
                    sql = "exec sp_RFQ_RFQListReport '" & pRFQSet & "' , " & pRev & ""
                End If


                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "RFQ_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report

                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim pRFQSet As String
        Dim PRNo As String
        Dim Rev As Integer
        Dim Approval As Integer

        pRFQSet = Session("RFQSet")
        PRNo = Session("PRNumber")
        Rev = Session("Revision")
        Approval = Session("Approval")

        If Approval = 1 Then
            Response.Redirect("~/RFQApprovalDetail.aspx?ID=" & pRFQSet & "|" & Rev)
            Session("RFQ_Number") = Nothing
        Else
            Response.Redirect("~/RFQDetail.aspx?ID=" & pRFQSet & "|" & PRNo & "|" & Rev)
        End If

    End Sub

End Class
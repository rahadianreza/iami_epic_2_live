﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="ItemListSourcing.aspx.vb" Inherits="IAMI_EPIC2.ItemListSourcing" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--Function for Message--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };

        function EnableDisableDownloadTemplete {
            if (cboProjectType.GetValue() != 'PT01') {
             alert('ok');
                btnDownloadTemplate.SetEnabled(false)
            } else {
            alert('ng');
                btnDownloadTemplate.SetEnabled(true)
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
            <tr style="height: 20px">
                <td>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td align="left">
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="width: 600px">
                    &nbsp;
                </td>
            </tr>
             <tr style="height: 10px">
                <td style="padding: 0px 0px 0px 10px" class="style1">
                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Project Type">
                    </dx:ASPxLabel>
                </td>
                <td style="width: 20px">
                    &nbsp;
                </td>
                <td colspan="3">
                       <dx:ASPxComboBox ID="cboProjectType" runat="server" ClientInstanceName="cboProjectType"
                        Width="120px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                        TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                         <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                            cboProject.PerformCallback('filter|' + cboProjectType.GetSelectedItem().GetColumnText(0));
                                 if (cboProjectType.GetValue() != 'PT01') {
                                        btnDownloadEPL.SetEnabled(false)
                                    } else {
                                        btnDownloadEPL.SetEnabled(true)
                                    }
                            }" />
                        <Columns>
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>


                </td>
                <td align="left">
                </td>
                <td style="width: 10px">
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            
            <tr style="height: 10px">
                <td style="padding: 10px 0px 0px 10px" class="style1">
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Project Name">
                    </dx:ASPxLabel>
                </td>
                <td style="width: 20px">
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 0px" colspan="3">
                    <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                        Font-Names="Segoe UI" TextField="Project_Name"
                        ValueField="Project_ID" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                        DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                        Height="25px">
                        <Columns>
                            <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                            <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td align="left">
                </td>
                <td style="width: 10px">
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
            <td style="height:10px;">
            </td>
            </tr>
            <tr style="height: 50px">
                <td style="width: 80px; padding: 0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">
                        <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td style="width: 80px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td style="width: 80px">
                    <dx:ASPxButton ID="btnUpload" runat="server" Text="Upload" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnUpload" Theme="Default">
                        <Paddings Padding="2px" />
                        <ClientSideEvents Click="function(s, e) {
	                        cbupload.PerformCallback('');
                        }" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                 <td style="width: 80px">
                    <dx:ASPxButton ID="btnDownloadEPL" runat="server" Text="Download From EPL" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownloadEPL" Theme="Default">
                        <Paddings Padding="2px" />
                        <ClientSideEvents Click="function(s, e) {
	                        cbupload.PerformCallback('');
                        }" />
                    </dx:ASPxButton>

                </td>
                 <td style="width: 10px">
                    &nbsp;
                </td>
                 <td style="width: 80px">
                    <dx:ASPxButton ID="btnDownloadTemplate" runat="server" Text="Download Template" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False" Enabled = "False"
                        ClientInstanceName="btnDownloadTemplate" Theme="Default">
                        <Paddings Padding="2px" />
                        <ClientSideEvents Click="function(s, e) {
	                        cbupload.PerformCallback('');
                        }" />
                    </dx:ASPxButton>


                </td>
              

                <td style="width: 10px">
                    &nbsp;
                </td>
                <td>
                     <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select Project_ID, Project_Name From Proj_Header">
                    </asp:SqlDataSource>
                    &nbsp;<dx:ASPxCallback ID="cbupload" runat="server" ClientInstanceName="cbupload">
                    </dx:ASPxCallback>
                    &nbsp;<asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                        SelectCommand="SELECT Par_Code ID,Par_Description Description FROM dbo.Mst_Parameter WHERE Par_Group = 'Language'">
                    </asp:SqlDataSource>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" KeyFieldName="Project_ID" Theme="Office2010Black" Width="100%"
            Font-Names="Segoe UI" Font-Size="9pt">
            <ClientSideEvents EndCallback="OnEndCallback" />
            <Columns>
                 <dx:GridViewDataTextColumn Caption="Project Name" FieldName="Project_Name" VisibleIndex="1"
                    Width="120px">
                    <PropertiesTextEdit MaxLength="15" Width="115px" ClientInstanceName="Project_Name">
                        <Style HorizontalAlign="Left">
                            
                        </Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Group" FieldName="Group_ID" VisibleIndex="1"
                    Width="120px">
                    <PropertiesTextEdit MaxLength="15" Width="115px" ClientInstanceName="Group_ID">
                        <Style HorizontalAlign="Left">
                            
                        </Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Commodity" Width="120px" Caption="Commodity"
                    VisibleIndex="2">
                    <PropertiesTextEdit MaxLength="25" Width="200px" ClientInstanceName="Commodity">
                        <Style HorizontalAlign="Left"></Style>
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Upc" FieldName="Upc" VisibleIndex="3"
                    Width="120px">
                    <PropertiesTextEdit Width="350px" DisplayFormatInEditMode="True" ClientInstanceName="Upc">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Fna" FieldName="Fna" VisibleIndex="4"
                    Width="100px">
                    <PropertiesTextEdit Width="100px" DisplayFormatInEditMode="True" ClientInstanceName="Fna">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Dtl Upc" FieldName="Dtl_Fna" VisibleIndex="5" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Dtl_Fna">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Dtl Fna" FieldName="Dtl_Fna" VisibleIndex="5" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Dtl_Fna">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn Caption="Usg Seq" FieldName="Usg_Seq" VisibleIndex="6" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Usg_Seq">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="lvl" FieldName="Lvl" VisibleIndex="7" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Lvl">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Hand" FieldName="Hand" VisibleIndex="8" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Hand">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="D C" FieldName="D_C" VisibleIndex="9" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="D_C">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Dwg No" FieldName="Dwg_No" VisibleIndex="10" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Dwg_No">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn Caption="Part No" FieldName="Part_No" VisibleIndex="11" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Part_No">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Part No 9 Digit" FieldName="Part_No_9_Digit" VisibleIndex="12" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Part_No_9_Digit">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Part Name" FieldName="Part_Name" VisibleIndex="13" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Part_Name">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Qty" FieldName="Qty" VisibleIndex="14" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Qty" DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                <%--  Area of variant--%>
              
                <%--End Of Variant--%>

                 <dx:GridViewDataTextColumn Caption="PIC" FieldName="PIC" VisibleIndex="16" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="PIC">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Guide Price" FieldName="Guide_Price" VisibleIndex="17" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Guide_Price" DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Currency" FieldName="Currency" VisibleIndex="17" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Currency">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Guide Price Rate (IDR)" FieldName="Guide_Price_Rate" VisibleIndex="17" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Guide_Price" DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Jpn Supplier" FieldName="Jpn_Supplier" VisibleIndex="18" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Jpn_Supplier">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                   <dx:GridViewDataTextColumn Caption="Drawing Receive" FieldName="Drawing_Receive" VisibleIndex="19" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Drawing_Receive" DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                   <%--<dx:GridViewDataTextColumn Caption="Send To Buyer" FieldName="Send_To_Buyer" VisibleIndex="20" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Send_To_Buyer">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                   <dx:GridViewDataTextColumn Caption="Drawing Check" FieldName="Drawing_Check" VisibleIndex="21" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Drawing_Check">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>--%>
                   <dx:GridViewDataTextColumn Caption="Latest Cost Report" FieldName="Latest_Cost_Report" VisibleIndex="22" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Latest_Cost_Report">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Part No Oes" FieldName="Part_No_Oes" VisibleIndex="23" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Part_No_Oes">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Purchasing Group" FieldName="Purchasing_Group" VisibleIndex="24" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Purchasing_Group">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>


                <dx:GridViewDataTextColumn Caption="Register By" FieldName="Register_By" VisibleIndex="25"
                    Width="150px">
                    <PropertiesTextEdit Width="150px" ClientInstanceName="RegisterBy">
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Register Date" FieldName="Register_Date" VisibleIndex="26"
                    Width="100px">
                    <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd MMM yyyy">
                    </PropertiesDateEdit>
                    <EditFormSettings Visible="False" />
                    <Settings />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn Caption="Update By" FieldName="Update_By" VisibleIndex="27"
                    Width="150px">
                    <PropertiesTextEdit Width="150px" ClientInstanceName="UpdateBy">
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="False" />
                    <Settings />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn Caption="Update Date" FieldName="Update_Date" VisibleIndex="28"
                    Width="100px">
                    <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" EditFormat="Custom" EditFormatString="dd MMM yyyy">
                    </PropertiesDateEdit>
                    <EditFormSettings Visible="False" />
                    <Settings />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AllowAutoFilter="False" />
                </dx:GridViewDataDateColumn>
            
            </Columns>
            <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
            <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm" />
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260"
                HorizontalScrollBarMode="Auto" />
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="320" />
            </SettingsPopup>
            <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header HorizontalAlign="Center">
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
            </Styles>
        </dx:ASPxGridView>
          <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
    </div>
</asp:Content>

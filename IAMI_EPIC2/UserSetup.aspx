﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="UserSetup.aspx.vb" Inherits="IAMI_EPIC2.UserSetup" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGlobalEvents" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    //toastr.options.timeOut = 1000;
                    //toastr.options.extendedTimeOut = 0;
                    //toastr.options.fadeOut = 250;
                    //toastr.options.fadeIn = 250;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    //toastr.options.timeOut = 1000;
                    //toastr.options.extendedTimeOut = 0;
                    //toastr.options.fadeOut = 250;
                    //toastr.options.fadeIn = 250;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    //toastr.options.timeOut = 1000;
                    //toastr.options.extendedTimeOut = 0;
                    //toastr.options.fadeOut = 250;
                    //toastr.options.fadeIn = 250;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "GroupID" || currentColumnName == "MenuID" || currentColumnName == "MenuDesc") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        }

        function OnGridFocusedRowChangedUser() {
    
            gridUser.GetRowValues(gridUser.GetFocusedRowIndex(), 'UserID;UserName;Password;UserType;Description;Locked;CatererID;Supplier_Code;Department_Code;Section_Code;Job_Position;User_Email;PIC_PO', OnGetRowValuesUser);
        }
        function OnGetRowValuesUser(values) {
            var pUserID = values[0].trim();
             cboUserGroup.SetText('');
            txtUserId.SetText(pUserID);
            txtUserIDTemp.SetText(pUserID);
            txtFullName.SetText(values[1].trim());
            txtPasswordUS.SetText(values[2].trim());
            txtConfPassword.SetText(values[2].trim());
            txtDesc.SetText(values[4].trim());
            txtEmail.SetText(values[11].trim());
            rdLock.SetValue(values[5].trim());
            rdPO.SetValue(values[12].trim());
            cboType.SetValue(values[3].trim());
            
            tmpDepartment.SetText(values[8].trim());
            tmpSection.SetText(values[9].trim());
            tmpJobPos.SetText(values[10].trim());
            cboDepartment.SetText('');
            cboSection.SetText('');
            cboJobPos.SetText('');
                      
            txtUserId.SetEnabled(false);
            txtUserId.GetMainElement().style.backgroundColor = 'WhiteSmoke';
            txtUserId.GetInputElement().style.backgroundColor = 'WhiteSmoke';
            txtUserId.inputElement.style.color = 'Black';

            if (cboType.GetValue() == "2") {
                cboCaterer.SetEnabled(true);
                cboCaterer.GetMainElement().style.backgroundColor = 'White';
                cboCaterer.GetInputElement().style.backgroundColor = 'White';
                cboCaterer.inputElement.style.color = 'Black';
                cboCaterer.SetText(values[6].trim());
                tmpSupplierCode.SetText(values[7].trim());                
            }
            else {
                cboCaterer.SetEnabled(false);
                cboCaterer.GetMainElement().style.backgroundColor = 'WhiteSmoke';
                cboCaterer.GetInputElement().style.backgroundColor = 'WhiteSmoke';
                cboCaterer.inputElement.style.color = 'Black';
                cboCaterer.SetText('');
                tmpSupplierCode.SetText('');
            }

            cbTmp.PerformCallback('FillCombo');
            gridMenu.PerformCallback('load|' + pUserID);            
        }

        function OnAccessCheckedChanged(s, e) {
            gridMenu.SetFocusedRowIndex(-1);
            if (s.GetValue() == -1) s.SetValue(1);
            for (var i = 0; i < gridMenu.GetVisibleRowsOnPage(); i++) {
                if (gridMenu.batchEditApi.GetCellValue(i, "AllowAccess", false) != s.GetValue()) {
                    gridMenu.batchEditApi.SetCellValue(i, "AllowAccess", s.GetValue());
                }                
            }
        }

        function OnUpdateCheckedChanged(s, e) {
            if (s.GetValue() == -1) s.SetValue(1);
            gridMenu.SetFocusedRowIndex(-1);
            for (var i = 0; i < gridMenu.GetVisibleRowsOnPage(); i++) {
                if (gridMenu.batchEditApi.GetCellValue(i, "AllowUpdate", false) != s.GetValue()) {
                    gridMenu.batchEditApi.SetCellValue(i, "AllowUpdate", s.GetValue());
                }                
            }
        }

        function OnInit(s, e) {
            AdjustSizeGrid();
        }
        function OnControlsInitializedGrid(s, e) {
            ASPxClientUtils.AttachEventToElement(window, "resize", function (evt) {
                AdjustSizeGrid();
            });
        }
        function AdjustSizeGrid() {

            var myWidth = 0, myHeight = 0;
            if (typeof (window.innerWidth) == 'number') {
                //Non-IE
                myWidth = window.innerWidth;
                myHeight = window.innerHeight;
            } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                //IE 6+ in 'standards compliant mode'
                myWidth = document.documentElement.clientWidth;
                myHeight = document.documentElement.clientHeight;
            } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                //IE 4 compatible
                myWidth = document.body.clientWidth;
                myHeight = document.body.clientHeight;
            }

            var height = Math.max(0, myHeight);
            height = height - (height * 70 / 100)
            gridMenu.SetHeight(height);

            var heightUser = Math.max(0, myHeight);
            heightUser = heightUser - (heightUser * 65 / 100)
            gridUser.SetHeight(heightUser);
        }

        function ComboTypeChanged (s, e) {
            if (cboType.GetValue() == '2') {
                cboCaterer.SetEnabled(true);
                cboCaterer.GetMainElement().style.backgroundColor = 'White';
                cboCaterer.GetInputElement().style.backgroundColor = 'White';
                cboCaterer.inputElement.style.color = 'Black';
            }
            else {
                cboCaterer.SetEnabled(false);
                cboCaterer.GetMainElement().style.backgroundColor = 'WhiteSmoke';
                cboCaterer.GetInputElement().style.backgroundColor = 'WhiteSmoke';
                cboCaterer.inputElement.style.color = 'Black';
                cboCaterer.SetText('');
            }
        }

        function NewData (s, e) {
            txtUserId.SetText('');
            txtFullName.SetText('');
            txtPasswordUS.SetText('');
            txtConfPassword.SetText('');
            txtDesc.SetText('');
            txtEmail.SetText('');
            cboUserGroup.SetText('');
            cboCaterer.SetText('');
            cboDepartment.SetText('');
            cboSection.SetText('');
            cboJobPos.SetText('');
            tmpSupplierCode.SetText('');
            tmpDepartment.SetText('');
            tmpSection.SetText('');
            tmpJobPos.SetText('');
            cboType.SetText('');
            rdLock.SetValue(0);
            rdPO.SetValue(0);
            gridUser.SetFocusedRowIndex(-1);
            gridUser.PerformCallback('load|' + 'kosong||');
            gridMenu.PerformCallback('load|' + 'kosong||');
            txtUserId.SetEnabled(true);
            txtUserId.GetMainElement().style.backgroundColor = 'White';
            txtUserId.GetInputElement().style.backgroundColor = 'White';
            txtUserId.inputElement.style.color = 'Black';

            txtUserId.Focus();
        }

        function SaveData(s, e) {
            if (txtUserId.GetText() == '') {
                toastr.warning('Please Insert User ID!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                txtUserId.Focus();
                return;
            }
            if (txtFullName.GetText() == '') {
                toastr.warning('Please input Full Name!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                txtFullName.Focus();
                return;
            }
            if (txtPasswordUS.GetText() == '') {
                toastr.warning('Please input Password!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                txtPasswordUS.Focus();
                return;
            }
            if (txtConfPassword.GetText() == '') {
                toastr.warning('Please input confirmation Password!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                txtConfPassword.Focus();
                return;
            }
            if (txtPasswordUS.GetText() != txtConfPassword.GetText()) {
                toastr.warning('Confirmation Password is not the same with Password!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                txtPasswordUS.Focus();
                return;
            }

            if (cboType.GetValue() == null) {
                toastr.warning('Please select User Type!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                cboType.Focus();
                return;
            }
            if (cboType.GetText() != 'SUPPLIER' && cboType.GetText() != 'ADMINISTRATOR') {
                if (cboDepartment.GetText() == '') {
                    toastr.warning('Please Select Department!', 'Warning');
                    cboDepartment.Focus();
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }
           

            var pUserID = txtUserId.GetText();
            var pFullName = txtFullName.GetText();
            var pPWd = txtPasswordUS.GetText();
            var pStatusAdmin = cboType.GetValue();
            var pPICPO = rdPO.GetValue(); 
            var pDesc = txtDesc.GetText();
            var pEmail = txtEmail.GetText();
            var pByUserID = cboUserGroup.GetText();
            var pLock = rdLock.GetValue();
            var pCatererID = cboCaterer.GetText();
            var pSupplierID = tmpSupplierCode.GetText();
            var pDepartment = tmpDepartment.GetText();
            var pSection = tmpSection.GetText();
            var pJobPos = tmpJobPos.GetText();

            if (pEmail==""){
                toastr.warning('Please input Email', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            //this for accept multiple email separator (;) /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9\-]+\.)+([a-zA-Z0-9\-\.]+)+([;]([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9\-]+\.)+([a-zA-Z0-9\-\.]+))*$/;
            var regexEmail =  /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
            if (!regexEmail.test(pEmail)) 
            {
                toastr.warning('Email is invalid', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            cbkValid.PerformCallback('save|' + pUserID + '|' + pFullName + '|' + pPWd + '|' + pStatusAdmin + '|' + pDesc + '|' + pByUserID + '|' + pLock + '|' + pSupplierID + '|' + pDepartment + '|' + pSection + '|' + pJobPos + '|' + pEmail + '|' + pPICPO);
        }

        function DeleteData(s, e) {
            if (gridUser.GetFocusedRowIndex() == -1) {
                document.getElementById('divClass').className = 'alert alert-warning fade in';
                lblMessage.SetText('Warning: Please select data first!');
                e.processOnServer = false;
                return;
            }

            var msg = confirm('Are you sure want to delete this data ?');
            if (msg == false) {
                e.processOnServer = false;
                return;
            }
            var pUserID = txtUserId.GetText();

            gridUser.PerformCallback('delete|' + pUserID);
            gridMenu.PerformCallback('load|' + pUserID);
            txtUserId.SetText('');
            txtFullName.SetText('');
            txtPasswordUS.SetText('');
            txtConfPassword.SetText('');
            txtDesc.SetText('');
            txtEmail.SetText('');
            cboUserGroup.SetText('');
            cboCaterer.SetText('');
            cboDepartment.SetText('');
            cboSection.SetText('');
            cboJobPos.SetText('');
            tmpSupplierCode.SetText('');
            tmpDepartment.SetText('');
            tmpSection.SetText('');
            tmpJobPos.SetText('');
            cboType.SetText('');
            rdLock.SetValue(0);
            rdPO.SetValue(0);
            cbAccount.SetValue(0);
            txtUserId.SetEnabled(true);
            txtUserId.GetMainElement().style.backgroundColor = 'White';
            txtUserId.GetInputElement().style.backgroundColor = 'White';
            txtUserId.inputElement.style.color = 'Black';
        }

        function SavePrivilege(s, e) {
            var errMsg = s.cpValidationMsg;
            if (errMsg != "") {
                toastr.warning(errMsg, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else {
                gridMenu.UpdateEdit();
                millisecondsToWait = 1000;
                setTimeout(function () {
                    gridMenu.PerformCallback('load|' + 'kosong||');
                    gridUser.PerformCallback('save|' + 'kosong||');                    
                    txtUserId.SetText('');
                    txtFullName.SetText('');
                    txtPasswordUS.SetText('');
                    txtConfPassword.SetText('');
                    txtDesc.SetText('');
                    txtEmail.SetText('');
                    cboUserGroup.SetText('');
                    cboCaterer.SetText('');
                    cboDepartment.SetText('');
                    cboSection.SetText('');
                    cboJobPos.SetText('');
                    tmpSupplierCode.SetText('');
                    tmpDepartment.SetText('');
                    tmpSection.SetText('');
                    tmpJobPos.SetText('');
                    cboType.SetText('');
                    rdLock.SetValue(0);
                    rdPO.SetValue(0);
                    gridUser.SetFocusedRowIndex(-1);
                    txtUserId.SetEnabled(true);
                    txtUserId.GetMainElement().style.backgroundColor = 'White';
                    txtUserId.GetInputElement().style.backgroundColor = 'White';
                    txtUserId.inputElement.style.color = 'Black';

                    txtUserId.Focus();
                }, millisecondsToWait);
            }
        }

        function SetCode(s, e) {
            if (s.cpCode != "") {
                tmpSupplierCode.SetText(s.cpCode);
            }
            if (s.cpCode2 != "") {
                tmpDepartment.SetText(s.cpCode2);
            }
            if (s.cpCode3 != "") {
                tmpSection.SetText(s.cpCode3);
            }
            if (s.cpCode4 != "") {
                tmpJobPos.SetText(s.cpCode4);
            }
            if (s.cpDepartment != "") {
                cboDepartment.SetText(s.cpDepartment);
            }
            if (s.cpSection != "") {
                cboSection.SetText(s.cpSection);
            }

            if (s.cpJobPos != "") {
                cboJobPos.SetText(s.cpJobPos);
            }

            if (s.cpSupplier != "") {
                cboCaterer.SetText(s.cpSupplier);
            }         
        }

        function FilterSection() {
            cboSection.PerformCallback('filter|' + cboDepartment.GetValue());
        }

        function Show() {
            var spass = txtPasswordUS.GetText();
            alert(spass);
        }

        function InsertSign(s, e) {
            if (txtUserId.GetText() == "") {
                toastr.warning('Please input User ID!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                return false;
            }            
            else {
                var w = 500;
                var h = 590;
                var left = (screen.width / 2) - (w / 2);
                var top = (screen.height / 2) - (h / 2) - 50;
                var winseting = 'height=' + h + ',width=' + w + ',left=' + left + ',top=' + top;
                var arg = window.open('ESign.aspx?prm=' + txtUserId.GetText(), 'ModalPopUp', winseting);
            }
        }

        function ApprovalSet(s, e) {
            if (txtUserId.GetText() == "") {
                toastr.warning('Please input User ID!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                return false;
            }
            else {
                var w = 900;
                var h = 400;
                var left = (screen.width / 2) - (w / 2);
                var top = (screen.height / 2) - (h / 2) - 50;
                var winseting = 'height=' + h + ',width=' + w + ',left=' + left + ',top=' + top;
                var arg = window.open('ApprovalSet.aspx?prm=' + txtUserId.GetText(), 'ModalPopUp', winseting);
            }
        }
        function OnComboBoxKeyDown(s, e) {
            // 'Delete' button key code = 46

            if (e.htmlEvent.keyCode == 46 || e.htmlEvent.keyCode == 8)
                s.SetSelectedIndex(-1);
            //alert(s.GetValue());
        }
    </script>
    <style type="text/css">
        .style1
        {
            width: 94px;
        }
        .style2
        {
            width: 90px;
        }
        .hidden-div
        {
            display:none;
        }
        .uppercase .dxeEditAreaSys  
        {  
            text-transform: uppercase;  
            
        }
        .firstLet .dxeEditAreaSys
        {
           text-transform: capitalize;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table width="100%">
        <tr>
            <td style="width:400px">
             <div style="padding: 5px 5px 5px 5px">
              <dx:ASPxGridView ID="gridUser" runat="server" AutoGenerateColumns="False" ClientIDMode="Predictable"
                                ClientInstanceName="gridUser" Font-Names="Segoe UI" Font-Size="8pt" KeyFieldName="UserID"
                                Theme="Office2010Black" Width="100%">
                                <ClientSideEvents EndCallback="OnEndCallback" 
                                    FocusedRowChanged="function(s, e) {OnGridFocusedRowChangedUser();}" 
                                    RowClick="function(s, e) {OnGridFocusedRowChangedUser();}" />
                                                              <Columns>
                                    <dx:GridViewDataTextColumn Caption="User ID" FieldName="UserID" Name="UserID" ReadOnly="True"
                                        VisibleIndex="1" Width="200px">
                                        <CellStyle Font-Names="Segoe UI" Font-Size="8pt">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Full Name" FieldName="UserName" Name="FullName" VisibleIndex="2"
                                        Width="200px">
                                        <CellStyle Font-Names="Segoe UI" Font-Size="8pt" HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Password" FieldName="Password" Name="Password"
                                        Visible="False" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="UserType" FieldName="UserType" 
                                        Name="UserType" Visible="False"
                                        VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Name="Description"
                                        Visible="False" VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Locked" FieldName="Locked" Name="Locked"
                                        Visible="False" VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="CatererID" FieldName="CatererID" Name="CatererID"
                                        Visible="False" VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Supplier_Code" FieldName="Supplier_Code" 
                                        Visible="False" Name="Supplier_Code" VisibleIndex="8">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Department_Code" FieldName="Department_Code" 
                                        Visible="False" Name="Department_Code" VisibleIndex="9">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Section_Code" FieldName="Section_Code" 
                                        Visible="False" Name="Section_Code" VisibleIndex="10">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Job_Position" FieldName="Job_Position" 
                                        Visible="False" Name="Job_Position" VisibleIndex="11">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="User_Email" FieldName="User_Email" 
                                        Visible="False" Name="User_Email" VisibleIndex="12">
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn Caption="PIC_PO" FieldName="PIC_PO" 
                                        Visible="true" Name="PIC_PO" VisibleIndex="13">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                     <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" 
                                    AllowSort="False" EnableRowHotTrack="True" />
                                  <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                                    <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260" HorizontalScrollBarMode="Auto" />
                                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                                        <Header HorizontalAlign="Center">
                                            <Paddings Padding="2px"></Paddings>
                                        </Header>

                                        <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                                            <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px"></Paddings>
                                        </EditFormColumnCaption>
                                        <CommandColumnItem ForeColor="Orange"></CommandColumnItem>
                                    </Styles>
                 
                    
                                 </dx:ASPxGridView>
                       </div>
            </td>
            <td style="width: 10px">
                &nbsp;
            </td>
            <td>
                <table width="100%">
                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;
                        </td>
                        <td align="left" style="width: 120px">
                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="User ID">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;
                        </td>
                        <td align="left" colspan="2">
                            <dx:ASPxTextBox ID="txtUserId" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Width="170px" ClientInstanceName="txtUserId" MaxLength="15" CssClass ="uppercase" 
                                Theme="Office2010Silver">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr  style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;
                        </td>
                        <td align="left" class="style2">
                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="Full Name">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;
                        </td>
                        <td align="left" colspan="2">
                            <dx:ASPxTextBox ID="txtFullName" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Width="170px" ClientInstanceName="txtFullName" MaxLength="25" CssClass="firstLet" 
                                Theme="Office2010Silver">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;
                        </td>
                        <td align="left" class="style2">
                            <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="Password">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;
                        </td>
                        <td align="left" class="style1">
                            <dx:ASPxTextBox ID="txtPasswordUS" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Width="170px" ClientInstanceName="txtPasswordUS" Theme="Office2010Silver">
                                <ClientSideEvents Init="function(s, e) {s.SetValue(s.cp_myPassword);
                                                        txtPasswordUS.GetInputElement().setAttribute('type', 'password');
                                                        txtPasswordUS.SetText('');
                                                        }" />
                            </dx:ASPxTextBox>                            
                        </td>

                        <td style=" padding:0px 0px 0px 10px">
                                        
                            <dx:ASPxCheckBox ID="chkShow" runat="server" ClientInstanceName="chkShow" 
                                Text="Show Password" ValueChecked="1" ValueType="System.Int32" ValueUnchecked="0" 
                                    Font-Names="Segoe UI" Font-Size="9pt" CheckState="Unchecked">
                                 <ClientSideEvents CheckedChanged="function(s, e) {
	                                                                if (chkShow.GetChecked() == (true)) {
                                                                        txtPasswordUS.GetInputElement().setAttribute('type', 'text');
                                                                    }                                    
                                                                    else {
                                                                        txtPasswordUS.GetInputElement().setAttribute('type', 'password');
	                                                                }
                                                                }" />
                    
                            </dx:ASPxCheckBox>
                                        
                        </td>                    
                    </tr>
                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;
                        </td>
                        <td align="left">
                            <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="Confirm Password">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;
                        </td>
                        <td align="left" colspan="2">
                            <dx:ASPxTextBox ID="txtConfPassword" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Width="170px" ClientInstanceName="txtConfPassword" Password="True" Theme="Office2010Silver">
                                <ClientSideEvents Init="function(s, e) {s.SetValue(s.cp_myPassword);}" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;
                        </td>
                        <td align="left" class="style2">
                            <dx:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="Description">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;
                        </td>
                        <td align="left" colspan="2">
                            <dx:ASPxTextBox ID="txtDesc" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Width="170px" ClientInstanceName="txtDesc" MaxLength="25" CssClass ="capitalize"
                                Theme="Office2010Silver">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>                                        
                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;</td>
                        <td align="left" class="style2">
                            <dx:ASPxLabel ID="ASPxLabel12" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="Email">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;</td>
                        <td align="left" colspan="2">
                            <dx:ASPxTextBox ID="txtEmail" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Width="170px" ClientInstanceName="txtEmail" MaxLength="50" 
                                Theme="Office2010Silver">
                                
                            </dx:ASPxTextBox>
                        </td>
                    </tr>                                        
                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;
                        </td>
                        <td align="left" class="style2">
                            <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="User Type">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;
                        </td>
                        <td align="left" class="style1">

                            <dx:ASPxComboBox ID="cboType" runat="server" Font-Names="Segoe UI" Width="170px" 
                                ClientInstanceName="cboType" Font-Size="8pt">
                                <ClientSideEvents ValueChanged="ComboTypeChanged" 
                                    TextChanged="ComboTypeChanged" />
<ClientSideEvents TextChanged="ComboTypeChanged" ValueChanged="ComboTypeChanged"></ClientSideEvents>
                                <Items>
                                    <dx:ListEditItem Text="USER" Value="0" />
                                    <dx:ListEditItem Text="ADMINISTRATOR" Value="1" />
                                    <dx:ListEditItem Text="COST CONTROL" Value="3" />
                                    <dx:ListEditItem Text="SUPPLIER" Value="2" />
                                </Items>
                            </dx:ASPxComboBox>

                        </td>
                        <td style=" padding:0px 0px 0px 10px">
                            <dx:ASPxComboBox ID="cboCaterer" runat="server" ClientInstanceName="cboCaterer"
                                Width="170px" Font-Names="Segoe UI" DataSourceID="SS_Caterer" TextField="SupplierID"
                                ValueField="SupplierID" TextFormatString="{1}" Font-Size="8pt" 
                                Theme="Office2010Silver" DropDownStyle="DropDownList" 
                                IncrementalFilteringMode="Contains" ClientEnabled="False">                                
                                <ClientSideEvents Init="function(s, e) {
                                                                        cboCaterer.SetEnabled(false);
                                                                        cboCaterer.GetMainElement().style.backgroundColor = 'WhiteSmoke';
                                                                        cboCaterer.GetInputElement().style.backgroundColor = 'WhiteSmoke';
                                                                        cboCaterer.inputElement.style.color = 'Black';	
                                                        }" SelectedIndexChanged="function(s, e) {
	cbTmp.PerformCallback('Code');
}" />
                                <Columns>
                                    <dx:ListBoxColumn Caption="Supplier ID" FieldName="SupplierID" Width="70px" />
                                    <dx:ListBoxColumn Caption="Supplier Name" FieldName="SupplierName" Width="250px" />
                                </Columns>
                                <LoadingPanelStyle ImageSpacing="5px">
                                </LoadingPanelStyle>
                                <ClientSideEvents KeyDown="OnComboBoxKeyDown" />
                            </dx:ASPxComboBox>
                             
                        </td>
                    </tr>
                    
                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;
                        </td>
                        <td align="left" class="style2">
                            <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="Account Status">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;
                        </td>
                        <td align="left" colspan="2">
                            <dx:ASPxRadioButtonList ID="rdLock" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                RepeatDirection="Horizontal" Width="170px" ClientInstanceName="rdLock"
                                Theme="Office2010Silver">
                                <ClientSideEvents Init="function(s, e) {
	rdLock.SetValue(0);
}" />
                                <Items>
                                    <dx:ListEditItem Text="Active" Value="0" />
                                    <dx:ListEditItem Text="Locked" Value="1" />
                                </Items>
                                <Border BorderStyle="None" />
                            </dx:ASPxRadioButtonList>                            
                        </td>
                    </tr>

                    <tr style="height:25px">
                        <td align="left" style="width:30px">
                            &nbsp;
                        </td>
                        <td align="left" class="style2">
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="Copy Privileges from" Width="120px">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;
                        </td>
                        <td align="left" class="style1">
                            <dx:ASPxComboBox ID="cboUserGroup" runat="server" ClientInstanceName="cboUserGroup"
                                Width="170px" Font-Names="Segoe UI" DataSourceID="SS_User" TextField="UserID"
                                ValueField="UserID" TextFormatString="{0}" Font-Size="8pt" 
                                Theme="Office2010Silver" DropDownStyle="DropDown" 
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True">
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {gridMenu.PerformCallback('loadPrivilege|' + cboUserGroup.GetText());}" />
                                <Columns>
                                    <dx:ListBoxColumn Caption="User ID" FieldName="UserID" Width="70px" />
                                    <dx:ListBoxColumn Caption="Full Name" FieldName="UserName" Width="150px" />
                                </Columns>
                                <LoadingPanelStyle ImageSpacing="5px">
                                </LoadingPanelStyle>
                            </dx:ASPxComboBox>
                        </td>
                        <td class="hidden-div" align="left">
                            <dx:ASPxTextBox ID="tmpSupplierCode" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Width="170px" ClientInstanceName="tmpSupplierCode" MaxLength="15" 
                                Theme="Office2010Silver">
                            </dx:ASPxTextBox>
                        </td>  
                    </tr>

                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;</td>
                        <td align="left" class="style2">
                            <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="Department" Width="120px">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;</td>
                        <td align="left" class="style1">
                            <dx:ASPxComboBox ID="cboDepartment" runat="server" ClientInstanceName="cboDepartment"
                                Width="170px" Font-Names="Segoe UI" DataSourceID="SqlDataSource2" TextField="Description"
                                ValueField="Code" TextFormatString="{1}" Font-Size="8pt" 
                                Theme="Office2010Silver" DropDownStyle="DropDown" 
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True">                            
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {
	cbTmp.PerformCallback('Code2');
}" 
                                    ValueChanged="FilterSection" />
                                <Columns>            
                                    <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                    <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                                </Columns>
                                <LoadingPanelStyle ImageSpacing="5px">
                                </LoadingPanelStyle>                                
                            </dx:ASPxComboBox>                        
                        </td>
                        <td class="hidden-div" align="left">
                            <dx:ASPxTextBox ID="tmpDepartment" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Width="170px" ClientInstanceName="tmpDepartment" MaxLength="15" 
                                Theme="Office2010Silver">
                            </dx:ASPxTextBox>
                        </td>  
                    </tr>
                    

                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;</td>
                        <td align="left" class="style2">
                            <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="Section" Width="120px">
                            </dx:ASPxLabel>
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;</td>
                        <td align="left" class="style1">
                            <dx:ASPxComboBox ID="cboSection" runat="server" ClientInstanceName="cboSection"
                                Width="170px" Font-Names="Segoe UI" TextField="Description"
                                ValueField="Code" TextFormatString="{1}" Font-Size="8pt" 
                                Theme="Office2010Silver" DropDownStyle="DropDown" 
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True">                            
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                                                                        cbTmp.PerformCallback('Code3');
                                                                        }" />
                                <Columns>            
                                    <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                    <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                                </Columns>
                                <LoadingPanelStyle ImageSpacing="5px">
                                </LoadingPanelStyle>                                        
                            </dx:ASPxComboBox>
                        
                        </td>
                        <td class="hidden-div" align="left">
                            <dx:ASPxTextBox ID="tmpSection" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Width="170px" ClientInstanceName="tmpSection" MaxLength="15" 
                                Theme="Office2010Silver">
                            </dx:ASPxTextBox>
                        </td>                      
                    </tr>
                    

                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                           &nbsp;</td>
                        <td align="left" class="style2"> 
                            <dx:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="Job Position" Width="120px">
                            </dx:ASPxLabel>                           
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboJobPos" runat="server" ClientInstanceName="cboJobPos"
                                Width="170px" Font-Names="Segoe UI" DataSourceID="SqlDataSource3" TextField="Description"
                                ValueField="Code" TextFormatString="{1}" Font-Size="8pt" 
                                Theme="Office2010Silver" DropDownStyle="DropDown" 
                                IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True">                            
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {
	cbTmp.PerformCallback('Code4');
}" />
                                <Columns>            
                                    <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                    <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                                </Columns>
                                <LoadingPanelStyle ImageSpacing="5px">
                                </LoadingPanelStyle>                                
                            </dx:ASPxComboBox>
                         </td>
                         <td class="hidden-div" align="left">
                            <dx:ASPxTextBox ID="tmpJobPos" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Width="170px" ClientInstanceName="tmpJobPos" MaxLength="15" 
                                Theme="Office2010Silver">
                            </dx:ASPxTextBox>
                        </td> 
                    </tr>
                    

                    <tr style="height: 25px">
                        <td align="left" style="width:30px">
                            &nbsp;</td>
                        <td align="left" class="style2"> 
                            <dx:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                Text="PIC PO"></dx:ASPxLabel>                    
                        </td>
                        <td align="left" style="width:10px">
                            &nbsp;</td>
                        <td>
                                        
                            <dx:ASPxRadioButtonList ID="rdPO" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                                RepeatDirection="Horizontal" Width="170px" ClientInstanceName="rdPO"
                                Theme="Office2010Silver">
                                <ClientSideEvents Init="function(s, e) {
	rdPO.SetValue(0);
}" />
                                <Items>
                                    <dx:ListEditItem Text="No" Value="0" />
                                    <dx:ListEditItem Text="Yes" Value="1" />
                                </Items>
                                <Border BorderStyle="None" />
                            </dx:ASPxRadioButtonList>                            
                                        
                        </td>
                         <td class="hidden-div" align="left">
                             &nbsp;</td> 
                    </tr>
                    

                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>

        
        <tr>
            <td colspan="4">
                <dx:ASPxGridView ID="gridMenu" runat="server" AutoGenerateColumns="False" ClientInstanceName="gridMenu"
                    Font-Names="Segoe UI" Font-Size="8pt" KeyFieldName="MenuID" Theme="Office2010Black"
                    Width="100%">
                                        <ClientSideEvents Init="OnInit" 
                                        BatchEditStartEditing="OnBatchEditStartEditing" 
                                        EndCallback="function(s, e) { 
	                                            gridMenu.CancelEdit();
                                            }" CallbackError="function(s, e) {
	                                        e.Cancel=True;
                                        }" />                    
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Menu Group" FieldName="GroupID" Name="GroupID"
                            ReadOnly="True" VisibleIndex="0" Width="220px">
                            <CellStyle Font-Names="Segoe UI" Font-Size="8pt">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Menu ID" FieldName="MenuID" Name="MenuID"
                            VisibleIndex="1" Width="100px">
                            <CellStyle Font-Names="Segoe UI" Font-Size="8pt" HorizontalAlign="Left">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Menu Name" FieldName="MenuDesc" 
                            Name="MenuDesc" VisibleIndex="2" Width="360px">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn Caption="" FieldName="AllowAccess" Name="AllowAccess"
                            VisibleIndex="3" Width="100px">
                            <PropertiesCheckEdit ValueChecked="1" ValueType="System.String" 
                                                    ValueUnchecked="0" AllowGrayedByClick="false">
                                                </PropertiesCheckEdit>
                            <HeaderCaptionTemplate>
                            <dx:ASPxCheckBox ID="chkAccess" runat="server" ClientInstanceName="chkAccess" ClientSideEvents-CheckedChanged="OnAccessCheckedChanged" ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="Access" Font-Names="Segoe UI" Font-Size="8pt" ForeColor="White"> 
                            </dx:ASPxCheckBox>
                            </HeaderCaptionTemplate> 
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataCheckColumn Caption="" FieldName="AllowUpdate" Name="AllowUpdate"
                            VisibleIndex="4" Width="100px">
                            <PropertiesCheckEdit ValueChecked="1" ValueType="System.String" 
                                                    ValueUnchecked="0" AllowGrayedByClick="false">
                                                </PropertiesCheckEdit>
                            <HeaderCaptionTemplate>
                            <dx:ASPxCheckBox ID="chkUpdate" runat="server" ClientInstanceName="chkUpdate" ClientSideEvents-CheckedChanged="OnUpdateCheckedChanged" ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="Update" Font-Names="Segoe UI" Font-Size="8pt" ForeColor="White"> 
                            </dx:ASPxCheckBox>
                            </HeaderCaptionTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataCheckColumn>
                    </Columns>
                    <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
                    <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                    </SettingsPager>
                    <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                        <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                    </SettingsEditing>
                    <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                        VerticalScrollableHeight="160" VerticalScrollBarMode="Visible" />
                                        <Styles>
                                            <Header HorizontalAlign="Center">
                                                <Paddings PaddingBottom="5px" PaddingTop="5px" />
                                            </Header>
                                        </Styles>
                    <StylesEditors ButtonEditCellSpacing="0">
                        <ProgressBar Height="21px">
                        </ProgressBar>
                    </StylesEditors>
                </dx:ASPxGridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%">
                    <tr>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td align="left">
                            <dx:ASPxTextBox ID="txtUserIDTemp" runat="server" BackColor="White" ClientInstanceName="txtUserIDTemp"
                                ForeColor="White" Width="170px">
                                <Border BorderColor="White" />
                            </dx:ASPxTextBox>
                        </td>
                        <td align="center" style="width:85px">
                           <dx:ASPxButton ID="btnApproval" runat="server" AutoPostBack="False" Font-Names="Segoe UI" ClientInstanceName="btnApproval"
                                Font-Size="8pt" Text="Approval" Theme="Office2010Silver" Width="80px">
                               <ClientSideEvents Click="ApprovalSet" />
                                
                            </dx:ASPxButton>
                        </td>
                        <td align="center" style="width:85px">
                            <dx:ASPxButton ID="btnESign" runat="server" AutoPostBack="False" Font-Names="Segoe UI" ClientInstanceName="btnESign"
                                Font-Size="8pt" Text="E-Sign" Theme="Office2010Silver" Width="80px">
                                <ClientSideEvents Click="InsertSign" />                                
                            </dx:ASPxButton>
                        </td>
                        <td align="center" style="width:85px">
                            <dx:ASPxButton ID="btnClear" runat="server" AutoPostBack="False" Font-Names="Segoe UI"
                                Font-Size="8pt" Text="New" Theme="Office2010Silver" Width="80px">
                                <ClientSideEvents Click="NewData" />
                            </dx:ASPxButton>
                        </td>
                        <td align="center" style="width:85px">
                            <dx:ASPxButton ID="btnDelete" runat="server" AutoPostBack="False" Font-Names="Segoe UI"
                                Font-Size="8pt" Text="Delete" Theme="Office2010Silver" Width="80px">
                                <ClientSideEvents Click="DeleteData" />
                            </dx:ASPxButton>
                        </td>
                        <td align="center" style="width:85px">
                            <dx:ASPxButton ID="btnSubmit" runat="server" AutoPostBack="False" ClientInstanceName="btnSubmit"
                                Font-Names="Segoe UI" Font-Size="8pt" Text="Save" Theme="Office2010Silver" 
                                Width="80px">
                                <ClientSideEvents Click="SaveData" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxPopupControl ID="search" runat="server" AllowDragging="True" ClientInstanceName="search"
                                CloseAction="CloseButton" EnableViewState="False" Font-Names="Segoe UI" HeaderText="Please input User ID!"
                                Modal="True" PopupAnimationType="None" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                                Theme="Office2010Silver" Width="280px" Font-Size="8pt">
                                <ClientSideEvents PopUp="function(s, e) { txtsearch.Focus(); }" />
                                <HeaderStyle Font-Names="Segoe UI" />
                                <ContentCollection>
                                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                        <dx:ASPxPanel ID="Panel1" runat="server" DefaultButton="btOK">
                                            <PanelCollection>
                                                <dx:PanelContent ID="PanelContent1" runat="server" Width="280px">
                                                    <table width="280px">
                                                        <tr>
                                                            <td valign="top" style="width:100px">
                                                                <dx:ASPxLabel ID="lblUsername1" runat="server" Font-Names="Segoe UI" 
                                                                    Text="User ID:" Font-Size="8pt" Theme="Office2010Silver">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="width:230px">
                                                                <dx:ASPxTextBox ID="txtsearch" runat="server" ClientInstanceName="txtsearch" Font-Names="Segoe UI"
                                                                    MaxLength="15" Width="230px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table width="280px">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <dx:ASPxButton ID="btOK" runat="server" AutoPostBack="False" Font-Names="Segoe UI"
                                                                                Text="OK" Theme="Office2010Silver" Width="80px" Font-Size="8pt">
                                                                                <ClientSideEvents Click="function(s, e) { 
                                                                                    ASPxCallback2.PerformCallback('search');
                                                                                    search.Hide();
                                                                                    txtsearch.SetText('');}" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                        <td align="right" style="width: 85px;">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxPanel>
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                                <Border BorderColor="#DFE5EA" />
                            </dx:ASPxPopupControl>
                            <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="ASPxCallback1">
                                <ClientSideEvents CallbackComplete="function(s, e) {
	                            txtPasswordUS.SetText(e.result);
                                txtConfPassword.SetText(e.result);
                            }" />
                            </dx:ASPxCallback>
                            <dx:ASPxCallback ID="ASPxCallback2" runat="server" ClientInstanceName="ASPxCallback2">
                                <ClientSideEvents EndCallback="function(s, e) {
	                            txtUserId.SetText(s.cpUserId);
                                txtFullName.SetText(s.cpFullName);
                                cboType.SetValue(s.cpStatusAdmin);
                                txtDesc.SetText(s.cpDescription);
                                txtEmail.SetText(s.cpEmail);
                                gridMenu.PerformCallback('loadPrivilege|' + s.cpUserId);               
                            }" />
                            </dx:ASPxCallback>

                            <dx:ASPxCallback ID="cbkValid" runat="server" ClientInstanceName="cbkValid">
                                <ClientSideEvents EndCallback="SavePrivilege" />
                            </dx:ASPxCallback>
                            <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
                                <ClientSideEvents CallbackComplete="SetCode" />
                            </dx:ASPxCallback>
                            <asp:SqlDataSource ID="SS_User" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                                SelectCommand="SELECT rtrim(UserID) UserID, rtrim(UserName) UserName FROM [UserSetup] where AppID = 'P01' "></asp:SqlDataSource>

                            <asp:SqlDataSource ID="SS_Caterer" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                                SelectCommand="SELECT Supplier_Code SupplierID, Supplier_Name SupplierName FROM dbo.Mst_Supplier"></asp:SqlDataSource>
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                                ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"                         
                                SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Department'">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SqlDataSource3" runat="server"
                                ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                                SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'JobPosition'">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

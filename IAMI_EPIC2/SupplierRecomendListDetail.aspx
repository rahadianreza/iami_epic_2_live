﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="SupplierRecomendListDetail.aspx.vb" Inherits="IAMI_EPIC2.SupplierRecomendListDetail" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">


       function GetMessage(s, e) {

           //alert(s.cpMessage);

           if (s.cpMessage == 'Draft data saved successfull') {
               toastr.success(s.cpMessage, 'Success');
               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;



               if (txtFlag.GetText() == '0') {
                   //alert(s.cpStatus);
                   txtRev.SetText(s.cpRevision);
                   txtSRNumber.SetText(s.cpSRNo);
                   txtParameter.SetText(s.cpSRNo + '|' + s.cpRevision + '|' + cboCPNumber.GetText());
                   // txtParameter.SetText(s.cpSRNo + '' + s.cpRevision + cboCPNumber.GetText());
                   cboCPNumber.SetEnabled(false);
                   dtSRDate.SetEnabled(false);

               }

           }
           else if (s.cpMessage == "Data saved successfull") {
               toastr.success(s.cpMessage, 'Success');
               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;

               btnDraft.SetEnabled(false);
               btnSubmit.SetEnabled(false);
               cbSuppNo.SetEnabled(false);
               memoConsi.SetEnabled(false);
               memoNote.SetEnabled(false);
               cboCPNumber.SetEnabled(false);
               dtSRDate.SetEnabled(false);

               millisecondsToWait = 1000;
               setTimeout(function () {
                   window.location.href = "/SupplierRecomendList.aspx";
               }, millisecondsToWait);
           }
           else if (s.cpMessage == null || s.cpMessage == "") {
               if (s.cpView == "") {
                   Grid.PerformCallback('view|' + txtParameter.GetText());  //xx|0|xx');
                   Grid_Delivery.PerformCallback('view|'  + txtParameter.GetText()); //xx|0|xx');
                   Grid_Quality.PerformCallback('view|'  + txtParameter.GetText()); //xx|0|xx');
               }

               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;


           }
           else {
               toastr.warning(s.cpMessage, 'Warning');
               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;
           }

           var status = s.cpStatus;

           //  alert(status);

           if (status == "0") {
               //  cbSuppNo.PerformCallback(s.cpCP);
               btnSubmit.SetEnabled(true);
               btnPrint.SetEnabled(true);
               btnQCD.SetEnabled(true);
           }
           else if (status == "1") {
               btnDraft.SetEnabled(false);
               btnSubmit.SetEnabled(false);
               cbSuppNo.SetEnabled(false);
               memoConsi.SetEnabled(false);
               memoNote.SetEnabled(false);
               btnQCD.SetEnabled(true);
           }
           else if (status == "2") {
               btnDraft.SetEnabled(false);
               btnSubmit.SetEnabled(false);
               cbSuppNo.SetEnabled(false);
               memoConsi.SetEnabled(false);
               memoNote.SetEnabled(false);
               btnQCD.SetEnabled(true);
           }
           else if (status == "3") {
               btnDraft.SetEnabled(false);
               btnSubmit.SetEnabled(false);
               cbSuppNo.SetEnabled(false);
               memoConsi.SetEnabled(false);
               memoNote.SetEnabled(false);
               btnQCD.SetEnabled(true);
           }
           else if (status == "4") {
               btnSubmit.SetEnabled(false);
               btnQCD.SetEnabled(true);
           }
           else if (status == "") {
               btnSubmit.SetEnabled(false);
               btnPrint.SetEnabled(false);
               btnQCD.SetEnabled(false);

           }

       }


       function SubmitProcess(s, e) {
           var msg = confirm('Are you sure to SUBMIT with this QCDMR Configuration?');
           if (msg == false) {
               e.processOnServer = false;
               return;
           }
           else {
               cbSubmit.PerformCallback();
               Grid.PerformCallback('load|' + txtParameter.GetText());
               Grid_Delivery.PerformCallback('load|' + txtParameter.GetText());
               Grid_Quality.PerformCallback('load|' + txtParameter.GetText());


           }
       }


       function DraftProcess() {
           btnQCD.SetEnabled(false);
           if (txtFlag.GetText() == '0') {
               if (cboCPNumber.GetText() == '') {
                   cboCPNumber.Focus();
                   toastr.warning("Please select CP Number first!", "Warning");
                   toastr.options.closeButton = false;
                   toastr.options.debug = false;
                   toastr.options.newestOnTop = false;
                   toastr.options.progressBar = false;
                   toastr.options.preventDuplicates = true;
                   toastr.options.onclick = null;
                   e.processOnServer = false;
                   return false;
               }

               if (cboCPNumber.GetSelectedIndex() == -1) {
                   cboCPNumber.Focus();
                   toastr.warning('Invalid CP Number!', 'Warning');
                   toastr.options.closeButton = false;
                   toastr.options.debug = false;
                   toastr.options.newestOnTop = false;
                   toastr.options.progressBar = false;
                   toastr.options.preventDuplicates = true;
                   toastr.options.onclick = null;
                   e.processOnServer = false;
                   return false;
               }

               if (cbSuppNo.GetText() == '') {
                   cbSuppNo.Focus();
                   toastr.warning("Please select Supplier Recommendation!", "Warning");
                   toastr.options.closeButton = false;
                   toastr.options.debug = false;
                   toastr.options.newestOnTop = false;
                   toastr.options.progressBar = false;
                   toastr.options.preventDuplicates = true;
                   toastr.options.onclick = null;
                   e.processOnServer = false;
                   return false;
               }

               if (memoConsi.GetText() == '') {
                   memoConsi.Focus();
                   toastr.warning("Please Input Consider!", "Warning");
                   toastr.options.closeButton = false;
                   toastr.options.debug = false;
                   toastr.options.newestOnTop = false;
                   toastr.options.progressBar = false;
                   toastr.options.preventDuplicates = true;
                   toastr.options.onclick = null;
                   e.processOnServer = false;
                   return false;
               }

           }

           var msg = confirm('Are you sure want to draft this data ?');
           if (msg == false) {
               e.processOnServer = false;
               return;
           }
           else {

               Grid.UpdateEdit();
               Grid_Delivery.UpdateEdit();
               Grid_Quality.UpdateEdit();

               millisecondsToWait = 1000;
               setTimeout(function () {
                   Grid.PerformCallback('save|');
                   Grid_Delivery.PerformCallback('save|');
                   Grid_Quality.PerformCallback('save|');
               }
                , millisecondsToWait);

               setTimeout(function () {
                   cbDraf.PerformCallback();
               }, millisecondsToWait);


               millisecondsToWait = 1000;
               setTimeout(function () {
                   cbDraf.PerformCallback();
               }, millisecondsToWait);

               setTimeout(function () {
                   //Grid.PerformCallback('draft|' + txtSRNumber.GetText() + '|' + txtSRNumber.GetText() + '|' + cboCPNumber.GetText());
                   Grid.PerformCallback('draft|' + txtParameter.GetText());
               }, 100);

               setTimeout(function () {
                   Grid_Delivery.PerformCallback('draft|' + txtParameter.GetText());
               }, 100);
               setTimeout(function () {
                   Grid_Quality.PerformCallback('draft|' + txtParameter.GetText());
               }, 100);

           }
           btnQCD.SetEnabled(true);
       }



       function warningsave(s, e) {
           // alert("rowIndex = " + e.visibleIndex + "; columnIndex = " + e.focusedColumn.index);

           //untuk Coloumn Code
           if (((e.visibleIndex == 1) && (e.focusedColumn.index == 2)) == '') {
               toastr.warning("Please Input !", "Warning");
               toastr.options.closeButton = false;
               toastr.options.debug = false;
               toastr.options.newestOnTop = false;
               toastr.options.progressBar = false;
               toastr.options.preventDuplicates = true;
               toastr.options.onclick = null;
               e.processOnServer = false;
               return false;
           }

       }

       function OnStartEditing(s, e) {
           // alert("rowIndex = " + e.visibleIndex + "; columnIndex = " + e.focusedColumn.index);

           //untuk Coloumn Code
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 0)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 1) && (e.focusedColumn.index == 0)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 2) && (e.focusedColumn.index == 0)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 3) && (e.focusedColumn.index == 0)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 4) && (e.focusedColumn.index == 0)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           //untuk Coloumn Information
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 1)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();

           }

           if ((e.visibleIndex == 1) && (e.focusedColumn.index == 1)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 2) && (e.focusedColumn.index == 1)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 3) && (e.focusedColumn.index == 1)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }



           //untuk Coloumn Quotation1
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 4)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           //untuk Coloumn Final1
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 5)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 1) && (e.focusedColumn.index == 5)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 2) && (e.focusedColumn.index == 5)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 3) && (e.focusedColumn.index == 5)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 4) && (e.focusedColumn.index == 5)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }



           //untuk Coloumn Quotation2
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 7)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();

           }

           //untuk Coloumn Final2
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 8)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 1) && (e.focusedColumn.index == 8)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 2) && (e.focusedColumn.index == 8)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 3) && (e.focusedColumn.index == 8)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 4) && (e.focusedColumn.index == 8)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           //untuk Coloumn Quotation3
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 11)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           //untuk Coloumn Final3
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 12)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 1) && (e.focusedColumn.index == 12)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 2) && (e.focusedColumn.index == 12)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 3) && (e.focusedColumn.index == 12)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 4) && (e.focusedColumn.index == 12)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           //untuk Coloumn Quotation4
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 15)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           //untuk Coloumn Final4
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 16)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 1) && (e.focusedColumn.index == 16)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 2) && (e.focusedColumn.index == 16)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 3) && (e.focusedColumn.index == 16)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 4) && (e.focusedColumn.index == 16)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           //untuk Coloumn Quotation5
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 20)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           //untuk Coloumn Final5
           if ((e.visibleIndex == 0) && (e.focusedColumn.index == 21)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 1) && (e.focusedColumn.index == 21)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 2) && (e.focusedColumn.index == 21)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 3) && (e.focusedColumn.index == 21)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }

           if ((e.visibleIndex == 4) && (e.focusedColumn.index == 21)) {
               e.cancel = true;
               Grid.batchEditApi.EndEdit();
           }
       }

       function FillComboSupplier() {
           cbSuppNo.PerformCallback(cboCPNumber.GetText());
       }

       function GridLoad() {
           //cboSupplier.PerformCallback(cboRFQSetNumber.GetText());
           Grid.PerformCallback('gridload|xx|0|' + cboCPNumber.GetText());
           Grid_Delivery.PerformCallback('gridload|xx|0|' + cboCPNumber.GetText());
           Grid_Quality.PerformCallback('gridload|xx|0|' + cboCPNumber.GetText());
           cbSuppNo.PerformCallback(cboCPNumber.GetText());
           //Grid.PerformCallback();
       }

       function GridLoadData() {
           cbSuppNo.PerformCallback(cboCPNumber.GetText());
           // Grid.PerformCallback('gridload|xx|0|' + cboCPNumber.GetText());
           //Grid.PerformCallback();
       }

       function GridLoadCompleted(s, e) {
//           CostLabelSup1.SetText(s.cpHeaderCaption1);
//           CostLabelSup2.SetText(s.cpHeaderCaption2);
//           CostLabelSup3.SetText(s.cpHeaderCaption3);
//           CostLabelSup4.SetText(s.cpHeaderCaption4);
//           CostLabelSup5.SetText(s.cpHeaderCaption5);


           CostLabelSup1.SetText(s.cpGridCostCaption1);
           CostLabelSup2.SetText(s.cpGridCostCaption2);
           CostLabelSup3.SetText(s.cpGridCostCaption3);
           CostLabelSup4.SetText(s.cpGridCostCaption4);
           CostLabelSup5.SetText(s.cpGridCostCaption5);

           DeliveryLabelSup1.SetText(s.cpHeaderCaption1);
           DeliveryLabelSup2.SetText(s.cpHeaderCaption2);
           DeliveryLabelSup3.SetText(s.cpHeaderCaption3);
           DeliveryLabelSup4.SetText(s.cpHeaderCaption4);
           DeliveryLabelSup5.SetText(s.cpHeaderCaption5);

           QualityLabelSup1.SetText(s.cpHeaderCaption1);
           QualityLabelSup2.SetText(s.cpHeaderCaption2);
           QualityLabelSup3.SetText(s.cpHeaderCaption3);
           QualityLabelSup4.SetText(s.cpHeaderCaption4);
           QualityLabelSup5.SetText(s.cpHeaderCaption5);

       }

       function OnTabChanging(s, e) {
           var tabName = (pageControl.GetActiveTab()).name;
           e.cancel = !ASPxClientEdit.ValidateGroup('group' + tabName);
       }

    </script>
    <style type="text/css">
        .colwidthbutton
        {
            width: 90px;
        }
        
        .colwidthbutton2
        {
            width: 130px;
        }
        
        .hidden-div
        {
            display: none;
        }
        
        .rowheight
        {
            height: 35px;
        }
        
        .col1
        {
            width: 10px;
        }
        .colLabel1
        {
            width: 150px;
        }
        .colLabel2
        {
            width: 113px;
        }
        .colInput1
        {
            width: 220px;
        }
        .colInput2
        {
            width: 133px;
        }
        .colSpace
        {
            width: 50px;
        }
        
        .customHeader
        {
            height: 15px;
        }
        .style1
        {
            width: 153px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr>
                    <td colspan="9" style="height: 10px">
                    </td>
                </tr>
                <tr class="rowheight">
                    <td class="col1">
                    </td>
                    <td class="colLabel1">
                        <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier Recommend No.">
                        </dx1:ASPxLabel>
                    </td>
                    <td style="width: 200px">
                        <dx1:ASPxTextBox ID="txtSRNumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="170px" ClientInstanceName="txtSRNumber" MaxLength="60" Height="30px" ReadOnly="True"
                            Font-Bold="True" BackColor="Silver">
                        </dx1:ASPxTextBox>
                    </td>
                    <td style="width: 35px">
                        <dx1:ASPxTextBox ID="txtRev" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="50px" ClientInstanceName="txtRev" MaxLength="20" Height="30px" ReadOnly="True"
                            Font-Bold="True" ForeColor="Black" HorizontalAlign="Center" BackColor="Silver">
                        </dx1:ASPxTextBox>
                    </td>
                    <td class="colSpace">
                    </td>
                    <td class="colLabel2">
                        <dx1:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier Recommend Date">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="colInput2">
                        <dx:ASPxDateEdit ID="dtSRDate" runat="server" Theme="Office2010Black" Width="120px"
                            AutoPostBack="false" ClientInstanceName="dtSRDate" EditFormatString="dd-MMM-yyyy"
                            DisplayFormatString="dd-MMM-yyyy" Font-Names="Segoe UI" Font-Size="9pt" 
                            Height="25px" ReadOnly="True"
                            Font-Bold="True" ClientEnabled="False">
                            <CalendarProperties>
                                <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                </HeaderStyle>
                                <DayStyle Font-Size="9pt" Paddings-Padding="5px" >
<Paddings Padding="5px"></Paddings>
                                </DayStyle>
                                <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
<Paddings Padding="5px"></Paddings>
                                </WeekNumberStyle>
                                <FooterStyle Font-Size="9pt" Paddings-Padding="10px" >
<Paddings Padding="10px"></Paddings>
                                </FooterStyle>
                                <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
<Paddings Padding="10px"></Paddings>
                                </ButtonStyle>
                                <FastNavProperties Enabled="False" />
                            </CalendarProperties>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxDateEdit>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="rowheight">
                    <td>
                    </td>
                    <td>
                        <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="CP Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td colspan="2">
                        <dx1:ASPxComboBox ID="cboCPNumber" runat="server" ClientInstanceName="cboCPNumber"
                            Width="200px" Font-Names="Segoe UI" TextField="CP_Number" ValueField="CP_Number"
                            TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="GridLoad" />
                            <Columns>
                                <dx:ListBoxColumn Caption="CP Number" FieldName="CP_Number" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td></td>
                    <td class="style1">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr style="display: none">
                    <td></td>
                    <td></td>
                    <td>
                        <dx1:ASPxTextBox ID="txtParameter" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="200px" ClientInstanceName="txtParameter" MaxLength="20" Height="30px"
                            ReadOnly="True" Font-Bold="True" ForeColor="Black" HorizontalAlign="Center">
                        </dx1:ASPxTextBox>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="3">
                        <dx1:ASPxTextBox ID="txtFlag" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="50px" ClientInstanceName="txtFlag" MaxLength="20" Height="30px" ReadOnly="True"
                            Font-Bold="True" ForeColor="Black" HorizontalAlign="Center">
                        </dx1:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px">
                    </td>
                    <td></td>
                    <td colspan="2">
                        <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                            <ClientSideEvents Init="GetMessage" />
                        </dx:ASPxCallback>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                        &nbsp; &nbsp;&nbsp;<dx:ASPxHiddenField ID="hf" runat="server" ClientInstanceName="hf">
                        </dx:ASPxHiddenField>
                        <dx:ASPxCallback ID="cbDraf" runat="server" ClientInstanceName="cbDraf">
                            <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
                        </dx:ASPxCallback>
                        <dx:ASPxCallback ID="cbSubmit" runat="server" ClientInstanceName="cbSubmit">
                            <ClientSideEvents Init="GetMessage" EndCallback="GetMessage" />
                        </dx:ASPxCallback>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxPageControl ID="pageControl" runat="server" ClientInstanceName="pageControl"
                ActiveTabIndex="0" EnableHierarchyRecreation="True" Width="100%">
                <ClientSideEvents ActiveTabChanging="OnTabChanging" />
                <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                </ActiveTabStyle>
                <TabPages>
                    <dx:TabPage Name="Cost" Text="Cost">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControlTab" runat="server">
                                <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                                    EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
                                    Font-Size="9pt" KeyFieldName="Information_Code">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="Information_Code" VisibleIndex="0"
                                            Width="0px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Information" FieldName="Information" VisibleIndex="1"
                                            Width="150px">
                                        </dx:GridViewDataTextColumn>
                                        
                                       <dx:GridViewBandColumn Caption="Supplier 1"  Name="Supplier1" VisibleIndex="2">
                                            <%--<HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="CostLabelSup1" ClientInstanceName="CostLabelSup1" runat="server" Text="Supplier 1"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier1" VisibleIndex="0"
                                                    Width="0.1px">
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Quotation" FieldName="Quotation1" VisibleIndex="1"
                                                    Width="250px">
                                                   </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Final" FieldName="Final1" VisibleIndex="2" Width="80px">
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 2" Name="Supplier2" VisibleIndex="3">
                                           <%-- <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="CostLabelSup2" ClientInstanceName="CostLabelSup2" runat="server" Text="Supplier 2"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Quotation" FieldName="Quotation2" VisibleIndex="1"
                                                    Width="250px">
                                                    
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Final" FieldName="Final2" VisibleIndex="2" Width="80px">
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier2" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 3" Name="Supplier3" VisibleIndex="4">
                                           <%-- <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="CostLabelSup3" ClientInstanceName="CostLabelSup3" runat="server" Text="Supplier 3"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Quotation" FieldName="Quotation3" VisibleIndex="1"
                                                    Width="250px">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Final" FieldName="Final3" VisibleIndex="2" Width="80px">
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier3" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 4" Name="Supplier4" VisibleIndex="5">
                                            <%--<HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="CostLabelSup4" ClientInstanceName="CostLabelSup4" runat="server" Text="Supplier 4"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Quotation" FieldName="Quotation4" VisibleIndex="1"
                                                    Width="250px">
                                                    
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Final" FieldName="Final4" VisibleIndex="2" Width="80px">
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier4" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 5" Name="Supplier5" VisibleIndex="6">
                                            <%--<HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="CostLabelSup5" ClientInstanceName="CostLabelSup5" runat="server" Text="Supplier 5"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier5" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Quotation" FieldName="Quotation5" VisibleIndex="1"
                                                    Width="250px">
                                                    
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Final" FieldName="Final5" VisibleIndex="2" Width="80px">
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>
                                    <Settings VerticalScrollBarMode="Visible" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" />
                                    <SettingsPager Mode="ShowAllRecords">
                                    </SettingsPager>
                                    <SettingsBehavior AllowSelectByRowClick="True" AllowSort="False" AllowDragDrop="false"
                                        ColumnResizeMode="Control" />
                                    <SettingsEditing Mode="Batch">
                                        <BatchEditSettings StartEditAction="Click" ShowConfirmOnLosingChanges="False" />
                                    </SettingsEditing>
                                    <ClientSideEvents BatchEditStartEditing="OnStartEditing" />
                                    <Styles Header-Paddings-Padding="1px" EditFormColumnCaption-Paddings-PaddingLeft="0px"
                                        EditFormColumnCaption-Paddings-PaddingRight="0px">
                                        <Header CssClass="customHeader" HorizontalAlign="Center">
                                            <Paddings Padding="1px"></Paddings>
                                        </Header>
                                        <EditFormColumnCaption>
                                            <Paddings PaddingLeft="0px" PaddingRight="0px"></Paddings>
                                        </EditFormColumnCaption>
                                    </Styles>
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Delivery" Text="Delivery">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl1" runat="server">
                                <dx:ASPxGridView ID="Grid_Delivery" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid_Delivery"
                                    EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
                                    Font-Size="9pt" KeyFieldName="Information_Code">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="Information_Code" VisibleIndex="0"
                                            Width="0px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Information" FieldName="Information" VisibleIndex="1"
                                            Width="150px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 1"  Name="Supplier1" VisibleIndex="2">
                                            <%--<HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="DeliveryLabelSup1" ClientInstanceName="DeliveryLabelSup1" runat="server" Text="Supplier 1"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier1" VisibleIndex="0"
                                                    Width="0.1px">
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption=" " FieldName="Quotation1" VisibleIndex="1" Width="250px">
                                                    
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 2" Name="Supplier2" VisibleIndex="3">
                                           <%-- <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="DeliveryLabelSup2" ClientInstanceName="DeliveryLabelSup2" runat="server" Text="Supplier 2"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption=" " FieldName="Quotation2" VisibleIndex="1" Width="250px">
                                                   
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier2" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 3" Name="Supplier3" VisibleIndex="4">
                                           <%-- <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="DeliveryLabelSup3" ClientInstanceName="DeliveryLabelSup3" runat="server" Text="Supplier 3"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption=" " FieldName="Quotation3" VisibleIndex="1" Width="250px">
                                                   
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier3" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 4" Name="Supplier4" VisibleIndex="5">
                                           <%-- <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="DeliveryLabelSup4" ClientInstanceName="DeliveryLabelSup4" runat="server" Text="Supplier 4"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption=" " FieldName="Quotation4" VisibleIndex="1" Width="250px">
                                                   
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier4" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 5" Name="Supplier5" VisibleIndex="6">
                                            <%--<HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="DeliveryLabelSup5" ClientInstanceName="DeliveryLabelSup5" runat="server" Text="Supplier 5"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier5" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption=" " FieldName="Quotation5" VisibleIndex="1" Width="250px">
                                                   
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>
                                    <Settings VerticalScrollBarMode="Visible" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" />
                                    <SettingsPager Mode="ShowAllRecords">
                                    </SettingsPager>
                                    <SettingsBehavior AllowSelectByRowClick="True" AllowSort="False" AllowDragDrop="false"
                                        ColumnResizeMode="Control" />
                                    <SettingsEditing Mode="Batch">
                                        <BatchEditSettings StartEditAction="Click" ShowConfirmOnLosingChanges="False" />
                                    </SettingsEditing>
                                    <%--<ClientSideEvents EndCallback="GridLoadCompleted" />--%>
                                    <Styles Header-Paddings-Padding="1px" EditFormColumnCaption-Paddings-PaddingLeft="0px"
                                        EditFormColumnCaption-Paddings-PaddingRight="0px">
                                        <Header CssClass="customHeader" HorizontalAlign="Center">
                                            <Paddings Padding="1px"></Paddings>
                                        </Header>
                                        <EditFormColumnCaption>
                                            <Paddings PaddingLeft="0px" PaddingRight="0px"></Paddings>
                                        </EditFormColumnCaption>
                                    </Styles>
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Name="Quality" Text="Quality">
                        <ActiveTabStyle Font-Names="Segoe UI" Font-Size="9pt">
                        </ActiveTabStyle>
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl2" runat="server">
                                <dx:ASPxGridView ID="Grid_Quality" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid_Quality"
                                    EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
                                    Font-Size="9pt" KeyFieldName="Information_Code">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="Information_Code" VisibleIndex="0"
                                            Width="0px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Information" FieldName="Information" VisibleIndex="1"
                                            Width="150px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 1" Name="Supplier1" VisibleIndex="2">
                                            <%--<HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="QualityLabelSup1" ClientInstanceName="QualityLabelSup1" runat="server" Text="Supplier 1"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier1" VisibleIndex="0"
                                                    Width="0.1px">
                                                    <CellStyle HorizontalAlign="Right">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption=" " FieldName="Quotation1" VisibleIndex="1" Width="250px">
                                                    
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 2" Name="Supplier2" VisibleIndex="3">
                                          <%--  <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="QualityLabelSup2" ClientInstanceName="QualityLabelSup2" runat="server" Text="Supplier 2"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption=" " FieldName="Quotation2" VisibleIndex="1" Width="250px">
                                                    
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier2" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 3" Name="Supplier3" VisibleIndex="4">
                                         <%--   <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="QualityLabelSup3" ClientInstanceName="QualityLabelSup3" runat="server" Text="Supplier 3"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption=" " FieldName="Quotation3" VisibleIndex="1" Width="250px">
                                                    
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier3" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 4" Name="Supplier4" VisibleIndex="5">
                                          <%--  <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="QualityLabelSup4" ClientInstanceName="QualityLabelSup4" runat="server" Text="Supplier 4"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption=" " FieldName="Quotation4" VisibleIndex="1" Width="250px">
                                                    
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier4" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Supplier 5" Name="Supplier5" VisibleIndex="6">
                                           <%-- <HeaderCaptionTemplate>
                                                <dx:ASPxLabel ID="QualityLabelSup5" ClientInstanceName="QualityLabelSup5" runat="server" Text="Supplier 5"
                                                    Font-Names="Segoe UI" Font-Size="9pt">
                                                </dx:ASPxLabel>
                                            </HeaderCaptionTemplate>--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Supplier" FieldName="Supplier5" ShowInCustomizationForm="True"
                                                    VisibleIndex="0" Width="0.1px">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption=" " FieldName="Quotation5" VisibleIndex="1" Width="250px">
                                                    
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                    </Columns>
                                    <Settings VerticalScrollBarMode="Visible" ShowStatusBar="Hidden" HorizontalScrollBarMode="Visible" />
                                    <SettingsPager Mode="ShowAllRecords">
                                    </SettingsPager>
                                    <SettingsBehavior AllowSelectByRowClick="True" AllowSort="False" AllowDragDrop="false"
                                        ColumnResizeMode="Control" />
                                    <SettingsEditing Mode="Batch">
                                        <BatchEditSettings StartEditAction="Click" ShowConfirmOnLosingChanges="False" />
                                    </SettingsEditing>
                                    <%--<ClientSideEvents EndCallback="GridLoadCompleted" />--%>
                                    <Styles Header-Paddings-Padding="1px" EditFormColumnCaption-Paddings-PaddingLeft="0px"
                                        EditFormColumnCaption-Paddings-PaddingRight="0px">
                                        <Header CssClass="customHeader" HorizontalAlign="Center">
                                            <Paddings Padding="1px"></Paddings>
                                        </Header>
                                        <EditFormColumnCaption>
                                            <Paddings PaddingLeft="0px" PaddingRight="0px"></Paddings>
                                        </EditFormColumnCaption>
                                    </Styles>
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
            </dx:ASPxPageControl>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr>
                    <td colspan="4" style="height: 10px">
                    </td>
                </tr>
                <tr>
                    <td class="col1">
                    </td>
                    <td colspan="3">
                        <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier Recommendation :">
                        </dx1:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <dx1:ASPxComboBox ID="cbSuppNo" runat="server" ClientInstanceName="cbSuppNo" Width="280px"
                        Font-Names="Segoe UI"  TextField="Description" ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" 
                            Height="25px">
                             <ClientSideEvents Validation=""  />
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Supplier Name" FieldName="Description" Width="200px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="col1">
                    </td>
                    <td colspan="3">
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Consideration :">
                        </dx1:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <dx:ASPxMemo ID="memoConsi" runat="server" Height="45px" Width="100%" ClientInstanceName="memoConsi"
                            MaxLength="300">
                            
                        </dx:ASPxMemo>
                    </td>
                    <td class="col1">
                    </td>
                </tr>
                <tr>
                    <td class="col1">
                    </td>
                    <td colspan="3">
                        <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Note :">
                        </dx1:ASPxLabel>
                    </td>
                </tr>
                <tr >
                    <td></td>
                    <td colspan="2">
                        <dx:ASPxMemo ID="memoNote" runat="server" Height="45px" Width="100%" ClientInstanceName="memoNote"
                            MaxLength="300">
                            
                        </dx:ASPxMemo>
                    </td>
                    <td class="col1">
                    </td>
                </tr>
                <tr style="display:none">
                    <td></td>
                    <td colspan="2" align="right">
                        <dx1:ASPxLabel ID="lblLenght" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            ClientInstanceName="lblLenght" Text="0/100">
                        </dx1:ASPxLabel>
                    </td>
                    <td></td>
                </tr>
                <tr style="height: 10px">
                    <td colspan="4">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <table>
                            <tr class="rowheight">
                                <td class="colwidthbutton">
                                    <dx1:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="False" Font-Names="Segoe UI"
                                        Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack" Theme="Default">
                                        <Paddings Padding="2px" />
                                    </dx1:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    <dx1:ASPxButton ID="btnDraft" runat="server" Text="Draft" Font-Names="Segoe UI" Font-Size="9pt"
                                        Width="80px" Height="25px" AutoPostBack="False" ClientInstanceName="btnDraft"
                                        Theme="Default">
                                        <ClientSideEvents Click="DraftProcess" />
                                        <Paddings Padding="2px" />
                                    </dx1:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    <dx1:ASPxButton ID="btnQCD" runat="server" Text="QCD" AutoPostBack="False"
                                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnQCD"
                                        Theme="Default">
                                        <Paddings Padding="2px" />
                                    </dx1:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    <dx1:ASPxButton ID="btnSubmit" runat="server" Text="Submit" Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                        ClientInstanceName="btnSubmit" Theme="Default">
                                        <ClientSideEvents Click="SubmitProcess" />
                                        <Paddings Padding="2px" />
                                    </dx1:ASPxButton>
                                </td>
                                <td class="colwidthbutton">
                                    <dx1:ASPxButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="False" Font-Names="Segoe UI"
                                        Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnPrint" Theme="Default">
                                        <Paddings Padding="2px" />
                                    </dx1:ASPxButton>
                                </td>
                                
                            </tr>
                        </table>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4">
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="height: 10px">
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddCrudgeOil.aspx.vb" Inherits="IAMI_EPIC2.AddCrudgeOil" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;


            cboCategoryMaterial.SetEnabled(false);
            dtPeriod.SetEnabled(false);
           
            //txtMaterialName.SetText('');
            //txtSpecification.SetText('');
            //cboUoM.SetText('');
           // txtPrice.SetText('');
            
           
        }
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/ItemList.aspx";
            cboCategoryMaterial.SetEnabled ( false);
            dtPeriod.SetEnabled( false);
           

        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

      
//            txtMaterialName.SetText('');
//            txtSpecification.SetText('');
            cboUoM.SetText('');
            txtPrice.SetText('');
        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }
    function GridLoad() {
        Grid.PerformCallback('refresh');
        tmpProjectType.SetText(cboProjectType.GetValue());
    };
    function fn_AllowonlyNumeric(s, e) {
        var theEvent = e.htmlEvent || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]/;

        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault)
                theEvent.preventDefault();
        }
    };
    function MessageDelete(s, e) {

        if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/ItemList.aspx";

        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
    }

    function FilterGroup() {
        cboGroupItem.PerformCallback('filter|' + cboPRType.GetValue());
    }

    function FilterCategory() {
        cboCategory.PerformCallback('filter|' + cboGroupItem.GetValue());
    }
    function SetCode(s, e) {
        tmpProjectType.SetText(cboProjectType.GetValue());
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
 <table style="width: 100%;">
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 70px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Oil Type">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 15px;">
                <dx:ASPxComboBox ID="cboCategoryMaterial" runat="server" ClientInstanceName="cboCategoryMaterial"
                    Width="170px" Font-Names="Segoe UI" TextField="Description" ValueField="Code" DataSourceID="SqlDataSource5"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'OilType'">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 70px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="UOM">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboUoM" runat="server" ClientInstanceName="cboUoM"
                    Width="170px" Font-Names="Segoe UI" TextField="Description" 
                    ValueField="Code" DataSourceID="SqlDataSource6"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                    Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'UoM'">
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 70px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Price">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtPrice" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtPrice" MaxLength="11" HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>.<00000..99>" />
                    <ValidationSettings Display="Dynamic"></ValidationSettings>
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 70px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;</td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxDateEdit ID="dtPeriod" runat="server" Theme="Office2010Black" 
                                        Width="120px" AutoPostBack="false" ClientInstanceName="dtPeriod"
                                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" >
                                        <CalendarProperties>
                                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                                        </CalendarProperties>
                                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                                    </dx:ASPxDateEdit>   
                    <%--
                        <dx:ASPxTimeEdit ID="TmPeriod" EditFormat="Custom" 
                            EditFormatString="MMM yyyy" DisplayFormatString="MMM yyyy"  
                            Theme="Office2010Black" runat="server" 
                            Height="22px">
                        </dx:ASPxTimeEdit>
--%>
                    </td>
        </tr>
       <tr>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
       </tr>
        <tr style="height: 25px;">
            <td style="width: 50px;">
                &nbsp;
            </td>
            <td style="width: 50px">
                &nbsp;
            </td>
            <td style="width: 500px">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack"
                    Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>

                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnUpdate" runat="server" Text="Update" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnUpdate" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>

            </td>
            <td>
              
                <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
                    <ClientSideEvents CallbackComplete="SetCode" />
                </dx:ASPxCallback>
                <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                    <ClientSideEvents Init="MessageError" />
                </dx:ASPxCallback>
                   <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
                    <ClientSideEvents Init="MessageError" />                                           
                </dx:ASPxCallback>
                   <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
                    <ClientSideEvents CallbackComplete="MessageDelete" />                                           
                </dx:ASPxCallback>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>

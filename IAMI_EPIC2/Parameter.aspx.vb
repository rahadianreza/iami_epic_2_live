﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class Parameter
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False

#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            up_GridLoad("1", GridDepart, "Department")
            up_GridLoad("0", GridSection, "Section", "Department")
            up_GridLoad("0", GridCostCenter, "CostCenter", "Department")
            up_GridLoad("0", GridGroupItem, "GroupItem", "PRType")
            up_GridLoad("0", GridCategory, "Category", "GroupItem")
            up_GridLoad("1", GridUOM, "UOM")
            up_GridLoad("1", GridCurr, "Currency")
            up_GridLoad("1", GridSupplier, "SupplierType")
            up_GridLoad("1", GridProvinsi, "Provinsi")
            up_GridLoad("0", GridKab, "Kabupaten", "Provinsi")
            up_GridLoad("1", GridLanguage, "Language")
            up_GridLoad("1", GridPeriod, "Period")
            up_GridLoad("1", GridPRType, "PRType")
            up_GridLoad("1", GridJobPos, "JobPosition")
			up_GridLoad("1", GridProjectType, "ProjectType")

            up_GridLoad("1", GridBank, "Bank")
            up_GridLoad("1", GridMaterialType, "MaterialType")
            up_GridLoad("0", GridMaterialCode, "MaterialCode", "MaterialCategory")
            up_GridLoad("0", GridMaterialGroup, "MaterialGroupItem", "MaterialType")
            up_GridLoad("0", GridMaterialCategoryItem, "MaterialGroupCategory", "MaterialGroupItem")
            up_GridLoad("1", GridMaterialPriceType, "MaterialPriceType")
            up_GridLoad("1", GridOilType, "OilType")
            up_GridLoad("1", GridPRBudget, "PRBudget")
            up_GridLoad("1", GridRateClass, "RateClass")
            up_GridLoad("1", GridSectoral, "Sectoral")
            up_GridLoad("0", GridVAKind, "VAKind", "RateClass")
            up_GridLoad("1", GridProcess, "Process")

        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A020")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0, GridDepart)
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A020")
        If AuthUpdate = False Then
            'Dim commandColumn = TryCast(GridDepart.Columns(0), GridViewCommandColumn)
            'commandColumn.Visible = False
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String, ByVal pGrid As ASPxGridView, ByVal pGroupID As String, Optional ByVal pParentGroupID As String = "")
        Dim ErrMsg As String = ""
        Dim Ses As New List(Of ClsParameter)
        If pGroupID = "GroupItem" Or pGroupID = "Category" Or pGroupID = "Kabupaten" Or pGroupID = "Section" Or pGroupID = "CostCenter" Or pGroupID = "VAKind" Or _
            pGroupID = "MaterialGroupCategory" Or pGroupID = "MaterialGroupItem" Then 'Or pGroupID = "MaterialCode" Then
            a = "0"
        End If
        Ses = ClsParameterDB.getlist(a, pGroupID, pParentGroupID, ErrMsg)
        If ErrMsg = "" Then
            pGrid.DataSource = Ses
            pGrid.DataBind()
            'pGrid.Columns.Item(0).HeaderStyle.BackColor = Color.Coral

        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_ExportExcel(ByVal pGrid As ASPxGridView)

    End Sub
#End Region

#Region "Control Event"
    Protected Sub GridProcess_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridProcess.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridProcess, "Process")
        End If
    End Sub

    Protected Sub GridDepart_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridDepart.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridDepart, "Department")
        End If
    End Sub

    Protected Sub GridSection_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridSection.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("0", GridSection, "Section", "Department")
        End If
    End Sub

    Protected Sub GridCostCenter_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridCostCenter.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("0", GridCostCenter, "CostCenter", "Department")
        End If
    End Sub

    Protected Sub GridGroupItem_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridGroupItem.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("0", GridGroupItem, "GroupItem", "PRType")
        End If
    End Sub

    Protected Sub GridCategory_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridCategory.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("0", GridCategory, "Category", "GroupItem")
        End If
    End Sub

    Protected Sub GridUOM_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridUOM.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridUOM, "UOM")
        End If
    End Sub

    Protected Sub GridCurr_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridCurr.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridCurr, "Currency")
        End If
    End Sub

    Protected Sub GridSupplier_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridSupplier.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridSupplier, "SupplierType")
        End If
    End Sub

    Protected Sub GridProvinsi_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridProvinsi.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridProvinsi, "Provinsi")
        End If
    End Sub

    Protected Sub GridKab_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridKab.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("0", GridKab, "Kabupaten", "Provinsi")
        End If
    End Sub

    Protected Sub GridLanguage_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridLanguage.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridKab, "Language")
        End If
    End Sub

    Protected Sub GridPeriod_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridPeriod.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridKab, "Period")
        End If
    End Sub

    Protected Sub GridPRType_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridPRType.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridPRType, "PRType")
        End If
    End Sub

    Protected Sub GridJobPos_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridJobPos.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridJobPos, "JobPosition")
        End If
    End Sub
    Protected Sub GridProjectType_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridProjectType.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridProjectType, "ProjectType")
        End If
    End Sub

    Protected Sub GridBank_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridBank.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridBank, "Bank")
        End If
    End Sub

    Protected Sub GridMaterialType_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridMaterialType.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridMaterialType, "MaterialType")
        End If
    End Sub

    Protected Sub GridMaterialCode_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridMaterialCode.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("0", GridMaterialCode, "MaterialCode", "MaterialCategory")
        End If
    End Sub

    Protected Sub GridMaterialGroup_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridMaterialGroup.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("0", GridMaterialGroup, "MaterialGroupItem", "MaterialType")
        End If
    End Sub

    Protected Sub GridMaterialCategoryItem_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridMaterialCategoryItem.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("0", GridMaterialCategoryItem, "MaterialGroupCategory", "MaterialGroupItem")
        End If
    End Sub

    Protected Sub GridMaterialPriceType_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridMaterialPriceType.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridMaterialPriceType, "MaterialType")
        End If
    End Sub

    Protected Sub GridOilType_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridOilType.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridOilType, "OilType")
        End If
    End Sub

    Protected Sub GridPRBudget_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridPRBudget.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridPRBudget, "PRBudget")
        End If
    End Sub

    Protected Sub GridRateClass_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridRateClass.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridRateClass, "RateClass")
        End If
    End Sub

    Protected Sub GridSectoral_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridSectoral.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("1", GridSectoral, "Sectoral")
        End If
    End Sub

    Protected Sub GridVAKind_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles GridVAKind.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad("0", GridVAKind, "VAKind", "RateClass")
        End If
    End Sub

    Protected Sub GridProcess_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Process",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridProcess)
        Else
            GridProcess.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridProcess)
            up_GridLoad("1", GridProcess, "Process")
        End If
    End Sub

    Protected Sub GridDepart_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Department",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridDepart)
        Else
            GridDepart.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridDepart)
            up_GridLoad("1", GridDepart, "Department")
        End If
    End Sub

    Protected Sub GridSection_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "Section",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridSection)
        Else
            GridSection.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridSection)
            up_GridLoad("0", GridSection, "Section", "Department")
        End If
    End Sub

    Protected Sub GridCostCenter_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "CostCenter",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCostCenter)
        Else
            GridCostCenter.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridCostCenter)
            up_GridLoad("0", GridCostCenter, "CostCenter", "Department")
        End If
    End Sub

    Protected Sub GridGroupItem_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "GroupItem",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridGroupItem)
        Else
            GridGroupItem.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridGroupItem)
            up_GridLoad("1", GridGroupItem, "GroupItem")
        End If
    End Sub

    Protected Sub GridCategory_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "Category",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCategory)
        Else
            GridCategory.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridCategory)
            up_GridLoad("0", GridCategory, "Category", "GroupItem")
        End If
    End Sub

    Protected Sub GridUOM_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "UOM",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridUOM)
        Else
            GridUOM.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridUOM)
            up_GridLoad("1", GridUOM, "UOM")
        End If
    End Sub

    Protected Sub GridCurr_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Currency",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCurr)
        Else
            GridCurr.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridCurr)
            up_GridLoad("1", GridCurr, "Department")
        End If
    End Sub

    Protected Sub GridSupplier_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "SupplierType",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridSupplier)
        Else
            GridSupplier.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridSupplier)
            up_GridLoad("1", GridSupplier, "Department")
        End If
    End Sub

    Protected Sub GridProvinsi_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Provinsi",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridProvinsi)
        Else
            GridProvinsi.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridProvinsi)
            up_GridLoad("1", GridProvinsi, "Department")
        End If
    End Sub

    Protected Sub GridKab_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "Kabupaten",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridKab)
        Else
            GridKab.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridKab)
            up_GridLoad("0", GridKab, "Department", "Provinsi")
        End If
    End Sub

    Protected Sub GridLanguage_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Language",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridLanguage)
        Else
            GridLanguage.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridLanguage)
            up_GridLoad("1", GridLanguage, "Language")
        End If
    End Sub

    Protected Sub GridPeriod_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Period",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPeriod)
        Else
            GridPeriod.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridPeriod)
            up_GridLoad("1", GridPeriod, "Period")
        End If
    End Sub

    Protected Sub GridJobPos_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "JobPosition",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridJobPos)
        Else
            GridJobPos.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridJobPos)
            up_GridLoad("1", GridJobPos, "JobPosition")
        End If
    End Sub
    Protected Sub GridProjectType_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "ProjectType",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridProjectType)
        Else
            GridProjectType.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridProjectType)
            up_GridLoad("1", GridProjectType, "ProjectType")
        End If
    End Sub

    Protected Sub GridBank_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Bank",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridBank)
        Else
            GridBank.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridBank)
            up_GridLoad("1", GridBank, "Bank")
        End If
    End Sub

    Protected Sub GridMaterialType_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "MaterialType",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialType)
        Else
            GridMaterialType.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridMaterialType)
            up_GridLoad("1", GridMaterialType, "MaterialType")
        End If
    End Sub

    Protected Sub GridMaterialCode_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "MaterialCode",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialCode)
        Else
            GridMaterialCode.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridMaterialCode)
            up_GridLoad("1", GridMaterialCode, "MaterialCode")
        End If
    End Sub

    Protected Sub GridMaterialGroup_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "MaterialGroupItem",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialGroup)
        Else
            GridMaterialGroup.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridMaterialGroup)
            up_GridLoad("1", GridMaterialGroup, "MaterialGroupItem")
        End If
    End Sub

    Protected Sub GridMaterialCategoryItem_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "MaterialGroupCategory",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialCategoryItem)
        Else
            GridMaterialCategoryItem.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridMaterialCategoryItem)
            up_GridLoad("0", GridMaterialCategoryItem, "MaterialCategoryItem", "MaterialGroupItem")
        End If
    End Sub

    Protected Sub GridMaterialPriceType_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "MaterialPriceType",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialPriceType)
        Else
            GridMaterialPriceType.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridMaterialPriceType)
            up_GridLoad("1", GridMaterialPriceType, "MaterialPriceType")
        End If
    End Sub

    Protected Sub GridOilType_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "OilType",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridOilType)
        Else
            GridOilType.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridOilType)
            up_GridLoad("1", GridOilType, "OilType")
        End If
    End Sub

    Protected Sub GridPRBudget_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "PRBudget",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPRBudget)
        Else
            GridPRBudget.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridPRBudget)
            up_GridLoad("1", GridPRBudget, "PRBudget")
        End If
    End Sub

    Protected Sub GridRateClass_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "RateClass",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPeriod)
        Else
            GridRateClass.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridRateClass)
            up_GridLoad("1", GridRateClass, "RateClass")
        End If
    End Sub

    Protected Sub GridSectoral_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Sectoral",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridSectoral)
        Else
            GridSectoral.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridSectoral)
            up_GridLoad("1", GridSectoral, "Sectoral")
        End If
    End Sub

    Protected Sub GridVAKind_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "VAKind",
                                         .RegisterBy = pUser}

        ClsParameterDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridVAKind)
        Else
            GridVAKind.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1, GridVAKind)
            up_GridLoad("0", GridVAKind, "VAKind", "RateClass")
        End If
    End Sub

    Protected Sub GridProcess_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridProcess.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Process",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridProcess)
        Else
            GridProcess.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridProcess)
            up_GridLoad("1", GridProcess, "Process")
        End If
    End Sub

    Protected Sub GridDepart_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridDepart.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Department",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridDepart)
        Else
            GridDepart.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridDepart)
            up_GridLoad("1", GridDepart, "Department")
        End If
    End Sub

    Protected Sub GridSection_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridSection.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "Section",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridSection)
        Else
            GridSection.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridSection)
            up_GridLoad("0", GridSection, "Section")
        End If
    End Sub

    Protected Sub GridCostCenter_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridCostCenter.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "CostCenter",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCostCenter)
        Else
            GridCostCenter.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridCostCenter)
            up_GridLoad("0", GridCostCenter, "CostCenter")
        End If
    End Sub

    Protected Sub GridGroupItem_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridGroupItem.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "GroupItem",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridGroupItem)
        Else
            GridGroupItem.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridGroupItem)
            up_GridLoad("1", GridGroupItem, "GroupItem")
        End If
    End Sub

    Protected Sub GridCategory_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridCategory.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "Category",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCategory)
        Else
            GridCategory.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridCategory)
            up_GridLoad("0", GridCategory, "Category")
        End If
    End Sub

    Protected Sub GridUOM_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridUOM.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "UOM",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridUOM)
        Else
            GridUOM.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridUOM)
            up_GridLoad("1", GridUOM, "UOM")
        End If
    End Sub

    Protected Sub GridCurr_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridCurr.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Currency",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCurr)
        Else
            GridCurr.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridCurr)
            up_GridLoad("1", GridCurr, "Currency")
        End If
    End Sub

    Protected Sub GridSupplier_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridSupplier.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "SupplierType",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridSupplier)
        Else
            GridSupplier.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridSupplier)
            up_GridLoad("1", GridSupplier, "Supplier")
        End If
    End Sub

    Protected Sub GridProvinsi_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridProvinsi.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Provinsi",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridProvinsi)
        Else
            GridProvinsi.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridProvinsi)
            up_GridLoad("1", GridProvinsi, "Provinsi")
        End If
    End Sub

    Protected Sub GridKab_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridKab.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "Kabupaten",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridKab)
        Else
            GridKab.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridKab)
            up_GridLoad("0", GridKab, "Kabupaten")
        End If
    End Sub

    Protected Sub GridLanguage_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridLanguage.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Language",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridLanguage)
        Else
            GridLanguage.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridLanguage)
            up_GridLoad("1", GridLanguage, "Language")
        End If
    End Sub

    Protected Sub GridPeriod_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridPeriod.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Period",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPeriod)
        Else
            GridPeriod.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridPeriod)
            up_GridLoad("1", GridPeriod, "Period")
        End If
    End Sub

    Protected Sub GridJobPos_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridJobPos.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "JobPosition",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridJobPos)
        Else
            GridJobPos.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridJobPos)
            up_GridLoad("1", GridJobPos, "JobPosition")
        End If
    End Sub

    Protected Sub GridProjectType_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridProjectType.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "ProjectType",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridProjectType)
        Else
            GridProjectType.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridProjectType)
            up_GridLoad("1", GridProjectType, "ProjectType")
        End If
    End Sub


    Protected Sub GridBank_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridBank.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Bank",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridBank)
        Else
            GridBank.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridBank)
            up_GridLoad("1", GridBank, "Bank")
        End If
    End Sub

    Protected Sub GridMaterialType_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridMaterialType.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "MaterialType",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialType)
        Else
            GridMaterialType.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridMaterialType)
            up_GridLoad("1", GridMaterialType, "MaterialType")
        End If
    End Sub

    Protected Sub GridMaterialCode_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridMaterialCode.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "MaterialCode",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialCode)
        Else
            GridMaterialCode.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridMaterialCode)
            up_GridLoad("1", GridMaterialCode, "MaterialCode")
        End If
    End Sub
    'Protected Sub GridCategory_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridCategory.CellEditorInitialize
    '    'If Not Grid.IsNewRowEditing Then
    '    If e.Column.FieldName = "General_Code" Then
    '        e.Editor.ReadOnly = True
    '        e.Editor.ForeColor = Color.Silver
    '        'e.Value("Parent_MaterialCode") = cboParentItem.Value
    '        e.Editor.Value = "--NEW--"

    '    End If
    '    'End If
    'End Sub
    'Protected Sub GridCategory_HtmlRowCreated(ByVal sender As Object, ByVal e As ASPxGridViewTableRowEventArgs) Handles GridCategory.HtmlRowCreated
    '    If e.RowType = GridViewRowType.EditForm AndAlso TryCast(sender, ASPxGridView).IsNewRowEditing Then
    '        Dim GeneralCode As ASPxTextBox = CType(GridCategory.FindEditFormTemplateControl("General_Code"), ASPxTextBox)
    '        GeneralCode.Enabled = False
    '        CType(GridCategory.FindEditFormTemplateControl("General_Code"), ASPxTextBox).Text = "--NEW--"
    '    End If

    'End Sub


    Protected Sub GridMaterialGroup_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridMaterialGroup.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "MaterialGroupItem",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialGroup)
        Else
            GridMaterialGroup.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridMaterialGroup)
            up_GridLoad("1", GridMaterialGroup, "MaterialGroupItem")
        End If
    End Sub

    Protected Sub GridMaterialCategoryItem_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridMaterialCategoryItem.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "MaterialGroupCategory",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialCategoryItem)
        Else
            GridMaterialCategoryItem.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridMaterialCategoryItem)
            up_GridLoad("0", GridMaterialCategoryItem, "MaterialCategoryItem", "MaterialGroupItem")
        End If
    End Sub

    Protected Sub GridMaterialPriceType_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridMaterialPriceType.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "MaterialPriceType",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialPriceType)
        Else
            GridMaterialPriceType.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridMaterialPriceType)
            up_GridLoad("1", GridMaterialPriceType, "MaterialPriceType")
        End If
    End Sub

    Protected Sub GridOilType_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridOilType.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "OilType",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridOilType)
        Else
            GridOilType.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridOilType)
            up_GridLoad("1", GridOilType, "OilType")
        End If
    End Sub

    Protected Sub GridPRBudget_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridPRBudget.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "PRBudget",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPRBudget)
        Else
            GridPRBudget.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridPRBudget)
            up_GridLoad("1", GridPRBudget, "PRBudget")
        End If
    End Sub

    Protected Sub GridRateClass_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridRateClass.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "RateClass",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPeriod)
        Else
            GridRateClass.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridRateClass)
            up_GridLoad("1", GridRateClass, "RateClass")
        End If
    End Sub

    Protected Sub GridSectoral_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridSectoral.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = "",
                                         .Cls_GroupID = "Sectoral",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridSectoral)
        Else
            GridSectoral.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridSectoral)
            up_GridLoad("1", GridSectoral, "Sectoral")
        End If
    End Sub

    Protected Sub GridVAKind_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles GridVAKind.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.NewValues("General_Code"),
                                         .General_Name = e.NewValues("General_Name"),
                                         .Parent_Code = e.NewValues("Parent_Code"),
                                         .Cls_GroupID = "VAKind",
                                         .RegisterBy = pUser}

        ClsParameterDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridVAKind)
        Else
            GridVAKind.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, GridVAKind)
            up_GridLoad("0", GridVAKind, "VAKind", "RateClass")
        End If
    End Sub

    Protected Sub GridProcess_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "Process", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridProcess)
            up_GridLoad("1", GridProcess, "Process")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridProcess)
        End If
    End Sub

    Protected Sub GridDepart_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "Department", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridDepart)
            up_GridLoad("1", GridDepart, "Department")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridDepart)
        End If
    End Sub

    Protected Sub GridSection_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "Section", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridSection)
            up_GridLoad("0", GridSection, "Section", "Department")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridSection)
        End If
    End Sub

    Protected Sub GridCostCenter_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "CostCenter", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridCostCenter)
            up_GridLoad("0", GridCostCenter, "CostCenter", "Department")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCostCenter)
        End If
    End Sub

    Protected Sub GridGroupItem_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "GroupItem", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridGroupItem)
            up_GridLoad("0", GridGroupItem, "GroupItem", "PRType")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridGroupItem)
        End If
    End Sub

    Protected Sub GridCategory_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = e.Values("Parent_Code")}
        ClsParameterDB.Delete(ses, "Category", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridCategory)
            up_GridLoad("0", GridCategory, "Category", "GroupItem")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCategory)
        End If
    End Sub

    Protected Sub GridUOM_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "UOM", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridUOM)
            up_GridLoad("1", GridUOM, "UOM")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridUOM)
        End If
    End Sub

    Protected Sub GridCurr_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "Currency", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridCurr)
            up_GridLoad("1", GridCurr, "Currency")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridCurr)
        End If
    End Sub

    Protected Sub GridSupplier_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "SupplierType", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridSupplier)
            up_GridLoad("1", GridSupplier, "SupplierType")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridSupplier)
        End If
    End Sub

    Protected Sub GridProvinsi_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "Provinsi", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridProvinsi)
            up_GridLoad("1", GridProvinsi, "Provinsi")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridProvinsi)
        End If
    End Sub

    Protected Sub GridKab_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = e.Values("Parent_Code")}
        ClsParameterDB.Delete(ses, "Kabupaten", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridKab)
            up_GridLoad("0", GridKab, "Kabupaten", "Provinsi")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridKab)
        End If
    End Sub

    Protected Sub GridLanguage_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "Language", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridLanguage)
            up_GridLoad("1", GridLanguage, "Language")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridLanguage)
        End If
    End Sub

    Protected Sub GridPeriod_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "Period", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridPeriod)
            up_GridLoad("1", GridPeriod, "Period")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPeriod)
        End If
    End Sub

    Protected Sub GridJobPos_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "JobPosition", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridJobPos)
            up_GridLoad("1", GridJobPos, "JobPosition")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridJobPos)
        End If
    End Sub

    Protected Sub GridProjectType_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = ""}
        ClsParameterDB.Delete(ses, "ProjectType", pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridProjectType)
            up_GridLoad("1", GridProjectType, "ProjectType")
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridProjectType)
        End If
    End Sub

    Protected Sub GridBank_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                         .Parent_Code = ""}

        ClsParameterDB.Delete(Ses, "Bank", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridBank)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridBank)
            up_GridLoad("1", GridBank, "Bank")
        End If
    End Sub

    Protected Sub GridMaterialType_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
            .Parent_Code = ""}

        ClsParameterDB.Delete(Ses, "MaterialType", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialType)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridMaterialType)
            up_GridLoad("1", GridMaterialType, "MaterialType")
        End If
    End Sub

    Protected Sub GridMaterialCode_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                          .Parent_Code = e.Values("Parent_Code")}

        ClsParameterDB.Delete(Ses, "MaterialCode", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialCode)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridMaterialCode)
            up_GridLoad("1", GridMaterialCode, "MaterialCode")
        End If
    End Sub

    Protected Sub GridMaterialGroup_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
            .Parent_Code = e.Values("Parent_Code")}

        ClsParameterDB.Delete(Ses, "MaterialGroupItem", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialGroup)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridMaterialGroup)
            up_GridLoad("1", GridMaterialGroup, "MaterialGroupItem")
        End If
    End Sub

    Protected Sub GridMaterialCategoryItem_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
            .Parent_Code = e.Values("Parent_Code")}

        ClsParameterDB.Delete(Ses, "MaterialGroupCategory", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialCategoryItem)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridMaterialCategoryItem)
            up_GridLoad("0", GridMaterialCategoryItem, "MaterialGroupCategory", "MaterialGroupItem")
        End If
    End Sub

    Protected Sub GridMaterialPriceType_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                         .Parent_Code = ""}

        ClsParameterDB.Delete(Ses, "MaterialPriceType", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridMaterialPriceType)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridMaterialPriceType)
            up_GridLoad("1", GridMaterialPriceType, "MaterialPriceType")
        End If
    End Sub

    Protected Sub GridOilType_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                         .Parent_Code = ""}

        ClsParameterDB.Delete(Ses, "OilType", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridOilType)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridOilType)
            up_GridLoad("1", GridOilType, "OilType")
        End If
    End Sub

    Protected Sub GridPRBudget_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                         .Parent_Code = ""}

        ClsParameterDB.Delete(Ses, "PRBudget", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPRBudget)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridPRBudget)
            up_GridLoad("1", GridPRBudget, "PRBudget")
        End If
    End Sub

    Protected Sub GridRateClass_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                         .Parent_Code = ""}

        ClsParameterDB.Delete(Ses, "RateClass", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridPeriod)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridRateClass)
            up_GridLoad("1", GridRateClass, "RateClass")
        End If
    End Sub

    Protected Sub GridSectoral_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                         .Parent_Code = ""}

        ClsParameterDB.Delete(Ses, "Sectoral", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridSectoral)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridSectoral)
            up_GridLoad("1", GridSectoral, "Sectoral")
        End If
    End Sub

    Protected Sub GridVAKind_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsParameter With {.General_Code = e.Values("General_Code"),
                                         .Parent_Code = e.Values("Parent_Code")}

        ClsParameterDB.Delete(Ses, "VAKind", pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, GridVAKind)
        Else
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1, GridVAKind)
            up_GridLoad("0", GridVAKind, "VAKind", "RateClass")
        End If
    End Sub
    Private Sub GridProcess_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridProcess.BeforeGetCallbackResult
        If GridProcess.IsNewRowEditing Then
            GridProcess.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridDepart_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridDepart.BeforeGetCallbackResult
        If GridDepart.IsNewRowEditing Then
            GridDepart.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridSection_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridSection.BeforeGetCallbackResult
        If GridSection.IsNewRowEditing Then
            GridSection.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridCostCenter_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridCostCenter.BeforeGetCallbackResult
        If GridCostCenter.IsNewRowEditing Then
            GridCostCenter.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridGroupItem_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridGroupItem.BeforeGetCallbackResult
        If GridGroupItem.IsNewRowEditing Then
            GridGroupItem.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridCategory_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridCategory.BeforeGetCallbackResult
        If GridCategory.IsNewRowEditing Then
            GridCategory.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridUOM_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridUOM.BeforeGetCallbackResult
        If GridUOM.IsNewRowEditing Then
            GridUOM.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridCurr_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridCurr.BeforeGetCallbackResult
        If GridCurr.IsNewRowEditing Then
            GridCurr.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridSupplier_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridSupplier.BeforeGetCallbackResult
        If GridSupplier.IsNewRowEditing Then
            GridSupplier.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridProvinsi_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridProvinsi.BeforeGetCallbackResult
        If GridProvinsi.IsNewRowEditing Then
            GridProvinsi.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridKab_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridKab.BeforeGetCallbackResult
        If GridKab.IsNewRowEditing Then
            GridKab.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridLanguage_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridLanguage.BeforeGetCallbackResult
        If GridLanguage.IsNewRowEditing Then
            GridLanguage.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridPeriod_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridPeriod.BeforeGetCallbackResult
        If GridPeriod.IsNewRowEditing Then
            GridPeriod.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridPRType_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridPRType.BeforeGetCallbackResult
        'If GridPRType.IsNewRowEditing Then
        '    GridPRType.SettingsCommandButton.UpdateButton.Text = "Save"
        'End If
    End Sub

    Private Sub GridJobPos_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridJobPos.BeforeGetCallbackResult
        If GridJobPos.IsNewRowEditing Then
            GridJobPos.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub
    Private Sub GridProjectType_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridProjectType.BeforeGetCallbackResult
        If GridProjectType.IsNewRowEditing Then
            GridProjectType.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridBank_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridBank.BeforeGetCallbackResult
        If GridBank.IsNewRowEditing Then
            GridBank.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridMaterialType_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridMaterialType.BeforeGetCallbackResult
        If GridMaterialType.IsNewRowEditing Then
            GridMaterialType.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridMaterialCode_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridMaterialCode.BeforeGetCallbackResult
        If GridMaterialCode.IsNewRowEditing Then
            GridMaterialCode.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridMaterialGroup_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridMaterialGroup.BeforeGetCallbackResult
        If GridMaterialGroup.IsNewRowEditing Then
            GridMaterialGroup.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridMaterialCategoryItem_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridMaterialCategoryItem.BeforeGetCallbackResult
        If GridMaterialCategoryItem.IsNewRowEditing Then
            GridMaterialCategoryItem.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridMaterialPriceType_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridMaterialPriceType.BeforeGetCallbackResult
        If GridMaterialPriceType.IsNewRowEditing Then
            GridMaterialPriceType.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridOilType_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridOilType.BeforeGetCallbackResult
        If GridOilType.IsNewRowEditing Then
            GridOilType.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridPRBudget_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridPRBudget.BeforeGetCallbackResult
        If GridPRBudget.IsNewRowEditing Then
            GridPRBudget.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridRateClass_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridRateClass.BeforeGetCallbackResult
        If GridRateClass.IsNewRowEditing Then
            GridRateClass.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridSectoral_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridSectoral.BeforeGetCallbackResult
        If GridSectoral.IsNewRowEditing Then
            GridSectoral.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridVAKind_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles GridVAKind.BeforeGetCallbackResult
        If GridVAKind.IsNewRowEditing Then
            GridVAKind.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub GridProcess_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridProcess.CellEditorInitialize
        If Not GridProcess.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridDepart_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridDepart.CellEditorInitialize
        If Not GridDepart.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridSection_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridSection.CellEditorInitialize
        If Not GridSection.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridCostCenter_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridCostCenter.CellEditorInitialize
        If Not GridCostCenter.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridGroupItem_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridGroupItem.CellEditorInitialize
        If Not GridGroupItem.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridCategory_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridCategory.CellEditorInitialize
        If Not GridCategory.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        Else
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
                e.Editor.Value = "--NEW--"

            End If
        End If
    End Sub

    Private Sub GridUOM_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridUOM.CellEditorInitialize
        If Not GridUOM.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridCurr_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridCurr.CellEditorInitialize
        If Not GridCurr.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridSupplier_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridSupplier.CellEditorInitialize
        If Not GridSupplier.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridProvinsi_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridProvinsi.CellEditorInitialize
        If Not GridProvinsi.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridKab_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridKab.CellEditorInitialize
        If Not GridKab.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridLanguage_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridLanguage.CellEditorInitialize
        If Not GridLanguage.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridPeriod_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridPeriod.CellEditorInitialize
        If Not GridPeriod.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridJobPos_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridJobPos.CellEditorInitialize
        If Not GridJobPos.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub

    Private Sub GridProjectType_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridProjectType.CellEditorInitialize
        If Not GridProjectType.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridBank_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridBank.CellEditorInitialize
        If Not GridBank.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridMaterialType_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridMaterialType.CellEditorInitialize
        If Not GridMaterialType.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridMaterialCode_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridMaterialCode.CellEditorInitialize
        If Not GridMaterialCode.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridMaterialGroup_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridMaterialGroup.CellEditorInitialize
        If Not GridMaterialGroup.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridMaterialCategoryItem_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridMaterialCategoryItem.CellEditorInitialize
        If Not GridMaterialCategoryItem.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridMaterialPriceType_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridMaterialPriceType.CellEditorInitialize
        If Not GridMaterialPriceType.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridOilType_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridOilType.CellEditorInitialize
        If Not GridOilType.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridPRBudget_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridPRBudget.CellEditorInitialize
        If Not GridPRBudget.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridRateClass_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridRateClass.CellEditorInitialize
        If Not GridRateClass.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridSectoral_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridSectoral.CellEditorInitialize
        If Not GridSectoral.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Private Sub GridVAKind_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles GridVAKind.CellEditorInitialize
        If Not GridVAKind.IsNewRowEditing Then
            If e.Column.FieldName = "General_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If

    End Sub

    Protected Sub GridProcess_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridProcess.IsNewRowEditing) Then
            GridProcess.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridProcess)
    End Sub

    Protected Sub GridDepart_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridDepart.IsNewRowEditing) Then
            GridDepart.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridDepart)
    End Sub

    Protected Sub GridSection_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridSection.IsNewRowEditing) Then
            GridSection.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridSection)
    End Sub

    Protected Sub GridCostCenter_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridCostCenter.IsNewRowEditing) Then
            GridCostCenter.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridCostCenter)
    End Sub

    Protected Sub GridGroupItem_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridGroupItem.IsNewRowEditing) Then
            GridGroupItem.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridGroupItem)
    End Sub

    Protected Sub GridCategory_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridCategory.IsNewRowEditing) Then
            GridCategory.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridCategory)
    End Sub

    Protected Sub GridUOM_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridUOM.IsNewRowEditing) Then
            GridUOM.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridUOM)
    End Sub

    Protected Sub GridCurr_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridCurr.IsNewRowEditing) Then
            GridCurr.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridCurr)
    End Sub

    Protected Sub GridSupplier_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridSupplier.IsNewRowEditing) Then
            GridSupplier.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridSupplier)
    End Sub

    Protected Sub GridProvinsi_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridProvinsi.IsNewRowEditing) Then
            GridProvinsi.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridProvinsi)
    End Sub

    Protected Sub GridKab_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridKab.IsNewRowEditing) Then
            GridKab.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridKab)
    End Sub

    Protected Sub GridLanguage_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridLanguage.IsNewRowEditing) Then
            GridLanguage.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridLanguage)
    End Sub

    Protected Sub GridPeriod_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridPeriod.IsNewRowEditing) Then
            GridPeriod.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridPeriod)
    End Sub

    Protected Sub GridJobPos_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridJobPos.IsNewRowEditing) Then
            GridJobPos.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridJobPos)
    End Sub

    Protected Sub GridProjectType_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridProjectType.IsNewRowEditing) Then
            GridProjectType.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridProjectType)
    End Sub

    Protected Sub GridBank_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridBank.IsNewRowEditing) Then
            GridBank.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridBank)
    End Sub

    Protected Sub GridMaterialType_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridMaterialType.IsNewRowEditing) Then
            GridMaterialType.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridMaterialType)
    End Sub

    Protected Sub GridMaterialCode_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridMaterialCode.IsNewRowEditing) Then
            GridMaterialCode.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridMaterialCode)
    End Sub

    Protected Sub GridMaterialGroup_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridMaterialGroup.IsNewRowEditing) Then
            GridMaterialGroup.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridMaterialGroup)
    End Sub

    Protected Sub GridMaterialCategoryItem_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridMaterialCategoryItem.IsNewRowEditing) Then
            GridMaterialCategoryItem.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridMaterialCategoryItem)
    End Sub

    Protected Sub GridMaterialPriceType_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridMaterialPriceType.IsNewRowEditing) Then
            GridMaterialPriceType.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridMaterialPriceType)
    End Sub

    Protected Sub GridOilType_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridOilType.IsNewRowEditing) Then
            GridOilType.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridOilType)
    End Sub

    Protected Sub GridPRBudget_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridPRBudget.IsNewRowEditing) Then
            GridPRBudget.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridPRBudget)
    End Sub

    Protected Sub GridRateClass_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridRateClass.IsNewRowEditing) Then
            GridRateClass.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridRateClass)
    End Sub

    Protected Sub GridSectoral_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridSectoral.IsNewRowEditing) Then
            GridSectoral.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridSectoral)
    End Sub

    Protected Sub GridVAKind_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not GridVAKind.IsNewRowEditing) Then
            GridVAKind.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0, GridVAKind)
    End Sub

    Private Sub GridProcess_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridProcess.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Process" Then
                    up_ExportExcel(GridProcess)
                End If

        End Select

    End Sub

    Private Sub GridDepart_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridDepart.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Department" Then
                    up_ExportExcel(GridDepart)
                End If

        End Select

    End Sub

    Private Sub GridSection_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridSection.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Section" Then
                    up_ExportExcel(GridSection)
                End If

        End Select
    End Sub

    Private Sub GridCostCenter_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridCostCenter.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "CostCenter" Then
                    up_ExportExcel(GridCostCenter)
                End If

        End Select
    End Sub

    Private Sub GridGroupItem_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridGroupItem.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "GroupItem" Then
                    up_ExportExcel(GridGroupItem)
                End If

        End Select
    End Sub

    Private Sub GridCategory_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridCategory.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Category" Then
                    up_ExportExcel(GridCategory)
                End If

        End Select

    End Sub
    Private Sub GridUOM_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridUOM.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "UOM" Then
                    up_ExportExcel(GridUOM)
                End If

        End Select

    End Sub
    Private Sub GridCurr_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridCurr.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Currency" Then
                    up_ExportExcel(GridCurr)
                End If

        End Select

    End Sub
    Private Sub GridSupplier_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridSupplier.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "SupplierType" Then
                    up_ExportExcel(GridSupplier)
                End If

        End Select

    End Sub
    Private Sub GridProvinsi_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridProvinsi.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Provinsi" Then
                    up_ExportExcel(GridProvinsi)
                End If

        End Select

    End Sub
    Private Sub GridKab_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridKab.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Kabupaten" Then
                    up_ExportExcel(GridKab)
                End If

        End Select

    End Sub

    Private Sub GridLanguage_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridLanguage.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Language" Then
                    up_ExportExcel(GridLanguage)
                End If

        End Select

    End Sub

    Private Sub GridPeriod_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridPeriod.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Period" Then
                    up_ExportExcel(GridPeriod)
                End If

        End Select

    End Sub

    Private Sub GridPRType_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridPRType.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "PRType" Then
                    up_ExportExcel(GridPRType)
                End If

        End Select

    End Sub

    Private Sub GridJobPos_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles GridJobPos.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "JobPosition" Then
                    up_ExportExcel(GridJobPos)
                End If

        End Select
    End Sub

    Private Sub GridProjectType_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridProjectType.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "ProjectType" Then
                    up_ExportExcel(GridProjectType)
                End If

        End Select
    End Sub

    Private Sub GridBank_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridBank.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Bank" Then
                    up_ExportExcel(GridBank)
                End If

        End Select
    End Sub

    Private Sub GridMaterialType_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridMaterialType.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "MaterialType" Then
                    up_ExportExcel(GridMaterialType)
                End If

        End Select
    End Sub

    Private Sub GridMaterialGroup_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridMaterialGroup.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "MaterialGroupItem" Then
                    up_ExportExcel(GridMaterialGroup)
                End If

        End Select
    End Sub

    Private Sub GridMaterialCategoryItem_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridMaterialCategoryItem.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "MaterialGroupCategory" Then
                    up_ExportExcel(GridMaterialCategoryItem)
                End If

        End Select
    End Sub

    Private Sub GridMaterialPriceType_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridMaterialPriceType.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "MaterialPriceType" Then
                    up_ExportExcel(GridMaterialPriceType)
                End If

        End Select
    End Sub

    Private Sub GridOilType_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridOilType.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "OilType" Then
                    up_ExportExcel(GridOilType)
                End If

        End Select
    End Sub

    Private Sub GridPRBudget_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridPRBudget.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "PRBudget" Then
                    up_ExportExcel(GridPRBudget)
                End If

        End Select
    End Sub

    Private Sub GridRateClass_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridRateClass.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "RateClass" Then
                    up_ExportExcel(GridRateClass)
                End If

        End Select
    End Sub

    Private Sub GridSectoral_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridSectoral.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "Sectoral" Then
                    up_ExportExcel(GridSectoral)
                End If

        End Select
    End Sub

    Private Sub GridMaterialCode_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridMaterialCode.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "MaterialCode" Then
                    up_ExportExcel(GridMaterialCode)
                End If

        End Select
    End Sub

    Private Sub GridVAKind_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridVAKind.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                If pageControl.ActiveTabPage.Text = "VAKind" Then
                    up_ExportExcel(GridVAKind)
                End If

        End Select
    End Sub

    Protected Sub GridProcess_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridProcess.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Process Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridProcess)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Process") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridProcess)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Process Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridProcess)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridDepart_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridDepart.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Department Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridDepart)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Department") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridDepart)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Department Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridDepart)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridSection_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridSection.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Section Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridSection)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Section") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridSection)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Section Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridSection)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridCostCenter_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridCostCenter.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Cost Center Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridCostCenter)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "CostCenter") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridCostCenter)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Cost Center Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridCostCenter)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridGroupItem_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridGroupItem.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Group Item Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridGroupItem)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "GroupItem") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridGroupItem)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Group Item Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridGroupItem)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridCategory_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridCategory.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Category Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridCategory)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Category") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridCategory)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Category Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridCategory)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridUOM_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridUOM.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input UOM Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridUOM)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "UOM") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridUOM)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input UOM Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridUOM)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridCurr_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridCurr.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Currency Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridCurr)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Currency") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridCurr)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Currency Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridCurr)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridSupplier_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridSupplier.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input SupplierType Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridSupplier)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "SupplierType") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridSupplier)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Supplier Type Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridSupplier)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridProvinsi_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridProvinsi.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Provinsi Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridProvinsi)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Provinsi") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridProvinsi)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Provinsi Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridProvinsi)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridKab_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridKab.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Kabupaten Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridKab)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Kabupaten") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridKab)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Kabupaten Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridKab)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridLanguage_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridLanguage.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Language Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridLanguage)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Language") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridLanguage)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Language Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridLanguage)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridPeriod_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridPeriod.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Period Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPeriod)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Period") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPeriod)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Period Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPeriod)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridJobPos_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridJobPos.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Period Code !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridJobPos)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "JobPosition") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridJobPos)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Period Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridJobPos)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridProjectType_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridProjectType.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Project Type Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridProjectType)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Language") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridProjectType)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Project Type Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridProjectType)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridBank_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridBank.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Bank Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridBank)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Bank") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridBank)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Bank Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridBank)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridMaterialType_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridMaterialType.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Type Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialType)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "MaterialType") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialType)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Type Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialType)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridMaterialCode_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridMaterialCode.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialCode)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "MaterialCode") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialCode)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialCode)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Parent_Code" Then
                If IsNothing(e.NewValues("Parent_Code")) OrElse e.NewValues("Parent_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Parent Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialCategoryItem)
                    Exit Sub
                End If
            End If


        Next column
    End Sub

    Protected Sub GridMaterialGroup_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridMaterialGroup.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Group Item Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialGroup)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "MaterialGroupItem") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialGroup)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Group Item Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialGroup)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Parent_Code" Then
                If IsNothing(e.NewValues("Parent_Code")) OrElse e.NewValues("Parent_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Parent Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialCategoryItem)
                    Exit Sub
                End If
            End If


        Next column
    End Sub

    Protected Sub GridMaterialCategoryItem_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridMaterialCategoryItem.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Category Item Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialCategoryItem)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "MaterialGroupCategory") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialCategoryItem)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Category Item Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialCategoryItem)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Parent_Code" Then
                If IsNothing(e.NewValues("Parent_Code")) OrElse e.NewValues("Parent_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Material Parent Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialCategoryItem)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridMaterialPriceType_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridMaterialPriceType.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Price Type Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialPriceType)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "MaterialPriceType") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialPriceType)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Price Type Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridMaterialPriceType)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridOilType_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridOilType.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Oil Type Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridOilType)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "OilType") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridOilType)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Oil Type Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridOilType)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridPRBudget_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridPRBudget.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input PR Budget Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPRBudget)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "PRBudget") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPRBudget)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input PR Budget Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridPRBudget)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridRateClass_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridRateClass.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Rate Class Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridRateClass)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "RateClass") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridRateClass)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Rate Class Descs!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridRateClass)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridSectoral_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridSectoral.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Sectoral Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridSectoral)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "Sectoral") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridSectoral)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Sectoral Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridSectoral)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub GridVAKind_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In GridVAKind.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "General_Code" Then
                If IsNothing(e.NewValues("General_Code")) OrElse e.NewValues("General_Code").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input VAKind Code!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridVAKind)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsParameterDB.isExist(e.NewValues("General_Code"), "VAKind") Then
                            e.Errors(dataColumn) = "Data is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridVAKind)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "General_Name" Then
                If IsNothing(e.NewValues("General_Name")) OrElse e.NewValues("General_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input VAKind Name!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1, GridVAKind)
                    Exit Sub
                End If
            End If

        Next column
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim ParameterName As String = ""

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)

        If pageControl.ActiveTabIndex = 0 Then
            up_GridLoad("1", GridDepart, "Department")
            ParameterName = "Department"
            link1.Component = GridExporterDept
        ElseIf pageControl.ActiveTabIndex = 1 Then
            up_GridLoad("0", GridSection, "Section", "Department")
            ParameterName = "Section"
            link1.Component = GridExporterSec
        ElseIf pageControl.ActiveTabIndex = 2 Then
            up_GridLoad("0", GridCostCenter, "CostCenter", "Department")
            ParameterName = "CostCenter"
            link1.Component = GridExporterCC
        ElseIf pageControl.ActiveTabIndex = 3 Then
            up_GridLoad("1", GridPRType, "PRType")
            ParameterName = "PRType"
            link1.Component = GridExporterPRType
        ElseIf pageControl.ActiveTabIndex = 4 Then
            up_GridLoad("0", GridGroupItem, "GroupItem", "PRType")
            ParameterName = "GroupItem"
            link1.Component = GridExporterGroupItem
        ElseIf pageControl.ActiveTabIndex = 5 Then
            up_GridLoad("0", GridCategory, "Category", "GroupItem")
            ParameterName = "Category"
            link1.Component = GridExporterCat
        ElseIf pageControl.ActiveTabIndex = 6 Then
            up_GridLoad("1", GridUOM, "UOM")
            ParameterName = "UOM"
            link1.Component = GridExporterUOM
        ElseIf pageControl.ActiveTabIndex = 7 Then
            up_GridLoad("1", GridCurr, "Currency")
            ParameterName = "Currency"
            link1.Component = GridExporterCur
        ElseIf pageControl.ActiveTabIndex = 8 Then
            up_GridLoad("1", GridSupplier, "SupplierType")
            ParameterName = "SupplierType"
            link1.Component = GridExporterSup
        ElseIf pageControl.ActiveTabIndex = 9 Then
            up_GridLoad("1", GridProvinsi, "Provinsi")
            ParameterName = "Province"
            link1.Component = GridExporterProv
        ElseIf pageControl.ActiveTabIndex = 10 Then
            up_GridLoad("0", GridKab, "Kabupaten", "Provinsi")
            ParameterName = "City"
            link1.Component = GridExporterKab
        ElseIf pageControl.ActiveTabIndex = 11 Then
            up_GridLoad("1", GridLanguage, "Language")
            ParameterName = "Language"
            link1.Component = GridExporterLang
        ElseIf pageControl.ActiveTabIndex = 12 Then
            up_GridLoad("1", GridPeriod, "Period")
            ParameterName = "Period"
            link1.Component = GridExporterPeriod
        ElseIf pageControl.ActiveTabIndex = 13 Then
            up_GridLoad("1", GridJobPos, "JobPosition")
            ParameterName = "JobPosition"
            link1.Component = GridExporterJobPos
        ElseIf pageControl.ActiveTabIndex = 14 Then 'no data group project type
            up_GridLoad("1", GridProjectType, "ProjectType")
            ParameterName = "ProjectType"
            link1.Component = GridExporterProjType
        ElseIf pageControl.ActiveTabIndex = 15 Then
            up_GridLoad("1", GridBank, "Bank")
            ParameterName = "Bank"
            link1.Component = GridExporterBank
        ElseIf pageControl.ActiveTabIndex = 16 Then
            up_GridLoad("1", GridMaterialPriceType, "MaterialPriceType")
            ParameterName = "MaterialPriceType"
            link1.Component = GridExporterMatPrice
        ElseIf pageControl.ActiveTabIndex = 17 Then
            up_GridLoad("1", GridMaterialType, "MaterialType")
            ParameterName = "MaterialType"
            link1.Component = GridExporterMatType
        ElseIf pageControl.ActiveTabIndex = 18 Then
            up_GridLoad("1", GridMaterialGroup, "MaterialGroupItem", "MaterialType")
            ParameterName = "MaterialGroupItem"
            link1.Component = GridExporterMatGroup
        ElseIf pageControl.ActiveTabIndex = 19 Then
            up_GridLoad("0", GridMaterialCategoryItem, "MaterialGroupCategory", "MaterialGroupItem")
            ParameterName = "MaterialGroupCategory"
            link1.Component = GridExporterMatCat
        ElseIf pageControl.ActiveTabIndex = 20 Then
            up_GridLoad("1", GridOilType, "OilType")
            ParameterName = "OilType"
            link1.Component = GridExporterOilType
        ElseIf pageControl.ActiveTabIndex = 21 Then
            up_GridLoad("1", GridPRBudget, "PRBudget")
            ParameterName = "PRBudget"
            link1.Component = GridExporterPRBudget
        ElseIf pageControl.ActiveTabIndex = 22 Then
            up_GridLoad("1", GridRateClass, "RateClass")
            ParameterName = "RateClass"
            link1.Component = GridExporterRateClass
        ElseIf pageControl.ActiveTabIndex = 23 Then
            up_GridLoad("1", GridSectoral, "Sectoral")
            ParameterName = "Sectoral"
            link1.Component = GridExporterSectoral
        ElseIf pageControl.ActiveTabIndex = 24 Then
            up_GridLoad("0", GridVAKind, "VAKind", "RateClass")
            ParameterName = "VAKind"
            link1.Component = GridExporterVaKind
        ElseIf pageControl.ActiveTabIndex = 25 Then
            up_GridLoad("1", GridProcess, "Process")
            ParameterName = "Process"
            link1.Component = GridExporterProcess

        End If


        ' ElseIf pageControl.ActiveTabIndex = 18 Then
        'up_GridLoad("1", GridMaterialCode, "MaterialCode")
        'ParameterName = "MaterialCode"
        'link1.Component = GridExporterMatCode

        Dim compositeLink As New CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=" & ParameterName & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

#End Region

End Class
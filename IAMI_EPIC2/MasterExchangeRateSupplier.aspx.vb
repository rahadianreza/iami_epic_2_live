﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
'Imports Microsoft.Office.Interop.Excel
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class MasterExchangeRateSupplier
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Method"
    Private Sub up_Excel(Optional ByRef outError As String = "")
        Dim cExchangeRate As New ClsExchangeRateSupplier
        Dim dt As DataTable
        Try
            Dim ps As New PrintingSystem()

            cExchangeRate.ExchangeRateDateFrom = dtDateFrom.Value
            cExchangeRate.ExchangeRateDateTo = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, dtDateTo.Value))
            cExchangeRate.SupplierCode = cboSupplierCode.Text
            cExchangeRate.CurrencyCode = cboCurrencyCode.Text

            dt = ClsExchangeRateSupplierDB.getDataTableExchangeRateSupplier(cExchangeRate, , , outError)

            Grid.DataSource = dt
            Grid.DataBind()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=ExchangeRateSupplier" & Format(Date.Now, "ddMMyyyyHHmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            outError = ex.Message
        End Try
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A200")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A200")

        Grid.JSProperties("cp_type") = ""
        Grid.JSProperties("cp_message") = ""

        If AuthUpdate = False Then
            dtDateFrom.Enabled = False
            dtDateTo.Enabled = False
            cboSupplierCode.Enabled = False
            cboCurrencyCode.Enabled = False
            btnAdd.Enabled = False
            btnRefresh.Enabled = False
        Else
            dtDateFrom.Enabled = True
            dtDateTo.Enabled = True
            cboSupplierCode.Enabled = True
            cboCurrencyCode.Enabled = True
            btnAdd.Enabled = True
            btnRefresh.Enabled = True
        End If

        If Not Page.IsPostBack Then
            cboSupplierCode.SelectedIndex = -1
            cboCurrencyCode.SelectedIndex = -1

            Dim Script As String
            Script = "btnDownload.SetEnabled(false);"
            ScriptManager.RegisterStartupScript(btnDownload, btnDownload.GetType(), "btnDownload", Script, True)

        End If
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("~/AddMasterExchangeRateSupplier.aspx?action=new")
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim ErrMsg As String = ""
        up_Excel(ErrMsg)
    End Sub

    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            'up_GridLoad(cboFundType.Value)
            Dim cExchangeRate As New ClsExchangeRateSupplier
            Dim pAction As String = ""
            Dim outError As String = ""

            Dim dt As System.Data.DataTable
            cExchangeRate.ExchangeRateDateFrom = dtDateFrom.Value
            cExchangeRate.ExchangeRateDateTo = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, dtDateTo.Value))
            cExchangeRate.SupplierCode = cboSupplierCode.Value
            cExchangeRate.CurrencyCode = cboCurrencyCode.Value

            dt = ClsExchangeRateSupplierDB.getDataTableExchangeRateSupplier(cExchangeRate, , , outError)
            If outError <> "" Then
                cbAction.JSProperties("cp_type") = 3
                cbAction.JSProperties("cp_message") = outError
                Exit Sub
            End If

            Grid.DataSource = dt
            Grid.DataBind()
        End If
    End Sub

    Protected Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim cExchangeRate As New ClsExchangeRateSupplier
        Dim pAction As String = ""
        Dim outError As String = ""

        Dim dt As System.Data.DataTable

        pAction = Split(e.Parameters, "|")(0)

        If pAction = "load" Then
            cExchangeRate.ExchangeRateDateFrom = Split(e.Parameters, "|")(1)
            cExchangeRate.ExchangeRateDateTo = Split(e.Parameters, "|")(2)
            cExchangeRate.SupplierCode = Split(e.Parameters, "|")(3)
            cExchangeRate.CurrencyCode = Split(e.Parameters, "|")(4)

            dt = ClsExchangeRateSupplierDB.getDataTableExchangeRateSupplier(cExchangeRate, , , outError)
            If outError <> "" Then
                cbAction.JSProperties("cp_type") = 3
                cbAction.JSProperties("cp_message") = outError
                Exit Sub
            End If

            Grid.DataSource = dt
            Grid.DataBind()

            If Grid.VisibleRowCount > 0 Then
                'If ds.Tables(0).Rows.Count > 0 Then
                Grid.JSProperties("cp_disabled") = "N"
                'Script = "btnDownload.SetEnabled(true);"
            Else
                Grid.JSProperties("cp_disabled") = "Y"
                'show_error(MsgTypeEnum.Info, "There is no data to show!", 1)
                Grid.JSProperties("cp_message") = "There is no data to show!"

                'Grid.JSProperties("cp_Message") = "There is no data to show!"
            End If

        End If
    End Sub

    Protected Sub cbAction_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbAction.Callback
        Dim cExchangeRate As New ClsExchangeRateSupplier
        Dim pAction As String = ""
        Dim sSupplierCode As String = ""
        Dim sCurrencyCode As String = ""
        Dim sDate As String = ""
        Dim outError As String = ""
        Dim dt As DataTable
        Try
            pAction = Split(e.Parameter, "|")(0)
            If pAction = "delete" Then
                sSupplierCode = Split(e.Parameter, "|")(1)
                sCurrencyCode = Split(e.Parameter, "|")(2)
                sDate = Split(e.Parameter, "|")(3)

                cExchangeRate.SupplierCode = sSupplierCode
                cExchangeRate.CurrencyCode = sCurrencyCode
                cExchangeRate.ExchangeRateDate = sDate

                If ClsExchangeRateSupplierDB.delete(cExchangeRate, , , outError) = True Then
                    cbAction.JSProperties("cp_type") = 1
                    cbAction.JSProperties("cp_message") = "Delete data successfully!"
                Else
                    cbAction.JSProperties("cp_type") = 3
                    cbAction.JSProperties("cp_message") = outError
                End If
            ElseIf pAction = "excel" Then
                cExchangeRate.ExchangeRateDateFrom = Split(e.Parameter, "|")(1)
                cExchangeRate.ExchangeRateDateTo = Split(e.Parameter, "|")(2)
                cExchangeRate.SupplierCode = Split(e.Parameter, "|")(3)
                cExchangeRate.CurrencyCode = Split(e.Parameter, "|")(4)

                dt = ClsExchangeRateSupplierDB.getDataTableExchangeRateSupplier(cExchangeRate, , , outError)
                If outError <> "" Then
                    cbAction.JSProperties("cp_type") = 3
                    cbAction.JSProperties("cp_message") = outError
                    Exit Sub
                End If

                Grid.DataSource = dt
                Grid.DataBind()

                Dim ps As New PrintingSystem()

                Dim link1 As New PrintableComponentLink(ps)
                link1.Component = GridExporter

                Dim compositeLink As New CompositeLink(ps)
                compositeLink.Links.AddRange(New Object() {link1})

                compositeLink.CreateDocument()
                Using stream As New MemoryStream()
                    compositeLink.PrintingSystem.ExportToXlsx(stream)
                    Response.Clear()
                    Response.Buffer = False
                    Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    Response.AppendHeader("Content-Disposition", "attachment; filename=ExchangeRateSupplier" & Format(Date.Now, "ddMMyyyyHHmmss") & ".xlsx")
                    Response.BinaryWrite(stream.ToArray())
                    Response.End()
                End Using

                ps.Dispose()
            End If
            e.Result = pAction
        Catch ex As Exception
            cbAction.JSProperties("cp_type") = 3
            cbAction.JSProperties("cp_message") = ex.Message
            e.Result = pAction
        End Try
    End Sub
#End Region

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="MaterialPartChart.aspx.vb" Inherits="IAMI_EPIC2.MaterialPartChart" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };
        function MessageBox(s, e) {
            //alert(s.cpmessage);
            if (s.cbMessage == "Download Excel Successfully") {
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cbMessage == "Request Date To must be higher than Request Date From") {
                toastr.success(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function ItemCodeValidation(s, e) {
            if (ItemCode.GetValue() == null) {
                e.isValid = false;
            }
        }

        function GridLoad() {
//            Grid.PerformCallback('refresh');
            tmpProjectType.SetText(cboProjectType.GetValue());
        }

        function SetCode(s, e) {
            tmpProjectType.SetText(cboProjectType.GetValue());
        }

    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        .style1
        {
            height: 30px;
        }
        .style2
        {
            height: 50px;
        }
        .style3
        {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%; border: 1px solid black; height: 100px;">
        <tr>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period From">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 10px 0 0 10px;">
               <%-- <dx:ASPxDateEdit ID="cboPeriodFrom" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="cboPeriodFrom" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <ClientSideEvents ValueChanged="GridLoad" />
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>--%>
                 <dx:ASPxTimeEdit ID="cboPeriodFrom" Theme="Office2010Black" Font-Names="Segoe UI"  Width="100px"
                    Font-Size="9pt" EditFormat="Custom" ClientInstanceName="cboPeriodFrom" runat="server"
                    DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy">
                      <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                </dx:ASPxTimeEdit>
            </td>
            <td style="padding: 10px 0 0 0;">
            </td>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period To">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 10px 0 0 10px;">
               <%-- <dx:ASPxDateEdit ID="cboPeriodTo" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="cboPeriodTo" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <ClientSideEvents ValueChanged="GridLoad" />
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>--%>
                  <dx:ASPxTimeEdit ID="cboPeriodTo" Theme="Office2010Black" Font-Names="Segoe UI" Width="100px"
                    Font-Size="9pt" EditFormat="Custom" ClientInstanceName="cboPeriodTo" runat="server"
                    DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy">
                      <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                </dx:ASPxTimeEdit>
            </td>
            <td style="width: 50%;">
            </td>
        </tr>
        <tr>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Type">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxComboBox ID="cboType" runat="server" ClientInstanceName="cboType" Width="150px"
                    Font-Names="Segoe UI" TextField="Description" ValueField="Code" DataSourceID="SqlDataSource2"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                      <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select 'ALL' Code, 'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialType'">
                </asp:SqlDataSource>
            </td>
            <td colspan="5" style="width: 70%;">
            </td>
        </tr>
        <tr>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material Type">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxComboBox ID="cboCategoryMaterial" runat="server" ClientInstanceName="cboCategoryMaterial"
                    Width="150px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    DataSourceID="SqlDataSource1" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                     <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                       <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                      cboMaterialCode.PerformCallback('filter|' + cboCategoryMaterial.GetSelectedItem().GetColumnText(0));
                      }"/>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select 'ALL' Code, 'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'MaterialCategory'">
                </asp:SqlDataSource>
            </td>
            <td colspan="5" style="width: 70%;">
            </td>
        </tr>
         <tr style="height: 25px">
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Material No">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="padding: 10px 0 0 10px;">
                 <dx:ASPxComboBox ID="cboMaterialCode" runat="server" ClientInstanceName="cboMaterialCode"
                    Width="150px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
            </td>
            <td colspan="5">
            </td>
        </tr>
           <tr>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Currency">
                </dx:ASPxLabel>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="padding: 10px 0 0 10px;">
                <dx:ASPxComboBox ID="cboCurrency" runat="server" ClientInstanceName="cboCurrency" Width="150px"
                    Font-Names="Segoe UI" TextField="Description" ValueField="Code" DataSourceID="SqlDataSource3"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                      <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Currency'">
                </asp:SqlDataSource>
            </td>
            <td colspan="5" style="width: 70%;">
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 0px 10px 10px" colspan="6" class="style1">
                <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnShowData" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
                            ChartGT.PerformCallback('load| ' + cboPeriodFrom.GetValue() + '|' + cboPeriodTo.GetValue() + '|' + cboType.GetValue() + '|' + cboCategoryMaterial.GetValue() + '|' + cboMaterialCode.GetValue() + '|' + cboCurrency.GetValue() );
                        }" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <br />
   

    <dxchartsui:WebChartControl ID="ChartGT" runat="server" CrosshairEnabled="True" Height="400px"
        Width="1050px" ClientInstanceName="ChartGT">
        <diagramserializable>
            <cc1:XYDiagram>
                <axisx visibleinpanesserializable="-1">
                </axisx>
                <axisy visibleinpanesserializable="-1" title-text=" " 
                       title-visible="True">
                </axisy>
            </cc1:XYDiagram>
        </diagramserializable>
        <legend alignmenthorizontal="Left" alignmentvertical="BottomOutside" 
            usecheckboxes="True" MaxVerticalPercentage="30"></legend>
        <seriestemplate>
            <viewserializable>
                <cc1:LineSeriesView AxisXName="Primary AxisX" AxisYName="Primary AxisY" 
                    PaneName="Default Pane">
                    <linemarkeroptions size="7">
                    </linemarkeroptions>
                </cc1:LineSeriesView>
            </viewserializable>
        </seriestemplate>
    </dxchartsui:WebChartControl>


</asp:Content>


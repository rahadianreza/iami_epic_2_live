﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ViewSupplierRecommendation.aspx.vb" Inherits="IAMI_EPIC2.ViewSupplierRecommendation" %>
<%@ Register Assembly="DevExpress.XtraReports.v14.1.Web, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<table style="width:100%;">  
<tr>
        <td valign="top" align="left">
                <dx:ASPxButton ID="btnBack" runat="server" Text="BACK"  ClientInstanceName="btnBack"
                    Font-Names="Tahoma"  Font-Size="8pt" Width="100px">
                </dx:ASPxButton>
        </td>
    </tr>
    <tr>
        <td>               
            <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" runat="server" 
                ReportTypeName="IAMI_EPIC2.rptSupplierRecommendation" DocumentViewerInternal=""
                ClientInstanceName="DocumentViewer">
                <toolbaritems>
                    <dx:ReportToolbarButton ItemKind="PrintReport" />
                    <dx:ReportToolbarButton ItemKind="FirstPage" />
                    <dx:ReportToolbarButton ItemKind="PreviousPage" />
                    <dx:ReportToolbarButton ItemKind="NextPage" />
                    <dx:ReportToolbarButton ItemKind="LastPage" />
                    <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                    <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                </toolbaritems>
            </dx:ASPxDocumentViewer>
        </td>
    </tr>
  
</table>
</asp:Content>

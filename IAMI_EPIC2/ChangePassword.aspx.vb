﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports DevExpress
Imports DevExpress.Web.ASPxCallback
Imports IAMIEngine

Public Class ChangePassword1
    Inherits System.Web.UI.Page

#Region "DECLARATION"
    Public lb_AuthUpdate As Boolean = False
    Dim clsDESEncryption As New clsDESEncryption("TOS")
#End Region

#Region "FORM EVENTS"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("Z030")
        Master.SiteTitle = sGlobal.menuName

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            txtCurrentPassword.Focus()
            'lblErrMsg.Visible = True
            Dim userID As String = Session("user") & ""
            Dim User As Cls_UserSetup = Cls_UserSetup_DB.GetData(userID)
            If User IsNot Nothing Then
                txtHint.Text = User.PasswordHint
                txtQuestion.Text = User.SecurityQuestion
                txtAnswer.Text = User.SecurityAnswer
            End If

            Call TabIndex()
            Call clear()
            txtCurrentPassword.Focus()
        End If

        lb_AuthUpdate = sGlobal.Auth_UserUpdate(Session("user"), "Z030")
        If lb_AuthUpdate = False Then
            btnClear.Enabled = False
            btnSubmit.Enabled = False
        End If
    End Sub

    Private Sub cbProgress_Callback(ByVal source As Object, ByVal e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbProgress.Callback
        Session("BA020Cancel") = ""
        Try
            Dim Err As String = ""
            Dim Userpass As New Cls_UserSetup
            Dim lb_IsUpdate As Boolean = validasiInput()
            If lb_IsUpdate = True Then
                Userpass.UserID = Session("user")
                Userpass.Password = clsDESEncryption.EncryptData(txtNewPassword.Text)
                Dim lb_Valid As Boolean = ValidasiJSOX(Userpass.UserID, txtNewPassword.Text)
                If lb_Valid = False Then

                    Return
                End If
                Userpass.PasswordHint = txtHint.Text
                Userpass.SecurityQuestion = txtQuestion.Text
                Userpass.SecurityAnswer = txtAnswer.Text
                ClsChangePassworddb.Update(Userpass, Err)

                If Err = "" Then
                    cbProgress.JSProperties("cpMessage") = "Update data successful."
                    clear()
                    txtCurrentPassword.Focus()
                Else
                    cbProgress.JSProperties("cpError") = Err
                End If

            End If
        Catch ex As Exception
            cbProgress.JSProperties("cpError") = ex.Message.ToString
        End Try
    End Sub
#End Region

#Region "PROCEDURE"
    Private Function validasiInput() As Boolean
        Dim sPassword As String
        Dim cUserSetup As New clsUserSetup
        cUserSetup.UserID = Session("user").ToString.Trim

        sPassword = clsUserSetupDB.getPassword(cUserSetup)
        If Trim(clsDESEncryption.EncryptData(txtCurrentPassword.Text)) <> Trim(sPassword) Then
            cbProgress.JSProperties("cpMessage") = "Current Password is not Correct!"
            'lblErrMsg.Visible = True
            Return False
        ElseIf Trim(txtNewPassword.Text) <> Trim(txtConfirmPassword.Text) Then
            cbProgress.JSProperties("cpMessage") = "New Password and Confirm Password does not match, please try again!"
            'lblErrMsg.Visible = True
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub clear()
        txtCurrentPassword.Text = ""
        txtNewPassword.Text = ""
        txtConfirmPassword.Text = ""
    End Sub

    Private Sub TabIndex()
        txtCurrentPassword.TabIndex = 1
        txtNewPassword.TabIndex = 2
        txtConfirmPassword.TabIndex = 3
        btnSubmit.TabIndex = 4
        btnClear.TabIndex = 5
    End Sub

    'Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
    '    Grid.JSProperties("cp_message") = ErrMsg
    'End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbProgress.JSProperties("cpMessage") = ErrMsg
    End Sub
    Private Function ValidasiJSOX(ByVal pUserID As String, pPassword As String) As Boolean
        Try
            Dim cJSOXSetup As New clsJSOXSetup
            cJSOXSetup.RuleID = "1"
            Dim jsox As clsJSOXSetup = clsJSOXSetupDB.GetRule(cJSOXSetup)
            Dim MinimumLength As Integer = jsox.ParamValue
            If jsox.Enable And Len(pPassword) < MinimumLength Then
                Dim sMsg As String = "Password length minimum " & MinimumLength & " characters"
                show_error(MsgTypeEnum.Warning, sMsg)
                txtNewPassword.Focus()
                Return False
            End If

            cJSOXSetup.RuleID = "7"
            jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
            If jsox.Enable = False And InStr(pPassword.ToUpper, pUserID.ToUpper) > 0 Then
                Dim sMsg As String = "Password cannot contain User ID"
                show_error(MsgTypeEnum.Warning, sMsg)
                txtNewPassword.Focus()
                Return False
            End If

            cJSOXSetup.RuleID = "8"
            jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
            If jsox.Enable And Not (pPassword.Contains("1") Or pPassword.Contains("2") Or pPassword.Contains("3") Or pPassword.Contains("4") Or pPassword.Contains("5") Or pPassword.Contains("6") Or pPassword.Contains("7") Or pPassword.Contains("8") Or pPassword.Contains("9") Or pPassword.Contains("0")) Then
                Dim sMsg As String = "Password must contain minimum 1 numeric"
                show_error(MsgTypeEnum.Warning, sMsg)
                txtNewPassword.Focus()
                Return False
            End If

            cJSOXSetup.RuleID = "9"
            jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
            If jsox.Enable = False Then
                Dim PrevChar As String = Mid(pPassword, 1, 1)
                For i As Integer = 2 To pPassword.Length
                    If Mid(pPassword, i, 1) = PrevChar Then
                        Dim sMsg As String = "Password cannot contain repeating characters"
                        show_error(MsgTypeEnum.Warning, sMsg)
                        txtNewPassword.Focus()
                        Return False
                    Else
                        PrevChar = Mid(pPassword, i, 1)
                    End If
                Next
            End If

            'cJSOXSetup.RuleID = 5
            'jsox = clsJSOXSetupDB.GetRule(cJSOXSetup)
            'If jsox.Enable And jsox.ParamValue > 0 Then
            '    Dim st As New clsCon
            '    st.CONN_METHOD = clsCon.CONNECTION_METHOD.ASPNET
            '    Dim PassList As List(Of clsPasswordHistory) = clsPasswordHistoryDB.GetList(pUserID, jsox.ParamValue, st.ConnectionString)
            '    For Each Pwd In PassList
            '        Dim OldPwd As String = clsDESEncryption.DecryptData(Pwd.Password)
            '        If pPassword = OldPwd Then
            '            Dim sMsg As String = "Password cannot be the same with your last " & jsox.ParamValue & " passwords"
            '            show_error(MsgTypeEnum.Warning, sMsg)
            '            Return False
            '        End If
            '    Next
            'End If

            Return True
        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message)
            Return False
        End Try
    End Function

    Private Sub ASPxCallback2_Callback(source As Object, e As CallbackEventArgs) Handles ASPxCallback2.Callback
        Session("BA020Cancel") = ""
        Try
            Dim Err As String = ""
            Dim Userpass As New Cls_UserSetup
            Dim lb_IsUpdate As Boolean = validasiInput()
            If lb_IsUpdate = True Then
                'Call up_SaveData(lb_IsUpdate,
                '         Trim(txtCurrentPassword.Text),
                '         Trim(txtNewPassword.Text),
                '         Trim(txtConfirmPassword.Text))
                Userpass.UserID = Session("user")
                Userpass.Password = clsDESEncryption.EncryptData(txtNewPassword.Text)

                ClsChangePassworddb.Update(Userpass, Err)
                If Err = "" Then
                    cbProgress.JSProperties("cpMessage") = "Update data successful."
                    'lblErrMsg.Visible = True
                    clear()
                    txtCurrentPassword.Focus()
                Else
                    cbProgress.JSProperties("cpError") = Err
                End If

            End If
        Catch ex As Exception
            cbProgress.JSProperties("cpError") = ex.Message.ToString
        End Try
    End Sub
#End Region
End Class
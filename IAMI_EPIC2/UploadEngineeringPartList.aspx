﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="UploadEngineeringPartList.aspx.vb" Inherits="IAMI_EPIC2.UploadEngineeringPartList" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        <script type="text/javascript">


        //------------Upload ----------------------------
        function OnBtnUploadClick(s, e) {
            ProgressBar.SetPosition(0);
        }

        function Uploader_OnUploadStart() {
            ProgressBar.SetPosition(0);
        }

        function Uploader_OnFilesUploadComplete(args) {
            UpdateUploadButton();
            ProgressBar.SetPosition(100);
        }
        function OnUploadProgressChanged(s, e) {
            ProgressBar.SetPosition(e.progress);
        }
        function UpdateUploadButton() {
            BtnSubmit.SetEnabled(Uploader.GetText(0) != "");
            var a = Uploader.GetText();
            ProgressBar.SetPosition(0);
        }
        function OnUploadProgressChanged(s, e){
            ProgressBar.SetPosition(e.progress);
        }

         function Validation() {
            if (cboProject.GetText() == '') {
                toastr.warning('Please Select Project Name');                
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else
            {
                popUp.Show();
            }
        }
    </script>
    <%--<style>
        .loader {
          border: 16px solid #f3f3f3;
          border-radius: 50%;
          border-top: 16px solid #3498db;
          width: 120px;
          height: 120px;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }
    </style>--%>
<%--    <style>
    
.cssload-loader {
	width: 244px;
	height: 49px;
	line-height: 49px;
	text-align: center;
	position: absolute;
	left: 50%;
	transform: translate(-50%, -50%);
		-o-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		-webkit-transform: translate(-50%, -50%);
		-moz-transform: translate(-50%, -50%);
	font-family: helvetica, arial, sans-serif;
	text-transform: uppercase;
	font-weight: 900;
	font-size:18px;
	color: rgb(206,66,51);
	letter-spacing: 0.2em;
}
.cssload-loader::before, .cssload-loader::after {
	content: "";
	display: block;
	width: 15px;
	height: 15px;
	background: rgb(206,66,51);
	position: absolute;
	animation: cssload-load 0.81s infinite alternate ease-in-out;
		-o-animation: cssload-load 0.81s infinite alternate ease-in-out;
		-ms-animation: cssload-load 0.81s infinite alternate ease-in-out;
		-webkit-animation: cssload-load 0.81s infinite alternate ease-in-out;
		-moz-animation: cssload-load 0.81s infinite alternate ease-in-out;
}
.cssload-loader::before {
	top: 0;
}
.cssload-loader::after {
	bottom: 0;
}



@keyframes cssload-load {
	0% {
		left: 0;
		height: 29px;
		width: 15px;
	}
	50% {
		height: 8px;
		width: 39px;
	}
	100% {
		left: 229px;
		height: 29px;
		width: 15px;
	}
}

@-o-keyframes cssload-load {
	0% {
		left: 0;
		height: 29px;
		width: 15px;
	}
	50% {
		height: 8px;
		width: 39px;
	}
	100% {
		left: 229px;
		height: 29px;
		width: 15px;
	}
}

@-ms-keyframes cssload-load {
	0% {
		left: 0;
		height: 29px;
		width: 15px;
	}
	50% {
		height: 8px;
		width: 39px;
	}
	100% {
		left: 229px;
		height: 29px;
		width: 15px;
	}
}

@-webkit-keyframes cssload-load {
	0% {
		left: 0;
		height: 29px;
		width: 15px;
	}
	50% {
		height: 8px;
		width: 39px;
	}
	100% {
		left: 229px;
		height: 29px;
		width: 15px;
	}
}

@-moz-keyframes cssload-load {
	0% {
		left: 0;
		height: 29px;
		width: 15px;
	}
	50% {
		height: 8px;
		width: 39px;
	}
	100% {
		left: 229px;
		height: 29px;
		width: 15px;
	}
}
    </style>--%>
    <style type="text/css">
.test {
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none;
}
.spinner {
  margin: 100px auto;
  width: 200px;
  height: 80px;
  text-align: center;
  font-size: 20px;
}

.spinner > div {
  background-color: #fff;
  height: 100%;
  width: 6px;
  display: inline-block;
  
  -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
  animation: sk-stretchdelay 1.2s infinite ease-in-out;
}

.spinner .rect2 {
  -webkit-animation-delay: -1.1s;
  animation-delay: -1.1s;
}

.spinner .rect3 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

.spinner .rect4 {
  -webkit-animation-delay: -0.9s;
  animation-delay: -0.9s;
}

.spinner .rect5 {
  -webkit-animation-delay: -0.8s;
  animation-delay: -0.8s;
}

@-webkit-keyframes sk-stretchdelay {
  0%, 40%, 100% { -webkit-transform: scaleY(0.4) }  
  20% { -webkit-transform: scaleY(1.0) }
}

@keyframes sk-stretchdelay {
  0%, 40%, 100% { 
    transform: scaleY(0.4);
    -webkit-transform: scaleY(0.4);
  }  20% { 
    transform: scaleY(1.0);
    -webkit-transform: scaleY(1.0);
  }
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
     <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
           <tr style="height: 20px">
                <td>
                </td>
                <td style="width:10px">&nbsp;</td>
                <td></td>
                <td style="width:10px">&nbsp;</td>
                <td style="width:10px">&nbsp;</td>
                <td align="left">
                </td>
                <td style="width:10px">&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td style="width:600px">&nbsp;</td>
            </tr>     
           <tr style="height: 10px">
                <td style=" padding:0px 0px 0px 10px" class="style1">            
                    <dx:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Project Name">
                    </dx:aspxlabel>
                </td>
                <td style=" width:20px">&nbsp;</td>
                <td colspan="3">
           
        <dx:aspxcombobox ID="cboProject" runat="server" ClientInstanceName="cboProject"
                            Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Project_Name"
                            ValueField="Project_ID" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                                         
                            <Columns>            
                            <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                            <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:aspxcombobox>
           
                </td>
                <td align="left">
                    
                </td>
                <td style=" width:10px">
           
          
           
                </td>
                <td>
                 
                              
                    &nbsp;</td>
                <td>
                 
                              
                     <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="Select P1.Project_ID, Project_Name From Proj_Header P1 left join Item_List_Sourcing P2 on P1.Project_ID = P2.Project_ID Where Project_Type='PT01' and  P2.Project_ID is null">
                    </asp:SqlDataSource></td>
                <td>
                     <dx:ASPxCallback ID="cbsubmit" runat="server" ClientInstanceName="cbsubmit" >
                    <ClientSideEvents EndCallback="function(s, e) {
	                                                         
                                alert(s.cpMessage);           
                            }" /> 
                    </dx:ASPxCallback></td>
                <td>
                       
                    </td>
            </tr>          
            

            <tr style="height: 30px">
                <td style=" width:100px; padding:0px 0px 0px 10px">
                 
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td style=" width:100px">
               
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td style=" width:10px">
                 
                </td>
                <td align="left">
                    
                </td>
                <td style=" width:10px">&nbsp;</td>
                <td>
         
                   
                   
                </td>
                <td>
         
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>

           <table style="width:100%; border:thin ridge #9598A1" >
        <tr>
            <td Width="100px" align="left">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="*FILE" Font-Names="Tahoma" Font-Size="8pt" Width="100"></dx:ASPxLabel></td>
            <td Width="800px" align="left">
             <dx:ASPxUploadControl ID="Uploader" runat="server" Width="100%" Font-Names="Verdana"
                 Font-Size="8pt" ClientInstanceName="Uploader" ShowClearFileSelectionButton="False"
                            NullText="Click here to browse files...">
                            <ClientSideEvents UploadingProgressChanged="OnUploadProgressChanged"  FilesUploadComplete="function(s, e) { Uploader_OnFilesUploadComplete(e); }"
                                FileUploadComplete="function(s, e) { Uploader_OnFileUploadComplete(e); }" FileUploadStart="function(s, e) { Uploader_OnUploadStart(); }"
                                TextChanged="function(s, e) { UpdateUploadButton(); }" />
                            <ValidationSettings AllowedFileExtensions=".xls,.xlsx" />
                            <BrowseButton Text="...">
                            </BrowseButton>
                            <BrowseButtonStyle Paddings-Padding="3px">
                            </BrowseButtonStyle>
                        </dx:ASPxUploadControl>
                        
                         </td>
                       
            <td align="right" Width="100px">
                <dx:ASPxButton ID="BtnSubmit" runat="server" Text="Upload" Font-Names="Tahoma"
                    Width="85px" Font-Size="8pt" TabIndex="2" AutoPostBack="False" 
                            UseSubmitBehavior="False" ClientInstanceName="BtnSubmit" OnClick="BtnSubmit_Click" >
                <ClientSideEvents Click="Validation" />
                </dx:ASPxButton>
                        </td>
                        
            <td align="right" Width="100px">
                <dx:ASPxButton ID="BtnClearLog" runat="server" Text="Clear Log" Font-Names="Tahoma"
                    Width="85px" Font-Size="8pt" TabIndex="3" AutoPostBack="False" UseSubmitBehavior="False" ClientInstanceName="BtnClearLog" >
                </dx:ASPxButton>

                        </td>          
            <td align="right" Width="100px">
                <dx:ASPxButton ID="BtnSaveError" runat="server" Text="Save Log" Font-Names="Tahoma"
                    Width="85px" Font-Size="8pt" TabIndex="4" AutoPostBack="False" UseSubmitBehavior="False" ClientInstanceName="BtnSaveError" >
                     
                </dx:ASPxButton>

                        </td>
              <td align="right" Width="100px">
             
                        </td>         
            <td align="left">
                        &nbsp;</td>
        </tr>
    </table>
    <table style="width:100%; border:thin ridge #9598A1" >
        <tr>
            <td class="style1" bgcolor="#99CCFF" align="left">
                <dx:ASPxProgressBar ID="ProgressBar" runat="server" 
                                Width="100%" ClientInstanceName="ProgressBar" Font-Names="verdana" 
                                Theme="Office2010Silver">
                </dx:ASPxProgressBar>
                
            </td>
        </tr>
    </table>
    <table style="width:100%; border:thin ridge #9598A1" >
        <tr>
            <td class="style1" bgcolor="#99CCFF" align="left">
            
                <dx:ASPxMemo ID="MemoMessage" runat="server" Height="320px" Width="100%">
                </dx:ASPxMemo>
            
            </td>
        </tr>
    </table>
      <table style="width:100%; border:thin ridge #9598A1" >
        <tr>
         <td valign="top" align="left">
                &nbsp;</td>
            <td class="style38" align="right">
                &nbsp;</td>
            <td Width="85px" align="right">
                &nbsp;</td>
            <td Width="85px" align="right">
                &nbsp;</td>
            <td Width="85px" align="right">
                &nbsp;</td>
            <td align="right" Width="85px">
                &nbsp;</td>
        </tr>
        
    </table>
    </div>
   <%-- HeaderText="Loading..." --%>
    <dx:ASPxPopupControl ID="popUp" runat="server"   Modal="True" CssClass="test"
      ClientInstanceName="popUp"  PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
     ModalBackgroundStyle-BackColor="Black"  AllowDragging="false" ShowHeader="false"
        PopupAnimationType="Fade" EnableViewState="False" Width="200px" BackColor="Transparent" >
       
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                <dx:ASPxPanel ID="Aspxpanel2" runat="server" DefaultButton="btOK" Width="100%">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent" runat="server" Width="100%">
                           <div class="spinner">
                              <div class="rect1"></div>
                              <div class="rect2"></div>
                              <div class="rect3"></div>
                              <div class="rect4"></div>
                              <div class="rect5"></div>
                            </div>
                            <br /><br />
                            <div style="color:White; text-align:center; font-size:17px; font-weight:700;">
                                LOADING
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <Border BorderColor="Transparent"></Border>
    </dx:ASPxPopupControl>
<%-- <dx:ASPxPopupControl ID="popUp" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popUp" 
        AllowDragging="True" ShowHeader="false"
        PopupAnimationType="None" EnableViewState="False" Width="200px" 
        HeaderStyle-BackColor="#255FDC" HeaderStyle-ForeColor="White" headerstyle-Font-Names="Verdana" 
        Border-BorderColor="#255FDC">
 
<HeaderStyle BackColor="#255FDC" Font-Names="Verdana" ForeColor="White"></HeaderStyle>
        <ContentCollection>
        <dx:popupcontrolcontentcontrol ID="PopupControlContentControl" runat="server">
        <dx:aspxpanel ID="Aspxpanel2" runat="server" DefaultButton="btOK" Width="100%">
        <PanelCollection>
            <dx:panelcontent ID="PanelContent" runat="server" Width="100%">
                <table style="width:100%">

       <div class="cssload-loader">
           <caption>
               Loading</caption>
                    </div>
        </table>
                  
                
                  
    </dx:panelcontent>
</PanelCollection>
</dx:aspxpanel>                                        
</dx:popupcontrolcontentcontrol>
</ContentCollection>
<Border BorderColor="#255FDC"></Border>
</dx:ASPxPopupControl>--%>
<dx:ASPxPopupControl ID="popUp2" runat="server" ClientInstanceName="popUp2" >
                    <ContentCollection>
                        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                </dx:ASPxPopupControl>
</asp:Content>

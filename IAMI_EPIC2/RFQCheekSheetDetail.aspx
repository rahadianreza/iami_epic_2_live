﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RFQCheekSheetDetail.aspx.vb"
    Inherits="IAMI_EPIC2.RFQCheekSheetDetail" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        };
        function MessageBox(s, e) {
//            alert(s.cp_message);
            if (s.cp_message == "Data Saved Successfully!") {
                btnSubmit.SetEnabled(false);
                toastr.success(s.cbMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }

            else {
                toastr.warning(s.cbMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }


    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        .style10
        {
            width: 100px;
        }
        .style11
        {
            width: 158px;
        }
        .style13
        {
            width: 72px;
        }
        .style15
        {
            width: 100px;
            height: 20px;
        }
        .style16
        {
            height: 20px;
        }
        .style17
        {
            width: 175px;
            height: 20px;
        }
        .style18
        {
            width: 200px;
            height: 20px;
        }
        .style19
        {
            width: 72px;
            height: 20px;
        }
        .style21
        {
            width: 184px;
            height: 20px;
        }
        .style23
        {
            width: 234px;
        }
        .style24
        {
            width: 184px;
        }
        .style25
        {
        }
        .style26
        {
            height: 16px;
        }
        .style27
        {
            height: 16px;
            width: 85px;
        }
        .style28
        {
            width: 85px;
        }
        .style29
        {
            height: 16px;
            width: 88px;
        }
        .style30
        {
            width: 88px;
        }
        .style31
        {
            width: 355px;
        }
        .style32
        {
            width: 355px;
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
   <div style="padding: 5px 5px 5px 5px">
   <table style="width: 100%; border: 1px solid black;">
   <tr>
<td class="style27"></td>
<td class="style29" ></td>
<td class="style26"></td>
<td class="style26"></td>
<td class="style26"></td>
<td class="style26"></td>
<td class="style26"></td>
<td class="style26"></td>
<td class="style26"></td>
<td class="style26"></td>
</tr>
<tr style="height: 10px">
<td style="padding: 0px 0px 0px 10px;" class="style28">
              
          <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnBack" Theme="Default">
                        <%--     <ClientSideEvents Click="function(s, e) {
                        alert('1')
                  }" ></ClientSideEvents>--%>
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
<td style="padding: 0px 0px 0px 10px;" class="style30">
                 <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnDownload" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                </td>
<td style="padding: 0px 0px 0px 10px;">
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" 
        Height="25px" AutoPostBack="False"
                    ClientInstanceName="btnSubmit" Theme="Default">
             <ClientSideEvents Click="function(s, e) {
                        cbMessage.PerformCallback();
                        }"
                         />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                </td>
<td>&nbsp;</td>
<td class="style24">&nbsp;</td>
<td class="style25">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="style13">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 10px">
<td style="padding: 0px 0px 0px 10px;" class="style28">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px;" class="style30">
                &nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="style24">&nbsp;</td>
<td class="style25">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="style13">&nbsp;</td>
<td>&nbsp;</td>
</tr>
   </table>
   </div>
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">
   <tr>
<td class="style31">&nbsp;</td>
<td class="style23">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="style24">&nbsp;</td>
<td class="style25">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="style13">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 2px">
<td align="center" style="padding: 0px 0px 0px 10px; vertical-align:middle; " colspan="10">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="12pt" Font-Bold="true"
                    Text="REQUEST FOR QUOTATION">
                </dx:ASPxLabel>
</td>
</tr>
<tr style="height: 2px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style23">
                &nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="style24">&nbsp;</td>
<td class="style25">&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="style13">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="No">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; ">


                <dx:ASPxTextBox ID="NoRfq" ClientInstanceName="NoRfq" runat="server" Width="230px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>
    </td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Attn : Dept Head ">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " colspan="2">


                <dx:ASPxTextBox ID="txtDeptHead" ClientInstanceName="txtDeptHead" runat="server" Width="200px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>
    </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        &nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr style="height: 5px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style23" >


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="inbox-data-from" >


                </td>
<td></td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style25" colspan="3">


                <dx:ASPxTextBox ID="txtDeptHeadEmail" ClientInstanceName="txtDeptHeadEmail" runat="server" Width="250px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style10">
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        </td>
<td></td>
</tr>

<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Date">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " >


                  <dx:ASPxTextBox ID="dateRfq" ClientInstanceName="dateRfq" runat="server" Width="100px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox></td>

<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project PIC">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " colspan="2">


                <dx:ASPxTextBox ID="txtProjPIC" ClientInstanceName="txtProjPIC"  runat="server" Width="200px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>
    </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        &nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr style="height: 5px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style23" >


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="inbox-data-from" >


                </td>
<td></td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style25" colspan="3">


                <dx:ASPxTextBox ID="txtProjPICEmail" ClientInstanceName="txtProjPICEmail"  runat="server" Width="250px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style10">
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        </td>
<td></td>
</tr>

<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="To">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " >


                  <dx:ASPxTextBox ID="txtTo" ClientInstanceName="txtTo" runat="server" Width="100px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox></td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Cost PIC">
                </dx:ASPxLabel>
            </td>
<td  style="padding: 0px 0px 0px 10px; " colspan="2">


                <dx:ASPxTextBox ID="txtCostPIC" ClientInstanceName="txtCostPIC"  runat="server" Width="200px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>
    </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        &nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr style="height: 5px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style23" >


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="inbox-data-from" >


                </td>
<td></td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style25" colspan="3">


                <dx:ASPxTextBox ID="txtCostPICEmail" ClientInstanceName="txtCostPICEmail" runat="server" Width="250px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>
    </td>
<td style="padding: 0px 0px 0px 10px; " class="style10">
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        </td>
<td></td>
</tr>

<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Phone">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; ">


                  <dx:ASPxTextBox ID="txtPhone" ClientInstanceName="txtPhone" runat="server" Width="100px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox></td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; "class="style24">
                <dx:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Estimated SOP">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " class="style25">


                <dx:ASPxLabel ID="ASPxLabel16" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="OTS Sample">
                </dx:ASPxLabel>
    </td>
<td style="padding: 0px 0px 0px 10px; width: 200px">


                  <dx:ASPxTextBox ID="txtOTS" ClientInstanceName="txtOTS" runat="server"  Width="100px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>
    </td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        &nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr style="height: 5px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style23" >


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="inbox-data-from" >


                </td>
<td></td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style25">


                <dx:ASPxLabel ID="ASPxLabel17" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="SOP Timing">
                </dx:ASPxLabel>
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style11">


                  <dx:ASPxTextBox ID="txtSOP" ClientInstanceName="txtSOP" runat="server" Width="100px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>


    </td>
<td style="padding: 0px 0px 0px 10px; width: 200px">


                  &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style10">
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        </td>
<td></td>
</tr>

<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Fax">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; "  >


                  <dx:ASPxTextBox ID="txtFax" ClientInstanceName="txtFax" runat="server" Width="100px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox></td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                <dx:ASPxLabel ID="ASPxLabel14" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Quotation base on">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " class="style25">


                <dx:ASPxLabel ID="ASPxLabel18" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text=" Exchange Rate">
                </dx:ASPxLabel>
    </td>
<td style="padding: 0px 0px 0px 10px; " colspan="2">


                <dx:ASPxTextBox ID="txtExchangeRate" ClientInstanceName="txtExchangeRate" runat="server"  Width="350px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>


    </td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        &nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr style="height: 5px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style23" >


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="inbox-data-from" >


                </td>
<td></td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style25">


                <dx:ASPxLabel ID="ASPxLabel19" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Include standard packaging">
                </dx:ASPxLabel>
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style11">


                  <dx:ASPxTextBox ID="txtInclude" ClientInstanceName="txtInclude" runat="server" Width="100px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>


    </td>
<td style="padding: 0px 0px 0px 10px; width: 200px">


                  &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style10">
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        </td>
<td></td>
</tr>

<tr>
<td style="padding: 0px 0px 0px 10px; " class="style32">
                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Vendor Name">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; "  class="style16" >


                  <dx:ASPxTextBox ID="txtVendorName" ClientInstanceName="txtVendorName" runat="server" Width="200px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox></td>
<td class="style16"></td>
<td style="padding: 0px 0px 0px 10px; " class="style15">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style21">
                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Due date of this inquiry is">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " class="style17">


                  <dx:ASPxTextBox ID="TxtDueDate" ClientInstanceName="TxtDueDate" runat="server" Width="100px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox>


                </td>
<td style="padding: 0px 0px 0px 10px; " class="style18">


                </td>
<td style="padding: 0px 0px 0px 10px; " class="style15">
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style19">
                        </td>
<td class="style16"></td>
</tr>

<tr style="height: 5px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style23" >


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="inbox-data-from" >


                </td>
<td></td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style25">
                        </td>
<td class="style11">


                &nbsp;</td>
<td>


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style10">
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        </td>
<td></td>
</tr>

<tr style="height: 20px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                <dx:ASPxLabel ID="ASPxLabel12" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Name">
                </dx:ASPxLabel>
            </td>
<td style="padding: 0px 0px 0px 10px; " >


                  <dx:ASPxTextBox ID="txtProjName" ClientInstanceName="txtProjName" runat="server" Width="150px" Enabled="false" Font-Bold="true">
                </dx:ASPxTextBox></td>
<td>&nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " align="right" class="style24">
                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style25">


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 200px">


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; width: 100px">
                        &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        &nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr style="height: 5px">
<td style="padding: 0px 0px 0px 10px; " class="style31">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style23" >


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="inbox-data-from" >


                </td>
<td></td>
<td style="padding: 0px 0px 0px 10px; " class="style24">
                </td>
<td style="padding: 0px 0px 0px 10px; " class="style25">
                        </td>
<td class="style11">


                &nbsp;</td>
<td>


                &nbsp;</td>
<td style="padding: 0px 0px 0px 10px; " class="style10">
                        </td>
<td style="padding: 0px 0px 0px 10px; " class="style13">
                        </td>
<td></td>
</tr>

</table>

    </div>

    <div style="padding: 5px 0px 5px 0px">
   
        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <ClientSideEvents CallbackComplete="MessageBox" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
            <ClientSideEvents CallbackComplete="SetCode" />
        </dx:ASPxCallback>
     <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
        </dx:ASPxGridViewExporter>

    </div>
        <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" KeyFieldName="Part_No" 
            Font-Names="Segoe UI" Font-Size="9pt">
            <ClientSideEvents EndCallback="OnEndCallback" />
            <Columns>
                <dx:GridViewDataTextColumn Caption="No" FieldName="No" VisibleIndex="0"
                    Width="50px">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
          
                <dx:GridViewDataTextColumn Caption="Upc"  FieldName="Upc" VisibleIndex="1"
                    Width="120px">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Fna" FieldName="Fna" VisibleIndex="2"
                    Width="100px">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="6px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="6px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn Caption="Dwg No" FieldName="Dwg_No" VisibleIndex="3" Width="150px">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn Caption="Part No" FieldName="Part_No" VisibleIndex="4" Width="150px">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
              
                 <dx:GridViewDataTextColumn Caption="Part Name" FieldName="Part_Name" VisibleIndex="5" Width="150px">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Qty" FieldName="Qty" VisibleIndex="6" Width="150px">
                    <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="Qty" DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>

                <%--  Area of variant--%>
              
                <%--End Of Variant--%>

                 <dx:GridViewDataTextColumn Caption="Volume / Years" FieldName="VolumeYears" VisibleIndex="8" Width="150px">
                   <PropertiesTextEdit DisplayFormatInEditMode="True" Width="150px" ClientInstanceName="VolumeYears" DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Caption="Remarks" FieldName="Remarks" VisibleIndex="9" Width="150px">
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                        <Paddings PaddingLeft="6px" />
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
            
            </Columns>
            <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
            <SettingsEditing EditFormColumnCount="2" Mode="PopupEditForm" />
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="260"
                HorizontalScrollBarMode="Auto" />
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="320" />
            </SettingsPopup>
            <SettingsDataSecurity AllowDelete="False" AllowInsert="False" />
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header HorizontalAlign="Center">
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
            </Styles>
        </dx:ASPxGridView>

    </div>
 
        </asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="PartItemList.aspx.vb" Inherits="IAMI_EPIC2.PartItemList" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function MessageError(s, e) {
            //alert(s.cpmessage);
            if (s.cpmessage == "Download Excel Successfully") {
                toastr.success(s.cpmessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpmessage == null || s.cpmessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpmessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function ItemCodeValidation(s, e) {
            if (Part_No.GetValue() == null) {
                e.isValid = false;
            }
        }

        function GridHeader(s, e) {
            Grid.PerformCallback('gridheader|0|0');
        }
        a

    </script>
    <style type="text/css">
        .style1
        {
            width: 82px;
        }
        .style5
        {
            height: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding: 5px 5px 5px 5px">
            <%--         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%"
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="ItemCode" DataSourceID="SqlDataSource1">--%>
            <table style="width: 100%; border: 1px solid black" >
                <tr style="height: 20px">
                    <td style="width:100px">
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td width="200px">
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td align="left" width="100px">
                    </td>
                    <td style="width: 10px">
                        &nbsp;
                    </td>
                    <td style="width: 200px">
                        &nbsp;
                    </td>
                    <td >
                        &nbsp;
                    </td>
                    <td style="width: 600px">
                        &nbsp;
                    </td>
                </tr>
                <tr style="height: 8px">
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        
                        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                   
                    </td>
                    <td>
                    </td>
                    <td align="left" class="style5">
                    </td>
                    <td style="width: 20px">
                        &nbsp;
                    </td>
                    <td>
                        <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                            <ClientSideEvents Init="MessageError" />
                        </dx:ASPxCallback>
                    </td>
                    <td>
                        <dx:ASPxCallback ID="cbExcel" runat="server" ClientInstanceName="cbExcel">
                            <ClientSideEvents Init="MessageError" />
                        </dx:ASPxCallback>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr style="height: 25px">
                    <td style="padding: 0px 0px 0px 10px" class="style1" colspan="9">
                        <dx:ASPxButton ID="btnRefresh" runat="server" Text="Show Data" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnRefresh" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
                            
                                                  
	                        Grid.PerformCallback('gridload|0|0');
                        }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;<dx:ASPxButton ID="BtnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="BtnDownload" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
	                            cbExcel.PerformCallback();
                            }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;
                        <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" UseSubmitBehavior="false" Font-Names="Segoe UI"
                            Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnAdd" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                </tr>
                <tr style="height: 0px">
                    <td style="padding: 0px 0px 0px 10px" class="style1" colspan="9">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Size="9pt" Font-Names="Segoe UI"
                KeyFieldName="Part_No">
                <%--              <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm"/>
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter" Width="320" />
            </SettingsPopup>--%>
                <%--             <dx:GridViewCommandColumn FixedStyle="Left"
                        VisibleIndex="0" ShowEditButton="true" ShowDeleteButton="true" 
                        ShowNewButtonInHeader="true" ShowClearFilterButton="true" Width="100px">
                        <HeaderStyle Paddings-PaddingLeft="3px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" >
                            <Paddings PaddingLeft="3px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewCommandColumn>--%>
                <ClientSideEvents CustomButtonClick="function(s, e) {            
                            if(e.buttonID == 'edit'){
                     
                                var rowKey = Grid.GetRowKey(e.visibleIndex);
                             
	                            window.location.href= 'AddPartItemMaster.aspx?ID=' + rowKey;
                            }
}"></ClientSideEvents>
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                    HorizontalScrollBarMode="Auto" />
                <ClientSideEvents CustomButtonClick="function(s, e) {            
                            if(e.buttonID == 'edit'){
                     
                                var rowKey = Grid.GetRowKey(e.visibleIndex);
                             
	                            window.location.href= 'AddPartItemMaster.aspx?ID=' + rowKey;
                            }
            }" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Part Number" FieldName="Part_No" VisibleIndex="5"
                        Settings-AutoFilterCondition="Contains">
                        <PropertiesTextEdit MaxLength="50" Width="90px" ClientInstanceName="Part_No">
                            <ClientSideEvents Validation="ItemCodeValidation" />
                            <Style HorizontalAlign="Left"></Style>
                        </PropertiesTextEdit>
                        <Settings AutoFilterCondition="Contains"></Settings>
                        <EditFormSettings VisibleIndex="1" />
                        <EditFormSettings VisibleIndex="1"></EditFormSettings>
                        <FilterCellStyle Paddings-PaddingRight="4px">
                            <Paddings PaddingRight="4px"></Paddings>
                        </FilterCellStyle>
                        <HeaderStyle Paddings-PaddingLeft="8px" HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="8px"></Paddings>
                        </HeaderStyle>
                        <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Part_Name" Caption="Part Name" VisibleIndex="6"
                        Width="250px">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="EPL_Part_No" Caption="EPL Part No" VisibleIndex="7"
                        Width="500px">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="pVariant" Caption="Variant" VisibleIndex="9"
                        Width="150px">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                   
                   <dx:GridViewDataTextColumn FieldName="UOM" Caption="UOM" VisibleIndex="10">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LastIAPrice" Caption="Last IA Price" VisibleIndex="11">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="LastSupplier" Caption="Last Supplier" VisibleIndex="12">
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn FieldName="CreateUser" Caption="Register By" VisibleIndex="17">
                        <Settings AutoFilterCondition="Contains" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="UpdateUser" Caption="Update By" VisibleIndex="23">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="UpdateDate" Caption="Update Date" VisibleIndex="24"
                        Width="150px">
                        <PropertiesTextEdit DisplayFormatString="dd MMM yyyy hh:nn:ss">
                        </PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewCommandColumn VisibleIndex="4" ButtonType="Link" Caption=" " FixedStyle="Left">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="Register Date" FieldName="CreateDate" VisibleIndex="22"
                        Width="150px">
                        <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                        </PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior ColumnResizeMode="Control" />
                <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                </SettingsPager>
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                    HorizontalScrollBarMode="Auto" />
                <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>
                    <EditFormColumnCaption>
                        <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
                    </EditFormColumnCaption>
                </Styles>
            </dx:ASPxGridView>
        </div>
    </div>
    <%--    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
    ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
            SelectCommand="SELECT ItemCode, ItemName FROM ItemMaster"></asp:SqlDataSource>--%>
</asp:Content>

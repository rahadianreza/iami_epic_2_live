﻿Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewPRAcceptance
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        up_LoadReport()
    End Sub

    Private Sub up_LoadReport()
        Dim sql As String
        Dim pPRNumber As String
        Dim pRevision As Integer

        Dim ds As New DataSet
        Dim Report As New rptPRAcceptance

        pPRNumber = Session("PRNumber")
        pRevision = Session("Revision")

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()
                sql = "exec sp_PR_PRAcceptanceReport '" & pPRNumber & "' , " & pRevision & ""

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "PRAcceptance_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report

                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim pPRNumber As String
        Dim pRev As Integer

        pPRNumber = Session("PRNumber")
        pRev = Session("Revision")
        Response.Redirect("~/PRAcceptanceDetail.aspx?ID=" & pPRNumber & "|" & pRev)

    End Sub

End Class
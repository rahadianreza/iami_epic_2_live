﻿<%@ Page Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeBehind="CrudgeOilChart.aspx.vb" Inherits="IAMI_EPIC2.CrudgeOilChart" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%@ Register assembly="DevExpress.XtraCharts.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
     <style type="text/css">
        .td-col-l 
        {
            padding:0px 0px 0px 10px;
            width:70px;
        }
        .td-col-m
        {
            width:20px;
        }
        .td-col-r
        {
            width:200px;
        }
        .td-col-f
        {
            width:50px;
        }
        .tr-height
        {
            height: 35px;
        }
            
        
    </style>
    <script type="text/javascript">
        function LoadChart() {
            ASPxCallbackPanel1.PerformCallback('show');
        }

        //popup calender month year
        function formatDateISO(date) {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }

        function Period_From_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            TmPeriod_From.SetDate(myDate);
        }

        function Period_From_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();

                var startDate = formatDateISO(TmPeriod_From.GetDate());
                var endDate = formatDateISO(TmPeriod_To.GetDate());

                if (startDate > endDate) {
                    Message(2, 'Period from must be lower than period to!');
                    TmPeriod_From.Focus();
                    return false;
                }
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }
        }

        //period to
        function Period_To_OnInit(s, e) {
            var calendar = s.GetCalendar();
            calendar.owner = s;
            //calendar.SetVisible(false);
            calendar.GetMainElement().style.opacity = '0';

            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            TmPeriod_To.SetDate(myDate);
        }

        function Period_To_OnDropDown(s, e) {
            var calendar = s.GetCalendar();
            var fastNav = calendar.fastNavigation;
            fastNav.activeView = calendar.GetView(0, 0);
            fastNav.Prepare();
            fastNav.GetPopup().popupVerticalAlign = "Below";
            fastNav.GetPopup().ShowAtElement(s.GetMainElement())

            fastNav.OnOkClick = function () {
                var parentDateEdit = this.calendar.owner;
                var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
                parentDateEdit.SetDate(currentDate);
                parentDateEdit.HideDropDown();

                var startDate = formatDateISO(TmPeriod_From.GetDate());
                var endDate = formatDateISO(TmPeriod_To.GetDate());

                if (startDate > endDate) {
                    Message(2, 'Period from must be lower than period to!');
                    TmPeriod_From.Focus();
                    return false;
                }
            }

            fastNav.OnCancelClick = function () {
                var parentDateEdit = this.calendar.owner;
                parentDateEdit.HideDropDown();
            }
        }
        //end popup calender
      


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
 
   <%-- <div style="padding : 5px 5px 5px 5px">
       <dxchartsui:WebChartControl ID="WebChartControl1" runat="server">
        </dxchartsui:WebChartControl>
        
    </div>--%>
    <div style="padding: 5px 5px 5px 5px"> 
        <div style="padding: 5px 5px 5px 5px"> 
            <table runat="server" style="border : 1px solid; width:100%">
                <tr style="height:10px">
                    
                    <td style="padding:0px 0px 0px 10px; width:80px" ></td>
                    <td class="td-col-m"></td>
                    <td class="td-col-r"></td>

                    <td class="td-col-f"></td>

                    <td class="td-col-l" ></td>
                    <td class="td-col-m"></td>
                    <td class="td-col-r"></td>

                    <td colspan="2"></td>
                </tr>
               <tr style="height: 20px">
                    <td style=" padding:0px 0px 0px 10px; width:10%">
                        <dx:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="Period">
                        </dx:aspxlabel>                 
                    </td>
                    <td style= "width:1%">&nbsp;</td>
                    <td style="width:10%">
                        <dx:ASPxDateEdit ID="TmPeriod_From" runat="server" ClientInstanceName="TmPeriod_From"
                            EnableTheming="True" ShowShadow="false" Font-Names="Segoe UI" Theme="Office2010Black"
                            Font-Size="9pt" Height="22px" DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy"
                            Width="120px">
                            <ClientSideEvents DropDown="Period_From_OnDropDown" Init="Period_From_OnInit" />
                        </dx:ASPxDateEdit>

                    </td>
                    <td style="width:1%; padding:0px 0px 0px 15px">    
                        <dx:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="~&nbsp; ">
                        </dx:aspxlabel>   
                                                     
                    </td>
                    <td style="width:180px">
                        <dx:ASPxDateEdit ID="TmPeriod_To" runat="server" ClientInstanceName="TmPeriod_To" EnableTheming="True"
                            ShowShadow="false" Font-Names="Segoe UI" Theme="Office2010Black" Font-Size="9pt"
                            Height="22px" DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy" Width="120px">
                            <ClientSideEvents DropDown="Period_To_OnDropDown" Init="Period_To_OnInit" />
                        </dx:ASPxDateEdit>

                    </td>
                </tr>    
               
                <tr class="tr-height">
                    <td style="padding:0px 0px 0px 10px" colspan="6">
                        <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnBack" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;&nbsp;
                         <dx:ASPxButton ID="btnShow" runat="server" Text="Show" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnShow" Theme="Default">
                         <ClientSideEvents Click="function(s, e) {
                                            
	                        ASPxCallbackPanel1.PerformCallback('show|');
   
   
}" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                    <td colspan="3"></td>
                </tr>
                <tr style="height:10px">
                    <td colspan="9"></td>
                </tr>
            </table>

        
          </div>
    </div>
    <div style="padding :5px 5px 5px 5px">
        <table runat="server" style="width:100%; border : 1px solid">
            <tr>
                <td style="padding:5px 5px 5px 5px">
                    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" ClientInstanceName="ASPxCallbackPanel1"
                        runat="server">
                    
                    </dx:ASPxCallbackPanel>
                </td>
            </tr>
        </table>
    </div>
    <div>
       
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Province, Rtrim(Par_Description) ProvinceDesc  From Mst_Parameter Where Par_Group = 'Provinsi'">
        </asp:SqlDataSource>
        <dx:ASPxCallback ID="ASPxCallback1" runat="server">
        </dx:ASPxCallback>
    </div>
    
</asp:Content>


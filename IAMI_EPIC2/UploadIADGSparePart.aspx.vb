﻿Imports DevExpress.Web.ASPxUploadControl
Imports System.IO
Imports OfficeOpenXml
Imports System.Transactions
Imports System.Drawing

Public Class UploadIADGSparePart
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""

    Dim FilePath As String = ""
    Dim FileName As String = ""
    Dim FileExt As String = ""
    Dim Ext As String = ""
    Dim aMsg As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("I070")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        'If Not Page.IsPostBack And Not Page.IsCallback Then
        '    If Request.QueryString("ID").Length > 0 Then
        '        Dim Project = Request.QueryString("ID").Split("|")(0)
        '        cboProject.Value = Project
        '    End If
        'End If
        

        Ext = Server.MapPath("")

    End Sub


    Protected Sub Uploader_FileUploadComplete(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)
        Try
            'e.CallbackData = SavePostedFiles(e.UploadedFile)
        Catch ex As Exception
            e.IsValid = False
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 0)
        End Try
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        cbMessage.JSProperties("cp_Message") = ErrMsg
    End Sub

    Private Function uf_ImpotExcel(ByVal pFileName As String, Optional ByRef perr As String = "") As String
        'Initialize of Excel
        Dim _IADGSparePart As New ClsIADGSparePart
        Dim pbar As Integer
        Dim pNoError As Integer
        Dim pno As Integer
        Dim pCountVariant As Integer
        Dim pError As String = ""
        Dim VariantName As String = ""
        Dim closedummyflag As Integer = 0

        Try
            popUp.ShowOnPageLoad = True
            Dim fi As New FileInfo(pFileName)
            Dim dt As New DataTable
            Dim exl As New ExcelPackage(fi)
            Dim ws As ExcelWorksheet
            Dim tbl As New DataTable
            MemoMessage.Text = ""
            MemoMessage.Text = "No  " & "Error Message ||" & vbCrLf

            Dim wss As OfficeOpenXml.ExcelWorksheets
            Try
                wss = exl.Workbook.Worksheets
            Catch ex As Exception
                wss = exl.Workbook.Worksheets
            End Try

            Using Scope As New TransactionScope(TransactionScopeOption.Required, TimeSpan.FromMinutes(10))
                ProgressBar.Value = 0
                ws = exl.Workbook.Worksheets(1)

                'Dim pcount As Integer = 21
                Dim pdatadifferent As Boolean = False
                Dim xMsg As String = ""

                If pError <> "" Then
                    pNoError = CInt(pNoError + 1)
                    MemoMessage.Text = MemoMessage.Text & pNoError & " " & pError & " ||" & vbCrLf
                End If

                'checking valid template
                If ws.Cells("B1").Value <> "Project Year" AndAlso ws.Cells("C1").Value <> "IADG Part No" AndAlso ws.Cells("D1").Value <> "Part Name" AndAlso _
                   ws.Cells("E1").Value <> "Model" AndAlso ws.Cells("F1").Value <> "New / Multi" AndAlso ws.Cells("G1").Value <> "Sample IADG Compare" AndAlso _
                   ws.Cells("H1").Value <> "Sample OEM" AndAlso ws.Cells("I1").Value <> "Volume / Month" AndAlso ws.Cells("J1").Value <> "Cost Target" AndAlso _
                   ws.Cells("K1").Value <> "Priority" Then
                    MemoMessage.Text = "Incorrect Excel Template"
                    popUp.ShowOnPageLoad = False
                    Exit Function
                End If

                Dim row = 3
                If CStr(ws.Cells(row, 2).Value) = "" AndAlso ws.Cells(row, 3).Value = "" AndAlso ws.Cells(row, 4).Value = "" AndAlso ws.Cells(row, 5).Value = "" AndAlso _
                        ws.Cells(row, 6).Value = "" AndAlso ws.Cells(row, 7).Value = "" AndAlso ws.Cells(row, 8).Value = "" AndAlso ws.Cells(row, 9).Value = "" AndAlso _
                        ws.Cells(row, 10).Value = "" AndAlso ws.Cells(row, 11).Value = "" Then
                    MemoMessage.Text = "No data uploaded"
                    popUp.ShowOnPageLoad = False
                    Exit Function
                End If




                'proses insert per row
                For row = 3 To ws.Dimension.End.Row
                    If CStr(ws.Cells(row, 2).Value) = "" AndAlso ws.Cells(row, 3).Value = "" AndAlso ws.Cells(row, 4).Value = "" AndAlso ws.Cells(row, 5).Value = "" AndAlso _
                       ws.Cells(row, 6).Value = "" AndAlso ws.Cells(row, 7).Value = "" AndAlso ws.Cells(row, 8).Value = "" AndAlso ws.Cells(row, 9).Value = "" AndAlso _
                       ws.Cells(row, 10).Value = "" AndAlso ws.Cells(row, 11).Value = "" Then
                        Exit For
                    End If

                    _IADGSparePart.ProjectID = cboProject.Value
                    _IADGSparePart.ProjectYear = ws.Cells(row, 2).Value ' ws.Celss(row,column)
                    _IADGSparePart.PartNo = ws.Cells(row, 3).Value
                    _IADGSparePart.PartName = ws.Cells(row, 4).Value
                    _IADGSparePart.Model = ws.Cells(row, 5).Value
                    _IADGSparePart.NewMulti = ws.Cells(row, 6).Value
                    _IADGSparePart.SampleIADGCompare = ws.Cells(row, 7).Value
                    _IADGSparePart.SampleUOM = ws.Cells(row, 8).Value
                    _IADGSparePart.VolumePerMonth = ws.Cells(row, 9).Value
                    _IADGSparePart.CostTarget = ws.Cells(row, 10).Value
                    _IADGSparePart.Priority = ws.Cells(row, 11).Value

                    'checking all row valid or not null

                    For rowD = 3 To ws.Dimension.End.Row
                        If CStr(ws.Cells(rowD, 2).Value) = "" AndAlso ws.Cells(rowD, 3).Value = "" AndAlso ws.Cells(rowD, 4).Value = "" AndAlso ws.Cells(rowD, 5).Value = "" AndAlso _
                            ws.Cells(rowD, 6).Value = "" AndAlso ws.Cells(rowD, 7).Value = "" AndAlso ws.Cells(rowD, 8).Value = "" AndAlso ws.Cells(rowD, 9).Value = "" AndAlso _
                            ws.Cells(rowD, 10).Value = "" AndAlso ws.Cells(rowD, 11).Value = "" Then
                            Exit For
                        End If

                        'validation numeric only / null value
                        For valueNull As Integer = 2 To ws.Dimension.End.Column
                            If ws.Cells(1, valueNull).Value <> "" Then
                                'If String.IsNullOrEmpty(ws.Cells(rowD, valueNull).Value) then 'And (Convert.ToInt32(ws.Cells(rowD, valueNull).Value) < 48 Or Convert.ToInt32((ws.Cells(rowD, valueNull).Value) > 57)) Then
                                aMsg += validationOfExcelData(ws.Cells(rowD, valueNull).Value, ws.Cells(1, valueNull).Value, rowD)
                                'End If
                                'For i = 0 To ws.Cells(rowD, valueNull).Value.Length - 1
                                '    If Convert.ToInt32(ws.Cells(rowD, valueNull).Value.Chars(i)) < 48 Or Convert.ToInt32(ws.Cells(rowD, valueNull).Value.Chars(i)) > 57 Then
                                '        aMsg += "Row : " + CStr(rowD) + " - " + Message + " only numeric ||" & vbCrLf
                                '    End If
                                'Next
                            Else
                                If ws.Cells(1, valueNull).Value = "Project Year" Or ws.Cells(1, valueNull).Value = "Volume / Month" _
                                    Or ws.Cells(1, valueNull).Value = "Cost Target" Or ws.Cells(1, valueNull).Value = "Priority" Then

                                    aMsg += validationOfExcelData(ws.Cells(rowD, valueNull).Value, ws.Cells(1, valueNull).Value, rowD)
                                End If
                            End If
                        Next
                        If closedummyflag = 0 Then
                            aMsg += validationOfSimilarData(cboProject.Value, ws.Cells(rowD, 2).Value, ws.Cells(rowD, 3).Value, ws.Cells(rowD, 5).Value, pUser, rowD)
                        End If
                    Next
                    closedummyflag = 1

                    'Dim ds As DataSet = ClsIADGSparePartDB.getExistsProjectID(pProjectID, pProjectYear, pPartNo, "1", pUser)
                    If aMsg <> "" Then
                        MemoMessage.Text = aMsg
                        popUp.ShowOnPageLoad = False
                        ClsIADGSparePartDB.getExistsProjectID("", "", "", "", "2", pUser)
                        Exit Function
                    End If

                    ClsIADGSparePartDB.insertData(_IADGSparePart, pUser, pError)

                    If pError <> "" Then
                        pNoError = CInt(pNoError + 1)
                        MemoMessage.Text = MemoMessage.Text & pNoError & " " & pError & " ||" & vbCrLf
                    End If

                    If pNoError = 0 Then
                        ProgressBar.Value = (row * 100) / ws.Dimension.End.Row
                    End If
                    ProgressBar.ShowPosition = True

                Next

                If xMsg <> "" Then
                    MemoMessage.Text = xMsg
                    popUp.ShowOnPageLoad = False
                    ClsIADGSparePartDB.getExistsProjectID("", "", "", "", "2", pUser)
                    Exit Function
                End If

                If pNoError > 0 Then
                    BtnSubmit.ClientEnabled = True
                    popUp.ShowOnPageLoad = False
                    ClsIADGSparePartDB.getExistsProjectID("", "", "", "", "2", pUser)
                    Exit Function
                End If

                ClsIADGSparePartDB.getExistsProjectID("", "", "", "", "2", pUser)
                Scope.Complete()
                ' If MemoMessage.Text <> "No data uploaded" Then
                MemoMessage.Text = "Upload IADG Spare Part Successfully"
                'End If

                popUp.ShowOnPageLoad = False
            End Using
        Catch ex As Exception
            popUp.ShowOnPageLoad = False
            MemoMessage.Text = ex.Message
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 1)
        End Try
    End Function


    Private Sub importExcel()
        Dim pErr As String = ""
        Try
            If Uploader.HasFile Then
                FileName = Path.GetFileName(Uploader.PostedFile.FileName)
                FileExt = Path.GetExtension(Uploader.PostedFile.FileName)
                'Ext = Mid(Ext, 1, Len(Ext) - 7)


                FilePath = Ext & "\Import\" & FileName

                Uploader.SaveAs(FilePath)

                'proses import from excel ke database
                uf_ImpotExcel(FilePath)


                Dim ls_Dir As New IO.DirectoryInfo(Ext & "\Import\")
                Dim ls_GetFile As IO.FileInfo() = ls_Dir.GetFiles()
                Dim ls_File As IO.FileInfo
                Dim li_CountDate As Long

                For Each ls_File In ls_GetFile
                    'keep file Import for 30 days
                    li_CountDate = DateDiff(DateInterval.Day, CDate(Format(ls_File.LastWriteTime, "MM/dd/yyyy")), CDate(Format(Now, "MM/dd/yyyy")))
                    If li_CountDate > 30 Then
                        File.Delete(ls_File.FullName)
                    End If
                Next
            Else
                cbMessage.JSProperties("cpMessage") = "Please Select File Excel"
            End If


        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 1)
        End Try
    End Sub

    Private Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSubmit.Click
        ' If validation() = True Then
        Uploader.Enabled = True
        ProgressBar.Value = 0
        importExcel()
        BtnSubmit.Enabled = True
        BtnSaveError.Enabled = True
        'End If
    End Sub

    'check validation data 
    Private Function validationOfExcelData(value As String, message As String, currentRow As Integer) As String
        'check data only numeric
        If (message = "Project Year" Or message = "Volume / Month" Or message = "Cost Target" Or message = "Priority") Then
            If String.IsNullOrEmpty(value) Then
                Return "Row : " + CStr(currentRow) + " - " + message + " only numeric ||" & vbCrLf
            Else
                For i = 0 To value.Length - 1
                    If Convert.ToInt32(value.Chars(i)) < 48 Or Convert.ToInt32(value.Chars(i)) > 57 Then
                        Return "Row : " + CStr(currentRow) + " - " + message + " only numeric ||" & vbCrLf
                    End If
                Next
            End If
        End If

        Dim color = System.Drawing.Color.Red

        If (message = "Sample IADG Compare" Or message = "Sample OEM") Then
            If value <> "Ready" AndAlso value <> "Not" Then
                Return "Row : " + CStr(currentRow) + " - " + message + " Invalid value " + value + "  ( Only accept value Ready or Not ) ||" & vbCrLf
            End If
        End If

        If (message = "New / Multi") Then
            If value <> "New" AndAlso value <> "Multi" Then
                Return "Row : " + CStr(currentRow) + " - " + message + " Invalid value " + value + "  ( Only accept value New or Multi ) ||" & vbCrLf
            End If
        End If

        'check data only date
        'If (message = "DRAWING RECEIVE") Then
        '    Dim isValidDate As Boolean = IsDate(value)
        '    If isValidDate = False Then
        '        Return "Row : " + CStr(currentRow) + " - " + message + " only date (#Format YYYY/DD/MM) ||" & vbCrLf
        '    End If
        'End If

        If (message = "Model") Then
            Dim isValidData As Boolean = ClsIADGSparePartDB.checkModelProject(value)
            If isValidData = False Then
                Return "Row : " + CStr(currentRow) + " - " + message + " invalid value ( " + value + " ) ||" & vbCrLf
            End If
        End If

        If String.IsNullOrEmpty(value) Then
            Return "Row : " + CStr(currentRow) + " - " + message + " cannot be empty ||" & vbCrLf
        Else
            Return ""
        End If
    End Function

    'check if IADG PartNo already on process upload excel
    Private Function validationOfSimilarData(pProjectID As String, pProjectYear As String, pPartNo As String, pModel As String, pUser As String, _currentRow As Integer) As String
        'create function check data exist upload
        'Dim ds As DataSet = clsItemListSourcingDB.getExistsTempUpload(_projectId, _groupId, _commodity, _partno, "1", _pUser)
        Dim ds As DataSet = ClsIADGSparePartDB.getExistsProjectID(pProjectID, pProjectYear, pPartNo, pModel, "1", pUser)
        If ds.Tables(0).Rows.Count > 0 Then
            Return "Row : " + CStr(_currentRow) + " - " + "IADG PartNo " + pPartNo + " already exists on this excel. ||" + vbCrLf
        Else
            Return Nothing
        End If
    End Function

    Protected Sub BtnSaveError_Click(sender As Object, e As EventArgs) Handles BtnSaveError.Click
        up_Excel()
    End Sub

    Private Sub up_Excel()
        Dim NameHeader As String = ""
        If MemoMessage.Text = "" Then
            Exit Sub
        End If

        MemoMessage.Text = "Row Data Error ||" + MemoMessage.Text

        Using Pck As New ExcelPackage
            Dim ws As ExcelWorksheet = Pck.Workbook.Worksheets.Add("Save Log")
            ' ws.DefaultColWidth = 50
            With ws
                .Cells(1, 1, 1, 1).Value = Split(MemoMessage.Text, "||")(0)
                For iRow = 1 To Split(MemoMessage.Text, "||").Count - 1
                    Dim r As Integer = iRow + 1
                    .Cells(r, 1, r, 1).Value = Split(MemoMessage.Text, "||")(iRow)
                    .Column(1).Width = 70
                Next

                Dim rg As OfficeOpenXml.ExcelRange = .Cells(1, 1, Split(MemoMessage.Text, "||").Count, 1)
                rg.Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Font.Name = "Tahoma"
                rg.Style.Font.Size = 10


                Dim rgHeader As OfficeOpenXml.ExcelRange = .Cells(1, 1, 1, 1)
                rgHeader.Style.Fill.PatternType = Style.ExcelFillStyle.Solid
                rgHeader.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue)
                rgHeader.Style.Font.Size = 11
                rgHeader.Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
            End With

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment; filename=Save_Log_" & Format(Date.Now, "yyyy-MM-dd HH:mm:ss") & ".xlsx")
            'Response.BufferOutput = True
            Dim stream As MemoryStream = New MemoryStream(Pck.GetAsByteArray())

            Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length)
            Response.Flush()
            Response.End()
            'Response.Close()

        End Using
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        MemoMessage.Text = ""
    End Sub

End Class
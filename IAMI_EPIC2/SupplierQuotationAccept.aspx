﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SupplierQuotationAccept.aspx.vb" Inherits="IAMI_EPIC2.SupplierQuotationAccept" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function FillComboQuotationNumber() {
        cboQuotationNumber.PerformCallback();
    }

    function FillComboSupplier() {
        cboSupplierCode.PerformCallback();
    }

    function SetSupplierNameAndQuotationNo(s, e) {
        if (s.GetText() == '') {
            txtSupplierName.SetText('');
        } else {
            txtSupplierName.SetText(s.GetSelectedItem().GetColumnText(1));

        }
    }

    function GridLoad(s, e) {
        
        var dtFrom = dtQuotationFrom.GetDate();
        var YearFrom = dtFrom.getFullYear();
        var MonthFrom = dtFrom.getMonth();
        var DateFrom = dtFrom.getDate();
        var dtQFrom = new Date(YearFrom, MonthFrom, DateFrom)

        var dtTo = dtQuotationTo.GetDate();
        var YearTo = dtTo.getFullYear();
        var MonthTo = dtTo.getMonth();
        var DateTo = dtTo.getDate();
        var dtQTo = new Date(YearTo, MonthTo, DateTo)

        if (dtQFrom > dtQTo) {
            toastr.error('Quotation Date From cannot be greater than Quotation Date To!', 'Error');
            dtQuotationFrom.Focus();
            return
        }

        Grid.PerformCallback('load|' + dtQuotationFrom.GetText() + '|' + dtQuotationTo.GetText());
    }

    function downloadValidation(s, e) {
        if (Grid.GetVisibleRowsOnPage() == 0) {
            toastr.info('There is no data to download!', 'Info');
            e.processOnServer = false;
            return;
        }
    }

    function GridCustomButtonClick(s, e) {
        if (e.buttonID == 'det') {
            var rowKey = Grid.GetRowKey(e.visibleIndex);
            window.location.href = 'SupplierQuotationAcceptDetail.aspx?ID=' + rowKey + '|' + cboQuotationNumber.GetText() + '|' + cboSupplierCode.GetText();
        }
    }

    function LoadCompleted(s, e) {
        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }
    }
</script>

<style type="text/css">
      .hidden-div
        {
            display:none;
        }    
    .colwidthbutton
    {
        width: 90px;
    }
    
    .rowheight
    {
        height: 35px;
    }
       
    .col1
    {
        width: 10px;
    }
     .colTo
    {
        width: 15px;
    }
    .col2
    {
        width: 133px;
    }
    .col3
    {
        width: 133px;
    }
    .col4
    {
        width: 25px;
    }
    .col5
    {
        width: 133px;
    }
    .customHeader {
        height: 30px;
    }
    .style1
    {
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">
            <tr>
                <td colspan="11" style="height: 10px">
                    </td>
               </tr>            
            <tr class="hidden-div">
                <td class="col1">
                    </td>
            </tr>
            <tr class="hidden-div">
                <td class="col1">
                    &nbsp;</td>
                <td class="col2">
                    <dx1:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Supplier Code"></dx1:aspxlabel>
                </td>
                <td >
                    <dx1:ASPxComboBox ID="cboSupplierCode" runat="server" ClientInstanceName="cboSupplierCode"
                        Width="120px" Font-Names="Segoe UI"  TextField="Supplier_Code"
                        ValueField="Supplier_Code" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">                            
                        <ClientSideEvents Init="FillComboSupplier" SelectedIndexChanged="SetSupplierNameAndQuotationNo" ValueChanged="FillComboQuotationNumber" />
                        <Columns>            
                            <dx:ListBoxColumn Caption="Supplier Code" FieldName="Supplier_Code" Width="120px" />
                            <dx:ListBoxColumn Caption="Supplier Name" FieldName="Supplier_Name" Width="290px" />                       
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                        <ValidationSettings CausesValidation="false" Display="None">
                            <RequiredField IsRequired="true"/>
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>

                        <ClientSideEvents ValueChanged="FillComboQuotationNumber" />
                    </dx1:ASPxComboBox>
                
                </td>
                <td >
                
                    &nbsp;</td>
                <td >
                
                    &nbsp;</td>
                <td colspan="3">
                    <dx:ASPxTextBox ID="txtSupplierName" runat="server" width="300px" Height="25px" 
                        ClientInstanceName="txtSupplierName" BackColor="#CCCCCC" ReadOnly="True">
                    </dx:ASPxTextBox> 
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr class="rowheight">
                <td class="col1">
                    &nbsp;</td>
                <td class="col2">
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Quotation Date Range"></dx1:aspxlabel>
                </td>
                <td class="col2">
                    <dx:aspxdateedit ID="dtQuotationFrom" runat="server" Theme="Office2010Black" 
                        Width="120px" ClientInstanceName="dtQuotationFrom"
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        <ClientSideEvents ValueChanged="FillComboQuotationNumber" />
                    </dx:aspxdateedit>
                
                </td>
                <td class="colTo">
                
                    <dx1:aspxlabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="To"></dx1:aspxlabel> 
                
                </td>
                <td class="col2">
                
                 <dx:aspxdateedit ID="dtQuotationTo" runat="server" Theme="Office2010Black" 
                        Width="120px" AutoPostBack="false" ClientInstanceName="dtQuotationTo"
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        <ClientSideEvents ValueChanged="FillComboQuotationNumber" />
                    </dx:aspxdateedit> 
                
                </td>
                <td colspan="3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr class="hidden-div">
                <td class="col1">
                    &nbsp;</td>
                <td class="col2">
                    <dx1:aspxlabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Quotation Number"></dx1:aspxlabel> &nbsp;
                </td>
                <td >
                    &nbsp;</td>
                <td >
                
                    &nbsp;</td>
                <td >
                
                    &nbsp;</td>
                <td colspan="3">
                    <dx1:ASPxComboBox ID="cboQuotationNumber" runat="server" ClientInstanceName="cboQuotationNumber"
                        Width="200px" Font-Names="Segoe UI"  TextField="Quotation_No"
                        ValueField="Quotation_No" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">                            
                        <ClientSideEvents Init="FillComboQuotationNumber" />
                        <Columns>            
                            <dx:ListBoxColumn Caption="Quotation Number" FieldName="Quotation_No" Width="100px" />                       
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                        <ValidationSettings CausesValidation="false" Display="None">
                            <RequiredField IsRequired="true"/>
                        </ValidationSettings>
                        <InvalidStyle BackColor="Red"></InvalidStyle>
                    </dx1:ASPxComboBox>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            
            <tr class="rowheight">
                <td colspan="11" class="rowheight">
                    </td>
               </tr>
            <tr class="rowheight">
                <td></td>
                <td colspan="10">
                    <table>
                        <tr class="rowheight">
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnShow" runat="server" Text="Show Data" UseSubmitBehavior="False"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                                    ClientInstanceName="btnShow" Theme="Default">                        
                                    <ClientSideEvents Click="GridLoad" />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnExcel" Theme="Default">                        
                                    <Paddings Padding="2px" />
                                    <ClientSideEvents
                                        Click="downloadValidation"
                                    />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 10px">
                    </td>
                <td>
                    </td>
                <td  colspan="3">
                    </td>
                <td>
                    </td>
                <td> 
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                </td>
            </tr>
        </table>
        
    </div>
   <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="Quotation_No;RFQ_Number;Rev;Supplier_Code;Supplier_Name" >
            
            <Columns>
                <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" Width="50px" FixedStyle="Left">
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="det" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>                    
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Quotation Number" FieldName="Quotation_No" FixedStyle="Left"
                     VisibleIndex="1" Width="180px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Rev" FieldName="Rev" FixedStyle="Left"
                     VisibleIndex="2" Width="50px">
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Quotation Date" FieldName="Quotation_Date" FixedStyle="Left"
                     VisibleIndex="3" Width="120px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="RFQ Set Number" FieldName="RFQSetNumber" 
                     VisibleIndex="4" Width="180px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="PR Number" FieldName="PRNumber" 
                     VisibleIndex="5" Width="180px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="RFQ Number" FieldName="RFQ_Number" 
                     VisibleIndex="6" Width="180px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="RFQ Date" FieldName="RFQ_Date" 
                     VisibleIndex="7" Width="120px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Deadline Date" FieldName="RFQ_DueDate" 
                     VisibleIndex="8" Width="120px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Status" FieldName="Quotation_Status" 
                     VisibleIndex="9" Width="120px">
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                     <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Supplier Code" FieldName="Supplier_Code" Visible="false"
                     VisibleIndex="10" Width="0px">
                </dx:GridViewDataTextColumn> 
                <dx:GridViewDataTextColumn Caption="Supplier Name" FieldName="Supplier_Name" Visible="true"
                     VisibleIndex="11" Width="150px">
                </dx:GridViewDataTextColumn>                           
             </Columns>

            <SettingsBehavior AllowFocusedRow="True" AllowSort="False" AllowDragDrop="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
            <SettingsPager PageSize="8"></SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollableHeight="200" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>
            
            <ClientSideEvents 
                CustomButtonClick="GridCustomButtonClick"
                EndCallback="LoadCompleted"
            />
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" 
                AllowInsert="False" />

            <Styles Header-Paddings-Padding="2px" EditFormColumnCaption-Paddings-PaddingLeft="0px" EditFormColumnCaption-Paddings-PaddingRight="0px" >
                <Header CssClass="customHeader" HorizontalAlign="Center">
                </Header>
            </Styles>
        </dx:ASPxGridView>
    </div>
    <%--<div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
      
        </table>
    </div>--%>
</div>
</asp:Content>
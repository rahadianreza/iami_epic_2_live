﻿Imports DevExpress.XtraPrinting
Imports System.IO
Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.XtraCharts
Imports System.Drawing

Public Class MaterialPartChart
    Inherits System.Web.UI.Page


#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A040")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If Not IsPostBack Then
            cboPeriodFrom.Value = CDate(Now.ToShortDateString)
            cboPeriodTo.Value = CDate(Now.ToShortDateString)
            cboType.SelectedIndex = 0
            cboCategoryMaterial.SelectedIndex = 0
            cboCurrency.Value = "USD"

            cboMaterialCode.DataSource = ClsMaterialPartDB.GetMaterialCode("ALL", pUser)
            cboMaterialCode.DataBind()
        End If

    End Sub

    Private Sub cboMaterialCode_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboMaterialCode.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(1)

        cboMaterialCode.DataSource = ClsMaterialPartDB.getMaterialCode(pFunction, pUser)
        cboMaterialCode.DataBind()
    End Sub

    Private Sub ChartGT_CustomCallback(ByVal sender As Object, ByVal e As DevExpress.XtraCharts.Web.CustomCallbackEventArgs) Handles ChartGT.CustomCallback
        Dim puser As String = Session("user")

        Dim pAction As String = Split(e.Parameter, "|")(0)
        Select Case pAction
            Case "load"
                Dim pPeriodFrom As String = cboPeriodFrom.Value
                Dim pPeriodTo As String = cboPeriodTo.Value
                Dim pType As String = Split(e.Parameter, "|")(3)
                Dim pCategoryMaterial As String = Split(e.Parameter, "|")(4)
                Dim pMaterialCode As String = Split(e.Parameter, "|")(5)
                Dim pCurrency As String = Split(e.Parameter, "|")(6)

                bindDataGT(pPeriodFrom, pPeriodTo, pType, pCategoryMaterial, pMaterialCode, pCurrency)
        End Select
        'bindDataGT()
    End Sub

    Private Sub bindDataGT(ByVal pPeriodFrom As String, ByVal pPeriodTo As String, ByVal pType As String, ByVal pCategoryMaterial As String, ByVal pMaterialCode As String, ByVal pCurrency As String)
        Dim tablename As String = ""
        Dim filtercode As String = ""

        'add chartGT title
        Dim cht1 As New ChartTitle
        'If cht1.Text = "Chart Title" Then
        '    If Not cht1 Is Nothing Then
        '        ChartGT.Titles.Remove(cht1)
        '    End If
        '    cht1.Text = cboCategoryMaterial.Value + " : " + cboCategoryMaterial.Text
        '    cht1.Font = New Font("Verdana", 16.0F)
        '    ChartGT.Titles.Add(cht1)
        'End If

        Dim dt As DataTable = getMonthDataGT(pPeriodFrom, pPeriodTo, pType, pCategoryMaterial, pMaterialCode, pCurrency)
        ChartGT.DataSource = dt
        ChartGT.DataBind()

        ChartGT.SeriesDataMember = "GrandTotal"

        ChartGT.SeriesTemplate.ArgumentDataMember = "Period"
        ChartGT.SeriesTemplate.ValueDataMembers.AddRange(New String() {"UnitPrice"})
        ChartGT.SeriesTemplate.ArgumentScaleType = ScaleType.Qualitative
    End Sub

    Private Function getMonthDataGT(ByVal pPeriodFrom As String, ByVal pPeriodTo As String, ByVal pType As String, ByVal pCategoryMaterial As String, ByVal pMaterialCode As String, pCurrency As String) As DataTable
        Dim ds As New DataSet

        Dim tablename As String = ""
        Dim filter As String = ""
        Dim filtercode As String = ""
        Dim dtView As DataTable


        ds = ClsMaterialPartDB.GetMaterialChart(pCategoryMaterial, pMaterialCode, pType, cboPeriodFrom.Value, cboPeriodTo.Value, pCurrency, pUser)

        dtView = New DataTable
        dtView = ds.Tables(0)

        Dim dt As New DataTable

        filtercode = "GrandTotal"

        dt.Columns.Add(New DataColumn("Period", GetType(String)))
        dt.Columns.Add(New DataColumn(filtercode, GetType(String)))
        dt.Columns.Add(New DataColumn("UnitPrice", GetType(Decimal)))

        Dim newDr, dr As DataRow

        For Each dr In dtView.Rows
            newDr = dt.NewRow

            newDr("Period") = dr.Item("Period")
            'newDr(filtercode) = dr.Item(filtercode).ToString
            newDr("UnitPrice") = dr.Item("UnitPrice").ToString

            dt.Rows.Add(newDr)
        Next

        Return dt
    End Function

End Class
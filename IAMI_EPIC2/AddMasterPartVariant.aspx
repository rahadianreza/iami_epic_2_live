﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AddMasterPartVariant.aspx.vb" Inherits="IAMI_EPIC2.AddMasterPartVariant" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function Message(s, e) {            
            if (s.cp_type == "1") {
                toastr.success(s.cp_message, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;                
            }

            else if (s.cp_type == "2") {
                toastr.error(s.cp_message, 'Error');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }
        
        function SubmitProcess(s, e) {
            if (txtType.GetText() == '') {
                toastr.warning('Please Input Type !', 'Warning');
                txtType.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (txtVariantCode.GetText() == '') {
                toastr.warning('Please Input Variant Code !', 'Warning');
                txtVariantCode.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (txtVariantName.GetText() == '') {
                toastr.warning('Please Input Variant Name !', 'Warning');
                txtVariantName.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (txtMaterialFUSAP.GetText() == '') {
                toastr.warning('Please Input Material FU SAP !', 'Warning');
                txtMaterialFUSAP.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            Grid.UpdateEdit();            
            cbSave.PerformCallback('loadMessage');
            //cbSave.PerformCallback('Save|' + cboUOM.GetValue());
        }

        function OnAddCheckedChanged(s, e) {
            Grid.SetFocusedRowIndex(-1);
            if (s.GetValue() == -1) s.SetValue(1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "Cek", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "Cek", s.GetValue());
                }
            }
        }

        function Grid_EndCallback(s, e) {
            if (s.cp_message != '') {
                Message(s.cp_type, s.cp_message);
                s.cp_message = '';
            }
        }

    </script>
    <style type="text/css">
        .td-col-l 
        {
            padding:0px 0px 0px 10px;
            width:120px;
        }
        .td-col-m
        {
            width:20px;
        }
        .td-col-r
        {
            width:200px;
        }
        
        .tr-height
        {
            height: 35px;
        }
            
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">       
        <tr style="height:10px">
            <td colspan="5"></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Type">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtType" runat="server" Width="170px" Height="25px" ClientInstanceName="txtType"
                    MaxLength="25" AutoCompleteType="Disabled">
                    <ValidationSettings CausesValidation="false" Display="None">
                        <RequiredField IsRequired="true" ErrorText="*" />
                    </ValidationSettings>
                    
                </dx:ASPxTextBox>
            </td>
            <td></td>
        </tr>
        
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Variant Code">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtVariantCode" runat="server" Width="170px" Height="25px" ClientInstanceName="txtVariantCode"
                    MaxLength="25" AutoCompleteType="Disabled">
                    <ValidationSettings CausesValidation="false" Display="None">
                        <RequiredField IsRequired="true" ErrorText="*" />
                    </ValidationSettings>
                    
                </dx:ASPxTextBox>
            </td>
            <td></td>
        </tr>

        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Variant Name">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtVariantName" runat="server" Width="170px" Height="25px" ClientInstanceName="txtVariantName"
                    MaxLength="100" AutoCompleteType="Disabled">
                    <ValidationSettings CausesValidation="false" Display="None">
                        <RequiredField IsRequired="true" ErrorText="*" />
                    </ValidationSettings>
                    
                </dx:ASPxTextBox>
            </td>
            <td></td>
        </tr>

        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material FU SAP">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtMaterialFUSAP" runat="server" Width="170px" Height="25px" ClientInstanceName="txtMaterialFUSAP"
                    MaxLength="100" AutoCompleteType="Disabled">
                    <ValidationSettings CausesValidation="false" Display="None">
                        <RequiredField IsRequired="true" ErrorText="*" />
                    </ValidationSettings>
                    
                </dx:ASPxTextBox>
            </td>
            <td></td>
        </tr>

       <tr >
           <td class="td-col-l"></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>

       </tr>
        <tr style="height: 25px;">
            <td class="td-col-l"></td>
            <td class="td-col-l"></td>
            <td></td>
            <td style="width: 500px">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" AutoPostBack="false"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack"
                    Theme="Default">
                    <Paddings Padding="2px" />
                    <ClientSideEvents Click="function(s, e) {     
                    var pathArray = window.location.pathname.split('/');
                    var url = window.location.origin + '/' + pathArray[1] + '/MasterPartVariant.aspx'
                    if (pathArray[1] == 'AddMasterPartVariant.aspx') {
                        window.location.href = window.location.origin + '/MasterPartVariant.aspx';
                    }
                    else {
                        window.location.href = url;
                    }          
                                   
	 //window.location.href = window.location.origin + '/MasterPartVariant.aspx';
}" />
                </dx:ASPxButton>

                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">                   
                    <ClientSideEvents Click="function(s, e) {
	                var msg = confirm('Are you sure want to submit this data ?');                
                        if (msg == false) {
                            e.processOnServer = false;
                            return;
                        }
	                SubmitProcess(s,e);}" />
                   
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
            <td></td>
           
        </tr>
        <tr style="display:none">
            <td colspan="9">
               
            </td>
        </tr>
    </table>
    <tr >
           <td class="td-col-l"></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
    </tr>
    <div style="padding: 5px 5px 5px 5px">
    <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid" Width="100%"
        EnableTheming="True" KeyFieldName="PartNo" Theme="Office2010Black" Font-Size="9pt" Font-Names="Segoe UI"  
        >        
        <Columns>
             <dx:GridViewDataCheckColumn Caption="" FieldName="Cek" Name="Cek" VisibleIndex="1" Width="50px">
                <PropertiesCheckEdit 
                    ValueChecked="1" ValueType="System.String" ValueUnchecked="0">
                </PropertiesCheckEdit>
                <HeaderCaptionTemplate>
                    <dx:ASPxCheckBox 
                        ID="chkAdd" runat="server" ClientInstanceName="chkAdd" ClientSideEvents-CheckedChanged="OnAddCheckedChanged" 
                        ValueType="System.String" ValueChecked="1" ValueUnchecked="0">
                    </dx:ASPxCheckBox>
                </HeaderCaptionTemplate>
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">                    
                </HeaderStyle>
             </dx:GridViewDataCheckColumn>

            <dx:GridViewDataTextColumn FieldName="PartNo" Caption="PartNo"
                VisibleIndex="2" Width="100px" Settings-AutoFilterCondition="Contains"  Visible="True">
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="PartName" Caption="Part Name"
                VisibleIndex="3" Width="300px" Settings-AutoFilterCondition="Contains"  Visible="True">
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Qty" Caption="Quantity"
                VisibleIndex="4" Width="100px" Settings-AutoFilterCondition="Contains" >
                <HeaderStyle Paddings-PaddingLeft="4px" HorizontalAlign="Center" VerticalAlign="Middle">
                    <Paddings PaddingLeft="4px"></Paddings>
                </HeaderStyle>
                <PropertiesTextEdit MaxLength="10" />                
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle"></CellStyle>                
            </dx:GridViewDataTextColumn>
            
        </Columns>
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
        <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
        <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
            <BatchEditSettings ShowConfirmOnLosingChanges="False" />
        </SettingsEditing>
        <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True" VerticalScrollableHeight="270" VerticalScrollBarMode="Visible" />
        <Styles>
            <Header>
                <Paddings Padding="2px"></Paddings>
            </Header>
        </Styles>
    </dx:ASPxGridView> 
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
            <ClientSideEvents  EndCallback="Message" />
        </dx:ASPxCallback>
    </div>
</asp:Content>

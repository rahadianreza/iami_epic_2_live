﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="AddPRJList.aspx.vb" Inherits="IAMI_EPIC2.AddPRJList" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function MessageError(s, e) {
            if (s.cpMessage == "Data Saved Successfully!") {
                window.location.href =  './AddPRJList.aspx?ID=' + s.cpID;

                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false; 
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Updated Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                millisecondsToWait = 2000;
                setTimeout(function () {
                    var pathArray = window.location.pathname.split('/');
                    var url = window.location.origin + '/' + pathArray[1] + '/PRJList.aspx';
                }, millisecondsToWait);
            }
            else if (s.cpMessage == "Data Clear Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Cancel Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }
        function GridLoad() {
            ComparePeriod()

            Grid.PerformCallback('refresh');
            tmpProjectType.SetText(cboProjectType.GetValue());
        };
        function fn_AllowonlyNumeric(s, e) {
            var theEvent = e.htmlEvent || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;

            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault)
                    theEvent.preventDefault();
            }
        };
        function MessageDelete(s, e) {

            if (s.cpMessage == "Data Deleted Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                //window.location.href = "/ItemList.aspx";
                millisecondsToWait = 2000;
                setTimeout(function () {
                    var pathArray = window.location.pathname.split('/');
                    var url = window.location.origin + '/' + pathArray[1] + '/PRJList.aspx'
                }, millisecondsToWait);

            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
        }

        function FilterGroup() {
            cboGroupItem.PerformCallback('filter|' + cboPRType.GetValue());
        }

        function FilterCategory() {
            cboCategory.PerformCallback('filter|' + cboGroupItem.GetValue());
        }
        function SetCode(s, e) {
            tmpProjectType.SetText(cboProjectType.GetValue());
        }

        function ComparePeriod() {
            var startdate = StartPeriod.GetDate(); // get value dateline date
            var projectdate = ProjectDate.GetDate(); // get value dateline date
            var enddate = EndPeriod.GetDate(); // get value dateline date
            
            var yearstartdate = startdate.getFullYear(); // where getFullYear returns the year (four digits)
            var monthstartdate = startdate.getMonth(); // where getMonth returns the month (from 0-11)
            //var daystartdate = startdate.getDate();   // where getDate returns the day of the month (from 1-31)
            var LastDayOfMonth = new Date(startdate.getFullYear(), startdate.getMonth() + 1, 0);
            var daystartdate = LastDayOfMonth.getDate();

            //alert(daystartdate);
            if (daystartdate < 10) {
                daystartdate = '0' + daystartdate
            }

            if (monthstartdate < 10) {
                monthstartdate = '0' + monthstartdate
            }

            var vStartPeriod = yearstartdate + '-' + monthstartdate + '-' + daystartdate;


            var yearprojectdate = projectdate.getFullYear(); // where getFullYear returns the year (four digits)
            var monthprojectdate = projectdate.getMonth(); // where getMonth returns the month (from 0-11)
            var dayprojectdate = projectdate.getDate();   // where getDate returns the day of the month (from 1-31)

            if (dayprojectdate < 10) {
                dayprojectdate = '0' + dayprojectdate;
            }

            if (monthprojectdate < 10) {
                monthprojectdate = '0' + monthprojectdate;
            }

            var vProjectDate = yearprojectdate + '-' + monthprojectdate + '-' + dayprojectdate;

            if (vProjectDate > vStartPeriod) {
                toastr.warning('Start Period must be bigger than Project Date', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return false;
            }

            var yearenddate = enddate.getFullYear(); // where getFullYear returns the year (four digits)
            var monthenddate = enddate.getMonth(); // where getMonth returns the month (from 0-11)
            //var dayenddate = enddate.getDate();   // where getDate returns the day of the month (from 1-31)
            var LastDayOfMonth = new Date(enddate.getFullYear(), enddate.getMonth() + 1, 0);
            var dayenddate = LastDayOfMonth.getDate();
            //alert(dayenddate);
            if (dayenddate < 10) {
                dayenddate = '0' + dayenddate;
            }

            if (monthenddate < 10) {
                monthenddate = '0' + monthenddate;
            }

            var vEndPeriod = yearenddate + '-' + monthenddate + '-' + dayenddate;
            if (vEndPeriod < vStartPeriod) {
                toastr.warning('End Period must be bigger than Start Period', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return false;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project ID">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 15px;">
                <dx:ASPxTextBox ID="txtProjectID" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="170px" ClientInstanceName="Project ID" MaxLength="20" Enabled="false"
                    Text="(Auto Generated)">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Name">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtProjectName" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="220px" ClientInstanceName="txtProjectName" MaxLength="30">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Date">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxDateEdit ID="ProjectDate" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="ProjectDate" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <ClientSideEvents ValueChanged="GridLoad" />
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 15px;">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Project Type">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px;">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxComboBox ID="cboProjectType" runat="server" ClientInstanceName="cboProjectType"
                    Width="220px" Font-Names="Segoe UI" TextField="Description" ValueField="Code"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                 
            </td>
            <td class="hidden-div" style="display:none">
                        <dx:ASPxTextBox ID="tmpProjectType" runat="server" Font-Names="Segoe UI" Font-Size="8pt"
                            Width="170px" ClientInstanceName="tmpProjectType" MaxLength="15" Theme="Office2010Silver">
                        </dx:ASPxTextBox>
                    </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Start Period">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                 <dx:ASPxTimeEdit ID="StartPeriod" Width="120px" Theme="Office2010Black" Font-Names="Segoe UI"
                    Font-Size="9pt" EditFormat="Custom" ClientInstanceName="StartPeriod" runat="server"
                    DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy">
                    <ClientSideEvents ValueChanged ="ComparePeriod" />
                    <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>   
                </dx:ASPxTimeEdit>
                
            </td>
        </tr>
         <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="lblEndPeriod" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="End Period">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTimeEdit ID="EndPeriod" Width="120px" Theme="Office2010Black" Font-Names="Segoe UI"
                    Font-Size="9pt" EditFormat="Custom" ClientInstanceName="EndPeriod" runat="server"
                    DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy">
                    <ClientSideEvents ValueChanged="ComparePeriod" />
                    <ButtonStyle Font-Size="9pt" Paddings-Padding="3px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxTimeEdit>
                
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Rate USD/IDR">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtRateUSD" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtRateUSD"  HorizontalAlign="Right">
                    <MaskSettings Mask="<0..100000000g>.<00..99>" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Rate YEN/IDR">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtRateYEN" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtRateYEN" MaxLength="11" HorizontalAlign="Right">
                     <MaskSettings Mask="<0..100000000g>.<00..99>" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Rate BTH/IDR">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxTextBox ID="txtRateBATH" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtRateBATH" MaxLength="11" HorizontalAlign="Right">
                     <MaskSettings Mask="<0..100000000g>.<00..99>" />
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="OTS Sample">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxDateEdit ID="OTSSample" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="OTSSample" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <ClientSideEvents ValueChanged="GridLoad" />
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>
            </td>
        </tr>
        <tr style="height: 25px">
            <td style="width: 120px; padding-left: 100px; padding-top: 10px;">
                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="SOP Timing">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td style="width: 50px; padding-top: 10px;">
                <dx:ASPxDateEdit ID="SOPTiming" runat="server" Theme="Office2010Black" Width="120px"
                    ClientInstanceName="SOPTiming" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                    Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                    <CalendarProperties>
                        <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </HeaderStyle>
                        <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </DayStyle>
                        <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                            <Paddings Padding="5px"></Paddings>
                        </WeekNumberStyle>
                        <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </FooterStyle>
                        <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                            <Paddings Padding="10px"></Paddings>
                        </ButtonStyle>
                    </CalendarProperties>
                    <ClientSideEvents ValueChanged="GridLoad" />
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxDateEdit>
            </td>
        </tr>
        <tr>
            <td style="height: 30px;">
                &nbsp;
            </td>
        </tr>
        <tr style="height: 25px;">
            <td style="width: 50px;">
                &nbsp;
            </td>
            <td style="width: 50px">
                &nbsp;
            </td>
            <td style="width: 500px">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack"
                    Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                    <ClientSideEvents Click="function(s, e) {
	 if (OTSSample.GetDate() &lt; ProjectDate.GetDate()) {
                toastr.warning('Please Input OTS Sample Date Be Grather than Project Date!', 'Warning');
                OTSSample.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            
        }
	 if (SOPTiming.GetDate() &lt; OTSSample.GetDate()) {
                toastr.warning('Please Input SOP Timing Date Be Grather than OTS Sample!', 'Warning');
                SOPTiming.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            
        }

        ComparePeriod();
        
}" />
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
            </td>
            <td>
                &nbsp;<asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRType'">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Category'">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select RTRIM(Par_Code) As Group_Code, RTRIM(Par_Description) As Group_Name From Mst_Parameter Where Par_Group = 'GroupItem'">
                </asp:SqlDataSource>
                &nbsp;<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="Select Rtrim(Par_Code) AS Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'UOM'">
                </asp:SqlDataSource>
                <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
                    <ClientSideEvents CallbackComplete="SetCode" />
                </dx:ASPxCallback>
                <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
                    <ClientSideEvents Init="MessageError" />
                </dx:ASPxCallback>
                   <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
                    <ClientSideEvents Init="MessageError" />                                           
                </dx:ASPxCallback>
                   <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
                    <ClientSideEvents CallbackComplete="MessageDelete" />                                           
                </dx:ASPxCallback>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>

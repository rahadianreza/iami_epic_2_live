﻿Imports DevExpress.Web.ASPxGridView
Imports System.IO
Imports DevExpress.XtraPrinting
Imports DevExpress.Web.ASPxEditors

Public Class DrawingItemListSourcing
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            up_GridLoad("0")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("I040")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            cboProjectType.SelectedIndex = 0
            cboStatusDrawing.SelectedIndex = 0
            cboProject.SelectedIndex = 0

            up_FillComboProjectType(cboProjectType, statusAdmin, pUser, "ProjectType")
            up_FillComboProject(cboProject, statusAdmin, pUser, cboProjectType.Value)
        End If
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
        End If
        fRefresh = False
       
    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Dim _itmLstSource As New clsItemListSourcing
        Dim pErr As String = ""
        Try
            With Grid
                If e.UpdateValues.Count > 0 Then
                    For i = 0 To e.UpdateValues.Count - 1
                        Dim No As Integer = e.UpdateValues(i).NewValues("No")
                        Dim Project_ID As String = e.UpdateValues(i).NewValues("Project_ID")
                        Dim Part_No As String = e.UpdateValues(i).OldValues("Part_No")
                        Dim Upc As String = e.UpdateValues(i).OldValues("Upc")
                        Dim Fna As String = e.UpdateValues(i).OldValues("Fna")
                        Dim Epl_Part_No As String = e.UpdateValues(i).OldValues("Epl_Part_No")
                        Dim Dtl_Upc As String = e.UpdateValues(i).OldValues("Dtl_Upc")
                        Dim Dtl_Fna As String = e.UpdateValues(i).OldValues("Dtl_Fna")
                        Dim Usg_Seq As String = e.UpdateValues(i).OldValues("Usg_Seq")
                        Dim Qty As String = e.UpdateValues(i).OldValues("Qty")
                        Dim D_C As String = e.UpdateValues(i).OldValues("D_C")
                        Dim Body_Color As String = e.UpdateValues(i).OldValues("Body_Color")
                        Dim Extr_Color As String = e.UpdateValues(i).OldValues("Extr_Color")
                        Dim Status_Drawing As String = e.UpdateValues(i).NewValues("Status_Drawing")

                        clsItemListSourcingDB.InsertStatusDrawing(Project_ID, Part_No, Upc, Fna, Dtl_Upc, Dtl_Fna, Usg_Seq, Qty, D_C,  pUser, Status_Drawing)
                    Next
                End If

                show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
            End With
        Catch ex As Exception
            show_error(MsgTypeEnum.ErrorMsg, ex.Message, 1)
        End Try
    End Sub

    Private Sub Grid_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If (e.Column.FieldName = "Status_Drawing") Then
            e.Editor.ReadOnly = False
        Else
            e.Editor.ReadOnly = True
        End If

        'Dim grid As ASPxGridView = sender
        'If () Then
        '    If clsItemListSourcingDB.checkStatusDrawingComplete(cboProject.Value, cboGroup.Value, cboCommodity.Value, grid.GetRowValues(e.VisibleIndex, "Part_No").ToString()) = "1" Then
        '        e = False
        '    End If
        'End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else

            up_GridLoad("1")
        End If
    End Sub

    Private Sub up_FillComboProjectType(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                              pAdminStatus As String, pUserID As String, _
                              Optional ByRef pGroup As String = "", _
                              Optional ByRef pParent As String = "", _
                              Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboOnlyPT01PT02WithALL(pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_FillComboGroupCommodityPIC(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _param As String, _param2 As String, _param3 As String, _type As String, _
                            pAdminStatus As String, pUserID As String, _
                            Optional ByRef pGroup As String = "", _
                            Optional ByRef pParent As String = "", _
                            Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsItemListSourcingDB.getGroupCommodityPIC(_param, _param2, _param3, _type, "Y", "1", pUserID)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsItemListSourcingDB.getHeaderGridDrawing(cboProject.Value, "", "", "", cboStatusDrawing.Value, pUser)
        If ErrMsg = "" Then

            Grid.DataSource = Ses
            Grid.DataBind()

        End If
    End Sub
#End Region

#Region "Event Control"
    Private Sub up_Excel()
        up_GridLoad("1")

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ItemListSourcing" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub up_FillComboProject(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                             pAdminStatus As String, pUserID As String, projtype As String, _
                             Optional ByRef pGroup As String = "", _
                             Optional ByRef pParent As String = "", _
                             Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboProjectWithALL(projtype, pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub cboProject_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboProject.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProjectType As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboProject(cboProject, statusAdmin, pUser, "")

        ElseIf pFunction = "filter" Then
            up_FillComboProject(cboProject, statusAdmin, pUser, pProjectType)
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Response.Redirect("DrawingItemListSourcingAcceptance.aspx")
    End Sub
   
    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        'If e.VisibleIndex = 0 Then
        
        If e.DataColumn.FieldName = "Status_Drawing" Then
            If Grid.GetRowValues(e.VisibleIndex, "Status_Drawing") = "1" Then
                Dim ds As DataSet = clsItemListSourcingDB.CheckingStatusDrawing(Grid.GetRowValues(e.VisibleIndex, "Project_ID"), Grid.GetRowValues(e.VisibleIndex, "Part_No"), Grid.GetRowValues(e.VisibleIndex, "Upc"), Grid.GetRowValues(e.VisibleIndex, "Fna"), Grid.GetRowValues(e.VisibleIndex, "Dtl_Upc"), Grid.GetRowValues(e.VisibleIndex, "Dtl_Fna"), _
                                                               Grid.GetRowValues(e.VisibleIndex, "Usg_Seq"), Grid.GetRowValues(e.VisibleIndex, "Qty"), Grid.GetRowValues(e.VisibleIndex, "D_C"))
                If ds.Tables(0).Rows(0)(0).ToString = "1" Then

                    e.Cell.Text = ""
                    e.Cell.Attributes.Add("onclick", "event.cancelBubble = true")
                    e.Cell.Attributes.Add("readonly", "true")
                End If
            End If
        End If
        'End If
    End Sub

#End Region
End Class
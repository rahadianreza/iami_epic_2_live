﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="RFQCreate.aspx.vb" Inherits="IAMI_EPIC2.RFQCreate" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;

            if (currentColumnName != "AllowCheck") {
                Grid.batchEditApi.EndEdit();
                e.cancel = true;
            }

            currentEditableVisibleIndex = e.visibleIndex;

            //if (currentColumnName == "GroupID" || currentColumnName == "MenuID" || currentColumnName == "MenuDesc") {
            //e.cancel = true;
            //}
            //currentEditableVisibleIndex = e.visibleIndex;
        }



        function GetMessage(s, e) {
              if (s.cpMessage == 'Draft data saved successfull') {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                btnSubmit.SetEnabled(true);
                btnPrint.SetEnabled(true);
                cboPRNumber.SetEnabled(false);
               
                txtRev.SetText(s.cpRevision);
                txtRFQNumber.SetText(s.cpRFQSetNo);

                txtRFQNo1.SetText(s.cpRFQNo1);
                txtRFQNo2.SetText(s.cpRFQNo2);
                txtRFQNo3.SetText(s.cpRFQNo3);
                txtRFQNo4.SetText(s.cpRFQNo4);
                txtRFQNo5.SetText(s.cpRFQNo5);

                Grid.PerformCallback('draft|' + cboPRNumber.GetText() + '|' + txtRFQNumber.GetText());

            }
            else if (s.cpMessage == 'Data reject successfull') {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                btnDraft.SetEnabled(true);
                btnSubmit.SetEnabled(false);
                btnReject.SetEnabled(false);
            }
            else if (s.cpMessage == 'Data saved successfull') {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                //01-04-2019 disabled component 
                btnDraft.SetEnabled(false);
                btnSubmit.SetEnabled(false);
                txtRFQTitle.SetEnabled(false);
                dtDatelineDate.SetEnabled(false);
                cboCurrency.SetEnabled(false);
                cboSupplier1.SetEnabled(false);
                cboSupplier2.SetEnabled(false);
                cboSupplier3.SetEnabled(false);
                cboSupplier4.SetEnabled(false);
                cboSupplier5.SetEnabled(false);

                txtRev.SetText(s.cpRevision);
                txtRFQNumber.SetText(s.cpRFQSetNo);

                txtRFQNo1.SetText(s.cpRFQNo1);
                txtRFQNo2.SetText(s.cpRFQNo2);
                txtRFQNo3.SetText(s.cpRFQNo3);
                txtRFQNo4.SetText(s.cpRFQNo4);
                txtRFQNo5.SetText(s.cpRFQNo5);

//                txtDepartment.SetText(s.cpDepartment);
//                txtSection.SetText(s.cpSection);
//                txtPRType.SetText(s.cpPRType);


                //millisecondsToWait = 2000;
                //setTimeout(function () {
                //    var pathArray = window.location.pathname.split('/');
                //    var url = window.location.origin + '/' + pathArray[1] + '/RFQList.aspx';
                //    //window.location.href = url;
                //    //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                //    if (pathArray[1] == "RFQCreate.aspx") {
                //        window.location.href = window.location.origin + '/RFQList.aspx';
                //    }
                //    else {
                //        window.location.href = url;
                //    }
                //}, millisecondsToWait);


                //            window.location.href = "/RFQList.aspx";

                //Grid.PerformCallback('save|' + cboPRNumber.GetText() + '|' + txtRFQNo1.GetText());         
            }
            else if (s.cpMessage == null || s.cpMessage == "") {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                txtDepartment.SetText(s.cpDepartment);
                txtSection.SetText(s.cpSection);
                txtPRType.SetText(s.cpPRType);

            }
            else {
                toastr.warning(s.cpMessage, 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                txtDepartment.SetText(s.cpDepartment);
                txtSection.SetText(s.cpSection);
                txtPRType.SetText(s.cpPRType);
            }
        }

        //21-03-2019
        function validation() {

            if (cboPRNumber.GetText() == '') {
                toastr.warning('Please Select PR Number!', 'Warning');
                cboPRNumber.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier1.GetSelectedIndex() == -1 && cboSupplier2.GetSelectedIndex() == -1 && cboSupplier3.GetSelectedIndex() == -1 && cboSupplier4.GetSelectedIndex() == -1 && cboSupplier5.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Supplier !', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier1.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Supplier 1!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier5.GetText() != "") {
                if (cboSupplier5.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 5!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier4.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 4!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 3!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier5.GetText() == cboSupplier1.GetText() || cboSupplier5.GetText() == cboSupplier2.GetText() || cboSupplier5.GetText() == cboSupplier3.GetText() || cboSupplier5.GetText() == cboSupplier4.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier4.GetText() == cboSupplier1.GetText() || cboSupplier4.GetText() == cboSupplier2.GetText() || cboSupplier4.GetText() == cboSupplier3.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetText() == cboSupplier1.GetText() || cboSupplier3.GetText() == cboSupplier2.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }
            else if (cboSupplier4.GetText() != "") {
                if (cboSupplier4.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 4!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 3!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier4.GetText() == cboSupplier1.GetText() || cboSupplier4.GetText() == cboSupplier2.GetText() || cboSupplier4.GetText() == cboSupplier3.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetText() == cboSupplier1.GetText() || cboSupplier3.GetText() == cboSupplier2.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }
            else if (cboSupplier3.GetText() != "") {
                if (cboSupplier3.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 3!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetText() == cboSupplier1.GetText() || cboSupplier3.GetText() == cboSupplier2.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }

            else if (cboSupplier2.GetText() != "") {
                if (cboSupplier2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }

            if (cboCurrency.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Currency!', 'Warning');
                cboCurrency.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (txtRFQNo5.GetText() != "" && cboSupplier5.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Supplier 5!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            if (dtDatelineDate.GetDate() <= dtDateToday.GetDate()) {
                toastr.warning('Please Input Deadline Date Be Grather than Current Date!', 'Warning');
                PODate.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

        function SubmitProcess() {
            //21-03-2019
            validation();

            startIndex = 0;

            var check = '';
            var x = false;

            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                check = parseFloat(Grid.batchEditApi.GetCellValue(i, 'AllowCheck'));

                if (check == '1') {
                    x = true;
                }
            }
            if (x == false) {
                toastr.warning('Please select material number', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
            }
        }

        function DraftProcess() {
            //21-03-2019
            validation();

            startIndex = 0;

            var check = "";
            var x = false;

            for (var i = startIndex; i < startIndex + Grid.GetVisibleRowsOnPage(); i++) {
                check = parseFloat(Grid.batchEditApi.GetCellValue(i, "AllowCheck"));

                if (check == "1") {
                    x = true;
                }
            }

            if (x == true) {

                //            var msg = confirm('Are you sure want to update this data ?');
                //            if (msg == false) {
                //                e.processOnServer = false;
                //                return;
                //            }

                Grid.UpdateEdit();
                cbDraft.PerformCallback('draft|' + txtRFQNumber.GetText() + '|' + cboPRNumber.GetText() + '|' + txtRev.GetText() + '|' + txtRFQNo1.GetText() + '|' + txtRFQNo2.GetText() + '|' + txtRFQNo3.GetText() + '|' + txtRFQNo4.GetText() + '|' + txtRFQNo5.GetText());
            }
            else {
                toastr.warning('Please select material number', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
            }

        }

        //    function SelectionChanged(s, e) {
        //        s.GetSelectedFieldValues("Material_No", GetSelectedFieldValuesCallback);
        //    }

        //    function GetSelectedFieldValuesCallback(values) {
        //        txtSelected.SetText(values);
        //    }

        function CheckedChanged(s, e) {

            //Grid.SetFocusedRowIndex(-1);
            if (s.GetValue() == -1) s.SetValue(1);

            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "AllowCheck", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "AllowCheck", s.GetValue());
                }
            }
        }


        function FillComboPRNumber() {
            cboPRNumber.PerformCallback('all|00|00|00');
        }


        function GridLoad() {
                      
                       Grid.PerformCallback('load|' + cboPRNumber.GetText() + '|' + txtRFQNo1.GetText());
        }

        function addDays(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);
            return result;
        }

        function DateAdd() {
            var a = addDays(dtDRFQDate.GetDate(), 10);
            dtDatelineDate.SetValue(a);
        }

        function OnComboBoxKeyDown(s, e) {
            // 'Delete' button key code = 46

            if (e.htmlEvent.keyCode == 46 || e.htmlEvent.keyCode == 8)
                s.SetSelectedIndex(-1);
            //alert(s.GetValue());
        }

    </script>
    <style type="text/css">
        .hidden-div
        {
            display: none;
        }
        
        .tr-all-height
        {
            height: 30px;
        }
        
        .td-col-left
        {
            width: 80px;
            padding: 0px 0px 0px 10px;
        }
        
        .td-col-left2
        {
            width: 140px;
        }
        
        
        .td-col-mid
        {
            width: 10px;
        }
        .td-col-right
        {
            width: 200px;
        }
         .td-col-right2
        {
            width: 150px;
        }
        .td-col-free
        {
            width: 20px;
        }
        
        .div-pad-fil
        {
            padding: 5px 5px 5px 5px;
        }
        
        .td-margin
        {
            padding: 10px;
        }
        
        .td-left-mrg
        {
            margin-left:-100px;
        }
        
        .style1
        {
            width: 80px;
            padding: 0px 0px 0px 10px;
            height: 30px;
        }
        .style2
        {
            width: 10px;
            height: 30px;
        }
        .style3
        {
            width: 200px;
            height: 30px;
        }
        .style4
        {
            width: 20px;
            height: 30px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 1px solid black; height: 100px;">
                <tr style="height: 10px">
                    <td class="td-col-left">
                    </td>
                    <td class="td-col-mid">
                    </td> 
                    <td class="td-col-right" colspan="3">
                    </td>
                    <td class="td-col-free"></td>
                   
                    <td class="td-col-left">
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                    </td>
                    <td class="td-col-free">
                    </td>
                    <td class="td-col-right">
                    </td>
                    <td></td>
                   
                    
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="RFQ Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxTextBox ID="txtRFQNumber" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="200px" ClientInstanceName="txtRFQNumber" MaxLength="20" Height="25px"
                            Text="--NEW--" BackColor="#CCCCCC" ReadOnly="True" Font-Bold="True">
                        </dx1:ASPxTextBox>
                       
                       
                    </td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-free" >
                        <dx1:ASPxTextBox ID="txtRev" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="50px" ClientInstanceName="txtRev" MaxLength="20" Height="25px" BackColor="Silver"
                            ReadOnly="True" Font-Bold="True" ForeColor="Black" HorizontalAlign="Center" >
                        </dx1:ASPxTextBox>
                    </td>
                    <td class="td-col-free"></td>
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier 1">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxComboBox ID="cboSupplier1" runat="server" ClientInstanceName="cboSupplier1"
                            Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            IncrementalFilteringMode="Contains"  DropDownStyle="DropDownList"  Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px" />
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                            <ClientSideEvents KeyDown="OnComboBoxKeyDown" />
                        </dx1:ASPxComboBox>
                    </td>
                    <td class="td-col-mid">
                       
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxTextBox ID="txtRFQNo1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="180px" ClientInstanceName="txtRFQNo1" MaxLength="20" Height="25px" BackColor="#CCCCCC"
                            ReadOnly="True" Font-Bold="True">
                        </dx1:ASPxTextBox>
                    </td>
                     <td class="td-col-left"></td>
                </tr>
                <tr class="tr-all-height">
                    <td class="style1">
                        <dx1:ASPxLabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="PR Number">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="style2">
                        
                    </td>
                    <td class="style3">
                        <dx1:ASPxComboBox ID="cboPRNumber" runat="server" ClientInstanceName="cboPRNumber"
                            Width="170px" Font-Names="Segoe UI" TextField="Code" ValueField="Code" TextFormatString="{0}"
                            Font-Size="9pt" Theme="Office2010Black" IncrementalFilteringMode="Contains" DropDownStyle="DropDownList" Height="25px">
                            <ClientSideEvents SelectedIndexChanged="GridLoad" ValueChanged="function(s, e) {
	cbGrid.PerformCallback();
}" />
                            <ClientSideEvents SelectedIndexChanged="GridLoad" Init="FillComboPRNumber"></ClientSideEvents>
                            <Columns>
                                <dx:ListBoxColumn Caption="PR Number" FieldName="Code" Width="100px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                            <ClientSideEvents KeyDown="OnComboBoxKeyDown" />
                        </dx1:ASPxComboBox>
                    </td>
                    <td class="style2"></td>
                    <td class="style4"></td>
                    <td class="style4"></td>

                    <td class="style1">
                        <dx1:ASPxLabel ID="ASPxLabel8" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier 2">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="style2"></td>
                    <td class="style3">
                        <dx1:ASPxComboBox ID="cboSupplier2" runat="server" ClientInstanceName="cboSupplier2"
                            Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            IncrementalFilteringMode="Contains" DropDownStyle="DropDownList"
                            Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px" />
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                            <ClientSideEvents KeyDown="OnComboBoxKeyDown" />
                        </dx1:ASPxComboBox>
                    </td>
                    <td class="style2"> 
                        
                    </td>
                    <td class="style3">
                        <dx1:ASPxTextBox ID="txtRFQNo2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="180px" ClientInstanceName="txtRFQNo2" MaxLength="20" Height="25px" BackColor="#CCCCCC"
                            ReadOnly="True" Font-Bold="True">
                        </dx1:ASPxTextBox>
                    </td>
                     <td class="style1"></td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Department">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxTextBox ID="txtDepartment" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="200px" ClientInstanceName="txtDepartment" MaxLength="20" Height="25px" BackColor="#CCCCCC"
                             ReadOnly="True" >
                        </dx1:ASPxTextBox>
                    </td>
                    <td class="td-col-mid"></td>
                    
                    <td class="td-col-free">
                    </td>
                    <td class="td-col-free"></td>
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel9" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier 3">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxComboBox ID="cboSupplier3" runat="server" ClientInstanceName="cboSupplier3"
                            Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            IncrementalFilteringMode="Contains" DropDownStyle="DropDownList"
                            Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px" />
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                            <ClientSideEvents KeyDown="OnComboBoxKeyDown" />
                        </dx1:ASPxComboBox>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxTextBox ID="txtRFQNo3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="180px" ClientInstanceName="txtRFQNo3" MaxLength="20" Height="25px" BackColor="#CCCCCC"
                            ReadOnly="True" Font-Bold="True">
                        </dx1:ASPxTextBox>
                    </td>
                    <td class="td-col-left"></td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Section">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxTextBox ID="txtSection" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="200px" ClientInstanceName="txtSection" MaxLength="20" 
                            Height="25px" BackColor="#CCCCCC"
                             ReadOnly="True">
                        </dx1:ASPxTextBox>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-free"></td>
                    <td class="td-col-free"></td>
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel10" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier 4">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxComboBox ID="cboSupplier4" runat="server" ClientInstanceName="cboSupplier4"
                            Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            IncrementalFilteringMode="Contains" DropDownStyle="DropDownList"
                            Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px" />
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                            <ClientSideEvents KeyDown="OnComboBoxKeyDown" />
                        </dx1:ASPxComboBox>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxTextBox ID="txtRFQNo4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="180px" ClientInstanceName="txtRFQNo4" MaxLength="20" Height="25px" BackColor="#CCCCCC"
                            ReadOnly="True" Font-Bold="True">
                        </dx1:ASPxTextBox>
                    </td>
                     <td class="td-col-left"></td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="PR Type">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxTextBox ID="txtPRType" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="200px" ClientInstanceName="txtPRType" MaxLength="20" 
                            Height="25px" BackColor="#CCCCCC"
                              ReadOnly="True">
                        </dx1:ASPxTextBox>
                    </td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-free"></td>
                    <td class="td-col-free"></td>
                    
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel11" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Supplier 5">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxComboBox ID="cboSupplier5" runat="server" ClientInstanceName="cboSupplier5"
                            Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                            IncrementalFilteringMode="Contains" DropDownStyle="DropDownList" Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px" />
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                            <ClientSideEvents KeyDown="OnComboBoxKeyDown" />
                        </dx1:ASPxComboBox>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxTextBox ID="txtRFQNo5" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="180px" ClientInstanceName="txtRFQNo5" MaxLength="20" Height="25px" BackColor="#CCCCCC"
                            ReadOnly="True" Font-Bold="True">
                        </dx1:ASPxTextBox>
                    </td>
                     <td class="td-col-left"></td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel12" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="RFQ Date">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx:ASPxDateEdit ID="dtDRFQDate" runat="server" Theme="Office2010Black" Width="100px"
                            ClientInstanceName="dtDRFQDate" EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                            Font-Names="Segoe UI" Enabled="false" Font-Size="9pt" Height="25px" EditFormat="Custom"
                            ReadOnly="True">
                            <CalendarProperties>
                                <HeaderStyle Font-Size="12pt" Paddings-Padding="5px">
                                    <Paddings Padding="5px"></Paddings>
                                </HeaderStyle>
                                <DayStyle Font-Size="9pt" Paddings-Padding="5px">
                                    <Paddings Padding="5px"></Paddings>
                                </DayStyle>
                                <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                    <Paddings Padding="5px"></Paddings>
                                </WeekNumberStyle>
                                <FooterStyle Font-Size="9pt" Paddings-Padding="10px">
                                    <Paddings Padding="10px"></Paddings>
                                </FooterStyle>
                                <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                    <Paddings Padding="10px"></Paddings>
                                </ButtonStyle>
                            </CalendarProperties>
                            <ClientSideEvents ValueChanged="DateAdd" />
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxDateEdit>
                    </td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-free"></td>
                    <td class="td-col-free"></td>
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel13" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Deadline Date">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                        <dx:ASPxDateEdit ID="dtDatelineDate" runat="server" Theme="Office2010Black" Width="100px"
                            AutoPostBack="false" ClientInstanceName="dtDatelineDate" EditFormatString="dd-MMM-yyyy"
                            DisplayFormatString="dd-MMM-yyyy" Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                            <CalendarProperties>
                                <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                                <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                                <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                                </WeekNumberStyle>
                                <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                                <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                                </ButtonStyle>
                            </CalendarProperties>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                            </ButtonStyle>
                        </dx:ASPxDateEdit>
                    </td>
                    <td class="td-col-mid">
                    </td>
                    <td class="td-col-right">
                       
                        <dx:ASPxCallback ID="cbGrid" runat="server" ClientInstanceName="cbGrid">
                         <ClientSideEvents EndCallback="GetMessage" />
                                                        
                </dx:ASPxCallback>

                    </td>
                     <td class="td-col-left"></td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="RFQ Title">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid"> 
                        &nbsp;
                    </td>
                    <td class="td-col-right" colspan="3">
                        <dx1:ASPxTextBox ID="txtRFQTitle" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Width="260px" ClientInstanceName="txtRFQTitle" MaxLength="40" Height="25px" AutoCompleteType="Disabled">
                        </dx1:ASPxTextBox>
                    </td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-left">
                        <dx1:ASPxLabel ID="ASPxLabel14" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                            Text="Currency">
                        </dx1:ASPxLabel>
                    </td>
                    <td class="td-col-mid">
                        
                    </td>
                    <td class="td-col-right">
                        <dx1:ASPxComboBox ID="cboCurrency" runat="server" ClientInstanceName="cboCurrency"
                            Width="80px" Font-Names="Segoe UI" TextField="Description" DataSourceID="dsCurrency"
                            ValueField="Code" TextFormatString="{0}" Font-Size="9pt" Theme="Office2010Black"
                            IncrementalFilteringMode="Contains" DropDownStyle="DropDownList"
                            Height="25px">
                            <Columns>
                                <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="60px" />
                            </Columns>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
                    </td>
                    <td class="td-col-mid">
                        
                    </td>
                    <td class="td-col-right">
                        <dx:ASPxCallback ID="cbReject" runat="server" ClientInstanceName="cbReject">
                            <ClientSideEvents EndCallback="GetMessage" />
                        </dx:ASPxCallback>
                    </td>
                     <td class="td-col-left"></td>
                </tr>
                <tr class="tr-all-height">
                    <td class="td-col-left"></td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-right">
                        <dx:ASPxCallback ID="cbPrint" runat="server" ClientInstanceName="cbPrint">
                            <ClientSideEvents EndCallback="GetMessage" />
                        </dx:ASPxCallback>
                    </td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-free"></td>
                    <td class="td-col-free"></td>
                    <td class="td-col-left"></td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-right">
                        <dx:ASPxCallback ID="cbDate" runat="server" ClientInstanceName="cbDate">
                            <ClientSideEvents EndCallback="GetMessage" />
                        </dx:ASPxCallback>
                    </td>
                    <td class="td-col-mid"></td>
                    <td class="td-col-right">
                        <dx:ASPxCallback ID="cbDraft" runat="server" ClientInstanceName="cbDraft">
                            <ClientSideEvents EndCallback="GetMessage" />
                        </dx:ASPxCallback>
                    </td>
                     <td class="td-col-left"></td>
                </tr>
            </table>
        </div>
        <div class="hidden-div">
            <dx1:ASPxTextBox ID="txtSelected" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                Width="100px" ClientInstanceName="txtSelected" MaxLength="30" Height="25px" Visible="False">
            </dx1:ASPxTextBox>
            <dx:ASPxDateEdit ID="dtDateToday" runat="server" Theme="Office2010Black" Width="100px"
                AutoPostBack="false" ClientInstanceName="dtDateToday" EditFormatString="dd-MMM-yyyy"
                DisplayFormatString="dd-MMM-yyyy" Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                <CalendarProperties>
                    <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                    <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                    <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px">
                    </WeekNumberStyle>
                    <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                    <ButtonStyle Font-Size="9pt" Paddings-Padding="10px">
                    </ButtonStyle>
                </CalendarProperties>
                <ButtonStyle Width="5px" Paddings-Padding="4px">
                </ButtonStyle>
            </dx:ASPxDateEdit>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="Select '00' Code, 'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Department'">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="Select '00' Code, 'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Section'">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="Select '00' Code, 'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRType'">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="Select Supplier_Code As Code, Supplier_Name As Description From Mst_Supplier">
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="dsCurrency" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="SELECT Code = RTRIM(Par_Code) FROM Mst_Parameter WHERE Par_Group = 'Currency'">
            </asp:SqlDataSource>
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
                Font-Size="9pt" KeyFieldName="PR_Number;Material_No">
                <ClientSideEvents BatchEditStartEditing="OnBatchEditStartEditing" EndCallback="function(s, e) {
	cbGrid.PerformCallback();
}" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Material No" Name="MaterialNo" VisibleIndex="1"
                        FieldName="Material_No" Width="150px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="2" Width="300px" FieldName="Description">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Specification" VisibleIndex="3" Width="450px"
                        FieldName="Specification">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Qty" VisibleIndex="4" FieldName="Qty" Width="80px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="UOM" VisibleIndex="5" Width="80px" FieldName="UOM_Description">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataCheckColumn Name="AllowCheck" VisibleIndex="0" Width="35px" FieldName="AllowCheck">
                        <PropertiesCheckEdit ValueChecked="1" ValueType="System.String" AllowGrayedByClick="false"
                            ValueUnchecked="0">
                        </PropertiesCheckEdit>
                        <HeaderCaptionTemplate>
                            <dx:ASPxCheckBox ID="checkBox" runat="server" ClientInstanceName="checkBox" ClientSideEvents-CheckedChanged="CheckedChanged"
                                ValueType="System.String" ValueChecked="1" ValueUnchecked="0" Text="" Font-Names="Segoe UI"
                                Font-Size="8pt" ForeColor="White">
                            </dx:ASPxCheckBox>
                        </HeaderCaptionTemplate>
                    </dx:GridViewDataCheckColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="True" AllowSort="False" ColumnResizeMode="Control"
                    EnableRowHotTrack="True" />
                <SettingsPager Mode="ShowAllRecords" NumericButtonCount="10">
                </SettingsPager>
                <SettingsEditing Mode="Batch" NewItemRowPosition="Bottom">
                    <BatchEditSettings ShowConfirmOnLosingChanges="False" />
                </SettingsEditing>
                <Settings HorizontalScrollBarMode="Visible" ShowStatusBar="Hidden" ShowVerticalScrollBar="True"
                    VerticalScrollableHeight="160" VerticalScrollBarMode="Visible" />
                <Styles>
                    <Header HorizontalAlign="Center">
                        <Paddings PaddingBottom="5px" PaddingTop="5px" />
                    </Header>
                </Styles>
                <StylesEditors ButtonEditCellSpacing="0">
                    <ProgressBar Height="21px">
                    </ProgressBar>
                </StylesEditors>
            </dx:ASPxGridView>
        </div>
        <div style="padding: 5px 5px 5px 5px">
            <table style="width: 100%; border: 0px">
                <tr style="height: 10px">
                    <td style="padding: 0px 0px 0px 10px; height: 40px">
                        <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnBack" Theme="Default">
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;<dx:ASPxButton ID="btnDraft" runat="server" Text="Draft" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnDraft" Theme="Default">
                            <ClientSideEvents Click="DraftProcess" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;<dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnSubmit" Theme="Default">
                            <ClientSideEvents Init="function(s, e) {
	btnSubmit.SetEnabled(false);
}" Click="function(s, e) {
		SubmitProcess();
		var msg = confirm('Are you sure want to submit this data ?');
        if (msg == false) {
            e.processOnServer = false;
            return;
        }						
		
	    //update grid if exist change value (check/uncheck) 
        Grid.UpdateEdit();
        cbDraft.PerformCallback('submit' + '|' + txtRFQNumber.GetText() + '|' + cboPRNumber.GetText() + '|' + txtRev.GetText() + '|' + txtRFQNo1.GetText() + '|' + txtRFQNo2.GetText() + '|' + txtRFQNo3.GetText() + '|' + txtRFQNo4.GetText() + '|' + txtRFQNo5.GetText());
}" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                        &nbsp;<dx:ASPxButton ID="btnPrint" runat="server" Text="Print" UseSubmitBehavior="False"
                            Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                            ClientInstanceName="btnPrint" Theme="Default">
                            <ClientSideEvents Click="function(s, e) {
	                        cbPrint.PerformCallback(txtRFQNumber.GetText() + '|' + cboPRNumber.GetText() + '|' + txtRev.GetText());

                             var pathArray = window.location.pathname.split('/');
                             var url = window.location.origin + '/' + pathArray[1] + '/ViewRFQ.aspx';

                             //alert(pathArray[1]);
                            if (pathArray[1] == 'RFQCreate.aspx') {
                                window.location.href = window.location.origin + '/ViewRFQ.aspx';
                            }
                            else {
                                window.location.href = url;
                            }

                            }" />
                            <Paddings Padding="2px" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

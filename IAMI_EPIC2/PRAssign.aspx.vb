﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class PRAssign
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fcategroy As String
    Dim fprtype As String

    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_GridLoad(pDateFrom As String, pDateTo As String, pPRType As String, pDepartment As String, pSection As String, pStatus As String)
        Dim ErrMsg As String = ""

        Dim ds As New DataSet
        ds = clsPRAssignDB.GetList(pDateFrom, pDateTo, pPRType, pDepartment, pSection, pStatus, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_FillCombo()
        Dim ds As New DataSet
        Dim pErr As String = ""

        ds = clsPRAcceptanceDB.GetComboData("PRType", pErr)
        If pErr = "" Then
            cboPRType.Items.Clear()
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboPRType.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("Department", pErr)
        If pErr = "" Then
            cboDepartment.Items.Clear()
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboDepartment.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

        ds = clsPRAcceptanceDB.GetComboData("Section", pErr)
        If pErr = "" Then
            cboSection.Items.Clear()
            For i = 0 To ds.Tables(0).Rows.Count - 1
                cboSection.Items.Add(Trim(ds.Tables(0).Rows(i)("Code") & ""))
            Next
        End If

    End Sub

    Private Sub up_ExcelGridAuto(Optional ByRef pErr As String = "")

        Try

            up_GridLoad(gs_DateFrom, gs_DateTo, gs_PRTypeCode, gs_DepartmentCode, gs_SectionCode, gs_StatusAssign)

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=PRAssign" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            pErr = ex.Message
        End Try

    End Sub

    Private Sub up_Excel(Optional ByRef pErr As String = "")
        Try
            Dim fi As New FileInfo(Server.MapPath("~\Download\PRAcceptance.xlsx"))
            If fi.Exists Then
                fi.Delete()
                fi = New FileInfo(Server.MapPath("~\Download\PRAcceptance.xlsx"))
            End If
            Dim exl As New ExcelPackage(fi)
            Dim ws As ExcelWorksheet
            ws = exl.Workbook.Worksheets.Add("Sheet1")
            ws.View.ShowGridLines = False

            With ws
                'Background Header Table
                Dim rgHeader As ExcelRange = .Cells(9, 1, 10, 17)
                rgHeader.Style.Fill.PatternType = Style.ExcelFillStyle.Solid
                rgHeader.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.DimGray)
                rgHeader.Style.Fill.PatternType = Style.ExcelFillStyle.Solid


                .Cells(1, 1, 1, 1).Value = "IAMI EPIC - Purchase Request Acceptance"
                .Cells(1, 1, 1, 1).Style.Font.Size = 14
                .Cells(1, 1, 1, 1).Style.Font.Bold = True

                .Cells(3, 1, 3, 1).Value = "Request Date"
                .Cells(3, 2, 3, 2).Value = ": " & Format(dtDateFrom.Value, "dd MMM yyyy") & " - " & Format(dtDateTo.Value, "dd MMM yyyy")

                .Cells(4, 1, 4, 1).Value = "PR Type"
                .Cells(4, 2, 4, 2).Value = ": " & cboPRType.Text

                '.Cells(5, 1, 5, 1).Value = "Approval Status"
                '.Cells(5, 2, 5, 2).Value = ": " & cboApproval.Text

                .Cells(6, 1, 6, 1).Value = "Department"
                .Cells(6, 2, 6, 2).Value = ": " & cboDepartment.Text

                .Cells(7, 1, 7, 1).Value = "Section"
                .Cells(7, 2, 7, 2).Value = ": " & cboSection.Text


                .Cells(9, 1, 9, 1).Value = "PR Number"
                .Cells(9, 1, 10, 1).Merge = True
                .Cells(9, 1, 9, 1).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 1, 9, 1).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 1, 9, 1).Style.Font.Size = 9
                .Cells(9, 1, 9, 1).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 1, 9, 1).Style.Font.Color.SetColor(Color.White)

                .Cells(9, 2, 9, 2).Value = "PR Date"
                .Cells(9, 2, 10, 2).Merge = True
                .Cells(9, 2, 9, 2).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 2, 9, 2).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 2, 9, 2).Style.Font.Size = 9
                .Cells(9, 2, 9, 2).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 2, 9, 2).Style.Font.Color.SetColor(Color.White)

                .Cells(9, 3, 9, 3).Value = "PR Budget"
                .Cells(9, 3, 10, 3).Merge = True
                .Cells(9, 3, 9, 3).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 3, 9, 3).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 3, 9, 3).Style.Font.Size = 9
                .Cells(9, 3, 9, 3).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 3, 9, 3).Style.Font.Color.SetColor(Color.White)

                .Cells(9, 4, 9, 4).Value = "PR Type"
                .Cells(9, 4, 10, 4).Merge = True
                .Cells(9, 4, 9, 4).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 4, 9, 4).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 4, 9, 4).Style.Font.Size = 9
                .Cells(9, 4, 9, 4).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 4, 9, 4).Style.Font.Color.SetColor(Color.White)

                .Cells(9, 5, 9, 5).Value = "Department"
                .Cells(9, 5, 10, 5).Merge = True
                .Cells(9, 5, 9, 5).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 5, 9, 5).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 5, 9, 5).Style.Font.Size = 9
                .Cells(9, 5, 9, 5).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 5, 9, 5).Style.Font.Color.SetColor(Color.White)

                .Cells(9, 6, 9, 6).Value = "Section"
                .Cells(9, 6, 10, 6).Merge = True
                .Cells(9, 6, 9, 6).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 6, 9, 6).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 6, 9, 6).Style.Font.Size = 9
                .Cells(9, 6, 9, 6).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 6, 9, 6).Style.Font.Color.SetColor(Color.White)

                .Cells(9, 7, 9, 7).Value = "Project"
                .Cells(9, 7, 10, 7).Merge = True
                .Cells(9, 7, 9, 7).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 7, 9, 7).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 7, 9, 7).Style.Font.Size = 9
                .Cells(9, 7, 9, 7).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 7, 9, 7).Style.Font.Color.SetColor(Color.White)

                .Cells(9, 8, 9, 8).Value = "Urgent Status"
                .Cells(9, 8, 10, 8).Merge = True
                .Cells(9, 8, 9, 8).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 8, 9, 8).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 8, 9, 8).Style.Font.Size = 9
                .Cells(9, 8, 9, 8).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 8, 9, 8).Style.Font.Color.SetColor(Color.White)

                .Cells(9, 9, 9, 9).Value = "Dept Head"
                .Cells(10, 9, 10, 9).Value = "Approval Name"
                .Cells(10, 10, 10, 10).Value = ""
                .Cells(10, 11, 10, 11).Value = "Approval Date"

                .Cells(9, 9, 9, 9).Value = "Dept Head"
                .Cells(9, 9, 9, 11).Merge = True
                .Cells(9, 9, 9, 9).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 9, 9, 9).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 9, 9, 9).Style.Font.Size = 9
                .Cells(9, 9, 9, 9).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 9, 10, 11).Style.Font.Color.SetColor(Color.White)


                .Cells(9, 12, 9, 12).Value = "General Manager"
                .Cells(10, 12, 10, 12).Value = "Approval Name"
                .Cells(10, 13, 10, 13).Value = ""
                .Cells(10, 14, 10, 14).Value = "Approval Date"

                .Cells(9, 12, 9, 12).Value = "General Manager"
                .Cells(9, 12, 9, 14).Merge = True
                .Cells(9, 12, 9, 12).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 12, 9, 12).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 12, 9, 12).Style.Font.Size = 9
                .Cells(9, 12, 9, 12).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 12, 10, 14).Style.Font.Color.SetColor(Color.White)


                .Cells(9, 15, 9, 15).Value = "Acceptance"
                .Cells(10, 15, 10, 15).Value = "Approval Name"
                .Cells(10, 16, 10, 16).Value = ""
                .Cells(10, 17, 10, 17).Value = "Approval Date"

                .Cells(9, 15, 9, 15).Value = "Acceptance"
                .Cells(9, 15, 9, 17).Merge = True
                .Cells(9, 15, 9, 15).Style.HorizontalAlignment = HorzAlignment.Center
                .Cells(9, 15, 9, 15).Style.VerticalAlignment = VertAlignment.Bottom
                .Cells(9, 15, 9, 15).Style.Font.Size = 9
                .Cells(9, 15, 9, 15).Style.Font.Name = "Arial Unicode MS"
                .Cells(9, 15, 10, 17).Style.Font.Color.SetColor(Color.White)

                Dim ds As DataSet
                Dim dtFrom, dtTo As String
                Dim vPRType As String = ""
                Dim vDepartment As String = ""
                Dim vSection As String = ""
                Dim vApproval As String = ""
                dtFrom = Format(dtDateFrom.Value, "yyyy-MM-dd")
                dtTo = Format(dtDateTo.Value, "yyyy-MM-dd")

                ds = clsPRAcceptanceDB.GetDataForExcel(dtFrom, dtTo, vPRType, vApproval, vDepartment, vSection, pErr)

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    .Cells(i + 11, 1, i + 11, 1).Value = ds.Tables(0).Rows(i)("PR_Number")
                    .Cells(i + 11, 2, i + 11, 2).Value = ds.Tables(0).Rows(i)("PR_Date")
                    .Cells(i + 11, 3, i + 11, 3).Value = ds.Tables(0).Rows(i)("PRBudgetName")
                    .Cells(i + 11, 4, i + 11, 4).Value = ds.Tables(0).Rows(i)("PRTypeName")
                    .Cells(i + 11, 5, i + 11, 5).Value = ds.Tables(0).Rows(i)("DepartmentName")
                    .Cells(i + 11, 6, i + 11, 6).Value = ds.Tables(0).Rows(i)("SectionName")
                    .Cells(i + 11, 7, i + 11, 7).Value = ds.Tables(0).Rows(i)("Project")
                    .Cells(i + 11, 8, i + 11, 8).Value = ds.Tables(0).Rows(i)("Urgent_Status")

                    .Cells(i + 11, 9, i + 11, 9).Value = ds.Tables(0).Rows(i)("Dept_ApprovedBy")
                    .Cells(i + 11, 10, i + 11, 10).Value = ds.Tables(0).Rows(i)("Dept_ApprovedStatus")
                    .Cells(i + 11, 11, i + 11, 11).Value = ds.Tables(0).Rows(i)("Dept_ApprovedDate")

                    .Cells(i + 11, 12, i + 11, 12).Value = ds.Tables(0).Rows(i)("GM_ApprovedBy")
                    .Cells(i + 11, 13, i + 11, 13).Value = ds.Tables(0).Rows(i)("GM_ApprovedStatus")
                    .Cells(i + 11, 14, i + 11, 14).Value = ds.Tables(0).Rows(i)("GM_ApprovedDate")

                    .Cells(i + 11, 15, i + 11, 15).Value = ds.Tables(0).Rows(i)("Acceptance_ApprovedBy")
                    .Cells(i + 11, 16, i + 11, 16).Value = ds.Tables(0).Rows(i)("Acceptance_ApprovedStatus")
                    .Cells(i + 11, 17, i + 11, 17).Value = ds.Tables(0).Rows(i)("Acceptance_ApprovedDate")
                Next

                'Set Column
                .Column(1).Width = 15
                .Column(2).Width = 15
                .Column(3).Width = 15
                .Column(4).Width = 15
                .Column(5).Width = 15
                .Column(6).Width = 15
                .Column(7).Width = 15
                .Column(8).Width = 15
                .Column(9).Width = 15

                .Column(10).Width = 15
                .Column(11).Width = 15
                .Column(12).Width = 15
                .Column(13).Width = 15
                .Column(14).Width = 15
                .Column(15).Width = 15
                .Column(16).Width = 15
                .Column(17).Width = 15
                'Set Font Size
                .Cells(2, 1, 11 + ds.Tables(0).Rows.Count, 17).Style.Font.Size = 9
                'Set Font Name 
                .Cells(2, 1, 11 + ds.Tables(0).Rows.Count, 17).Style.Font.Name = "Arial Unicode MS"

                .Cells(9, 1, 11 + ds.Tables(0).Rows.Count - 1, 17).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                .Cells(9, 1, 11 + ds.Tables(0).Rows.Count - 1, 17).Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                .Cells(9, 1, 11 + ds.Tables(0).Rows.Count - 1, 17).Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
                .Cells(9, 1, 11 + ds.Tables(0).Rows.Count - 1, 17).Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
                .Cells(9, 1, 11 + ds.Tables(0).Rows.Count - 1, 17).Style.Border.Top.Style = Style.ExcelBorderStyle.Thin

            End With

            exl.Save()
            DevExpress.Web.ASPxClasses.ASPxWebControl.RedirectOnCallback("Download/" & fi.Name)
        Catch ex As Exception
            'show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            pErr = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("B040")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "B040")
        If (Not Page.IsPostBack) Then

            If gs_Back = False Then
                Dim dfrom As Date
                dfrom = Year(Now) & "-" & Month(Now) & "-01"
                dtDateFrom.Value = dfrom
                dtDateTo.Value = Now

                up_FillCombo()

                If cboPRType.Items.Count > 0 Then
                    cboPRType.SelectedIndex = 0
                End If

                If cboDepartment.Items.Count > 0 Then
                    cboDepartment.SelectedIndex = 0
                End If

                If cboSection.Items.Count > 0 Then
                    cboSection.SelectedIndex = 0
                End If
                cboStatus.SelectedIndex = 0
            Else
                dtDateFrom.Value = gs_dtFromBack
                dtDateTo.Value = gs_dtToBack
                cboPRType.SelectedIndex = gs_PRType
                cboDepartment.SelectedIndex = gs_Department
                cboSection.SelectedIndex = gs_Section

                up_GridLoad(gs_DateFrom, gs_DateTo, gs_PRTypeCode, gs_DepartmentCode, gs_SectionCode, gs_StatusAssign)
                gs_Back = False
                cboStatus.SelectedIndex = 0
            End If
           
        End If
        'cbExcel.JSProperties("cpmessage") = ""
    End Sub

    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs)
        up_GridLoad(gs_DateFrom, gs_DateTo, gs_PRTypeCode, gs_DepartmentCode, gs_SectionCode, gs_StatusAssign)
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pDateFrom As Date = Split(e.Parameters, "|")(1)
        Dim pDateTo As Date = Split(e.Parameters, "|")(2)
        Dim pPRType As String = Split(e.Parameters, "|")(3)
        'Dim pApproval As String = Split(e.Parameters, "|")(4)
        Dim pDepartment As String = Split(e.Parameters, "|")(4)
        Dim pSection As String = Split(e.Parameters, "|")(5)
        Dim pStatus As String = Split(e.Parameters, "|")(6)

        Dim vDateFrom As String = Format(pDateFrom, "yyyy-MM-dd")
        Dim vDateTo As String = Format(pDateTo, "yyyy-MM-dd")

        gs_DateFrom = vDateFrom
        gs_DateTo = vDateTo
        gs_PRTypeCode = pPRType
        gs_DepartmentCode = pDepartment
        gs_SectionCode = pSection
        'gs_ApprovalCode = pApproval

        gs_dtFromBack = pDateFrom
        gs_dtToBack = pDateTo

        gs_PRType = cboPRType.SelectedIndex
        gs_Department = cboDepartment.SelectedIndex
        gs_Section = cboSection.SelectedIndex
        gs_StatusAssign = cboStatus.Text
        'gs_Approval = cboApproval.SelectedIndex

        If pFunction = "gridload" Then
            up_GridLoad(vDateFrom, vDateTo, pPRType, pDepartment, pSection, pStatus)
        End If
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim ErrMsg As String = ""
        'up_Excel(ErrMsg)
        up_ExcelGridAuto(ErrMsg)

        'If ErrMsg <> "" Then
        '    cbMessage.JSProperties("cpMessage") = ErrMsg
        'Else
        'cbMessage.JSProperties("cpMessage") = "Download Excel Successfully"
        'End If
    End Sub
End Class
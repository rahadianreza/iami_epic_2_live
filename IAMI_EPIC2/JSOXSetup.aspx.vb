﻿Imports IAMIEngine

Public Class JSOXSetup
    Inherits System.Web.UI.Page
    Dim pUser As String
    Dim menuId As String = "Z030"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu(menuId)
        Master.SiteTitle = sGlobal.menuName
        show_error(MsgTypeEnum.Info, "", 0)
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            Dim cJSOXSetup As New clsJSOXSetup            
            Dim JsoxList As List(Of clsJSOXSetup) = clsJSOXSetupDB.GetList(cJSOXSetup)
            txt1.Text = JsoxList(0).ParamValue
            txt2.Text = JsoxList(1).ParamValue
            txt3.Text = JsoxList(2).ParamValue
            txt4.Text = JsoxList(3).ParamValue
            txt5.Text = JsoxList(4).ParamValue
            txt8.Text = JsoxList(7).ParamValue
            txt10.Text = JsoxList(9).ParamValue

            chk1.Checked = JsoxList(0).Enable
            chk2.Checked = JsoxList(1).Enable
            chk3.Checked = JsoxList(2).Enable
            chk4.Checked = JsoxList(3).Enable
            chk5.Checked = JsoxList(4).Enable
            chk6.Checked = JsoxList(5).Enable
            chk7.Checked = JsoxList(6).Enable
            chk8.Checked = JsoxList(7).Enable
            chk9.Checked = JsoxList(8).Enable
            chk10.Checked = JsoxList(9).Enable
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, Optional ByVal pVal As Integer = 1)
        cbProgress.JSProperties("cp_message") = ErrMsg
        cbProgress.JSProperties("cp_type") = msgType
        cbProgress.JSProperties("cp_val") = pVal
    End Sub

    Private Sub cbProgress_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbProgress.Callback
        Dim jsox As New clsJSOXSetup

        jsox.RuleID = 1
        jsox.Enable = chk1.Checked
        jsox.ParamValue = Val(txt1.Value)
        jsox.UpdateUser = pUser
        clsJSOXSetupDB.Update(jsox)

        jsox.RuleID = 2
        jsox.Enable = chk2.Checked
        jsox.ParamValue = Val(txt2.Value)
        jsox.UpdateUser = pUser
        clsJSOXSetupDB.Update(jsox)

        jsox.RuleID = 3
        jsox.Enable = chk3.Checked
        jsox.ParamValue = Val(txt3.Value)
        jsox.UpdateUser = pUser
        clsJSOXSetupDB.Update(jsox)

        jsox.RuleID = 4
        jsox.Enable = chk4.Checked
        jsox.ParamValue = Val(txt4.Value)
        jsox.UpdateUser = pUser
        clsJSOXSetupDB.Update(jsox)

        jsox.RuleID = 5
        jsox.Enable = chk5.Checked
        jsox.ParamValue = Val(txt5.Value)
        jsox.UpdateUser = pUser
        clsJSOXSetupDB.Update(jsox)

        jsox.RuleID = 6
        jsox.Enable = chk6.Checked
        jsox.ParamValue = 0
        jsox.UpdateUser = pUser
        clsJSOXSetupDB.Update(jsox)

        jsox.RuleID = 7
        jsox.Enable = chk7.Checked
        jsox.ParamValue = 0
        jsox.UpdateUser = pUser
        clsJSOXSetupDB.Update(jsox)

        jsox.RuleID = 8
        jsox.Enable = chk8.Checked
        jsox.ParamValue = Val(txt8.Value)
        jsox.UpdateUser = pUser
        clsJSOXSetupDB.Update(jsox)

        jsox.RuleID = 9
        jsox.Enable = chk9.Checked
        jsox.ParamValue = 0
        jsox.UpdateUser = pUser
        clsJSOXSetupDB.Update(jsox)

        jsox.RuleID = 10
        jsox.Enable = chk10.Checked
        jsox.ParamValue = Val(txt10.Value)
        jsox.UpdateUser = pUser
        clsJSOXSetupDB.Update(jsox)

        show_error(MsgTypeEnum.Success, "Update data successful.")
    End Sub
End Class
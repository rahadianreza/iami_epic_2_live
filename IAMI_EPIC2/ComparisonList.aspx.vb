﻿Imports DevExpress.Web.ASPxGridView
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Drawing

Public Class ComparisonList
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fRefresh As Boolean = False
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("L010")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            FillCombo("", "", "", "", 1)
            FillCombo("All", "", "", "", 2)
            FillCombo("All", "All", "", "", 3)
            FillCombo("All", "All", "All", "", 4)
            FillCombo("All", "All", "All", "All", 5)
            cboProject.SelectedIndex = 0
            cboGroup.SelectedIndex = 0
            cboCommodity.SelectedIndex = 0
            cboCommodityGroup.SelectedIndex = 0
            cboPartNo.SelectedIndex = 0
        End If
    End Sub
#End Region

#Region "Control Event"
    Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)

        ds = clsComparisonDB.getProjectID(Project, "", "", "", 2, pUser)
        If pmsg = "" Then
            cboGroup.DataSource = ds
            cboGroup.DataBind()
        End If
    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)
        Dim Group As String = Split(e.Parameter, "|")(1)

        ds = clsComparisonDB.getProjectID(Project, Group, "", "", 3, pUser)
        If pmsg = "" Then
            cboCommodity.DataSource = ds
            cboCommodity.DataBind()
        End If
    End Sub
    Private Sub cboCommodityGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodityGroup.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)
        Dim Group As String = Split(e.Parameter, "|")(1)
        Dim Commodity As String = Split(e.Parameter, "|")(2)

        ds = clsComparisonDB.getProjectID(Project, Group, Commodity, "", 4, pUser)
        If pmsg = "" Then
            cboCommodityGroup.DataSource = ds
            cboCommodityGroup.DataBind()
        End If
    End Sub
    Private Sub cboPartNo_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboPartNo.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)
        Dim Group As String = Split(e.Parameter, "|")(1)
        Dim Commodity As String = Split(e.Parameter, "|")(2)
        Dim CommodityGroup As String = Split(e.Parameter, "|")(3)

        ds = clsComparisonDB.getProjectID(Project, Group, Commodity, CommodityGroup, 5, pUser)
        If pmsg = "" Then
            cboPartNo.DataSource = ds
            cboPartNo.DataBind()
        End If
    End Sub


    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
        End If

    End Sub


    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "load"
                up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)

        End Select
    End Sub

    Private Sub up_Excel()
        up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)

        Dim ps As New PrintingSystem()

        Dim link1 As New PrintableComponentLink(ps)
        link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ComparisonList" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Procedure"
    Private Sub FillCombo(ByVal proj As String, ByVal group As String, ByVal commodity As String, ByVal commodityGroup As String, pstatus As Integer)
        Dim ds As New DataSet
        Dim pmsg As String = ""

        If pstatus = 1 Then
            ds = clsComparisonDB.getProjectID(proj, "", "", "", 1, pUser)
            If pmsg = "" Then
                cboProject.DataSource = ds
                cboProject.DataBind()
            End If
        End If

        If pstatus = 2 Then
            ds = clsComparisonDB.getProjectID(proj, "", "", "", 2, pUser)
            If pmsg = "" Then
                cboGroup.DataSource = ds
                cboGroup.DataBind()
            End If
        End If

        If pstatus = 3 Then
            ds = clsComparisonDB.getProjectID(proj, group, "", "", 3, pUser)
            If pmsg = "" Then
                cboCommodity.DataSource = ds
                cboCommodity.DataBind()
            End If
        End If

        If pstatus = 4 Then
            ds = clsComparisonDB.getProjectID(proj, group, commodity, "", 4, pUser)
            If pmsg = "" Then
                cboCommodityGroup.DataSource = ds
                cboCommodityGroup.DataBind()
            End If
        End If
        If pstatus = 5 Then
            ds = clsComparisonDB.getProjectID(proj, group, commodity, commodityGroup, 5, pUser)
            If pmsg = "" Then
                cboPartNo.DataSource = ds
                cboPartNo.DataBind()
            End If
        End If


    End Sub

    Private Sub up_GridLoad(ByVal pProjectID As String, ByVal pGroup As String, ByVal pComodity As String, ByVal pGroupComodity As String, ByVal pPartNo As String)
        Dim ErrMsg As String = ""
        Dim Ses As New DataSet
        Ses = clsComparisonDB.getlistds(pProjectID, pGroup, pComodity, pGroupComodity, pPartNo, pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub

#End Region






End Class
﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewSupplierRecommendation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        up_LoadReport()
    End Sub

    Private Sub up_LoadReport()
        Dim sql As String
        Dim ds As New DataSet
        Dim Report2 As New rptSupplierRecommendationRow2
        Dim Report3 As New rptSupplierRecommendationRow3
        Dim Report4 As New rptSupplierRecommendationRow4
        Dim Report5 As New rptSupplierRecommendationRow5
        Dim pSRNumber As String
        Dim pAction As String
        Dim pRev As Integer
        Dim pCPNumber As String
        Dim pJumlah As String
        pAction = Session("Action")
        pSRNumber = Session("SRNumber")
        pRev = Session("Revision")
        pCPNumber = Session("CPNumber")
        pJumlah = Session("Jumlah")

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_SR_ReportSupplierRecommendation '" & pSRNumber & "' , " & pRev & ""

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)

                If pJumlah = "2" Then
                    Report2.DataSource = ds
                    Report2.Name = "SupplierRecommend_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                    ASPxDocumentViewer1.Report = Report2
                ElseIf pJumlah = "3" Then
                    Report3.DataSource = ds
                    Report3.Name = "SupplierRecommend_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                    ASPxDocumentViewer1.Report = Report3
                ElseIf pJumlah = "4" Then
                    Report4.DataSource = ds
                    Report4.Name = "SupplierRecommend_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                    ASPxDocumentViewer1.Report = Report4
                Else
                    Report5.DataSource = ds
                    Report5.Name = "SupplierRecommend_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                    ASPxDocumentViewer1.Report = Report5
                End If
               

                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim pSRNumber As String
        Dim pRev As Integer
        Dim pCPNumber As String
        Dim pAction As String
        Dim pJumlah As String


        pAction = Session("Action")
        pSRNumber = Session("SRNumber")
        pRev = Session("Revision")
        pCPNumber = Session("CPNumber")
        pJumlah = Session("Jumlah")
        If pAction = "1" Then
            Response.Redirect("~/SupplierRecomendListDetail.aspx?ID=" & pSRNumber & "|" & pRev & "|" & pCPNumber)
        Else
            Response.Redirect("~/SupplierRecomendApprovalDetail.aspx?ID=" & pSRNumber & "|" & pRev & "|" & pCPNumber)

        End If
    End Sub

End Class
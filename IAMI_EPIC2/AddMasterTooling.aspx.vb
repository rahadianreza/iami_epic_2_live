﻿Public Class AddMasterTooling
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
    Dim CategoryMaterial As String = ""
    Dim MaterialCode As String = ""
    Dim period As String = ""
    Dim type As String = ""
    Dim toolingid As String = ""
    Dim partno As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A100")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        Dim s As String = Request.QueryString("ID")
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            Dim status As String = Request.QueryString("pagestatus")
            If status = "Add" Then
                btnSubmit.Visible = True
                btnUpdate.Visible = False
            Else
                btnSubmit.Visible = False
                btnUpdate.Visible = True
                GetExistData()
            End If
        End If

        toolingid = Request.QueryString("toolingid")
        partno = Request.QueryString("partno")
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim pErr As String = ""

        If String.IsNullOrEmpty(txtToolingId.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Tooling ID"
        ElseIf String.IsNullOrEmpty(txtToolingName.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Tooling Name"
        ElseIf String.IsNullOrEmpty(txtAssetNo.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Asset No"
        ElseIf String.IsNullOrEmpty(txtNumberOfProcess.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Number of Process"
        ElseIf String.IsNullOrEmpty(txtContractNo.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Contract No"
        ElseIf String.IsNullOrEmpty(txtDimesion.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Dimesion"
        ElseIf String.IsNullOrEmpty(txtAdditional1.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Additional 1"
        ElseIf String.IsNullOrEmpty(txtWeight.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Weight"
        ElseIf String.IsNullOrEmpty(txtAdditional2.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Additional 2"
        ElseIf String.IsNullOrEmpty(txtPrice.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Price"
        ElseIf String.IsNullOrEmpty(txtPartNo.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Part No"
        ElseIf String.IsNullOrEmpty(txtModel.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Model"
        ElseIf String.IsNullOrEmpty(txtPartName.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Part Name"
        ElseIf String.IsNullOrEmpty(txtUoM.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input UoM"
        ElseIf String.IsNullOrEmpty(txtGroup.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Group"
        ElseIf String.IsNullOrEmpty(txtQty.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Qty"
        ElseIf String.IsNullOrEmpty(txtPurchasedYear.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Purchased Year"
        ElseIf String.IsNullOrEmpty(txtInvestationNo.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Investation No"
        Else
            cls_MasterToolingDB.InsertMasterTooling(txtToolingId.Text, txtToolingName.Text, txtAssetNo.Text, txtContractNo.Text, txtAdditional1.Text, txtAdditional2.Text, _
                                                    txtPartNo.Text, txtPartName.Text, txtUoM.Text, txtQty.Text, txtNumberOfProcess.Text, txtDimesion.Text, txtWeight.Text, _
                                                    txtPrice.Text, txtModel.Text, txtGroup.Text, txtPurchasedYear.Text, txtInvestationNo.Text, pUser, pUser, pErr)
            If pErr = "" Then
                cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
            Else
                cbSave.JSProperties("cpMessage") = pErr
            End If

        End If
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"

    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim pErr As String = ""

        If String.IsNullOrEmpty(txtToolingId.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Tooling ID"
        ElseIf String.IsNullOrEmpty(txtToolingName.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Tooling Name"
        ElseIf String.IsNullOrEmpty(txtAssetNo.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Asset No"
        ElseIf String.IsNullOrEmpty(txtNumberOfProcess.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Number of Process"
        ElseIf String.IsNullOrEmpty(txtContractNo.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Contract No"
        ElseIf String.IsNullOrEmpty(txtDimesion.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Dimesion"
        ElseIf String.IsNullOrEmpty(txtAdditional1.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Additional 1"
        ElseIf String.IsNullOrEmpty(txtWeight.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Weight"
        ElseIf String.IsNullOrEmpty(txtAdditional2.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Additional 2"
        ElseIf String.IsNullOrEmpty(txtPrice.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Price"
        ElseIf String.IsNullOrEmpty(txtPartNo.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Part No"
        ElseIf String.IsNullOrEmpty(txtModel.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Model"
        ElseIf String.IsNullOrEmpty(txtPartName.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Part Name"
        ElseIf String.IsNullOrEmpty(txtUoM.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input UoM"
        ElseIf String.IsNullOrEmpty(txtGroup.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Group"
        ElseIf String.IsNullOrEmpty(txtQty.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Qty"
        ElseIf String.IsNullOrEmpty(txtPurchasedYear.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Purchased Year"
        ElseIf String.IsNullOrEmpty(txtInvestationNo.Text) Then
            cbSave.JSProperties("cpMessage") = "Please Input Investation No"
        Else
            cls_MasterToolingDB.UpdateMasterTooling(txtToolingId.Text, txtToolingName.Text, txtAssetNo.Text, txtContractNo.Text, txtAdditional1.Text, txtAdditional2.Text, _
                                                  txtPartNo.Text, txtPartName.Text, txtUoM.Text, txtQty.Text, txtNumberOfProcess.Text, txtDimesion.Text, txtWeight.Text, _
                                                  txtPrice.Text, txtModel.Text, txtGroup.Text, txtPurchasedYear.Text, txtInvestationNo.Text, pUser, pUser, pErr)
            If pErr = "" Then
                cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
            Else
                cbSave.JSProperties("cpMessage") = pErr
            End If

        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("MasterTooling.aspx")
    End Sub

    Sub GetExistData()
        Dim ses As DataSet
        ses = cls_MasterToolingDB.getGrid(toolingid, partno)
        If Not ses Is Nothing Then
            If ses.Tables(0).Rows.Count > 0 Then
                txtToolingId.Text = ses.Tables(0).Rows(0)("Tooling_ID")
                txtToolingName.Text = ses.Tables(0).Rows(0)("Tooling_Name")
                txtAssetNo.Text = ses.Tables(0).Rows(0)("Asset_No")
                txtNumberOfProcess.Text = ses.Tables(0).Rows(0)("Number_Of_Process")
                txtContractNo.Text = ses.Tables(0).Rows(0)("Contract_No")
                txtDimesion.Text = ses.Tables(0).Rows(0)("Dimension")
                txtAdditional1.Text = ses.Tables(0).Rows(0)("Additional_1")
                txtWeight.Text = ses.Tables(0).Rows(0)("Weight")
                txtAdditional2.Text = ses.Tables(0).Rows(0)("Additional_2")
                txtPrice.Text = ses.Tables(0).Rows(0)("Price")
                txtPartNo.Text = ses.Tables(0).Rows(0)("Part_No")
                txtModel.Text = ses.Tables(0).Rows(0)("Model")
                txtPartName.Text = ses.Tables(0).Rows(0)("Part_Name")
                txtUoM.Text = ses.Tables(0).Rows(0)("UoM")
                txtGroup.Text = ses.Tables(0).Rows(0)("Group_ID")
                txtQty.Text = ses.Tables(0).Rows(0)("Qty")
                txtPurchasedYear.Text = ses.Tables(0).Rows(0)("Purchased_Year")
                txtInvestationNo.Text = ses.Tables(0).Rows(0)("Investation_No")

            End If
        End If


    End Sub
End Class
﻿Imports DevExpress.Web.ASPxGridView

Public Class QuotationSupplier_List
    Inherits System.Web.UI.Page
#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim grpcomm As String = ""
    Dim flag As Integer = 0
    Dim pt As String = ""
    Dim buyer As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("K010")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        prj = Request.QueryString("projectid")
        grp = Request.QueryString("groupid")
        comm = Request.QueryString("commodity")
        grpcomm = Request.QueryString("groupcomodity")
        pt = Request.QueryString("projecttype")
        buyer = Request.QueryString("pic")

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If String.IsNullOrEmpty(prj) Then
                cboProject.SelectedIndex = 0
                cboGroup.SelectedIndex = 0
                cboCommodity.SelectedIndex = 0
                cboGroupCommodity.SelectedIndex = 0
                cboPIC.SelectedIndex = 0
                ' cboComplete.SelectedIndex = 0
            End If

            cboProject.Value = prj
            cboGroup.Value = grp
            cboCommodity.Value = comm
            cboGroupCommodity.Value = grpcomm
            cboPIC.Value = buyer
            'cboProjectType.Value = pt

            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboPIC, cboProject.Value, cboGroup.Value, cboCommodity.Value, "S", statusAdmin, pUser)
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub Grid_CustomButtonCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs) Handles Grid.CustomButtonCallback

    End Sub

    Private Sub Grid_CustomButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonEventArgs) Handles Grid.CustomButtonInitialize
        If e.VisibleIndex = -1 Then
            Return
        End If

        'If clsGateICheckSheetDB.CheckGateStatus(Grid.GetRowValues(e.VisibleIndex, "Project_ID"), Grid.GetRowValues(e.VisibleIndex, "Group_ID"), Grid.GetRowValues(e.VisibleIndex, "Commodity"), pUser, Grid.GetRowValues(e.VisibleIndex, "Group_Comodity")) = "Accepted" Then
        '    If e.ButtonID = "Confirm" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        '    If e.ButtonID = "Approve" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        '    If e.ButtonID = "AfterApprove" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        'End If
        'If clsGateICheckSheetDB.CheckGateStatus(Grid.GetRowValues(e.VisibleIndex, "Project_ID"), Grid.GetRowValues(e.VisibleIndex, "Group_ID"), Grid.GetRowValues(e.VisibleIndex, "Commodity"), pUser, Grid.GetRowValues(e.VisibleIndex, "Group_Comodity")) = "Completed" Then
        '    If e.ButtonID = "Submit" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        '    If e.ButtonID = "Approve" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        '    If e.ButtonID = "AfterApprove" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        'End If

        'If clsGateICheckSheetDB.CheckGateStatus(Grid.GetRowValues(e.VisibleIndex, "Project_ID"), Grid.GetRowValues(e.VisibleIndex, "Group_ID"), Grid.GetRowValues(e.VisibleIndex, "Commodity"), pUser, Grid.GetRowValues(e.VisibleIndex, "Group_Comodity")) = "Confirmed" Then
        '    If e.ButtonID = "Submit" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        '    If e.ButtonID = "Confirm" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        '    If e.ButtonID = "AfterApprove" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        'End If

        'Dim status As String = clsGateICheckSheetDB.CheckGateStatus(Grid.GetRowValues(e.VisibleIndex, "Project_ID"), Grid.GetRowValues(e.VisibleIndex, "Group_ID"), Grid.GetRowValues(e.VisibleIndex, "Commodity"), pUser, Grid.GetRowValues(e.VisibleIndex, "Group_Comodity"))
        'If status <> "Confirmed" And status <> "Completed" And status <> "Accepted" Then
        '    If e.ButtonID = "Submit" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        '    If e.ButtonID = "Confirm" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        '    If e.ButtonID = "Approve" Then
        '        e.Visible = DevExpress.Utils.DefaultBoolean.False
        '    End If
        'End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            fRefresh = True
        Else
            up_GridLoad("1")
        End If
    End Sub

    Private Sub up_FillComboProjectType(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                              pAdminStatus As String, pUserID As String, _
                              Optional ByRef pGroup As String = "", _
                              Optional ByRef pParent As String = "", _
                              Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboProjectTypeGateISheet(pUser, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_FillComboGroupCommodityPIC(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _projid As String, _grpid As String, _comm As String, _type As String, _
                           pAdminStatus As String, pUserID As String, _
                           Optional ByRef pGroup As String = "", _
                           Optional ByRef pParent As String = "", _
                           Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsGateICheckSheetDB.FillComboProject("4", "", _projid, _grpid, _comm, _type, pUserID, statusAdmin, pmsg)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_FillComboProject(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _
                             pAdminStatus As String, pUserID As String, projtype As String, _
                             Optional ByRef pGroup As String = "", _
                             Optional ByRef pParent As String = "", _
                             Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsPRJListDB.FillComboProjectISheet(projtype, pUserID, pAdminStatus, pGroup, pParent, pErr)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsQuotationSupplierDB.QuotationSupplierList(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboGroupCommodity.Value, cboPIC.Value, cboComplete.Value, pUser, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub
#End Region

#Region "Event Control"
    Private Sub cboGroupCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroupCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProject As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroupCommodity, cboProject.Value, cboGroup.Value, cboCommodity.Value, "X", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboProject_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboProject.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProjectType As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboProject, "", "", "", "P", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboGroup.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pProject As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboGroup, cboProject.Value, "", "", "G", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboCommodity.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboCommodity, cboProject.Value, cboGroup.Value, "", "C", statusAdmin, pUser)
        End If
    End Sub

    Private Sub cboPIC_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboPIC.Callback
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim pGroup As String = Split(e.Parameter, "|")(1)

        If pFunction = "load" Then
            up_FillComboGroupCommodityPIC(cboPIC, cboProject.Value, cboGroup.Value, cboCommodity.Value, "S", statusAdmin, pUser)
        ElseIf pFunction = "filter" Then
            up_FillComboGroupCommodityPIC(cboPIC, cboProject.Value, cboGroup.Value, cboCommodity.Value, "S", statusAdmin, pUser)
        End If
    End Sub
#End Region

   
End Class
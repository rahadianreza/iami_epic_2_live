﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="DevelopmentSparePartApproval.aspx.vb" Inherits="IAMI_EPIC2.DevelopmentSparePartApproval" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    function GetMessage(s, e) {
        if (s.cpMessage == "There is no data to show!") {
            //alert(s.cpRowCount);
            toastr.info(s.cpMessage, 'Information');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            btnDownload.SetEnabled(false);

        } else if (s.cpMessage == "Download Excel Successfully!") {
            toastr.info(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            btnDownload.SetEnabled(true);
        } else if (s.cpMessage == "Success") {
            btnDownload.SetEnabled(true);
        } else {
            toastr.error(s.cpMessage, 'Error');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        
    }
    
   
    function Validation() {
        if (cboProject.GetText() == '') {
            toastr.warning('Please Select Project !', 'Warning');
            cboProject.Focus();
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
            e.processOnServer = false;
            return;
        }
    }
</script>
<style type="text/css">
    .td-col-l
    {
        padding:0px 0px 0px 10px;
        width:100px;
    }
    .td-col-m
    {
        width:10px;
    }
    td.col-r
    {
        width:200px;
    }
    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
            <tr style="height:10px">
                <td class="td-col-l"></td>
                <td class="td-col-m"></td>
                <td class="td-col-r"></td>
                <td ></td>
                <td></td>
            </tr>
            <tr style="height:25px">
                <td style="padding:0px 0px 0px 10px" class="td-col-l">
                    <dx:aspxlabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Project Name">
                    </dx:aspxlabel>
                </td>
                <td class="td-col-m"></td>
                <td class="td-col-r">
                    <dx:ASPxComboBox ID="cboProject" runat="server" ClientInstanceName="cboProject" Width="120px"
                        Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Project_Name"
                        ValueField="Project_ID" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                        DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                        Height="25px">
                        <Columns>
                            <dx:ListBoxColumn Caption="Project Code" FieldName="Project_ID" Width="100px" />
                            <dx:ListBoxColumn Caption="Project Name" FieldName="Project_Name" Width="120px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td colspan="2">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="SELECT 'ALL' Project_ID ,'ALL' Project_Name UNION ALL Select Project_ID, Project_Name From Proj_Header Where Project_Type='PT03'">
                        <%--SelectCommand="SELECT Project_ID, Project_Name From VW_DevelopmentPart_getProjectID ">--%>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr style="height:20px">
                <td colspan="5"></td>
            </tr>
            <tr style="height:35px">
                <td colspan="5" style="padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {
                         
                           Grid.PerformCallback('load|' + cboProject.GetValue());
                           
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp; &nbsp;
                     <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload"  Theme="Default">                        
                         <ClientSideEvents Click="Validation" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp; &nbsp;
                    
                    </td>
            </tr>
            <tr style="height:10px">
                <td colspan="5">
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                </td>
            </tr>
        </table>
    </div>
    <div>
           <dx:ASPxCallback ID="cbExcel" runat="server" ClientInstanceName="cbExcel">
                         <ClientSideEvents EndCallback="GetMessage" />
                                                        
          </dx:ASPxCallback>
    </div>
    <div style="padding:5px 5px 5px 5px">
         <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                EnableTheming="True" KeyFieldName="ProjectID" Theme="Office2010Black" Width="100%" 
                Font-Names="Segoe UI" Font-Size="9pt" >
              <ClientSideEvents EndCallback="GetMessage" CustomButtonClick="function(s, e) {
	                  if(e.buttonID == &#39;Edit&#39;){ 
                            var rowKey = Grid.GetRowKey(e.visibleIndex);
                            window.location.href= &#39;DevelopmentSparePartApprovalDetail.aspx?ID=&#39; + rowKey;
                      }
                    
                  }">
             </ClientSideEvents>
             <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                    HorizontalScrollBarMode="Auto" />
             <Columns>
                 <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" Width="80px">
                     <CustomButtons>
                         <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Detail">
                         </dx:GridViewCommandColumnCustomButton>
                     </CustomButtons>
                    
                 </dx:GridViewCommandColumn>
                 <dx:GridViewDataTextColumn Caption="No" FieldName="No" VisibleIndex="1" 
                     Width="50px">
                     <Settings AutoFilterCondition="Contains" AllowAutoFilter="False"/>
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" >
                         <Paddings PaddingLeft="5px" PaddingTop="3px" PaddingBottom="3px"/>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="ProjectID" Caption="Project ID" VisibleIndex="2"
                     Width="150px">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px" PaddingTop="3px" PaddingBottom="3px"/>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="ProjectName" Width="250px" Caption="Project Name"
                     VisibleIndex="3">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewBandColumn VisibleIndex="4">
                     <Columns>
                         <dx:GridViewDataTextColumn Caption="Approval Name" VisibleIndex="0" FieldName="ApprovalPerson01">
                             <HeaderStyle HorizontalAlign="Center" />
                         </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn Caption="Approval Date" VisibleIndex="1" FieldName="ApprovalDate01">
                             <HeaderStyle HorizontalAlign="Center" />
                             <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                             </PropertiesTextEdit>
                             <Settings AllowAutoFilter="False" />
                         </dx:GridViewDataTextColumn>
                     </Columns>
                     <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>
                 <dx:GridViewBandColumn VisibleIndex="5">
                     <Columns>
                         <dx:GridViewDataTextColumn Caption="Approval Name" VisibleIndex="0" FieldName="ApprovalPerson02">
                             <HeaderStyle HorizontalAlign="Center" />
                         </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn Caption="Approval Date" VisibleIndex="1" FieldName="ApprovalDate02">
                             <HeaderStyle HorizontalAlign="Center" />
                             <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                             </PropertiesTextEdit>
                             <Settings AllowAutoFilter="False" />
                         </dx:GridViewDataTextColumn>
                     </Columns>
                     <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>
                  <dx:GridViewBandColumn VisibleIndex="6">
                     <Columns>
                         <dx:GridViewDataTextColumn Caption="Approval Name" VisibleIndex="0" FieldName="ApprovalPerson03">
                             <HeaderStyle HorizontalAlign="Center" />
                         </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn Caption="Approval Date" VisibleIndex="1" FieldName="ApprovalDate03">
                             <HeaderStyle HorizontalAlign="Center" />
                             <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                             </PropertiesTextEdit>
                             <Settings AllowAutoFilter="False" />
                         </dx:GridViewDataTextColumn>
                     </Columns>
                     <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>
                  <dx:GridViewBandColumn VisibleIndex="7">
                     <Columns>
                         <dx:GridViewDataTextColumn Caption="Approval Name" VisibleIndex="0" FieldName="ApprovalPerson04">
                             <HeaderStyle HorizontalAlign="Center" />
                         </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn Caption="Approval Date" VisibleIndex="1" FieldName="ApprovalDate04">
                             <HeaderStyle HorizontalAlign="Center" />
                             <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                             </PropertiesTextEdit>
                             <Settings AllowAutoFilter="False" />
                         </dx:GridViewDataTextColumn>
                     </Columns>
                     <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>
                  <dx:GridViewBandColumn VisibleIndex="8">
                     <Columns>
                         <dx:GridViewDataTextColumn Caption="Approval Name" VisibleIndex="0" FieldName="ApprovalPerson05">
                             <HeaderStyle HorizontalAlign="Center" />
                         </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn Caption="Approval Date" VisibleIndex="1" FieldName="ApprovalDate05">
                             <HeaderStyle HorizontalAlign="Center" />
                             <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                             </PropertiesTextEdit>
                             <Settings AllowAutoFilter="False" />
                         </dx:GridViewDataTextColumn>
                     </Columns>
                     <HeaderStyle HorizontalAlign="Center" />
                 </dx:GridViewBandColumn>
                 <dx:GridViewDataTextColumn FieldName="RegisterBy" Width="120px" Caption="Register By"
                     VisibleIndex="10">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="RegisterDate" Width="100px" Caption="Register Date"
                     VisibleIndex="11">
                     <Settings AutoFilterCondition="Contains" />
                        <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                        </PropertiesTextEdit>
                        <Settings AllowAutoFilter="False" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                            Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="UpdateBy" Width="120px" Caption="Update By"
                     VisibleIndex="12">
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="UpdateDate" Width="100px" Caption="Update Date"
                     VisibleIndex="13">
                     <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                        </PropertiesTextEdit>
                        <Settings AllowAutoFilter="False" />
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                            Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                 </dx:GridViewDataTextColumn>
               
             </Columns>
             <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
             </SettingsPager>
             <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                 HorizontalScrollBarMode="Auto" />
             <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
                 <Header>
                     <Paddings Padding="2px"></Paddings>
                 </Header>
                 <EditFormColumnCaption>
                     <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
                 </EditFormColumnCaption>
             </Styles>

         </dx:ASPxGridView>
    </div>
</asp:Content>

﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxGridView
Imports System.IO
Imports System.Transactions

Public Class SupplierQuotationAcceptDetail
    Inherits System.Web.UI.Page

#Region "DECLARATION"

#End Region

#Region "PROCEDURE"
    Private Sub FillComboRevisionNo()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        If Request.QueryString.Count <= 0 Then
            'Using Add Button
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_FillCombo", "SQA|SupplierCode|Condition|RFQNumber|QuotationNo", "1|" & txtSupplierCode.Text & "|1||", ErrMsg)
        Else
            'Using Detail Link from Grid
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_FillCombo", "SQA|SupplierCode|Condition|RFQNumber|QuotationNo", "1|" & txtSupplierCode.Text & "|2|" & Split(Request.QueryString(0), "|")(1) & "|" & Split(Request.QueryString(0), "|")(0), ErrMsg)
        End If

        If ErrMsg = "" Then
            cboRevisionNo.DataSource = ds.Tables(0)
            cboRevisionNo.DataBind()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cboRevisionNo)
        End If
    End Sub

    Private Sub GridLoad()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Acceptance_Detail_List", "SupplierCode|RFQNo|RevisionNo", txtSupplierCode.Text & "|" & txtRFQNumber.Text & "|" & cboRevisionNo.Text, ErrMsg)

        If ErrMsg = "" Then
            Grid.DataSource = ds.Tables(0)
            Grid.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub

    Private Sub SetInformation(ByVal pObj As Object)
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        'Using Detail Link from Grid
        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_SetHeader", "SQA|Source|SupplierCode|RFQNo|RevisionNo", "1|2|" & txtSupplierCode.Text & "|" & txtRFQNumber.Text & "|" & hf.Get("cboRev"), ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                pObj.JSProperties("cpQuotationNo") = ds.Tables(0).Rows(0).Item("Quotation_No").ToString()
                pObj.JSProperties("cpQuotationDate") = ds.Tables(0).Rows(0).Item("Quotation_Date")
                pObj.JSProperties("cpRFQTitle") = ds.Tables(0).Rows(0).Item("RFQ_Title").ToString()
                pObj.JSProperties("cpNote") = ds.Tables(0).Rows(0).Item("Notes").ToString()
                pObj.JSProperties("cpQuotationStatus") = ds.Tables(0).Rows(0).Item("Quotation_Status").ToString()
                pObj.JSProperties("cpAcceptRejectNotes") = ds.Tables(0).Rows(0).Item("Reject_Notes").ToString()
                Session("C030_RFQSet") = ds.Tables(0).Rows(0).Item("RFQ_Set").ToString()


            Else
                pObj.JSProperties("cpQuotationNo") = ""
                pObj.JSProperties("cpRFQTitle") = ""
                pObj.JSProperties("cpNote") = ""
                pObj.JSProperties("cpQuotationStatus") = "0"
                pObj.JSProperties("cpAcceptRejectNotes") = ""
                Session("C030_RFQSet") = ""

                Dim dt As Date
                dt = Year(Now) & "-" & Month(Now) & "-01"
                pObj.JSProperties("cpQuotationDate") = dt
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, cb)
        End If
    End Sub

    Private Sub GridLoadAttachment()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        Try
            ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Detail_ListAttachment", "SQA|QuotationNo|RevisionNo|RFQNumber", "1|" & txtQuotationNo.Text & "|" & cboRevisionNo.Text & "|" & txtRFQNumber.Text, ErrMsg)

            If ErrMsg = "" Then
                GridAtc.DataSource = ds.Tables(0)
                GridAtc.DataBind()

            Else
                DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridAtc)
            End If

        Catch ex As Exception
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, GridAtc)
        End Try
    End Sub
#End Region

#Region "FUNCTION"
    Private Function GetQuotationStatus() As String
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = "0"

        ds = GetDataSource(CmdType.SQLScript, "SELECT Quotation_Status FROM Quotation_Header WHERE Quotation_No = '" & txtQuotationNo.Text & "' AND RFQ_Number = '" & txtRFQNumber.Text & "' ORDER BY Rev DESC", "", "", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            Else
                retVal = ds.Tables(0).Rows(0).Item("Quotation_Status").ToString()
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If

        Return retVal
    End Function

    Private Function GetRFQTitle() As String
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "SELECT RFQ_Title FROM Quotation_Header WHERE Quotation_No = '" & txtQuotationNo.Text & "' AND RFQ_Number = '" & txtRFQNumber.Text & "' ORDER BY Rev DESC", "", "", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count = 0 Then
                DisplayMessage(MsgTypeEnum.Info, "There is no data to show!", Grid)
            Else
                retVal = ds.Tables(0).Rows(0).Item("RFQ_Title").ToString()
            End If
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If

        Return retVal
    End Function

    Private Function IsValidCurrency() As String
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = "Y"

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Acceptence_ValidationCurrency", "QuotationNo", txtQuotationNo.Text, ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                retVal = ds.Tables(0).Rows(0).Item("Result").ToString()
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If

        Return retVal
    End Function

    Private Function IsAllSupplierSubmit() As String
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = "Y"

        ds = GetDataSource(CmdType.StoreProcedure, "sp_Quotation_Acceptence_ValidationAllSupplierSubmit", "RFQNumber", txtRFQNumber.Text, ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                retVal = ds.Tables(0).Rows(0).Item("Result").ToString()
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If

        Return retVal
    End Function

    Private Function GetListSupplierNotSubmit()
        Dim ds As New DataSet
        Dim ErrMsg As String = "", retVal As String = ""

        ds = GetDataSource(CmdType.SQLScript, "SELECT [Result] = dbo.uf_Quotation_Acceptence_ListSupplierNotSubmit('" & txtRFQNumber.Text & "')", "", "", ErrMsg)

        If ErrMsg = "" Then
            retVal = ds.Tables(0).Rows(0).Item("Result").ToString().Trim()
        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If

        Return retVal
    End Function
#End Region

#Region "EVENTS"
    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|C040", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAccept.SetEnabled(false);" & vbCrLf & _
                         "btnReject.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAccept, btnAccept.GetType(), "btnAccept", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboRevisionNo)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.SiteTitle = "SUPPLIER QUOTATION ACCEPTANCE DETAIL"
        Dim dt As Date

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            dt = Year(Now) & "-" & Month(Now) & "-01"
            dtQuotationDate.Value = dt
        End If

        If Request.QueryString.Count > 0 Then
            txtQuotationNo.Text = Split(Request.QueryString(0), "|")(0)
            txtRFQNumber.Text = Split(Request.QueryString(0), "|")(1)
            cboRevisionNo.Text = Split(Request.QueryString(0), "|")(2)
            txtSupplierCode.Text = Split(Request.QueryString(0), "|")(3)
            txtSupplierName.Text = Split(Request.QueryString(0), "|")(4)
            txtRFQTitle.Text = GetRFQTitle()

            Dim Status As String = GetQuotationStatus()
            If Status = "3" Then
                'ACCEPT
                ScriptManager.RegisterStartupScript(cboRevisionNo, cboRevisionNo.GetType(), "cboRevisionNo", "cboRevisionNo.SetEnabled(false);", True)
                ScriptManager.RegisterStartupScript(dtQuotationDate, dtQuotationDate.GetType(), "dtQuotationDate", "dtQuotationDate.ReadOnly(true);", True)
                btnAccept.Enabled = False
                btnReject.Enabled = False

                ' dtQuotationDate.Enabled = False
                ' cboRevisionNo.Enabled = False
            End If
        End If
    End Sub

    Private Sub SupplierQuotationAcceptDetail_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        'Session.Remove("C030_DataExist")
    End Sub

    Private Sub cb_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cb.Callback
        Call SetInformation(cb)
    End Sub

    Private Sub cbExist_Callback(source As Object, e As DevExpress.Web.ASPxCallback.CallbackEventArgs) Handles cbExist.Callback
        Dim ds As New DataSet
        Dim ErrMsg As String = ""

        Select Case e.Parameter
            Case "CurrencyValidation"
                Try
                    cbExist.JSProperties("cpParameter") = "CurrValidation"
                    cbExist.JSProperties("cpValidCurrency") = IsValidCurrency()

                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                End Try

            Case "AllSupplierSubmitValidation"
                Try
                    Dim ls_AllSupplierSubmit As String = IsAllSupplierSubmit()
                    cbExist.JSProperties("cpParameter") = "AllSupplierSubmitValidation"
                    cbExist.JSProperties("cpValidAllSupplierSubmit") = ls_AllSupplierSubmit

                    If ls_AllSupplierSubmit = "N" Then
                        cbExist.JSProperties("cpListSupplierNotSubmit") = GetListSupplierNotSubmit()
                    End If

                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                End Try

            Case "accept"
                Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
                Dim idx As Integer = 0

                Try
                    cbExist.JSProperties("cpParameter") = "accept"

                    Dim rev As String = "0"
                    If IsNothing(hf.Get("cboRev")) = True Then
                        rev = cboRevisionNo.Text
                    Else
                        rev = hf.Get("cboRev")
                    End If

                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()

                        Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                            If IsNothing(Session("C030_RFQSet")) = False Then
                                ls_RFQSet = Session("C030_RFQSet").ToString()
                            End If

                            ls_SQL = "EXEC sp_Quotation_Acceptance_Update "
                            ls_SQL = ls_SQL + "'" & txtQuotationNo.Text & "','" & rev & "','" & ls_RFQSet & "','" & txtRFQNumber.Text & "','" & txtSupplierCode.Text & "','3','" & UserLogin & "','" & NoteReject.Text & "'  "

                            Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()

                            sqlTran.Commit()
                        End Using
                    End Using

                    Call SetInformation(cbExist)
                    Call DisplayMessage(MsgTypeEnum.Success, "Data updated successfully!", cbExist)

                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                End Try

            Case "reject"
                Dim ls_SQL As String = "", ls_RFQSet As String = "", UserLogin As String = Session("user")
                Dim idx As Integer = 0, QuotationDate As String = Format(dtQuotationDate.Value, "yyyy-MM-dd")

                Try
                    cbExist.JSProperties("cpParameter") = "reject"

                    Using sqlConn As New SqlConnection(Sconn.Stringkoneksi)
                        sqlConn.Open()

                        Using sqlTran As SqlTransaction = sqlConn.BeginTransaction()

                            If IsNothing(Session("C030_RFQSet")) = False Then
                                ls_RFQSet = Session("C030_RFQSet").ToString()
                            End If

                            '#HEADER
                            ls_SQL = "EXEC sp_Quotation_Update_Header " & _
                                     "'1'," & _
                                     "'" & txtQuotationNo.Text & "','" & cboRevisionNo.Text & "','" & ls_RFQSet & "','" & txtRFQNumber.Text & "','" & txtSupplierCode.Text & "'," & _
                                     "'" & txtRFQTitle.Text & "','" & QuotationDate & "','" & NoteReject.Text & "','4','" & UserLogin & "'"
                            Dim sqlComm As New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()

                            '#DETAIL
                            ls_SQL = "EXEC sp_Quotation_Update_Detail " & _
                                     "'1','" & txtQuotationNo.Text & "','" & cboRevisionNo.Text & "','" & txtRFQNumber.Text & "','','0','','0','" & UserLogin & "'"
                            sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            sqlComm.CommandType = CommandType.Text
                            sqlComm.ExecuteNonQuery()
                            sqlComm.Dispose()

                            'dont copy for attachment if reject  
                            '#ATTACHMENT
                            'ls_SQL = "EXEC sp_Quotation_Update_Attachment '" & cboRevisionNo.Text & "','" & txtRFQNumber.Text & "'"
                            'sqlComm = New SqlCommand(ls_SQL, sqlConn, sqlTran)
                            'sqlComm.CommandType = CommandType.Text
                            'sqlComm.ExecuteNonQuery()
                            'sqlComm.Dispose()

                            sqlTran.Commit()
                        End Using
                    End Using

                    Call SetInformation(cbExist)
                    Call DisplayMessage(MsgTypeEnum.Success, "Data updated successfully!", cbExist)

                Catch ex As Exception
                    DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cbExist)
                End Try
        End Select
    End Sub

    Private Sub cboRevisionNo_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboRevisionNo.Callback
        Call FillComboRevisionNo()
        If cboRevisionNo.Items.Count > 0 Then
            'If there are more than 1 data, then set the default selected index (ALL)
            'If Request.QueryString.Count > 0 Then
            '    cboRevisionNo.SelectedIndex = cboRevisionNo.Items.Count - 1
            'End If
        End If
    End Sub

    Private Sub Grid_BatchUpdate(sender As Object, e As DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs) Handles Grid.BatchUpdate
        Try

        Catch ex As Exception
            Session("C030_ErrMsg") = Err.Description
        End Try
    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Grid.JSProperties("cpType") = ""
        Grid.JSProperties("cpMessage") = ""

        Select Case e.Parameters
            Case "load"
                Call GridLoad()
                Grid.JSProperties("cpActionAfter") = "gridload"

            Case "save"
                Call GridLoad()
                Grid.JSProperties("cpActionAfter") = "save"

                Call SetInformation(Grid)
                Call DisplayMessage(MsgTypeEnum.Info, "Data save successfully!", Grid)
        End Select

        If IsNothing(Session("C030_ErrMsg")) = False Then
            Call DisplayMessage(MsgTypeEnum.ErrorMsg, Session("C030_ErrMsg"), Grid)
            Session.Remove("C030_ErrMsg")
        End If
    End Sub

    Private Sub GridAtc_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles GridAtc.CommandButtonInitialize
        If (e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Update Or e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Cancel) Then
            e.Visible = False
        End If
    End Sub

    Private Sub GridAtc_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles GridAtc.CustomCallback
        Call GridLoadAttachment()
    End Sub

    Private Sub GridAtc_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles GridAtc.HtmlDataCellPrepared
        e.Cell.BackColor = Color.LemonChiffon
    End Sub

    Protected Sub keyFieldLink_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim link As ASPxHyperLink = TryCast(sender, ASPxHyperLink)
        Dim container As GridViewDataItemTemplateContainer = TryCast(link.NamingContainer, GridViewDataItemTemplateContainer)

        If container.ItemIndex >= 0 Then
            link.Text = Split(container.KeyValue, "|")(0)
            link.Target = "_blank"
            link.NavigateUrl = Split(container.KeyValue, "|")(1)
        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Try
            If Request.QueryString.Count > 0 Then
                Session("btnBack_SupplierQuotationAcceptDetail") = Split(Request.QueryString(0), "|")(3) & "|" & Split(Request.QueryString(0), "|")(5)
            End If
            hf.Remove("cboRev")

        Catch ex As Exception

        End Try
        
        Response.Redirect("SupplierQuotationAccept.aspx")
    End Sub
#End Region

End Class
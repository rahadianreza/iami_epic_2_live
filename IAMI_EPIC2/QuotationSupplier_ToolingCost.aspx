﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"
    CodeBehind="QuotationSupplier_ToolingCost.aspx.vb" Inherits="IAMI_EPIC2.QuotationSupplier_ToolingCost" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <%--FunctionforMessage--%>
        function OnEndCallback(s, e) {
            if (s.cp_message != "" && s.cp_val == 1) {
                if (s.cp_type == "Success" && s.cp_val == 1) {
                    toastr.success(s.cp_message, 'Success');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                   
                    TotalAmountInterest(s.cp_Total);
                    TotalToolingCost(s.cp_Total);
                    DepreciationCost(s.cp_Total);
                    
                    if (txtTotalAmountInterest.GetValue() == '' )
                    {
                        txtTotalAmountInterest.SetText(0);
                    } 

                    if (txtTotalToolingCost.GetValue() == '' )
                    {
                        txtTotalToolingCost.SetText(0);
                    } 

                    if (txtDepreciationCost.GetValue() == '')
                    {
                        txtDepreciationCost.SetText(0);
                    } 
                }
                else if (s.cp_type == "Warning" && s.cp_val == 1) {
                    toastr.warning(s.cp_message, 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    ss.cp_val = 0;
                    s.cp_message = "";
                }
                else if (s.cp_type == "ErrorMsg" && s.cp_val == 1) {
                    toastr.error(s.cp_message, 'Error');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    s.cp_val = 0;
                    s.cp_message = "";
                }
            }
            else if (s.cp_message == "" && s.cp_val == 0) {
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                txtInterest.SetText(s.cp_interest);
                txtDepreciationPeriodYear.SetText(s.cp_Depreciation_Period);
                txtDepreciationPeriodYearAfter.SetText(s.cp_Depreciation_Period_After);
                txtTotalInterest.SetText(s.cp_Total_Interest);
                txtQtyPerUnit.SetText(s.cp_Qty_PerUnit);
                txtQtyPerUnitAfter.SetText(s.cp_Qty_PerUnit_After);

                txtProjectProdUnit.SetText(s.cp_Project_Prod_unit_Month);
                txtProjectProdUnitAfter.SetText(s.cp_Project_Prod_unit_Month_After);

                txtDepreciationPeriod.SetText(s.cp_Total_Depreciation_Period);
                txtTotalAmountInterest.SetText(s.cp_Total_Amount_Interest);
                txtTotalToolingCost.SetText(s.cp_Total_Tooling_Cost);
                txtDepreciationCost.SetText(s.cp_Decpreciation_Cost);
                cboType.SetValue(s.cp_Toolingtype);
               
                if (txtTotalAmountInterest.GetValue() == 'NaN')
                {
                    txtTotalAmountInterest.SetText(0);
                } 

                if (txtTotalToolingCost.GetValue() == 'NaN')
                {
                    txtTotalToolingCost.SetText(0);
                } 

                if (txtDepreciationCost.GetValue() == 'NaN')
                {
                    txtDepreciationCost.SetText(0);
                } 

               
            }
        };

         function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtItemCode.Focus();
            cboGroupItem.SetText('');
            cboCategory.SetText('');
            cboPRType.SetText('');
            txtSAPNumber.SetText('');
            cboType.SetText('');

            TotalAmountInterest(s.cp_Total);
            TotalToolingCost(s.cp_Total);
            DepreciationCost(s.cp_Total);
        }
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            TotalAmountInterest(s.cp_Total);
            TotalToolingCost(s.cp_Total);
            DepreciationCost(s.cp_Total);
            //window.location.href = "/ItemList.aspx";

            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/ItemList.aspx';
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                if (pathArray[1] == "AddItemMaster.aspx") {
                    window.location.href = window.location.origin + '/ItemList.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtSAPNumber.SetText('');
            txtItemCode.Focus();
            cboType.SetText('');
        }
        else if (s.cpMessage == "Data Completed!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            txtItemCode.SetText('');
            txtDescription.SetText('');
            txtSpecification.SetText('');
            cboUOM.SetText('');
            txtLastIAPrice.SetText('');
            txtLastSupplier.SetText('');
            txtItemCode.Focus();
            cboGroupItem.SetText('');
            cboCategory.SetText('');
            cboPRType.SetText('');
            txtSAPNumber.SetText('');
            cboType.SetText('');
        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            TotalAmountInterest(s.cp_Total);
            TotalToolingCost(s.cp_Total);
            DepreciationCost(s.cp_Total);          
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    };

         function OnBatchEditStartEditing(s, e) {
            currentColumnName = e.focusedColumn.fieldName;
            if (currentColumnName == "Part_No" || currentColumnName == "Commodity" || currentColumnName == "Upc") {
                e.cancel = true;
            }
            currentEditableVisibleIndex = e.visibleIndex;
        };  

        function SaveData(s, e) {
            Grid.UpdateEdit()
        }

        function disableenablebuttonsubmit(_val) {
            if (_val = '1') {
                btnSave.SetEnabled(false)
            } else {
                btnSave.SetEnabled(true)
             }
        }

          function OnUpdateCheckedChanged(s, e) {
            if (s.GetValue() == -1) s.SetValue(1);
            Grid.SetFocusedRowIndex(-1);
            for (var i = 0; i < Grid.GetVisibleRowsOnPage(); i++) {
                if (Grid.batchEditApi.GetCellValue(i, "Status_Drawing", false) != s.GetValue()) {
                    Grid.batchEditApi.SetCellValue(i, "Status_Drawing", s.GetValue());
                }                
            }
        };
       
       function selectedValueCbo(cmbCategory) {
            if(Grid.GetEditor("Material_Code").InCallback())
                lastCountry = cmbCategory.GetValue().toString();
            else 
                Grid.GetEditor("Material_Code").PerformCallback(cmbCategory.GetValue().toString());
       };

       function interestTotal() {
           txtTotalInterest.SetText(txtInterest.GetValue() * txtDepreciationPeriodYear.GetValue());
           depreTotal();

            TotalAmountInterest(s.cp_Total);
            TotalToolingCost(s.cp_Total);
            DepreciationCost(s.cp_Total);
           //txtTotalInterest.value = txtInterest.text * txtDepreciationPeriodYear.text
       };

        function depreTotal() {
           txtDepreciationPeriod.SetText(txtQtyPerUnit.GetValue() * txtProjectProdUnit.GetValue() * (txtDepreciationPeriodYear.GetValue()*12))
           //txtTotalInterest.value = txtInterest.text * txtDepreciationPeriodYear.text

             TotalAmountInterest(s.cp_Total);
            TotalToolingCost(s.cp_Total);
            DepreciationCost(s.cp_Total);
       };

       function TotalAmountInterest(val)
       {
            txtTotalAmountInterest.SetText((val * (txtTotalInterest.GetValue()/100)).toLocaleString('en'))
       };

       function TotalToolingCost(val)
       {
            txtTotalToolingCost.SetText((parseInt(val,10) + parseInt((txtTotalAmountInterest.GetValue().replace(',','')),10)).toLocaleString('en'))
       };

       function DepreciationCost(val)
       {
            txtDepreciationCost.SetText((parseInt(  txtTotalToolingCost.GetValue().replace(',',''),10) / parseInt((txtDepreciationPeriod.GetValue().replace(',','')),10)).toLocaleString('en'))
            
       };

        function SelectValueType1(s,e){
      
            if (AdjustmentType.GetValue() == 'F'){
                VisibilityFormula(true);
                VisibilityTotalAmount(false);
            }
            if (AdjustmentType.GetValue() == 'M'){
                VisibilityFormula(false);
                VisibilityTotalAmount(true);
            }
      };

       function VisibilityFormula(visible) {
            var disp = visible ? 'table-cell' : 'none';
            $('td.Hidden').css('display', disp);
       };

       function VisibilityTotalAmount(visible) {
            var disp = visible ? 'table-cell' : 'none';
            $('td.Amount').css('display', disp);
       };

        const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2
    })

      
    </script>
    <style>
        .Amount
        {
            display: none;
        }
        .AmountVisible
        {
            
        }
        .Hidden 
        {
        }
        .style1
        {
            width: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server" >
    <div style="border: 1px solid black">
        <table>
            <tr>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                        Text="Part No">
                    </dx:ASPxLabel>
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="padding: 10px 0px 0px 10px; width">
                    <dx:ASPxComboBox ID="cboPartNo" runat="server" ClientInstanceName="cboPartNo" Width="120px"
                        Font-Names="Segoe UI" TextField="Part_Name" ValueField="Part_No" TextFormatString="{1}"
                        Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                        EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                        <Columns>
                            <dx:ListBoxColumn Caption="Part No" FieldName="Part_No" Width="100px" />
                            <dx:ListBoxColumn Caption="Part Name" FieldName="Part_Name" Width="120px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx:ASPxComboBox>
                </td>
                <td>
                </td>
                 <td style="padding: 10px 0px 0px 50px; vertical-align: top;" rowspan="2">
                    <table>
                        <tr>
                            <td style="font-family: Segoe UI; font-size: 9pt;" colspan="3">
                               <b>Currency Rate</b> 
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                USD &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                : &nbsp
                            </td>
                            <td>
                                <dx1:ASPxLabel ID="lblUSDRate" runat="server" ClientInstanceName="lblUSDRate"  Font-Names="Segoe UI" Font-Size="9pt"></dx1:ASPxLabel> &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                JPY &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                : &nbsp
                            </td>
                            <td>
                                <dx1:ASPxLabel ID="lblJPYRate" runat="server" ClientInstanceName="lblJPYRate"  Font-Names="Segoe UI" Font-Size="9pt"></dx1:ASPxLabel> &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                THB &nbsp
                            </td>
                            <td style="font-family: Segoe UI; font-size: 9pt;">
                                : &nbsp
                            </td>
                            <td>
                                <dx1:ASPxLabel ID="lblTHBRate" runat="server" ClientInstanceName="lblTHBRate" Font-Names="Segoe UI" Font-Size="9pt"></dx1:ASPxLabel> &nbsp
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr style="height: 50px">
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnBack" runat="server" Text="Back to List" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnBack" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">
                        <ClientSideEvents Click="function(s, e) {
                            Grid.PerformCallback('load|');
                        }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
                <td style="width: 80px; padding: 10px 0px 0px 10px">
                    <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload" OnClick="btnDownload_Click" Theme="Default">
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                </td>
                <td style="width: 10px">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <div style="border: 1px solid #000000; padding: 10px 10px 10px 10px;">
            <table>
                <tr>
                    <td style="padding: 5px 5px 5px 5px; width: 21%;">
                        Interest / Year
                    </td>
                    <td style="padding: 0 5px 0 5px;">
                        :
                    </td>
                    <td colspan="3">
                        <dx:ASPxTextBox ID="txtInterest" ClientInstanceName="txtInterest" runat="server"
                            HorizontalAlign="Right" Width="100px" ValidationSettings-Display="Dynamic" MaxLength="3">
                            <ClientSideEvents KeyUp="interestTotal" />
                        </dx:ASPxTextBox>
                    </td>
                   
                    <td style="text-align: center; width: 5%;">
                        %
                    </td>
                    <td style="width: 150px;">
                        &nbsp;
                    </td>
                    <td style="width: 25%;">
                        Qty PerUnit
                    </td>
                    <td style="padding: 0 5px 0 5px;">
                        :
                    </td>
                    <td >
                        <dx:ASPxTextBox ID="txtQtyPerUnit" ClientInstanceName="txtQtyPerUnit" runat="server"
                            HorizontalAlign="Right" Width="100px" ValidationSettings-Display="Dynamic" MaxLength="3">
                            <ClientSideEvents KeyUp="depreTotal" />
                        </dx:ASPxTextBox>
                    </td>
                    <td style="padding: 0px 0px 0px 5px;">
                         <dx:ASPxTextBox ID="txtQtyPerUnitAfter" ClientInstanceName="txtQtyPerUnitAfter" runat="server"
                            HorizontalAlign="Right" Width="100px" ValidationSettings-Display="Dynamic" MaxLength="3">
                            <ClientSideEvents KeyUp="depreTotal" />
                        </dx:ASPxTextBox>
                    </td>
                    
                    
                </tr>
                <tr>
                    <td style="padding: 5px 5px 5px 5px;">
                        Depreciation Period (Year)
                    </td>
                    <td style="padding: 0 5px 0 5px;">
                        :
                    </td>
                    <td width="120px">
                        <dx:ASPxTextBox ID="txtDepreciationPeriodYear" ClientInstanceName="txtDepreciationPeriodYear"
                            HorizontalAlign="Right" runat="server" Width="100px" ValidationSettings-Display="Dynamic"
                            MaxLength="3">
                            <ClientSideEvents KeyUp="interestTotal" />
                        </dx:ASPxTextBox>
                         
                    </td>
                    <td style="padding: 0px 0px 0px 5px;">
                        <dx:ASPxTextBox ID="txtDepreciationPeriodYearAfter" ClientInstanceName="txtDepreciationPeriodYearAfter"
                            HorizontalAlign="Right" runat="server" Width="100px" ValidationSettings-Display="Dynamic"
                            MaxLength="3">
                            <ClientSideEvents KeyUp="interestTotal" />
                        </dx:ASPxTextBox>
                    </td>
                    <td></td>
                    <td style="text-align: center; ">
                        Years
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Project Prod Unit PerMonth
                    </td>
                    <td style="padding: 0 5px 0 5px;">
                        :
                    </td>
                    <td >
                        <dx:ASPxTextBox ID="txtProjectProdUnit" ClientInstanceName="txtProjectProdUnit" runat="server"
                            HorizontalAlign="Right" Width="100px" ValidationSettings-Display="Dynamic" MaxLength="3">
                            <ClientSideEvents KeyUp="depreTotal" />
                        </dx:ASPxTextBox>
                    </td>
                     <td style="padding: 0px 0px 0px 5px;">
                        <dx:ASPxTextBox ID="txtProjectProdUnitAfter" ClientInstanceName="txtProjectProdUnitAfter" runat="server"
                            HorizontalAlign="Right" Width="100px" ValidationSettings-Display="Dynamic" MaxLength="3">
                            <ClientSideEvents KeyUp="depreTotal" />
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px 5px 5px 5px;">
                        Total Interest
                    </td>
                    <td style="padding: 0 5px 0 5px;">
                        :
                    </td>
                    <td colspan="2">
                        <dx:ASPxTextBox ID="txtTotalInterest" ClientInstanceName="txtTotalInterest" runat="server"
                            HorizontalAlign="Right" Width="100px" ReadOnly="true" ValidationSettings-Display="Dynamic">
                        </dx:ASPxTextBox>
                    </td>
                   
                    <td></td>
                    <td style="text-align: center; ">
                        %
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Depreciation Period
                    </td>
                    <td style="padding: 0 5px 0 5px;">
                        :
                    </td>
                    <td style="width: 100px">
                        <dx:ASPxTextBox ID="txtDepreciationPeriod" ClientInstanceName="txtDepreciationPeriod"
                            HorizontalAlign="Right" runat="server" Width="100px" ReadOnly="true" ValidationSettings-Display="Dynamic">
                        </dx:ASPxTextBox>
                    </td>
                    <td style="width: 0%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px 5px 5px 5px;">
                        Type
                    </td>
                    <td style="padding: 0 5px 0 5px;">
                        :
                    </td>
                    <td colspan="2">
                        <dx:ASPxComboBox ID="cboType" runat="server" ClientInstanceName="cboType" Width="100px"
                            Font-Names="Segoe UI" TextField="Type" ValueType="System.String" Value='<%# Bind("ACTIVE") %>' TextFormatString="{1}"
                            Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                            EnableIncrementalFiltering="True" Height="25px" TabIndex="3">
                            <Items>
                                <dx:ListEditItem Text="Lumpsum" Value="0" />
                                <dx:ListEditItem Text="Depreciation" Value="1" />
                            </Items>
                            <ItemStyle Height="10px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                            <ButtonStyle Width="5px" Paddings-Padding="4px">
                                <Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx:ASPxComboBox>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="11" style="height:5px;">
                        
                    </td>
                </tr>
                <tr>
                    <td  style="padding: 5px 5px 5px 5px;">
                    </td>
                    <td>
                    </td>
                    <td colspan="9" style="padding: 5px 0 0 0" class="style1">
                        <dx:ASPxButton ID="btnSubmit" ClientInstanceName="btnSubmit" runat="server" Text="Submit"
                            Width="100px">
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <div>
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" Font-Names="Segoe UI"
            Font-Size="9pt" KeyFieldName="Tooling_Name">
            <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="true" ShowNewButtonInHeader="true" ShowDeleteButton="true">
                </dx:GridViewCommandColumn>
                 <dx:GridViewDataComboBoxColumn Caption="Input Type" FieldName="AdjustmentType" HeaderStyle-HorizontalAlign="Center"
                    VisibleIndex="2" Width="120px">
                    <PropertiesComboBox Width="145px" TextFormatString="{1}" ClientInstanceName="AdjustmentType"
                        DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith">
                        <ClientSideEvents SelectedIndexChanged="SelectValueType1" />
                        <Items>
                            <dx:ListEditItem Text="Formula" Value="F" />
                            <dx:ListEditItem Text="Value" Value="M" />
                        </Items>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="2px">
                            <Paddings Padding="2px"></Paddings>
                        </ButtonStyle>
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataTextColumn Caption="Die Name" VisibleIndex="4" FieldName="Tooling_Name">
                    <PropertiesTextEdit MaxLength="20">
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn Caption="Currency" FieldName="Currency" VisibleIndex="2"
                    Width="120px">
                    <PropertiesComboBox TextField="Par_Description" ValueField="Par_Code" DataSourceID="sdsCurrency"
                        Width="145px" TextFormatString="{1}" ClientInstanceName="Currency" DropDownStyle="DropDownList"
                        IncrementalFilteringMode="StartsWith">
                        <Columns>
                            <dx:ListBoxColumn FieldName="Par_Code" Caption="Code" Width="65px" />
                            <dx:ListBoxColumn FieldName="Par_Description" Caption="Name" Width="200px" />
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px">
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="2px">
                            <Paddings Padding="2px"></Paddings>
                        </ButtonStyle>
                    </PropertiesComboBox>
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                    <Settings AutoFilterCondition="Contains" AllowAutoFilter="true" />
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataTextColumn Caption="(A) <br/> Mfg Cost" VisibleIndex="5" Width="130px" FieldName="Mfg_Cost">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                     <EditCellStyle CssClass="Hidden">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Hidden">
                    </EditFormCaptionStyle>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(B) <br/> Administrative Expense" VisibleIndex="6" FieldName="Administrative_Expense" Width="150px">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                     <EditCellStyle CssClass="Hidden">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Hidden">
                    </EditFormCaptionStyle>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="(A + B) <br/> Tooling Cost" VisibleIndex="6" FieldName="ToolCost" Width="150px">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                     <EditCellStyle CssClass="Amount">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Amount">
                    </EditFormCaptionStyle>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn Caption="Tooling Cost (IDR)" VisibleIndex="6" FieldName="BaseAmount">
                    <PropertiesTextEdit DisplayFormatString="#,###" Style-HorizontalAlign="Right">
                        <MaskSettings Mask="<0..100000000g>" IncludeLiterals="DecimalSymbol" />
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="false" />
                     <EditCellStyle CssClass="Amount">
                    </EditCellStyle>
                    <EditFormCaptionStyle CssClass="Amount">
                    </EditFormCaptionStyle>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Remarks" VisibleIndex="6" FieldName="Remarks">
                    <PropertiesTextEdit MaxLength="50">
                    </PropertiesTextEdit>
                    <EditFormSettings Visible="true" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register By" VisibleIndex="6" FieldName="Register_By">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Register Date" VisibleIndex="6" FieldName="Register_Date">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update By" VisibleIndex="6" FieldName="Update_By">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Update Date" VisibleIndex="6" FieldName="Update_Date">
                    <EditFormSettings Visible="False" />
                    <Settings AllowAutoFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                    </PropertiesTextEdit>
                    <Settings AutoFilterCondition="Contains" />
                    <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="True">
                        <Paddings PaddingLeft="5px"></Paddings>
                    </HeaderStyle>
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
            <SettingsEditing EditFormColumnCount="1" Mode="PopupEditForm" />
            <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
            </SettingsPager>
            <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Auto" />
            <SettingsPopup>
                <EditForm Modal="false" HorizontalAlign="WindowCenter" VerticalAlign="WindowCenter"
                    Width="320" />
            </SettingsPopup>
            <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px"
                EditFormColumnCaption-Paddings-PaddingRight="10px">
                <Header>
                    <Paddings Padding="2px"></Paddings>
                </Header>
                <EditFormColumnCaption Font-Size="9pt" Font-Names="Segoe UI">
                    <Paddings PaddingLeft="15px" PaddingTop="5px" PaddingBottom="5px" PaddingRight="15px">
                    </Paddings>
                </EditFormColumnCaption>
                <CommandColumnItem ForeColor="Orange">
                </CommandColumnItem>
            </Styles>
            <SettingsText ConfirmDelete="Are you sure want to delete ?"></SettingsText>
            <Templates>
                <EditForm>
                    <div style="padding: 15px 15px 15px 15px">
                        <dx:ContentControl ID="ContentControl1" runat="server">
                            <dx:ASPxGridViewTemplateReplacement ID="Editors" ReplacementType="EditFormEditors"
                                runat="server"></dx:ASPxGridViewTemplateReplacement>
                        </dx:ContentControl>
                    </div>
                    <div style="text-align: left; padding: 5px 5px 5px 15px">
                        <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                        <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                            runat="server"></dx:ASPxGridViewTemplateReplacement>
                    </div>
                </EditForm>
            </Templates>
        </dx:ASPxGridView>
        <br />
        <div style="border: 1px solid #000000; padding: 10px 10px 10px 10px;">
            <table style="width: 100%;">
                <tr>
                    <td style="padding: 5px 5px 5px 5px;">
                        Total Amount Interest
                    </td>
                    <td style="padding: 0 5px 0 5px;">
                        :
                    </td>
                    <td>
                        <%-- <dx:ASPxSpinEdit  ID="txtTotalAmountInterest" ClientInstanceName="txtTotalAmountInterest" runat="server" ReadOnly="true"
                    HorizontalAlign="Right" DisplayFormatString="#,###" Width="170px">
                      <SpinButtons ShowIncrementButtons="false"></SpinButtons>
                      
                    </dx:ASPxSpinEdit>--%>
                        <dx:ASPxTextBox ID="txtTotalAmountInterest" ClientInstanceName="txtTotalAmountInterest"
                            ReadOnly="true" HorizontalAlign="Right" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                    <td style="width: 52%">
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px 5px 5px 5px;">
                        Total Tooling Cost
                    </td>
                    <td style="padding: 0 5px 0 5px;">
                        :
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtTotalToolingCost" ClientInstanceName="txtTotalToolingCost"
                            ReadOnly="true" HorizontalAlign="Right" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px 5px 5px 5px;">
                        Depreciation Cost
                    </td>
                    <td style="padding: 0 5px 0 5px;">
                        :
                    </td>
                    <td>
                        <dx:ASPxTextBox ID="txtDepreciationCost" ClientInstanceName="txtDepreciationCost"
                            ReadOnly="true" HorizontalAlign="Right" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                    </td>
                </tr>
            </table>
        </div>
        <dx:ASPxCallback ID="cbMessage" runat="server" ClientInstanceName="cbMessage">
            <%--<ClientSideEvents CallbackComplete="MessageBox" />--%>
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbkDrawingComplete" runat="server" ClientInstanceName="cbkDrawingComplete">
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
            <ClientSideEvents Init="MessageError" />
        </dx:ASPxCallback>
        <asp:SqlDataSource ID="sdsPartNo" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
            SelectCommandType="Text"></asp:SqlDataSource>
        <dx:ASPxCallback ID="cbDS" runat="server" ClientInstanceName="cbDS">
        </dx:ASPxCallback>
        <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
        </dx:ASPxGridViewExporter>
           <asp:SqlDataSource ID="sdsCurrency" runat="server" SelectCommand="SELECT Par_Code, Par_Description FROM Mst_Parameter WHERE Par_Group = 'Currency'"
            ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" SelectCommandType="Text">
        </asp:SqlDataSource>
    </div>
</asp:Content>

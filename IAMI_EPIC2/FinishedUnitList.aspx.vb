﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.Data
Imports OfficeOpenXml
Imports Microsoft.Office.Interop.Excel
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class FinishedUnitList
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""

    Dim pHeader As Boolean
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Procedure"
    Private Sub AllowUpdateSetting()
        Dim ds As New DataSet
        Dim ErrMsg As String = ""
        Dim Allow As String = ""
        ds = GetDataSource(CmdType.StoreProcedure, "sp_UserSetup_AllowUpdateSetting", "UserID|MenuID", Session("user") & "|A150", ErrMsg)

        If ErrMsg = "" Then
            If ds.Tables(0).Rows.Count > 0 Then
                Allow = ds.Tables(0).Rows(0).Item("AllowUpdate").ToString().Trim()
            End If

            If Allow = "0" Then
                Dim script As String = ""
                script = "btnAdd.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(btnAdd, btnAdd.GetType(), "btnAdd", script, True)
            End If

        Else
            DisplayMessage(MsgTypeEnum.ErrorMsg, ErrMsg, Grid)
        End If
    End Sub


    Private Sub up_GridLoad(pVariant As String, pSupplier As String)
        Dim ErrMsg As String = ""
        Dim Pro As List(Of clsFinishedItemList)
        Pro = clsFinishedItemListDB.GetList(pVariant, pSupplier, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Pro
            Grid.DataBind()
        Else
            show_error(MsgTypeEnum.ErrorMsg, ErrMsg, 1)
        End If
    End Sub

    Private Sub up_ExcelGridAuto(Optional ByRef pErr As String = "")

        Try


            up_GridLoad(gs_FilterVariant, gs_FilterSupplier)

            Dim ps As New PrintingSystem()

            Dim link1 As New PrintableComponentLink(ps)
            link1.Component = GridExporter

            Dim compositeLink As New CompositeLink(ps)
            compositeLink.Links.AddRange(New Object() {link1})

            compositeLink.CreateDocument()
            Using stream As New MemoryStream()
                compositeLink.PrintingSystem.ExportToXlsx(stream)
                Response.Clear()
                Response.Buffer = False
                Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AppendHeader("Content-Disposition", "attachment; filename=ItemList" & Format(Date.Now, "ddMMyyyyHHmmss") & ".xlsx")
                Response.BinaryWrite(stream.ToArray())
                Response.End()
            End Using

            ps.Dispose()
        Catch ex As Exception
            pErr = ex.Message
        End Try

    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        'If Not Page.IsPostBack Then
        'up_GridLoad(fgroup, fcategroy, fprtype, flastsupplier)

        'End If

        If IsNothing(Session("user")) = False Then
            Try
                Call AllowUpdateSetting()
            Catch ex As Exception
                DisplayMessage(MsgTypeEnum.ErrorMsg, Err.Description, cboVariant)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A150")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A150")

        cbExcel.JSProperties("cpmessage") = ""
        Dim i As Integer


        If Not Page.IsPostBack Then
            If gs_Back = True Then
                up_GridLoad(gs_FilterVariant, gs_FilterSupplier)
                cboVariant.SelectedIndex = gs_FilterVariantIndex
                cboType.SelectedIndex = gs_FilterSupplierIndex
            Else
                cboVariant.SelectedIndex = 0
                cboType.SelectedIndex = 0
            End If
        End If

    End Sub
#End Region

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("~/AddFinishedUnit.aspx")
    End Sub

    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback


        up_GridLoad(gs_FilterVariant, gs_FilterSupplier)
        'pHeader = False
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback

        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim pVariant As String
        Dim psupplier As String

        pVariant = Split(e.Parameters, "|")(1)
        psupplier = Split(e.Parameters, "|")(2)

        pHeader = True
        If pFunction = "gridload" Then
            If pVariant = "ALL" Then
                gs_FilterVariant = ""
            Else
                gs_FilterVariant = pVariant
            End If
            If psupplier = "ALL" Then
                gs_FilterSupplier = ""
            Else
                gs_FilterSupplier = psupplier
            End If

            up_GridLoad(gs_FilterVariant, gs_FilterSupplier)

            gs_FilterVariantIndex = cboVariant.SelectedIndex
            gs_FilterSupplierIndex = cboType.SelectedIndex
        Else
            gs_FilterVariant = "x"
            gs_FilterSupplier = "x"
            up_GridLoad(gs_FilterVariant, gs_FilterSupplier)
        End If

    End Sub


    Protected Sub BtnDownload_Click(sender As Object, e As EventArgs) Handles BtnDownload.Click
        'cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        Dim ErrMsg As String = ""
        'up_Excel(ErrMsg)
        up_ExcelGridAuto(ErrMsg)

        If ErrMsg <> "" Then
            cbExcel.JSProperties("cpmessage") = ErrMsg
        Else
            cbExcel.JSProperties("cpmessage") = "Download Excel Successfully"
        End If

    End Sub

End Class
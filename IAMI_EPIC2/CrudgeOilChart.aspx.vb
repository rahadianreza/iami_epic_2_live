﻿Imports DevExpress.XtraCharts
Imports System.Drawing
Imports DevExpress.XtraCharts.Web
Imports DevExpress.Web.ASPxCallbackPanel
Imports System.Data.SqlClient

Public Class CrudgeOilChart
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A090")
        Master.SiteTitle = "Crudge Oil Chart"
        pUser = Session("user")

      

        If Not Page.IsCallback And Not Page.IsPostBack Then
            Dim dfrom As Date
            dfrom = Year(Now) & "-01-01"
            TmPeriod_From.Value = dfrom
            TmPeriod_To.Value = Now
            
        End If
    End Sub

   

    Protected Sub ASPxCallbackPanel1_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel1.Callback
        Dim wbc As New WebChartControl()
        'Dim ds As New DataSet
        Dim pFunction As String = Split(e.Parameter, "|")(0)


        If pFunction = "show" Then
            Dim ds As DataSet = CreateData()
            Dim chart As WebChartControl = New WebChartControl()
            Dim series1 As Series = New Series("Crude Oil", ViewType.Spline)

            ' Create a line series. 
            CType(series1.View, PointSeriesView).PointMarkerOptions.Size = "2"
            'CType(chart.SeriesTemplate.View, SplineSeriesView).LineTensionPercent = "0"
            'Dim view As PointSeriesView = CType(chart.SeriesTemplate.View, PointSeriesView)
            'view.PointMarkerOptions.Kind = MarkerKind.Circle
            'view.PointMarkerOptions.Size = 2

            chart.DataSource = ds
            'chart.DataMember = "Mst_CrudgeOil"
            chart.Series.Add(series1)

            Dim chartTitle1 As New ChartTitle()
            ' Define the text for the titles. 
            chartTitle1.Text = "<b>Crudge Oil</b>"
            chartTitle1.Alignment = StringAlignment.Center
            chartTitle1.Dock = ChartTitleDockStyle.Top
            chartTitle1.Antialiasing = True
            chartTitle1.Font = New Font("Segoe UI", 14, FontStyle.Bold)
            chart.Titles.AddRange(New ChartTitle() {chartTitle1})

            series1.ArgumentDataMember = "Period"
            series1.ValueDataMembers(0) = "Price"
            'Me.Controls.Add(chart)
            chart.DataBind()
            chart.Width = 1000
            chart.Height = 400

            CType(sender, ASPxCallbackPanel).Controls.Add(chart)

           

            '        ' Create a line series. 
            '        Dim series1 As New Series("Series 1", ViewType.Line)
            '        CType(series1.View, LineSeriesView).Color = Color.MediumSeaGreen  '.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True
            '        CType(series1.View, LineSeriesView).LineMarkerOptions.Kind = MarkerKind.Circle
            '        CType(series1.View, LineSeriesView).LineStyle.Thickness = "1"
            '        CType(series1.Label, PointSeriesLabel).BackColor = Color.DarkOrange

            '        Dim chartTitle1 As New ChartTitle()
            '        ' Dim chartTitle2 As New ChartTitle()

          

            '        'chartTitle2.Text = "*UMR = Upah Minimum Regional(Minimum Regional Wage) <br>*UMSP = Upah Minimum Sektoral Provinsi (Minimum Province Sectoral Wage)  "
            '        'chartTitle2.Alignment = StringAlignment.Center
            '        'chartTitle2.Dock = ChartTitleDockStyle.Bottom
            '        'chartTitle2.Font = New Font("Segoe UI", 9)

            '        ' wbc.SmallChartText.Text = "Labour Cost"
            '        'create chart bar
            '        'ChartControl1.Titles(0).Text = "Test"
            '        wbc.Series.Add(series1)
            '        wbc.SeriesDataMember = "Price"
            '        wbc.SeriesTemplate.ArgumentDataMember = "Period"
            '        wbc.SeriesSorting = SortingMode.Ascending
            '        wbc.SeriesTemplate.ValueDataMembers.Item(0) = "Price"
            '        wbc.PaletteName = "Office 2013"
            '        wbc.Titles.AddRange(New ChartTitle() {chartTitle1})


            '        wbc.Series(0).ArgumentDataMember = "Period"
            '        wbc.Series(0).SeriesPointsSorting = SortingMode.Ascending
            '        'wbc.Series(0).Name = "Estimated UMSP Level"
            '        'wbc.Series(0).SummaryFunction = "AVERAGE([EstimatedUMSP])"
            '        wbc.Series(0).ValueDataMembers.Item(0) = "Price"

            '        wbc.Series(0).ArgumentScaleType = ScaleType.Qualitative
            '        wbc.Series(0).CrosshairEnabled = DevExpress.Utils.DefaultBoolean.True
            '        wbc.Series(0).CrosshairLabelVisibility = DevExpress.Utils.DefaultBoolean.True


            '        'position description city/region
            '        wbc.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Center
            '        wbc.Legend.AlignmentVertical = LegendAlignmentVertical.BottomOutside
            '        wbc.Legend.Direction = LegendDirection.LeftToRight
            '        wbc.DataBind()


            '        'wbc.Series.Add(New Series("Series", ViewType.Line))
            '        ' wbc.Series(0).ArgumentScaleType = ScaleType.DateTime
            '        'wbc.Series(0).ValueScaleType = ScaleType.Numerical
            '        wbc.Width = 1000
            '        wbc.Height = 500

            '        CType(sender, ASPxCallbackPanel).Controls.Add(wbc)

            '        '' Create a chart.
            '        ' Dim WebChartControl As WebChartControl = New WebChartControl()

            '        ' ' Note that a chart isn't initialized until it's added to the form's collection of controls.
            '        ' 'Me.Form.Controls.Add(WebChartControl)
            '        ' ' Create a new bar series.
            '        ' Dim series As Series = New Series("Series 1", ViewType.Bar)
            '        ' ' Add the series to the chart.
            '        ' WebChartControl.Series.Add(series)
            '        ' ' Specify the series data source.
            '        ' Dim seriesData As DataSet = ds
            '        ' series.DataSource = seriesData
            '        ' ' Specify an argument data member.
            '        ' series.ArgumentDataMember = "Period"
            '        ' ' Specify a value data member.
            '        ' series.ValueDataMembers.Item(0) = "Price"


            '        ' CType(sender, ASPxCallbackPanel).Controls.Add(WebChartControl)


        End If

        ' Dim r As New Random()
        'For i As Integer = 0 To 4
        '    wbc.Series(0).Points.Add(New SeriesPoint(DateTime.Today.AddDays(i), _
        '    (CInt(Fix((r.NextDouble() * 100) * 10))) / 10.0))
        'Next i


    End Sub

    Private Function CreateData() As DataSet
        Dim ds As New DataSet
        Dim pErr As String = ""
        Dim PeriodFrom, PeriodTo
        Try
            PeriodFrom = Format(TmPeriod_From.Value, "MM") + Format(TmPeriod_From.Value, "yyyy")
            PeriodTo = Format(TmPeriod_To.Value, "MM") + Format(TmPeriod_To.Value, "yyyy")

            Using con As New SqlConnection(Sconn.Stringkoneksi)
                Dim cmd As New SqlCommand("sp_Mst_CrudgeOil_Chart", con)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@PeriodFrom", PeriodFrom)
                cmd.Parameters.AddWithValue("@PeriodTo", PeriodTo)
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ds)
                Return ds
            End Using

        Catch ex As Exception
            pErr = ex.Message
            Return Nothing
        End Try



        'Dim ds As DataSet = New DataSet()
        'Dim dt As DataTable = New DataTable() With {
        '    .TableName = "Table"
        '}
        'ds.Tables.Add(dt)
        'dt.Columns.Add("Argument")
        'dt.Columns.Add("Value", GetType(Double))
        'dt.Rows.Add("Russia", 17.0752)
        'dt.Rows.Add("Canada", 9.98467)
        'dt.Rows.Add("USA", 9.63142)
        'dt.Rows.Add("China", 9.59696)
        'dt.Rows.Add("Brazil", 8.511965)
        'dt.Rows.Add("Australia", 7.68685)
        'dt.Rows.Add("India", 3.28759)
        'dt.Rows.Add("Others", 81.2)

    End Function

   

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("CrudgeOil.aspx")
    End Sub
End Class
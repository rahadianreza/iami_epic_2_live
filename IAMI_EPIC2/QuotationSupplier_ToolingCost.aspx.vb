﻿Imports DevExpress.XtraPrinting
Imports DevExpress.Web.ASPxGridView
Imports System.IO
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxClasses
Imports System.Drawing

Public Class QuotationSupplier_ToolingCost
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim fRefresh As Boolean = False
    Dim statusAdmin As String
    Dim pErr As String = ""

    Dim prj As String = ""
    Dim grp As String = ""
    Dim comm As String = ""
    Dim py As String = ""
    Dim pty As String = ""
    Dim sDS As String = ""
    Dim pg As String = ""
    Dim supp As String = ""
    Dim grpcommodity As String = ""
    Dim pic As String = ""
    Dim status As String = "0"
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("K010")
        Master.SiteTitle = "Quotation Supplier Tooling Cost"
        pUser = Session("user")
        show_error(MsgTypeEnum.Info, "", 0)
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        prj = Request.QueryString("projectid")
        grp = Request.QueryString("groupid")
        comm = Request.QueryString("commodity")
        pty = Request.QueryString("projecttype")
        pg = Request.QueryString("page")
        supp = Request.QueryString("supplier")
        grpcommodity = Request.QueryString("groupcommodity")
        pic = Request.QueryString("pic")
        status = Request.QueryString("status")

        Dim script As String = ""
        'If status = "2" Or status = "4" Then 'accept
        If status = "0" Then
            script = "txtInterest.SetEnabled(true);" & vbCrLf & _
                   "txtDepreciationPeriodYear.SetEnabled(true);" & vbCrLf & _
                   "txtTotalInterest.SetEnabled(true);" & vbCrLf & _
                   "cboType.SetEnabled(true);" & vbCrLf & _
                   "txtQtyPerUnit.SetEnabled(true);" & vbCrLf & _
                   "txtProjectProdUnit.SetEnabled(true);" & vbCrLf & _
                   "txtDepreciationPeriod.SetEnabled(true);"
            ScriptManager.RegisterStartupScript(txtInterest, txtInterest.GetType(), "txtInterest", script, True)
            ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "btnSubmit", "btnSubmit.SetEnabled(true);", True)

            txtDepreciationPeriodYearAfter.Visible = False
            txtQtyPerUnitAfter.Visible = False
            txtProjectProdUnitAfter.Visible = False

        Else
            txtDepreciationPeriodYearAfter.Visible = True
            txtQtyPerUnitAfter.Visible = True
            txtProjectProdUnitAfter.Visible = True

            script = "txtInterest.SetEnabled(false);" & vbCrLf & _
                     "txtDepreciationPeriodYear.SetEnabled(false);" & vbCrLf & _
                     "txtTotalInterest.SetEnabled(false);" & vbCrLf & _
                     "cboType.SetEnabled(false);" & vbCrLf & _
                     "txtQtyPerUnit.SetEnabled(false);" & vbCrLf & _
                     "txtProjectProdUnit.SetEnabled(false);" & vbCrLf & _
                     "txtDepreciationPeriod.SetEnabled(false);" & vbCrLf & _
                     "txtDepreciationPeriodYearAfter.SetEnabled(false);" & vbCrLf & _
                     "txtQtyPerUnitAfter.SetEnabled(false);" & vbCrLf & _
                     "txtProjectProdUnitAfter.SetEnabled(false);"
            ScriptManager.RegisterStartupScript(txtInterest, txtInterest.GetType(), "txtInterest", script, True)
            ScriptManager.RegisterStartupScript(btnSubmit, btnSubmit.GetType(), "btnSubmit", "btnSubmit.SetEnabled(false);", True)
       

        End If

        sdsPartNo.SelectCommand = "SELECT Purchased_Part_Code, Purchased_Part_Name FROM Mst_Purchased_Part"

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If Not String.IsNullOrEmpty(prj) Then
                Dim ds As DataSet = clsPRJListDB.getDetailGrid(prj, pUser, "")

                lblUSDRate.Text = Format(CDec(ds.Tables(0).Rows(0)("Rate_USD_IDR").ToString()), "#,##0.00")
                lblJPYRate.Text = Format(CDec(ds.Tables(0).Rows(0)("Rate_YEN_IDR").ToString()), "#,##0.00")
                lblTHBRate.Text = Format(CDec(ds.Tables(0).Rows(0)("Rate_BATH_IDR").ToString()), "#,##0.00")
            End If

            up_FillComboPartNo(cboPartNo, prj, grp, comm)
        End If



    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_Excel()
        up_GridLoad("1")

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)
        'link1.Component = GridExporter

        Dim compositeLink As New DevExpress.XtraPrintingLinks.CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=ItemListSourcing" & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

    Private Sub up_FillComboPartNo(cbo As DevExpress.Web.ASPxEditors.ASPxComboBox, _projid As String, _grpid As String, _comm As String,
                           Optional ByRef pGroup As String = "", _
                           Optional ByRef pParent As String = "", _
                           Optional ByRef pErr As String = "")

        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = clsQuotationSupplierDB.fillComboPartNo(_projid, _grpid, _comm, grpcommodity, supp, pmsg)
        If pmsg = "" Then
            cbo.DataSource = ds
            cbo.DataBind()
        End If
    End Sub

    Private Sub up_GridLoad(ByVal a As String)
        Dim ErrMsg As String = ""
        Dim Ses As DataSet
        Ses = clsQuotationSupplierDB.getHeaderToolingCost(prj, grp, comm, cboPartNo.Value, supp, ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()

        End If
    End Sub
#End Region

#Region "Event Control"
    Private Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If fRefresh = False Then
            up_GridLoad("1")
        End If
        fRefresh = False

        Dim _type As String = status
        Dim ds As DataSet = clsQuotationSupplierDB.getHeaderToolingCost_Data(IIf(String.IsNullOrEmpty(prj), "", prj), IIf(String.IsNullOrEmpty(grp), "", grp) _
                                                                             , IIf(String.IsNullOrEmpty(comm), "", comm), IIf(String.IsNullOrEmpty(cboPartNo.Value), "", cboPartNo.Value) _
                                                                             , _type, IIf(String.IsNullOrEmpty(supp), "", supp))
        If ds.Tables(0).Rows.Count > 0 Then
            Grid.JSProperties("cp_Status") = status
            Grid.JSProperties("cp_interest") = ds.Tables(0).Rows(0)("Interest_Year")
            Grid.JSProperties("cp_Depreciation_Period") = ds.Tables(0).Rows(0)("Depreciation_Period")
            Grid.JSProperties("cp_Depreciation_Period_After") = ds.Tables(0).Rows(0)("Depreciation_Period_After")

            Grid.JSProperties("cp_Total_Interest") = ds.Tables(0).Rows(0)("Total_Interest")
            Grid.JSProperties("cp_Qty_PerUnit") = ds.Tables(0).Rows(0)("Qty_PerUnit")
            Grid.JSProperties("cp_Qty_PerUnit_After") = ds.Tables(0).Rows(0)("Qty_PerUnit_After")

            Grid.JSProperties("cp_Project_Prod_unit_Month") = ds.Tables(0).Rows(0)("Project_Prod_unit_Month")
            Grid.JSProperties("cp_Project_Prod_unit_Month_After") = ds.Tables(0).Rows(0)("Project_Prod_unit_Month_After")

            Grid.JSProperties("cp_Total_Depreciation_Period") = ds.Tables(0).Rows(0)("Total_Depreciation_Period")
            Grid.JSProperties("cp_Total_Amount_Interest") = Decimal.Parse(ds.Tables(0).Rows(0)("Total_Amount_Interest")).ToString("#,###")
            Grid.JSProperties("cp_Total_Tooling_Cost") = Decimal.Parse(ds.Tables(0).Rows(0)("Total_Tooling_Cost")).ToString("#,###")
            Grid.JSProperties("cp_Decpreciation_Cost") = Decimal.Parse(ds.Tables(0).Rows(0)("Decpreciation_Cost")).ToString("#,###")
            Grid.JSProperties("cp_Toolingtype") = ds.Tables(0).Rows(0)("Type")
            Grid.JSProperties("cp_Total") = clsQuotationSupplierDB.getHeaderToolingCost_amount(prj, grp, comm, cboPartNo.Value, supp, pErr)
        Else
            Grid.JSProperties("cp_Status") = status
            Grid.JSProperties("cp_interest") = 0
            Grid.JSProperties("cp_Depreciation_Period") = 0
            Grid.JSProperties("cp_Depreciation_Period_After") = 0
            Grid.JSProperties("cp_Total_Interest") = 0
            Grid.JSProperties("cp_Qty_PerUnit") = 0
            Grid.JSProperties("cp_Qty_PerUnit_After") = 0
            Grid.JSProperties("cp_Project_Prod_unit_Month") = 0
            Grid.JSProperties("cp_Project_Prod_unit_Month_After") = 0
            Grid.JSProperties("cp_Total_Amount_Interest") = 0
            Grid.JSProperties("cp_Total_Tooling_Cost") = 0
            Grid.JSProperties("cp_Decpreciation_Cost") = 0
            Grid.JSProperties("cp_Toolingtype") = 0
            Grid.JSProperties("cp_Total") = clsQuotationSupplierDB.getHeaderToolingCost_amount(prj, grp, comm, cboPartNo.Value, supp, pErr)
        End If
    End Sub

    Private Sub Grid_BeforeGetCallbackResult(sender As Object, e As System.EventArgs) Handles Grid.BeforeGetCallbackResult
        If Grid.IsNewRowEditing Then
            Grid.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim ds As New DataSet
        Dim pFunction As String = e.Parameters

        If pFunction = "refresh" Then
            up_GridLoad("1")
            ds = ClsPRListDB.GetList("", "", "", "", "", "")

            Grid.DataSource = ds
            Grid.DataBind()
            'Grid.Columns.Item("Status").Visible = False
            fRefresh = True
        Else

            up_GridLoad("1")
        End If
    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()
        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub Grid_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles Grid.RowDeleting
        e.Cancel = True
        'prj = Request.QueryString("projectid")
        'grp = Request.QueryString("groupid")
        'comm = Request.QueryString("commodity")

        clsQuotationSupplierDB.DeleteQuotationSupplierToolingCost(prj, grp, comm, cboPartNo.Value, e.Keys("Tooling_Name"), supp, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
        End If

        Grid.JSProperties("cp_Total") = clsQuotationSupplierDB.getHeaderToolingCost_amount(prj, grp, comm, cboPartNo.Value, supp, pErr)
    End Sub

    Public Function checking(value As String) As Boolean
        If String.IsNullOrEmpty(value) Or value = "0" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub Grid_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles Grid.RowInserting
        e.Cancel = True
        Dim aFlag As String = 0

        If e.NewValues("AdjustmentType") = "F" Then
            If checking(prj) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
            ElseIf checking(grp) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
            ElseIf checking(comm) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Commodity cannot empty", 1)
            ElseIf checking(cboPartNo.Value) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Part No cannot empty", 1)
            ElseIf checking(e.NewValues("Tooling_Name")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Tooling Name cannot empty", 1)
            ElseIf checking(e.NewValues("Mfg_Cost")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Mfg Cost cannot empty", 1)
            ElseIf checking(e.NewValues("Administrative_Expense")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Administrative Expense cannot empty", 1)
            Else
                aFlag = 1
            End If
        Else
            If checking(prj) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
            ElseIf checking(grp) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
            ElseIf checking(comm) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Commodity cannot empty", 1)
            ElseIf checking(cboPartNo.Value) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Part No cannot empty", 1)
            ElseIf checking(e.NewValues("Tooling_Name")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Tooling Name cannot empty", 1)
            ElseIf checking(e.NewValues("ToolCost")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Tooling Cost cannot empty", 1)
            Else
                aFlag = 1
            End If
        End If

        If aFlag = 1 Then
            clsQuotationSupplierDB.InsertQuotationSupplierToolingCost(prj, grp, comm, cboPartNo.Value, supp, e.NewValues("Tooling_Name"), e.NewValues("Mfg_Cost"), e.NewValues("Administrative_Expense"), e.NewValues("Remarks"), e.NewValues("AdjustmentType"), e.NewValues("Currency"), grpcommodity, e.NewValues("ToolCost"), supp, pErr)
            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Insert data successfully!", 1)
                up_FillComboPartNo(cboPartNo, prj, grp, comm)
            End If
        Else
            Grid.JSProperties("cp_AdjustmentType") = e.NewValues("AdjustmentType")
        End If

        Grid.JSProperties("cp_Total") = clsQuotationSupplierDB.getHeaderToolingCost_amount(prj, grp, comm, cboPartNo.Value, supp, pErr)

    End Sub

    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        Dim aFlag As String = 0

        If e.NewValues("AdjustmentType") = "F" Then
            If checking(prj) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
            ElseIf checking(grp) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
            ElseIf checking(comm) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Commodity cannot empty", 1)
            ElseIf checking(cboPartNo.Value) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Part No cannot empty", 1)
            ElseIf checking(e.NewValues("Tooling_Name")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Tooling Name cannot empty", 1)
            ElseIf checking(e.NewValues("Mfg_Cost")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Mfg Cost cannot empty", 1)
            ElseIf checking(e.NewValues("Administrative_Expense")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Administrative Expense cannot empty", 1)
            Else
                aFlag = 1
            End If
        Else
            If checking(prj) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
            ElseIf checking(grp) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
            ElseIf checking(comm) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Commodity cannot empty", 1)
            ElseIf checking(cboPartNo.Value) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Part No cannot empty", 1)
            ElseIf checking(e.NewValues("Tooling_Name")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Tooling Name cannot empty", 1)
            ElseIf checking(e.NewValues("ToolCost")) = False Then
                aFlag = 0
                show_error(MsgTypeEnum.Warning, "Tooling Cost cannot empty", 1)
            Else
                aFlag = 1
            End If
        End If

        If aFlag = 1 Then
            clsQuotationSupplierDB.UpdateQuotationSupplierToolingCost(prj, grp, comm, cboPartNo.Value, supp, e.NewValues("Tooling_Name"), e.NewValues("Mfg_Cost"), e.NewValues("Administrative_Expense"), e.NewValues("Remarks"), e.NewValues("AdjustmentType"), e.NewValues("Currency"), grpcommodity, e.NewValues("ToolCost"), supp, pErr)
            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
            End If
        Else
            Grid.JSProperties("cp_AdjustmentType") = e.NewValues("AdjustmentType")
        End If

        Grid.JSProperties("cp_Total") = clsQuotationSupplierDB.getHeaderToolingCost_amount(prj, grp, comm, cboPartNo.Value, supp, pErr)

    End Sub

    Private Sub Grid_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If Not Grid.IsNewRowEditing Then
            If e.Column.FieldName = "Tooling_Name" Or e.Column.FieldName = "AdjustmentType" Then
                e.Editor.ClientEnabled = False
                e.Editor.ForeColor = Color.Silver
            End If
        End If

        Dim getAdjustmentType As String = Grid.GetRowValues(e.VisibleIndex, "AdjustmentType")

        If getAdjustmentType = "M" Then
            If e.Column.FieldName = "Administrative_Expense" Or e.Column.FieldName = "Mfg_Cost" Then
                e.Editor.Visible = False
                e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
                e.Column.EditFormSettings.Caption = ""
                e.Column.EditCellStyle.CssClass = "Hidden"
                e.Column.EditFormCaptionStyle.CssClass = "Hidden"
            End If
            If e.Column.FieldName = "ToolCost" Then
                e.Editor.Visible = True
                e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True
                e.Column.EditCellStyle.CssClass = "AmountVisible"
                e.Column.EditFormCaptionStyle.CssClass = "AmountVisible"
            End If
        End If
    End Sub

#End Region

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If checking(prj) = False Then
            show_error(MsgTypeEnum.Warning, "Project ID cannot empty", 1)
        ElseIf checking(grp) = False Then
            show_error(MsgTypeEnum.Warning, "Group ID cannot empty", 1)
        ElseIf checking(comm) = False Then
            show_error(MsgTypeEnum.Warning, "Commodity cannot empty", 1)
        ElseIf checking(cboPartNo.Value) = False Then
            show_error(MsgTypeEnum.Warning, "Part No cannot empty", 1)
        Else
            clsQuotationSupplierDB.InsertQuotationSupplierToolingCost_Header(prj, grp, comm, cboPartNo.Value, supp, txtInterest.Text.Replace(",", ""), txtDepreciationPeriodYear.Text.Replace(",", ""), _
                                                                             txtTotalInterest.Text, txtQtyPerUnit.Text.Replace(",", ""), txtProjectProdUnit.Text.Replace(",", ""), txtDepreciationPeriod.Text.Replace(",", ""), _
                                                                              IIf(txtTotalAmountInterest.Text = "", 0, txtTotalAmountInterest.Text), IIf(txtTotalToolingCost.Text = "", 0, txtTotalToolingCost.Text), IIf(txtDepreciationCost.Text = "", 0, txtDepreciationCost.Text), grpcommodity, cboType.Value, supp, pErr)

            If pErr <> "" Then
                show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
            Else
                Grid.CancelEdit()
                show_error(MsgTypeEnum.Success, "Saved data successfully!", 1)
                up_FillComboPartNo(cboPartNo, prj, grp, comm)
            End If
        End If

    End Sub

    Private Sub Grid_CommandButtonInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewCommandButtonEventArgs) Handles Grid.CommandButtonInitialize
        If (clsQuotationSupplierDB.checkingStatus(prj, grp, comm, cboPartNo.Value, supp, grpcommodity) <> "2" And clsQuotationSupplierDB.checkingStatusComplete(prj, grp, comm, cboPartNo.Value, supp, grpcommodity) = "1") Or pg = "acc" Then

            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Edit Then
                e.Visible = False
            End If
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.Delete Then
                e.Visible = False
            End If
            If e.ButtonType = DevExpress.Web.ASPxGridView.ColumnCommandButtonType.New Then
                e.Visible = False
            End If

        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        If pg = "acc" Then
            Response.Redirect("QuotationSupplier_Acceptance.aspx?projectid=" + prj + "&groupid=" + grp + "&commodity=" + comm + "&projecttype=" + pty + "&supplier=" + supp + "&groupcomodity=" + grpcommodity + "&pic=" + pic)
        Else
            Response.Redirect("QuotationSupplier.aspx?projectid=" + prj + "&groupid=" + grp + "&commodity=" + comm + "&projecttype=" + pty + "&supplier=" + pUser + "&groupcomodity=" + grpcommodity + "&pic=" + pic)
        End If
    End Sub

    Private Sub Grid_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs) Handles Grid.StartRowEditing
        If (Not Grid.IsNewRowEditing) Then
            Grid.DoRowValidation()
        End If
    End Sub
End Class
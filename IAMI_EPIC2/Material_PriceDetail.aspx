﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Material_PriceDetail.aspx.vb" 
Inherits="IAMI_EPIC2.Material_PriceDetail" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxImageZoom" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    function MessageError(s, e) {
        if (s.cpMessage == "Data Saved Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;


            //alert(s.cpButtonText);
            btnSubmit.SetText(s.cpButtonText);
            cboMaterialCode.SetEnabled(false);
            cboPriceType.SetEnabled(false);
            cboSupplier.SetEnabled(false);
            dtPeriod.SetEnabled(false);
            
        }
       
        else if (s.cpMessage == "Data Updated Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            //window.location.href = "/Labor.aspx";
            millisecondsToWait = 2000;
            setTimeout(function () {
                var pathArray = window.location.pathname.split('/');
                var url = window.location.origin + '/' + pathArray[1] + '/Material_Price.aspx';
                //window.location.href = url;
                //window.location.href = "http://" + window.location.host + "/ItemList.aspx";
                if (pathArray[1] == "Material_PriceDetail.aspx") {
                    window.location.href = window.location.origin + '/Material_Price.aspx';
                }
                else {
                    window.location.href = url;
                }
            }, millisecondsToWait);

        }
        else if (s.cpMessage == "Data Clear Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;

            cboMaterialCode.SetEnabled(true);
            cboPriceType.SetEnabled(true);
            cboSupplier.SetEnabled(true);
            dtPeriod.SetEnabled(true);

            cboMaterialCode.SetText('');
            cboPriceType.SetText('');
            cboSupplier.SetText('');
            txtMaterialType.SetText('');
            //var today = new Date().toLocaleDateString();
            //var year = day.getFullYear();
            var d = new Date();
            var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
            dtPeriod.SetDate(myDate);
            //dtPeriod.SetText(today);
            cboCurrency.SetText('');
            txtPrice.SetText('0');
            
        }
        else if (s.cpMessage == "Data Cancel Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == "Data Deleted Successfully!") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }


    //filter combobox Supplier berdasarkan price type yang dipilih
    function FilterSupplier() {
        cboSupplier.PerformCallback('filter|' + cboPriceType.GetValue());
    }

    function FilterMaterialCode() {
        txtMaterialCode.SetText(cboMaterialCode.GetText());
        cboMaterialCode.PerformCallback(cboMaterialCode.GetValue());
        
        
    }

    function GetValueMaterialType(s, e) {
        //alert(s.cpGetValue1);
        cboMaterialCode.SetText(s.cpGetValue1);
        txtMaterialType.SetText(s.cpGetValue2);
    }

     function SubmitProcess(s, e) {
        if (s.GetText() == 'Update') {
           
            if (cboCurrency.GetText() == '') {
                toastr.warning('Please Select Currency !', 'Warning');
                cboCurrency.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (txtPrice.GetText() == '' || txtPrice.GetText() == 0) {
                toastr.warning('Price must greater than zero', 'Warning');
                txtPrice.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
           
        }
        else {
            if (cboMaterialCode.GetText() == '') {
                toastr.warning('Please Select Material Code !', 'Warning');
                cboMaterialCode.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            if (cboPriceType.GetText() == '') {
                toastr.warning('Please Select Price Type Code !', 'Warning');
                cboMaterialType.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            if (cboSupplier.GetText() == '') {
                toastr.warning('Please Select Supplier Code !', 'Warning');
                cboSupplier.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
                       
            if (cboCurrency.GetText() == '') {
                toastr.warning('Please Select Currency !', 'Warning');
                cboCurrency.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }

            if (txtPrice.GetText() == '' || txtPrice.GetText() == 0) {
                toastr.warning('Price must greater than zero', 'Warning');
                txtPrice.Focus();
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
        }

        if (btnSubmit.GetText() != 'Update') {
            cbSave.PerformCallback('Save');
        } else {
            cbSave.PerformCallback('Update');

        }
    }

    //popup calender month year
    function formatDateISO(date) {
        var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function Period_OnInit(s, e) {
        var calendar = s.GetCalendar();
        calendar.owner = s;
        //calendar.SetVisible(false);
        calendar.GetMainElement().style.opacity = '0';

        var d = new Date();
        var myDate = new Date(d.getFullYear(), d.getMonth(), 1);
        dtPeriod.SetDate(myDate);
    }

    function Period_OnDropDown(s, e) {
        var calendar = s.GetCalendar();
        var fastNav = calendar.fastNavigation;
        fastNav.activeView = calendar.GetView(0, 0);
        fastNav.Prepare();
        fastNav.GetPopup().popupVerticalAlign = "Below";
        fastNav.GetPopup().ShowAtElement(s.GetMainElement())

        fastNav.OnOkClick = function () {
            var parentDateEdit = this.calendar.owner;
            var currentDate = new Date(fastNav.activeYear, fastNav.activeMonth, 1);
            parentDateEdit.SetDate(currentDate);
            parentDateEdit.HideDropDown();

        }

        fastNav.OnCancelClick = function () {
            var parentDateEdit = this.calendar.owner;
            parentDateEdit.HideDropDown();
        }
    }

    </script>
    <style type="text/css">
        .td-col-l 
        {
            padding:0px 0px 0px 10px;
            width:120px;
        }
        .td-col-m
        {
            width:20px;
        }
        .td-col-r
        {
            width:100px;
        }
        
        .tr-height
        {
            height: 35px;
        }
            
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 100%;">
        <tr style="height:10px">
            <td colspan="7"></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Material Code">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboMaterialCode" runat="server" ClientInstanceName="cboMaterialCode" DataSourceID ="SqlDataSource4"
                    Width="170px" Font-Names="Segoe UI" TextField="Material_Name" ValueField="Material_Code"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                    <ClientSideEvents SelectedIndexChanged="FilterMaterialCode" EndCallback="GetValueMaterialType"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Material_Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Material_Name" Width="120px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="select Material_Code,Material_Name from mst_material">
                </asp:SqlDataSource>
           
            </td>
            <td class="td-col-m"></td>
            <td style="width:250px">
                <dx:ASPxTextBox ID="txtMaterialType" runat="server" Width="170px" Height="25px" ClientInstanceName="txtMaterialType"
                    MaxLength="100" AutoCompleteType="Disabled" ReadOnly="true" BackColor="LightGray">
                </dx:ASPxTextBox>
            </td>
            <td></td>
        </tr>
      
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Price Type">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m">
                &nbsp;
            </td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboPriceType" runat="server" ClientInstanceName="cboPriceType"
                    Width="170px" Font-Names="Segoe UI" TextField="PriceTypeDesc" ValueField="PriceType"
                    DataSourceID="SqlDataSource1" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                    Height="22px">
                    
                    <ClientSideEvents SelectedIndexChanged="FilterSupplier"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="PriceType" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="PriceTypeDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="select Par_Code AS PriceType,Par_Description AS PriceTypeDesc from Mst_Parameter where Par_Group='MaterialPriceType'">
                </asp:SqlDataSource>
            </td>
            <td></td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Supplier Code">
                </dx:ASPxLabel>
            </td>
            <td style="width: 20px">
                &nbsp;
            </td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboSupplier" runat="server" ClientInstanceName="cboSupplier" 
                    Width="170px" Font-Names="Segoe UI" TextField="SupplierName" ValueField="SupplierCode"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="25px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="SupplierCode" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="SupplierName" Width="120px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>
               <%-- <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT SupplierCode,SupplierName FROM VW_Mst_Material_getSupplier">
                </asp:SqlDataSource>--%>
            </td>
            <td>
               
            </td>
        </tr>
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="lblPeriod" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Period">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxDateEdit ID="dtPeriod" runat="server" ClientInstanceName="dtPeriod" EnableTheming="True"
                    ShowShadow="false" Font-Names="Segoe UI" Theme="Office2010Black" Font-Size="9pt"
                    Height="25px" DisplayFormatString="MMM yyyy" EditFormatString="MMM yyyy" Width="120px">
                    <ClientSideEvents DropDown="Period_OnDropDown" Init="Period_OnInit" />
                </dx:ASPxDateEdit>
            </td>
        </tr>
        <td></td>
        
       
        <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Currency">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxComboBox ID="cboCurrency" runat="server" ClientInstanceName="cboCurrency"
                    Width="170px" Font-Names="Segoe UI" TextField="CurrencyDesc" ValueField="Currency" DataSourceID="SqlDataSource6"
                    TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black" DropDownStyle="DropDown"
                    IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" Height="22px">
                    <Columns>
                        <dx:ListBoxColumn Caption="Code" FieldName="Currency" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="CurrencyDesc" Width="250px" />
                    </Columns>
                    <ItemStyle Height="10px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ItemStyle>
                    <ButtonStyle Width="5px" Paddings-Padding="4px">
                        <Paddings Padding="4px"></Paddings>
                    </ButtonStyle>
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                    SelectCommand="SELECT RTRIM(Par_Code) Currency,RTRIM(Par_Description) CurrencyDesc FROM Mst_Parameter WHERE Par_Group = 'Currency'">
                </asp:SqlDataSource>
            </td>
          
            <td></td>
        </tr>
          <tr class="tr-height">
            <td class="td-col-l"></td>
            <td class="td-col-l">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Text="Price">
                </dx:ASPxLabel>
            </td>
            <td class="td-col-m"></td>
            <td class="td-col-r">
                <dx:ASPxTextBox ID="txtPrice" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
                    Width="120px" ClientInstanceName="txtPrice"  HorizontalAlign="Right">
                     <MaskSettings Mask="<0..100000000g>.<00..99>" />
                </dx:ASPxTextBox>
            </td>
            <td></td>
        </tr>
       <tr >
           <td class="td-col-l"></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>

       </tr>
        <tr style="height: 25px;">
            <td class="td-col-l"></td>
            <td class="td-col-l"></td>
            <td></td>
            <td colspan="3" style="width: 300px">
                <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnBack"
                    Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>

                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnClear" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
                &nbsp;&nbsp;
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnSubmit" Theme="Default">
                   
                    <ClientSideEvents Click="function(s, e) {
	var msg = confirm('Are you sure want to submit this data ?');                
                                if (msg == false) {
                                     e.processOnServer = false;
                                     return;
                                }
	SubmitProcess(s,e);
    
   
}" />
                   
                    <Paddings Padding="2px" />
                </dx:ASPxButton>
              <%--  <dx:ASPxButton ID="btnUpdate" runat="server" Text="Update" AutoPostBack="False" Font-Names="Segoe UI"
                    Font-Size="9pt" Width="80px" Height="25px" ClientInstanceName="btnUpdate" Theme="Default">
                    <Paddings Padding="2px" />
                </dx:ASPxButton>--%>

            </td>
            <td></td>
           
        </tr>
        <tr style="display:none">
            <td colspan="9">
               
            </td>
        </tr>
    </table>
    <div style="display:none">
        <dx:ASPxTextBox ID="txtMaterialCode" runat="server" Font-Names="Segoe UI" Font-Size="9pt"
            Width="80px" ClientInstanceName="txtMaterialCode" MaxLength="100">
        </dx:ASPxTextBox>
    </div>
    <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxCallback ID="cbTmp" runat="server" ClientInstanceName="cbTmp">
           <%-- <ClientSideEvents CallbackComplete="SetCode" />--%>
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbSave" runat="server" ClientInstanceName="cbSave">
            <ClientSideEvents  EndCallback="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbClear" runat="server" ClientInstanceName="cbClear">
            <ClientSideEvents Init="MessageError" />
        </dx:ASPxCallback>
        <dx:ASPxCallback ID="cbDelete" runat="server" ClientInstanceName="cbDelete">
           <%-- <ClientSideEvents CallbackComplete="MessageDelete" />--%>
        </dx:ASPxCallback>
    </div>
</asp:Content>

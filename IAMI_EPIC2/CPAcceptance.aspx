﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CPAcceptance.aspx.vb" Inherits="IAMI_EPIC2.CPAcceptance" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function FillComboCPNumber() {
        cboCPNumber.PerformCallback(dtCPFrom.GetText() + '|' + dtCPTo.GetText());
    }

    function GridLoad(s, e) {
        if (cboCPNumber.GetText() == '') {
            toastr.warning('Please select Counter Proposal Number first!', 'Warning');
            cboCPNumber.Focus();
            return false;
        }

        if (cboCPNumber.GetSelectedIndex() == -1) {
            toastr.warning('Invalid Counter Proposal Number!', 'Warning');
            cboCPNumber.Focus();
            return false;
        }

        var dtFrom = dtCPFrom.GetDate();
        var YearFrom = dtFrom.getFullYear();
        var MonthFrom = dtFrom.getMonth();
        var DateFrom = dtFrom.getDate();
        var dtQFrom = new Date(YearFrom, MonthFrom, DateFrom)

        var dtTo = dtCPTo.GetDate();
        var YearTo = dtTo.getFullYear();
        var MonthTo = dtTo.getMonth();
        var DateTo = dtTo.getDate();
        var dtQTo = new Date(YearTo, MonthTo, DateTo)

        if (dtQFrom > dtQTo) {
            toastr.warning('Counter Proposal Date From cannot be greater than Counter Proposal Date To!', 'Warning');
            dtCPFrom.Focus();
            return;
        }

        Grid.PerformCallback('load|' + dtCPFrom.GetText() + '|' + dtCPTo.GetText());
    }

    function downloadValidation(s, e) {
        if (Grid.GetVisibleRowsOnPage() == 0) {
            toastr.info('There is no data to download!', 'Info');
            e.processOnServer = false;
            return;
        }
    }

    function GridCustomButtonClick(s, e) {
        if (e.buttonID == 'det') {
            var rowKey = Grid.GetRowKey(e.visibleIndex);

            var jsDate = dtCPFrom.GetDate();
            var year = jsDate.getFullYear(); // where getFullYear returns the year (four digits)
            var month = jsDate.getMonth() + 1; // where getMonth returns the month (from 0-11)
            var day = jsDate.getDate();   // where getDate returns the day of the month (from 1-31)
            var myDateFrom = year + "-" + month + "-" + day;

            jsDate = dtCPTo.GetDate();
            year = jsDate.getFullYear(); // where getFullYear returns the year (four digits)
            month = jsDate.getMonth() + 1; // where getMonth returns the month (from 0-11)
            day = jsDate.getDate();   // where getDate returns the day of the month (from 1-31)
            var myDateTo = year + "-" + month + "-" + day;

            var rowPos = Grid.GetVisibleRowsOnPage();

            //<12 Parameters>
            //0 : CPNumber
            //1 : Rev
            //2 : CENumber
            window.location.href = 'CPAcceptanceDetail.aspx?ID=' + rowKey;
        }
    }



    function LoadCompleted(s, e) {
        if (s.cpType == "0") {
            //INFO
            toastr.info(s.cpMessage, 'Information');

        } else if (s.cpType == "1") {
            //SUCCESS
            toastr.success(s.cpMessage, 'Success');

        } else if (s.cpType == "2") {
            //WARNING
            toastr.warning(s.cpMessage, 'Warning');

        } else if (s.cpType == "3") {
            //ERROR
            toastr.error(s.cpMessage, 'Error');

        }
    }
</script>

<style type="text/css">
    .colwidthbutton
    {
        width: 90px;
    }
    
    .rowheight
    {
        height: 35px;
    }
       
    .col1
    {
        width: 10px;
    }
    .colLabel1
    {
        width: 153px;
    }
    .colLabel2
    {
        width: 150px;
    }
    .colInput1
    {
        width: 133px;
    }
    .colInput2
    {
        width: 133px;
    }
    .colSpace
    {
        width: 100px;
    }
        
    .customHeader {
        height: 20px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">
            <tr>
                <td colspan="9" style="height: 10px">
                </td>
            </tr>
            <tr class="rowheight">
                <td class="col1">
                    </td>
                <td class="colLabel1">
                    <dx1:aspxlabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Counter Proposal Date"></dx1:aspxlabel>
                </td>
                <td class="colInput1">
                    <dx:aspxdateedit ID="dtCPFrom" runat="server" Theme="Office2010Black" 
                        Width="120px" ClientInstanceName="dtCPFrom"
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px" EditFormat="Custom">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        <ClientSideEvents ValueChanged="FillComboCPNumber" />
                    </dx:aspxdateedit>
                </td>
                <td style="width:20px">
                    <dx1:aspxlabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="~"></dx1:aspxlabel>
                </td>
                <td class="colInput1">
                    <dx:aspxdateedit ID="dtCPTo" runat="server" Theme="Office2010Black" 
                        Width="120px" AutoPostBack="false" ClientInstanceName="dtCPTo"
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        <ClientSideEvents ValueChanged="FillComboCPNumber" />
                    </dx:aspxdateedit> 
                </td>
                <td class="colSpace">
                    </td>
                <td class="colLabel2">
                    
                </td>
                <td class="colInput2">
                    
                </td>
                <td>
                    </td>
            </tr>
            <tr class="rowheight">
                <td>
                    </td>
                <td>
                    <dx1:aspxlabel ID="ASPxLabel5" runat="server" Font-Names="Segoe UI" Font-Size="9pt" Text="Counter Proposal Number"></dx1:aspxlabel>
                </td>
                <td colspan="3">
                    <dx1:ASPxComboBox ID="cboCPNumber" runat="server" ClientInstanceName="cboCPNumber"
                        Width="210px" Font-Names="Segoe UI"  TextField="CP_Number"
                        ValueField="CP_Number" TextFormatString="{0}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">
                        <ClientSideEvents Init="FillComboCPNumber" Validation="" EndCallback="LoadCompleted" />
                        <Columns>            
                            <dx:ListBoxColumn Caption="Counter Proposal Number" FieldName="CP_Number" Width="100px" />                       
                        </Columns>
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
                            <Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx1:ASPxComboBox>
                </td>
                <td>
                    </td>
                <td> 
                    
                </td>
                <td>
                    
                </td>
                <td>
                    </td>
            </tr>
            <tr style="height:10px">
                <td colspan="9">
                    </td>
               </tr>
            <tr class="rowheight">
                <td></td>
                <td colspan="8">
                    <table>
                        <tr class="rowheight">
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnShow" runat="server" Text="Show Data" UseSubmitBehavior="False"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                                    ClientInstanceName="btnShow" Theme="Default">                        
                                    <ClientSideEvents Click="GridLoad" />
                                    <Paddings Padding="2px" />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="false"
                                    Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                                    ClientInstanceName="btnExcel" Theme="Default">                        
                                    <Paddings Padding="2px" />
                                    <ClientSideEvents
                                        Click="downloadValidation"
                                    />
                                </dx:ASPxButton>
                            </td>
                            <td class="colwidthbutton">
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 10px">
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td> 
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    </td>
                <td>
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                </td>
            </tr>
        </table>
    </div>
   <div style="padding: 5px 5px 5px 5px">
        <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" Width="100%" 
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="KeyField" >
            
            <Columns>
                <dx:GridViewCommandColumn Caption=" " VisibleIndex="0" FixedStyle="Left" Width="50px">                    
                    <CustomButtons>
                        <dx:GridViewCommandColumnCustomButton ID="det" Text="Detail">
                        </dx:GridViewCommandColumnCustomButton>
                    </CustomButtons>                    
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Counter Proposal Number" FieldName="CP_Number"
                     VisibleIndex="1" Width="200px" FixedStyle="Left">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Rev" FieldName="Rev"
                     VisibleIndex="2" Width="40px" FixedStyle="Left">
                     <CellStyle HorizontalAlign="Center" Wrap="True"></CellStyle>
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Counter Proposal Date" FieldName="CP_Date" 
                     VisibleIndex="3" Width="100px" FixedStyle="Left">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center" Wrap="True"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="CE Number" FieldName="CE_Number" 
                     VisibleIndex="4" Width="180px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="CE Date" FieldName="CE_Date" 
                     VisibleIndex="5" Width="80px">
                     <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                     </PropertiesTextEdit>
                     <CellStyle HorizontalAlign="Center"></CellStyle>
                     <Settings AllowAutoFilter="False"/>
                </dx:GridViewDataTextColumn>                
                <dx:GridViewDataTextColumn Caption="Status" FieldName="CP_Status" 
                     VisibleIndex="6" Width="70px">
                    <Settings AutoFilterCondition="Contains" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="KeyFieldName" FieldName="KeyField" Visible="false"
                     VisibleIndex="7" Width="0px">
                    <Settings AutoFilterCondition="Contains" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="CE Status" FieldName="CE_Status" 
                     VisibleIndex="8" Width="70px">
                    <Settings AutoFilterCondition="Contains" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
               

             </Columns>
            
            <SettingsBehavior AllowFocusedRow="True" AllowSort="False" AllowDragDrop="False" ColumnResizeMode="Control" EnableRowHotTrack="True" />
            <SettingsPager PageSize="9"></SettingsPager>
            <Settings ShowFilterRow="false" VerticalScrollableHeight="225" HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto" ShowStatusBar="Hidden"></Settings>
            
            <ClientSideEvents 
                CustomButtonClick="GridCustomButtonClick"
                EndCallback="LoadCompleted"
            />
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" 
                AllowInsert="False" />

            <Styles Header-Paddings-Padding="2px" EditFormColumnCaption-Paddings-PaddingLeft="0px" EditFormColumnCaption-Paddings-PaddingRight="0px" >
                <Header CssClass="customHeader" HorizontalAlign="Center" Wrap="True">
                </Header>
            </Styles>
        </dx:ASPxGridView>
    </div>
    <%--<div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
      
        </table>
    </div>--%>
</div>
</asp:Content>

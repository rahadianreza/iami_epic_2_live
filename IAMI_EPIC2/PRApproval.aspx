﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PRApproval.aspx.vb" Inherits="IAMI_EPIC2.PRApproval" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    function MessageBox(s, e) {
        if (s.cpMessage == "Download Excel Successfully") {
            toastr.success(s.cpMessage, 'Success');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else if (s.cpMessage == null || s.cpMessage == "") {
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }
        else {
            toastr.warning(s.cpMessage, 'Warning');
            toastr.options.closeButton = false;
            toastr.options.debug = false;
            toastr.options.newestOnTop = false;
            toastr.options.progressBar = false;
            toastr.options.preventDuplicates = true;
            toastr.options.onclick = null;
        }

    }

</script>
    <style type="text/css">
        .style1
        {
            height: 32px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
    <div style="padding: 5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black; height: 100px;">  
            <tr style="height: 2px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>      
           
            <tr style="height: 35px">
                    <td style=" padding:0px 0px 0px 10px; width:100px">
                        <dx1:ASPxLabel ID="ASPxLabel1" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="Request Date">
                        </dx1:ASPxLabel>                 
                    </td>
                    <td>&nbsp;</td>
                    <td style="width:110px">
                    <dx:ASPxDateEdit ID="ReqDate_From" runat="server" Theme="Office2010Black" 
                        Width="120px" AutoPostBack="false" ClientInstanceName="ReqDate_From"
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                    </dx:ASPxDateEdit>   
                    
                    </td>
                    <td style="width:30px; padding:0px 0px 0px 15px">   
                    
                        <dx1:ASPxLabel ID="ASPxLabel3" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="~">
                        </dx1:ASPxLabel>   
                                                     
                    </td>
                    <td style="width:180px">
                    <dx:ASPxDateEdit ID="ReqDate_To" runat="server" Theme="Office2010Black" 
                        Width="120px" AutoPostBack="false" ClientInstanceName="ReqDate_To"
                        EditFormatString="dd-MMM-yyyy" DisplayFormatString="dd-MMM-yyyy"
                        Font-Names="Segoe UI" Font-Size="9pt" Height="25px">
                        <CalendarProperties>
                            <HeaderStyle Font-Size="12pt" Paddings-Padding="5px" />
                            <DayStyle Font-Size="9pt" Paddings-Padding="5px" />
                            <WeekNumberStyle Font-Size="9pt" Paddings-Padding="5px"></WeekNumberStyle>
                            <FooterStyle Font-Size="9pt" Paddings-Padding="10px" />
                            <ButtonStyle Font-Size="9pt" Paddings-Padding="10px"></ButtonStyle>
                        </CalendarProperties>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                    </dx:ASPxDateEdit>   
                    
                    </td>
                    <td style="width:20px">&nbsp;</td>
                    <td style="width:70px">
                        <dx1:ASPxLabel ID="ASPxLabel4" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                    Text="Department">
                        </dx1:ASPxLabel>                 
                    </td>
                    <td style="width:20px">&nbsp;</td>
                    <td>
           
                    <dx1:ASPxComboBox ID="cboDepartment" runat="server" ClientInstanceName="cboDepartment"
                        Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource2" TextField="Description"
                        ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">                            
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	cboSection.PerformCallback(cboDepartment.GetValue());
}" />
                        <Columns>            
                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                        </Columns>
                                <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx1:ASPxComboBox>
           
                    </td>
                    <td style="width:20px">&nbsp;</td>
                </tr>    
           
            <tr style="height: 35px">
               <td style=" padding:0px 0px 0px 10px">
                    <dx1:ASPxLabel ID="ASPxLabel2" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="PR Type">
                    </dx1:ASPxLabel>                 
                </td>
                <td></td>
                <td>
           
        <dx1:ASPxComboBox ID="cboPRType" runat="server" ClientInstanceName="cboPRType"
                            Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource1" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" />
                        <ButtonStyle Width="5px" Paddings-Padding="4px" ></ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel6" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Section">
                    </dx1:ASPxLabel>                 
                </td>
                <td>&nbsp;</td>
                <td>
           
        <dx1:ASPxComboBox ID="cboSection" runat="server" ClientInstanceName="cboSection"
                            Width="120px" Font-Names="Segoe UI" TextField="Description"
                            ValueField="Code" TextFormatString="{1}" Font-Size="9pt" 
                            Theme="Office2010Black" DropDownStyle="DropDown" 
                            IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                            Height="25px">                            
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
	txtSection.SetText(cboSection.GetValue());
}" />
                            <Columns>            
                            <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                            <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="120px" />
                            </Columns>
                                 <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                            </ButtonStyle>
                        </dx1:ASPxComboBox>
           
                </td>
                <td>&nbsp;</td>
            </tr>  

                        <tr style="height: 10px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td ></td>
                <td>&nbsp;</td>
                <td>
                    <dx1:ASPxLabel ID="ASPxLabel7" runat="server" Font-Names="Segoe UI" Font-Size="9pt" 
                                Text="Status">
                    </dx1:ASPxLabel>                 
                            </td>
                <td>&nbsp;</td>
                <td>
           
                    <dx1:ASPxComboBox ID="cboStatus" runat="server" ClientInstanceName="cboStatus"
                        Width="120px" Font-Names="Segoe UI" DataSourceID="SqlDataSource4" ValueField="Status" TextField="Status" TextFormatString="{1}" Font-Size="9pt" 
                        Theme="Office2010Black" DropDownStyle="DropDown" 
                        IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True" 
                        Height="25px">  
                                                
                        
                        <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                        </ItemStyle>
                        <ButtonStyle Width="5px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                        </ButtonStyle>
                    </dx1:ASPxComboBox>
           
                            </td>
                <td>&nbsp;</td>
            </tr>  
           
             <tr>
                <td style=" padding:0px 0px 0px 10px" colspan="10" class="style1">
                    <dx:ASPxButton ID="btnShowData" runat="server" Text="Show Data" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnShowData" Theme="Default">                        
                        <ClientSideEvents Click="function(s, e) {


                            var startdate = ReqDate_From.GetDate(); // get value dateline date
                            var enddate = ReqDate_To.GetDate(); // get value dateline date

                            if (startdate == null)
                            {
                                toastr.warning('Please input valid Start Date', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;                                
                            }
                            else if (enddate == null)
                            {
                                toastr.warning('Please input valid End Date', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;                                
                            }

                            var yearstartdate = startdate.getFullYear(); // where getFullYear returns the year (four digits)
                            var monthstartdate = startdate.getMonth(); // where getMonth returns the month (from 0-11)
                            var daystartdate = startdate.getDate();   // where getDate returns the day of the month (from 1-31)

                            if (daystartdate &lt; 10) {
                                daystartdate = '0'+ daystartdate  
                            }

                            if (monthstartdate &lt; 10) {
                                monthstartdate = '0'+ monthstartdate  
                            }

                            var vStartDate = yearstartdate + '-' + monthstartdate + '-' + daystartdate;

                            
                            var yearenddate = enddate.getFullYear(); // where getFullYear returns the year (four digits)
                            var monthenddate = enddate.getMonth(); // where getMonth returns the month (from 0-11)
                            var dayenddate = enddate.getDate();   // where getDate returns the day of the month (from 1-31)

                            if (dayenddate &lt; 10) {
                                dayenddate = '0'+ dayenddate  
                            }

                            if (monthenddate &lt; 10) {
                                monthenddate = '0'+ monthenddate  
                            }

                            var vEndDate = yearenddate + '-' + monthenddate + '-' + dayenddate;

                            if (vEndDate &lt; vStartDate) {
                                toastr.warning('End date must be bigger than Start Date', 'Warning');
                                toastr.options.closeButton = false;
                                toastr.options.debug = false;
                                toastr.options.newestOnTop = false;
                                toastr.options.progressBar = false;
                                toastr.options.preventDuplicates = true;
                                toastr.options.onclick = null;
                                e.processOnServer = false;
                                return;
                            }


	                        Grid.PerformCallback('gridload' + '|' + ReqDate_From.GetText() + '|' + ReqDate_To.GetText() + '|' + cboPRType.GetValue() + '|' + cboDepartment.GetValue() + '|' + cboSection.GetValue() );
                    }" />
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 &nbsp;
                    <dx:ASPxButton ID="btnExcel" runat="server" Text="Download" UseSubmitBehavior="false"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="false"
                        ClientInstanceName="btnRefresh" Theme="Default">                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                 </td>
            </tr>  
             <tr style="display:none">
                <td>
                     <dx:ASPxTextBox ID="txtSection" runat="server" ClientInstanceName="txtSection" 
                        Height="25px" Width="100px">
                    </dx:ASPxTextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
             </tr>
             <tr style="height: 10px">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                         <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                        </dx:ASPxGridViewExporter>
                    </td>
                <td>&nbsp;</td>
            </tr>  
        </table>

    </div>


    <div style="padding: 5px 5px 5px 5px">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        SelectCommand="select '00' Code,'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'PRType'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
                        SelectCommand="select '00' Code,'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Department'">
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
                        
                        SelectCommand="select '00' Code,'ALL' Description UNION ALL Select Rtrim(Par_Code) Code, Rtrim(Par_Description) Description  From Mst_Parameter Where Par_Group = 'Section'">
                    </asp:SqlDataSource>

                    <asp:SqlDataSource ID="SqlDataSource4" runat="server"
                        ConnectionString="<%$ ConnectionStrings:ApplicationServices %>" 
                        
                        SelectCommand="Select Status From VW_Status_Approval">
                    </asp:SqlDataSource>
                    <dx:ASPxCallback ID="cbMessage" runat="server" 
                        ClientInstanceName="cbMessage">
                    <ClientSideEvents Init="MessageBox" />                                           
                </dx:ASPxCallback>    
              <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
            EnableTheming="True" Theme="Office2010Black" 
             Width="100%" OnAfterPerformCallback="Grid_AfterPerformCallback"
            Font-Names="Segoe UI" Font-Size="9pt" KeyFieldName="PR_Number" >

                  <ClientSideEvents CustomButtonClick="function(s, e) {
		          if(e.buttonID == 'edit'){
                     
                                var rowKey = Grid.GetRowKey(e.visibleIndex);
                             
	                            window.location.href= 'PRApprovalDetail.aspx?ID=' + rowKey;
                            }
}" />

                  <Columns>
                      <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" FixedStyle="Left">
                          <CustomButtons>
                              <dx:GridViewCommandColumnCustomButton ID="edit" Text="Detail">
                              </dx:GridViewCommandColumnCustomButton>
                          </CustomButtons>
                      </dx:GridViewCommandColumn>

                      <dx:GridViewDataTextColumn Caption="PR Number" VisibleIndex="1" FixedStyle="Left"
                          FieldName="PR_Number" Width="170px">    
                          <Settings AutoFilterCondition="Contains" />
                          <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn Caption="PR Date" VisibleIndex="3" FixedStyle="Left"
                          FieldName="PR_Date" Width="90px">
                          <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                          <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                          </PropertiesTextEdit>
                          <Settings AllowAutoFilter="False" />
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn Caption="PR Budget" VisibleIndex="4" 
                          FieldName="PRBudget">
                          <Settings AutoFilterCondition="Contains" />
                          <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn Caption="PR Type" VisibleIndex="5" Width="130px"
                          FieldName="PRType">
                          <Settings AutoFilterCondition="Contains" />
                          <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn Caption="Department" VisibleIndex="6" 
                          FieldName="Department">
                          <Settings AutoFilterCondition="Contains" />
                          <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn Caption="Section" VisibleIndex="7" Width="150px"
                          FieldName="Section"> 
                          <Settings AutoFilterCondition="Contains" />
                          <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn Caption="Project" VisibleIndex="8" 
                          FieldName="Project">
                          <Settings AutoFilterCondition="Contains" />
                          <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn Caption="Urgent Status" VisibleIndex="9" 
                          FieldName="Urgent_Status">
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                      </dx:GridViewDataTextColumn>

                       <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="10" 
                          FieldName="Status">
                        <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" 
                                    VerticalAlign="Middle" Wrap="True">
                                    <Paddings PaddingLeft="5px"></Paddings>
                              </HeaderStyle>
                      </dx:GridViewDataTextColumn>

                      <dx:GridViewBandColumn VisibleIndex="11">
                          <Columns>
                              <dx:GridViewDataTextColumn Caption="Approval Name" VisibleIndex="0" 
                                  FieldName="AppPerson01">
                              <HeaderStyle HorizontalAlign="Center" />
                              </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Date" VisibleIndex="3" 
                                  FieldName="AppDate01">
                              <HeaderStyle HorizontalAlign="Center" />
                                  <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                                  </PropertiesTextEdit>
                                  <Settings AllowAutoFilter="False" />
                              </dx:GridViewDataTextColumn>
                          </Columns>
                          <HeaderStyle HorizontalAlign="Center" />
                      </dx:GridViewBandColumn>
                      <dx:GridViewBandColumn VisibleIndex="12">
                          <Columns>
                              <dx:GridViewDataTextColumn Caption="Approval Name" VisibleIndex="0" 
                                  FieldName="AppPerson02">
                                <HeaderStyle HorizontalAlign="Center" />
                              </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Date" VisibleIndex="2" 
                                  FieldName="AppDate02">
                                   <HeaderStyle HorizontalAlign="Center" />
                                  <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                                  </PropertiesTextEdit>
                                  <Settings AllowAutoFilter="False" />
                              </dx:GridViewDataTextColumn>
                          </Columns>
                          <HeaderStyle HorizontalAlign="Center" />
                      </dx:GridViewBandColumn>
                      <dx:GridViewBandColumn VisibleIndex="13">
                          <Columns>
                              <dx:GridViewDataTextColumn Caption="Approval Name" VisibleIndex="0" 
                                  FieldName="AppPerson03">
                              <HeaderStyle HorizontalAlign="Center" />
                              </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Date" VisibleIndex="2" 
                                  FieldName="AppDate03">
                                <HeaderStyle HorizontalAlign="Center" />
                                  <PropertiesTextEdit DisplayFormatString="dd MMM yyyy">
                                  </PropertiesTextEdit>
                                  <Settings AllowAutoFilter="False" />
                              </dx:GridViewDataTextColumn>
                          </Columns>
                          <HeaderStyle HorizontalAlign="Center" />
                      </dx:GridViewBandColumn>

                      <dx:GridViewBandColumn VisibleIndex="14">
                          <Columns>
                              <dx:GridViewDataTextColumn Caption="Approval Date" FieldName="AppDate04" 
                                  VisibleIndex="1">
                              </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Name" FieldName="AppPerson04" 
                                  VisibleIndex="0">
                              </dx:GridViewDataTextColumn>
                          </Columns>
                      </dx:GridViewBandColumn>
                      <dx:GridViewBandColumn VisibleIndex="15">
                          <Columns>
                              <dx:GridViewDataTextColumn Caption="Approval Date" FieldName="AppDate05" 
                                  VisibleIndex="1">
                              </dx:GridViewDataTextColumn>
                              <dx:GridViewDataTextColumn Caption="Approval Name" FieldName="AppPerson05" 
                                  VisibleIndex="0">
                              </dx:GridViewDataTextColumn>
                          </Columns>
                      </dx:GridViewBandColumn>
                      <dx:GridViewDataTextColumn Caption="Rev" FieldName="Rev" VisibleIndex="2" FixedStyle="Left"
                          Width="50px">
                      </dx:GridViewDataTextColumn>
                  </Columns>

                    <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true"></SettingsPager>
                    <Settings ShowFilterRow="True" VerticalScrollableHeight="300" 
                    HorizontalScrollBarMode="Auto" VerticalScrollBarMode="Auto"></Settings>

                    <Styles Header-Paddings-Padding="5px" EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px" >
                        <Header HorizontalAlign="Center">
                            <Paddings Padding="2px"></Paddings>
                        </Header>

<EditFormColumnCaption>
<Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
</EditFormColumnCaption>
                    </Styles>

            </dx:ASPxGridView>
    </div>
    
</div>
</asp:Content>

﻿Imports DevExpress.Web.ASPxGridView
'Imports DevExpress.XtraPrinting
Imports System.Collections.Generic
Imports System.Web
Imports System.IO
Imports System.Drawing
Imports System.Web.UI
Imports System.Web.UI.Control
Imports Microsoft.VisualBasic
Imports DevExpress.Web
Imports DevExpress.Web.ASPxEditors
Imports OfficeOpenXml

Public Class QCDMR
    Inherits System.Web.UI.Page
    Private mergedCells As New Dictionary(Of GridViewDataColumn, TableCell)()
    Private cellRowSpans As New Dictionary(Of TableCell, Integer)()
#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fRefresh As Boolean = False
    Dim PrjID As String = ""
    Dim Group As String = ""
    Dim commodity As String = ""
    Dim Groupcommodity As String = ""
    Dim PartNo As String = ""
#End Region

#Region "Initialization"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("M020")
        Master.SiteTitle = sGlobal.menuName
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)

        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            PrjID = Request.QueryString("projectid")
            Group = Request.QueryString("groupid")
            commodity = Request.QueryString("commodity")
            Groupcommodity = Request.QueryString("groupcommodity")
            PartNo = Request.QueryString("PartNo")

            FillCombo("", "", "", "", 1)
            FillCombo(PrjID, "", "", "", 2)
            FillCombo(PrjID, Group, "", "", 3)
            FillCombo(PrjID, Group, commodity, "", 4)
            FillCombo(PrjID, Group, commodity, Groupcommodity, 5)
            cboProject.Value = PrjID
            cboGroup.Value = Group
            cboCommodity.Value = commodity
            cboCommodityGroup.Value = Groupcommodity
            cboPartNo.Value = PartNo


            cboProject.ClientEnabled = False
            cboGroup.ClientEnabled = False
            cboCommodity.ClientEnabled = False
            cboCommodityGroup.ClientEnabled = False
            cboPartNo.ClientEnabled = False
        End If
    End Sub
#End Region

#Region "Control Event"

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("QCDMRList.aspx")
    End Sub
    Private Sub FillCombo(ByVal proj As String, ByVal group As String, ByVal commodity As String, ByVal commodityGroup As String, pstatus As Integer)
        Dim ds As New DataSet
        Dim pmsg As String = ""

        If pstatus = 1 Then
            ds = clsQCDMRDB.getProjectID(proj, "", "", "", 1, pUser)
            If pmsg = "" Then
                cboProject.DataSource = ds
                cboProject.DataBind()
            End If
        End If

        If pstatus = 2 Then
            ds = clsQCDMRDB.getProjectID(proj, "", "", "", 2, pUser)
            If pmsg = "" Then
                cboGroup.DataSource = ds
                cboGroup.DataBind()
            End If
        End If

        If pstatus = 3 Then
            ds = clsQCDMRDB.getProjectID(proj, group, "", "", 3, pUser)
            If pmsg = "" Then
                cboCommodity.DataSource = ds
                cboCommodity.DataBind()
            End If
        End If

        If pstatus = 4 Then
            ds = clsQCDMRDB.getProjectID(proj, group, commodity, "", 4, pUser)
            If pmsg = "" Then
                cboCommodityGroup.DataSource = ds
                cboCommodityGroup.DataBind()
            End If
        End If
        If pstatus = 5 Then
            ds = clsQCDMRDB.getProjectID(proj, group, commodity, commodityGroup, 5, pUser)
            If pmsg = "" Then
                cboPartNo.DataSource = ds
                cboPartNo.DataBind()
            End If
        End If
    End Sub

    Protected Sub cboGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase)
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)

        ds = clsQCDMRDB.getProjectID(Project, "", "", "", 2, pUser)
        If pmsg = "" Then
            cboGroup.DataSource = ds
            cboGroup.DataBind()
        End If
    End Sub

    Protected Sub cboCommodity_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase)
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)
        Dim Group As String = Split(e.Parameter, "|")(1)

        ds = clsQCDMRDB.getProjectID(Project, Group, "", "", 3, pUser)
        If pmsg = "" Then
            cboCommodity.DataSource = ds
            cboCommodity.DataBind()
        End If
    End Sub
    Protected Sub cboCommodityGroup_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase)
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)
        Dim Group As String = Split(e.Parameter, "|")(1)
        Dim Commodity As String = Split(e.Parameter, "|")(2)

        ds = clsQCDMRDB.getProjectID(Project, Group, Commodity, "", 4, pUser)
        If pmsg = "" Then
            cboCommodityGroup.DataSource = ds
            cboCommodityGroup.DataBind()
        End If
    End Sub
    Protected Sub cboPartNo_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase)
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim Project As String = Split(e.Parameter, "|")(0)
        Dim Group As String = Split(e.Parameter, "|")(1)
        Dim Commodity As String = Split(e.Parameter, "|")(2)
        Dim CommodityGroup As String = Split(e.Parameter, "|")(3)

        ds = clsQCDMRDB.getProjectID(Project, Group, Commodity, CommodityGroup, 5, pUser)
        If pmsg = "" Then
            cboPartNo.DataSource = ds
            cboPartNo.DataBind()
        End If
    End Sub

    Private Sub Grid_HtmlRowCreated(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs) Handles Grid.HtmlRowCreated
        For i As Integer = e.Row.Cells.Count - 1 To 0 Step -1
            Dim dataCell As DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataCell = TryCast(e.Row.Cells(i), DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataCell)
            Dim pnext As Boolean = True
            Dim pPrev As Boolean = True

            If dataCell IsNot Nothing Then
                If dataCell.DataColumn.ToString = "Aspect" Then

                    MergeCells(dataCell.DataColumn, e.VisibleIndex, dataCell)
                ElseIf dataCell.DataColumn.ToString = "Sub Total1" Or dataCell.DataColumn.ToString = "Sub Total2" Or dataCell.DataColumn.ToString = "Sub Total3" Or dataCell.DataColumn.ToString = "Sub Total4" _
                    Or dataCell.DataColumn.ToString = "Sub Total5" Or dataCell.DataColumn.ToString = "Sub Total6" Then
                    If Grid.GetRowValues(e.VisibleIndex, "seqnogroup") = Grid.GetRowValues(e.VisibleIndex + 1, "seqnogroup") Then
                        pnext = True
                    Else
                        pnext = False
                    End If
                    If Grid.GetRowValues(e.VisibleIndex, "seqnogroup") = Grid.GetRowValues(e.VisibleIndex - 1, "seqnogroup") Then
                        pPrev = True
                    Else
                        pPrev = False
                    End If
                    MergeCells2(dataCell.DataColumn, e.VisibleIndex, dataCell, pnext, pPrev)


                End If
            End If
        Next i
    End Sub
    Private Sub MergeCells2(ByVal column As GridViewDataColumn, ByVal visibleIndex As Integer, ByVal cell As TableCell, ByVal pnext As Boolean, ByVal pPrev As Boolean)
        Dim isNextTheSame As Boolean = IsNextRowHasSameData(column, visibleIndex)
        'If isNextTheSame Then
        If pnext Then
            If Not mergedCells.ContainsKey(column) Then
                mergedCells(column) = cell
            End If
        End If
        'If IsPrevRowHasSameData(column, visibleIndex) Then
        If pPrev Then
            CType(cell.Parent, TableRow).Cells.Remove(cell)
            If mergedCells.ContainsKey(column) Then
                Dim mergedCell As TableCell = mergedCells(column)
                If Not cellRowSpans.ContainsKey(mergedCell) Then
                    cellRowSpans(mergedCell) = 1
                End If
                cellRowSpans(mergedCell) = cellRowSpans(mergedCell) + 1
            End If
        End If
        'If Not isNextTheSame Then
        If Not pnext Then
            mergedCells.Remove(column)
        End If
    End Sub
    Private Sub MergeCells(ByVal column As GridViewDataColumn, ByVal visibleIndex As Integer, ByVal cell As TableCell)
        Dim isNextTheSame As Boolean = IsNextRowHasSameData(column, visibleIndex)
        If isNextTheSame Then
            If Not mergedCells.ContainsKey(column) Then
                mergedCells(column) = cell
            End If
        End If
        If IsPrevRowHasSameData(column, visibleIndex) Then
            CType(cell.Parent, TableRow).Cells.Remove(cell)
            If mergedCells.ContainsKey(column) Then
                Dim mergedCell As TableCell = mergedCells(column)
                If Not cellRowSpans.ContainsKey(mergedCell) Then
                    cellRowSpans(mergedCell) = 1
                End If
                cellRowSpans(mergedCell) = cellRowSpans(mergedCell) + 1
            End If
        End If
        If Not isNextTheSame Then
            mergedCells.Remove(column)
        End If
    End Sub
    Private Function IsNextRowHasSameData(ByVal column As GridViewDataColumn, ByVal visibleIndex As Integer) As Boolean
        'is it the last visible row
        If visibleIndex >= Grid.VisibleRowCount - 1 Then
            Return False
        End If

        Return IsSameData(column.FieldName, visibleIndex, visibleIndex + 1)
    End Function
    Private Function IsPrevRowHasSameData(ByVal column As GridViewDataColumn, ByVal visibleIndex As Integer) As Boolean

        Dim grid_Renamed As ASPxGridView = column.Grid
        'is it the first visible row
        If visibleIndex <= Grid.VisibleStartIndex Then
            Return False
        End If

        Return IsSameData(column.FieldName, visibleIndex, visibleIndex - 1)
    End Function
    Private Function IsSameData(ByVal fieldName As String, ByVal visibleIndex1 As Integer, ByVal visibleIndex2 As Integer) As Boolean
        ' is it a group row?
        If Grid.GetRowLevel(visibleIndex2) <> Grid.GroupCount Then
            Return False
        End If

        Return Object.Equals(Grid.GetRowValues(visibleIndex1, fieldName), Grid.GetRowValues(visibleIndex2, fieldName))
    End Function

    Private Sub Grid_CellEditorInitialize(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If Not Grid.IsNewRowEditing Then
            If e.Column.FieldName = "seqnogroup" Or e.Column.FieldName = "ID" Or e.Column.FieldName = "pValueSubTotal1" Or _
                e.Column.FieldName = "pValueSubTotal2" Or e.Column.FieldName = "pValueSubTotal3" Or e.Column.FieldName = "pValueSubTotal4" Or _
                e.Column.FieldName = "pValueSubTotal5" Or e.Column.FieldName = "pValueSubTotal6" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
                e.Editor.Visible = False
                e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
            End If
            If e.Column.FieldName = "Aspect" Or e.Column.FieldName = "Item" Or e.Column.FieldName = "VendorCode1" Or e.Column.FieldName = "VendorCode2" Or e.Column.FieldName = "VendorCode3" Or e.Column.FieldName = "VendorCode4" _
                Or e.Column.FieldName = "VendorCode5" Or e.Column.FieldName = "VendorCode6" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
                e.Editor.Visible = True
            End If
        End If
    End Sub
    Protected Sub Grid_StartRowEditing(sender As Object, e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not Grid.IsNewRowEditing) Then
            Grid.DoRowValidation()
        End If
    End Sub
    Private Sub Grid_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        Dim pdataHeader As New List(Of clsQCDMR)
        Dim No As Integer = 0
        Dim pErr As String = ""
        Dim Ses As New clsQCDMR With {.Project_ID = cboProject.Value,
                                              .Group = cboGroup.Value,
                                              .Comodity = cboCommodity.Value,
                                              .ComodityGroup = cboCommodityGroup.Value,
                                              .PartNo = cboPartNo.Value,
                                              .pValue1 = e.NewValues("pValue1"),
                                              .pValue2 = e.NewValues("pValue2"),
                                              .pValue3 = e.NewValues("pValue3"),
                                              .pValue4 = e.NewValues("pValue4"),
                                              .pValue5 = e.NewValues("pValue5"),
                                              .pValue6 = e.NewValues("pValue6"),
                                              .VendorCode1 = e.NewValues("VendorCode1"),
                                              .VendorCode2 = e.NewValues("VendorCode2"),
                                              .VendorCode3 = e.NewValues("VendorCode3"),
                                              .VendorCode4 = e.NewValues("VendorCode4"),
                                              .VendorCode5 = e.NewValues("VendorCode5"),
                                              .VendorCode6 = e.NewValues("VendorCode6"),
                                              .ID = e.Keys("ID"),
                                              .UserID = pUser}
        clsQCDMRDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1, Grid)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1, Grid)
            up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
            pdataHeader = clsQCDMRDB.getHeaderName(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
            For x = 1 To 6
                Grid.Columns("VendorCode" & x).Caption() = ""
                Grid.Columns("pValue" & x).Caption() = ""
                Grid.Columns("pValueSubTotal" & x).Caption() = ""
                Grid.Columns("pValue" & x).Visible = False
                Grid.Columns("pValueSubTotal" & x).Visible = False
                Grid.Columns("VendorCode" & x).Visible = False
            Next

            For Each xdata In pdataHeader
                No = No + 1
                Grid.Columns("VendorCode" & No).Visible = True
                Grid.Columns("VendorCandidate").Visible = True
                Grid.Columns("pValue" & No).Visible = True
                Grid.Columns("pValueSubTotal" & No).Visible = True
                Grid.Columns("pValue" & No).Caption() = xdata.SupplierName
                Grid.Columns("pValueSubTotal" & No).Caption() = "Sub Total" & No

            Next
        End If
    End Sub

    Protected Sub Grid_AfterPerformCallback(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        Dim pdataHeader As New List(Of clsQCDMR)
        Dim No As Integer = 0
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
            pdataHeader = clsQCDMRDB.getHeaderName(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
            For x = 1 To 6
                Grid.Columns("VendorCode" & x).Caption() = ""
                Grid.Columns("pValue" & x).Caption() = ""
                Grid.Columns("pValueSubTotal" & x).Caption() = ""
                Grid.Columns("pValue" & x).Visible = False
                Grid.Columns("pValueSubTotal" & x).Visible = False
                Grid.Columns("VendorCode" & x).Visible = False
            Next

            For Each xdata In pdataHeader
                No = No + 1
                Grid.Columns("VendorCode" & No).Visible = True
                Grid.Columns("VendorCandidate").Visible = True
                Grid.Columns("pValue" & No).Visible = True
                Grid.Columns("pValueSubTotal" & No).Visible = True
                Grid.Columns("pValue" & No).Caption() = xdata.SupplierName
                Grid.Columns("pValueSubTotal" & No).Caption() = "Sub Total" & No

            Next
        End If
        If e.CallbackName = "CANCELEDIT" Then
            up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
            pdataHeader = clsQCDMRDB.getHeaderName(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
            For x = 1 To 6
                Grid.Columns("VendorCode" & x).Caption() = ""
                Grid.Columns("pValue" & x).Caption() = ""
                Grid.Columns("pValueSubTotal" & x).Caption() = ""
                Grid.Columns("pValue" & x).Visible = False
                Grid.Columns("pValueSubTotal" & x).Visible = False
                Grid.Columns("VendorCode" & x).Visible = False
            Next

            For Each xdata In pdataHeader
                No = No + 1
                Grid.Columns("VendorCode" & No).Visible = True
                Grid.Columns("VendorCandidate").Visible = True
                Grid.Columns("pValue" & No).Visible = True
                Grid.Columns("pValueSubTotal" & No).Visible = True
                Grid.Columns("pValue" & No).Caption() = xdata.SupplierName
                Grid.Columns("pValueSubTotal" & No).Caption() = "Sub Total" & No

            Next
        End If
    End Sub

    Private Sub Grid_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs) Handles Grid.HtmlDataCellPrepared
        e.Cell.Attributes.Add("ci", e.DataColumn.VisibleIndex.ToString())

        If cellRowSpans.ContainsKey(e.Cell) Then
            e.Cell.RowSpan = cellRowSpans(e.Cell)
        End If

    End Sub

    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)
        Dim Project As String = Split(e.Parameters, "|")(1)
        Dim Group As String = Split(e.Parameters, "|")(2)
        Dim Commodity As String = Split(e.Parameters, "|")(3)
        Dim CommodityGroup As String = Split(e.Parameters, "|")(4)
        Dim PartNo As String = Split(e.Parameters, "|")(5)
        Dim pdataHeader As New List(Of clsQCDMR)
        Dim No As Integer = 0
        Select Case pFunction
            Case "load"
                Dim pProjectID As String = Split(e.Parameters, "|")(1)
                up_GridLoad(Project, Group, Commodity, CommodityGroup, PartNo)
                pdataHeader = clsQCDMRDB.getHeaderName(Project, Group, Commodity, CommodityGroup, PartNo)
                For x = 1 To 6
                    Grid.Columns("VendorCode" & x).Caption() = ""
                    Grid.Columns("pValue" & x).Caption() = ""
                    Grid.Columns("pValueSubTotal" & x).Caption() = ""
                    Grid.Columns("pValue" & x).Visible = False
                    Grid.Columns("pValueSubTotal" & x).Visible = False
                    Grid.Columns("VendorCode" & x).Visible = False
                Next

                For Each xdata In pdataHeader
                    No = No + 1
                    Grid.Columns("VendorCode" & No).Visible = True
                    Grid.Columns("VendorCandidate").Visible = True
                    Grid.Columns("pValue" & No).Visible = True
                    Grid.Columns("pValueSubTotal" & No).Visible = True
                    Grid.Columns("pValue" & No).Caption() = xdata.SupplierName
                    Grid.Columns("pValueSubTotal" & No).Caption() = "Sub Total" & No

                Next
        End Select
    End Sub
    Protected Sub btnXlsxExport_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim No As Integer = 0
        Dim pdataHeader As New List(Of clsQCDMR)
        up_GridLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
        'rename header name variant 
        For x = 1 To 6
            Grid.Columns("pValue" & x).Caption() = ""
            Grid.Columns("pValue" & x).Visible = False
        Next
        For Each xdata In pdataHeader
            No = No + 1
            Grid.Columns("pValue" & No).Visible = True
            Grid.Columns("pValue" & No).Caption() = xdata.SupplierName

        Next
        GridExporter.WriteXlsToResponse()
    End Sub
    Private Sub up_Excel()
        Dim ds As New DataSet
        Dim Checkitem As String = ""
        Dim pint As Integer = 0
        With Grid
            If .VisibleRowCount = 0 Then
                Return
            End If
        End With
        Using Pck As New ExcelPackage
            Dim ws As ExcelWorksheet = Pck.Workbook.Worksheets.Add("QCDMR")
            With ws
                'EpPlusInsertHeader(ws)
                Dim irowunit As Integer = 3
                ds = clsQCDMRDB.GetLoad(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value, "")
                Dim iRow As Integer = 0
                Dim r As Integer = 0
                Dim pcol As Integer = 0
                Dim pcolm As Integer = 2
                For iRow = 0 To ds.Tables(0).Rows.Count - 1
                    r = irowunit
                    .Cells(r, 2, r, 2).Value = ds.Tables(0).Rows(iRow)("Item").ToString
                    If Checkitem <> ds.Tables(0).Rows(iRow)("Aspect").ToString Then
                        .Cells(r, 1, r, 1).Value = ds.Tables(0).Rows(iRow)("Aspect").ToString
                        pcolm = 2
                        For pData = 1 To 6
                            If ds.Tables(0).Rows(iRow)("vendorCode" & pData).ToString <> "" Then
                                pcolm = pcolm + 2
                                .Cells(r, pcolm, r, pcolm).Value = ds.Tables(0).Rows(iRow)("pValueSubTotal" & pData)
                            End If
                        Next

                        pcolm = 2
                        If r <> 3 Then
                            .Cells(pint, 1, r - 1, 1).Merge = True
                            For pData = 1 To 6
                                If ds.Tables(0).Rows(iRow)("vendorCode" & pData).ToString <> "" Then
                                    pcolm = pcolm + 2
                                    .Cells(pint, pcolm, r - 1, pcolm).Merge = True
                                End If
                            Next

                        End If
                        pint = r
                    End If

                    pcolm = 1
                    For pData = 1 To 6
                        If ds.Tables(0).Rows(iRow)("vendorCode" & pData).ToString <> "" Then
                            pcolm = pcolm + 2
                            .Cells(r, pcolm, r, pcolm).Value = ds.Tables(0).Rows(iRow)("pValue" & pData).ToString

                        End If
                    Next

                    Checkitem = ds.Tables(0).Rows(iRow)("Aspect").ToString

                    irowunit = irowunit + 1

                Next

                pcolm = 2
                If r <> 3 Then
                    .Cells(pint, 1, r, 1).Merge = True
                    For pData = 1 To 6
                        If ds.Tables(0).Rows(iRow - 1)("vendorCode" & pData).ToString <> "" Then
                            pcolm = pcolm + 2
                            .Cells(pint, pcolm, r, pcolm).Merge = True
                        End If
                    Next
                End If


                Dim rg As OfficeOpenXml.ExcelRange = .Cells(1, 1, irowunit - 1, pcolm)
                rg.Style.Border.Bottom.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Left.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Right.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Border.Top.Style = Style.ExcelBorderStyle.Thin
                rg.Style.Font.Name = "Tahoma"
                rg.Style.Font.Size = 10

                Dim rf As OfficeOpenXml.ExcelRange = .Cells(3, 1, irowunit - 1, 1)
                rf.Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center

                rf = .Cells(3, 3, irowunit - 1, pcolm)
                rf.Style.Numberformat.Format = "#0.00"
                rf.Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Right
                rf.Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center

                Dim rgHeader As OfficeOpenXml.ExcelRange = .Cells(1, 1, 2, pcolm)
                rgHeader.Style.Fill.PatternType = Style.ExcelFillStyle.Solid
                rgHeader.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue)

                .Cells("A1").Value = "Aspect"
                .SelectedRange("A1:A2").Merge = True
                .Cells("B1").Value = "Item"
                .SelectedRange("B1:B2").Merge = True

                If pcolm > 2 Then
                    .Cells("C1").Value = "Vendor Candidate"
                    Dim pcolheader As Integer = 3
                    Dim pdataHeader As New List(Of clsQCDMR)
                    pdataHeader = clsQCDMRDB.getHeaderName(cboProject.Value, cboGroup.Value, cboCommodity.Value, cboCommodityGroup.Value, cboPartNo.Value)
                    'rename header name supplier 
                    For Each xdata In pdataHeader
                        .Cells(2, pcolheader, 2, pcolheader).Value = xdata.SupplierName
                        .Cells(2, pcolheader + 1, 2, pcolheader + 1).Value = "Sub Total"
                        .Column(pcolheader).Width = 16
                        .Column(pcolheader + 1).Width = 10
                        pcolheader = pcolheader + 2
                    Next
                End If

                .Column(1).Width = 25
                .Column(2).Width = 60
                rf = .Cells(1, 3, 1, pcolm)
                rf.Merge = True
                rf = .Cells(1, 1, 2, pcolm)
                rf.Style.VerticalAlignment = Style.ExcelVerticalAlignment.Center
                rf.Style.HorizontalAlignment = Style.ExcelHorizontalAlignment.Center
                rf.Style.WrapText = True
            End With



            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment; filename=QCDMR" & Format(Date.Now, "yyyy-MM-dd") & ".xlsx")

            Dim stream As MemoryStream = New MemoryStream(Pck.GetAsByteArray())

            Response.OutputStream.Write(stream.ToArray(), 0, stream.ToArray().Length)
            Response.Flush()
            Response.Close()
        End Using

    End Sub

    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        up_Excel()

        cbMessage.JSProperties("cpmessage") = "Download Data to Excel Has Been Successfully"
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer, ByVal pGrid As ASPxGridView)
        pGrid.JSProperties("cp_message") = ErrMsg
        pGrid.JSProperties("cp_type") = msgType
        pGrid.JSProperties("cp_val") = pVal
    End Sub
#End Region

#Region "Procedure"

    Private Sub up_GridLoad(ByVal pProjectID As String, ByVal pGroup As String, ByVal pComodity As String, ByVal pComodityGroup As String, ByVal PartNo As String)
        Dim ErrMsg As String = ""
        Dim Ses As New List(Of clsQCDMR)
        Session("bold") = 0
        Ses = clsQCDMRDB.getlist(IIf(pProjectID = Nothing, "", pProjectID), IIf(pGroup = Nothing, "", pGroup), IIf(pComodity = Nothing, "", pComodity), IIf(pComodityGroup = Nothing, "", pComodityGroup), IIf(PartNo = Nothing, "", PartNo), ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub

#End Region



End Class
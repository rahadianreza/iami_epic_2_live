﻿Imports DevExpress.XtraCharts
Imports System.Drawing
Imports DevExpress.XtraCharts.Web
Imports DevExpress.Web.ASPxCallbackPanel
Imports System.Data.SqlClient

Public Class ElectricityChart
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A080")
        Master.SiteTitle = "Electricity Chart"
        pUser = Session("user")

        If Not Page.IsCallback And Not Page.IsPostBack Then
            Period_From.Value = Now
            Period_To.Value = Now
            'cboRateClass.SelectedIndex = 0
            'FillCombo("ALL")
            ' cboVAKind.SelectedIndex = 0

        End If


    End Sub

    Private Sub FillCombo(ByVal RateClass As String)
        Dim ds As New DataSet
        Dim pmsg As String = ""

        ds = ClsElectricityDB.getVAKind_Chart(RateClass, pmsg)
        If pmsg = "" Then
            cboVAKind.DataSource = ds
            cboVAKind.DataBind()
        End If

    End Sub
    Private Sub cboVAKind_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles cboVAKind.Callback
        Dim ds As New DataSet
        Dim pmsg As String = ""
        Dim RateClass As String = Split(e.Parameter, "|")(0)

        ds = ClsElectricityDB.getVAKind_Chart(RateClass, pmsg)
        If pmsg = "" Then
            cboVAKind.DataSource = ds
            cboVAKind.DataBind()
            ' cboVAKind.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ASPxCallbackPanel1_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel1.Callback
        Dim wbc As New WebChartControl()
        'Dim ds As New DataSet
        Dim pFunction As String = Split(e.Parameter, "|")(0)
        Dim p1 As String = Split(e.Parameter, "|")(1) 'rate
        Dim p2 As String = Split(e.Parameter, "|")(2) 'vakind
        Dim p3 As String = Format(Period_From.Value, "yyyyMM")
        Dim p4 As String = Format(Period_To.Value, "yyyyMM")

        Dim ErrMsg As String = ""
        If pFunction = "show" Then
            Try
                Using con As New SqlConnection(Sconn.Stringkoneksi)
                    Dim ChartDS As New DataSet
                    ChartDS = ClsElectricityDB.ChartSource(p1, p2, p3, p4, ErrMsg)

                    Dim chart As WebChartControl = New WebChartControl()
                    Dim series1 As Series = New Series("Line View", ViewType.Spline)

                    ' Create a line series. 
                    CType(series1.View, PointSeriesView).PointMarkerOptions.Size = "2"

                    chart.DataSource = ChartDS
                    'chart.DataMember = "Mst_CrudgeOil"
                    chart.Series.Add(series1)

                    Dim chartTitle1 As New ChartTitle()
                    ' Define the text for the titles. 
                    chartTitle1.Text = "<b>Electricity</b>"
                    chartTitle1.Alignment = StringAlignment.Center
                    chartTitle1.Dock = ChartTitleDockStyle.Top
                    chartTitle1.Antialiasing = True
                    chartTitle1.Font = New Font("Segoe UI", 14, FontStyle.Bold)
                    chart.Titles.AddRange(New ChartTitle() {chartTitle1})

                    series1.ArgumentDataMember = "Period"
                    series1.ValueDataMembers(0) = "PostPaid"
                    'Me.Controls.Add(chart)
                    chart.DataBind()
                    chart.Width = 1000
                    chart.Height = 400

                    CType(sender, ASPxCallbackPanel).Controls.Add(chart)
                End Using
            Catch ex As Exception
                'pErr = ex.Message

            End Try

        End If

        ' Dim r As New Random()
        'For i As Integer = 0 To 4
        '    wbc.Series(0).Points.Add(New SeriesPoint(DateTime.Today.AddDays(i), _
        '    (CInt(Fix((r.NextDouble() * 100) * 10))) / 10.0))
        'Next i


    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("Electricity.aspx")
    End Sub

    Protected Sub WebChartControl1_CustomCallback(ByVal sender As Object, ByVal e As CustomCallbackEventArgs)
        'SeriesView.Indicators.Clear()
        'Dim indicator As SeparatePaneIndicator = CreateIndicator()
        'indicator.Pane = Diagram.Panes(0)
        'indicator.AxisY = Diagram.SecondaryAxesY(0)
        'SeriesView.Indicators.Add(indicator)
        'CType(WebChartControl1.Annotations(0), TextAnnotation).Text = indicator.Name
    End Sub
End Class
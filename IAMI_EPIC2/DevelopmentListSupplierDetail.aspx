﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="DevelopmentListSupplierDetail.aspx.vb" Inherits="IAMI_EPIC2.DevelopmentListSupplierDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
        function OnEndCallback(s, e) {
            if (!Grid.cpKVProjectYear && !Grid.cpKVProjectID && !Grid.cpKVPartNo && !Grid.cpKVModel)
                return;
            popup.Show();
            popup.PerformCallback(Grid.cpKVProjectYear + '|' + Grid.cpKVProjectID + "|" + Grid.cpKVPartNo + "|" + Grid.cpKVModel);
//            alert(Grid.cpKVProjectID);

        }

        function GetMessage(s, e) {
            if (s.cpMessage == "Update Data Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            } else if (s.cpMessage == "Data Submit Successfully!") {
                toastr.success(s.cpMessage, 'Success');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;

                btnCompleted.SetEnabled(false);
                Grid.SetEnabled(false);

                setTimeout(function () {
                    window.location.href = 'DevelopmentListSupplierDetail.aspx?ID=' + s.cpProjectID;
                }, 1000);


            } else {
                toastr.error(s.cpMessage, 'Error');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
            }
            Grid.cpKVProjectYear = null;
            Grid.cpKVProjectID = null;
            Grid.cpKVPartNo = null;
            Grid.cpKVModel = null;
        }

        function InsertData(s, e) {
            if (cboSupplier1.GetSelectedIndex() == -1 && cboSupplier2.GetSelectedIndex() == -1 && cboSupplier3.GetSelectedIndex() == -1 && cboSupplier4.GetSelectedIndex() == -1 && cboSupplier5.GetSelectedIndex() == -1 && cboSupplier6.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Supplier !', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            else if (cboSupplier1.GetSelectedIndex() == -1) {
                toastr.warning('Please Select Supplier 1!', 'Warning');
                toastr.options.closeButton = false;
                toastr.options.debug = false;
                toastr.options.newestOnTop = false;
                toastr.options.progressBar = false;
                toastr.options.preventDuplicates = true;
                toastr.options.onclick = null;
                e.processOnServer = false;
                return;
            }
            //check data supplier bind dari database
            else if (cboSupplier6.GetText() != "") {
                if (cboSupplier6.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 6!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier5.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 5', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier4.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 4', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 3', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 2', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier6.GetText() == cboSupplier1.GetText() || cboSupplier6.GetText() == cboSupplier2.GetText() || cboSupplier6.GetText() == cboSupplier3.GetText() || cboSupplier6.GetText() == cboSupplier4.GetText() || cboSupplier6.GetText() == cboSupplier5.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier5.GetText() == cboSupplier1.GetText() || cboSupplier5.GetText() == cboSupplier2.GetText() || cboSupplier5.GetText() == cboSupplier3.GetText() || cboSupplier5.GetText() == cboSupplier4.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier4.GetText() == cboSupplier1.GetText() || cboSupplier4.GetText() == cboSupplier2.GetText() || cboSupplier4.GetText() == cboSupplier3.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetText() == cboSupplier1.GetText() || cboSupplier3.GetText() == cboSupplier2.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }
            else if (cboSupplier5.GetText() != "") {
                if (cboSupplier5.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 5!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier4.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 4!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 3!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier5.GetText() == cboSupplier1.GetText() || cboSupplier5.GetText() == cboSupplier2.GetText() || cboSupplier5.GetText() == cboSupplier3.GetText() || cboSupplier5.GetText() == cboSupplier4.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier4.GetText() == cboSupplier1.GetText() || cboSupplier4.GetText() == cboSupplier2.GetText() || cboSupplier4.GetText() == cboSupplier3.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetText() == cboSupplier1.GetText() || cboSupplier3.GetText() == cboSupplier2.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }
            else if (cboSupplier4.GetText() != "") {
                if (cboSupplier4.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 4!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 3!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier4.GetText() == cboSupplier1.GetText() || cboSupplier4.GetText() == cboSupplier2.GetText() || cboSupplier4.GetText() == cboSupplier3.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetText() == cboSupplier1.GetText() || cboSupplier3.GetText() == cboSupplier2.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }
            else if (cboSupplier3.GetText() != "") {
                if (cboSupplier3.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 3!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier3.GetText() == cboSupplier1.GetText() || cboSupplier3.GetText() == cboSupplier2.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }

            else if (cboSupplier2.GetText() != "") {
                if (cboSupplier2.GetSelectedIndex() == -1) {
                    toastr.warning('Please Input Valid Supplier 2!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
                else if (cboSupplier2.GetText() == cboSupplier1.GetText()) {
                    toastr.warning('Duplicate Supplier!', 'Warning');
                    toastr.options.closeButton = false;
                    toastr.options.debug = false;
                    toastr.options.newestOnTop = false;
                    toastr.options.progressBar = false;
                    toastr.options.preventDuplicates = true;
                    toastr.options.onclick = null;
                    e.processOnServer = false;
                    return;
                }
            }

            cbUpdate.PerformCallback(Grid.cpKVProjectYear + '|' + Grid.cpKVProjectID + '|' + Grid.cpKVPartNo + '|' + Grid.cpKVModel);
            Grid.PerformCallback(Grid.cpKVProjectID);
            popup.Hide();
        }
</script>
<style type="text/css">
    .td-col-l
    {
        padding:0px 0px 0px 10px;
        width:100px;
    }
    .td-col-m
    {
        width:10px;
    }
    td.col-r
    {
        width:200px;
    }
    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div style="padding: 5px 5px 5px 5px">
        <div style="padding:5px 5px 5px 5px">
        <table style="width: 100%; border: 1px solid black">
             <tr style="height:10px">
                <td class="td-col-l"></td>
                <td class="td-col-m"></td>
                <td class="td-col-r"></td>
                <td ></td>
                <td></td>
            </tr>
              <tr style="height:35px">
                <td colspan="5" style="padding:0px 0px 0px 10px">
                    <dx:ASPxButton ID="btnBack" runat="server" Text="Back" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnBack" Theme="Default">                        
                        <Paddings Padding="2px" />
                    </dx:ASPxButton>
                    &nbsp; &nbsp;
                     <dx:ASPxButton ID="btnDownload" runat="server" Text="Download" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnDownload"  Theme="Default">                        
                        <Paddings Padding="2px" />
                        <%--<ClientSideEvents Click="Validation" />--%>
                    </dx:ASPxButton>
                    &nbsp; &nbsp;
                    <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" UseSubmitBehavior="False"
                        Font-Names="Segoe UI" Font-Size="9pt" Width="80px" Height="25px" AutoPostBack="False"
                        ClientInstanceName="btnSubmit"  Theme="Default">                        
                        <Paddings Padding="2px" />
                        <ClientSideEvents Click="function (s,e) {  
                                                    var msg = confirm('Are you sure want to delete this data ?');                
                                                    if (msg == false) {
                                                         e.processOnServer = false;
                                                         return;
                                                    } 
                                                    cbSubmit.PerformCallback();
                                                 }" 
                                                />
                    </dx:ASPxButton>
                    </td>
            </tr>
            <tr style="height:10px">
                <td colspan="5">
                    <dx:ASPxGridViewExporter ID="GridExporter" runat="server" GridViewID="Grid">
                    </dx:ASPxGridViewExporter>
                </td>
                
                    
            </tr>
        </table>
        </div>
        <div>
            <dx:ASPxCallback ID="cbUpdate" runat="server" ClientInstanceName="cbUpdate" >
                <ClientSideEvents EndCallback="GetMessage" />
            </dx:ASPxCallback>
            <dx:ASPxCallback ID="cbSubmit" runat="server" ClientInstanceName="cbSubmit" >
                <ClientSideEvents EndCallback="GetMessage" />
            </dx:ASPxCallback>
             <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ApplicationServices %>"
                SelectCommand="Select Supplier_Code As Code, Supplier_Name As Description From Mst_Supplier">
            </asp:SqlDataSource>
        </div>
        <div style="padding:5px 5px 5px 5px">
            <dx:ASPxGridView ID="Grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="Grid"
                    EnableTheming="True" KeyFieldName="ProjectYear;ProjectID;PartNo;Model" Theme="Office2010Black" Width="100%" 
                    Font-Names="Segoe UI" Font-Size="9pt" OnCustomButtonCallback="Grid_CustomButtonCallback" >
                 <ClientSideEvents EndCallback="OnEndCallback" />
   
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                HorizontalScrollBarMode="Auto" />
                <Columns>
                  <%--  <dx:GridViewCommandColumn ButtonType="Link" Caption=" " VisibleIndex="0" Width="60px">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="Edit" Text="Detail">
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>
                    </dx:GridViewCommandColumn>--%>
                    <dx:GridViewCommandColumn VisibleIndex="0" Caption=" " Width="40px">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="btnDetails" Text="Edit"  />
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="No" FieldName="No" VisibleIndex="1" Width="50px">
                        <Settings AutoFilterCondition="Contains" AllowAutoFilter="False" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px" PaddingTop="3px" PaddingBottom="3px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ProjectYear" Caption="Project Year" VisibleIndex="2"
                        Width="100px">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px" PaddingTop="3px" PaddingBottom="3px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PartNo" Caption="Part No" VisibleIndex="3"
                        Width="120px">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px" PaddingTop="3px" PaddingBottom="3px" />
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="PartName" Width="180px" Caption="Part Name"
                        VisibleIndex="4">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn FieldName="Model" Width="100px" Caption="ModelCd" Visible ="false"
                        VisibleIndex="5">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ModelName" Width="100px" Caption="Model"
                        VisibleIndex="6">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="FIGNo" Width="120px" Caption="FIG No"
                        VisibleIndex="7">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="KeyNo" Width="120px" Caption="Key No"
                        VisibleIndex="8">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TargetPrice" Caption="Target Price" VisibleIndex="9" >
                     <PropertiesTextEdit DisplayFormatString= "#,##0">
                     </PropertiesTextEdit>
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="TargetConsumption" Caption="Target Consumption" VisibleIndex="10" Width="150px" >
                     <PropertiesTextEdit DisplayFormatString="#,##0">
                     </PropertiesTextEdit>
                     <Settings AutoFilterCondition="Contains" />
                     <HeaderStyle Paddings-PaddingLeft="5px" HorizontalAlign="Center" VerticalAlign="Middle">
                         <Paddings PaddingLeft="5px"></Paddings>
                     </HeaderStyle>
                 </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="Supplier1" Width="180px" Caption="Supplier 1" 
                        VisibleIndex="11">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Supplier2" Width="180px" Caption="Supplier 2"
                        VisibleIndex="12">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Supplier3" Width="180px" Caption="Supplier 3"
                        VisibleIndex="13">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Supplier4" Width="180px" Caption="Supplier 4"
                        VisibleIndex="14">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Supplier5" Width="180px" Caption="Supplier 5"
                        VisibleIndex="15">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Supplier6" Width="180px" Caption="Supplier 6"
                        VisibleIndex="16">
                        <Settings AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                            <Paddings PaddingLeft="5px"></Paddings>
                        </HeaderStyle>
                </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsPager Mode="ShowPager" PageSize="10" AlwaysShowPager="true">
                </SettingsPager>
                <Settings ShowFilterRow="True" VerticalScrollBarMode="Auto" VerticalScrollableHeight="270"
                    HorizontalScrollBarMode="Auto" />
                <Styles EditFormColumnCaption-Paddings-PaddingLeft="10px" EditFormColumnCaption-Paddings-PaddingRight="10px">
                    <Header>
                        <Paddings Padding="2px"></Paddings>
                    </Header>
                    <EditFormColumnCaption>
                        <Paddings PaddingLeft="10px" PaddingRight="10px"></Paddings>
                    </EditFormColumnCaption>
                </Styles>
            </dx:ASPxGridView>
           
        </div>
        <div>
            <dx:ASPxPopupControl ID="popup" ClientInstanceName="popup" runat="server" AllowDragging="true"  OnWindowCallback="popup_WindowCallback"
                HeaderText="Edit Form" Width="500" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
            <contentcollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <table runat="server" style="width:100%">
                        <tr style="height:35px">
                            <td class="td-col-l">
                                 <dx:ASPxLabel ID="lblProject" ClientInstanceName="lblProject" runat="server" Text="Project ID"
                                               Font-Names="Segoe UI" Font-Size="9pt">
                                 </dx:ASPxLabel>
                            </td>
                            <td class="td-col-m"></td>
                            <td class="td-col-r">
                                <dx:ASPxTextBox ID="txtProjectID" runat="server" ClientInstanceName="txtProjectID" Enabled="false" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td class="td-col-l">
                                 <dx:ASPxLabel ID="lblPartNo" ClientInstanceName="lblPartNo" runat="server" Text="Part No"
                                               Font-Names="Segoe UI" Font-Size="9pt">
                                 </dx:ASPxLabel>
                            </td>
                            <td class="td-col-m"></td>
                            <td class="td-col-r">
                                <dx:ASPxTextBox ID="txtPartNo" runat="server" ClientInstanceName="txtPartNo" Enabled="false" Font-Names="Segoe UI" Font-Size="9pt">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td class="td-col-l">
                                 <dx:ASPxLabel ID="lblSupplier1" ClientInstanceName="lblSupplier1" runat="server" Text="Supplier 1"
                                               Font-Names="Segoe UI" Font-Size="9pt">
                                 </dx:ASPxLabel>
                            </td>
                            <td class="td-col-m"></td>
                            <td class="td-col-r">
                                <dx:ASPxComboBox ID="cboSupplier1" runat="server" ClientInstanceName="cboSupplier1"
                                    Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                                    ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                                    Height="25px">
                                    <Columns>
                                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                                    </Columns>
                                    <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                    </ItemStyle>
                                    <ButtonStyle Width="5px" Paddings-Padding="4px">
<Paddings Padding="4px"></Paddings>
                                    </ButtonStyle>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td class="td-col-l">
                                 <dx:ASPxLabel ID="lblSupplier2" ClientInstanceName="lblSupplier2" runat="server" Text="Supplier 2"
                                               Font-Names="Segoe UI" Font-Size="9pt">
                                 </dx:ASPxLabel>
                            </td>
                            <td class="td-col-m"></td>
                            <td class="td-col-r">
                                <dx:ASPxComboBox ID="cboSupplier2" runat="server" ClientInstanceName="cboSupplier2"
                                    Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                                    ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                                    Height="25px">
                                    <Columns>
                                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                                    </Columns>
                                    <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                    </ItemStyle>
                                    <ButtonStyle Width="5px" Paddings-Padding="4px">
<Paddings Padding="4px"></Paddings>
                                    </ButtonStyle>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td class="td-col-l">
                                 <dx:ASPxLabel ID="lblSupplier3" ClientInstanceName="lblSupplier3" runat="server" Text="Supplier 3"
                                               Font-Names="Segoe UI" Font-Size="9pt">
                                 </dx:ASPxLabel>
                            </td>
                            <td class="td-col-m"></td>
                            <td class="td-col-r">
                                <dx:ASPxComboBox ID="cboSupplier3" runat="server" ClientInstanceName="cboSupplier3"
                                    Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                                    ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                                    Height="25px">
                                    <Columns>
                                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                                    </Columns>
                                    <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                    </ItemStyle>
                                    <ButtonStyle Width="5px" Paddings-Padding="4px">
<Paddings Padding="4px"></Paddings>
                                    </ButtonStyle>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td class="td-col-l">
                                 <dx:ASPxLabel ID="lblSupplier4" ClientInstanceName="lblSupplier4" runat="server" Text="Supplier 4"
                                               Font-Names="Segoe UI" Font-Size="9pt">
                                 </dx:ASPxLabel>
                            </td>
                            <td class="td-col-m"></td>
                            <td class="td-col-r">
                                 <dx:ASPxComboBox ID="cboSupplier4" runat="server" ClientInstanceName="cboSupplier4"
                                    Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                                    ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                                    Height="25px">
                                    <Columns>
                                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                                    </Columns>
                                    <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                     </ItemStyle>
                                    <ButtonStyle Width="5px" Paddings-Padding="4px">
<Paddings Padding="4px"></Paddings>
                                    </ButtonStyle>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td class="td-col-l">
                                 <dx:ASPxLabel ID="ASPxLabel1" ClientInstanceName="lblSupplier5" runat="server" Text="Supplier 5"
                                               Font-Names="Segoe UI" Font-Size="9pt">
                                 </dx:ASPxLabel>
                            </td>
                            <td class="td-col-m"></td>
                            <td class="td-col-r">
                                 <dx:ASPxComboBox ID="cboSupplier5" runat="server" ClientInstanceName="cboSupplier5"
                                    Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                                    ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                                    Height="25px">
                                    <Columns>
                                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                                    </Columns>
                                    <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                     </ItemStyle>
                                    <ButtonStyle Width="5px" Paddings-Padding="4px">
<Paddings Padding="4px"></Paddings>
                                    </ButtonStyle>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td class="td-col-l">
                                 <dx:ASPxLabel ID="lblSupplier6" ClientInstanceName="lblSupplier5" runat="server" Text="Supplier 6"
                                               Font-Names="Segoe UI" Font-Size="9pt">
                                 </dx:ASPxLabel>
                            </td>
                            <td class="td-col-m"></td>
                            <td class="td-col-r">
                                 <dx:ASPxComboBox ID="cboSupplier6" runat="server" ClientInstanceName="cboSupplier6"
                                    Width="200px" Font-Names="Segoe UI" TextField="Description" DataSourceID="SqlDataSource4"
                                    ValueField="Code" TextFormatString="{1}" Font-Size="9pt" Theme="Office2010Black"
                                    DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith" EnableIncrementalFiltering="True"
                                    Height="25px">
                                    <Columns>
                                        <dx:ListBoxColumn Caption="Code" FieldName="Code" Width="100px" />
                                        <dx:ListBoxColumn Caption="Description" FieldName="Description" Width="300px" />
                                    </Columns>
                                    <ItemStyle Height="10px" Paddings-Padding="4px" >
<Paddings Padding="4px"></Paddings>
                                     </ItemStyle>
                                    <ButtonStyle Width="5px" Paddings-Padding="4px">
<Paddings Padding="4px"></Paddings>
                                    </ButtonStyle>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                        <tr style="height:10px">
                            <td colspan="3"></td>
                        </tr>
                        <tr style="height:35px">
                            <td colspan="3">
                                 <dx:ASPxButton ID="btnUpdate" runat="server" Text="Save" ClientInstanceName="btnUpdate" 
                                                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" AutoPostBack="False">
                                    <ClientSideEvents Click="InsertData" />
                                    <Paddings Padding="2px" />
                                 </dx:ASPxButton> &nbsp; &nbsp;
                                 <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" ClientInstanceName="btnCancel" 
                                                Font-Names="Segoe UI" Font-Size="9pt" Width="80px" AutoPostBack="False">
                                    <ClientSideEvents Click="function(s, e) {popup.Hide();}" />
                                    <Paddings Padding="2px" />
                                 </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                    
                   
                </dx:PopupControlContentControl>
            </contentcollection>
            </dx:ASPxPopupControl>
        </div>
    </div>
    
</asp:Content>
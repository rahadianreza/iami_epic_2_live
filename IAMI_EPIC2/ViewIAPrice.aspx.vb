﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI

Public Class ViewIAPrice
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        up_LoadReport()
    End Sub

    Private Sub up_LoadReport()
        Dim sql As String
        Dim ds As New DataSet
        Dim Report As New rptIAPrice
        Dim pIANumber As String
        Dim pRev As Integer
        Dim pCENumber As String
        Dim pAction As String

        pIANumber = Session("IANumber")
        pRev = Session("Revision")
        pCENumber = Session("CENumber")
        pAction = Session("Action")

        Try
            Using con As New SqlConnection(Sconn.Stringkoneksi)
                con.Open()

                sql = "exec sp_IA_Report '" & pCENumber & "' , '" & pIANumber & "' , " & pRev & ""

                Dim da As New SqlDataAdapter(sql, con)
                da.Fill(ds)
                Report.DataSource = ds
                Report.Name = "IAPrice_" & Format(CDate(Now), "yyyyMMdd_HHmmss")
                ASPxDocumentViewer1.Report = Report

                ASPxDocumentViewer1.DataBind()

                con.Close()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim pIANumber As String
        Dim pRev As Integer
        Dim pCENumber As String
        Dim pAction As String

        pIANumber = Session("IANumber")
        pRev = Session("Revision")
        pCENumber = Session("CENumber")
        pAction = Session("Action")

        If pAction = "1" Then
            Response.Redirect("~/IAPriceDetail.aspx?ID=" & pIANumber & "|" & pRev & "|" & pIANumber)
        Else
            Response.Redirect("~/IAPriceApprovalDetail.aspx?ID=" & pIANumber & "|" & pRev & "|" & pIANumber)
        End If

    End Sub

End Class
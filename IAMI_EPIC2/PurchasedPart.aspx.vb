﻿Imports Microsoft.VisualBasic
Imports System
Imports DevExpress.Utils
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports OfficeOpenXml
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class PurchasedPart
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Public AuthInsert As Boolean = False
    Public AuthUpdate As Boolean = False
    Public AuthDelete As Boolean = False
#End Region

#Region "Initialization"
    Private Sub Page_Init(ByVal sender As Object, ByVale As System.EventArgs) Handles Me.Init
        If Not Page.IsPostBack Then
            up_GridLoad()
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sGlobal.getMenu("A050")
        Master.SiteTitle = sGlobal.menuName
        pUser = Session("user")
        AuthUpdate = sGlobal.Auth_UserUpdate(pUser, "A050")
    End Sub
#End Region

#Region "Procedure"
    Private Sub up_GridLoad()
        Dim ErrMsg As String = ""
        Dim Ses As New List(Of ClsPurchasedPart)
        Ses = ClsPurchasedPartDB.getlist(ErrMsg)
        If ErrMsg = "" Then
            Grid.DataSource = Ses
            Grid.DataBind()
        End If
    End Sub

    Private Sub show_error(ByVal msgType As MsgTypeEnum, ByVal ErrMsg As String, ByVal pVal As Integer)
        Grid.JSProperties("cp_message") = ErrMsg
        Grid.JSProperties("cp_type") = msgType
        Grid.JSProperties("cp_val") = pVal
    End Sub

    Private Sub up_ExportExcel(ByVal pGrid As ASPxGridView)

    End Sub
#End Region

#Region "Control Event"
 
    Protected Sub Grid_AfterPerformCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs) Handles Grid.AfterPerformCallback
        If e.CallbackName <> "CANCELEDIT" Then
            up_GridLoad()
        End If
    End Sub


    Protected Sub Grid_RowInserting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsPurchasedPart With {.Purchased_Part_Name = e.NewValues("Purchased_Part_Name"),
                                         .Specification = e.NewValues("Specification"),
                                         .UoM = e.NewValues("UoM"),
                                         .Price = e.NewValues("Price"),
                                         .RegisterBy = pUser}

        ClsPurchasedPartDB.Insert(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Save data successfully!", 1)
            up_GridLoad()
        End If
    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles Grid.RowUpdating
        e.Cancel = True
        Dim pErr As String = ""
        Dim Ses As New ClsPurchasedPart With {.Purchased_Part_Code = e.NewValues("Purchased_Part_Code"),
                                         .Purchased_Part_Name = e.NewValues("Purchased_Part_Name"),
                                         .Specification = e.NewValues("Specification"),
                                         .UoM = e.NewValues("UoM"),
                                         .Price = e.NewValues("Price"),
                                         .RegisterBy = pUser}

        ClsPurchasedPartDB.Update(Ses, pErr)

        If pErr <> "" Then
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        Else
            Grid.CancelEdit()
            show_error(MsgTypeEnum.Success, "Update data successfully!", 1)
            up_GridLoad()
        End If
    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        e.Cancel = True
        Dim pErr As String = ""
        Dim ses As New ClsPurchasedPart With {.Purchased_Part_Code = e.Values("Purchased_Part_Code")}
        ClsPurchasedPartDB.Delete(ses, pErr)
        If pErr = "" Then
            show_error(MsgTypeEnum.Success, "Delete data successfully!", 1)
            up_GridLoad()
        Else
            show_error(MsgTypeEnum.ErrorMsg, pErr, 1)
        End If
    End Sub


    Private Sub Grid_BeforeGetCallbackResult(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.BeforeGetCallbackResult
        If Grid.IsNewRowEditing Then
            Grid.SettingsCommandButton.UpdateButton.Text = "Save"
        End If
    End Sub

    Private Sub Grid_CellEditorInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs) Handles Grid.CellEditorInitialize
        If Grid.IsNewRowEditing Then
            If e.Column.FieldName = "Purchased_Part_Code" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
                e.Editor.Visible = False
                e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False
            End If
        End If
        If Not Grid.IsNewRowEditing Then
            If e.Column.FieldName = "Purchased_Part_Code" Or e.Column.FieldName = "Purchased_Part_Name" Then
                e.Editor.ReadOnly = True
                e.Editor.ForeColor = Color.Silver
            End If
        End If
    End Sub


    Protected Sub Grid_StartRowEditing(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxStartRowEditingEventArgs)
        If (Not Grid.IsNewRowEditing) Then
            Grid.DoRowValidation()
        End If
        show_error(MsgTypeEnum.Info, "", 0)
    End Sub

   
    Private Sub Grid_CustomCallback(sender As Object, e As ASPxGridViewCustomCallbackEventArgs) Handles Grid.CustomCallback
        Dim pFunction As String = Split(e.Parameters, "|")(0)

        Select Case pFunction
            Case "excel"
                up_ExportExcel(Grid)


        End Select

    End Sub


    
    Protected Sub Grid_RowValidating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataValidationEventArgs)
        Dim GridData As DevExpress.Web.ASPxGridView.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView.ASPxGridView)

        For Each column As GridViewColumn In Grid.Columns
            Dim dataColumn As GridViewDataColumn = TryCast(column, GridViewDataColumn)
            If dataColumn Is Nothing Then
                Continue For
            End If

            If dataColumn.FieldName = "Purchased_Part_Name" Then
                If IsNothing(e.NewValues("Purchased_Part_Name")) OrElse e.NewValues("Purchased_Part_Name").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Purchased Part Name !"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1)
                    Exit Sub
                Else
                    If e.IsNewRow Then
                        If ClsPurchasedPartDB.isExist(e.NewValues("Purchased_Part_Name")) Then
                            e.Errors(dataColumn) = "Purchased Part Name is already exist!"
                            show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1)
                        End If
                    End If
                End If
            End If

            If dataColumn.FieldName = "Specification" Then
                If IsNothing(e.NewValues("Specification")) OrElse e.NewValues("Specification").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input Spesification!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "UoM" Then
                If IsNothing(e.NewValues("UoM")) OrElse e.NewValues("UoM").ToString.Trim = "" Then
                    e.Errors(dataColumn) = "Please input UoM!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1)
                    Exit Sub
                End If
            End If

            If dataColumn.FieldName = "Price" Then
                If IsNothing(e.NewValues("Price")) OrElse e.NewValues("Price").ToString.Trim = "" OrElse e.NewValues("Price").ToString.Trim = 0 Then
                    e.Errors(dataColumn) = "Please input Price!"
                    show_error(MsgTypeEnum.Warning, e.Errors(dataColumn), 1)
                    Exit Sub
                End If
            End If

        Next column
    End Sub


    Protected Sub btnDownload_Click(sender As Object, e As EventArgs) Handles btnDownload.Click
        Dim ParameterName As String = ""

        Dim ps As New PrintingSystem()
        Dim link1 As New PrintableComponentLink(ps)

        up_GridLoad()
        ParameterName = "PurchasedPart"
        link1.Component = GridExporter7
      

        Dim compositeLink As New CompositeLink(ps)
        compositeLink.Links.AddRange(New Object() {link1})

        compositeLink.CreateDocument()
        Using stream As New MemoryStream()
            compositeLink.PrintingSystem.ExportToXlsx(stream)
            Response.Clear()
            Response.Buffer = False
            Response.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AppendHeader("Content-Disposition", "attachment; filename=" & ParameterName & Format(Date.Now, "ddMMyyyyhhmmss") & ".xlsx")
            Response.BinaryWrite(stream.ToArray())
            Response.End()
        End Using
        ps.Dispose()
    End Sub

#End Region




End Class
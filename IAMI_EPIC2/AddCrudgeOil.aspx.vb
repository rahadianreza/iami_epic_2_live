﻿Public Class AddCrudgeOil
    Inherits System.Web.UI.Page

#Region "Declaration"
    Dim pUser As String = ""
    Dim statusAdmin As String
    Dim fcategroy As String
    Dim fprtype As String
    Dim fRefresh As Boolean = False
    Dim vStatus As String = ""
    Dim OilType As String = ""
    Dim Period As String = ""
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pUser = Session("user")
        sGlobal.getMenu("A090")
        Master.SiteTitle = "CRUDGE OIL DETAIL"
        pUser = Session("user")
        statusAdmin = ClsPRListDB.GetStatusUser(pUser)
        Dim s As String = Request.QueryString("ID")
        Dim script As String = ""
        If (Not Page.IsPostBack) AndAlso (Not Page.IsCallback) Then
            If s <> "" Then
                btnSubmit.Visible = False
                btnUpdate.Visible = True
                'cboCategoryMaterial.Enabled = False

                script = "dtPeriod.SetEnabled(false);" & vbCrLf & _
                         "cboCategoryMaterial.SetEnabled(false);"
                ScriptManager.RegisterStartupScript(dtPeriod, dtPeriod.GetType(), "dtPeriod", script, True)
                'dtPeriod.Enabled = False
                OilType = s.Split("|")(0)
                Period = s.Split("|")(1)


                Dim ds As DataSet = ClsCrudgeOilDB.getDetail(OilType, Period, pUser)

                cboCategoryMaterial.Value = ds.Tables(0).Rows(0)(0).ToString()
                cboUoM.Text = ds.Tables(0).Rows(0)(1).ToString()
                txtPrice.Text = ds.Tables(0).Rows(0)(2).ToString()
                dtPeriod.Value = CDate(ds.Tables(0).Rows(0)(3).ToString())
            Else
                btnSubmit.Visible = True
                btnUpdate.Visible = False

                script = "dtPeriod.SetEnabled(true);" & vbCrLf & _
                         "cboCategoryMaterial.SetEnabled(true);"
                ScriptManager.RegisterStartupScript(dtPeriod, dtPeriod.GetType(), "dtPeriod", script, True)
                'cboCategoryMaterial.Enabled = True
            End If
        End If

    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("CrudgeOil.aspx")
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim pErr As String = ""
        Dim Period As String
        If String.IsNullOrEmpty(cboCategoryMaterial.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Oil Type"
        ElseIf String.IsNullOrEmpty(cboUoM.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input UoM"
        ElseIf String.IsNullOrEmpty(txtPrice.Value) Or txtPrice.Text = "0" Then
            cbSave.JSProperties("cpMessage") = "Please Input Price"
        ElseIf String.IsNullOrEmpty(dtPeriod.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Period"
        Else
            Period = Format(dtPeriod.Value, "yyyy-MM-dd")
            ClsCrudgeOilDB.Insert(cboCategoryMaterial.Value, Period, cboUoM.Value, txtPrice.Text, pUser, pErr)
            If pErr = "" Then
                cbSave.JSProperties("cpMessage") = "Data Saved Successfully!"
            Else
                cbSave.JSProperties("cpMessage") = pErr
            End If

        End If
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        cbClear.JSProperties("cpMessage") = "Data Clear Successfully!"

    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim pErr As String = ""
        Dim Period As String
       If String.IsNullOrEmpty(cboCategoryMaterial.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Oil Type"
        ElseIf String.IsNullOrEmpty(cboUoM.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input UoM"
        ElseIf String.IsNullOrEmpty(txtPrice.Value) Or txtPrice.Text = "0" Then
            cbSave.JSProperties("cpMessage") = "Please Input Price"
        ElseIf String.IsNullOrEmpty(dtPeriod.Value) Then
            cbSave.JSProperties("cpMessage") = "Please Input Period"
        Else
            Period = Format(dtPeriod.Value, "yyyy-MM-dd")
            ClsCrudgeOilDB.Update(cboCategoryMaterial.Value, Period, cboUoM.Value, txtPrice.Text, pUser, pErr)
            If pErr = "" Then
                cbSave.JSProperties("cpMessage") = "Data Updated Successfully!"
            Else
                cbSave.JSProperties("cpMessage") = pErr
            End If

        End If
    End Sub
End Class